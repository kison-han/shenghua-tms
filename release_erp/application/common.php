<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------
use app\common\help\Help;
use \Underscore\Types\Arrays;
// 应用公共文件
function getProportionOneToOne(){
    $firstday = date("Y-m");
    $data['proportion_time'] = date('Ymd',strtotime("$firstday +1 month -1 day"));
   $getProportionOneToOne = Help::callSoaErp('post','/system/getProportionOneToOne',$data);
//   var_dump($getProportionOneToOne);exit;
}

function noOfPax($date,$btb_tour_id){

   return Help::callSoaErp('post','/b2b_operate/noOfPax',['date'=>$date,'btb_tour_id'=>$btb_tour_id])['data'][0]['pax'];

}

function sort_common($d){
    return Arrays::sort($d,'count','desc');
}

function getSeat($d){
    $seat = [];
    foreach($d as $v){
        if($v['seat_no']){
            $seat[] = $v['seat_no'];
        }
    }

    $seat = Arrays::sort($seat,'asc');
    $i = '';
    $ii = 0;
    $bb = 0;
    foreach($seat as $k=>$v){
        if($k==0){
            $i.=$v;$ii=$v;
        }else{
            if($ii+1==$v){
                $bb = 1;$ii=$v;
                if(count($seat)==$k+1){
                    $i.='-'.$v;
                }
                continue;
            }else{
                if($bb){
                    $i.='-'.$v;
                }else{
                    $i.=','.$v;
                }
                $ii=$v;
            }
        }
    }

    return trim($i,',');


}

function getAdult($d){
    $age_group = Arrays::group($d,'age_group');
    return count($age_group['Adult']);
}
function getChild($d){
    $age_group = Arrays::group($d,'age_group');
    return count($age_group['Child']);
}
function getInfant($d){
    $age_group = Arrays::group($d,'age_group');
    return count($age_group['Infant']);
}


if (!function_exists("to_JS_date_format")) {
    /*
     * Matches each symbol of PHP date format standard
     * with jQuery equivalent codeword
     * @author Tristan Jahier
     */
    function to_JS_date_format($php_format, $type = 1)
    {
        /*
         * $type 1 ==>jquery ui
         * $type 2 ==>masked input
         * $type 3 ==>date js
         */
        switch ($type) {
            case 3:
                $SYMBOLS_MATCHING = array(
                    // Day
                    'd' => 'dd',
                    'D' => 'ddd',
                    'j' => 'd',
                    'l' => 'dddd',
                    'N' => '',
                    'S' => '',
                    'w' => '',
                    'z' => '',
                    // Week
                    'W' => '',
                    // Month
                    'F' => '',
                    'm' => 'MM',
                    'M' => 'MMM',
                    'n' => 'M',
                    't' => '',
                    // Year
                    'L' => '',
                    'o' => '',
                    'Y' => 'yyyy',
                    'y' => 'yy',
                    // Time
                    'a' => 't',
                    'A' => 'tt',
                    'B' => '',
                    'g' => 'h',
                    'G' => 'H',
                    'h' => 'hh',
                    'H' => 'HH',
                    'i' => 'mm',
                    's' => 'ss',
                    'u' => ''
                );
                break;
            case 2:
                $SYMBOLS_MATCHING = array(
                    // Day
                    'd' => '99',
                    'D' => 'aaa',
                    'j' => '99',
                    'l' => '',
                    'N' => '9',
                    'S' => '',
                    'w' => '',
                    'z' => '999',
                    // Week
                    'W' => '',
                    // Month
                    'F' => '',
                    'm' => '99',
                    'M' => 'aaa',
                    'n' => '99',
                    't' => '99',
                    // Year
                    'L' => '',
                    'o' => '',
                    'Y' => '9999',
                    'y' => '99',
                    // Time
                    'a' => 'aa',
                    'A' => 'aa',
                    'B' => '999',
                    'g' => '99',
                    'G' => '99',
                    'h' => '99',
                    'H' => '99',
                    'i' => '99',
                    's' => '99',
                    'u' => ''
                );
                break;
            case 1:
            default:
                $SYMBOLS_MATCHING = array(
                    // Day
                    'd' => 'dd',
                    'D' => 'D',
                    'j' => 'd',
                    'l' => 'DD',
                    'N' => '',
                    'S' => '',
                    'w' => '',
                    'z' => 'o',
                    // Week
                    'W' => '',
                    // Month
                    'F' => 'MM',
                    'm' => 'mm',
                    'M' => 'M',
                    'n' => 'm',
                    't' => '',
                    // Year
                    'L' => '',
                    'o' => '',
                    'Y' => 'yy',
                    'y' => 'y',
                    // Time
                    'a' => '',
                    'A' => '',
                    'B' => '',
                    'g' => '',
                    'G' => '',
                    'h' => '',
                    'H' => '',
                    'i' => '',
                    's' => '',
                    'u' => ''
                );
                break;
        }
        $jqueryui_format = "";
        $escaping = false;
        for ($i = 0; $i < strlen($php_format); $i++) {
            $char = $php_format[$i];
            if ($char === '\\') // PHP date format escaping character
            {
                $i++;
                if ($escaping) $jqueryui_format .= $php_format[$i];
                else $jqueryui_format .= '\'' . $php_format[$i];
                $escaping = true;
            } else {
                if ($escaping) {
                    $jqueryui_format .= "'";
                    $escaping = false;
                }
                if (isset($SYMBOLS_MATCHING[$char]))
                    $jqueryui_format .= $SYMBOLS_MATCHING[$char];
                else
                    $jqueryui_format .= $char;
            }
        }
        return $jqueryui_format;
    }
}


function base_url($url){
    return '/'.$url;
}

if ( ! function_exists('anchor'))
{
    function anchor($uri = '', $title = '', $attributes = '')
    {
        $title = (string) $title;

        if ( ! is_array($uri))
        {
            $site_url = ( ! preg_match('!^\w+://! i', $uri)) ? ($uri) : $uri;
        }
        else
        {
            $site_url = ($uri);
        }

        if ($title == '')
        {
            $title = $site_url;
        }

        if ($attributes != '')
        {
//            $attributes = _parse_attributes($attributes);
        }

        return '<a href="'.$site_url.'"'.$attributes.'>'.$title.'</a>';
    }
}

if ( ! function_exists('getSupplier'))
{
	
	    function getSupplier($supplier_id,$field)
    {
		if(!is_numeric($supplier_id))
		    return "null";

		$supplier_data=file_get_contents('uploads/supplier.dat');
		if(!$supplier_data)
		{
            $supplier_data= Help::callSoaErp('post','/source/getSupplier',"");

           $supplier_data=$supplier_data['data'];
            $supplier_data=json_encode($supplier_data,265);
               file_put_contents('uploads/supplier.dat',$supplier_data);
        }
        $supplier_data=json_decode($supplier_data,true);
		foreach ($supplier_data as $k=>$v)
        {
            if($v['supplier_id']==$supplier_id)
                return $v[$field];
        }


        $supplier_data= Help::callSoaErp('post','/source/getSupplier',"");
        $supplier_data=$supplier_data['data'];
        $supplier_data=json_encode($supplier_data,265);
        file_put_contents('uploads/supplier.dat',$supplier_data);

    }
	
}



if ( ! function_exists('getCity'))
{

    function getCity($city_id,$field,$level=0,$getfather=false){

        //	$cityResult = session('city');
    $cityString=file_get_contents('map.json');
    $cityResult=json_decode($cityString,true);
    if(!$cityString) {


        $cityData= Help::callSoaErp('post', '/city/getCity', "")['msg'];
        file_put_contents('map.json',$cityData);
        $cityResult=$cityData;
    }
    if(!$getfather)
    {
       foreach ($cityResult as $k=>$v)
       {
         if( $v['city_id']==$city_id)
         {
             return $v[$field];
         }
       }
    }

    $cityContent = [];
    if($level!=0){
        for($i=0;$i<count($cityResult);$i++){
            if($cityResult[$i]['level'] == $level){
                $cityContent[] = $cityResult[$i];
            }
        }
    }else{
        for($i=0;$i<count($cityResult);$i++){
            if($cityResult[$i]['parent_id'] == $city_id){
                return $cityResult[$i][$field];
                $cityContent[] = $cityResult[$i];
            }
        }
    }


    return array_values($cityContent);
}

}

if ( ! function_exists('put_csv'))
{
    //导出csv文件
     function put_csv($list,$title)
{
    $file_name = "sh".time().".csv";
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename='.$file_name );
    header('Cache-Control: max-age=0');
    $file = fopen('php://output',"a");
    $limit = 10000;
    $calc = 0;
    foreach ($title as $v){
        $tit[] = iconv('UTF-8', 'GB2312//IGNORE',$v);
    }
    fputcsv($file,$tit);
    foreach ($list as $v){
        $calc++;
        if($limit == $calc){
            ob_flush();
            flush();
            $calc = 0;
        }
        foreach($v as $t){
            $tarr[] = iconv('UTF-8', 'GB2312//IGNORE',$t);
        }
        fputcsv($file,$tarr);
        unset($tarr);
    }
    unset($list);
    fclose($file);
    exit();
}

}