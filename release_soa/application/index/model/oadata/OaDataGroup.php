<?php

namespace app\index\model\oadata;

use think\Db;
use think\Model;
use app\common\help\Help;

class OaDataGroup extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'data_group';

    /**
     * 获取资料分组
     * 韩
     */
    public function getOaDataGroup($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
        $data = "1=1 ";

        if(isset($params['group_id'])){ //分类ID
            $data.= " and data_group.group_id= ".$params['group_id'];
        }

        if(!empty($params['group_name'])){ //分类名称
            $data.= ' and data_group.group_name like "%' . $params['group_name'] . '%"';
        }

        if(!empty($params['remark'])){ //备注
            $data.=' and data_group.remark ="'.$params['remark'].'"';
        }

        if($is_count==true){
            $result = $this->table("data_group")->where($data)->count();
        }else{
            if($is_page == true){
                $result = $this->table("data_group")->
                where($data)->limit($page, $page_size)->order('create_time desc')->
                field(['group_id','group_name','remark','father_id'=>'father_id2','level_id',
                    "(select group_name from data_group where group_id = father_id2)"=>'superior_datagroup_name',
                    "(select nickname from user where user.user_id = data_group.create_user_id)"=> 'create_user_name',
                    "(select nickname from user where user.user_id = data_group.update_user_id)"=> 'update_user_name',
                    'status',
                    'from_unixtime(create_time)'=> 'create_time',
                    'from_unixtime(update_time)'=> 'update_time'
                ])->select();
            }else{
                $result = $this->table("data_group")->
                where($data)->order('create_time desc')->
                field(['group_id','group_name','remark','father_id'=>'father_id2','level_id',
                    "(select group_name from data_group where group_id = father_id2)"=>'superior_datagroup_name',
                    "(select nickname from user where user.user_id = data_group.create_user_id)"=> 'create_user_name',
                    "(select nickname from user where user.user_id = data_group.update_user_id)"=> 'update_user_name',
                    'status',
                    'from_unixtime(create_time)'=> 'create_time',
                    'from_unixtime(update_time)'=> 'update_time'
                ])->select();
            }
        }
        return $result;
    }

    /**
     * 添加资料分组
     * 韩
     */
    public function addDataGroup($params){
        $t = time();

        $data['group_name'] = $params['group_name'];
        $data['father_id'] = $params['father_id'];
        $data['level_id'] = $params['level_id'];
        $data['remark'] = $params['remark'];
        $data['create_time'] = $t;
        $data['create_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        $data['update_user_id'] = $params['user_id'];
        $data['status'] = $params['status'];

        Db::startTrans();
        try{
            $this->table("data_group")->insertGetId($data);
            $result = 1;
            // 提交事务
            Db::commit();
        }catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return $result;
    }

    /**
     * 修改资料分组
     * 韩
     */
    public function updateDataGroupByGroupId($params){
        $t = time();

        $data['group_name'] = $params['group_name'];
        $data['father_id'] = $params['father_id'];
        $data['level_id'] = $params['level_id'];
        $data['remark'] = $params['remark'];
//        $data['create_time'] = $t;
//        $data['create_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        $data['update_user_id'] = $params['user_id'];
        $data['status'] = $params['status'];

        Db::startTrans();
        try {
            Db::name('data_group')->where("group_id = " . $params['group_id'])->update($data);
            $result = 1;
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return $result;
    }

    /**
     * 获取组装资料分组所有层级关系
     */
    public function getOaDataGroupAjax(){

        $res = DB::query("select * from data_group where father_id = 0 and level_id=1");

        $returnArr = [];

        foreach($res as $k1=>$v1){ //for1
            $returnArr[$k1]['value'] = $v1['group_id'];
            $returnArr[$k1]['name'] = $v1['group_name'];
            $returnArr[$k1]['level_id'] = $v1['level_id'];
            $returnArr[$k1]['father_id'] = $v1['father_id'];

            $map2['father_id'] = $v1['group_id'];
            $map2['level_id'] = 2;
            $result2 = DB::query("select * from data_group where father_id = {$map2['father_id']} and level_id={$map2['level_id']}");

            foreach($result2 as $k2=>$val2){ //for2
                $returnArr[$k1]['children'][$k2]['value'] = $val2['group_id'];
                $returnArr[$k1]['children'][$k2]['name'] = $val2['group_name'];
                $returnArr[$k1]['children'][$k2]['level_id'] = $val2['level_id'];
                $returnArr[$k1]['children'][$k2]['father_id'] = $val2['father_id'];

                $map3['father_id'] = $val2['group_id'];
                $map3['level_id'] = 3;
                $result3 = DB::query("select * from data_group where father_id = {$map3['father_id']} and level_id={$map3['level_id']}");

                foreach($result3 as $k3=>$val3){ //for3
                    $returnArr[$k1]['children'][$k2]['children'][$k3]['value'] = $val3['group_id'];
                    $returnArr[$k1]['children'][$k2]['children'][$k3]['name'] = $val3['group_name'];
                    $returnArr[$k1]['children'][$k2]['children'][$k3]['level_id'] = $val3['level_id'];
                    $returnArr[$k1]['children'][$k2]['children'][$k3]['father_id'] = $val3['father_id'];

                    $map4['father_id'] = $val3['group_id'];
                    $map4['level_id'] = 4;
                    $result4 = DB::query("select * from data_group where father_id = {$map4['father_id']} and level_id={$map4['level_id']}");

                    foreach($result4 as $k4=>$val4){ //for4
                        $returnArr[$k1]['children'][$k2]['children'][$k3]['children'][$k4]['value'] = $val4['group_id'];
                        $returnArr[$k1]['children'][$k2]['children'][$k3]['children'][$k4]['name'] = $val4['group_name'];
                        $returnArr[$k1]['children'][$k2]['children'][$k3]['children'][$k4]['level_id'] = $val4['level_id'];
                        $returnArr[$k1]['children'][$k2]['children'][$k3]['children'][$k4]['father_id'] = $val4['father_id'];

                        $map5['father_id'] = $val4['group_id'];
                        $map5['level_id'] = 5;
                        $result5 = DB::query("select * from data_group where father_id = {$map5['father_id']} and level_id={$map5['level_id']}");

                        foreach($result5 as $k5=>$val5){ //for5
                            $returnArr[$k1]['children'][$k2]['children'][$k3]['children'][$k4]['children'][$k5]['value'] = $val5['group_id'];
                            $returnArr[$k1]['children'][$k2]['children'][$k3]['children'][$k4]['children'][$k5]['name'] = $val5['group_name'];
                            $returnArr[$k1]['children'][$k2]['children'][$k3]['children'][$k4]['children'][$k5]['level_id'] = $val5['level_id'];
                            $returnArr[$k1]['children'][$k2]['children'][$k3]['children'][$k4]['children'][$k5]['father_id'] = $val5['father_id'];

                        }
                    }
                }
            }

        }

        return $returnArr;
    }
}