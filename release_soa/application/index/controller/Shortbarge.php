<?php


namespace app\index\controller;


use app\index\model\shortbarge\ShortbargeGoods;
use think\Request;

class Shortbarge extends Base
{

    public function getNumber(){
     $params=$this->input();

     $despatch_model=new \app\index\model\despatch\Despatch();
     $shortBarge_model=new \app\index\model\shortbarge\Shortbarge();

     if($params['abnormal_type']==1)
     {
        $data= $shortBarge_model->getNumber($params);
     }else
     {
       $data=  $despatch_model->getNumber($params);
     }

$this->outPut($data);
    }

    public function getShortBargeGoods(){
        $params=$this->input();
        $goods_model=new ShortbargeGoods();
        $res=$goods_model->getShortBargeGoods($params);
        $this->outPut($res);

    }

 public function getCount(){

     $short_model=new \app\index\model\shortbarge\Shortbarge();
     $res=$short_model->getCount();
     $this->outPut($res);
 }

    public  function getShortBarge(){
        $params=$this->input();
        $short_model=new \app\index\model\shortbarge\Shortbarge();
        $res=$short_model->getShortBarge($params);
        $this->outPut($res);
    }


public function addShortBarge(){
$params=$this->input();

$short_model=new \app\index\model\shortbarge\Shortbarge();

$res=$short_model->addShortBarge($params);
$this->outPut($res);

}


}