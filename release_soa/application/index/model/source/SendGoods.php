<?php

namespace app\index\model\source;
use app\index\model\Base;
use http\Params;
use think\Model;
use app\common\help\Help;
use app\index\service\PublicService;
use think\config;
use think\Db;
class SendGoods extends Base {
    //protected $connection = ['database' => 'erp'];
    protected $table = 'send_goods';
    private $_public_service;

    public function initialize()
    {
    	$this->_public_service = new PublicService();
    	parent::initialize();
    
    }

    public function parseSendGoods($params)
    {
        $where['project_id'] = $params['project_id'];
        $where['province_id'] = $params['send_province_id'];
        $where['city_id'] = $params['send_city_id'];
        $where['area_id'] = $params['send_area_id'];
        $where['send_location_id'] = $params['send_location_id'];
        $where['send_goods_company'] = $params['send_goods_company'];
        $where['send_goods_address'] = $params['send_address'];
        $where['send_goods_name'] = $params['send_name'];
        $where['send_goods_cellphone'] = $params['send_cellphone'];
        $res = $this->where($where)->find();
        if ($res)
            return ['send_goods_id' => $res->send_goods_id];
        else{
            $where['status']=1;
            $where['create_user_id']=$params['user_id'];
            $where['update_user_id']=$params['user_id'];
            $where['create_time']=time();
            $where['update_time']=time();
            $where['remark']="无";


            return ['send_goods_id' => $this->pubInsert($where)];
        }

    }

    public function getSendgoodsname($params){
        if(isset($params['keyword']))
        {
            $data=$this->whereLike('send_goods_company','%'.$params['keyword'].'%')->select();
            return $data;
        }

    }

    /**
     * 添加客户
     * 胡
     */
    public function addSendGoods($params){
    	$t = time();

    	$data['project_id'] = $params['project_id'];
    	$data['province_id'] = $params['province_id'];
    	$data['area_id'] = $params['area_id'];
    	$data['city_id'] = $params['city_id'];
    	$data['send_location_id'] = $params['send_location_id'];
    	
    	if(isset($params['send_goods_address'])){
    		$data['send_goods_address'] = $params['send_goods_address'];
    	}

    	if(isset($params['send_goods_company'])){
    		$data['send_goods_company'] = $params['send_goods_company'];
    	}    	 
    
    	if(isset($params['send_goods_name'])){
    		$data['send_goods_name'] = $params['send_goods_name'];
    		 
    	}
    
    	if(isset($params['send_goods_cellphone'])){
    		$data['send_goods_cellphone'] = $params['send_goods_cellphone'];    		 
    	}
    	if(isset($params['remark'])){
    		$data['remark'] = $params['remark'];
    		 
    	}
    
    
    
    	$data['create_time'] = $t;
    	$data['create_user_id'] = $params['user_id'];
    	$data['update_time'] = $t;
    	$data['update_user_id'] = $params['user_id'];
    	$data['status'] = $params['status'];
    
    
    	Db::startTrans();
    	try{
    		$pk_id = Db::name('send_goods')->insertGetId($data);
    
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
    
    	return $result;
    }
    
    /**
     * 获取客户
     * 胡
     */
    public function getSendGoods($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    	$data = "1=1 ";
    	if(is_numeric($params['send_goods_id'])){
    		$data.= " and send_goods.send_goods_id = ".$params['send_goods_id'];
    	} 
    	if(is_numeric($params['project_id'])){
    		$data.= " and send_goods.project_id = ".$params['project_id'];
    	}
    	if(is_numeric($params['province_id'])){
    		$data.= " and send_goods.province_id = ".$params['province_id'];
    	}
    	if(is_numeric($params['city_id'])){
    		$data.= " and send_goods.city_id = ".$params['city_id'];
    	}
    	if(is_numeric($params['area_id'])){
    		$data.= " and send_goods.area_id = ".$params['area_id'];
    	}
    	if(is_numeric($params['status'])){
    		$data.= " and send_goods.status = ".$params['status'];
    	}
    	

    	if(!empty($params['send_goods_name'])){
    		$data.= ' and send_goods.send_goods_name like "%'.$params['send_goods_name'].'%"';
    	}
    	if(!empty($params['send_goods_company'])){
    		$data.= ' and send_goods.send_goods_company like "%'.$params['send_goods_company'].'%"';
    	}    
    	
    	if(!empty($params['send_goods_cellphone'])){
    		$data.= ' and send_goods.send_goods_cellphone like "%'.$params['send_goods_cellphone'].'%"';
    	}

    
    	if($is_count==true){
    		$result = $this->table("send_goods")->where($data)->count();
    	}else {
    		if ($is_page == true) {
    			$result = $this->table("send_goods")->
    			join("project",'project.project_id = send_goods.project_id')->
    			where($data)->limit($page, $page_size)->order('send_goods.create_time desc')->
    			field(['send_goods.*','project.project_name','send_goods.send_goods_company',
    					"(select city_name  from city where city.city_id= send_goods.send_location_id)"=> 'send_location_name',
    					"(select city_name  from city where city.city_id= send_goods.province_id)"=> 'province_name',    					 						
    					"(select city_name  from city where city.city_id= send_goods.city_id)"=> 'city_name',
						"(select city_name  from city where city.city_id= send_goods.area_id)"=> 'area_name',
    					"(select nickname  from user where user.user_id = send_goods.create_user_id)"=> 'create_user_name',
    					"(select nickname  from user where user.user_id = send_goods.update_user_id)"=> 'update_user_name',
    					'send_goods.update_time', 'send_goods.create_time', "send_goods.status",
    			])->select();
    			 
    			 
    		}else{
    			$result = $this->table("send_goods")->alias('send_goods')->
    			join("project",'project.project_id = send_goods.project_id')->
    
    			where($data)->order('send_goods.create_time desc')->
    			field(['send_goods.*','project.project_name','send_goods.send_goods_company',
    					"(select city_name  from city where city.city_id= send_goods.send_location_id)"=> 'send_location_name',
    					"(select city_name  from city where city.city_id= send_goods.province_id)"=> 'province_name',    					 						
    					"(select city_name  from city where city.city_id= send_goods.city_id)"=> 'city_name',
						"(select city_name  from city where city.city_id= send_goods.area_id)"=> 'area_name',
    					"(select nickname  from user where user.user_id = send_goods.create_user_id)"=> 'create_user_name',
    					"(select nickname  from user where user.user_id = send_goods.update_user_id)"=> 'update_user_name',
    					'send_goods.update_time', 'send_goods.create_time', "send_goods.status",
    			])->select();
    		}
    	}
    
    	return $result;
    
    }
    
    /**
     * 修改发货方
     */
    public function updateSendGoodsBySendGoodsId($params){
    
    	$t = time();
    	 
        $data['project_id'] = $params['project_id'];
    	$data['province_id'] = $params['province_id'];
    	$data['area_id'] = $params['area_id'];
    	$data['city_id'] = $params['city_id'];
    	$data['send_location_id'] = $params['send_location_id'];
    	if(isset($params['send_goods_address'])){
    		$data['send_goods_address'] = $params['send_goods_address'];
    	}
    	 
    
    	if(isset($params['send_goods_name'])){
    		$data['send_goods_name'] = $params['send_goods_name'];
    		 
    	}
    
    	if(isset($params['send_goods_cellphone'])){
    		$data['send_goods_cellphone'] = $params['send_goods_cellphone'];
    		 
    	}
    	if(isset($params['send_goods_company'])){
    		$data['send_goods_company'] = $params['send_goods_company'];
    		 
    	}
    	if(isset($params['remark'])){
    		$data['remark'] = $params['remark'];
    		 
    	}


        $data['status'] = $params['status'];
    	$data['update_user_id'] = $params['user_id'];
    	$data['update_time'] = $t;
    
    
    
    	$source_price=[];
    	Db::startTrans();
    	try{
    		Db::name('send_goods')->where("send_goods_id = ".$params['send_goods_id'])->update($data);
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    
    	}
    	return $result;
    }
    

}