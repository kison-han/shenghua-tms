<?php

namespace app\index\model\source;
use think\Model;
use app\common\help\Help;
use app\index\service\PublicService;
use think\config;
use think\Db;
class CustomerSupplier extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'customer_supplier';

    /**
     * 获取承运商
     * 韩
     */
    public function getCustomerSupplier($params,$is_count=false,$is_page=false,$page=null,$page_size=20){

        $data = "1=1 ";

        if(isset($params['supplier_id'])){
            $data.= " and customer_supplier.supplier_id= ".$params['supplier_id'];
        }

        if(isset($params['supplier_name'])){
            $data.= " and customer_supplier.supplier_name like '%".$params['supplier_name']."%'";
        }

        if(isset($params['status'])){
            $data.= " and customer_supplier.status = ".$params['status'];
        }

        if($is_count==true){
            $result = $this->table("customer_supplier")->alias("customer_supplier")->where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("customer_supplier")->alias('customer_supplier')->
                where($data)->limit($page, $page_size)->order('create_time desc')->
                field(['customer_supplier.supplier_id', "customer_supplier.supplier_name", "customer_supplier.remark",
                    "(select nickname  from user where user.user_id = customer_supplier.create_user_id)"=> 'create_user_name',
                    "(select nickname  from user where user.user_id = customer_supplier.update_user_id)"=> 'update_user_name',
                    'customer_supplier.update_time', 'customer_supplier.create_time', "customer_supplier.status"])->select();
            }else{
                $result = $this->table("customer_supplier")->alias('customer_supplier')->
                where($data)->order('create_time desc')->
                field(['customer_supplier.supplier_id', "customer_supplier.supplier_name", "customer_supplier.remark",
                    "(select nickname  from user where user.user_id = customer_supplier.create_user_id)"=> 'create_user_name',
                    "(select nickname  from user where user.user_id = customer_supplier.update_user_id)"=> 'update_user_name',
                    'customer_supplier.update_time', 'customer_supplier.create_time', "customer_supplier.status"])->select();
            }

        }

        return $result;

    }

    /**
     * 添加承运商
     * 韩
     */
    public function addCustomerSupplier($params){

        $t = time();

        $data['supplier_name'] = $params['supplier_name'];

        if(isset($params['remark'])){
            $data['remark'] = $params['remark'];
        }

        $data['create_time'] = $t;
        $data['update_time'] = $t;
        $data['create_user_id'] = $params['user_id'];
        $data['update_user_id'] = $params['user_id'];
        $data['status'] = $params['status'];

        Db::startTrans();
        try{
            $pk_id = Db::name('customer_supplier')->insertGetId($data);

            $result = 1;
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
            //\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            //exit();

        }

        return $result;
    }


    /**
     * 修改承运商
     * 韩
     */
    public function updateCustomerSupplierByCustomerSupplierId($params){

        $t = time();

        if(!empty($params['supplier_name'])){
            $data['supplier_name'] = $params['supplier_name'];
        }

        if(isset($params['remark'])){
            $data['remark'] = $params['remark'];

        }

        $data['update_user_id'] = $params['user_id'];
        $data['update_time'] = $t;

        Db::startTrans();
        try{
            Db::name('customer_supplier')->where("supplier_id = ".$params['supplier_id'])->update($data);

            $result = 1;
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();

        }

        return $result;
    }

}