<?php

namespace app\index\controller;

use app\common\help\Help;
use think\Request;

class WmsWarehouse extends Base
{
    /**
     *  圣华
     *  仓储管理
     */
    public function index(){

		return $this->fetch('index');

    }

    /**
     *  圣华
     *  仓库显示页面
     */
    public function showWarehouse(){

        return $this->fetch('warehouse_manage');

    }

    /**
     *  圣华
     *  获取仓库动态表格数据
     */
    public function getWarehouseData(){

        $params = Request::instance()->param();

        $data=[
            'page'=>$params['page'],
            'page_size'=>$params['limit'],

        ];

        $data_all = array_merge($params,$data);

        $result = $this->callSoaErp('post','/wmswarehouse/getWareHouse',$data_all);

        $new = [
            "code"=> 200,

            "total"=>$result['data']['count'],
            "data"=>$result['data']['list']

        ];
        return $new;
    }

    /**
     *  圣华
     *  仓库新增页面
     */
    public function showWarehouseAdd(){

        //获取公司信息
        $data = [
            'status'=>1
        ];
        $company_result = $this->callSoaErp('post', '/system/getCompany',$data);
        $this->assign("companyResult",$company_result['data']);

        //生成对账编号
        $warehouseNumber = Help::getWmsWarehouseCode();

        //获取省市区信息
        $city = Help::getCity(0, 1);
        $this->assign('shengResult',$city);

        $data2 = [
            'ware_id'=>input('ware_id')
        ];
        //获取所有仓库
        $warehouse = $this->callSoaErp('post','/warehouse/getWarehouse',$data2);

        //获取所有客户信息
        $user_info = $this->callSoaErp('post','/user/getUser');
        $this->assign("userResult", $user_info['data']);
//        dump($user_info['data']);exit;

        $this->assign('warehouseNumber',$warehouseNumber);
        return $this->fetch('warehouse_add');

    }

    /**
     *  圣华
     *  仓库修改页面
     */
    public function showWarehouseEdit(){

        $data2 = [
            'ware_id'=>input('ware_id')
        ];
        //获取所有仓库
        $warehouse = $this->callSoaErp('post','/wmswarehouse/getWarehouse',$data2);
//        var_dump($warehouse);exit;

        //获取省市区信息
        $city = Help::getCity(0, 1);
        $this->assign('shengResult',$city);

        if(is_numeric(input('ware_id'))) {
            //通过拿到的省再去拿市数据
            $cityResult = Help::getCity($warehouse['data'][0]['ware_province_id'],2);
            $this->assign('cityResult', $cityResult);
            //通过拿到的市再去区
            $areaResult = Help::getCity($warehouse['data'][0]['ware_city_id'],3);
            $this->assign('areaResult', $areaResult);

//            dump($warehouse['data'][0]);exit;
            $this->assign("bindResult", $warehouse['data'][0]);
        }

        return $this->fetch('warehouse_edit');
    }

    /**
     * 圣华
     * 仓库新增AJAX / 修改AJAX
     */
    public function addwarehouseAjax(Request $request)
    {

        $data = $request->param();

        if(is_numeric($data['ware_id'])){
            //修改仓库
            $data['user_id'] = session("user_id");
            $result = $this->callSoaErp('post', '/warehouse/updateWarehouseByWarehouseId',$data);
            return $result;
        }else {
            //新增仓库
            $data['user_id'] = session("user_id");
            $result = $this->callSoaErp('post', '/warehouse/addWarehouse', $data);
            return $result; //['code' => '400', 'msg' => $data];
        }

    }

    /**
     *  圣华
     *  仓库区域显示页面
     */
    public function showRegion(){

        //获取所有仓库
        $warehouse = $this->callSoaErp('post','/wmswarehouse/getRegion');
        $this->assign("warehouseResult", $warehouse['data']);

        //获取所有客户信息
        $user_info = $this->callSoaErp('post','/user/getUser');
        $this->assign("userResult", $user_info['data']);

        return $this->fetch('region_manage');

    }

    /**
     *  圣华
     *  获取仓库动态表格数据
     */
    public function getRegionData(){

        $params = Request::instance()->param();

        $data=[
            'page'=>$params['page'],
            'page_size'=>$params['limit'],

        ];

        $data_all = array_merge($params,$data);

        $result = $this->callSoaErp('post','/wmswarehouse/getRegion',$data_all);

        $new = [
            "code"=> 200,

            "total"=>$result['data']['count'],
            "data"=>$result['data']['list']

        ];
        return $new;
    }

    /**
     *  圣华
     *  仓库区域新增页面
     */
    public function showRegionAdd(){

        //生成对账编号
        $warehouseNumber = Help::getRegionCode();

        //获取省市区信息
        $city = Help::getCity(0, 1);
        $this->assign('shengResult',$city);

        $data2 = [
            'region_id'=>input('region_id')
        ];
        //获取所有仓库
        $warehouse = $this->callSoaErp('post','/wmswarehouse/getRegion',$data2);
        $this->assign("warehouseResult", $warehouse['data']);

        //获取所有客户信息
        $user_info = $this->callSoaErp('post','/user/getUser');
        $this->assign("userResult", $user_info['data']);
//        dump($user_info['data']);exit;

        $user_id = session("user")['user_id'];
        $user_phone = session("user")['phone'];
        $this->assign('user_id',$user_id);
        $this->assign('user_phone',$user_phone);

        $this->assign('warehouseNumber',$warehouseNumber);
        return $this->fetch('region_add');

    }

    /**
     *  圣华
     *  仓库区域修改页面
     */
    public function showRegionEdit(){

        $data2 = [
            'region_id'=>input('region_id')
        ];
        //获取所有仓库区域
        $region = $this->callSoaErp('post','/wmswarehouse/getRegion',$data2);
//        var_dump($region);exit;

        //获取所有仓库
        $warehouse = $this->callSoaErp('post','/wmswarehouse/getRegion',[]);
        $this->assign("warehouseResult", $warehouse['data']);

        //获取省市区信息
        $city = Help::getCity(0, 1);
        $this->assign('shengResult',$city);

        //获取所有客户信息
        $user_info = $this->callSoaErp('post','/user/getUser');
        $this->assign("userResult", $user_info['data']);

        if(is_numeric(input('region_id'))) {
            //通过拿到的省再去拿市数据
            $cityResult = Help::getCity($region['data'][0]['region_province_id'],2);
            $this->assign('cityResult', $cityResult);
            //通过拿到的市再去区
            $areaResult = Help::getCity($region['data'][0]['region_city_id'],3);
            $this->assign('areaResult', $areaResult);

//            dump($warehouse['data'][0]);exit;
            $this->assign("bindResult", $region['data'][0]);
        }

        return $this->fetch('region_edit');
    }

    /**
     *  圣华
     *  库区显示页面
     */
    public function showReservoirarea(){

        return $this->fetch('reservoirarea_manage');

    }
}