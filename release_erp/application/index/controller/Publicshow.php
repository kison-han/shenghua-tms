<?php
namespace app\index\controller;
use app\common\help\Help;
use app\common\help\Contents;

use app\index\controller\Base;

use \think\File;
use think\Session;
use think\Request;
use think\config;
use think\Controller;
class Publicshow extends Controller
{
	private $_soaerpConfig;
	private $_soaerpUrl;
    public function _initialize()
    {
    
    	$this->_soaerpConfig= config('soaerp');
    	
    	$this->_soaerpUrl = $this->_soaerpConfig['ip'].':'.$this->_soaerpConfig['port'];
    	parent::_initialize();
    }


    /**
     * 调取报价账单AJAX
     */
    public function getPriceBillAjax(){
    
    	$bill_template_id = input("bill_template_id");//账单ID
    	$company_order_number = input('company_order_number');//公司订单
    	$company_order_id = input("company_order_id");
    	if(!empty($company_order_id)){
    		$company_order_params['company_order_id'] = $company_order_id;
    		$company_order_result  = $this->callSoaErp('post','/branchcompany/getCompanyOrder',$company_order_params);
    
    		$company_order_number = $company_order_result['data'][0]['company_order_number'];
    	}
    	$receivable_info_id = input('receivable_info_id');//收款ID多个逗号隔开
    	$default_bill_template_id = input('default_bill_template_id',0);//是否默认 1为是
    	$bill_template = [
    			'bill_template_id'=>$bill_template_id,
    			'company_order_number'=>$company_order_number,
    			'receivable_info_id'=>$receivable_info_id,
    			'default_bill_template_id'=>$default_bill_template_id,
    			'now_user_id'=>input('now_user_id')
    	];
    	 
    	$sale_bill_result = $this->callSoaErp('post','/branchcompany/getPriceBill',$bill_template);
    
    	return $sale_bill_result;
    
    }
    
    /**
     * 读取soaerp方法
     */
    public function callSoaErp($method,$function,$data=[]){
    
    	$data['appKey'] = 'nexus';
    	$data['appSecret']='nexusIt';

    	$data['now_user_id']  =$data['now_user_id'];

    
    
    	$result = Help::http($method,$this->_soaerpUrl.$function,$data);
    	
    	//		dump($result);exit;
    	$result = json_decode($result,true);
    	//error_log(print_r($result,1));
    	//dump($result);
    	if($result['code']==200){
    	
    		$result = ['code' => '200', 'msg' => 'success','data'=>$result['data'],'count'=>$result['data_count']];
    
    	}else{
    		//$this->outPutError($result);
    		$result = ['code' => '400', 'msg' => $result['msg']];
    	}
    	return $result;
    }
   	public function pdf(){
		$bill_template_id = input('bill_template_id');
		$company_order_id = input('company_order_id');
		$company_order_number = input('company_order_number');
		$receivable_info_id = input('receivable_info_id');
		$finance_approve_id = input('finance_approve_id');
		$default_bill_template_id = input('default_bill_template_id');
		$now_user_id = input('now_user_id');
		$this->assign('bill_template_id',$bill_template_id);
		$this->assign('company_order_id',$company_order_id);
		$this->assign('company_order_number',$company_order_number);
		$this->assign('receivable_info_id',$receivable_info_id);
		$this->assign('default_bill_template_id',$default_bill_template_id);
		$this->assign('now_user_id',$now_user_id);
		$this->assign('finance_approve_id',$finance_approve_id);
		return $this->fetch('pdf');
   	}
   	//销售收款
   	public function downloadPdf(){
   		$bill_template_id = input('bill_template_id');
   		$company_order_number = input('company_order_number');
   		$receivable_info_id = input('receivable_info_id');
   		$finance_approve_id = input('finance_approve_id');
   		$default_bill_template_id = input('default_bill_template_id');
   		$now_user_id = input('now_user_id');
   		$time = time();
   		//获取当前域名
   		$request = Request::instance();
   		$request->root();
   		// 获取当前域名
   		$domain = $request->domain();
   		
   		$path_name = ROOT_PATH.'public/static/uploads/pdf/'.$time.'.pdf';
   		

   		shell_exec("wkhtmltopdf  {$domain}/publicshow/pdf/now_user_id/{$now_user_id}/bill_template_id/{$bill_template_id}/company_order_number/{$company_order_number}/receivable_info_id/{$receivable_info_id}/default_bill_template_id/{$default_bill_template_id}/finance_approve_id/{$finance_approve_id} {$path_name}");
   		
   		//echo "wkhtmltopdf  {$domain}/publicshow/pdf/now_user_id/{$now_user_id}/bill_template_id/{$bill_template_id}/company_order_id/{$company_order_id}/receivable_info_id/{$receivable_info_id}/default_bill_template_id/{$default_bill_template_id} {$path_name}";
   		
   		return '/static/uploads/pdf/'.$time.'.pdf';
   	}
   	//销售收款打印 郭丹娜版本
   	public function downloadPdfNa(){
   		$bill_template_id = input('bill_template_id');
   		$company_order_number = input('company_order_number');

   		$default_bill_template_id = input('default_bill_template_id');
   		$now_user_id = input('now_user_id');
   		$time = time();
   		//获取当前域名
   		$request = Request::instance();
   		$request->root();
   		// 获取当前域名
   		$domain = $request->domain();
   		 
   		$path_name = ROOT_PATH.'public/static/uploads/pdf/'.$time.'.pdf';

   	
   		shell_exec("wkhtmltopdf  {$domain}/publicshow/pdfNa/now_user_id/{$now_user_id}/bill_template_id/{$bill_template_id}/company_order_number/{$company_order_number}/receivable_info_id/{$receivable_info_id}/default_bill_template_id/{$default_bill_template_id}/finance_approve_id/{$finance_approve_id} {$path_name}");
   		
   		//echo "wkhtmltopdf  {$domain}/publicshow/pdf/now_user_id/{$now_user_id}/bill_template_id/{$bill_template_id}/company_order_id/{$company_order_id}/receivable_info_id/{$receivable_info_id}/default_bill_template_id/{$default_bill_template_id} {$path_name}";
   		 
   		return '/static/uploads/pdf/'.$time.'.pdf';
   	}
   	public function pdfNa(){
   		$bill_template_id = input('bill_template_id');
   		$company_order_id = input('company_order_id');
   		$company_order_number = input('company_order_number');
   		$receivable_info_id = input('receivable_info_id');
   		$finance_approve_id = input('finance_approve_id');
   		$default_bill_template_id = input('default_bill_template_id');
   		$now_user_id = input('now_user_id');
   		$this->assign('bill_template_id',$bill_template_id);
   		$this->assign('company_order_id',$company_order_id);
   		$this->assign('company_order_number',$company_order_number);
   	
   		$this->assign('default_bill_template_id',$default_bill_template_id);
   		$this->assign('now_user_id',$now_user_id);
   	
   		return $this->fetch('pdfNa');
   	}
   
   	//报价PDF
   	public function pricePdf(){
   		$bill_template_id = input('bill_template_id');
   		$company_order_id = input('company_order_id');
   		$company_order_number = input('company_order_number');
   		$receivable_info_id = input('receivable_info_id');
   		$default_bill_template_id = input('default_bill_template_id');
   		$now_user_id = input('now_user_id');
   		$this->assign('bill_template_id',$bill_template_id);
   		$this->assign('company_order_id',$company_order_id);
   		$this->assign('receivable_info_id',$receivable_info_id);
   		$this->assign('default_bill_template_id',$default_bill_template_id);
   		$this->assign('now_user_id',$now_user_id);
   		return $this->fetch('pricepdf');
   	}
   	
   	//报价账单
   	public function downloadPricePdf(){
   		$bill_template_id = input('bill_template_id');
   		$company_order_id = input('company_order_id');
   		$receivable_info_id = input('receivable_info_id');
   		$default_bill_template_id = input('default_bill_template_id');
   		$now_user_id = input('now_user_id');
   		$time = time();
   		//获取当前域名
   		$request = Request::instance();
   		$request->root();
   		// 获取当前域名
   		$domain = $request->domain();
   		 
   		$path_name = ROOT_PATH.'public/static/uploads/pdf/'.$time.'.pdf';
   		shell_exec("wkhtmltopdf  {$domain}/publicshow/pricePdf/now_user_id/{$now_user_id}/bill_template_id/{$bill_template_id}/company_order_id/{$company_order_id}/receivable_info_id/{$receivable_info_id}/default_bill_template_id/{$default_bill_template_id} {$path_name}");
   		

   		//error_log(print_r("{$domain}/publicshow/pricePdf/now_user_id/{$now_user_id}/bill_template_id/{$bill_template_id}/company_order_id/{$company_order_id}/receivable_info_id/{$receivable_info_id}/default_bill_template_id/{$default_bill_template_id}",1));
   		//echo "wkhtmltopdf  {$domain}/publicshow/pdf/now_user_id/{$now_user_id}/bill_template_id/{$bill_template_id}/company_order_id/{$company_order_id}/receivable_info_id/{$receivable_info_id}/default_bill_template_id/{$default_bill_template_id} {$path_name}";
   		 
   		return '/static/uploads/pdf/'.$time.'.pdf';
   	}    	
   	/**
   	 * 验证码
   	 */
   	public function showYzm(){
	
        $captcha = new \think\captcha\Captcha();


   		return $captcha->entry();
   		
   	}
   	/**
   	 * 重置密码
   	 */
   	public function ChangePasswordAjax(){
   	
   		$data = [
   				'user_id'=>	session('user')['user_id'],
   	
   				'password'=>input('password'),
   				'choose_user_id'=>session('user')['user_id'],
				'true_password'=>input('password')
   		];
   		$this->callSoaErp('post', '/user/updateUserByUserId',$data);
   		return 1;
   	}
	
	/**
	 * 打印单子
	 */
	public function plan_print_invoice(){
		return $this->fetch('plan_print_invoice');
		
	}
	
	//获取城市SESSION
	public function getCity(){
		$params = Request::instance()->param();
		
		$cityParams['parent_id'] = $params['city_id'];
		$cityParams['status'] = 1;
		$result = $this->callSoaErp('post','/source/getCity',$cityParams);
		return $result['data'];
		
	}

    //获取市区县
    public function getCityAndArea(){
        $params = Request::instance()->param();

        $city_id = $params['city_id'];
        $level = $params['level'];

        return Help::getCityAndArea($city_id,$level);

    }
	
	//获取包装类型
	public function getGoodsPackagingType(){
		return 1;exit();
		$params = Request::instance()->param();
		
		$result = Contents::goodsPackagingType();
		for($i=0;$i<count($result);$i++){
			if($result[$i]['id'] = $id){
				return $result[$i]['goods_packaging_type_name'];
				exit();
			}
			
			
		}
		
	}

    //获取公司名称
    public function getCompany(){

        $params = Request::instance()->param();

        $ware_id = $params['ware_id'];

        return Help::getCompany($ware_id);

    }

    //获取会员数据
    public function getUser(){

        $params = Request::instance()->param();

        $ware_id = $params['user_id'];

        return Help::getUser($ware_id);
    }
	
    
    //获取海康出入口放行类型
    public function HikReleaseMode($releaseMode){
    	
    	$releaseModeData = Contents::HikReleaseMode();
    	
    	for($i=0;$i<count($releaseModeData);$i++){
    		if($releaseMode == $releaseModeData[$i]['id']){
    			
    			return  $releaseModeData[$i]['release_mode_name'];
    			exit();
    		}
    	}
    }
    
    /**
     * 获取海康车辆类型
     */
    public function HikVehicleType($vehicleType){
    	$vehicleTypeData = Contents::HikVehicleType();
    	 
    	for($i=0;$i<count($vehicleTypeData);$i++){
    		if($vehicleType == $vehicleTypeData[$i]['id']){
    			 
    			return  $vehicleTypeData[$i]['vehicleTypeName'];
    			exit();
    		}
    	}   	
    	
    }
   //发送邮件
   public function sendEmail(){
	   $params['status'] = 1;
	   $result = $this->callSoaErp('post','/system/getSendEmail',$params);
	   $result=$result['data'];
	   for($i=0;$i<count($result);$i++){
		   $k = Help::sendEmail($result[$i]);
		   
		   if($k==1){
			   
			   $updateSendEmail['send_email_id']=$result[$i]['send_email_id'];
			 
			   $this->callSoaErp('post','/system/updateSendEmail',$updateSendEmail);
		   }else{
			  
			   $sosParams['send_email'] = 'IT001@sun-hua.com';
			   $sosParams['send_name'] = '胡伊敏';
			   $sosParams['send_title'] = '邮件发送失败';
			   $sosParams['send_content'] = '无';
			   Help::sendEmail($sosParams);
		   }
	   }
	   
	   
	   echo 1;
   }
    
}
