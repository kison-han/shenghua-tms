<?php

namespace app\index\model\index;

use app\common\help\Contents;
use think\Model;
use think\config;
use app\common\help\Help;
use think\Db;

class IndexModel extends Model
{
    protected $autoWriteTimestamp = true;

    public function initialize()
    {
        parent::initialize();
    }

    public function updateStatusByTableId($params)
    {

        if (isset($params['status'])) {
            $field = isset($params['field']) ? $params['field'] : "status";
            $data[$field] = intval($params['status']);
        }


        $a = $params['table_name'];
        $b = $params['table_id_name'];
        $c = $params['table_id'];

        Db::startTrans();
        try {
            if (is_array($c)) {
                Db::name("$a")->whereIn($b, $c)->update($data);
            } else
                Db::name("$a")->where(" $b = '" . $c . "'")->update($data);

            $result = 1;
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();

        }

        return $result;


    }

}