<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:85:"/var/www/html/test_erp/public/../application/index/view/order/order_abnormal_add.html";i:1665309865;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






	 <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <title>添加异常运单</title>
<style type="text/css">
.xm-select-title {
height:22px!important;
min-height:22px!important;
}
.xm-select--suffix input{
height:22px;
}

.xm-select{
height:22px!important;
min-height:22px!important;
}
	.xm-select-parent input{
	min-height:22px!important;
	height:22px!important;
	
	}
	.layui-table-body table{
	margin-bottom: 0!important;
	}
</style>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    

    <div class="layui-body layui-body-bg">

        <!-- 内容主体区域 -->
        <div class="content_body">
            <div class="body-top">
                <div class='layui-form-item'>
                    <span class="layui-breadcrumb" lay-separator="-">
                        <a>首页</a>
                        <a>运单管理</a>
						<?php if($data[0][abnormal_id]): ?>
						  <a><cite>修改异常运单</cite></a>
	                     <?php else: ?>
                        <a><cite>新建异常运单</cite></a>
						<?php endif; ?>
                    </span>
                </div>
                <div class='layui-block all-search-bg'>
                    <form class="layui-form" method='get' onsubmit="return sub()" action='/order/showOrderAbnormalManage'>
                        
						<?php if($data[0][abnormal_id]): ?>
						<input type="hidden" name="abnormal_id" id="" value="<?php echo $data[0][abnormal_id]; ?>" />
						<?php endif; ?>
 						
						
						<div class="layui-row">
						
						<h3 style="color: #aaa;">基本信息</h3>
							<div class="layui-row">
								<div class="layui-form-item layui-col-md4">
									<label class="layui-form-label input-required">运单编号:</label>
									<div class="layui-input-block">
									  <select name="orders_id" lay-verify="required" id='department_id' lay-verify="required" xm-select="select1" xm-select-radio="" xm-select-direction="down" xm-select-search="" xm-select-search-type="dl">
									  								<option value="<?php echo $data[0][orders_id]; ?>" <?php if($data[0]): ?>selected="selected" <?php endif; ?>><?php echo $data[0][orders_number]; ?></option>
									  							</select>	</div>
								</div>
								
								<div class="layui-col-md4">
									
									
<!--
									<div class="layui-form-item">
										<label class="layui-form-label">发货方:</label>
										<div class="layui-input-block">
										    <input style="pointer-events: none;" type="text" id="send_name" name="send_name" maxlength="300" autocomplete="off" value="<?php echo $data[0][order_info][send_name]; ?>"  placeholder="发货方" class="layui-input">
										</div>
									</div>-->
								</div>
							</div>
						</div>
						<div>
						
						<table id="orders_info_table" lay-filter="orders_info_table" border=1>
						<thead><tr><th lay-data="{field:'orders_number', width:100}">运单编号</th><th lay-data="{field:'username', width:100}">发货方信息</th><th lay-data="{field:'username1', width:100}">货物信息</th><th lay-data="{field:'username2', width:200}">收货方信息</th><th lay-data="{field:'username3', width:200}">承运商</th></tr></thead>
						<tbody><td id="orders_number"><?php echo \think\Request::instance()->get('orders_number'); ?></td><td id="send_goods_company"></td><td id="goods_name"></td><td id="accept_goods_company"></td><td id="supplier_name"></td></tbody>
						</table>
						
						</div>
						
						
						
						
							<div class="layui-row">
								
							<h3 style="color: #aaa;">异常处理</h3>
							</div>
							<div class="layui-row">
								<div class="layui-col-md2">
									<div class="layui-form-item">
										
									<label class="layui-form-label ">异常类型:</label>
									<div class="layui-input-block">
									<select name="abnormal_type_id" class="layui-input" >
									        <option value="">请选择</option>
											<?php if(is_array($abnormalType) || $abnormalType instanceof \think\Collection || $abnormalType instanceof \think\Paginator): if( count($abnormalType)==0 ) : echo "" ;else: foreach($abnormalType as $key=>$vo3): ?>
											
											  
									          <optgroup label="<?php echo $vo3['name']; ?>">
												  <?php if(is_array($vo3['child']) || $vo3['child'] instanceof \think\Collection || $vo3['child'] instanceof \think\Paginator): if( count($vo3['child'])==0 ) : echo "" ;else: foreach($vo3['child'] as $key=>$vo4): ?>
												  
									            <option <?php if($data[0][abnormal_type_id]==$vo4['id']): ?>selected="selected"<?php endif; ?> value="<?php echo $vo4['id']; ?>"><?php echo $vo4['name']; ?></option>
												<?php endforeach; endif; else: echo "" ;endif; ?>
												
									          </optgroup>
											 <?php endforeach; endif; else: echo "" ;endif; ?> 
										
									        </select> 
									 
									 </div>
									</div>
								</div>
								
								<div class="layui-col-md2">
									<div class="layui-form-item">
										
									<label class="layui-form-label ">退货处理:</label>
									<div class="layui-input-block">
										<select  name="return_goods_type"  >
											  <option value="">请选择</option>
											<?php if(is_array($baseConfig['abnormal']['return_goods_type']) || $baseConfig['abnormal']['return_goods_type'] instanceof \think\Collection || $baseConfig['abnormal']['return_goods_type'] instanceof \think\Paginator): if( count($baseConfig['abnormal']['return_goods_type'])==0 ) : echo "" ;else: foreach($baseConfig['abnormal']['return_goods_type'] as $key3=>$vo3): ?>
											<option <?php if($data[0][return_goods_type]==$key3): ?>selected="selected"<?php endif; ?> value="<?php echo $key3; ?>"><?php echo $vo3; ?></option>
											<?php endforeach; endif; else: echo "" ;endif; ?>
										</select>
										
										
									  	</div>
									</div>
								</div>
								
								<div class="layui-col-md2">
									<div class="layui-form-item">
										
									<label class="layui-form-label ">理赔处理:</label>
									<div class="layui-input-block">
										<select  name="indemnity_type"  >
											  <option value="">请选择</option>
											<?php if(is_array($baseConfig['abnormal']['indemnity_type']) || $baseConfig['abnormal']['indemnity_type'] instanceof \think\Collection || $baseConfig['abnormal']['indemnity_type'] instanceof \think\Paginator): if( count($baseConfig['abnormal']['indemnity_type'])==0 ) : echo "" ;else: foreach($baseConfig['abnormal']['indemnity_type'] as $key3=>$vo3): ?>
											<option <?php if($data[0][indemnity_type]==$key3): ?>selected="selected"<?php endif; ?> value="<?php echo $key3; ?>"><?php echo $vo3; ?></option>
											<?php endforeach; endif; else: echo "" ;endif; ?>
										</select>
										
										
									  	</div>
									</div>
								</div>
								<div class="layui-col-md2">
								<div class="layui-form-item ">
									
									<label class="layui-form-label " style="width:99px">理赔金额(承运商):</label>
									<div class="layui-input-block" style="width:99px">
									    <input type="number" id=""  name="supplier_abnormal_money" maxlength="300" autocomplete="off" value="<?php echo $data[0][supplier_abnormal_money]; ?>"  placeholder="" class="layui-input">
									</div>
								</div>
								</div>
								
								<div class="layui-row">
								<div class="layui-col-md2">
									<div class="layui-form-item">
										
									<label class="layui-form-label ">产生货损:</label>
									<div class="layui-input-block">
										<select  name="damaged"  >
											 
											<?php if(is_array($baseConfig['abnormal']['damaged']) || $baseConfig['abnormal']['damaged'] instanceof \think\Collection || $baseConfig['abnormal']['damaged'] instanceof \think\Paginator): if( count($baseConfig['abnormal']['damaged'])==0 ) : echo "" ;else: foreach($baseConfig['abnormal']['damaged'] as $key3=>$vo3): ?>
											<option <?php if($data[0][damaged]==$key3): ?>selected="selected"<?php endif; ?> value="<?php echo $key3; ?>"><?php echo $vo3; ?></option>
											<?php endforeach; endif; else: echo "" ;endif; ?>
										</select>
										
										
									  	</div>
									</div>
								</div>
								
								<div class="layui-col-md2">
								<div class="layui-form-item ">
									
									<label class="layui-form-label ">货损数:</label>
									<div class="layui-input-block">
									    <input type="text" id="" placeholder="0"  name="abnormal_count" maxlength="300" autocomplete="off" value="<?php echo $data[0][abnormal_count]; ?>"  placeholder="" class="layui-input">
									</div>
								</div>
								</div>
								
								<div class="layui-col-md2">
									<div class="layui-form-item">
										
									<label class="layui-form-label ">责任方:</label>
									<div class="layui-input-block">
										<select  name="responsible_party"  >
											 <option value="">请选择</option>
											<?php if(is_array($baseConfig['abnormal']['responsible_party']) || $baseConfig['abnormal']['responsible_party'] instanceof \think\Collection || $baseConfig['abnormal']['responsible_party'] instanceof \think\Paginator): if( count($baseConfig['abnormal']['responsible_party'])==0 ) : echo "" ;else: foreach($baseConfig['abnormal']['responsible_party'] as $key3=>$vo3): ?>
											<option <?php if($data[0][responsible_party]==$key3): ?>selected="selected"<?php endif; ?> value="<?php echo $key3; ?>"><?php echo $vo3; ?></option>
											<?php endforeach; endif; else: echo "" ;endif; ?>
										</select>
										
										
									  	</div>
									</div>
								</div>
								
								
							
								
								<div class="layui-col-md2">
								<div class="layui-form-item ">
									
									<label class="layui-form-label" style="width:99px">理赔金额(客户):</label>
									<div class="layui-input-block" style="width:99px">
									    <input type="number" id=""  name="abnormal_money" maxlength="300" autocomplete="off" value="<?php echo $data[0][abnormal_money]; ?>"  placeholder="" class="layui-input">
									</div>
								</div>
								</div>
								</div>
								
								
								<div class="layui-form-item">
									<label class="layui-form-label">理赔事件描述:</label>
									
									
									<div class="layui-input-block">
								<textarea type="text" rows="6" style="height:100px;max-width:500px"   name="indemnity_event_description" cols="" class="layui-input"  ><?php echo $data[0][indemnity_event_description]; ?></textarea>
									</div>
								</div>
								<br>
								<div class="layui-form-item">
									<label class="layui-form-label">备注:</label>
									
									
									<div class="layui-input-block">
								<textarea type="text" rows="6" style="height:100px;max-width:500px"   name="remark" cols="" class="layui-input"  ><?php echo $data[0][remark]; ?></textarea>
									</div>
								</div>
								
							</div>
							<h3 style="color: #aaa;">异常图片</h3>
							<div class="layui-row">
								<div class="layui-col-md4">
									<div class="layui-form-item">
										<div class="layui-form-item" >
										    <label class="layui-form-label"></label>
										    <div class="layui-input-block bill-picture">
										        <button type="button" class="layui-btn" id="pic">
										            <i class="layui-icon">&#xe67c;</i>上传图片
										        </button>
										
										
										
										    </div>
										    <div id="u-img1" style="padding: 10px; margin-left: 100px">
										
										<?php if(is_array($data[0][upload]) || $data[0][upload] instanceof \think\Collection || $data[0][upload] instanceof \think\Paginator): if( count($data[0][upload])==0 ) : echo "" ;else: foreach($data[0][upload] as $key=>$vo3): ?>
										<div style="padding: 5px;width: 110px;float: left" class="img_list"><div><img src="<?php echo $vo3['url']; ?>" height="100" width="100" /><input type="hidden" value="<?php echo $vo3['url']; ?>" name="image[]" /></div><div><a class="layui-btn layui-btn-danger r-journey-img nav-edit" onclick="del(this)">删除</a></div></div>
										<?php endforeach; endif; else: echo "" ;endif; ?>
										    </div>
										</div>
										
									</div>
									
								</div>
								
								
							</div>
							
							<div class="layui-row">
                            <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">异常编号:</label>
                                    <div class="layui-input-block">
                                <input style="pointer-events: none;" type="text" name="abnormal_number" id="" value="<?php echo $data[0][abnormal_number]; ?>" class="layui-input"/>
                                    </div>
                                </div>
                            </div>   	
                            <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">操作人:</label>
                                    <div class="layui-input-block">
                                        <input style="pointer-events: none;" type="text" id="" name="" maxlength="300" autocomplete="off" value="<?php echo \think\Session::get('user.nickname'); ?>"  placeholder="" class="layui-input">
                                    </div>
                                </div>
                            </div>  <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">创建时间:</label>
                                    <div class="layui-input-block">
                                        <input type="text" id="" name="" maxlength="300" autocomplete="off" value="<?php echo date('Y-m-d',time()); ?>" style="pointer-events: none;"  placeholder="" class="layui-input">
                                    </div>
                                </div>
                            </div>  
							</div>
							
							
							
						
                        <!--<div class='layui-input-inline'>-->
                        <!--<input type="text" id="" name=""  placeholder="电话、联系人、操作人" class="layui-input">-->
                        <!--</div>-->
                        <div class="layui-row">
                            <div class="layui-col-md4 layui-col-md-offset4">
                                <div class='input-inline all-button-center pages-search-margin'>
                                    <button lay-submit lay-filter="formDemo" class="layui-btn nav-search" >提交</button>
									
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            

        </div>
    </div>

   
</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
<script src='/static/javascript/system/source.js'></script>
<script src='/static/javascript/public/formSelects-v4.js'></script>
<script>

	layui.use('element', function(){
		var element = layui.element;
	});
	var isShow = true;  //定义⼀个标志位
	$('.kit-side-fold').click(function(){
		//选择出所有的span，并判断是不是hidden
		$('.layui-nav-item span').each(function(){
			if($(this).is(':hidden')){
				$(this).show();
			}else{
				$(this).hide();
			}
		});
		//判断isshow的状态
		if(isShow){
			$('.layui-side.layui-bg-black').width(50); //设置宽度
			$('.kit-side-fold i').css('margin-right', '70%');  //修改图标的位置
			//将footer和body的宽度修改
			$('.layui-body').css('left', 60+'px');
			$('.layui-footer').css('left', 60+'px');
			//将⼆级导航栏隐藏
			$('dd span').each(function(){
				$(this).hide();
			});
			//修改标志位
			isShow =false;
		}else{
			$('.layui-side.layui-bg-black').width(200);
			$('.kit-side-fold i').css('margin-right', '10%');
			$('.layui-body').css('left', 200+'px');
			$('.layui-footer').css('left', 200+'px');
			$('dd span').each(function(){
				$(this).show();
			});
			isShow =true;
		}
	});

function sub(){
	var flag=false;
	var data=$("form").serializeArray();
var load_index=layui.layer.load();
layui.layer.load();
$.ajax({
	url:"/order/OrderAbnormalAdd",
	data:data,
	success:(e)=>{
		if(e.code==200)
		{
		window.parent.layer.closeAll();//window.location.href="/order/showOrderAbnormalManage"
		window.parent.global_table.reload()
		
		}
		
	
	},
	complete:function(){
			layui.layer.close(load_index)
			
		}
})
	
	
	
	return flag;
}

function update_base_info(orders_id){

$.ajax({
			url:"/order/showOrderManage?orders_id="+orders_id,
			success:function(e){
			var order_data=e.data[0];
			
			$("#send_goods_company").html(order_data.send_goods_company);
			var info=[];
			if(order_data.orders_goods_info.length>0)
			 order_data.orders_goods_info.map(function (i) { info.push(i.goods_name);  })
			
			$("#goods_name").html(info.join("/"))
			$("#send_goods_company").html(order_data.send_goods_company)
			$("#accept_goods_company").html(order_data.accept_goods_company)
			$("#orders_number").html(order_data.orders_number)
			
			
			$("#supplier_name").html(order_data.supplier_name)
			layui.table.init('orders_info_table', {
  height: 75,
  width:710
  ,limit: 1 
});


			}
			})
			
}

	var formSelects = layui.formSelects;

	var file_name_hidden = $("#file_name_hidden").val();

	

 layui.formSelects.on('select1', function(id, vals, val, isAdd, isDisabled){
            //id:           点击select的id
            //vals:         当前select已选中的值
            //val:          当前select点击的值
            //isAdd:        当前操作选中or取消
            //isDisabled:   当前选项是否是disabled
			
			$("#send_name").val(val.send_name);
			$("#transport_number").val(val.transport_number);
			
			
			update_base_info(val.value)
			
			
          //如果return false, 那么将取消本次操作
            /* if(val.value == 1){
                return false;
            } */
        });
layui.formSelects.config('select1', {
            searchUrl: '/order/orderAbnormalData', 
            beforeSearch: function(id, url, val){
				//console.log(this)
				
                if(!val){
                    console.log('内容为空, 不进行远程搜索')
                    return true;
                }
                return true;
            },
			success:function(e){
				console.log(layui.formSelects.value(e, 'name'))
				console.log(this)
			}
        });

    function openlayer(url,title){
        layer.open({
            type:2,
            title:title,
            content:url,
            area:["500px","600px"]
        })
    }


    var layer = layui.layer;

    var form = layui.form;
	
	form.on('select(replacement_prive_type)',function(data){
		var table_id = $(data.elem).parents("tr").children(".order_id").html();
		
		var table_id_name = "orders_id";
		var table_name = "orders";
		status=data.value;
		
		$.ajax({
		    type: "POST",
		    url: '/index/changeStatus',
		    data: {
		        table_id:table_id,
		        table_id_name:table_id_name,
		        table_name:table_name,
		        status:status,
				field:"replacement_prive_type"
		    },
		    success: function(data){
		        //console.log(data);
		        if(data.code!=200){
		            layer.msg(data.msg);
		            return false;
		        }else if(data.code==200) {
		            layer.msg('操作成功',{time:1,end : function(layero, index){
		
		                }
		            });
		        }
		    },
		    error:function(XMLHttpRequest, textStatus, errorThrown){
		        layer.msg('验证失败')
		    }
		});
		
		
		
		console.log(data)
	})
	
	
    form.on('switch(switchTest)', function(data){
        var table_id = $(data.elem).parents("tr").children(".order_id").html();

        var table_id_name = "order_id";
        var table_name = "order";
        if(this.checked == true){
            var status = 1;
        }else{
            var status = 0;
        }

        $.ajax({
            type: "POST",
            url: '/index/changeStatus',
            data: {
                table_id:table_id,
                table_id_name:table_id_name,
                table_name:table_name,
                status:status,
            },
            success: function(data){
                //console.log(data);
                if(data.code!=200){
                    layer.msg(data.msg);
                    return false;
                }else if(data.code==200) {
                    layer.msg('操作成功',{time:1,end : function(layero, index){

                        }
                    });
                }
            },
            error:function(XMLHttpRequest, textStatus, errorThrown){
                layer.msg('验证失败')
            }
        });
    });

    function del(obj){
        $(obj).parents("#u-img1").siblings(".bill-picture").find(".upload-hide").hide();
        $(obj).parents(".img_list").remove();
    }


 /*   var form =layui.form;
    form.on('submit(formDemo)',function (data) {
        $.ajax({
            url:"/order/addReceiptAjax",
            data:data.field,
            success:function (e) {
				parent.location.reload();
                window.location.href="/order/orderUploadView?orders_id="+data.field.orders_id;
            }
        })


console.log(data);
    }) */


var image_total=0;
    layui.use('upload', function(){
        var upload = layui.upload;

        //头部图片
        var uploadInst = upload.render({
            elem: '#pic' //绑定元素
            ,url: '/demo/uploadOtaFile', //上传接口
            multiple:true,
            drag:true,
			size:"100000"
            ,acceptMime:'image/*'
            ,accept:'file'
            ,before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
                layer.load(); //上传loading
            }
            ,done: function(res){
                //上传完毕回调
                // console.log(res);
                layer.closeAll('loading'); //关闭loading
                if(res.code == 200){
                    var h =
                        '<div style="padding: 5px;width: 110px;float: left" class="img_list"><div><img src="'+res.data+'" height="100" width="100" /><input type="hidden" value="'+res.data+'" name="image[]" /></div><div><a class="layui-btn layui-btn-danger r-journey-img nav-edit" onclick="del(this)">删除</a></div></div>';
                    $('#u-img1').append(h);
                    image_total++
                    $("#pic").siblings(".upload-hide").show();
                }
            } 
            ,error: function(res){
                //请求异常回调
                console.log(res);
                layer.closeAll('loading'); //关闭loading
            }
        });
    });
layui.table.init('orders_info_table', {
  height: 75,
  width:610
  ,limit: 1 
});
 
<?php if(is_numeric(\think\Request::instance()->get('orders_id'))): ?>

update_base_info(<?php echo \think\Request::instance()->get('orders_id'); ?>)
<?php endif; ?>

</script>
</body>
</html>


