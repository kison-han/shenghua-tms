<?php

namespace app\index\model\shipment;
use think\Model;
use app\common\help\Help;

use think\config;
use think\Db;
class ShipmentGoods extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'shipment_goods';

    public function initialize()
    {
    

    	parent::initialize();
    
    }


    /**
     * 获取发运
     * 胡
     */
    public function getShipmentGoods($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    
    
    	$data = "shipment_goods.status =1 ";

    	if(!empty($params['shipment_uuid'])){
    		$data.= " and shipment_goods.shipment_uuid= '".$params['shipment_uuid']."'";
    	}   

  	
        if($is_count==true){
            $result = $this->table("shipment_goods")->alias("shipment_goods")->where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("shipment_goods")->alias('shipment_goods')->
				join("goods","goods.goods_id = shipment_goods.goods_id")->


                where($data)->limit($page, $page_size)->order('shipment_goods.create_time desc')->
                field([
                		'shipment_goods.*','goods.goods_name'

		                ])->select();
            }else{
                $result = $this->table("shipment_goods")->alias('shipment_goods')->
				join("goods","goods.goods_id = shipment_goods.goods_id")->

                where($data)->order('create_time desc')->
                field([
					'shipment_goods.*','goods.goods_name'
                		
                ])->select();
            }
        }
    	 
 
    
    	return $result;
    
    }



}