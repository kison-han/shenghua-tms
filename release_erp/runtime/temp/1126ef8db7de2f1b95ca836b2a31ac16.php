<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:79:"/var/www/html/test_erp/public/../application/index/view/screen/orders_info.html";i:1664154293;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






    <title>账单管理</title>
    <link href="/static/css/source.css" rel="stylesheet">
    <link rel="stylesheet" href="/static/css/index.css">
</head>
<body class="layui-layout-body">
<!-- 内容主体区域 -->
<div style="padding: 0px 0px 0px 0px;">
    <div class="control-top">
        <div class="layui-row layui-col-space10">
<!--            <div style="position:absolute;right:10%;"><a href="/Screen/showOrdersInfo2" class="layui-btn">收货方 [到站] 城市数据</a></div>-->
            <div class="layui-col-xs6">
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-xs3">
                        <div class="bg-four">
                            <a href="javscript:void(0)">
                                <div style="color:white;">当年总计数</div>
                                <h4 id="month_customer_count" style="color:white;">16049</h4>
                            </a>
                        </div>
                    </div>
                    <div class="layui-col-xs3" >
                        <div class="bg-one">
                            <a href="javscript:void(0)">
                                <div style="color:white;">当年运单数</div>
                                <h4 id="today_customer_count" style="color:white;">7142</h4>
                            </a>
                        </div>
                    </div>
                    <div class="layui-col-xs3">
                        <div class="bg-two ">
                            <a href="javscript:void(0)">
                                <div style="color:white;">当年发运数</div>
                                <h4 id="today_team_product_count" style="color:white;">7089</h4>
                            </a>
                        </div>
                    </div>
                    <div class="layui-col-xs3">
                        <div class="bg-three ">
                            <a href="javscript:void(0)">
                                <div style="color:white;">当年短驳数</div>
                                <h4 id="today_receivable" style="color:white;">3318</h4>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--top-->
    <div class="content-bg">
        <div id="main" style="width:105%;height:250px;margin-top:0px;display:inline-block;"></div>

        <div class="layui-carousel" id="test1" lay-filter="test1">
            <div carousel-item="" style="width:105%;margin-top:-20px">
                <div id="main2" style="width:900px;height:260px;"></div>
                <div id="main3" style="width:900px;height:260px;"></div>
            </div>
        </div>




        <script type="text/javascript">

        </script>
    </div>

</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
<script src='/static/javascript/system/source.js'></script>
<script>


    layui.use(['layer','table','form','carousel'], function() {
        var table = layui.table //表格
            , layer = layui.layer //弹层
            , form = layui.form,
            carousel = layui.carousel

        //常规轮播
        carousel.render({
            elem: '#test1'
            ,width: '100%'
            ,height: '440px'
            ,interval: 3000
            ,anim: 'fade'
        });

        $('input[name="show_btn"]').bind('click', function () {
            var customer_bill_number = $(this).val();

            layer.open({
                id:'layer1',
                type:2,
                title:'对账详情',
                content:"/bill/showCustomerYesBillInfoManage?customer_bill_number="+customer_bill_number,
                area:["800px","600px"]
            })
        });

        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('main'));
        var myChart2 = echarts.init(document.getElementById('main2'));
        var myChart3 = echarts.init(document.getElementById('main3'));

        // 指定图表的配置项和数据
        var option = {
            title: {
                text: '综合数据'
            },
            tooltip: {},
            legend: {
                data: ['运单','发运','短驳']
            },
            xAxis: {
                data: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月']
            },
            yAxis: {},
            series: [
                {
                    name: '运单',
                    type: 'bar',
                    stack: '',
                    label: {
                        show: true,
                        position: 'insideBottom'
                    },
                    data: [1190, 972, 1482, 205, 572, 1353, 368, 1259, 0, 0, 0, 0]
                },{
                    name: '发运',
                    type: 'bar',
                    stack: '',
                    label: {
                        show: true,
                        position: 'inside'
                    },
                    data: [1207, 979, 1489, 78, 697, 1361, 278, 1013, 0, 0, 0, 0]
                },{
                    name: '短驳',
                    type: 'bar',
                    stack: '',
                    label: {
                        show: true,
                        position: 'inside'
                    },
                    data: [502, 532, 764, 9, 180, 650, 181, 589, 0, 0, 0, 0]
                }
            ]
        };

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);

        option2 = {
            title: {
                text: '客户运单数据',
                subtext: '2022年',
                left: 'center'
            },
            tooltip: {
                trigger: 'item'
            },
            legend: {
                orient: 'vertical',
                left: 'left'
            },
            series: [
                {
                    name: 'Access From',
                    type: 'pie',
                    radius: '50%',
                    label: {
                        formatter: '{b|{b}: }{c}  {per|{d}%}',
                        rich: {
                            a: {
                                color: '#6E7079',
                                lineHeight: 22,
                                align: 'center'
                            },
                            b: {
                                color: '#4C5058',
                                fontSize: 14,
                                fontWeight: 'bold',
                                lineHeight: 33
                            },
                            per: {
                                color: '#fff',
                                backgroundColor: '#4C5058',
                                padding: [3, 4],
                                borderRadius: 4
                            }
                        }
                    },
                    data: [
                        { value: 1129, name: '芳精香料' },
                        { value: 877, name: '三樱' },
                        { value: 689, name: '曼盛' },
                        { value: 548, name: '泰莱' },
                        { value: 520, name: '运宏' },
                        { value: 439, name: '好来喜' },
                        { value: 356, name: '贝德玛' },
                        { value: 352, name: '爱焙士' },
                        { value: 170, name: '北京西雅克' },
                        { value: 142, name: '鼎亦化工' },
                        { value: 1099, name: '其它' },

                    ],
                    emphasis: {
                        itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };
        myChart2.setOption(option2);

        option3 = {
            title: {
                text: '客户发运数据',
                subtext: '2022年',
                left: 'center'
            },
            tooltip: {
                trigger: 'item'
            },
            legend: {
                orient: 'vertical',
                left: 'left'
            },
            series: [
                {
                    name: 'Access From',
                    type: 'pie',
                    radius: '50%',
                    label: {
                        formatter: '{b|{b}: }{c}  {per|{d}%}',
                        rich: {
                            a: {
                                color: '#6E7079',
                                lineHeight: 22,
                                align: 'center'
                            },
                            b: {
                                color: '#4C5058',
                                fontSize: 14,
                                fontWeight: 'bold',
                                lineHeight: 33
                            },
                            per: {
                                color: '#fff',
                                backgroundColor: '#4C5058',
                                padding: [3, 4],
                                borderRadius: 4
                            }
                        }
                    },
                    data: [
                        { value: 555, name: '上海顺衡物流有限公司（顺丰）' },
                        { value: 389, name: '江苏满运软件科技有限公司' },
                        { value: 387, name: '上海智权国际物流有限公司' },
                        { value: 243, name: '上海诚载物流有限公司' },
                        { value: 221, name: '上海卓冠物流有限公司' },
                        { value: 217, name: '姜洪君' },
                        { value: 216, name: '上海硕慧物流有限公司' },
                        { value: 179, name: '上海永传物流有限公司' },
                        { value: 179, name: '沪D99681' },
                        { value: 177, name: '沪FE0657' },
                        { value: 3530, name: '其它' }
                    ],
                    emphasis: {
                        itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };
        myChart3.setOption(option3);

    });

</script>
</body>
</html>
<script src='/static/echarts/dist/echarts.js'></script>
