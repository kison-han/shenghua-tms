<?php
/**
 * Created by PhpStorm.
 * User: 胡
 * Date: 20190/02/19
 * Time: 16:40
 */

namespace app\index\controller;
use \Underscore\Types\Arrays;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
use app\common\help\Help;

class Project extends Base
{
	/**
	 *  圣华
	 *  客户管理显示页面
	 */
	public function showProjectManage(){
		//读取所有的地接信息
	
		$company_name = input("company_name");

		$status = input("status");
		$data = [
				'page'=>$this->page(),
				'page_size'=>$this->_page_size,
	
	
		];
		if(!empty($company_name)){
			$data['company_name'] = $company_name;
		}
		if(is_numeric($status)){
			$data['status'] = $status;
		}

		$result = $this->callSoaErp('post','/source/getCustomer',$data);

		$this->getPageParams($result);
		return $this->fetch('project_manage');
			
			
	}
	
	/**
	 * 客户管理显示新增页面
	 */
	public function showCustomerAdd(){
	
		$customer_id = input('customer_id');
		if(is_numeric($customer_id)){
			
			$data['customer_id'] = $customer_id;
			$result = $this->callSoaErp('post','/source/getCustomer',$data);
			$this->assign('result',$result['data'][0]);
			
		}
		
		
		
		return $this->fetch('customer_add');
	}
	
	/**
	 * 客户新增数据
	 */
	public function addCustomerAjax(){
		$data = Request::instance()->param();
		

		if(is_numeric($data['customer_id'])){
	
	
			$result = $this->callSoaErp('post', '/source/updateCustomerByCustomerId',$data);
		}else{
		
			$result = $this->callSoaErp('post', '/source/addCustomer',$data);
		}

		return   $result;//['code' => '400', 'msg' => $data];
	}
}