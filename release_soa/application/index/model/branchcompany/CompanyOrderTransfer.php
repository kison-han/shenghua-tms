<?php

namespace app\index\model\branchcompany;
use app\common\help\Help;
use think\Db;
use think\Exception;
use think\Model;

class CompanyOrderTransfer extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'company_order_transfer';
    public function initialize()
    {
    	parent::initialize();
    
    }



    public function updateCompanyOrderTransfer($params)
    {
        try{
            Db::startTrans();
            $this->where(['company_order_number' => $params['company_order_number']])->update(['status'=>0]);

            $params['flight'] = [];
            foreach ($params['transfer'] as $v)
            {
                $data = [];
                $data['temp_group'] = $v['temp_group'];
                $data['seat_group'] = trim($v['seat_group'],',');
                $data['airport'] = $v['airport'];
                $data['transfer_type'] = $v['transfer_type'];
                $data['flight_code'] = $v['flight_code'];
                $data['charge_num'] = $v['charge_num'];
                $data['bus_pax'] = $v['bus_pax'];
                $data['date'] = $v['date'];
                $data['time'] = $v['time'];
                $data['pickup_location'] = $v['pickup_location'];
                $data['team_product_number'] = $v['team_product_number'];
                $data['status'] = 1;

                if ($v['company_order_transfer_id'])
                {
                    $where = " company_order_transfer_id = ". $v['company_order_transfer_id'];
                    $this->where($where)->update($data);
                }
                else
                {
                    $data['company_order_number'] = $params['company_order_number'];

                    $this->insert($data);
                }

                if (in_array($data['transfer_type'], [1,2]))
                {
                    $temp_group = explode(',', $v['temp_group']);
                    foreach ($temp_group as $temp_group_v)
                    {
                        $data['company_order_customer_id'] = $temp_group_v;

                        $company_order_customer_info = $this->table('company_order_customer')->where(['company_order_customer_id' => $temp_group_v])->find();

                        $data['customer_id'] = $company_order_customer_info['customer_id'];
                        array_push($params['flight'], $data);
                        unset($data['company_order_customer_id']);
                        unset($data['customer_id']);
                    }
                }

            }
            $company_order_flight = new CompanyOrderFlight();
            $company_order_flight->addFlight($params);
            Db::commit();
            $result = 1;
        } catch (\Exception $e) {
            Db::rollback();
            $result = $e->getMessage();

            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }

        return $result;
    }

    public function getTransferByOrderNumber($company_order_number)
    {
        try
        {
            $where['company_order_number'] = $company_order_number;
            $where['status'] = 1;
            $result = $this->alias('company_order_transfer')->where($where)->select();
        }
        catch (Exception $e)
        {
            $result = $e->getMessage();
            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }

        return $result;
    }

}