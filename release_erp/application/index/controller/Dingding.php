<?php

namespace app\index\controller;

use think\Cookie;
use Underscore\Types\Arrays;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
use app\common\help\Contents;
class Dingding
{


	public $webhook;
	public function __construct()
	{
		$this->webhook = Contents::dingdingWebHook();
		
		
	}
	/**
	 * 获取数据
	 */
	public function test(){
	
		$data = Request::instance()->param();
	
		
// 		if(trim($data['text']['content'])=='胡伊敏'){
// 			$text ='胡伊敏最棒';
// 		}else if(trim($data['text']['content'])=='韩冰'){
// 			$text ='韩冰最low';
// 		}else if(trim($data['text']['content'])=='娄恬'){
// 			$text ='娄恬最美';
// 		}else if(trim($data['text']['content'])=='我们公司老板是谁'){
// 			$text ='施松标';
// 		}
		
		if(trim($data['text']['content'])=='开门禁'){
			
			//门禁点反控
	
	
				$dataDoor=[
						'controlType'=>2
				];
			
				$dataDoor['doorIndexCodes'] =array('08aaca7dcac7465b841c5e2affd045a5');
			
				$result = action('haikang/door_control',['postData' => $dataDoor]);
				

			
			
			
		}else if(trim($data['text']['content'])=='开大门'){
			$data['roadwaySyscode'] = '47b9fbb2d82444158b73338c5ab03050';
			$data['command'] = 1;
			
			$result = action('haikang/device_control',['postData' => $data]);
			
			
		}

// 		// text类型
// 		$textString1 = json_encode([
// 				'msgtype' => 'text',
// 				'text' => [
// 						"content" =>'圣华小慧:'.$text
// 				],
		
// 		]);
		

// 		$a = $this->request_by_curl($this->webhook, $textString1);
	
	}
	
	
	
	public function request_by_curl($remote_server, $post_string)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $remote_server);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json;charset=utf-8'));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// 不用开启curl证书验证
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$data = curl_exec($ch);
		//$info = curl_getinfo($ch);
		//var_dump($info);
		curl_close($ch);
		return $data;
	}
	public function curlPost($url = '', $postData = '', $options = array())
	{
		if (is_array($postData)) {
			$postData = http_build_query($postData);
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); //设置cURL允许执行的最长秒数
		if (!empty($options)) {
			curl_setopt_array($ch, $options);
		}
		//https请求 不验证证书和host
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}	
	
}

