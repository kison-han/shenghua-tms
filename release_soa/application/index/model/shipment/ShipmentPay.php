<?php

namespace app\index\model\shipment;
use think\Model;
use app\common\help\Help;

use think\config;
use think\Db;
class ShipmentPay extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'shipment_pay';

    public function initialize()
    {
    

    	parent::initialize();
    
    }


    /**
     * 获取发运支付
     * 胡
     */
    public function getShipmentPay($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    
    
    	$data = "shipment_pay.status =1 ";

    	if(!empty($params['shipment_uuid'])){
    		$data.= " and shipment_pay.shipment_uuid= '".$params['shipment_uuid']."'";
    	}   

  
        if($is_count==true){
            $result = $this->table("shipment_pay")->alias("shipment_pay")->where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("shipment_pay")->alias('shipment_pay')->
	


                where($data)->limit($page, $page_size)->order('shipment_pay.create_time desc')->
                field([
                		'shipment_pay.*'

		                ])->select();
            }else{
                $result = $this->table("shipment_pay")->alias('shipment_pay')->

                where($data)->order('shipment_pay.create_time desc')->
                field([
						'shipment_pay.*'
                		
                ])->select();
            }
        }
    	 
 
    
    	return $result;
    
    }



}