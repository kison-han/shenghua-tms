<?php


namespace app\index\controller;


use think\Model;
use app\index\model\source\City as citymodel;

class City extends Base
{

    public function getCity(){
		$citymodel=new citymodel();
		return json_encode($citymodel->getAllCity(),256);

    }

    /**
     * 通过市区获取省市区数据
     */
    public function areaGetProvinceAndcityId(){

        $params = $this->input();

        $citymodel=new citymodel();
        $cityResult = $citymodel->areaGetProvinceAndcityId($params);
        $this->outPut($cityResult);

    }

    public function a(){
    	$citymodel=new citymodel();
    	$cityResult = $citymodel->aa($params);    	
    }
    
    /**
     * 通过名称获取ID
     */
    public function getCityNameToCityId(){

        $params = $this->input();

        $citymodel=new citymodel();
        $cityidResult = $citymodel->getCity($params);
        $this->outPut($cityidResult);

    }

    /**
     * 通过名称获取省ID
     */
    public function getCityNameToParentCityId(){

        $params = $this->input();

        $citymodel=new citymodel();
        $cityidResult = $citymodel->getProvinceCity($params);
        $this->outPut($cityidResult);

    }

    /**
     * 通过省ID获取省市区数据
     */
    public function getCityAndArea(){

        $params = $this->input();

        $citymodel=new citymodel();
        $cityResult = $citymodel->getCityAndArea($params);
        $this->outPut($cityResult);
    }

}