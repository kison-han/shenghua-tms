<?php if (!defined('THINK_PATH')) exit(); /*a:5:{s:84:"/var/www/html/test_erp/public/../application/index/view/warehouse/warehouse_add.html";i:1655951489;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:64:"/var/www/html/test_erp/application/index/view/public/header.html";i:1658978091;s:77:"/var/www/html/test_erp/application/index/view/public/left_warehouse_menu.html";i:1654051705;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






    <title>
        <?php if(\think\Request::instance()->get('ware_id') ==  ''): ?>
        新增仓库
        <?php else: ?>
        修改仓库
        <?php endif; ?>

    </title>

</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">

    <div class="layui-header">
            <div class="layui-logo"></div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left">
        <li class="layui-nav-item">
            <a href="/turnoverbox/index">智能周转箱</a>
        </li>
        <li class="layui-nav-item">
            <a href="/">TMS</a>
        </li>
        <li class="layui-nav-item">
 
            <a href="/warehouse/index">WMS</a>
        </li>
        <li class="layui-nav-item">
            <a href="/wisdompark/index">智慧园区</a>

        </li>
        <li class="layui-nav-item">
            <a href="/Oa/index">OA</a>

        </li>   
         <li class="layui-nav-item">
            <a href="/Bms/index">BMS</a>

        </li>
        <li class="layui-nav-item">
            <a href="/equipment/equipmentManage">设备</a>

        </li>
    </ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item tips-system-message" style="cursor: pointer"><?php echo $language_tag['index_nav_system_message']; ?> <span class="system-message-quantity" style="color: red"> 1</span> </li>
      <li class="layui-nav-item">
        <a href="javascript:;">
          <!-- <img src="http://t.cn/RCzsdCq" class="layui-nav-img"> -->
          <?php echo \think\Session::get('user.nickname'); ?>
        </a>
        <dl class="layui-nav-child head-top">
          <dd><a href="/system/setUserInfo/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_basicDocument']; ?></a></dd>
          <dd><a href="/system/showChangePassword/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_resetPassword']; ?></a></dd>
        </dl>
      </li>
      <li class="layui-nav-item"><a href="/login/loginOut">退出登录</a></li>
    </ul>

    <div id="tips-system-message-js" style='display:none;'>
        <div  style="max-height:350px;width: 450px">
             <div style="color: #000;text-align: center;height: 10px;padding: 10px">你有 <font class="f-tips-system-message"></font> 条未读消息</div>
             <hr>
             <div class="tips-system-message-div" style="height:220px;overflow-y: auto">
           
                 <li style="color: #000; padding: 10px;cursor: pointer" data-href="" data-id="" class="aUrl" onclick="Aurl(this)" ></li>
                 <hr>
             
             </div>
            <div style="color: #000;text-align: center;height: 40px;padding-top: 15px"><a href="/reminderManagement/allInStationLetter"><?php echo $language_tag['index_nav_viewAll_messa']; ?></a></div>
        </div>
    </div>

    <!--  下列保存用户SESSION信息 -->
	
    <input type="hidden" id='now_url'  value="<?php echo $now_url; ?>" />
	<input type="hidden" id='after_url'  value="<?php echo $after_url; ?>" />
    <input type="hidden" id='user_company_id' value="<?php echo \think\Session::get('user.company_id'); ?>" />
	<!--  下列保存其他信息 -->
	<input type="hidden" id='http_referer' value="<?php echo $http_referer; ?>" />
	
	<script type='text/javascript'>
		$('.tips-system-message').on('click',function(){
			var html = $('#tips-system-message-js').html();
		    layer.tips(html, '.tips-system-message', {
		        tips: [3, '#fff'],
		        padding:'20',
		        tipsMore: false,
		        area: ['450px', 'auto'],
		        shade: [0.01, '#fff'],
		        shadeClose:true,
		        time:0
			
		    });
		
		});
		
		$(document).ready(function(){ 
			$(document).mousemove(function(e){ 
				
				if(e.pageX<=10){
					$('.layui-bg-black').show()
				}
				
				if(e.pageX>200){
					$('.layui-bg-black').hide()
				
				}
				if(e.pageY<=10){
					$('.layui-header').show()
				}
				
				if(e.pageY>40){
					$('.layui-header').hide()
				
				}			
			}); 
			
		}); 
	

	</script>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
                  <ul class="layui-nav layui-nav-tree" id="left-nav" lay-filter="test">
          <li class="layui-nav-item"><a href="/"><i class="iconfont img-homepage_fill"></i><em>控制面板</em></a></li>
        <li <?php if($controller_name == 'wms_warehouse'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>基础资料管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showwarehouse,showwarehouseadd,showwarehouseedit"))): ?> class="layui-this"<?php endif; ?>><a href="/wms_warehouse/showWarehouse">仓库管理</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showregion,showregionadd,showregionedit"))): ?> class="layui-this"<?php endif; ?>><a href="/wms_warehouse/showRegion">区域管理</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showreservoirarea,showreservoirareaadd,showreservoirareadit"))): ?> class="layui-this"<?php endif; ?>><a href="/wms_warehouse/showReservoirarea">库区管理</a></dd>

          </dl>
          </li>

        <li <?php if($controller_name == 'warehouse'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>仓库管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showwarehouse,showwarehouseadd"))): ?> class="layui-this"<?php endif; ?>><a href="/warehouse/showWarehouse">仓库</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showposition,showpositionadd"))): ?> class="layui-this"<?php endif; ?>><a href="/position/showPosition">货区</a></dd>
<!--              <dd <?php if(in_array(($function_name), explode(',',"showpetarticlemanage,showpetarticleadd"))): ?> class="layui-this"<?php endif; ?>><a href="/warehouse/">货位</a></dd>-->
          </dl>
        </li>

        <li <?php if($controller_name == 'wmsmonitor'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>监测管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showpetarticlemanage,showpetarticleadd"))): ?> class="layui-this"<?php endif; ?>><a href="/wmsmonitor/showTHPoint">温湿度点位</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showpetarticlemanage,showpetarticleadd"))): ?> class="layui-this"<?php endif; ?>><a href="/wmsmonitor/showTH">温湿度数据</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showpetarticlemanage,showpetarticleadd"))): ?> class="layui-this"<?php endif; ?>><a href="/wmsmonitor/showTHVisualization">温湿度数据可视化</a></dd>

          </dl>
        </li>    
          <!--<li <?php if($controller_name == 'otaarticle' or $controller_name == 'enquirty' or $controller_name == 'otasystem' or $controller_name == 'otaslide' or $controller_name == 'otaproduct' or $controller_name == 'otamember'): ?>-->
              <!--class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>-->
          <!--<a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-website"></i><em>官网管理</em></a>-->
          <!--<dl class="layui-nav-child">-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showotasystemmanage"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_system/showOtaSystemManage">网站设置</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showotamenumanage"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_system/showOtaMenuManage?status=1">菜单</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showarticletypemanage,showarticletypeadd,showarticletypeedit,showarticletypeinfo"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_article/showArticleTypeManage?status=1">文章分类</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showarticlemanage,showarticleadd,showarticleedit,showarticleinfo"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_article/showArticleManage?status=1">文章</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showslidemanage,showslideadd,showslideedit,showslideinfo"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_slide/showSlideManage?status=1">幻灯片</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showadvertmanage,showadvertadd,showadvertedit,showadvertinfo"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_slide/showAdvertManage?status=1">友情链接</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showadvertisingmanage"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_slide/showAdvertisingManage?status=1">广告位</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showcompanywebsitemanage"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_system/showCompanyWebsiteManage?status=1">域名管理</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"index,addenquirty,editenquirty"))): ?> class="layui-this" <?php endif; ?> ><a href="/enquirty/index">需求定制</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"lst,add,edit"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_member/lst?status=1">账号管理</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"types,addtype,edittype,gettypeinfo"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_product/types?status=1">旅游产品分类</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"productlists,addlist,editlist"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_product/productLists?status=1">旅游产品列表</a></dd>-->
              <!--&lt;!&ndash;<dd <?php if(in_array(($function_name), explode(',',"products,addlist,editlist"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_product/products?status=1">旅游产品</a></dd>&ndash;&gt;-->
          <!--</dl>-->

          <!--</li>-->
      </ul>
        </div>
    </div>

    <div class="layui-body">

        <!-- 内容主体区域 -->
        <div class="content_body">
            <div class='layui-form-item'>
		   		<span class="layui-breadcrumb" lay-separator="-">
					<a>首页</a>
					<a>仓库管理</a>
					<a>仓库</a>
					<a><cite>
                            <?php if(\think\Request::instance()->get('ware_id') ==  ''): ?>
                            新增仓库
                            <?php else: ?>
                            修改仓库
                            <?php endif; ?>
                        </cite>
                    </a>
				</span>
            </div>
            <br/>
            <br/>
            <form class="layui-form" onSubmit="return warehouseAdd()">
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-md4">
                        <div class="layui-form-item">
                            <label class="layui-form-label input-required">公司名称:</label>
                            <div class="layui-input-block">
                                <select name="company_id" id='company_id' >
                                    <?php if(is_array($companyResult) || $companyResult instanceof \think\Collection || $companyResult instanceof \think\Paginator): if( count($companyResult)==0 ) : echo "" ;else: foreach($companyResult as $key=>$vo): ?>
                                    <option  value="<?php echo $vo['company_id']; ?>" <?php if($vo['company_id'] == $bindResult['company_id']): ?>selected='selected'<?php endif; ?> ><?php echo $vo['company_name']; ?></option>
                                    <?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-md4">
                        <div class="layui-form-item">
                            <label class="layui-form-label input-required">仓库名称:</label>
                            <div class="layui-input-block">
                                <input name="ware_name" id="ware_name" value="<?php echo $bindResult['ware_name']; ?>"  lay-verify="required"  autocomplete="off" class="layui-input" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-md4">
                        <div class="layui-form-item">
                            <label class="layui-form-label input-required">所在省:</label>
                            <div class="layui-input-block">
                                <select name="province_id" id='province_id'  lay-verify="required" lay-filter="sheng-shi">
                                    <?php if(is_array($shengResult) || $shengResult instanceof \think\Collection || $shengResult instanceof \think\Paginator): if( count($shengResult)==0 ) : echo "" ;else: foreach($shengResult as $key=>$vo): ?>
                                    <option value="<?php echo $vo['city_id']; ?>" <?php if($vo['city_id'] == $bindResult['ware_province_id']): ?>selected='selected'<?php endif; ?>><?php echo $vo['city_name']; ?></option>
                                    <?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-md4">
                        <div class="layui-form-item">
                            <label class="layui-form-label input-required">所在市:</label>
                            <div class="layui-input-block">
                                <select name="city_id" id='city_id'  lay-verify="required" lay-filter="shi-qu">
                                    <?php if(is_array($cityResult) || $cityResult instanceof \think\Collection || $cityResult instanceof \think\Paginator): if( count($cityResult)==0 ) : echo "" ;else: foreach($cityResult as $key=>$vo): ?>
                                    <option value="<?php echo $vo['city_id']; ?>" <?php if($vo['city_id'] == $bindResult['ware_city_id']): ?>selected='selected'<?php endif; ?>><?php echo $vo['city_name']; ?></option>
                                    <?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-md4">
                        <div class="layui-form-item">
                            <label class="layui-form-label input-required">所在区:</label>
                            <div class="layui-input-block">
                                <select name="area_id" id='area_id'  lay-verify="required">
                                    <?php if(is_array($areaResult) || $areaResult instanceof \think\Collection || $areaResult instanceof \think\Paginator): if( count($areaResult)==0 ) : echo "" ;else: foreach($areaResult as $key=>$vo): ?>
                                    <option value="<?php echo $vo['city_id']; ?>" <?php if($vo['city_id'] == $bindResult['ware_area_id']): ?>selected='selected'<?php endif; ?>><?php echo $vo['city_name']; ?></option>
                                    <?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-md4">
                        <div class="layui-form-item">
                            <label class="layui-form-label input-required">所在地址:</label>
                            <div class="layui-input-block">
                                <input name="ware_address" id="ware_address" value="<?php echo $bindResult['ware_address']; ?>"  lay-verify="required"  autocomplete="off" class="layui-input" type="text">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-md4">
                        <div class="layui-form-item">
                            <label class="layui-form-label input-required">联系人:</label>
                            <div class="layui-input-block">
<!--                                <input name="contacts_name" id="contacts_name"  value="<?php echo $bindResult['contacts_name']; ?>" autocomplete="off" class="layui-input" type="text">-->
                                <select name="contacts_id" id='contacts_id'  lay-verify="required" lay-filter="users">
                                    <?php if(is_array($userResult) || $userResult instanceof \think\Collection || $userResult instanceof \think\Paginator): if( count($userResult)==0 ) : echo "" ;else: foreach($userResult as $key=>$vo): ?>
                                    <option value="<?php echo $vo['user_id']; ?>" <?php if($vo['user_id'] == $bindResult['contacts_id']): ?>selected='selected'<?php endif; ?>><?php echo $vo['nickname']; ?></option>
                                    <?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>
                    </div>
<!--                    <div class="layui-col-md4">-->
<!--                        <div class="layui-form-item">-->
<!--                            <label class="layui-form-label input-required">联系电话:</label>-->
<!--                            <div class="layui-input-block">-->
<!--                                <input name="contacts_cellphone" id="contacts_cellphone" value="<?php echo $bindResult['contacts_cellphone']; ?>"  autocomplete="off" class="layui-input " readonly="readonly" type="text">-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
                    <div class="layui-col-md4">
                        <div class="layui-form-item query-criteria-more">
                            <label class="layui-form-label input-required">状态:</label>
                            <div class="layui-input-block">
                                <select name="status" id='status' >
                                    <option value="1" <?php if($bindResult['status'] == '1'): ?>selected='selected'<?php endif; ?>>启用</option>
                                    <option value="0" <?php if($bindResult['status'] == '0'): ?>selected='selected'<?php endif; ?>>禁用</option>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-row layui-col-space10">
                    <div class="layui-row layui-col-space10">



                    </div>

                    <br/>
                    <br/>
                    <br/>

                    <input type="hidden" id="ware_id" value="<?php echo $bindResult['ware_id']; ?>">
                    <div class="layui-form-item">
                        <div class="all-button-center">
                            <button class="layui-btn nav-submit" lay-submit="" lay-filter="formDemo" id="language_add_button">提交</button>&nbsp;
                            <a href='/warehouse/showWarehouse'><button type="button" class="layui-btn layui-btn-primary">返回</button></a>
                        </div>
                    </div>
            </form>
        </div>
    </div>

    <div class="layui-footer">
        <!-- 底部固定区域 -->
        © layui.com - 底部固定区域
    </div>
</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
<script src='/static/javascript/warehouse/warehouse.js'></script>
<script>

</script>

</body>
</html>
