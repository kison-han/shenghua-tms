<?php


namespace app\index\model\orders;


use app\index\model\Base;
use app\index\model\system\User;
use think\Db;
use think\Exception;

class OrdersOperating extends Base
{
    protected $table = "orders_operating_info";

    public function addInfo($params)
    {
        $data['create_time'] = time();
        $data['orders_id'] = $params['orders_id'];
        $data['order_status'] = $params['order_status'];
        $data['create_user_id'] = $params['user_id'];

        $userModel = new User();
        $userData = $userModel->where('user_id', $params['user_id'])->find();
        if ($userData)
            $data['create_user_name'] = $userData->username;

        return $this->pubInsert($data);
    }

    public function getInfo($params,$is_count=false)
    {
        if($is_count)
        return $this->where('orders_id', $params['orders_id'])->count();
        if(isset($params['page']))
        return $this->where(['orders_id'=>$params['orders_id']])->page($params['page'],$params['limit'])->order('id','desc')->select();
        return $this->where(['orders_id'=>$params['orders_id']])->order('id','desc')->select();

    }
	
	//获取跟踪
	public function getFollowInfo($params){
		$orderWhere['orders_number'] = $params['orders_number'];
		
		$ordersResult = DB::table('orders')->where($orderWhere)->find();
		
		$ordersOperation['orders_id'] = $ordersResult['orders_id'];
		$ordersResult = DB::table('orders_operating_info')->where($ordersOperation)->select();
		return $ordersResult;
	}
	
    public function updateContent($params){

        Db::startTrans();
        try {
            $res=$this->where('id',$params['id'])->update(['remark'=>$params['remark']]);
            Db::commit();
        }
        catch (Exception $e){
            $res=$e->getMessage();
            Db::rollback();

        }
		return $res;
    }

}