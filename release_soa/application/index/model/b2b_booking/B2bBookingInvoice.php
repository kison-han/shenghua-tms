<?php

namespace app\index\model\b2b_booking;
use app\common\help\Help;
use think\Exception;
use think\Model;
use think\Db;
class B2bBookingInvoice extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'b2b_booking_invoice';
    public function initialize()
    {
    	parent::initialize();
    
    }


    public function addB2bBookingInvoice($params){

        $data['b2b_booking_id'] = $params['b2b_booking_id'];
        $data['account_code'] = $params['account_code'];
        $data['invoice_number'] = Help::getNumber(203) . Help::getRandomNumber(6);
        $data['myob_sales'] = $params['myob_sales'];
        $data['myob_cost'] = $params['myob_cost'];
        $data['balance_due'] = bcadd($data['myob_sales'], $data['myob_cost']);
        $data['due_date'] = date("Ymd",strtotime("last month",strtotime($params['begin_time'])));
        $data['invoice_type'] = 0;
    	$data['status'] = $data['is_ready'] = $data['approval'] = 1;
        $data['total_cost'] = $params['total_cost'];
        $data['total_sales'] = $params['total_sales'];

    	try{
    		Db::name('b2b_booking_invoice')->insert($data);
    	} catch (\Exception $e) {

            $result = $e->getMessage();
    		\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		exit();
    	}

    }

    public function updateB2bBookingInvoice($params)
    {
        try{

            $where = " b2b_booking_id = ". $params['b2b_booking_id'];

            if ($params['myob_sales'])
            {
                $data['myob_sales'] = $params['myob_sales'];
            }

            if ($params['myob_cost'])
            {
                $data['myob_cost'] = $params['myob_cost'];
            }
            $data['balance_due'] = bcadd($data['myob_sales'], $data['myob_cost'],2);

            if ($params['status'])
            {
                $data['status'] = $params['status'];
            }
            if ($params['invoice_type'])
            {
                $data['invoice_type'] = $params['invoice_type'];
            }
            if ($params['is_ready'])
            {
                $data['is_ready'] = $params['is_ready'];
            }
            if ($params['approval'])
            {
                $data['approval'] = $params['approval'];
            }

            if ($params['invoice_note'])
            {
                $data['invoice_note'] = $params['invoice_note'];
            }

            $this->where($where)->update($data);
            $result = 1;
        } catch (\Exception $e) {
            Db::rollback();
            $result = $e->getMessage();

            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }
        return $result;
    }

    public function getB2bBookingInvoiceByBtbBookingId($b2b_booking_id)
    {
        try
        {
            $where['b2b_booking_invoice.b2b_booking_id'] = $b2b_booking_id;
            $result = $this->table("b2b_booking_invoice")->alias('b2b_booking_invoice')
                ->join('b2b_booking', 'b2b_booking.b2b_booking_id = b2b_booking_invoice.b2b_booking_id')
                ->field([
                    'b2b_booking_invoice.*', 'b2b_booking.agent_reference_id',
                    '(select create_time from company_order where company_order.company_order_number = b2b_booking.company_order_number)'=> 'create_time',
                    '(select distributor_code from distributor where distributor.distributor_id = b2b_booking.agent_id)'=> 'agent_code',
                    '(select company_name from distributor inner join company on distributor.company_id = company.company_id where distributor.distributor_id = b2b_booking.agent_id)'=> 'agent_company_name',
                    '(select address from distributor where distributor.distributor_id = b2b_booking.agent_id)'=> 'agent_address'
                ])
                ->where($where)->find();
        }
        catch (Exception $e)
        {
            $result = $e->getMessage();
            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }

        return $result;
    }

}