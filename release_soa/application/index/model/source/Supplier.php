<?php

namespace app\index\model\source;
use think\Model;
use app\common\help\Help;
use app\index\service\PublicService;
use think\config;
use think\Db;
class Supplier extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'supplier';
    private $_languageList;
    private $_public_service;
    public function initialize()
    {
    	$this->_languageList = config('systom_setting')['language_list'];
    	$this->_public_service = new PublicService();
    	parent::initialize();
    
    }

    /**
     * 添加供应商
     * 胡
     */
    public function addSupplier($params){
    	$t = time();


    	$data['supplier_name'] = $params['supplier_name'];
    	$data['supplier_type'] = $params['supplier_type'];
    	if(isset($params['linkman'])){
    		$data['linkman'] = $params['linkman'];
    	}
    	if(isset($params['address'])){
    		$data['address'] = $params['address'];
    	}
    	if(isset($params['zip_code'])){
    		$data['zip_code'] = $params['zip_code'];
    	}
    	if(isset($params['phone'])){
    		$data['phone'] = $params['phone'];
    	}
    	if(isset($params['website'])){
    		$data['website'] = $params['website'];
    	}
    	if(isset($params['fax'])){
    		$data['fax'] = $params['fax'];
    	}
    	if(isset($params['cellphone'])){
    		$data['cellphone'] = $params['cellphone'];
    	}
    	if(isset($params['email'])){
    		$data['email'] = $params['email'];
    	}

        if(isset($params['remark'])){
            $data['remark'] = $params['remark'];
        }


        if(isset($params['account_name'])){
        	$data['account_name'] = $params['account_name'];
        }
        if(isset($params['bank_code'])){
        	//$data['bank_code'] = $params['bank_code'];
        	$data['bank_code'] = $params['bank_code'];
        }
        if(isset($params['bank_number'])){
        	$data['bank_number'] = $params['bank_number'];
        }
        if(isset($params['bank_name'])){
        	$data['bank_name'] = $params['bank_name'];
        }
        if(is_numeric($params['supplier_tax_type'])){
        	$data['supplier_tax_type'] = $params['supplier_tax_type'];
        }
        if(is_numeric($params['supplier_tax'])){
        	$data['supplier_tax'] = $params['supplier_tax'];
        }		
    	$data['create_time'] = $t;
    	$data['update_time'] = $t;
    	$data['create_user_id'] = $params['user_id'];
    	$data['update_user_id'] = $params['user_id'];
    	$data['status'] = $params['status'];

	

        $data['supplier_uuid']= Help::getUuid();
    	Db::startTrans();
    	try{
    		$pk_id = Db::name('supplier')->insertGetId($data);

    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
    
    	return $result;
    }

    /**
     * 添加供应商
     * 胡
     */
    public function addSupplierAndCopyOther($params){
        $t = time();

        $old_supplier_id = $params['supplier_id'];
        $data['supplier_name'] = $params['supplier_name'];
        $data['supplier_type'] = $params['supplier_type'];
        if(isset($params['linkman'])){
            $data['linkman'] = $params['linkman'];
        }
        if(isset($params['address'])){
            $data['address'] = $params['address'];
        }
        if(isset($params['zip_code'])){
            $data['zip_code'] = $params['zip_code'];
        }
        if(isset($params['phone'])){
            $data['phone'] = $params['phone'];
        }
        if(isset($params['website'])){
            $data['website'] = $params['website'];
        }
        if(isset($params['fax'])){
            $data['fax'] = $params['fax'];
        }
        if(isset($params['cellphone'])){
            $data['cellphone'] = $params['cellphone'];
        }
        if(isset($params['email'])){
            $data['email'] = $params['email'];
        }

        if(isset($params['remark'])){
            $data['remark'] = $params['remark'];
        }


        if(isset($params['account_name'])){
            $data['account_name'] = $params['account_name'];
        }
        if(isset($params['bank_code'])){
            //$data['bank_code'] = $params['bank_code'];
            $data['bank_code'] = $params['bank_code'];
        }
        if(isset($params['bank_number'])){
            $data['bank_number'] = $params['bank_number'];
        }
        if(isset($params['bank_name'])){
            $data['bank_name'] = $params['bank_name'];
        }
        if(is_numeric($params['supplier_tax_type'])){
            $data['supplier_tax_type'] = $params['supplier_tax_type'];
        }
        if(is_numeric($params['supplier_tax'])){
            $data['supplier_tax'] = $params['supplier_tax'];
        }
        $data['create_time'] = $t;
        $data['update_time'] = $t;
        $data['create_user_id'] = $params['user_id'];
        $data['update_user_id'] = $params['user_id'];
        $data['status'] = $params['status'];



        $data['supplier_uuid']= Help::getUuid();
        Db::startTrans();
        try{
            $supplier_id = Db::name('supplier')->insertGetId($data);

            if($supplier_id>0){
                //查询计价配置
                $data2['supplier_id'] = $old_supplier_id;
                $pricing_configure_result = $this->table("pricing_configure")->alias('pricing_configure')->
                where($data2)->order('pricing_configure.pricing_configure_id asc')->
                field(['pricing_configure.*'])->select();

                $new_data = [];
                foreach($pricing_configure_result as $k1=>$v1){
                    $new_data[$k1]['project_id'] = $v1['project_id'];
                    $new_data[$k1]['supplier_id'] = $supplier_id;
                    $new_data[$k1]['province_start_id'] = $v1['province_start_id'];
                    $new_data[$k1]['city_start_id'] = $v1['city_start_id'];
                    $new_data[$k1]['departure_id'] = $v1['departure_id'];
                    $new_data[$k1]['province_end_id'] = $v1['province_end_id'];
                    $new_data[$k1]['city_end_id'] = $v1['city_end_id'];
                    $new_data[$k1]['arrival_id'] = $v1['arrival_id'];
                    $new_data[$k1]['billing_unit'] = $v1['billing_unit'];
                    $new_data[$k1]['goods_id'] = $v1['goods_id'];
                    $new_data[$k1]['interval_start'] = $v1['interval_start'];
                    $new_data[$k1]['interval_end'] = $v1['interval_end'];
                    $new_data[$k1]['interval_value'] = $v1['interval_value'];
                    $new_data[$k1]['unit_price'] = $v1['unit_price'];
                    $new_data[$k1]['starting_price'] = $v1['starting_price'];
                    $new_data[$k1]['picking_id'] = $v1['picking_id'];
                    $new_data[$k1]['picking_rules'] = $v1['picking_rules'];
                    $new_data[$k1]['picking_price'] = $v1['picking_price'];
                    $new_data[$k1]['delivery_id'] = $v1['delivery_id'];
                    $new_data[$k1]['delivery_rules'] = $v1['delivery_rules'];
                    $new_data[$k1]['delivery_price'] = $v1['delivery_price'];
                    $new_data[$k1]['unloading_id'] = $v1['unloading_id'];
                    $new_data[$k1]['unloading_rules'] = $v1['unloading_rules'];
                    $new_data[$k1]['unloading_price'] = $v1['unloading_price'];
                    $new_data[$k1]['goods_type'] = $v1['goods_type'];
                    $new_data[$k1]['orders_type'] = $v1['orders_type'];
                    $new_data[$k1]['create_user_id'] = $v1['create_user_id'];
                    $new_data[$k1]['create_time'] = $v1['create_time'];
                    $new_data[$k1]['update_user_id'] = $v1['update_user_id'];
                    $new_data[$k1]['update_time'] = $v1['update_time'];
                    $new_data[$k1]['status'] = $v1['status'];
                }
                //复制计价配置
                $status_result = Db::name('pricing_configure')->insertAll($new_data);

                //复制线路
                $data2['supplier_id'] = $old_supplier_id;
                $supplier_uuid = $this->table("supplier")->where($data2)->find()['supplier_uuid'];

                $data4['supplier_id'] = $supplier_id;
                $new_supplier_uuid = $this->table("supplier")->where($data4)->find()['supplier_uuid'];

                $data3['supplier_uuid'] = $supplier_uuid; //老uuid
                $supplier_line_result = $this->table("supplier_line")->alias('supplier_line')->
                where($data3)->order('supplier_line.supplier_line_id asc')->
                field(['supplier_line.*'])->select();

                $new_data2 = [];
                foreach($supplier_line_result as $k2=>$v2){
                    $new_data2[$k2]['supplier_line_uuid'] = $v2['supplier_line_uuid'];
                    $new_data2[$k2]['supplier_uuid'] = $new_supplier_uuid;
                    $new_data2[$k2]['start_province_id'] = $v2['start_province_id'];
                    $new_data2[$k2]['start_city_id'] = $v2['start_city_id'];
                    $new_data2[$k2]['start_location_id'] = $v2['start_location_id'];
                    $new_data2[$k2]['end_province_id'] = $v2['end_province_id'];
                    $new_data2[$k2]['end_city_id'] = $v2['end_city_id'];
                    $new_data2[$k2]['end_location_id'] = $v2['end_location_id'];
                    $new_data2[$k2]['cost_hour'] = $v2['cost_hour'];
                    $new_data2[$k2]['line_type'] = $v2['line_type'];
                    $new_data2[$k2]['start_site_name'] = $v2['start_site_name'];
                    $new_data2[$k2]['start_site_phone'] = $v2['start_site_phone'];
                    $new_data2[$k2]['start_site_address'] = $v2['start_site_address'];
                    $new_data2[$k2]['end_site_name'] = $v2['end_site_name'];
                    $new_data2[$k2]['end_site_phone'] = $v2['end_site_phone'];
                    $new_data2[$k2]['end_site_address'] = $v2['end_site_address'];
                    $new_data2[$k2]['create_user_id'] = $v2['create_user_id'];
                    $new_data2[$k2]['create_time'] = $v2['create_time'];
                    $new_data2[$k2]['update_user_id'] = $v2['update_user_id'];
                    $new_data2[$k2]['update_time'] = $v2['update_time'];
                    $new_data2[$k2]['status'] = $v2['status'];

                }

                //复制计价配置
                $status_supplier_result = Db::name('supplier_line')->insertAll($new_data2);

                
                    
             

            }
			$result = 1;
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
            //\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            //exit();

        }

        return $result;
    }

    /**
     * 获取供应商
     * 胡
     */
    public function getSupplier($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    

    	$data = "1=1 ";



    	if(isset($params['keyword'])){
    		$data.= " and supplier.supplier_name like '%".$params['keyword']."%'";
    	}
    	if(isset($params['supplier_id'])){
    		$data.= " and supplier.supplier_id= ".$params['supplier_id'];
    	}
        if(!empty($params['supplier_uuid'])){

            $supplier_array= explode(",",$params['supplier_uuid']);
            $supplier_str="";
            foreach ($supplier_array as $k1=>$v1)
            {   $supplier_array[$k1]="'".$v1."'";
            }
            $supplier_str=implode(',',$supplier_array);
            $data.=' and supplier.supplier_uuid in ('.$supplier_str.')';
        }
		
		
    	if(isset($params['status'])){
    		$data.= " and supplier.status = ".$params['status'];
    	}
    	if(isset($params['supplier_name'])){
    		$data.= ' and supplier.supplier_name like "%'.$params['supplier_name'].'%"';
    	}
    	if(!empty($params['supplier_type'])){
    		$data.= " and supplier.supplier_type = ".$params['supplier_type'];
    	}
        if(!empty($params['supplier_names'])){
            $data.=' and supplier.supplier_name ="'.$params['supplier_names'].'"';
        }
    	if(isset($params['phone'])){
    		$data.= " and supplier.phone = '".$params['phone']."'";
    	} 
    	if(isset($params['linkman'])){
    			$data.= ' and supplier.linkman like "%'.$params['linkman'].'%"';
    	}
    	if(isset($params['choose_company_id'])){
    			$data.= ' and supplier.company_id = '.$params['choose_company_id'];
    	}	
        if($is_count==true){
            $result = $this->table("supplier")->alias("supplier")->where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("supplier")->alias('supplier')->

                //join('source_level','source_level.source_level_id = supplier.source_level_id','left')->

                where($data)->limit($page, $page_size)->order('create_time desc')->
                field(['supplier.supplier_id', "supplier_uuid","supplier.supplier_name", "supplier.supplier_type",
                     'supplier.linkman', 'supplier.address', 'supplier.zip_code','supplier.cellphone',
                    'supplier.phone', 'supplier.website', 'supplier.fax', 'supplier.email', 'supplier.remark','supplier.supplier_tax',
        
                	'supplier.bank_code','supplier.account_name','bank_name','bank_number',
					'supplier.supplier_tax_type','supplier.supplier_tax',

		
                    "(select nickname  from user where user.user_id = supplier.create_user_id)"=> 'create_user_name',
                    "(select nickname  from user where user.user_id = supplier.update_user_id)"=> 'update_user_name',

                    'supplier.update_time', 'supplier.create_time', "supplier.status"])->select();
            }else{
                $result = $this->table("supplier")->alias('supplier')->
                where($data)->order('create_time desc')->
                field(['supplier.supplier_id', "supplier_uuid","supplier.supplier_name", "supplier.supplier_type",
                     'supplier.linkman', 'supplier.address', 'supplier.zip_code', 'supplier.cellphone',
                    'supplier.phone', 'supplier.website', 'supplier.fax', 'supplier.email', 'supplier.remark',
					'supplier.supplier_tax',
                	'supplier.bank_code','supplier.account_name','bank_name','bank_number',
					'supplier.supplier_tax_type','supplier.supplier_tax',
                    "(select nickname  from user where user.user_id = supplier.create_user_id)"=> 'create_user_name',
                    "(select nickname  from user where user.user_id = supplier.update_user_id)"=> 'update_user_name',

                    'supplier.update_time', 'supplier.create_time', "supplier.status"])->select();
            }
			
			if(is_numeric($params['send_location_id'])){
				$new_result = [];
				$getihu = [];
				for($i=0;$i<count($result);$i++){
					
					$newWhere['supplier_uuid'] = $result[$i]['supplier_uuid'];

					$newWhere['start_location_id'] = $params['send_location_id'];
					$newWhere['end_location_id'] = $params['accept_location_id'];
				
					$supplier_count = $this->table("supplier_line")->where($newWhere)->count();
					
					
					
					if($supplier_count>0){
						$result[$i]['is_line'] = 1;
						array_unshift($new_result,$result[$i]);
					}else{
						//代表公司自营
						if($result[$i]['supplier_type']==1){
							$result[$i]['is_line'] = 0;
							array_push($new_result,$result[$i]);
							
						}else{
							if($result[$i]['supplier_type']==3){
													
								$result[$i]['is_line'] = 0;
								array_push($getihu,$result[$i]);
							}						
							
						}
						

	
					}
					
				}
				$new_result = array_merge($new_result,$getihu);
				
				$result = $new_result;
			}
			
        }
    	 
		
    
    	return $result;
    
    }


    /**
     * 修改供应商多语言数据根据供应商多语言ID
     */
    public function updateSupplierLanguageBySupplierLanguageId($params){
    	
    	$t = time();
    	$user_id = $params['user_id'];
    	
    	$original_number = $params['data'][0]['supplier_number'];
    	
    	$original_data['supplier_number'] = $original_number;
    	
    	 
    	$params = $params['data'];
    	
    	//原始数据主键
    	
    	$original_result = $this->getSupplier($original_data);
    
    	
    	$default_language_id = $original_result[0]['default_language_id'];
		
    	$this->startTrans();
    	try{
    		for($i=0;$i<count($params);$i++){
    			
    			$data = [];
    			if(!trim($params[$i]['supplier_name'])==''){
    				 
    				$data['supplier_name'] = $params[$i]['supplier_name'];
    				$data['update_time'] = $t;
    				$data['update_user_id'] = $user_id;
    
    				if(is_numeric($params[$i]['supplier_language_id'])){
    					$this->table('supplier_language')->where("supplier_language_id = ".$params[$i]['supplier_language_id'])->update($data);
    				
    					//再查询是否是原始数据  如果是原始数据那么原始 数据也要更改
    					if($default_language_id == $params[$i]['lang_id']){
    					
    						$this->where("supplier_number = '$original_number'")->update($data);
    						
    						
    					}
    				}else{
    						
    					$data['create_time'] = $t;
    					$data['create_user_id'] = $user_id;
    					$data['status'] = 1;
    					$data['supplier_number'] = $original_number;
    					$data['language_id'] = $params[$i]['lang_id'];
    
    					$this->table("supplier_language")->insert($data);
    				
    					 
    				}
    			}
    		}
    
    		$result = 1;
    		// 提交事务
    		$this->commit();
    		 
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		$this->rollback();
    		 
    	}
    
    	return $result;
    	 
    }    
    /**
     * 修改供应商 根据supplier_id
     */
    public function updateSupplierBySupplierId($params){
    
    	$t = time();

    	if(!empty($params['supplier_name'])){
    		$data['supplier_name'] = $params['supplier_name'];    
    	}

    	$data['supplier_type'] = $params['supplier_type'];


        if(isset($params['linkman'])){
            $data['linkman'] = $params['linkman'];
        }
        if(isset($params['address'])){
            $data['address'] = $params['address'];
        }
        if(isset($params['zip_code'])){
            $data['zip_code'] = $params['zip_code'];
        }
        if(isset($params['phone'])){
            $data['phone'] = $params['phone'];
        }
        if(isset($params['website'])){
            $data['website'] = $params['website'];
        }
        if(isset($params['fax'])){
            $data['fax'] = $params['fax'];
        }


    	if(!empty($params['email'])){
    		$data['email'] = $params['email'];
    	}
    	
    	if(isset($params['status'])){
    		$data['status'] = $params['status'];
    
    	}
    	if(isset($params['remark'])){
    		$data['remark'] = $params['remark'];
    	
    	}

        if(isset($params['account_name'])){
        	$data['account_name'] = $params['account_name'];
        }
        if(isset($params['bank_code'])){
        	//$data['bank_code'] = $params['bank_code'];
        	$data['bank_code'] = $params['bank_code'];
        }
        if(isset($params['bank_number'])){
        	$data['bank_number'] = $params['bank_number'];
        }
        if(isset($params['bank_name'])){
        	$data['bank_name'] = $params['bank_name'];
        }  
        if(is_numeric($params['supplier_tax_type'])){
        	$data['supplier_tax_type'] = $params['supplier_tax_type'];
        }
        if(is_numeric($params['supplier_tax'])){
        	$data['supplier_tax'] = $params['supplier_tax'];
        }	    
    	$data['update_user_id'] = $params['user_id'];
    	$data['update_time'] = $t;


        if(isset($params['cellphone'])){
            $data['cellphone'] = $params['cellphone'];
        }    
		
	

    	Db::startTrans();
    	try{
    		Db::name('supplier')->where("supplier_id = ".$params['supplier_id'])->update($data);

    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    
    	}
		
    	return $result;
    }

    /**
     * getOneSupplier
     *
     * 获取一条供应商数据
     * @author shj
     *
     * @param $supplier_id
     *
     * @return void
     * Date: 2019/2/26
     * Time: 10:24
     */
    public function getOneSupplier($supplier_id){
        $result = $this->table("supplier")->where(['supplier_id' => $supplier_id])->find();
        return $result;
    }

    /**
     * getOneSupplierByParams
     *
     * @author shj
     *
     * 根据数组条件获取一条供应商数据
     * @param $params
     *
     * @return array|false|\PDOStatement|string|Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * Date: 2019/4/23
     * Time: 17:27
     */
    public function getOneSupplierByParams($params){

      
        $result = $this->table("supplier")->where($params)->find();
        return $result;
    }

    /**
     * getNewSupplier
     *
     * @author shj
     *
     * 重写获取供应商列表
     * @param      $params
     * @param bool $is_count
     * @param bool $is_page
     * @param null $page
     * @param int  $page_size
     *
     * @return false|int|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * Date: 2019/4/23
     * Time: 17:28
     */
    public function getNewSupplier($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
        $data = "1=1 ";
        if(isset($params['supplier_id'])){
            $data.= " and supplier.supplier_id= ".$params['supplier_id'];
        }
        if($params['supplier_number']){
            $data.= " and supplier.supplier_number= '".$params['supplier_number']."'";
        }
        if(isset($params['status'])){
            $data.= " and supplier.status = ".$params['status'];
        }
        if($params['supplier_name']){
            $data.= " and supplier.supplier_name like '%".$params['supplier_name']."%'";
        }
        if(!empty($params['supplier_type_id'])){
            $data.= " and supplier.supplier_type_id = ".$params['supplier_type_id'];
        }
        if(!empty($params['country_id'])){
            $data.= " and supplier.country_id = '".$params['country_id']."'";
        }
        if(isset($params['phone'])){
            $data.= " and supplier.phone = '".$params['phone']."'";
        }
        if($params['linkman']){
            $data.= " and supplier.linkman = '".$params['linkman']."'";
        }
        if ($params['add'] == 1){
            if(is_numeric($params['company_id'])) {
                $data .= " and supplier.company_id = '" . $params['company_id'] . "'";
            }
        }else{
            if(is_numeric($params['company_id'])){
                $data.= " and ((supplier.company_id = ".$params['company_id']." and supplier.is_company = 0 ) or (supplier.company_id <> ".$params['company_id']." and supplier.is_company = 1))";
            }
        }

        if(!empty($params['supplier_style'])){
            $data.= " and supplier.supplier_style = ".$params['supplier_style'];
        }

        //b2b酒店
        if(!empty($params['supplier_name_en'])){
            $data.= " and supplier.supplier_name_en like '%".trim($params['supplier_name_en'])."%'";
        }
        if(!empty($params['address'])){
            $data.= " and supplier.address like '%".trim($params['address'])."%'";
        }
        if(!empty($params['address_en'])){
            $data.= " and supplier.address_en like '%".trim($params['address_en'])."%'";
        }

      

        if($is_count==true){
            $result = $this->table("supplier")->where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("supplier")
                    ->join("country", "supplier.country_id= country.country_id", 'left')
                    ->join("country a", "a.country_id= country.pid", 'left')
                    ->join("country b", "b.country_id= a.pid", 'left')
                    ->join('supplier_type', 'supplier_type.supplier_type_id = supplier.supplier_type_id','left')
                    ->join('company', 'company.company_id= supplier.company_id','left')
                    ->where($data)->limit($page, $page_size)->order('create_time desc')
                    ->field(['supplier.*','company.company_name','supplier_type.supplier_type_name',
                    'country.country_id as city_id','country.country_name as city_name','country.level as city_level','country.pid as city_pid',
                    'a.country_id as province_id','a.country_name as province_name','a.pid as province_pid',
                    'b.country_id as country_id','b.country_name as country_name',
                    ])->select();
            }else{
                $result = $this->table("supplier")
                    ->join("country", "supplier.country_id= country.country_id", 'left')
                    ->join("country a", "a.country_id= country.pid", 'left')
                    ->join("country b", "b.country_id= a.pid", 'left')
                    ->join('supplier_type', 'supplier_type.supplier_type_id = supplier.supplier_type_id','left')
                    ->join('company', 'company.company_id= supplier.company_id','left')
                    ->where($data)->order('create_time desc')
                    ->field(['supplier.*','company.company_name','supplier_type.supplier_type_name',
                        'country.country_id as city_id','country.country_name as city_name','country.level as city_level','country.pid as city_pid',
                        'a.country_id as province_id','a.country_name as province_name','a.pid as province_pid',
                        'b.country_id as country_id','b.country_name as country_name',
                    ])->select();
            }
        }



        return $result;

    }


    public function getSuppliersWithResources($params){
        $where = "supplier.status=1";
        if($params['company_id']){
            $where .= " and supplier.company_id={$params['company_id']}";
        }

        $ar = [];
        if($params['supplier_type_id']==2){ //酒店
            $where .= " and hotel.company_id={$params['company_id']}";
            $ar = $this->table('supplier')->join('hotel','hotel.supplier_id=supplier.supplier_id')
            ->where($where)->group('supplier.supplier_id')->select();
        }
        if($params['supplier_type_id']==3){ //用餐
            $where .= " and dining.company_id={$params['company_id']}";
            $ar = $this->table('supplier')->join('dining','dining.supplier_id=supplier.supplier_id')
            ->where($where)->group('supplier.supplier_id')->select();
        }
        if($params['supplier_type_id']==4){
            $where .= " and flight.company_id={$params['company_id']}";
            $ar = $this->table('supplier')->join('flight','flight.supplier_id=supplier.supplier_id')
            ->where($where)->group('supplier.supplier_id')->select(); 
        }
        if($params['supplier_type_id']==5){
            $where .= " and cruise.company_id={$params['company_id']}";
            $ar = $this->table('supplier')->join('cruise','cruise.supplier_id=supplier.supplier_id')
            ->where($where)->group('supplier.supplier_id')->select(); 
        }
        if($params['supplier_type_id']==6){
            $where .= " and visa.company_id={$params['company_id']}";
            $ar = $this->table('supplier')->join('visa','visa.supplier_id=supplier.supplier_id')
            ->where($where)->group('supplier.supplier_id')->select(); 
        }
        if($params['supplier_type_id']==7){
            $where .= " and scenic_spot.company_id={$params['company_id']}";
            $ar = $this->table('supplier')->join('scenic_spot','scenic_spot.supplier_id=supplier.supplier_id')
            ->where($where)->group('supplier.supplier_id')->select(); 
        }
        if($params['supplier_type_id']==8){
            $where .= " and vehicle.company_id={$params['company_id']}";
            $ar = $this->table('supplier')->join('vehicle','vehicle.supplier_id=supplier.supplier_id')
            ->where($where)->group('supplier.supplier_id')->select(); 
        }
        if($params['supplier_type_id']==9){
            $where .= " and tour_guide.company_id={$params['company_id']}";
            $ar = $this->table('supplier')->join('tour_guide','tour_guide.supplier_id=supplier.supplier_id')
            ->where($where)->group('supplier.supplier_id')->select(); 
        }
        if($params['supplier_type_id']==10){
            $where .= " and single_source.company_id={$params['company_id']}";
            $ar = $this->table('supplier')->join('single_source','single_source.supplier_id=supplier.supplier_id')
            ->where($where)->group('supplier.supplier_id')->select(); 
        }
        if($params['supplier_type_id']==11){
            $where .= " and own_expense.company_id={$params['company_id']}";
            $ar = $this->table('supplier')->join('own_expense','own_expense.supplier_id=supplier.supplier_id')
            ->where($where)->group('supplier.supplier_id')->select(); 
        }
        


        return $ar;


    }

}