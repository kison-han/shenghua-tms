<?php


namespace app\index\controller\device;


use app\index\controller\Base;

class Smartboxmanage extends Base
{

    public function getboxList(){
        $params = request()->param();
        $box_model = new  \app\index\model\device\SmartBox();
        $this->outPut($box_model->getList($params));

    }
}