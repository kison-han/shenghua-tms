<?php

namespace app\index\model\source;
use think\Model;
use app\common\help\Help;
use app\index\service\PublicService;
use think\config;
use think\Db;
class Driver extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'driver';
    private $_languageList;
    private $_public_service;
    public function initialize()
    {
    	$this->_languageList = config('systom_setting')['language_list'];
    	$this->_public_service = new PublicService();
    	parent::initialize();
    
    }

    /**
     * 添加司机
     * 胡
     */
    public function addDriver($params){
    	$t = time();


    	$data['supplier_id'] = $params['supplier_id'];

    	
    	if(!empty($params['vehicle_id'])){
    		$data['vehicle_id'] = $params['vehicle_id'];
    	}

    	if(!empty($params['driver_license_type'])){
    		$data['driver_license_type'] = $params['driver_license_type'];
    	}if(!empty($params['driver_license_number'])){
    		$data['driver_license_number'] = $params['driver_license_number'];
    	}
    	
    	$data['driver_name'] = $params['driver_name'];
    	if(!empty($params['driver_cellphone'])){
    		$data['driver_cellphone'] = $params['driver_cellphone'];
    	
    	}
    	if(!empty($params['driver_idcard'])){
    		$data['driver_idcard'] = $params['driver_idcard'];
    	
    	}


        if(isset($params['remark'])){
        	$data['remark'] = $params['remark'];
        }        

    	$data['create_time'] = $t;  	
    	$data['create_user_id'] = $params['user_id'];
    	$data['update_time'] = $t;
    	$data['update_user_id'] = $params['user_id'];
    	$data['status'] = $params['status'];

    	$data['driver_uuid'] = Help::getUuid();
    	Db::startTrans();
    	try{
    		$pk_id = Db::name('driver')->insertGetId($data);
	
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
    
    	return $result;
    }
    
    /**
     * 获取司机
     * 胡
     */
    public function getDriver($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    	$data = "1=1 ";

    	if(is_numeric($params['status'])){
    		$data.= " and driver.status = ".$params['status'];
    	}
    	if(!empty($params['vehicle_id'])){
    		$data.= " and driver.vehicle_id = '".$params['vehicle_id']."'";
    	}

    	if(!empty($params['supplier_id'])){
    		$data.= " and driver.supplier_id = '".$params['supplier_id']."'";
    	}	
    	if(!empty($params['number_plate'])){
    		$data.= ' and vehicle.number_plate like "%'.$params['number_plate'].'%"';
    	}
    	if(!empty($params['driver_name'])){
    		$data.= ' and driver.driver_name like "%'.$params['driver_name'].'%"';
    	}   	
    	if(!empty($params['driver_cellphone'])){
    		$data.= ' and driver.driver_cellphone like "%'.$params['driver_cellphone'].'%"';
    	}    	

    	if(!empty($params['driver_idcard'])){
    		$data.= ' and driver.driver_idcard like "%'.$params['driver_idcard'].'%"';
    	}
    	

        if($is_count==true){
            $result = $this->table("driver")-> join('vehicle', 'driver.vehicle_id = vehicle.vehicle_id','left')->
                join('supplier', 'supplier.supplier_id = driver.supplier_id')->where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("driver")->
                join('vehicle', 'driver.vehicle_id = vehicle.vehicle_id','left')->
                join('supplier', 'supplier.supplier_id = driver.supplier_id')->
          
                where($data)->limit($page, $page_size)->order('create_time desc')->
                field(['driver.driver_id', "driver.driver_uuid", 'driver.supplier_id',
                   
                    'driver.vehicle_id', 'driver.driver_name',
                    'driver.driver_cellphone', "driver.driver_idcard",
                    'driver.remark',
                    'driver.driver_license_type','driver.driver_license_number',
                    'supplier.supplier_name', 'vehicle.number_plate',

                    "(select nickname  from user where user.user_id = driver.create_user_id)"=> 'create_user_name',
                    "(select nickname  from user where user.user_id = driver.update_user_id)"=> 'update_user_name',
                    'driver.update_time', 'driver.create_time', "driver.status",
                ])->select();
               
            }else{
                $result = $this->table("driver")->alias('driver')->
                join('vehicle', 'driver.vehicle_id = vehicle.vehicle_id','left')->
                join('supplier', 'supplier.supplier_id = driver.supplier_id')->
                where($data)->order('create_time desc')->
                field(['driver.driver_id', "driver.driver_uuid", 'driver.supplier_id',
                   
                    'driver.vehicle_id', 'driver.driver_name',
                    'driver.driver_cellphone', "driver.driver_idcard",

                    'driver.driver_license_type','driver.driver_license_number',
                    'driver.remark',

                    'supplier.supplier_name', 'vehicle.number_plate',

                    "(select nickname  from user where user.user_id = driver.create_user_id)"=> 'create_user_name',
                    "(select nickname  from user where user.user_id = driver.update_user_id)"=> 'update_user_name',
                    'driver.update_time', 'driver.create_time', "driver.status",
                ])->select();
            }
        }

        return $result;
    
    }

    /**
     * 修改司机
     */
    public function updateDriverByDriverId($params){
    
    	$t = time();
    	
    	if(!empty($params['driver_name'])){
    		$data['driver_name'] = $params['driver_name'];
    	
    	}

        if(!empty($params['driver_license_type'])){
            $data['driver_license_type'] = $params['driver_license_type'];
        }if(!empty($params['driver_license_number'])){
            $data['driver_license_number'] = $params['driver_license_number'];
        }

    	$data['supplier_id'] = $params['supplier_id'];
    		 
 

        $data['vehicle_id'] = $params['vehicle_id'];
    		 
    
        if(isset($params['driver_name'])){
            $data['driver_name'] = $params['driver_name'];
        }
        if(isset($params['driver_cellphone'])){
            $data['driver_cellphone'] = $params['driver_cellphone'];
        }
        if(isset($params['driver_idcard'])){
            $data['driver_idcard'] = $params['driver_idcard'];
        }

        if(!empty($params['remark'])){
            $data['remark'] = $params['remark'];

        }
    	if(!empty($params['status'])){
    		$data['status'] = $params['status'];
    		 
    	}



    	$data['update_user_id'] = $params['user_id'];   
    	$data['update_time'] = $t;

    
    
    	$source_price=[];
    	Db::startTrans();
    	try{
    		Db::name('driver')->where("driver_id = ".$params['driver_id'])->update($data);
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    
    	}
    	return $result;
    }

}