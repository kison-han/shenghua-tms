<?php

namespace app\index\model\Btob;
use think\Model;
use app\common\help\Help;
use think\config;
use think\Db;
class DistributorConsultant extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'distributor_consultant';
    private $_languageList;
    public function initialize()
    {
        $this->_languageList = config('systom_setting')['language_list'];
        parent::initialize();

    }

    public function addDistributorConsultant($params){

        $data['distributor_id'] = $params['distributor_id'];
        $data['consultant_name'] = $params['consultant_name'];

        Db::startTrans();
        try{
            $result = Db::name('distributor_consultant')->insertGetId($data);
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return $result;
    }

    public function getDistributorConsultant($params,$is_count=false,$is_page=false,$page=null,$page_size=20){

    	$data = "1=1";
    	if($params['distributor_consultant_id']>0){
    		$data.= " and distributor_consultant.distributor_consultant_id = '".$params['distributor_consultant_id']."'";
    	}
    	if(!empty($params['consultant_name'])){
    		$data.= " and distributor_consultant.consultant_name like'%".$params['consultant_name']."%'";
    	}
        if(!empty($params['distributor_id'])){
            $data.= " and distributor_consultant.distributor_id = '".$params['distributor_id']."'";
        }

        if($is_count==true){
            $result = $this->table("distributor_consultant")->where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("distributor_consultant")
                    ->where($data)->limit($page, $page_size)
                    ->field(['distributor_consultant.*'])
                    ->select();
            }else{
                $result = $this->table("distributor_consultant")
                    ->where($data)
                    ->field(['distributor_consultant.*'])
                    ->select();
            }
        }

        return $result;

    }


    public function updateDistributorConsultant($params){
        if(isset($params['consultant_name'])){
            $data['consultant_name'] = $params['consultant_name'];
        }
        Db::startTrans();
        try{
            Db::name('distributor_consultant')->where("distributor_consultant_id = ".$params['distributor_consultant_id'])->update($data);

            $result = 1;
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();

        }
        return $result;
    }

    public function deleteDistributorConsultant($params){

        if(!empty($params['distributor_consultant_id'])){
            $result = Db::name('distributor_consultant')->where("distributor_consultant_id = ".$params['distributor_consultant_id'])->delete();
            return $result;
        }
    }

    public function getOneDistributorConsultant($params){
        $result = $this->table("distributor_consultant")->where(['distributor_consultant_id' => $params['distributor_consultant_id']])->find();
        return $result;
    }
}