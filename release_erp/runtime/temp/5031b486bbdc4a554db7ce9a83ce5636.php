<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:83:"/var/www/html/test_erp/public/../application/index/view/order/order_income_add.html";i:1663569097;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<html >
<head>
	  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






	<title>
  										<?php if(\think\Request::instance()->get('order_id') ==  ''): ?>
										新增运单
										<?php else: ?>
										修改运单
										<?php endif; ?>	
	
	</title>
<style>
	td{overflow: inherit!important;}
.addGoods{
	    color: blue;
	    display: block;
	    margin: 0 auto;
	    text-align: center;
	    font-size: 39px;
}
.delGoods{
	    color: red;
	    display: block;
	    margin: 0 auto;
	    text-align: center;
	    font-size: 39px;
}
	
</style>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">

	

	

	<div class="layui-body">

		<!-- 内容主体区域 -->
		<div class="content_body">
			<div class='layui-form-item'>
		   		<span class="layui-breadcrumb" lay-separator="-">
					<a>首页</a>
					<a>运单管理</a>
					<a >收入核算 </a>	
		
					<a><cite>
					
					  				 <?php if(\think\Request::instance()->get('order_id') ==  ''): ?>
										修改核算
										<?php else: ?>
										修改核算
										<?php endif; ?>	
					
					</cite></a>
				</span>
			</div>
			<br/>


			<br/>
			<form class="layui-form layui-form-pane"  id="form1" onSubmit="return orderIncomeAdd()">
	
	
					<div style="pointer-events: none;">
					
						<div class="layui-inline">
							<label class="layui-form-label input-required">项目:</label>
							<div class="layui-input-inline " >
								<select name="project_id" id='project_id'  lay-verify="required"   lay-filter="orderProject">
										<option value=''>请选择</option>
									<?php if(is_array($projectResult) || $projectResult instanceof \think\Collection || $projectResult instanceof \think\Paginator): if( count($projectResult)==0 ) : echo "" ;else: foreach($projectResult as $key=>$vo): ?>
										<option value="<?php echo $vo['project_id']; ?>" <?php if($vo['project_id'] == \think\Request::instance()->get('project_id')): ?>selected<?php endif; ?>> <?php echo $vo['project_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>
						
			
					<hr/>
				<div class="layui-row layui-col-space10" >
					<div class="layui-form-item">
						<div class="layui-inline">
							<label class="layui-form-label">发货方:</label>
							<div class="layui-input-inline">
								<select name="send_goods_id" id='send_goods_id'  lay-filter="orderSend">
								
									<?php if(is_array($sendResult) || $sendResult instanceof \think\Collection || $sendResult instanceof \think\Paginator): if( count($sendResult)==0 ) : echo "" ;else: foreach($sendResult as $key=>$vo): ?>
										<option value="<?php echo $vo['send_goods_id']; ?>" <?php if($result['send_goods_id'] == $vo['send_goods_id']): ?>select<?php endif; ?>> <?php echo $vo['send_goods_company']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>
						<div class="layui-inline">
							<label class="layui-form-label input-required">联系人:</label>
							<div class="layui-input-inline">
								 <input id="send_name" name="send_name" value="<?php echo $result['send_name']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
					
							</div>
						</div>
		
						<div class="layui-inline">
							<label class="layui-form-label input-required">联系方式:</label>
							<div class="layui-input-inline">
								 <input id="send_cellphone" name="send_cellphone" value="<?php echo $result['send_cellphone']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
				
							</div>
						</div>					
					
						<div class="layui-inline">
							<label class="layui-form-label input-required">省:</label>
							<div class="layui-input-inline " >
								<select name="send_province_id" id='send_province_id'  lay-verify="required"   lay-filter="orderProvince"  lay-search>
									<?php if(is_array($provinceResult) || $provinceResult instanceof \think\Collection || $provinceResult instanceof \think\Paginator): if( count($provinceResult)==0 ) : echo "" ;else: foreach($provinceResult as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $result['send_province_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>
				
			
						<div class="layui-inline">
							<label class="layui-form-label input-required">市:</label>
							<div class="layui-input-inline">
								<select name="send_city_id" id='send_city_id'  lay-verify="required" lay-filter="orderCity">
									<?php if(is_array($cityResult) || $cityResult instanceof \think\Collection || $cityResult instanceof \think\Paginator): if( count($cityResult)==0 ) : echo "" ;else: foreach($cityResult as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $result['send_city_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>
		
		
						<div class="layui-inline">
							<label class="layui-form-label input-required">区:</label>
							<div class="layui-input-inline">
								<select name="send_area_id" id='send_area_id'  lay-verify="required">
									<?php if(is_array($areaResult) || $areaResult instanceof \think\Collection || $areaResult instanceof \think\Paginator): if( count($areaResult)==0 ) : echo "" ;else: foreach($areaResult as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $result['send_area_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>
							
			
						<div class="layui-inline">
							<label class="layui-form-label input-required">地址:</label>
							<div class="layui-input-inline">
								 <input id="send_address" name="send_address" value="<?php echo $result['send_address']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
													
							</div>
						</div>
			
			

					
																								
				</div>
			</div>		
			
				<div class="layui-row layui-col-space10" >
					<div class="layui-form-item">
						<div class="layui-inline">
							<label class="layui-form-label">收货方:</label>
							<div class="layui-input-inline">
								<select name="accept_goods_id" id='accept_goods_id'   lay-filter="orderAccept">
						
									<?php if(is_array($acceptResult) || $acceptResult instanceof \think\Collection || $acceptResult instanceof \think\Paginator): if( count($acceptResult)==0 ) : echo "" ;else: foreach($acceptResult as $key=>$vo): ?>
										<option value="<?php echo $vo['accept_goods_id']; ?>" <?php if($result['accept_goods_id'] == $vo['accept_goods_id']): ?>select<?php endif; ?>> <?php echo $vo['accept_goods_company']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>
				
						<div class="layui-inline">
							<label class="layui-form-label input-required">联系人:</label>
							<div class="layui-input-inline">
								 <input id="accept_name" name="accept_name" value="<?php echo $result['accept_name']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
					
							</div>
						</div>
				
				
						<div class="layui-inline">
							<label class="layui-form-label input-required">联系方式:</label>
							<div class="layui-input-inline">
								 <input id="accept_cellphone" name="accept_cellphone" value="<?php echo $result['accept_cellphone']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
				
							</div>
						</div>
						
						<div class="layui-inline">
							<label class="layui-form-label input-required">省:</label>
							<div class="layui-input-inline" >
								<select name="accept_province_id" id='accept_province_id'  lay-verify="required"   lay-filter="accept_province_id"  lay-search>
									<?php if(is_array($provinceResult) || $provinceResult instanceof \think\Collection || $provinceResult instanceof \think\Paginator): if( count($provinceResult)==0 ) : echo "" ;else: foreach($provinceResult as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $result['accept_province_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>

						<div class="layui-inline">
							<label class="layui-form-label input-required">市:</label>
							<div class="layui-input-inline">
								<select name="accept_city_id" id='accept_city_id'  lay-verify="required" lay-filter="accept_city_id" lay-search placeholder='请选择市'>
									<?php if(is_array($cityResult) || $cityResult instanceof \think\Collection || $cityResult instanceof \think\Paginator): if( count($cityResult)==0 ) : echo "" ;else: foreach($cityResult as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $result['accept_city_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>

						<div class="layui-inline">
							<label class="layui-form-label input-required">区:</label>
							<div class="layui-input-inline">
								<select name="accept_area_id" id='accept_area_id'  lay-verify="required" lay-filter="accept_area_id" lay-search placeholder='请选择区'>
									<?php if(is_array($areaResult) || $areaResult instanceof \think\Collection || $areaResult instanceof \think\Paginator): if( count($areaResult)==0 ) : echo "" ;else: foreach($areaResult as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>" <?php if($vo['city_id'] == $result['accept_area_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>
							<div class="layui-inline">
							<label class="layui-form-label input-required">地址:</label>
							<div class="layui-input-inline">
								 <input id="accept_address" name="accept_address" value="<?php echo $result['accept_address']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
													
							</div>
						</div>					
						
		

	

																					
					</div>
				</div>
				<hr/>
			
	
				<hr/>		
		
			
				<div class="layui-row layui-col-space10" >	
	
							<div class="layui-form-item">
							         <div class="layui-inline">
							      		<label class="layui-form-label">客户单号</label>
							      			<div class="layui-input-inline">
								  				<input id="customer_order_number" name="customer_order_number" value="<?php echo $result['customer_order_number']; ?>"   placeholder="" autocomplete="off" class="layui-input" type="text">
							     			 </div>
							    	</div>								
								<div class="layui-inline">
									<label class="layui-form-label input-required">支付方式:</label>
									<div class="layui-input-inline">
										<select name="pay_type" id='pay_type'  lay-verify="required">

											<option value="3" <?php if($result['pay_type'] == 3): ?>selected<?php endif; ?>> 月结</option>
											<option value="1" <?php if($result['pay_type'] == 1): ?>selected<?php endif; ?>> 现付</option>
											<option value="2" <?php if($result['pay_type'] == 2): ?>selected<?php endif; ?>> 到付</option>
										</select>							
									</div>
								</div>
						
					
								<div class="layui-inline">
									<label class="layui-form-label input-required">计价方式:</label>
									<div class="layui-input-inline">
										<select name="bargain_type" id='bargain_type'  lay-verify="required"  lay-filter="bargain_type">
										
												<option value="1" <?php if($result['bargain_type'] == 1): ?>selected<?php endif; ?>> 现付</option>
												<option value="2" <?php if($result['bargain_type'] ==  2): ?>selected<?php endif; ?>> 到付</option>
												<option value="3" <?php if($result['bargain_type'] ==  3): ?>selected<?php endif; ?>> 月结</option>

										</select>					
									</div>
								</div>
								  <div class="layui-inline">
							      		<label class="layui-form-label " id="bargain_price_title">预计收入</label>
							      			<div class="layui-input-inline">
								  				<input id="bargain_price" type="number" name="bargain_price" value="<?php echo $result['bargain_price']; ?>"    placeholder="" autocomplete="off" class="layui-input" type="text">
							     			 </div>
							    	</div>						
									<div class="layui-inline">
										<label class="layui-form-label input-required">运输类型:</label>
										<div class="layui-input-inline">
											<select name="transportation_type" id='transportation_type'  lay-verify="required"  lay-filter="bargain_type">
											
													<option value="1" <?php if($result['transportation_type'] == 1): ?>selected<?php endif; ?>> 公路</option>
													<option value="2" <?php if($result['transportation_type'] == 2): ?>selected<?php endif; ?>> 铁路</option>
													<option value="3" <?php if($result['transportation_type'] == 3): ?>selected<?php endif; ?>> 水运</option>
													<option value="4" <?php if($result['transportation_type'] == 4): ?>selected<?php endif; ?>> 航空</option>		
													<option value="5" <?php if($result['transportation_type'] == 5): ?>selected<?php endif; ?>> 快递</option>												
											</select>												
										
										</div>			
									</div>
									
								
								  <div class="layui-inline">
							      		<label class="layui-form-label">提货时间</label>
							      			<div class="layui-input-inline">
								  				<input id="pickup_time" name="pickup_time" value="<?php if($result['pickup_time'] != ''): ?><?php echo date('Y-m-d',$result['pickup_time']); endif; ?>" placeholder="" autocomplete="off" class="layui-input" type="text">
							     			 </div>
							    	</div>	
								  <div class="layui-inline">
							      		<label class="layui-form-label">送达时间</label>
							      			<div class="layui-input-inline">
								  				<input id="send_time" name="send_time" value="<?php if($result['send_time'] != ''): ?><?php echo date('Y-m-d',$result['send_time']); endif; ?>"  placeholder="" autocomplete="off" class="layui-input" type="text">
							     			 </div>
							    	</div>								    								    						    	
								  <div class="layui-inline">
							      		<label class="layui-form-label">代收货款</label>
							      			<div class="layui-input-inline">
								  				<input id="replacement_prive" type="number" name="replacement_prive" value="<?php echo $result['replacement_prive']; ?>"   placeholder="" autocomplete="off" class="layui-input" type="text">
							     			 </div>
							    	</div>										
								  <div class="layui-inline">
							      		<label class="layui-form-label">货值</label>
							      			<div class="layui-input-inline">
								  				<input id="insurance_goods" name="insurance_goods" value="<?php echo $result['insurance_goods']; ?>"   placeholder="" autocomplete="off" class="layui-input" type="text">
							     			 </div>
							    	</div>	

								  <div class="layui-inline">
							      		<label class="layui-form-label">备注</label>
							      			<div class="layui-input-inline">
								  				<input id="remark" name="remark" value="<?php echo $result['remark']; ?>"  placeholder="" autocomplete="off" class="layui-input" type="text">
							     			 </div>
							    	</div>
							    										    								    											
							</div>
				


						</div>	
					
						
						
						<table class="layui-table">
						  <colgroup>
						    <col width="150">
						    <col width="100">
						    <col>
						  </colgroup>
						  <thead>
						    <tr>
						      <th>货物信息</th>
							   <th>下单件数</th>
						      <th>下单数量</th>
						     
						      <th>下单单位</th>
						      <th>下单重量</th>
						       <th>下单体积</th>
						       
						       <th>计费件数</th>
						      <th>计费数量</th>
						     
						       <th>计费单位</th>
						       <th>计费重量</th>           
						        <th>计费体积</th>    
						      
						        <th>操作</th>    
						    </tr> 
						  </thead>
						  <tbody class="goods-contents-body">
							  <?php if(!$result['orders_goods_info'][0]): ?>
						    <tr>
						      <td style="overflow: inherit;">
						      <div>
																	<select name="goods_id[]" lay-verify="required"  lay-filter="bargain_type">
																			<?php if(is_array($goodsResult) || $goodsResult instanceof \think\Collection || $goodsResult instanceof \think\Paginator): if( count($goodsResult)==0 ) : echo "" ;else: foreach($goodsResult as $key=>$vo): ?>
																			<option value="<?php echo $vo['goods_id']; ?>" <?php if($vo['goods_id'] == $vv['goods_id']): ?>selected<?php endif; ?>> <?php echo $vo['goods_name']; ?></option>
													
																			<?php endforeach; endif; else: echo "" ;endif; ?>
																	</select>		          
						      </div>
						      </td>
						      <td>									  				
						      	<input  name="estimated_count[]"    value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdsl" type="text">
						      </td><td>									  				
						      	<input  name="estimated_pack_count[]"    value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdsl" type="text">
						      </td>
						   <td><select name="estimated_pack_unit[]" id="">
																			<?php if(is_array($baseConfig['order']['goods_pack']) || $baseConfig['order']['goods_pack'] instanceof \think\Collection || $baseConfig['order']['goods_pack'] instanceof \think\Paginator): if( count($baseConfig['order']['goods_pack'])==0 ) : echo "" ;else: foreach($baseConfig['order']['goods_pack'] as $key2=>$vo2): ?>
																			<option value="<?php echo $key2; ?>"><?php echo $vo2; ?></option>
																			<?php endforeach; endif; else: echo "" ;endif; ?>
																		</select></td>
						   <td><input name="estimated_weight[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdzl" type="text">
													     		</td> 
																<td><input name="estimated_volume[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdzl" type="text">
													     		</td>
																
																<td>	<input  name="realy_count[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input jfsl" type="text">
													     			</td><td>	<input  name="realy_pack_count[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input jfsl" type="text">
													     			</td>
																	<td>
											
														<select name="realy_pack_unit[]" id="">
																												<?php if(is_array($baseConfig['order']['goods_pack']) || $baseConfig['order']['goods_pack'] instanceof \think\Collection || $baseConfig['order']['goods_pack'] instanceof \think\Paginator): if( count($baseConfig['order']['goods_pack'])==0 ) : echo "" ;else: foreach($baseConfig['order']['goods_pack'] as $key2=>$vo2): ?>
																												<option value="<?php echo $key2; ?>"><?php echo $vo2; ?></option>
																												<?php endforeach; endif; else: echo "" ;endif; ?>
																											</select>				
																	</td>
														<td>
															<input  name="realy_weight[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input zfzl" type="text">
															
														</td>	
															<td>
														<input  name="realy_volume[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input jftj" type="text">
																
															</td>
															
															<td><a href="javascript:void();" class="addGoods">+</a></td>
						    </tr>
							<?php endif; if(is_array($result['orders_goods_info']) || $result['orders_goods_info'] instanceof \think\Collection || $result['orders_goods_info'] instanceof \think\Paginator): if( count($result['orders_goods_info'])==0 ) : echo "" ;else: foreach($result['orders_goods_info'] as $key3=>$vv): ?>
							<tr>
							   <td style="overflow: inherit;">
							   <div>
																			<select name="goods_id[]" lay-verify="required"  lay-filter="bargain_type">
																					<?php if(is_array($goodsResult) || $goodsResult instanceof \think\Collection || $goodsResult instanceof \think\Paginator): if( count($goodsResult)==0 ) : echo "" ;else: foreach($goodsResult as $key=>$vo): ?>
																					<option value="<?php echo $vo['goods_id']; ?>" <?php if($vo['goods_id'] == $vv['goods_id']): ?>selected<?php endif; ?>> <?php echo $vo['goods_name']; ?></option>
															
																					<?php endforeach; endif; else: echo "" ;endif; ?>
																			</select>		          
							   </div>
							   </td>
							   <td>									  				
							   	<input  name="estimated_count[]"    value="<?php echo $vv['estimated_count']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdsl" type="text">
							   </td> <td>									  				
							   	<input  name="estimated_pack_count[]"    value="<?php echo $vv['estimated_pack_count']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdsl" type="text">
							   </td>
							<td><select name="estimated_pack_unit[]" id="">
																					<?php if(is_array($baseConfig['order']['goods_pack']) || $baseConfig['order']['goods_pack'] instanceof \think\Collection || $baseConfig['order']['goods_pack'] instanceof \think\Paginator): if( count($baseConfig['order']['goods_pack'])==0 ) : echo "" ;else: foreach($baseConfig['order']['goods_pack'] as $key2=>$vo2): ?>
																					<option value="<?php echo $key2; ?>" <?php if($key2==$vv['estimated_pack_unit']): ?> selected="selected"<?php endif; ?>><?php echo $vo2; ?></option>
																					<?php endforeach; endif; else: echo "" ;endif; ?>
																				</select></td>
							<td><input name="estimated_weight[]" value="<?php echo $vv['estimated_weight']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdzl" type="text">
															     		</td> 
																		<td><input name="estimated_volume[]" value="<?php echo $vv['estimated_volume']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdzl" type="text">
															     		</td>
																		
																		<td>	<input  name="realy_count[]" value="<?php echo $vv['realy_count']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input jfsl" type="text">
															     			</td><td>	<input  name="realy_pack_count[]" value="<?php echo $vv['realy_count']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input jfsl" type="text">
															     			</td>
																			<td>
													
																<select name="realy_pack_unit[]" id="">
																														<?php if(is_array($baseConfig['order']['goods_pack']) || $baseConfig['order']['goods_pack'] instanceof \think\Collection || $baseConfig['order']['goods_pack'] instanceof \think\Paginator): if( count($baseConfig['order']['goods_pack'])==0 ) : echo "" ;else: foreach($baseConfig['order']['goods_pack'] as $key2=>$vo2): ?>
																														<option value="<?php echo $key2; ?>" <?php if($key2==$vv['realy_pack_unit']): ?> selected="selected"<?php endif; ?>><?php echo $vo2; ?></option>
																														<?php endforeach; endif; else: echo "" ;endif; ?>
																													</select>				
																			</td>
																<td>
																	<input  name="realy_weight[]" value="<?php echo $vv['realy_weight']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input zfzl" type="text">
																	
																</td>	
																	<td>
																<input  name="realy_volume[]" value="<?php echo $vv['realy_volume']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input jftj" type="text">
																		
																	</td>
																	
																	<td>
																		<?php if($key3==0): ?>
																		<a href="javascript:void();" class="addGoods">+</a>
																		<?php else: ?>
																		<a href="javascript:void();" class="delGoods">-</a>
																		<?php endif; ?>
																		</td>
							 </tr>
							
							<?php endforeach; endif; else: echo "" ;endif; ?>
							
						  </tbody>
						</table>
						
						</div>
					 <h3>收入核算备注</h3>
						<textarea name="income_remark" style="width:500px;height:50px"><?php echo $income_remark; ?></textarea>
						
 <h3>财务信息</h3>
 
 
 
 
 <div class="layui-form-label income_add" style="cursor: pointer;">
 	添加费用
 </div>
			<div class="layui-form-item income_content">
				
				<?php if(is_array($orders_income) || $orders_income instanceof \think\Collection || $orders_income instanceof \think\Paginator): $i = 0; $__LIST__ = $orders_income;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo3): $mod = ($i % 2 );++$i;?>
				
				
				
				<div class="layui-inline">
						
							<div class="o-flex" >
								<select name="cost_id[]" class="layui-input">
									<?php if(is_array($all_income) || $all_income instanceof \think\Collection || $all_income instanceof \think\Paginator): $i = 0; $__LIST__ = $all_income;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo2): $mod = ($i % 2 );++$i;?>
									<option value="<?php echo $vo2['cost_id']; ?>" <?php if($vo2['cost_id']==$vo3['cost_id']): ?> selected="selected"<?php endif; ?> ><?php echo $vo2['cost_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
									
								</select>
								<input name="cost_value[]" value="<?php echo $vo3['money']; ?>"  lay-verify="required" placeholder="请输入费用" autocomplete="off" class="layui-input" type="number" style="width:100px" >
								<input name="remark[]" value="<?php echo $vo3['remark']; ?>"  lay-verify="required" placeholder="请输入备注" autocomplete="off" class="layui-input" type="text" style="width:100px" >
										
											<div class="close">
												X
											</div>
																 </div>
				</div>
				
				
				
				<?php endforeach; endif; else: echo "" ;endif; ?>
				
			</div>

						
				<input type='hidden' id='orders_id' name='orders_id'  value="<?php echo $result['orders_id']; ?>" />
				<input type='hidden' id='project_id' name='project_id'  value="<?php echo \think\Request::instance()->get('project_id'); ?>" />
				<div class="layui-form-item">
					<div class="layui-input-block all-button-center">
						<div class="layui-btn" onclick="orderIncomeAdd()">提交</div>
						
					</div>
				</div>
			</form>
		</div>
		<hr/>
		
		
	</div>
					

</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
<script src='/static/javascript/system/customer.js'></script>

</body>
</html>
<script>

	layui.use('element', function(){
		var element = layui.element;
	});
	var isShow = true;  //定义⼀个标志位
	$('.kit-side-fold').click(function(){
		//选择出所有的span，并判断是不是hidden
		$('.layui-nav-item span').each(function(){
			if($(this).is(':hidden')){
				$(this).show();
			}else{
				$(this).hide();
			}
		});
		//判断isshow的状态
		if(isShow){
			$('.layui-side.layui-bg-black').width(50); //设置宽度
			$('.kit-side-fold i').css('margin-right', '70%');  //修改图标的位置
			//将footer和body的宽度修改
			$('.layui-body').css('left', 60+'px');
			$('.layui-footer').css('left', 60+'px');
			//将⼆级导航栏隐藏
			$('dd span').each(function(){
				$(this).hide();
			});
			//修改标志位
			isShow =false;
		}else{
			$('.layui-side.layui-bg-black').width(200);
			$('.kit-side-fold i').css('margin-right', '10%');
			$('.layui-body').css('left', 200+'px');
			$('.layui-footer').css('left', 200+'px');
			$('dd span').each(function(){
				$(this).show();
			});
			isShow =true;
		}
	});

layui.use(['laydate','element','form'], function(){
    var laydate = layui.laydate;
    var element = layui.element;
    var formSelects = layui.formSelects;
    var upload=layui.upload;
    laydate.render({
        elem: '#pickup_time',
        trigger: 'click',
        lang: 'zn'
    });
    laydate.render({
        elem: '#send_time',
        trigger: 'click',//解决一闪而过的问题
        lang: 'zn'
    });
  
    
    $(document).on("click",".addGoods",function(){
    	$('#goodsList').append($('#hiddenAddGoods').html())
    	form.render('select');
    })
    $(document).on("click",".delGoods",function(){
    	$(this).parent().remove()
    	
    })
    

});




</script>

<script type="text/javascript">
var income_index=1000;

$(".income_add").click(function(){
	income_index++;
	$(".income_content").append(`	<div class="layui-inline">
			
				<div class="o-flex" >
					<select name="cost_id[]" class="layui-input">
						<?php if(is_array($all_income) || $all_income instanceof \think\Collection || $all_income instanceof \think\Paginator): $i = 0; $__LIST__ = $all_income;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo2): $mod = ($i % 2 );++$i;?>
						<option value="<?php echo $vo2['cost_id']; ?>"><?php echo $vo2['cost_name']; ?></option>
						<?php endforeach; endif; else: echo "" ;endif; ?>
				
					</select>
					<input name="cost_value[]" value=""  lay-verify="required" placeholder="请输入费用" autocomplete="off" class="layui-input" type="number" style="width:100px" >
					<input name="remark[]" value=""  lay-verify="required" placeholder="请输入备注" autocomplete="off" class="layui-input" type="text" style="width:100px" >
							
								<div class="close">
									X
								</div>
													 </div>
	</div>`);
	form.render('select');
})

$(document).on('click',".close",function(){
	$(this).parent().parent().remove();
})


function orderIncomeAdd(){
	
	var da_array=$("form").serializeArray();
	console.log(da_array)
	da_array.orders_id=<?php echo $think['get']['orders_id']; ?>
 $.ajax({
	url:"/order/addIncomeAjax",
 data:da_array,
 success:function(e){
		console.log(e)
		if(e.code==200)
		{window.parent.layer.closeAll();//window.location.href="/order/incomeAccountingManage";
		window.parent.global_table.reload();
		}
		else
		layer.msg("财务已审核不能修改")
	},
	})
	 
	
	return false;
}


 $(document).on("click",".addGoods",function(){
    	/* $('#goodsList').append($('#hiddenAddGoods').html())
    	form.render('select'); */
		
		$(".goods-contents-body").append(`<tr>
          <td style="overflow: inherit;">
          <div>
												<select name="goods_id[]" lay-verify="required"  lay-filter="bargain_type">
														<?php if(is_array($goodsResult) || $goodsResult instanceof \think\Collection || $goodsResult instanceof \think\Paginator): if( count($goodsResult)==0 ) : echo "" ;else: foreach($goodsResult as $key=>$vo): ?>
														<option value="<?php echo $vo['goods_id']; ?>" <?php if($vo['goods_id'] == $vv['goods_id']): ?>selected<?php endif; ?>> <?php echo $vo['goods_name']; ?></option>
								
														<?php endforeach; endif; else: echo "" ;endif; ?>
												</select>		          
          </div>
          </td>
          <td>									  				
          	<input  name="estimated_count[]"    value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdsl" type="text">
          </td>
       <td><select name="estimated_pack_unit[]" id="">
														<?php if(is_array($baseConfig['order']['goods_pack']) || $baseConfig['order']['goods_pack'] instanceof \think\Collection || $baseConfig['order']['goods_pack'] instanceof \think\Paginator): if( count($baseConfig['order']['goods_pack'])==0 ) : echo "" ;else: foreach($baseConfig['order']['goods_pack'] as $key2=>$vo2): ?>
														<option value="<?php echo $key2; ?>"><?php echo $vo2; ?></option>
														<?php endforeach; endif; else: echo "" ;endif; ?>
													</select></td>
	   <td><input name="estimated_weight[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdzl" type="text">
								     		</td> 
											<td><input name="estimated_volume[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdzl" type="text">
								     		</td>
											
											<td>	<input  name="realy_count[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input jfsl" type="text">
								     			</td>
												<td>
						
									<select name="realy_pack_unit[]" id="">
																							<?php if(is_array($baseConfig['order']['goods_pack']) || $baseConfig['order']['goods_pack'] instanceof \think\Collection || $baseConfig['order']['goods_pack'] instanceof \think\Paginator): if( count($baseConfig['order']['goods_pack'])==0 ) : echo "" ;else: foreach($baseConfig['order']['goods_pack'] as $key2=>$vo2): ?>
																							<option value="<?php echo $key2; ?>"><?php echo $vo2; ?></option>
																							<?php endforeach; endif; else: echo "" ;endif; ?>
																						</select>				
												</td>
									<td>
										<input  name="realy_weight[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input zfzl" type="text">
										
									</td>	
										<td>
									<input  name="realy_volume[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input jftj" type="text">
											
										</td>
										
										<td><a href="javascript:void();" class="delGoods">-</a></td>
        </tr>`);
		form.render('select');
		
    })
    $(document).on("click",".delGoods",function(){
    	/* $(this).parent().remove() */
		
		$(this).parents('tr').remove()
		/* if($(".goods-contents-body tr").length>1)
    	$(".goods-contents-body tr:last-child").remove(); */
    })

</script>

