<?php

namespace app\index\controller;
use app\common\help\Contents;
use think\Validate;
use \Underscore\Types\Arrays;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
use app\common\help\Help;


class Equipment extends Base
{
    /**
     *  圣华
     *  设备
     */
    public function index(){
	
        return $this->fetch('index');
    }

    /**
     *  圣华
     *  智能设备管理
     */
    public function equipmentManage(){
        return $this->fetch('show_equipment_manage');
    }
	public function addEquipment(){
		
		return $this->fetch('add_equipment');
	}
    public function showEquipmentHistoryManage(){

        //获取历史定位数据
        $params = Request::instance()->param();

        $result = $this->callSoaErp('post','/equipment/getEquipmentHistory',$params);
//        var_dump($result);exit;

        $this->assign('equimentResult', $result['data']);

        return $this->fetch('show_equipment_history_manage');
    }

    public function showEquipmentMap(){
        return $this->fetch('show_equipment_map');
    }
	
    /**
     *  圣华
     *  获取设备动态表格数据
     */
    public function getEquipmentAjax(){

        $params = Request::instance()->param();

        $data=[
            'page'=>$params['page'],
            'page_size'=>$params['limit'],

        ];



        $result = $this->callSoaErp('post','/equipment/getEquipment',$data);
//        return $result;
		for($i=0;$i<count($result['data']['list']);$i++){
			$result['data']['list'][$i]['equipment_type_name']=Contents::baseConfig()['shebei']['type'][$result['data']['list'][$i]['equipment_type']];
			
			$result['data']['list'][$i]['create_time'] = date('Y-m-d H:i:s',$result['data']['list'][$i]['create_time']);
		}

        $r = [
            'code'=>200,
            'data'=>$result['data']['list'],
            'count'=>	$result['data']['count'],

        ];
        return $r;
    }
	public function addEquipmentAjax(){
		
		$params = Request::instance()->param();		
	
    	if(is_numeric($data['equipment_id'])){   
		
    		$result = $this->callSoaErp('post', '/equipment/updateEquipment',$params);
			
    	}else{ 
			
    		$result = $this->callSoaErp('post','/equipment/addEquipment',$params);
				
    	}		
		
	}

}