<?php
namespace app\index\controller;

use app\common\help\Help;
use think\Controller;

class Qimen extends Controller
{
    private $appSecret  = '55757445c78741beac2ba6eb3d354336';

    protected function _init()
    {

    }

    /**
     * 奇门请求
     * Author: Kison.Han
     */
    public function encrypt_curl($url, $post = [], $get = [], $secret )
    {
//        if (!$get) {
//            return false;
//        }
        ksort($get);
        $string = $secret;

        foreach ($get as $key => $value) {
            $string .= $key . $value;
            $url    .= $key . '=' . $value . '&';
        }
        $string     .= json_encode($post) . $secret;
        $sign       = strtoupper(sha1($string));//$this->generateSign($get);
        $url        .= 'sign=' . $sign;

        $respData = Help::http("post",$url,$post);
//        $client     = new Client();
//        $response = $client->request('post', $url, [
//            'json' => $post,
//            'verify' => false,
//        ]);
//        $respString = $response->getBody()->getContents();
        $respData   = json_decode($respData, true);
        return $respData;
    }

    /**
     * 奇门加密-生成sign
     * Author: Kison.Han
     */
    public function generateSign($data)
    {
        ksort($data);

        $str = $this->appSecret;
        foreach ($data as $key => $datum) {
            $str .= $key . $datum;
        }
        $str .= $this->appSecret;

        $sign = strtoupper(sha1($str));

        return $sign;
    }
}