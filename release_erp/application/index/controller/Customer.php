<?php
/**
 * Created by PhpStorm.
 * User: 胡
 * Date: 20190/02/19
 * Time: 16:40
 */

namespace app\index\controller;
use \Underscore\Types\Arrays;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
use app\common\help\Help;

class Customer extends Base
{
	/**
	 *  圣华
	 *  客户管理显示页面
	 */
	public function showCustomerManage(){
		//读取所有的地接信息
	
		$company_name = input("company_name");

		$status = input("status");
		$data = [
				'page'=>$this->page(),
				'page_size'=>$this->_page_size,
	
	
		];
		if(!empty($company_name)){
			$data['company_name'] = $company_name;
		}
		if(is_numeric($status)){
			$data['status'] = $status;
		}

		$result = $this->callSoaErp('post','/source/getCustomer',$data);

		$this->getPageParams($result);
		return $this->fetch('customer_manage');
			
			
	}
	
	/**
	 * 客户管理显示新增页面
	 */
	public function showCustomerAdd(){
	
		$customer_id = input('customer_id');
		if(is_numeric($customer_id)){
			
			$data['customer_id'] = $customer_id;
			$result = $this->callSoaErp('post','/source/getCustomer',$data);
			$this->assign('result',$result['data'][0]);
			
		}
		
		
		
		return $this->fetch('customer_add');
	}
	
	/**
	 * 客户新增数据
	 */
	public function addCustomerAjax(){
		$data = Request::instance()->param();
		

		if(is_numeric($data['customer_id'])){
	
	
			$result = $this->callSoaErp('post', '/source/updateCustomerByCustomerId',$data);
		}else{
		
			$result = $this->callSoaErp('post', '/source/addCustomer',$data);
		}

		return   $result;//['code' => '400', 'msg' => $data];
	}
	/**
	 *  圣华
	 *  项目管理显示页面
	 */
	public function showProjectManage(){
		//读取所有的地接信息
		$zjm = input("zjm");
		$project_name = input("project_name");
		
		$status = input("status");
		
		$data = [
				'page'=>$this->page(),
				'page_size'=>$this->_page_size,
	
	
		];

		if(is_numeric($status)){
			$data['status'] = $status;
		}
		if(!empty($zjm)){
			$data['zjm'] = $zjm;
		}
		if(is_numeric($project_name)){
			$data['project_name'] = $project_name;
		}
		
		$result = $this->callSoaErp('post','/source/getProject',$data);
	
		$this->getPageParams($result);
		return $this->fetch('project_manage');
			
			
	}
	/**
	 * 项目新增页面
	 */
	public function showProjectAdd(){
	
		$project_id = input('project_id');
		$customerData['status'] = 1;
		$customerResult = $this->callSoaErp('post','/source/getCustomer',$customerData);

		$this->assign('customerResult',$customerResult['data']);
				
		
		if(is_numeric($project_id)){
			$projectData['project_id'] = $project_id;
			$customerResult = $this->callSoaErp('post','/source/getProject',$projectData);

			$this->assign('result',$customerResult['data'][0]);
		}
		
	
	
	
		return $this->fetch('project_add');
	}		
	
	/**
	 * 项目新增AJAX
	 */
	public function addProjectAjax(){
		$data = Request::instance()->param();
	

		if(is_numeric($data['project_id'])){
	
	
			$result = $this->callSoaErp('post', '/source/updateProjectByProjectId',$data);
		}else{
	
			$result = $this->callSoaErp('post', '/source/addProject',$data);
		}

		return   $result;//['code' => '400', 'msg' => $data];
	}
	/**
	 *  圣华
	 *  收货管理显示页面
	 */
	public function showAcceptGoodsManage(){
		//读取所有的项目


		$projectResult = $this->callSoaErp('post','/source/getProject',$data);
		$this->assign('projectResult',$projectResult['data']);
		
		$data = [
				'page'=>$this->page(),
				'page_size'=>$this->_page_size,


		];
		if(is_numeric(input('project_id'))){
				
			$data['project_id'] = input('project_id');
		}	
		$status = input('status');
		if(is_numeric($status)){
			$data['status'] = $status;
		}
		if(is_numeric(input('accept_goods_cellphone'))){
			$data['accept_goods_cellphone'] = input('accept_goods_cellphone');
		}
		if(!empty(input('accept_goods_name'))){
			$data['accept_goods_name'] = input('accept_goods_name');
		}
	
		$result = $this->callSoaErp('post','/source/getAcceptGoods',$data);
	
		$this->getPageParams($result);
		return $this->fetch('accept_goods_manage');
			
			
	}
	/**
	 *  圣华
	 *  获取收货信息
	 */
	public function showAcceptGoodsAjax(){
			$data = Request::instance()->param();
			$goods['accept_goods_id'] = $data['accept_goods_id'];
			$result = $this->callSoaErp('post','/source/getAcceptGoods',$data);
			
			return $result;
	}	
	/**
	 *  圣华
	 *  获取发货信息
	 */
	public function showSendGoodsAjax(){
		$data = Request::instance()->param();
	
		$goods['send_goods_id'] = $data['send_goods_id'];
		$result = $this->callSoaErp('post','/source/getSendGoods',$data);

		return $result;
	}
	/**
	 * 收货新增页面
	 */
	public function showAcceptGoodsAdd(){
		
	
		
		$project_id = input('project_id');
		$accept_goods_id = input('accept_goods_id');
		//获取所有城市
		

		$city = Help::getCity('-1');


		
	
		$this->assign('sheng',$city);					
		$projectResult = $this->callSoaErp('post','/source/getProject',[]);



		$this->assign('projectResult',$projectResult['data']);
		

	
	
		if(is_numeric($accept_goods_id)){
			$projectData['accept_goods_id'] = $accept_goods_id;
			$customerResult = $this->callSoaErp('post','/source/getAcceptGoods',$projectData);



			$this->assign('result',$customerResult['data'][0]);
			
			//通过拿到的省再去拿市数据
			$cityResult = Help::getCity($customerResult['data'][0]['province_id']);
			$this->assign('cityResult',$cityResult);
			//通过拿到的市再去区
			$areaResult = Help::getCity($customerResult['data'][0]['city_id']);
			
	
			$this->assign('areaResult',$areaResult);			
		}
	
	
	
	
		return $this->fetch('accept_goods_add');
	}
	
	/**
	 * 收货新增AJAX
	 */
	public function addAcceptGoodsAjax(){
		$data = Request::instance()->param();

        //echo json_encode($data,256);exit();

		if(is_numeric($data['accept_goods_id'])){
			$result = $this->callSoaErp('post', '/source/updateAcceptGoodsByAcceptGoodsId',$data);
		}else{
	
			$result = $this->callSoaErp('post', '/source/addAcceptGoods',$data);
		}
		

		return   $result;//['code' => '400', 'msg' => $data];
	}	
	/**
	 *  发货管理显示页面
	 */
	public function showSendGoodsManage(){
		//读取所有的项目
	
	
		$projectResult = $this->callSoaErp('post','/source/getProject',$data);
		$this->assign('projectResult',$projectResult['data']);
	
		$data = [
				'page'=>$this->page(),
				'page_size'=>$this->_page_size,
	
	
		];
		if(is_numeric(input('project_id'))){
	
			$data['project_id'] = input('project_id');
		}
		$status = input('status');
		if(is_numeric($status)){
			$data['status'] = $status;
		}
		if(is_numeric(input('send_goods_cellphone'))){
			$data['send_goods_cellphone'] = input('send_goods_cellphone');
		}
		if(is_numeric(input('send_goods_name'))){
			$data['send_goods_name'] = input('send_goods_name');
		}
	
		$result = $this->callSoaErp('post','/source/getSendGoods',$data);
	
		$this->getPageParams($result);
		return $this->fetch('send_goods_manage');
			
			
	}
	/**
	 * 发货新增页面
	 */
	public function showSendGoodsAdd(){
	
	
	
		$project_id = input('project_id');
		$send_goods_id = input('send_goods_id');
		//获取所有城市
	
	
		$city = Help::getCity('-1');
	
	
	
		$this->assign('sheng',$city);
	
		//获取所有项目
	
	
		$projectResult = $this->callSoaErp('post','/source/getProject',[]);
	
		$this->assign('projectResult',$projectResult['data']);
	
	
	
	
		if(is_numeric($send_goods_id)){
			$projectData['send_goods_id'] = $send_goods_id;
			$customerResult = $this->callSoaErp('post','/source/getSendGoods',$projectData);
	
			$this->assign('result',$customerResult['data'][0]);
				
			//通过拿到的省再去拿市数据
			$cityResult = Help::getCity($customerResult['data'][0]['province_id']);
			$this->assign('cityResult',$cityResult);
			//通过拿到的市再去区
			$areaResult = Help::getCity($customerResult['data'][0]['city_id']);
				
	
			$this->assign('areaResult',$areaResult);
		}
	
	
	
	
		return $this->fetch('send_goods_add');
	}
	
	/**
	 * 发货新增AJAX
	 */
	public function addSendGoodsAjax(){
		$data = Request::instance()->param();

	
		if(is_numeric($data['send_goods_id'])){
	
	
			$result = $this->callSoaErp('post', '/source/updateSendGoodsBySendGoodsId',$data);
		}else{
	
			$result = $this->callSoaErp('post', '/source/addSendGoods',$data);
		}
	
		
	
		return   $result;//['code' => '400', 'msg' => $data];
	}	
	/**
	 *  货物显示页面
	 */
	public function showGoodsManage(){
		//读取所有的项目
	
	
		$projectResult = $this->callSoaErp('post','/source/getProject',$data);
		$this->assign('projectResult',$projectResult['data']);
	
		$data = [
				'page'=>$this->page(),
				'page_size'=>$this->_page_size,
	
	
		];
		if(is_numeric(input('project_id'))){
	
			$data['project_id'] = input('project_id');
		}
		$status = input('status');
		if(is_numeric($status)){
			$data['status'] = $status;
		}

		if(!empty(input('goods_name'))){
			$data['goods_name'] = input('goods_name');
		}
		if(is_numeric(input('goods_packaging_type'))){
			$data['goods_packaging_type'] = input('goods_packaging_type');
		}	
		$result = $this->callSoaErp('post','/source/getGoods',$data);
	
		$this->getPageParams($result);
		return $this->fetch('goods_manage');
			
			
	}
	/**
	 * 货物新增页面
	 */
	public function showGoodsAdd(){
	
	
	
		$project_id = input('project_id');
		$goods_id = input('goods_id');
		//获取所有城市
	
	

	
		//获取所有项目
	
	
		$projectResult = $this->callSoaErp('post','/source/getProject',[]);
	
		$this->assign('projectResult',$projectResult['data']);
	
	
	
	
		if(is_numeric($goods_id)){
			$projectData['goods_id'] = $goods_id;
			$goodsResult = $this->callSoaErp('post','/source/getGoods',$projectData);
	
			$this->assign('result',$goodsResult['data'][0]);
	

		}
	
	
	
	
		return $this->fetch('goods_add');
	}
	
	/**
	 * 货物新增AJAX
	 */
	public function addGoodsAjax(){
		$data = Request::instance()->param();
	
		
		if(is_numeric($data['goods_id'])){
	
	
			$result = $this->callSoaErp('post', '/source/updateGoodsByGoodsId',$data);
		}else{
	
			$result = $this->callSoaErp('post', '/source/addGoods',$data);
		}

	
		return   $result;//['code' => '400', 'msg' => $data];
	}	
}