<?php


namespace app\index\model\despatch;


use app\index\model\Base;
use app\index\model\system\User;

class DespatchAbnormal extends Base
{

    protected $table="despatch_abnormal";


    public function getAbnormal($params){
        $where=[];
        $user_model=new User();
       $data= $this->pubGetData($where);
       foreach ($data['list'] as $k=>$v)
       {
          $user_info= $user_model->where('user_id',$v['user_id'])->find();
           $data['list'][$k]['user_name']=$user_info->nickname;
       }


       return $data;
    }

    public function addAbnormal($params){
     $data=[];
     $data['abnormal_type']=$params['abnormal_type'];
     $data['table_id']=$params['table_id'];
     $data['remark']=$params['remark'];
     $data['solve_way']=$params['solve_way'];
     $data['abnormal_money']=$params['abnormal_money'];
     $data['user_id']=$params['user_id'];
     $data['create_time']=time();
     $res=$this->pubInsert($data);

     $ab_info=$this->where('abnormal_id',$res)->find();
     $ab_info->abnormal_number='ab'.$res;
     $ab_info->save();

     return true;

    }

}