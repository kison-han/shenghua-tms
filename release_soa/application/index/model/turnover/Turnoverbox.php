<?php

namespace app\index\model\turnover;

use app\common\help\Contents;
use app\common\help\Transport as transportHelp;
use app\index\model\dispatch\Dispatch;
use app\index\model\dispatch\Dispatch as Dispatchmodel;
use app\index\model\dispatch\DispathGoods;
use app\index\model\orders\Ordersgoods;
use app\index\model\system\User;
use think\Model;
use app\common\help\Help;
use app\index\service\PublicService;

use think\config;
use think\Db;

class Turnoverbox extends Model
{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'transport';
    private $_public_service;



    public function getTurnoverBoxLog($params)
    {
    	$data = "1=1 ";
    	//团队产品ID
    	if(isset($params['begin_time'])){
    		$data.= " and create_time>".$params['begin_time'];
    	}
    	if(isset($params['end_time'])){
    		$data.= " and create_time <".$params['end_time'];
    	}
    
    	$result =  $this->table("device_box_log")->where($data)->
    	field(['device_box_log.*'])->select();

    	return $result;

    }
    public function getTurnoverBox($params)
    {

    
    	$result =  $this->table("device_box")->where($data)->select();
    
    	return $result;
    
    }
    public function getTransportById($params)
    {
        $transport_id = $params['transport_id'];
        $data = $this->where('transport_id', $transport_id)->find();
        $dispatchInfo = Dispatch::where('transport_number', $data['transport_number'])->order('dispatch_type', 'asc')->select();
        foreach ($dispatchInfo as $k => $v) {
            $v['goods'] = DispathGoods::where('dispatch_id', $v['dispatch_id'])->select();
        }
        $data['dispatch_info'] = $dispatchInfo;

        return ['list' => $data];
    }


    public function getTransport($params, $openCount)
    {
        $where = [];
        if ($openCount) {
            $page = isset($params['page']) ? $params['page'] : 1;
            $pageSize = isset($params['$pageSize']) ? $params['$pageSize'] : Contents::PAGE_SIZE;
            $data['count'] = $this->where($where)->count();
            $data['page_count'] = ceil($data['count'] / $pageSize);
            $data['list'] = $this->where($where)->page($page, $pageSize)->order('transport_id', 'desc')
                ->select();
            return $data;
        } else
            return $this->where($where)->order('transport_id', 'desc')->select();

    }
    protected function base($query)
    {
        $query->where('status',1);
    }

    public function initialize()
    {
        $this->_public_service = new PublicService();
        parent::initialize();

    }

    public function getCreateUserAttr($value, $data)
    {
        $userid = $data['create_user_id'];
        $userinfo = User::get($userid);
        return $userinfo['nickname'];
    }

    public function getUpdateUserAttr($value, $data)
    {
        $userid = $data['update_user_id'];
        $userinfo = User::get($userid);
        return $userinfo['nickname'];
    }

    //转换省市区
    public function getAcceptProvinceAttr($value, $data)
    {
        return Help::cityIdToNname($data['accept_province_id'])['city_name'];
    }

    public function getAcceptCityAttr($value, $data)
    {
        return Help::cityIdToNname($data['accept_city_id'])['city_name'];
    }

    public function getAcceptAreaAttr($value, $data)
    {
        return Help::cityIdToNname($data['accept_area_id'])['city_name'];
    }

    public function getSendProvinceAttr($value, $data)
    {
        return Help::cityIdToNname($data['accept_province_id'])['city_name'];
    }

    public function getSendCityAttr($value, $data)
    {
        return Help::cityIdToNname($data['accept_city_id'])['city_name'];
    }

    public function getSendAreaAttr($value, $data)
    {
        return Help::cityIdToNname($data['accept_area_id'])['city_name'];
    }


    //运单类型转换
    public function getTransportTypeTextAttr($value, $data)
    {
        $status = Contents::transporType();
        return $status[$data['transport_type']];
    }

    //运单状态转换
    public function getStatusTextAttr($value, $data)
    {
        $status = [0 => '删除', 1 => '正常'];
        return $status[$data['status']];
    }

//时间处理
    public function getUpdateTimeTextAttr($value, $data)
    {
        $time = $data['update_time'];
        return date('Y-m-d H:i:s', $time);
    }

    //时间处理
    public function getCreateTimeTextAttr($value, $data)
    {
        $time = $data['create_time'];
        return date('Y-m-d H:i:s', $time);
    }

    /**
     * 添加订单
     * 胡
     */
    public function addTransport($params)
    {


        Db::startTrans();
        try {

            $id = $this->insertGetId($params);
            $transport_replace = $this->get($id);
            $transport_replace->save(['transport_number' => 'T-' . $id]);
            Db::name('Orders')->whereIn('orders_number', $params['order_number'])->update(['order_status' => 2]);

            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
            //\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            //exit();

        }

        return $id;
    }

    /**
     * 获取订单
     * 胡
     */
    public function getOrder($params, $is_count = false, $is_page = false, $page = null, $page_size = 20)
    {
        $data = "1=1 ";


        if (!empty($params['project_id'])) {
            $data .= " and orders.project_id =" . $params['project_id'];

        }
        if (!empty($params['accept_name'])) {
            $data .= ' and orders.accept_name like "%' . $params['accept_name'] . '%"';
        }
        if (!empty($params['accept_cellphone'])) {
            $data .= ' and orders.accept_cellphone like "%' . $params['accept_cellphone'] . '%"';
        }
        if (is_numeric($params['order_status'])) {
            $data .= " and orders.order_status =" . $params['order_status'];

        }

        if (is_numeric($params['orders_id_is_like']) && $params['orders_id_is_like'] == 1) {
            $data .= " and orders.orders_id in (" . $params['orders_id'] . ")";

        } else {
            if (is_numeric($params['orders_id'])) {
                $data .= " and orders.orders_id =" . $params['orders_id'];
            }
        }


        if ($is_count == true) {
            $result = $this->table("orders")->where($data)->count();
        } else {
            if ($is_page == true) {
                $result = $this->table("orders")->

                join("project", 'project.project_id= orders.project_id')->
                where($data)->limit($page, $page_size)->order('orders.create_time desc')->
                field(['orders.*', 'project.project_name',
                    "(select city_name  from city where city.city_id= orders.send_province_id)" => 'send_province_name',
                    "(select city_name  from city where city.city_id= orders.send_city_id)" => 'send_city_name',
                    "(select city_name  from city where city.city_id= orders.send_area_id)" => 'send_area_name',
                    "(select city_name  from city where city.city_id= orders.accept_province_id)" => 'accept_province_name',
                    "(select city_name  from city where city.city_id= orders.accept_city_id)" => 'accept_city_name',
                    "(select city_name  from city where city.city_id= orders.accept_area_id)" => 'accept_area_name',
                    "(select nickname  from user where user.user_id = orders.create_user_id)" => 'create_user_name',
                    "(select nickname  from user where user.user_id = orders.update_user_id)" => 'update_user_name',

                ])->select();


            } else {
                $result = $this->table("orders")->alias('orders')->

                join("project", 'project.project_id= orders.project_id')->
                where($data)->order('orders.create_time desc')->
                field(['orders.*', 'project.project_name',
                    "(select city_name  from city where city.city_id= orders.send_province_id)" => 'send_province_name',
                    "(select city_name  from city where city.city_id= orders.send_city_id)" => 'send_city_name',
                    "(select city_name  from city where city.city_id= orders.send_area_id)" => 'send_area_name',
                    "(select city_name  from city where city.city_id= orders.accept_province_id)" => 'accept_province_name',
                    "(select city_name  from city where city.city_id= orders.accept_city_id)" => 'accept_city_name',
                    "(select city_name  from city where city.city_id= orders.accept_area_id)" => 'accept_area_name',
                    "(select nickname  from user where user.user_id = orders.create_user_id)" => 'create_user_name',
                    "(select nickname  from user where user.user_id = orders.update_user_id)" => 'update_user_name',

                ])->select();
            }
        }


        return $result;

    }

    /**
     * 修改发货方
     */
    public function updateOrderByOrderId($params)
    {

        $t = time();
        $data['project_id'] = $params['project_id'];
        $data['pay_type'] = $params['pay_type'];//支付方式
        $data['bargain_type'] = $params['bargain_type'];
        if ($data['bargain_type'] == 2) {
            $data['bargain_price'] = $params['bargain_price'];
        } else {
            $data['bargain_price'] = 0;
        }


        $data['transportation_type'] = $params['transportation_type'];


        $data['send_province_id'] = $params['send_province_id'];
        $data['send_city_id'] = $params['send_city_id'];
        $data['send_area_id'] = $params['send_area_id'];
        $data['send_address'] = $params['send_address'];
        $data['send_name'] = $params['send_name'];
        $data['send_cellphone'] = $params['send_cellphone'];

        $data['accept_province_id'] = $params['accept_province_id'];
        $data['accept_city_id'] = $params['accept_city_id'];
        $data['accept_area_id'] = $params['accept_area_id'];
        $data['accept_address'] = $params['accept_address'];
        $data['accept_name'] = $params['accept_name'];
        $data['accept_cellphone'] = $params['accept_cellphone'];
        //客户单号

        $data['customer_order_number'] = $params['customer_order_number'];


        //代收货款

        $data['replacement_prive'] = $params['replacement_prive'];


        //保险货值

        $data['insurance_goods'] = $params['insurance_goods'];


        //提货时间
        if ($params['pickup_time'] == '') {
            $data['pickup_time'] = null;

        }


        //送货时间
        if ($params['send_time'] == '') {
            $data['send_time'] = null;

        }


        $data['remark'] = $params['remark'];


        $data['update_user_id'] = $params['user_id'];
        $data['update_time'] = $t;


        $source_price = [];
        Db::startTrans();
        try {
            Db::name('orders')->where("orders_id = " . $params['orders_id'])->update($data);
            Db::name('orders_goods')->where("orders_id = " . $params['orders_id'])->update(['status'=>0]);
            for ($i = 0; $i < count($params['goods_id']); $i++) {
                $orders_goods = [];
                $orders_goods['orders_id'] = $params['orders_id'];
                $orders_goods['goods_id'] = $params['goods_id'][$i];
                $orders_goods['estimated_count'] = $params['estimated_count'][$i];
                $orders_goods['estimated_pack_count'] = $params['estimated_pack_count'][$i];
                $orders_goods['estimated_weight'] = $params['estimated_weight'][$i];
                $orders_goods['estimated_volume'] = $params['estimated_volume'][$i];
                $orders_goods['realy_count'] = $params['realy_count'][$i];
                $orders_goods['realy_pack_count'] = $params['realy_pack_count'][$i];
                $orders_goods['realy_weight'] = $params['realy_weight'][$i];
                $orders_goods['realy_volume'] = $params['realy_volume'][$i];
                $orders_goods['create_time'] = $t;
                $orders_goods['create_user_id'] = $params['user_id'];
                $orders_goods['update_time'] = $t;
                $orders_goods['update_user_id'] = $params['user_id'];
                $orders_goods['status'] = 1;

                Db::name('orders_goods')->insertGetId($orders_goods);
            }
            $result = 1;
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();

        }
        return $result;
    }


}