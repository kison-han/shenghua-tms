<?php

namespace app\index\model\bill;

use think\Db;
use think\Model;
use app\common\help\Help;

class CustomerBillInfo extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'customer_bill_info';

    /**
     * 获取客户账单
     * 韩
     */
    public function getCustomerBillInfo($params,$is_count=false,$is_page=false,$page=null,$page_size=20){

        $data = "1=1 ";

        if(is_numeric($params['customer_bill_id'])){ 
            $data.= " and customer_bill_info.customer_bill_id = ".$params['customer_bill_id'];
        }

        if($is_count==true){
            $result = $this->table("customer_bill_info")->where($data)->count();
        }else{
            if($is_page == true){
                $result = $this->table("customer_bill")->
                where($data)->limit($page, $page_size)->order('create_time desc')->
                field(["customer_bill_info.*"
                ])->select();

              
            }else{
                $result = $this->table("customer_bill_info")->
                	where($data)->order('create_time desc')->
                field(["customer_bill_info.*"
                ])->select();

            }
        }
        return $result;
    }

}