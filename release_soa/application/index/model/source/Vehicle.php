<?php

namespace app\index\model\source;
use think\Model;
use app\common\help\Help;
use app\index\service\PublicService;
use think\config;
use think\Db;
class Vehicle extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'vehicle';
    private $_languageList;
    private $_public_service;
    public function initialize()
    {
    	$this->_languageList = config('systom_setting')['language_list'];
    	$this->_public_service = new PublicService();
    	parent::initialize();
    
    }

    /**
     * 添加车辆
     * 胡
     */
    public function addVehicle($params){
    	$t = time();

    	$data['supplier_uuid'] = $params['supplier_uuid'];
    	$data['number_plate'] = $params['number_plate'];
    	$data['vehicle_type_id'] = $params['vehicle_type_id'];
        if(isset($params['vehicle_brand'])){
    		$data['vehicle_brand'] = $params['vehicle_brand'];
    	}
        if(isset($params['vehicle_width'])){
            $data['vehicle_width'] = $params['vehicle_width'];
        }
        if(isset($params['vehicle_lenght'])){
            $data['vehicle_lenght'] = $params['vehicle_lenght'];
        }
        if(!empty($params['vehicle_height'])){
            $data['vehicle_height'] = $params['vehicle_height'];

        }
        if(isset($params['max_load'])){
            $data['max_load'] = $params['max_load'];
        }
        if(isset($params['max_Volume'])){
            $data['max_Volume'] = $params['max_Volume'];
        }
        if(isset($params['remark'])){
        	$data['remark'] = $params['remark'];
        }        

    	$data['create_time'] = $t;  	
    	$data['create_user_id'] = $params['user_id'];
    	$data['update_time'] = $t;
    	$data['update_user_id'] = $params['user_id'];
    	$data['status'] = $params['status'];

    	$data['vehicle_uuid'] = Help::getUuid();
    	Db::startTrans();
    	try{
    		$pk_id = Db::name('vehicle')->insertGetId($data);
	
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
    
    	return $result;
    }
    
    /**
     * 获取车辆
     * 胡
     */
    public function getVehicle($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    	$data = "1=1 ";

    	if(is_numeric($params['status'])){
    		$data.= " and vehicle.status = ".$params['status'];
    	}
    	if(!empty($params['vehicle_id'])){
    		$data.= " and vehicle.vehicle_id = '".$params['vehicle_id']."'";
    	}

    	if(!empty($params['supplier_uuid'])){
    		$data.= " and vehicle.supplier_uuid = '".$params['supplier_uuid']."'";
    	}	
        if(!empty($params['vehicle_type_id'])){
    		$data.= " and vehicle.vehicle_type_id = '".$params['vehicle_type_id']."'";
    	}
    	if(!empty($params['max_load'])){
    		$data.= ' and vehicle.max_load like "%'.$params['max_load'].'%"';
    	}   	
    	if(!empty($params['number_plate'])){
    		$data.= ' and vehicle.number_plate like "%'.$params['number_plate'].'%"';
    	}    	


        if($is_count==true){
            $result = $this->table("vehicle")->where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("vehicle")->
                join('vehicle_type', 'vehicle_type.vehicle_type_id= vehicle.vehicle_type_id')->
                join('supplier', 'supplier.supplier_uuid = vehicle.supplier_uuid')->
          
                where($data)->limit($page, $page_size)->order('create_time desc')->
                field(['vehicle.vehicle_id', "vehicle.vehicle_uuid", 'vehicle.number_plate',
                   
                    'vehicle.vehicle_brand', 'vehicle.vehicle_width',
                    'vehicle.vehicle_lenght', "vehicle.vehicle_height",
                	'vehicle.max_load',
                    'vehicle.max_Volume',
                    'vehicle.remark',
              		'vehicle.supplier_uuid',
                    'supplier.supplier_name', 'vehicle_type.vehicle_type_name',

                    "(select nickname  from user where user.user_id = vehicle.create_user_id)"=> 'create_user_name',
                    "(select nickname  from user where user.user_id = vehicle.update_user_id)"=> 'update_user_name',
                    'vehicle.update_time', 'vehicle.create_time', "vehicle.status",
                ])->select();
            }else{
                $result = $this->table("vehicle")->alias('vehicle')->
                join('vehicle_type', 'vehicle_type.vehicle_type_id= vehicle.vehicle_type_id')->
                join('supplier', 'supplier.supplier_uuid = vehicle.supplier_uuid')->
                where($data)->order('create_time desc')->
                field(['vehicle.vehicle_id', "vehicle.vehicle_uuid", 'vehicle.number_plate',
                   
                    'vehicle.vehicle_brand', 'vehicle.vehicle_width',
                    'vehicle.vehicle_lenght', "vehicle.vehicle_height",
                	'vehicle.max_load',
                    'vehicle.max_Volume',
                    'vehicle.remark',
                	'vehicle.supplier_uuid',
                    'supplier.supplier_name', 'vehicle_type.vehicle_type_name',
                    "(select nickname  from user where user.user_id = vehicle.create_user_id)"=>'create_user_name',
                    "(select nickname  from user where user.user_id = vehicle.update_user_id)"=>'update_user_name',
                    'vehicle.update_time','vehicle.create_time',"vehicle.status",
                ])->select();
            }
        }

        return $result;
    
    }
    /**
     * 获取车辆数据根据签证_ID与lang_id
     */
    public function getVehicleByVehicleIdLangId($params){
    
    	$lang_id = $params['lang_id'];
    	$data['language_id'] = $lang_id;
    	$data['source_number'] = $params['source_number'];
    	$result = $this->table('vehicle_language')->
    	where($data)->find();
    
    	return $result;
    }
    
    /**
     * 修改车辆多语言数据根据车辆多语言ID
     */
    public function updateVehicleLanguageByVehicleLanguageId($params){
    
    	$t = time();
    	$user_id = $params['user_id'];
    
    	$original_number = $params['data'][0]['source_number'];
    
    	$original_data['source_number'] = $original_number;
    
    
    	$params = $params['data'];
    
    	//原始数据主键
    	$original_result = $this->getVehicle($original_data);
    
    	$default_language_id = $original_result[0]['default_language_id'];
    
    	$this->startTrans();
    	try{
    		for($i=0;$i<count($params);$i++){
    
    			$data = [];
    			if(!trim($params[$i]['vehicle_name'])==''){
    					
    				$data['vehicle_name'] = $params[$i]['vehicle_name'];
    				$data['update_time'] = $t;
    				$data['update_user_id'] = $user_id;
    
    				if(is_numeric($params[$i]['vehicle_language_id'])){
    
    					$this->table('vehicle_language')->where("vehicle_language_id = ".$params[$i]['vehicle_language_id'])->update($data);
    
    					//再查询是否是原始数据  如果是原始数据那么原始 数据也要更改
    					if($default_language_id == $params[$i]['lang_id']){
    							
    						$this->where("source_number = '$original_number'")->update($data);
    
    					}
    				}else{
    
    					$data['create_time'] = $t;
    					$data['create_user_id'] = $user_id;
    					$data['status'] = 1;
    					$data['source_number'] = $original_number;
    					$data['language_id'] = $params[$i]['lang_id'];
    					$this->table("vehicle_language")->insert($data);
    
    				}
    			}
    		}
    
    		$result = 1;
    		// 提交事务
    		$this->commit();
    		 
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		$this->rollback();
    		 
    	}
    
    	return $result;
    
    }
    
    
    /**
     * 修改车辆
     */
    public function updateVehicleByVehicleId($params){
    
    	$t = time();
    	
    	if(!empty($params['number_plate'])){
    		$data['number_plate'] = $params['number_plate'];
    	
    	}
    	if(!empty($params['supplier_uuid'])){
    		$data['supplier_uuid'] = $params['supplier_uuid'];
    		 
    	}
    	if(!empty($params['vehicle_type_id'])){
    		$data['vehicle_type_id'] = $params['vehicle_type_id'];
    		 
    	}
        if(isset($params['vehicle_brand'])){
            $data['vehicle_brand'] = $params['vehicle_brand'];
        }
        if(isset($params['vehicle_width'])){
            $data['vehicle_width'] = $params['vehicle_width'];
        }
        if(isset($params['vehicle_lenght'])){
            $data['vehicle_lenght'] = $params['vehicle_lenght'];
        }
        if(isset($params['vehicle_height'])){
            $data['vehicle_height'] = $params['vehicle_height'];
        }
        if(isset($params['max_load'])){
            $data['max_load'] = $params['max_load'];
        }
        if(isset($params['max_Volume'])){
            $data['max_Volume'] = $params['max_Volume'];
        }
        if(!empty($params['remark'])){
            $data['remark'] = $params['remark'];

        }
    	if(!empty($params['status'])){
    		$data['status'] = $params['status'];
    		 
    	}



    	$data['update_user_id'] = $params['user_id'];   
    	$data['update_time'] = $t;

    
    
    	$source_price=[];
    	Db::startTrans();
    	try{
    		Db::name('vehicle')->where("vehicle_id = ".$params['vehicle_id'])->update($data);
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    
    	}
    	return $result;
    }

    /**
     * getOneVehicle
     *
     * 获取一条车辆信息
     * @author shj
     *
     * @param $vehicle_id
     *
     * @return void
     * Date: 2019/2/27
     * Time: 17:00
     */
    public function getOneVehicle($vehicle_id){
        $result = $this->table("vehicle")->where(['vehicle_id' => $vehicle_id])->find();
        return $result;
    }
}