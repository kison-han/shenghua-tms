<?php


namespace app\index\model\orders;


use app\index\model\Base;

class OrdersRemark extends Base
{
    protected $table = "orders_remark";

    public function getRemark($params)
    {
        $where = [];
        if (is_numeric($params['type']))
            $where['type'] = $params['type'];
        if (is_numeric($params['orders_id']))
            $where['orders_id'] = $params['orders_id'];
        return $this->where($where)->select();

    }
//处理备注
    public function handleRemark($params)
    {
        $where = [];
        $where['type'] = $params['type'] ? $params['type'] : 3;
        $where['orders_id'] = $params['orders_id'];
        $remark_info = $this->where($where)->find();
        if ($remark_info) {
            $remark_info->content = $params['income_remark'];
            $remark_info->save();
            return 1;
        } else {
            $insertData['content'] = $params['income_remark'];
            $insertData = array_merge($insertData, $where);
            return $this->pubInsert($insertData);
        }
    }

}