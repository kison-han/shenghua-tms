<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:92:"/var/www/html/test_erp/public/../application/index/view/order/order_customer_follow_add.html";i:1663202225;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






	<title>
  										<?php if(\think\Request::instance()->get('project_id') ==  ''): ?>
										新增项目
										<?php else: ?>
										修改项目
										<?php endif; ?>	
	
	</title>

</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
   
	<div class="a">

		<!-- 内容主体区域 -->
		<div class="content_body">
			<div class='layui-form-item'>
		   		<span class="layui-breadcrumb" lay-separator="-">
					<a>首页</a>
					<a>运单管理</a>
					<a>	<cite>发运跟踪</cite></a>					
				
		
					
					
				</span>
			</div>
			<br/>
			<div class="layui-row layui-col-space10">

			<br/>
			<form class="layui-form"  id="form1" onSubmit="return orderCustomerFollowAdd()">
			

					<div class="layui-col-md4">
						<div class="layui-form-item">
							<label class="layui-form-label input-required">运单号:</label>
							<div class="layui-input-block">
								<input  id="order_id"  value="<?php echo \think\Request::instance()->get('orders_number'); ?>" readonly="readonly" class="layui-input" type="text">
							</div>
						</div>
					</div>
					                    <div class="layui-form-item">
                        <label class="layui-form-label input-required">内容：</label>
                        <div class="layui-input-block">
                            <textarea  type="text" style="height:300px" class="layui-input" name="orders_customer_follow_content" id="EditorId" placeholder="请输入内容"><?php echo $result['orders_customer_follow_content']; ?></textarea>
                        </div>
                    </div>



				<input type='hidden'  name='orders_customer_follow_id' value='<?php echo $result['orders_customer_follow_id']; ?>' />
				<input type='hidden'  name='orders_number' id='orders_number' value='<?php echo \think\Request::instance()->get('orders_number'); ?>' />
				<div class="layui-form-item">
					<div class="layui-input-block all-button-center">
						<button class="layui-btn nav-submit" lay-submit="" lay-filter="formDemo" id="dining_add_button">提交</button>
						
					</div>
				</div>
			</form>
		</div>
		<hr/>
		
		
	</div>

</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
<script src='/static/javascript/system/customer.js'></script>
<script>

var layer = layui.layer;

var form = layui.form;

layui.use('element', function(){
	var element = layui.element;
});
var isShow = true;  //定义⼀个标志位
$('.kit-side-fold').click(function(){
	//选择出所有的span，并判断是不是hidden
	$('.layui-nav-item span').each(function(){
		if($(this).is(':hidden')){
			$(this).show();
		}else{
			$(this).hide();
		}
	});
	//判断isshow的状态
	if(isShow){
		$('.layui-side.layui-bg-black').width(50); //设置宽度
		$('.kit-side-fold i').css('margin-right', '70%');  //修改图标的位置
		//将footer和body的宽度修改
		$('.layui-body').css('left', 60+'px');
		$('.layui-footer').css('left', 60+'px');
		//将⼆级导航栏隐藏
		$('dd span').each(function(){
			$(this).hide();
		});
		//修改标志位
		isShow =false;
	}else{
		$('.layui-side.layui-bg-black').width(200);
		$('.kit-side-fold i').css('margin-right', '10%');
		$('.layui-body').css('left', 200+'px');
		$('.layui-footer').css('left', 200+'px');
		$('dd span').each(function(){
			$(this).show();
		});
		isShow =true;
	}
});

//加载日期控件
layui.laydate.render({
    elem: '#follow_time' ,//input的id
    trigger: 'click',//解决一闪而过的问题
});

function orderCustomerFollowAdd(){

    $.ajax({
        type: "POST",
        url: '/order/addOrderCustomerFollowAjax',
        data: $('#form1').serializeArray(),
        success: function(data){
            // console.log(data);
            if(data.code!=200){
                layer.msg(data.msg);
                return false;
            }else if(data.code==200) {
            	layer.msg('修改成功'); 
				window.parent.layer.closeAll();
				window.parent.newtable.reload()
            	//location.href='/order/showOrderReceiptManage'
            }
        },
        error:function(XMLHttpRequest, textStatus, errorThrown){
            layer.msg('验证失败')
        }
    });
    return false;
} 
</script>
                    <script type="text/javascript" charset="utf-8">
                        window.UEDITOR_HOME_URL = "/static/ueditor/";
                        window.onload=function(){
                            window.UEDITOR_CONFIG.initialFrameHeight=300;
                            window.UEDITOR_CONFIG.initialFrameWidth=370;
                            window.UEDITOR_CONFIG.autoHeightEnabled=false;
                            window.UEDITOR_CONFIG.autoFloatEnabled=false;
                            window.UEDITOR_CONFIG.elementPathEnabled=false;
                            var editor = new UE.ui.Editor({
                                imageUrl : '',
                                fileUrl : '',
                                imagePath : '',
                                filePath : '',
                                imageManagerUrl:'',
                                imageManagerPath:'/static/uploads/images/'
                            });
                            editor.render("EditorId1");
                        }
                    </script>
</body>
</html>
