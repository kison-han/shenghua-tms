<?php

namespace app\index\model\warehouse;

use think\Db;
use think\Model;

class Warehouse extends Model
{
    protected $table = 'wms_warehouse';

    /**
     * 获取仓库
     * 韩
     */
    public function getWarehouse($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
        $data = "1=1 ";

        if(!empty($params['ware_id'])){ //仓库id
            $data.=" and wms_warehouse.ware_id =".$params['ware_id'];
        }

        if(!empty($params['company_id'])){ //公司id
            $data.=" and wms_warehouse.company_id =".$params['company_id'];
        }

        if(!empty($params['ware_name'])){ //仓库名称
            $data.=' and wms_warehouse.ware_name ="'.$params['ware_name'].'"';
        }

        if(!empty($params['contacts_id'])){ //联系人
            $data.=' and wms_warehouse.contacts_id ="'.$params['contacts_id'].'"';
        }

        if(is_numeric($params['status'])){ //状态
            $data.=" and wms_warehouse.status =".$params['status'];
        }

        if($is_count==true){
            $result = $this->table("wms_warehouse")->where($data)->count();
        }else{
            if($is_page == true){
                $result = $this->table("wms_warehouse")->
                join("company",'company.company_id= wms_warehouse.company_id')->
                where($data)->limit($page, $page_size)->order('wms_warehouse.create_time desc')->
                field(['wms_warehouse.ware_id','company.company_name','wms_warehouse.ware_name','wms_warehouse.company_id',
                    "(select city_name from city where city.city_id= wms_warehouse.ware_province_id)"=> 'ware_province_name',
                    "(select city_name from city where city.city_id= wms_warehouse.ware_city_id)"=> 'ware_city_name',
                    "(select city_name from city where city.city_id= wms_warehouse.ware_area_id)"=> 'ware_area_name',
                    'wms_warehouse.ware_address',
                    'wms_warehouse.contacts_id',
                    "(select nickname from user where user.user_id = wms_warehouse.contacts_id)"=> 'contacts_name',
                    "(select nickname from user where user.user_id = wms_warehouse.create_user_id)"=> 'create_user_name',
                    "(select nickname from user where user.user_id = wms_warehouse.update_user_id)"=> 'update_user_name',
                    'wms_warehouse.ware_province_id','wms_warehouse.ware_city_id','wms_warehouse.ware_area_id',
                    'wms_warehouse.status',
                    'from_unixtime(wms_warehouse.create_time)'=> 'create_time',
                    'from_unixtime(wms_warehouse.update_time)'=> 'update_time'
                    ])->select();
            }else{
                $result = $this->table("wms_warehouse")->
                join("company",'company.company_id= wms_warehouse.company_id')->
                where($data)->order('wms_warehouse.create_time desc')->
                field(['wms_warehouse.ware_id','company.company_name','wms_warehouse.ware_name','wms_warehouse.company_id',
                    "(select city_name from city where city.city_id= wms_warehouse.ware_province_id)"=> 'ware_province_name',
                    "(select city_name from city where city.city_id= wms_warehouse.ware_city_id)"=> 'ware_city_name',
                    "(select city_name from city where city.city_id= wms_warehouse.ware_area_id)"=> 'ware_area_name',
                    'wms_warehouse.ware_address',
                    'wms_warehouse.contacts_id',
                    "(select nickname from user where user.user_id = wms_warehouse.contacts_id)"=> 'contacts_name',
                    "(select nickname from user where user.user_id = wms_warehouse.create_user_id)"=> 'create_user_name',
                    "(select nickname from user where user.user_id = wms_warehouse.update_user_id)"=> 'update_user_name',
                    'wms_warehouse.ware_province_id','wms_warehouse.ware_city_id','wms_warehouse.ware_area_id',
                    'wms_warehouse.status',
                    'from_unixtime(wms_warehouse.create_time)'=> 'create_time',
                    'from_unixtime(wms_warehouse.update_time)'=> 'update_time'
                ])->select();
            }
        }
        return $result;
    }

    /**
     * 添加仓库
     * 韩
     */
    public function addWarehouse($params){
        $t = time();

        $data['company_id'] = $params['company_id'];
        $data['ware_name'] = $params['ware_name'];
        $data['ware_province_id'] = $params['ware_province_id'];
        $data['ware_city_id'] = $params['ware_city_id'];
        $data['ware_area_id'] = $params['ware_area_id'];
        $data['ware_address'] = $params['ware_address'];
        $data['contacts_id'] = $params['contacts_id'];
        $data['create_time'] = $t;
        $data['create_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        $data['update_user_id'] = $params['user_id'];
        $data['status'] = $params['status'];

        Db::startTrans();
        try{
            $this->table("wms_warehouse")->insertGetId($data);
            $result = 1;
            // 提交事务
    		Db::commit();
        }catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return $result;
    }

    /**
     * 修改仓库
     * 韩
     */
    public function updateWarehouseByWarehouseId($params){
        $t = time();

        $data['company_id'] = $params['company_id'];
        $data['ware_name'] = $params['ware_name'];
        $data['ware_province_id'] = $params['ware_province_id'];
        $data['ware_city_id'] = $params['ware_city_id'];
        $data['ware_area_id'] = $params['ware_area_id'];
        $data['ware_address'] = $params['ware_address'];
        $data['contacts_id'] = $params['contacts_id'];
//        $data['create_time'] = $t;
//        $data['create_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        $data['update_user_id'] = $params['user_id'];
        $data['status'] = $params['status'];

        Db::startTrans();
        try {
            Db::name('wms_warehouse')->where("ware_id = " . $params['ware_id'])->update($data);
            $result = 1;
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return $result;
    }
}