<?php
/**
 * Created by PhpStorm.
 * User: Hugh
 * Date: 2019/11/04
 * Time: 13:40
 */

namespace app\index\controller;
use app\common\help\Contents;
use \Underscore\Types\Arrays;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
use app\common\help\Help;

class B2bMyinfo extends BookingBase
{
    public function index(){
        $this->assign('site_title','Update My Info');

        //获取代理信息
        $w['distributor_id'] = session('booking_user')['distributor_id'];
        $distributor = $this->callSoaErp('post','/btob/getBtoBDistributor',$w);
        $this->assign('dd',$distributor['data'][0]);
//        echo '<pre>'; print_r($distributor);exit;

        //获取顾问
        $data['distributor_id'] = session('booking_user')['distributor_id'];
        $distributorData = $this->callSoaErp('post', '/btob/getDistributorConsultant', $data);
        $this->assign('distributorData',$distributorData['data']);

        return $this->fetch('index');
    }



    public function searchConsultants(){
        $w['distributor_id'] = $_POST['agent_id'];
        $w['distributor_consultant_id'] = $_POST['search_consultant_id'];
        $w['consultant_name'] = $_POST['search_consultant_name'];

        return $this->callSoaErp('post', '/btob/getDistributorConsultant', $w);

    }

    public function add_consultant(){
        $d['distributor_id'] = $_POST['agentID'];
        $d['consultant_name'] = $_POST['name'];
        return $this->callSoaErp('post','/btob/addDistributorConsultant',$d);
    }

    public function update_consultant(){
        $d['distributor_id'] = $_POST['agentID'];
        $d['consultant_name'] = $_POST['name'];
        $d['distributor_consultant_id'] = $_POST['consultant_id'];
        return $this->callSoaErp('post','/btob/updateDistributorConsultant',$d);
    }

    public function update(){
//        echo '<pre>';print_r($_POST);

        $data = $_POST;

        foreach($_FILES as $fieldName => $fileObject){
            $arr = $this->uploadFile($fieldName);
            if ($arr['code']==400) break;
            $data['logo'] = $arr['data'];
        }

//var_dump($data);exit;
        $data['status'] = 1;
        $data['user_id'] = session('booking_user')['create_user_id']?:0;
        $this->callSoaErp('post','/btob/updateBtoBDistributorByDistributorId',$data);
        echo '<script>location.href="/b2b_myinfo/index"</script>';
    }


    public function uploadFile($fieldName){
        $file = request()->file($fieldName);
        if($file){
            $file_result = $file->getInfo();
            $info = $file->move(ROOT_PATH . 'public' . DS . 'static'.DS.'uploads'.DS.'images');

            $image_name = $file_result['name'];
            //获取后缀名
            $image_name = substr($image_name,strpos($image_name,'.')+1);

            $temp_name = $file_result['tmp_name'];
            $temp_name = str_replace('tmp',$image_name,$temp_name);
            $url = config('soaupload')['ip'].':'.config('soaupload')['port'];

            $result = help::curlImages($info->getPathname(), $url."/index/uploadImages");

            if($result){
                $result = json_decode($result,true);
                $result['name'] = $file_result['name'];
                $result['get'] = $_GET;
                return $result;

            }else{
                $d['data'] = '/static/uploads/images/'.$info->getSaveName();
                $d['code'] = 200;
                $d['get'] = $_GET;
                $d['name'] = $file_result['name'];
                return $d;
            }
        }else{
            // 上传失败获取错误信息
            return;
        }

    }
}