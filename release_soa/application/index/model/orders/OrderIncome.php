<?php


namespace app\index\model\orders;


use app\index\model\Base;
use app\index\model\finance\Cost;
use think\Db;
use think\Model;

class OrderIncome extends Base
{
protected $table="orders_income";

public function incomeAccountingImport($params,$user_id=1){
    $order_module=new Orders();
    $fee_module=new Cost();
    $remark_module=new OrdersRemark();
    $fee_config=$fee_module->where('cost_type',1)->select();
    $fee_ids=[];

    $fee_data=$params[0];
    $data_length=count($fee_data)-1;
    unset($fee_data[$data_length]);
    unset($fee_data[0]);

    foreach ($fee_data as $k=>$v)
    {

        $fee_info=$fee_module->where('cost_name','like',$v)
            ->where('cost_type',1)
            ->find();
        if($fee_info==null)
            return "费用名称不存在".$v;
        $fee_ids[$k]=$fee_info['cost_id'];
    }


unset($params[0]);

foreach ($params as $k=>$v)
{    $order_number=$v[0];
    $order_res=$order_module->where('orders_number',$order_number)->find();


    if(!$order_res)
       return "行号:".($k+1)."订单号:".$order_number.'不存在';
    if($order_res['verify_status']>2)
        return "行号:".($k+1)."订单号:".$order_number.'财务已对账不能添加';


    unset($v[0]);
    $key_count=0;
    foreach ($v as $k1=>$v1)
    {

         if(is_numeric($v1))
         $key_count++;
    }
    if($key_count==0){
        return "行号:".($k+1).",订单号:".$order_number.'数据为空';
    }



}foreach ($params as $k=>$v)
{    $order_number=$v[0];
     $remark=" ";
     if($v[$data_length])
     $remark=$v[$data_length];
    $order_info=$order_module->where('orders_number',$order_number)->find();
    if($order_info)
    {
        $orders_id=$order_info->orders_id;
        $remark_module->where('orders_id',$orders_id)->where('type',3)->delete();
        $remark_module->pubInsert(['orders_id'=>$orders_id,'type'=>3,'content'=>$remark]);
    }




    $this->where('orders_id',$order_info['orders_id'])->delete();

    unset($v[0]);
    unset($v[$data_length]);

    $order_info->money=array_sum($v);
    $order_info->is_free=(array_sum($v))>0?0:1;

    foreach ($v as $k1=>$v1)
    {
        if(is_numeric($v1)) {
            $insertData=[];
            $insertData['orders_id']=$order_info['orders_id'];
            $insertData['money']=$v1;
            $insertData['cost_id']=$fee_ids[$k1];
            $insertData['cost_name']=$fee_data[$k1];
            $insertData['create_time']=time();
            $insertData['update_time']=time();
            $insertData['status']=1;
            $insertData['create_user_id']=$user_id;
            $insertData['update_user_id']=$user_id;


            $this->pubInsert($insertData);

        }
        }
    $order_info->save();

}

return 0;
}

public function getinfo($params){
return $this->where('orders_id',$params['orders_id'])->select();
}

public function add($params){

    $order_remark_model=new OrdersRemark();
    $order_remark_model->handleRemark($params);

    $this->where('orders_id',$params['orders_id'])->delete();
    $cost_model=new Cost();
    $finance_model=new OrdersFinance();
   // $finance_model->where('orders_id',$params['orders_id'])->delete();

    $data=[];
    $money_count=0;
    /*$f_id=$finance_model->insertGetId(['orders_id'=>$params['orders_id'],'create_time'=>time(),
        'update_time'=>time(),'create_user_id'=>$params['user_id'],'update_user_id'=>$params['user_id']]);*/
    foreach ($params['cost_id'] as  $k=>$v){
      $tmp=[];
        $cost_info=$cost_model->where('cost_id',$v)->find();
        $tmp['money']=$params['cost_value'][$k];
        $tmp['remark']=$params['remark'][$k];
        $money_count+=$params['cost_value'][$k];
        $tmp['cost_name']=$cost_info->cost_name;
        $tmp['cost_id']=$v;
        $tmp['orders_id']=$params['orders_id'];
       // $tmp['finance_id']=$f_id;
        $tmp['create_time']=time();
        $tmp['update_time']=time();
        $tmp['create_user_id']=$params['user_id'];
        $tmp['update_user_id']=$params['user_id'];

        $data[]=$tmp;
    }
    /*$finance_info=$finance_model->where('finance_id',$f_id)->find();
    $finance_info->money=$money_count;
    $finance_info->save();*/
    $order_model=new Orders();
    $order_info=$order_model->where('orders_id',$params['orders_id'])->find();

    if($order_info->verify_status>2) {
        return \think\Response::create(['code' => '400', 'msg' => "不允许修改"], 'json')->send();
        exit();
    }
    $order_model->where('orders_id',$params['orders_id'])->update(['verify_status'=>1,'money'=>$money_count]);



    $this->pubInsertAll($data);



    return $result;


}

}

