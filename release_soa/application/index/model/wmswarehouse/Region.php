<?php

namespace app\index\model\wmswarehouse;

use think\Db;
use think\Model;

class Region extends Model
{
    protected $table = 'wms_region';

    /**
     * 获取仓库
     * 韩
     */
    public function getRegion($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
        $data = "1=1 ";

        if(!empty($params['region_id'])){ //区域id
            $data.=" and region.region_id =".$params['region_id'];
        }

        if(!empty($params['region_code'])){ //区域代码
            $data.=' and region.region_code ="'.$params['region_code'].'"';
        }

        if(!empty($params['ware_id'])){ //仓库id
            $data.=" and region.ware_id =".$params['ware_id'];
        }

        if(!empty($params['contacts_id'])){ //联系人
            $data.=" and region.contacts_id =".$params['contacts_id'];
        }

        if(!empty($params['contacts_phone'])){ //联系电话
            $data.=' and region.contacts_phone ="'.$params['contacts_phone'].'"';
        }

        //创建时间

        if(is_numeric($params['status'])){ //状态
            $data.=" and region.status =".$params['status'];
        }

        if($is_count==true){
            $result = $this->table("wms_region")->alias("region")->where($data)->count();
        }else{
            if($is_page == true){
                $result = $this->table("wms_region")->alias("region")->
                join("wms_warehouse_v1 wms_warehouse",'wms_warehouse.ware_id= region.ware_id')->
                where($data)->limit($page, $page_size)->order('region.create_time desc')->
                field(['region.region_id','region.region_code','region.region_name','region.ware_id','wms_warehouse.ware_name',
                    "(select city_name from city where city.city_id= region.region_province_id)"=> 'region_province_name',
                    "(select city_name from city where city.city_id= region.region_city_id)"=> 'region_city_name',
                    "(select city_name from city where city.city_id= region.region_area_id)"=> 'region_area_name',
                    'region.region_address',
                    'region.contacts_id',
                    "(select nickname from user where user.user_id = region.contacts_id)"=> 'contacts_name',
                    'region.contacts_phone',
                    'region.remark',
                    "(select nickname from user where user.user_id = region.create_user_id)"=> 'create_user_name',
                    "(select nickname from user where user.user_id = region.update_user_id)"=> 'update_user_name',
                    'region.region_province_id','region.region_city_id','region.region_area_id',
                    'region.status',
                    'from_unixtime(region.create_time)'=> 'create_time',
                    'from_unixtime(region.update_time)'=> 'update_time'
                    ])->select();
            }else{
                $result = $this->table("wms_region")->alias("region")->
                join("wms_warehouse_v1 wms_warehouse",'wms_warehouse.ware_id= region.ware_id')->
                where($data)->order('region.create_time desc')->
                field(['region.region_id','region.region_code','region.region_name','region.ware_id','wms_warehouse.ware_name',
                    "(select city_name from city where city.city_id= region.region_province_id)"=> 'region_province_name',
                    "(select city_name from city where city.city_id= region.region_city_id)"=> 'region_city_name',
                    "(select city_name from city where city.city_id= region.region_area_id)"=> 'region_area_name',
                    'region.region_address',
                    'region.contacts_id',
                    "(select nickname from user where user.user_id = region.contacts_id)"=> 'contacts_name',
                    'region.contacts_phone',
                    'region.remark',
                    "(select nickname from user where user.user_id = region.create_user_id)"=> 'create_user_name',
                    "(select nickname from user where user.user_id = region.update_user_id)"=> 'update_user_name',
                    'region.region_province_id','region.region_city_id','region.region_area_id',
                    'region.status',
                    'from_unixtime(region.create_time)'=> 'create_time',
                    'from_unixtime(region.update_time)'=> 'update_time'
                ])->select();
            }
        }
        return $result;
    }

    /**
     * 添加仓库
     * 韩
     */
    public function addWarehouse($params){
        $t = time();

        $data['company_id'] = $params['company_id'];
        $data['ware_name'] = $params['ware_name'];
        $data['ware_province_id'] = $params['ware_province_id'];
        $data['ware_city_id'] = $params['ware_city_id'];
        $data['ware_area_id'] = $params['ware_area_id'];
        $data['ware_address'] = $params['ware_address'];
        $data['contacts_id'] = $params['contacts_id'];
        $data['create_time'] = $t;
        $data['create_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        $data['update_user_id'] = $params['user_id'];
        $data['status'] = $params['status'];

        Db::startTrans();
        try{
            $this->table("wms_warehouse")->insertGetId($data);
            $result = 1;
            // 提交事务
    		Db::commit();
        }catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return $result;
    }

    /**
     * 修改仓库
     * 韩
     */
    public function updateWarehouseByWarehouseId($params){
        $t = time();

        $data['company_id'] = $params['company_id'];
        $data['ware_name'] = $params['ware_name'];
        $data['ware_province_id'] = $params['ware_province_id'];
        $data['ware_city_id'] = $params['ware_city_id'];
        $data['ware_area_id'] = $params['ware_area_id'];
        $data['ware_address'] = $params['ware_address'];
        $data['contacts_id'] = $params['contacts_id'];
//        $data['create_time'] = $t;
//        $data['create_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        $data['update_user_id'] = $params['user_id'];
        $data['status'] = $params['status'];

        Db::startTrans();
        try {
            Db::name('wms_warehouse')->where("ware_id = " . $params['ware_id'])->update($data);
            $result = 1;
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return $result;
    }
}