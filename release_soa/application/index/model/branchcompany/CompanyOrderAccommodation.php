<?php

namespace app\index\model\branchcompany;
use think\Model;
use app\common\help\Help;
use think\config;
use think\Db;
class CompanyOrderAccommodation extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'company_order_accommodation';
    private $_languageList;
    public function initialize()
    {
        $this->_languageList = config('systom_setting')['language_list'];
        parent::initialize();

    }

    /**
     * 添加公司订单的顾客住宿
     * 胡
     */
    public function addCompanyOrderCustomerAccommodation($params){

        $t = time();
		$data['company_order_number'] = $params['company_order_number'];
		$data['company_order_customer_id'] = $params['company_order_customer_id'];
		$data['customer_id'] = $params['customer_id'];
		if(is_numeric($params['room_code'])){
			$data['room_code'] = $params['room_code'];
		}
		
		if(is_numeric($params['room_type'])){
			$data['room_type'] =$params['room_type'];
		}
		
		
		$data['check_in'] = $params['check_in'];
		$data['check_on'] = $params['check_on'];
        $data['remark'] = $params['remark'];
		$data['check_in_hotel'] = $params['check_in_hotel'];
        $data['check_on_hotel'] = $params['check_on_hotel'];
		
		$data['create_time'] = $t;
		$data['update_time'] = $t;
		$data['create_user_id'] = $params['now_user_id'];
		$data['update_user_id'] = $params['now_user_id'];

		$data['status'] = 1;
		
        $this->startTrans();
        try{
			
        	$result = $this->insertGetId($data);
        	
        	

            // 提交事务
            $this->commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            $this->rollback();

        }

        return $result;
    }
	/**
	 * 添加占位
	 */
    public function addCustomerOccupy($params){
    	$t = time();
    	$user_id = $params['user_id'];
    	$order_number = $params['company_order_number'];
    	$sql =  "insert into company_order_customer (company_order_customer_id,company_order_number,customer_id,create_time,create_user_id,update_time,update_user_id,status) values";
    	
    	
    	$this->startTrans();
    	try{
    		for($i=0;$i<$params['occupy_count'];$i++){
    			$sql.="('','$order_number','',$t,$user_id,$t,$user_id,2),";
    			
    			
    		}
    		
    		$sql = trim($sql,',');
    		
    		$this->execute($sql);

    		
    		$result = 1;
    	
    		 
    		// 提交事务
    		$this->commit();
    	
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		$this->rollback();
    	
    	}
    	
    	return $result;
    }
    /**
     * 获取分公司订单住宿
     * 胡
     */
    public function getCompanyOrderAccommodation($params){

 
    	$data = "1=1";
    	if(isset($params['customer_id'])){
    		$data.= " and company_order_accommodation.customer_id = ".$params['customer_id'];
    	}
    	if(isset($params['company_order_customer_id'])){
    		$data.= " and company_order_accommodation.company_order_customer_id = ".$params['company_order_customer_id'];
    	}
    	if(is_numeric($params['status'])){
    		$data.= " and company_order_accommodation.status = ".$params['status'];
    	}
    	if(isset($params['company_order_accommodation_id'])){
    		$data.= " and company_order_accommodation.company_order_accommodation_id = '".$params['company_order_accommodation_id']."'";
    	}
    	if(isset($params['company_order_number'])){
    		$data.= " and company_order_accommodation.company_order_number = '".$params['company_order_number']."'";
    	}
		
            $result= $this->table("company_order_accommodation")->alias("company_order_accommodation")->
            join("customer customer","customer.customer_id = company_order_accommodation.customer_id",'left')->

            where($data)->
            field(['company_order_accommodation.company_order_accommodation_id',"company_order_accommodation.company_order_number",
            		'company_order_accommodation.customer_id','customer.customer_first_name',
            		'customer.customer_last_name','customer.english_first_name','customer.english_last_name',
            		'company_order_accommodation.room_code',
            		'company_order_accommodation.room_type','company_order_accommodation.check_in','company_order_accommodation.check_on',
                    'company_order_accommodation.check_in_hotel','company_order_accommodation.check_on_hotel',
            		"(select nickname  from user where user.user_id = company_order_accommodation.create_user_id)"=>'create_user_name',
            		"(select nickname  from user where user.user_id = company_order_accommodation.update_user_id)"=>'update_user_name',
            		'company_order_accommodation.create_user_id','company_order_accommodation.create_time','company_order_accommodation.update_user_id',
            		'company_order_accommodation.update_time','company_order_accommodation.status'])->
            
            select();

       


        return $result;

    }


    /**
     * 修改分公司订单-游客住宿 根据游客住宿ID
     */
    public function updateCompanyOrderAccommodationByCompanyOrderAccommodationId($params){

        $t = time();
        
        if(!empty($params['room_code'])){
        	$data['room_code'] = $params['room_code'];

        }
        if(!empty($params['room_type'])){
        	$data['room_type'] = $params['room_type'];
        
        }
        if(isset($params['check_in'])){
        	$data['check_in'] = $params['check_in'];
        
        } 
        if(isset($params['check_on'])){
        	$data['check_on'] = $params['check_on'];
        
        }       
        if(isset($params['check_in_hotel'])){
        	$data['check_in_hotel'] = $params['check_in_hotel'];
        
        }
        if(isset($params['check_on_hotel'])){
        	$data['check_on_hotel'] = $params['check_on_hotel'];
        
        }

        if(is_numeric($params['status'])){
            $data['status'] = $params['status'];

        }


        $data['update_user_id'] = $params['user_id'];

        $data['update_time'] = $t;




        $this->startTrans();
        try{
            $this->where("company_order_accommodation_id = ".$params['company_order_accommodation_id'])->update($data);

            $result = 1;
            

            
            // 提交事务
            $this->commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            $this->rollback();

        }
        return $result;
    }
	
    /**
     * 修改分公司订单-游客住宿 根据公司订单航班ID以及顾客ID
     */
    public function updateCompanyOrderAccommodationByCompanyOrderNumberAndCustomerId($params){
    
    	$t = time();
    
    	if(!empty($params['room_code'])){
    		$data['room_code'] = $params['room_code'];
    
    	}
    	if(!empty($params['room_type'])){
    		$data['room_type'] = $params['room_type'];
    
    	}
    	if(is_numeric($params['check_in'])){
    		$data['check_in'] = $params['check_in'];
    
    	}
    
    	if(is_numeric($params['check_on'])){
    		$data['check_on'] = $params['check_on'];
    
    	}

        if(isset($params['check_in_hotel'])){
            $data['check_in_hotel'] = $params['check_in_hotel'];

        }
        if(isset($params['check_on_hotel'])){
            $data['check_on_hotel'] = $params['check_on_hotel'];

        }
    
    	if(is_numeric($params['status'])){
    		$data['status'] = $params['status'];
    
    	}
    
    
    	$data['update_user_id'] = $params['user_id'];
    
    	$data['update_time'] = $t;
    
    
    
    	
    	$this->startTrans();
    	try{
    		$result = $this->where("company_order_number = '".$params['company_order_number']."' and customer_id =".$params['customer_id'])->update($data);
    	

    		// 提交事务
    		$this->commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		$this->rollback();
    
    	}
    	
  
    	return $result;
    }


    public function updateAccommodations($params)
    {
        try{
            $this->startTrans();

            $this->table("company_order_accommodations")->where(['company_order_number' => $params['company_order_number']])->update(['status'=>0]);
            $params['accommodation'] = [];
            foreach ($params['accommodations'] as $v)
            {
                $data = [];
                $data['temp_group'] = $v['temp_group'];
                $data['seat_group'] = trim($v['seat_group'],',');
                $data['accommodation_type'] = $v['accommodation_type'];
                $data['room_type'] = $v['room_type'];
                $data['nights'] = $v['nights'];
                $data['hotel'] = $v['hotel'];
                $data['team_product_number'] = $v['team_product_number'];
               
                $data['status'] = 1;
                if ($v['company_order_accommodations_id'])
                {
                    $where = " company_order_accommodations_id = ". $v['company_order_accommodations_id'];
                    $this->table("company_order_accommodations")->where($where)->update($data);
                }
                else
                {
                    $data['company_order_number'] = $params['company_order_number'];
                    $this->table("company_order_accommodations")->insert($data);
                }
                $hotel = '';
                for ($i=0;$i<$data['nights'];$i++)
                {
                    $hotel .= $data['hotel'].',';
                }

                $temp_group = explode(',', $v['temp_group']);
                foreach ($temp_group as $temp_group_v)
                {
                    $data['company_order_customer_id'] = $temp_group_v;
                    $company_order_customer_info = $this->table('company_order_customer')->where(['company_order_customer_id' => $temp_group_v])->find();

                    $data['customer_id'] = $company_order_customer_info['customer_id'];

                    if ($v['accommodation_type'] == 1)
                    {
                        $data['check_in'] = 0 - $data['nights'];
                        $data['check_in_room_type'] = $v['room_type'];
                        $data['check_in_hotel'] = trim($hotel,',');
                    }

                    if ($v['accommodation_type'] == 2)
                    {
                        $data['check_on'] = $data['nights'];
                        $data['check_on_room_type'] = $v['room_type'];
                        $data['check_on_hotel'] = trim($hotel,',');
                    }
                    $data['room_code'] = $v['room_code'];
                    unset($data['room_type']);
                    array_push($params['accommodation'], $data);
                    unset($data['company_order_customer_id']);
                    unset($data['customer_id']);
                }

            }

            $this->where(['company_order_number' => $params['company_order_number']])->update(['check_in_room_type'=>0, 'check_on_room_type'=>0]);
            $this->updateAccommodation($params,'accommodation');
            $this->commit();
            $result = 1;
        } catch (\Exception $e) {
            $this->rollback();
            $result = $e->getMessage();
            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }
        return $result;
    }


    public function getAccommodations($company_order_number)
    {
        try
        {
            $where['company_order_number'] = $company_order_number;
            $where['status'] = 1;
            $result = $this->table("company_order_accommodations")->alias('company_order_accommodations')
                ->field(['company_order_accommodations.*',
                    "(select room_type_name from room_type where company_order_accommodations.room_type = room_type.room_type_id)"=>'room_name'
                    ])
                ->where($where)->select();
        }
        catch (\Exception $e)
        {
            $result = $e->getMessage();
            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }

        return $result;
    }

    public function updateAccommodation($params, $type)
    {
        $t = time();
        try{
            $where['company_order_number'] = $params['company_order_number'];
            $clear_data = [];
            if ($type == 'room')
            {
                $clear_data = [
                    'room_code' => 0,
                    'room_type' => 0
                ];
            }

            if ($type == 'accommodation')
            {
                $clear_data = [
                    'check_in_room_type' => 0,
                    'check_on_room_type' => 0,
                    'check_in' => 0,
                    'check_on' => 0,
                    'check_in_hotel' => '',
                    'check_on_hotel' => ''
                ];
            }

            $this->where($where)->update($clear_data);  //置空原数据
			
            foreach ($params['accommodation'] as $v)
            {
                $data = [];
                $data['company_order_number'] = $params['company_order_number'];
                $where['company_order_customer_id'] = $data['company_order_customer_id'] = $v['company_order_customer_id'];
                $where['customer_id'] = $data['customer_id'] = $v['customer_id'];

                $accommodation_info = $this->where($data)->find();

                if ($v['room_code'])
                {
                    $data['room_code'] = $v['room_code'];
                }

                if ($v['room_type'])
                {
                    $data['room_type'] = $v['room_type'];
                }

                if ($v['check_in_room_type'])
                {
                    $data['check_in_room_type'] = $v['check_in_room_type'];
                }

                if ($v['check_on_room_type'])
                {
                    $data['check_on_room_type'] = $v['check_on_room_type'];
                }

                if ($v['check_in'])
                {
                    $data['check_in'] = $v['check_in'];
                }

                if ($v['check_on'])
                {
                    $data['check_on'] = $v['check_on'];
                }

                if ($v['check_in_hotel'])
                {
                    $data['check_in_hotel'] = $v['check_in_hotel'];
                }

                if ($v['check_on_hotel'])
                {
                    $data['check_on_hotel'] = $v['check_on_hotel'];
                }
                $data['status'] = 1;

                if ($accommodation_info)
                {
                    $data['update_time'] = $t;
                    $data['update_user_id'] = $params['now_user_id'];
                    $this->where($where)->update($data);
                }
                else
                {
                    $data['create_time'] = $data['update_time'] = $t;
                    $data['create_user_id'] = $data['update_user_id'] = $params['now_user_id'];
                    $this->where($where)->update($data);
                    $this->insert($data);
                }

            }

            $result = 1;

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }
        return $result;
    }

}