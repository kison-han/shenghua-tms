<?php


namespace app\index\model\dispatch;


use app\common\help\Contents;
use app\index\model\source\Goods;
use think\Model;

class DispathGoods extends Model
{
    protected $table = 'dispatch_goods';
    protected  $append=['goods_name'];


    public function getGoodsNameAttr($value,$data){
       $goods_id=$data['goods_id'];
       $goodsInfo=Goods::get( $goods_id);
       return $goodsInfo['goods_name'];
    }

   public function  add($goods,$dispatch_id){
        $goodslist=[];
        $this->where('dispatch_id',$dispatch_id)->update(['status'=>0]);

        if(isset($goods['order_number']))
        {
            foreach ($goods['order_number'] as $k=>$v){
                $goodslist[]=[
                    'dispatch_id'=>$dispatch_id,
                    'order_id'=>explode('-',$v)[0],
                    'goods_id'=>explode('-',$v)[1],
                    'order_number'=>'O-'. explode('-',$v)[0],
                    'goods_number'=>$goods['goods_number'][$k],
                    'goods_pack_number'=>$goods['goods_pack_number'][$k],
                    'goods_weight'=>$goods['goods_weight'][$k],
                    'goods_volume'=>$goods['goods_volume'][$k],
                    'create_user_id'=>$goods['user_id'][$k],
                    'update_user_id'=>$goods['user_id'][$k],
                    'create_time'=>time(),
                    'update_time'=>time(),
                    'status'=>1,

                ];
                $orderids[]='O-'. explode('-',$v)[0];
            }
        }
        else
        {
            foreach ($goods as $k=>$v){
                     $goodslist[]=[


                    'dispatch_id'=>$dispatch_id,
                    'goods_id'=>$v['goods_id'],
                    'order_id'=>$v['orders_id'],
                    'order_number'=>'O-'. $v['orders_id'],
                    'goods_number'=>$v['goods_number'],
                    'goods_pack_number'=>$v['goods_pack_number'],
                    'goods_weight'=>$v['goods_weight'],
                    'goods_volume'=>$v['goods_volume'],
                    'create_user_id'=>$v['user_id'],
                    'update_user_id'=>$v['user_id'],
                    'create_time'=>time(),
                    'update_time'=>time(),
                    'status'=>1,
                        ];
            }
        }

        $this->insertAll($goodslist);

    }
    protected function base($query)
    {
        $query->where('status',1);
    }
    public function getDispatchGoods($params,$openCount=false){
        $where = [
            'dispatch_id'=>$params['dispatch_id']

        ];
        if($openCount) {
            $page = isset($params['page']) ? $params['page'] : 1;
            $pageSize = isset($params['page_size']) ? $params['page_size'] :Contents::PAGE_SIZE;
            $data['count'] = $this->where($where)->count();
            $data['page_count']=ceil($data['count']/$pageSize);
            $data['list']=$this->where($where)->page($page,$pageSize)->select();
            return $data;
        }
        else
            return $this->where($where)->select();
    }


}