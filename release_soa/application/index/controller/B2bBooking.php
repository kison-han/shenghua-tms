<?php
namespace app\index\controller;
use app\common\help\Contents;
use app\common\help\Help;
use app\index\model\b2b_tour\B2bTour;
use app\index\model\b2b_booking\B2bBooking as B2bBookingModel;
use app\index\model\b2b_booking\B2bBookingAccommodation;
use app\index\model\b2b_booking\B2bBookingInvoice;
use app\index\model\b2b_booking\B2bBookingRoom;
use app\index\model\b2b_booking\B2bBookingSales;
use app\index\model\b2b_booking\B2bBookingTransfer;
use app\index\model\b2b_tour\B2bTourCommission;
use app\index\model\branchcompany\CompanyOrderAnnex;
use app\index\model\branchcompany\CompanyOrderComment;
use app\index\model\branchcompany\CompanyOrderCustomer;
use app\index\model\btob\B2bReceivableCope;
use app\index\model\product\TeamProduct;


class B2bBooking extends Base
{
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * 创建B2bBooking
     */
    public function addB2bBooking(){
        $params = $this->input();

        $tour_commission_model = new B2bTourCommission();
        $tour_commission_data = $tour_commission_model->getB2bTourCommissionByBtbTourId($params['btb_tour_id']);

        //明细
        $sales_array = [];
        $customer_count = count($params['customer']);
        $arr['qty'] = $customer_count;      //数量
        $arr['is_gst'] = 1;
        $arr['status'] = 'enable';
        if ($tour_commission_data['adult_nett_cn'])
        {
            $arr['product_code_id'] = 2;
            $arr['net_unit_price'] = $tour_commission_data['adult_nett_cn'];               //单价
            $arr['net_total'] = bcmul($arr['net_unit_price'], $customer_count);       //总价
            array_push($sales_array, $arr);
        }

        if ($tour_commission_data['child_wbed_nett_cn'])
        {
            $arr['product_code_id'] = 15;
            $arr['net_unit_price'] = $tour_commission_data['child_wbed_nett_cn'];           //单价
            $arr['net_total'] = bcmul($arr['net_unit_price'], $customer_count);         //总价
            array_push($sales_array, $arr);
        }

        if ($tour_commission_data['child_nbed_nett_cn'])
        {
            $arr['product_code_id'] = 16;
            $arr['net_unit_price'] = $tour_commission_data['child_nbed_nett_cn'];       //单价
            $arr['net_total'] = bcmul($arr['net_unit_price'], $customer_count);       //总价
            array_push($sales_array, $arr);
        }

        if ($tour_commission_data['single_supp'])
        {
            $arr['product_code_id'] = 11;
            $arr['net_unit_price'] = $tour_commission_data['single_supp'];        //单价
            $arr['net_total'] = bcmul($arr['net_unit_price'], $customer_count);    //总价
            array_push($sales_array, $arr);
        }

        if ($tour_commission_data['transfer'])
        {
            //接机
            if ($params['hotel_transfer'] > 0)
            {
                $arr['product_code_id'] = 3;
                $arr['net_unit_price'] = $tour_commission_data['transfer'];        //单价
                $arr['net_total'] = bcmul($arr['net_unit_price'], $customer_count);      //总价
                array_push($sales_array, $arr);
            }
            //送机
            if ($params['airport_transfer'] > 0)
            {
                $arr['product_code_id'] = 4;
                $arr['net_unit_price'] = $tour_commission_data['transfer'];     //单价
                $arr['net_total'] = bcmul($arr['net_unit_price'], $customer_count);          //总价
                array_push($sales_array, $arr);
            }
        }

        if ($tour_commission_data['infant']) {
            $arr['product_code_id'] = 32;
            $arr['net_unit_price'] = $tour_commission_data['infant'];     //单价
            $arr['net_total'] = bcmul($arr['net_unit_price'], $customer_count);   //总价
            array_push($sales_array, $arr);
        }

        if ($tour_commission_data['tipping']) {
            $arr['product_code_id'] = 7;
            $arr['net_unit_price'] = $tour_commission_data['tipping'];    //单价
            $arr['net_total'] = bcmul($arr['net_unit_price'], $customer_count);  //总价
            array_push($sales_array, $arr);
        }

        if ($tour_commission_data['compulsory_program']) {
            $arr['product_code_id'] = 8;
            $arr['net_unit_price'] = $tour_commission_data['compulsory_program'];       //单价
            $arr['net_total'] = bcmul($arr['net_unit_price'], $customer_count);       //总价
            array_push($sales_array, $arr);
        }

        $params['sales'] = $sales_array;

        //获取b2b产品信息
        $b2b_tour_model = new B2bTour();
        $b2b_tour = $b2b_tour_model->getB2bTourGeneralById($params['btb_tour_id']);
        //获取团队产品信息
        $team_product = new TeamProduct();
        $team_product_result = $team_product->getTeamProductBase(['route_template_id' => $b2b_tour['route_template_id'], 'begin_time' => $params['begin_time'], 'can_watch_company_id' => $params['user_company_id']])[0];
        //团队产品的应收 = b2b应付
        $params['cope']['price'] = $team_product_result['settlement_type'] == 1 ? $team_product_result['once_price'] : $team_product_result['real_price'];
        $params['cope']['currency_id'] = $team_product_result['currency_id'];
        $params['cope']['supplier_company_id'] = $team_product_result['company_id']; //供应商分公司
        $params['team_product_id'] = $team_product_result['team_product_id'];
        $params['team_product_number'] = $team_product_result['team_product_number'];
        $params['branch_product_number'] = $b2b_tour['branch_product_number'];
        $params['route_template_id'] = $b2b_tour['route_template_id'];
        $params['route_template_number'] = $b2b_tour['route_number'];
        $params['product_name'] = $b2b_tour['tour_name'];
        $params['account_code'] = $b2b_tour['account_code'];
        $params['receivable_number'] = Help::getNumber(5);
        $params['company_order_number'] = help::getNumber(4);
        $model = new B2bBookingModel();
        $result = $model->addB2bBooking($params);
        $this->outPut($result);
    }


    public function getB2bBooking()
    {
        $params = $this->input();
        $model = new B2bBookingModel();
        if(isset($params['page'])){
            $page_size =  isset($params['page_size'])?$params['page_size']:Contents::PAGE_SIZE;
            $page = ($params['page']-1)*$page_size;
            $count = $model->getB2bBooking($params, true);
            $result = $model->getB2bBooking($params,false,'true',$page,$page_size);
        }else{
            $result = $model->getB2bBooking($params);
        }

        if(isset($params['page'])){
            $data = [
                'count'=>$count,
                'list'=>$result,
                'page_count'=>ceil($count/$page_size)
            ];
            return $this->output($data);
        }else{

            $this->outPut($result);
        }
    }

    public function updateB2bBookingGeneral()
    {
        $params = $this->input();

        $paramRule = [
            'b2b_booking_id'=>'b2b_booking_id',

        ];
        $this->paramCheckRule($paramRule,$params);

        $model = new B2bBookingModel();
        $result = $model->updateB2bBookingGeneral($params);
        $this->outPut($result);
    }

    public function updateB2bBookingCustomer()
    {
        $params = $this->input();

        $model = new CompanyOrderCustomer();
        $result = $model->updateB2bBookingCustomer($params);
        $this->outPut($result);
    }


    public function updateB2bBookingRoom()
    {
        $params = $this->input();

        $model = new B2bBookingRoom();
        $result = $model->updateB2bBookingRoom($params);
        $this->outPut($result);
    }

    public function updateB2bBookingTransfer()
    {
        $params = $this->input();

        $model = new B2bBookingTransfer();
        $result = $model->updateB2bBookingTransfer($params);
        $this->outPut($result);
    }

    public function updateB2bBookingAccommodation()
    {
        $params = $this->input();

        $model = new B2bBookingAccommodation();
        $result = $model->updateB2bBookingAccommodation($params);
        $this->outPut($result);
    }


    public function getB2bBookingGeneral()
    {
        $params = $this->input();

        $model = new B2bBookingModel();
        $result = $model->getB2bBookingGeneralById($params['b2b_booking_id']);

        $this->outPut($result);
    }

    public function getB2bBookingCustomer()
    {
        $params = $this->input();

        $model = new CompanyOrderCustomer();
        $result = $model->getB2bBookingCustomerByBtbBookingId($params['b2b_booking_id']);

        $this->outPut($result);
    }

    public function getB2bBookingRoom()
    {
        $params = $this->input();
        $model = new B2bBookingRoom();
        $result = $model->getB2bBookingRoomByBtbBookingId($params['b2b_booking_id']);
        $this->outPut($result);
    }

    public function getB2bBookingTransfer()
    {
        $params = $this->input();
        $model = new B2bBookingTransfer();
        $result = $model->getB2bBookingTransferByBtbBookingId($params['b2b_booking_id']);
        $this->outPut($result);
    }

    public function getB2bBookingAccommodation()
    {
        $params = $this->input();
        $model = new B2bBookingAccommodation();
        $result = $model->getB2bBookingAccommodationByBtbBookingId($params['b2b_booking_id']);
        $this->outPut($result);
    }

    public function getB2bBookingAnnex()
    {
        $params = $this->input();
        $model = new CompanyOrderAnnex();
        $result = $model->getB2bAnnex($params);
        $this->outPut($result);
    }

    public function getB2bBookingSales()
    {
        $params = $this->input();
        $invoice_model = new B2bBookingInvoice();
        $result['invoice'] = $invoice_model->getB2bBookingInvoiceByBtbBookingId($params['b2b_booking_id']);

        $sales_model = new B2bBookingSales();
        $result['sales'] = $sales_model->getB2bBookingSalesByBtbBookingId($params['b2b_booking_id']);

        $receivable_cope_model = new B2bReceivableCope();
        $result['receivable_cope'] = $receivable_cope_model->getB2bReceivableCope($params);

        foreach ($result['receivable_cope'] as $k=>$v)
        {
            $fee_type_name = Help::getFeeType($v);

            $result['receivable_cope'][$k]['fee_type_name'] = $fee_type_name['fee_type_name'];
        }

        $this->outPut($result);
    }

    public function getB2bBookingPayment()
    {
        $params = $this->input();
        $payment_model = new B2bBookingInvoice();
    }


    public function updateB2bBookingInvoice()
    {
        $params = $this->input();
        $sales_model = new B2bBookingSales();
        $result = $sales_model->updateB2bBookingSales($params);
        $this->outPut($result);
    }

    public function updateB2bBookingAnnex()
    {
        $params = $this->input();
        $model = new CompanyOrderAnnex();
        $result = $model->updateB2bAnnex($params);
        $this->outPut($result);
    }

    public function getB2bBookingComment()
    {
        $params = $this->input();

        $booking_model = new B2bBookingModel();
        $booking_info = $booking_model->where(['b2b_booking_id' => $params['b2b_booking_id']])->find();

        $model = new CompanyOrderComment();
        $result = $model->getCompanyOrderComment(['company_order_number' => $booking_info['company_order_number'], 'status'=>1]);
        $this->outPut($result);
    }

    public function addB2bBookingComment()
    {
        $params = $this->input();

        $booking_model = new B2bBookingModel();
        $booking_info = $booking_model->where(['b2b_booking_id' => $params['b2b_booking_id']])->find();

        $model = new CompanyOrderComment();
        $params['company_order_number'] = $booking_info['company_order_number'];
        $result = $model->addCompanyOrderComment($params);
        $this->outPut($result);
    }

}