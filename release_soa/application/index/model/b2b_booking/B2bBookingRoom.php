<?php

namespace app\index\model\b2b_booking;
use think\Exception;
use think\Model;
use think\Db;
class B2bBookingRoom extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'b2b_booking_room';
    public function initialize()
    {
    	parent::initialize();
    
    }


    public function addB2bBookingRoom($params){

        $data['b2b_booking_id'] = $params['b2b_booking_id'];
        $data['temp_group'] = $params['temp_group'];
        $data['room_type'] = $params['room_type'];
        $data['room_code'] = $params['room_code'];
        $data['charge_num'] = $params['charge_num'];
        $data['charge_type'] = $params['charge_type'];
    	$data['status'] = 1;

    	try{
    		Db::name('b2b_booking_room')->insert($data);
    	} catch (\Exception $e) {

            $result = $e->getMessage();
    		\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		exit();
    	}

    }

    public function updateB2bBookingRoom($params)
    {
        try{
            Db::name('b2b_booking_room')->startTrans();
            Db::name('b2b_booking_room')->where(['b2b_booking_id' => $params['b2b_booking_id']])->update(['status'=>0]);
            foreach ($params['room'] as $v)
            {
                $data['temp_group'] = $v['temp_group'];
                $data['seat_group'] = $v['seat_group'];
                $data['room_type'] = $v['room_type'];
                $data['room_code'] = $v['room_code'];
                $data['charge_num'] = $v['charge_num'];
                $data['charge_type'] = $v['charge_type'];
                $data['status'] = 1;

                if ($v['b2b_booking_room_id'])
                {
                    $where = " b2b_booking_room_id = ". $v['b2b_booking_room_id'];
                    Db::name('b2b_booking_room')->where($where)->update($data);
                }
                else
                {
                    $data['b2b_booking_id'] = $params['b2b_booking_id'];
                    Db::name('b2b_booking_room')->insert($data);
                }
            }
            Db::name('b2b_booking_room')->commit();
            $result = 1;
        } catch (\Exception $e) {
            Db::name('b2b_booking_room')->rollback();
            $result = $e->getMessage();
            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }

        return $result;
    }

    public function getB2bBookingRoomByBtbBookingId($b2b_booking_id)
    {
        try
        {
            $where['b2b_booking_id'] = $b2b_booking_id;
            $where['status'] = 1;
            $result = $this->table("b2b_booking_room")->alias('b2b_booking_room')->where($where)->select();
        }
        catch (Exception $e)
        {
            $result = $e->getMessage();
            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }

        return $result;
    }

}