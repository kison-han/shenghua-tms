<?php

namespace app\index\controller;

use app\common\help\Help;
use think\Request;

class Examination extends Base
{
    /**
     *  圣华
     *  办公自动化
     */
    public function index(){

		return $this->fetch('index');

    }

    /**
     *  圣华
     *  考试题库显示页面
     */
    public function showExaminationManage(){

        $params = Request::instance()->param();

        $data = [
            'page'=>$this->page(),
            'page_size'=>$this->_page_size,
        ];
        $data_all = array_merge($params,$data);

        $result = $this->callSoaErp('post','/examination/getExamination',$data_all);
        $this->assign('examinationResult',$result['data']);
        $this->getPageParams($result);

        return $this->fetch('examination_manage');

    }

    /**
     *  圣华
     *  在线考试显示页面
     */
    public function showOnlineExaminationManage(){

        $params = Request::instance()->param();

        $data = [
            'page'=>$this->page(),
            'page_size'=>$this->_page_size,
        ];
        $data_all = array_merge($params,$data);

        $result = $this->callSoaErp('post','/examination/getExamination',$data_all);
        $this->assign('examinationResult',$result['data']);
        $this->getPageParams($result);

        return $this->fetch('online_examination_manage');
    }

    /**
     *  圣华
     *  考试题库新增页面
     */
    public function showExaminationAdd(){

        if(is_numeric(input('examination_id'))) {
            //修改界面
            $data = [
                'examination_id'=>input('examination_id')
            ];
            //获取题库和题目
            $examination = $this->callSoaErp('post','/examination/getOnlineExamination',$data);
//            echo "<pre>";
//            var_dump($examination['data']);exit;
//            echo "</pre>";
            $this->assign("examinationResult", $examination['data']);
        }

        return $this->fetch('examination_add');

    }

    /**
     *  圣华
     *  提交考卷处理界面
     */
    public function doOnlineExamination(){
        $params = Request::instance()->param();

        //获取题库和题目数据
        $examination = $this->callSoaErp('post','/examination/getOnlineExamination');
        $data = $examination['data'];

        //用户提交数据组装
        $data2 = $params;
        $user = [];

        $count = [];
        $score = [];
        foreach($data['data'] as $k=>$v){
            $count[$k] = count($v['data']);
            $score[$k] = round($v['score']/$count[$k]);
        }

        $sum = 0;
        foreach ($data2 as $key => $value) {
            $arrkey = explode('#', $key);
            if($arrkey[0]=="binary"){
                //判断
                $user['binary'][$arrkey[1]] = $value;
            }else if($arrkey[0]=="single"){
                //单选
                $user['single'][$arrkey[1]] = $value;
            }else if($arrkey[0]=="multiple"){
                //多选
                $user['multiple'][$arrkey[1]] = $value;
            }else if($arrkey[0]=="fill"){
                //填空
                $user['fill'][$arrkey[1]] = $value;
            }else if($arrkey[0]=="brief"){
                //简答
                $user['brief'][$arrkey[1]] = $value;
            }
        }
//        echo "<pre>";
//        var_dump($user);exit;
//        echo "</pre>";
        $dui = 0;
        $cuo = 0;
        foreach($data['data'] as $type=>$each){
            foreach($each['data'] as $kk=>$vv) {
                //获取用户提交答案
                $answer = $user[$type][$kk+1];
                //判断答案是否正确
                if($type=="multiple"){
                    if(in_array($answer,explode(",",$vv['answer']))){
                        $total[$type][$kk] = true;
                        $sum += $score[$type];
                        $dui += count($total[$type][$kk]);
                    }else{
                        $total[$type][$kk] = false;
                        $cuo += count($total[$type][$kk]);
                    }
                }else{
                    if($vv['answer']==$answer){
                        $total[$type][$kk] = true;
                        $sum += $score[$type];
                        $dui += count($total[$type][$kk]);
                    }else{
                        $total[$type][$kk] = false;
                        $cuo += count($total[$type][$kk]);
                    }
                }
            }
        }

        //插入历史题目日志
        var_dump(json_encode($data));exit;

//        echo "总分：100分, 得分：".$sum."分";
//        echo "<br/>";
//        echo "共 ".($dui+$cuo)." 题 "."&nbsp;&nbsp;答对：".$dui."题 &nbsp;&nbsp; 答错：".$cuo."题";
//        echo "<br/>";
//        echo "正确率：".number_format($dui/($dui+$cuo)*100,2)."%";
//        echo "<br/>";
//        if($sum>$data['fraction']){
//            echo "<span style='color:green'>本次考试通过！</span>";
//        }else{
//            echo "<span style='color:red'>本次考试未通过，还需努力！</span>";
//        }
        $this->success("总分：100分, 得分：".$sum."分<br/>共 ".($dui+$cuo)." 题 "."&nbsp;&nbsp;答对：".$dui."题 &nbsp;&nbsp; 答错：".$cuo."题<br/>正确率：".number_format($dui/($dui+$cuo)*100,2)."%<br/>", "/Examination/showOnlineExaminationManage","",3);
    }

    /**
     *  圣华
     *  在线考试新增页面
     */
    public function showOnlineExaminationAdd(){

        if(is_numeric(input('examination_id'))) {
            //修改界面
            $data = [
                'examination_id'=>input('examination_id')
            ];
            //获取题库和题目
            $examination = $this->callSoaErp('post','/examination/getOnlineExamination',$data);
//            echo "<pre>";
//            var_dump($examination['data']);exit;
//            echo "</pre>";
            $this->assign("examinationResult", $examination['data']);
        }

        return $this->fetch('online_examination_add');
    }

    /**
     * 圣华
     * 资料分类新增AJAX / 修改AJAX
     */
    public function addDataGroupAjax(Request $request)
    {

        $data = $request->param();

        if(is_numeric($data['group_id'])){
            //修改资料
            $data['user_id'] = session("user_id");
            $result = $this->callSoaErp('post', '/oadatagroup/updateDataGroupByGroupId',$data);
            return $result;
        }else {
            //新增资料
            $data['user_id'] = session("user_id");
            $result = $this->callSoaErp('post', '/oadatagroup/addDataGroup', $data);
            return $result; //['code' => '400', 'msg' => $data];
        }

    }

    /**
     *  圣华
     *  资料显示页面
     */
    public function showDataManage(){

        $params = Request::instance()->param();

        $data = [
            'page'=>$this->page(),
            'page_size'=>$this->_page_size,
        ];
        $data_all = array_merge($params,$data);

        $result = $this->callSoaErp('post','/oadata/getData',$data_all);

        $this->assign('warehouseResult',$result['data']['list']);
        $this->getPageParams($result);

        return $this->fetch('oadata_manage');

    }

    /**
     *  圣华
     *  资料新增页面
     */
    public function showDataAdd(){

        if(is_numeric(input('data_id'))) {
            //修改界面
            $data = [
                'data_id'=>input('data_id')
            ];
            //获取资料
            $oadata = $this->callSoaErp('post','/oadata/getData',$data);
            $this->assign("oadataResult", $oadata['data'][0]);
        }

        return $this->fetch('oadata_add');

    }

    /**
     * 圣华
     * 资料新增AJAX / 修改AJAX
     */
    public function addDataAjax(Request $request)
    {

        $data = $request->param();

        if(is_numeric($data['data_id'])){
            //修改资料
            $data['user_id'] = session("user_id");
            $result = $this->callSoaErp('post', '/oadata/updateDataByDataId',$data);
            return $result;
        }else {
            //新增资料
            $data['user_id'] = session("user_id");
            $result = $this->callSoaErp('post', '/oadata/addData', $data);
            return $result; //['code' => '400', 'msg' => $data];
        }

    }

    /**
     * 获取资料分类组装json数据
     */
    public function getDataGroupAjax(){
        $result = $this->callSoaErp('post','/oadatagroup/getDataGroupAjax');
        return $result;
    }
}