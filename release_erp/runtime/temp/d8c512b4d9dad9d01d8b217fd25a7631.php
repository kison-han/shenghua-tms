<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:92:"/var/www/html/test_erp/public/../application/index/view/source/accept_goods_add_nomenu2.html";i:1656990247;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






	
<style type="text/css">
	.lianxiren-delete{margin-left: 15px;}
	.lianxiren-item{
		display: flex;width: 80%;padding: 0 10%;
	}
</style>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">

	
	<div class="dd">

		<!-- 内容主体区域 -->
		<div class="content_body">
			<div class='layui-form-item'>
		   		<span class="layui-breadcrumb" lay-separator="-">
					<a>首页</a>
					<a>客户管理</a>
					<a >项目</a>	
					<a >收货方</a>					
					<a><cite>
						新增收货方
					</cite></a>
				</span>
			</div>
			<br/>
			<div class="layui-row layui-col-space10">

			<br/>
			<form class="layui-form"  id="form1" onSubmit="return acceptGoodsAdd()">
				<div class="layui-row layui-col-space10">
					<div class="layui-col-md4">
						<div class="layui-form-item">
							<label class="layui-form-label input-required">项目名称:</label>
							<div class="layui-input-block">
								<select name="project_id" id='project_id' disabled lay-verify="required">
									<?php if(is_array($projectResult) || $projectResult instanceof \think\Collection || $projectResult instanceof \think\Paginator): if( count($projectResult)==0 ) : echo "" ;else: foreach($projectResult as $key=>$vo): ?>
										<option value="<?php echo $vo['project_id']; ?>" <?php if($vo['project_id'] == \think\Request::instance()->get('project_id')): ?>selected<?php endif; ?>> <?php echo $vo['project_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>
					</div>
					<div class="layui-col-md4">
						<div class="layui-form-item">
							<label class="layui-form-label input-required">收货方名称:</label>
							<div class="layui-input-block">
								<input name="accept_goods_company" id="accept_goods_company" value="<?php echo $result['accept_goods_company']; ?>" maxlength="300" lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>
					</div>						
					<div class="layui-col-md4">
						<div class="layui-form-item">
							<label class="layui-form-label input-required">省:</label>
							<div class="layui-input-block">
								<select name="province_id" id='province_id'  lay-verify="required" lay-search lay-filter="sheng-shi">
										<option value=''>--请选择--</option>
									<?php if(is_array($sheng) || $sheng instanceof \think\Collection || $sheng instanceof \think\Paginator): if( count($sheng)==0 ) : echo "" ;else: foreach($sheng as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>" <?php if($vo['city_id'] == $result['province_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>		
							</div>
						</div>
					</div>						
					<div class="layui-col-md4">
						<div class="layui-form-item">
							<label class="layui-form-label input-required">市:</label>
							<div class="layui-input-block">
								<select name="city_id" id='city_id' lay-verify="required" lay-search lay-filter="shi-qu">
									<?php if(is_array($cityResult) || $cityResult instanceof \think\Collection || $cityResult instanceof \think\Paginator): if( count($cityResult)==0 ) : echo "" ;else: foreach($cityResult as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>" <?php if($vo['city_id'] == $result['city_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>		
							</div>
						</div>
					</div>					
					<div class="layui-col-md4">
						<div class="layui-form-item">
							<label class="layui-form-label ">区:</label>
							<div class="layui-input-block">
								<select name="area_id" id='area_id'>
									<option value=''>请选择</option>
									<?php if(is_array($areaResult) || $areaResult instanceof \think\Collection || $areaResult instanceof \think\Paginator): if( count($areaResult)==0 ) : echo "" ;else: foreach($areaResult as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>" <?php if($vo['city_id'] == $result['area_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>		
							</div>
						</div>
					</div>
					<div class="layui-col-md4">
						<div class="layui-form-item">
							<label class="layui-form-label input-required">到站:</label>
							<div class="layui-input-block">
								<select name="accept_location_id" id='accept_location_id' lay-verify="required"  lay-search>
									<?php if(is_array($daozhanResult) || $daozhanResult instanceof \think\Collection || $daozhanResult instanceof \think\Paginator): if( count($daozhanResult)==0 ) : echo "" ;else: foreach($daozhanResult as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>" <?php if($vo['city_id'] == $result['accept_location_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>		
							</div>
						</div>
					</div>					
					<div class="layui-col-md4">
						<div class="layui-form-item">
							<label class="layui-form-label input-required">地址:</label>
							<div class="layui-input-block">
								<input name="accept_goods_address" id="accept_goods_address" value="<?php echo $result['accept_goods_address']; ?>" maxlength="300" lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>
					</div>	
											
					<div class="layui-col-md4">
						<div class="layui-form-item">
							<label class="layui-form-label ">备注:</label>
							<div class="layui-input-block">
								<input name="remark" id="remark" value="<?php echo $result['remark']; ?>" maxlength="300" lay-verify="" placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>
					</div>													
</div>
<div class="layui-row layui-col-space10">
					<div class="layui-col-md4">
						<div class="layui-form-item">
							<label class="layui-form-label">状态:</label>
							<div class="layui-input-block">
								<select name="status" id='status' lay-verify="required">
									<option value="1" selected='selected'>启用</option>
									<option value="0" >禁用</option>
								</select>
							</div>
						</div>
					</div>	
					
</div>
					
					
								
									
									
				


				<br/>
				
				<br/>
				<div class="lianxiren-box" >
				<div class="lianxiren-titile-box" style="margin-left: 80px;margin-bottom: 15px;">
					<div class="">
						<label class="input-required">联系方式</label>
						<div class="layui-btn nav-submit" onclick="addConatct()">
							添加联系方式
						</div>
					</div>
				</div>	
				
				
					<div class="lianxiren-item" style="">
							
						<input name="lianxiren-select" type="radio" value="0" checked="">
						<div class="layui-form-item">
							<label class="layui-form-label input-required" style="width: 80px;">联系人:</label>
							<div class="layui-input-block">
								<input name="accept_goods_name[0]" id="accept_goods_name" value="<?php echo $result['accept_goods_name']; ?>" maxlength="300" lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>
						<div class="layui-form-item">
							<label class="layui-form-label" style="width: 80px;">手机号:</label>
							<div class="layui-input-block">
								<input name="accept_goods_cellphone[0]" id="accept_goods_cellphone" value="<?php echo $result['accept_goods_cellphone']; ?>" maxlength="300" placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>
						
						
					</div>
					
					<?php if(is_array($result['contact']) || $result['contact'] instanceof \think\Collection || $result['contact'] instanceof \think\Paginator): if( count($result['contact'])==0 ) : echo "" ;else: foreach($result['contact'] as $key=>$vo): if($vo['contact_cellphone'] != $result['accept_goods_cellphone']): ?>
						<div class="lianxiren-item" style="">
											<input type="hidden" name="contact_ids[<?php echo $key+1; ?>]" id="" value="" />
										<input name="lianxiren-select" value="<?php echo $key+1; ?>" type="radio" >
										<div class="layui-form-item">
											<label class="layui-form-label input-required" style="width: 80px;"><i>*</i>联系人:</label>
											<div class="layui-input-block">
												<input name="accept_goods_name[<?php echo $key+1; ?>]" id="accept_goods_name" value="<?php echo $vo['contact_name']; ?>" maxlength="300" lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
											</div>
										</div>
										<div class="layui-form-item">
											<label class="layui-form-label input-required" style="width: 80px;"><i>*</i>手机号:</label>
											<div class="layui-input-block">
												<input name="accept_goods_cellphone[<?php echo $key+1; ?>]" id="accept_goods_cellphone" value="<?php echo $vo['contact_cellphone']; ?>" maxlength="300" lay-verify="required|phone" placeholder="" autocomplete="off" class="layui-input" type="text">
											</div>
										</div>
										<div class="layui-btn layui-btn-primary lianxiren-delete" onclick="delContact(this)" style="">
											删除
										</div>
										
									</div>
					<?php endif; endforeach; endif; else: echo "" ;endif; ?>
						
				</div>	
				
				
				<input type='hidden' id='choose_type' name='accept_goods_id'  value="2" />
				<input type='hidden' id='accept_goods_id' name='accept_goods_id'  value="" />
				<input type='hidden' id='project_id' name='project_id'  value="<?php echo \think\Request::instance()->get('project_id'); ?>" />
				<div class="layui-form-item">
					<div class="layui-input-block all-button-center">
						<button class="layui-btn nav-submit" lay-submit="" lay-filter="formDemo" id="dining_add_button">提交</button>
						
					</div>
				</div>
			</form>
		</div>
		<hr/>
		
		
	</div>


</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
<script src='/static/javascript/system/customer3.js'></script>

<script type="text/javascript">
 contactIndex=100;
	function addConatct(){
		contactIndex++;
		$(".lianxiren-box").append(`<div class="lianxiren-item" style="">
							
						<input name="lianxiren-select" value="${contactIndex}" type="radio" >
						<div class="layui-form-item">
							<label class="layui-form-label input-required" style="width: 80px;"><i>*</i>联系人:</label>
							<div class="layui-input-block">
								<input name="accept_goods_name[${contactIndex}]" id="accept_goods_name" value="" maxlength="300" lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>
						<div class="layui-form-item">
							<label class="layui-form-label input-required" style="width: 80px;"><i>*</i>手机号:</label>
							<div class="layui-input-block">
								<input name="accept_goods_cellphone[${contactIndex}]" id="accept_goods_cellphone" value="" maxlength="300" lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>
						<div class="layui-btn layui-btn-primary lianxiren-delete" onclick="delContact(this)" style="">
							删除
						</div>
						
					</div>`);
					form.render()
	}
	function delContact(a){
		
		
		
		$(a).parents(".lianxiren-item").remove();
	}
	
</script>


</body>
</html>
