<?php

namespace app\index\model\source;
use think\Model;
use app\common\help\Help;
use app\index\service\PublicService;
use think\config;
use think\Db;
class CustomerSendGoods extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'customer_send_goods';

    /**
     * 获取发货客户
     * 韩
     */
    public function getCustomerSendGoods($params,$is_count=false,$is_page=false,$page=null,$page_size=20){

        $data = "1=1 ";

        if(isset($params['customer_send_goods_id'])){
            $data.= " and customer_send_goods.customer_send_goods_id= ".$params['customer_send_goods_id'];
        }

        if(isset($params['send_goods_name'])){
            $data.= " and customer_send_goods.send_goods_name like '%".$params['send_goods_name']."%'";
        }

        if(isset($params['status'])){
            $data.= " and customer_send_goods.status = ".$params['status'];
        }

        if($is_count==true){
            $result = $this->table("customer_send_goods")->alias("customer_send_goods")->where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("customer_send_goods")->alias('customer_send_goods')->
                where($data)->limit($page, $page_size)->order('create_time desc')->
                field(['customer_send_goods.customer_send_goods_id', "customer_send_goods.send_goods_name", "customer_send_goods.send_name", "customer_send_goods.phone",  "customer_send_goods.address", "customer_send_goods.remark",
                    "(select nickname  from user where user.user_id = customer_send_goods.create_user_id)"=> 'create_user_name',
                    "(select nickname  from user where user.user_id = customer_send_goods.update_user_id)"=> 'update_user_name',
                    'customer_send_goods.update_time', 'customer_send_goods.create_time', "customer_send_goods.status"])->select();
            }else{
                $result = $this->table("customer_send_goods")->alias('customer_send_goods')->
                where($data)->order('create_time desc')->
                field(['customer_send_goods.customer_send_goods_id', "customer_send_goods.send_goods_name", "customer_send_goods.send_name", "customer_send_goods.phone",  "customer_send_goods.address", "customer_send_goods.remark",
                    "(select nickname  from user where user.user_id = customer_send_goods.create_user_id)"=> 'create_user_name',
                    "(select nickname  from user where user.user_id = customer_send_goods.update_user_id)"=> 'update_user_name',
                    'customer_send_goods.update_time', 'customer_send_goods.create_time', "customer_send_goods.status"])->select();
            }

        }

        return $result;

    }

    /**
     * 添加发货客户
     * 韩
     */
    public function addCustomerSendGoods($params){

        $t = time();

        $data['send_goods_name'] = $params['send_goods_name'];

        if(isset($params['send_name'])){
            $data['send_name'] = $params['send_name'];
        }
        if(isset($params['phone'])){
            $data['phone'] = $params['phone'];
        }
        if(isset($params['address'])){
            $data['address'] = $params['address'];
        }
        if(isset($params['remark'])){
            $data['remark'] = $params['remark'];
        }

        $data['create_time'] = $t;
        $data['update_time'] = $t;
        $data['create_user_id'] = $params['user_id'];
        $data['update_user_id'] = $params['user_id'];
        $data['status'] = $params['status'];

        Db::startTrans();
        try{
            $pk_id = Db::name('customer_send_goods')->insertGetId($data);

            $result = 1;
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
            //\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            //exit();

        }

        return $result;
    }


    /**
     * 修改发货客户
     * 韩
     */
    public function updateCustomerSendGoodsByCustomerSendGoodsId($params){

        $t = time();

        if(!empty($params['send_goods_name'])){
            $data['send_goods_name'] = $params['send_goods_name'];
        }
        if(isset($params['send_name'])){
            $data['send_name'] = $params['send_name'];
        }
        if(isset($params['phone'])){
            $data['phone'] = $params['phone'];
        }
        if(isset($params['address'])){
            $data['address'] = $params['address'];
        }
        if(isset($params['remark'])){
            $data['remark'] = $params['remark'];
        }

        $data['update_user_id'] = $params['user_id'];
        $data['update_time'] = $t;

        Db::startTrans();
        try{
            Db::name('customer_send_goods')->where("customer_send_goods_id = ".$params['customer_send_goods_id'])->update($data);

            $result = 1;
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();

        }

        return $result;
    }

}