<?php


namespace app\index\model\wmsmonitor;


use app\common\help\Help;
use app\index\model\source\Supplier;
use app\index\model\system\User;
use think\Db;
use think\Model;

class THCalc extends Model
{
    protected $table = 'calc_send_th';

    /**
     * 添加温湿度数据发送人
     * 韩
     */
    public function addTHSendUser($params){

        $data['send_user_id'] = $params['send_user_id'];
        $data['calc_date'] = $params['calc_date'];
        $data['status'] = $params['status'];

        Db::startTrans();
        try{
            $this->table("calc_send_th")->insertGetId($data);
            $result = 1;
            // 提交事务
            Db::commit();
        }catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return $result;
    }

}