<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:83:"/var/www/html/test_erp/public/../application/index/view/order/order_follow_add.html";i:1660126516;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






	<title>
  										<?php if(\think\Request::instance()->get('project_id') ==  ''): ?>
										新增项目
										<?php else: ?>
										修改项目
										<?php endif; ?>	
	
	</title>
<style>
td,th{padding:5px}
</style>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">


	<div class="layui-body">


<?php if(!\think\Request::instance()->get('hide_form')): ?>

		<!-- 内容主体区域 -->
		<div class="content_body">
			<div class='layui-form-item'>
		   		<span class="layui-breadcrumb" lay-separator="-">
					<a>首页</a>
					<a>运单管理</a>
					<a>	<cite>发运跟踪</cite></a>					
				
		
					
					
				</span>
			</div>
			<br/>
			<div class="layui-row layui-col-space10">

			<br/>
				<button style="margin-left:50px" onclick="openlayer('/order/showOrderAbnormalAdd?small=1&orders_id=<?php echo \think\Request::instance()->get('orders_id'); ?>&orders_number=<?php echo \think\Request::instance()->get('orders_number'); ?>')">添加异常</button>
				<br><br>
			<table border="1">
			
			<tr>
			<th>运单日期</th><th>承运商</th><th>运单编号</th><th>发货方</th><th>收货方</th><th>收货地址</th><th>收货电话</th><th>收货人</th><th>货物名称</th><th>计费件数</th><th>计费重量</th><th>计费体积</th><th>备注</th>
			
			</tr>
			
			<tr>
			<td><?php echo date("Y-m-d",$result['pickup_time']); ?></td>
			<td><?php echo $result['supplier_name']; ?></td>
			<td><?php echo $result['orders_number']; ?></td>
			<td><?php echo $result['send_goods_company']; ?></td>
			<td><?php echo $result['accept_goods_company']; ?></td>
			<td><?php echo $result['accept_address']; ?></td>
			<td><?php echo $result['accept_cellphone']; ?></td>
			<td><?php echo $result['accept_name']; ?></td>
			<td>
			<?php if(is_array($result['orders_goods_info']) || $result['orders_goods_info'] instanceof \think\Collection || $result['orders_goods_info'] instanceof \think\Paginator): if( count($result['orders_goods_info'])==0 ) : echo "" ;else: foreach($result['orders_goods_info'] as $key=>$vo1): ?> <?php echo $vo1['goods_name']; ?> /<?php endforeach; endif; else: echo "" ;endif; ?>
			</td><td>
			<?php echo $jfjs; ?>
			</td><td>
			<?php echo $jfzl; ?>
			</td><td>
			<?php echo $jftj; ?>
			</td><td>
			<?php echo $result['remark']; ?>
			</td>
			
			</tr>
			
			</table>
			
		
			
			
			<form class="layui-form"  id="form1" onSubmit="return orderFollowAdd()">
			

					<div class="layui-col-md4">
						<div class="layui-form-item">
							<label class="layui-form-label input-required">运单号:</label>
							<div class="layui-input-block">
								<input  id="order_id"  value="<?php echo \think\Request::instance()->get('orders_number'); ?>" readonly="readonly" class="layui-input" type="text">
							</div>
						</div>
					</div>			
					<div class="layui-col-md4 clear">
						<div class="layui-form-item">
							<label class="layui-form-label input-required">是否重要:</label>
							<div class="layui-input-block">
								<input type="checkbox" lay-skin="primary" name='follow_level' class="orderClass" value="1" <?php if($result['follow_level'] == 1): ?>checked<?php endif; ?>>
							</div>
						</div>
					</div>				

<!--					
				<div class='clear'></div>
					<div class="layui-col-md4">
						<div class="layui-form-item">
							<label class="layui-form-label ">跟踪时间:</label>
							<div class="layui-input-block">
								<input  id="follow_time"  name='follow_time'  value="<?php if($result['follow_time'] != ''): ?><?php echo date('Y-m-d',$result['follow_time']); endif; ?>"  class="layui-input" type="text" id='follow_time'>
							</div>
						</div>
					</div>		
					
					
							
		<div class="layui-form-item layui-form-text">
          <label class="layui-form-label">跟踪内容</label>
          <div class="layui-input-block">
            <textarea name="follow_remark" placeholder="请输入内容" class="layui-textarea" style='height:200px;'><?php echo $result['follow_remark']; ?></textarea>
          </div>
        </div>-->





				<input type='hidden'  name='orders_id' id='orders_id' value='<?php echo \think\Request::instance()->get('orders_id'); ?>' />
				<div class="layui-form-item">
					<div class="layui-input-block all-button-center">
						<button class="layui-btn nav-submit" lay-submit="" lay-filter="formDemo" id="dining_add_button">提交</button>
						
					</div>
				</div>
			</form>
		</div>
		<hr/>
		
		
	</div>
<?php endif; ?>
	
	<div style="padding-left:15px"> 
	<table border="1" style="margin:0 auto;">
	 <tr>
	 <th>操作日期</th>
	 <th>操作</th>
	 <th>操作人</th>
	  <th>内容</th>
	 </tr>
	<?php if(is_array($operating) || $operating instanceof \think\Collection || $operating instanceof \think\Paginator): if( count($operating)==0 ) : echo "" ;else: foreach($operating as $key=>$vo): ?>
	
	
	<tr>
	<td><?php echo date("Y-m-d H:i:s",$vo['create_time']); ?></td><td><?php echo $baseConfig['order']['order_status'][$vo['order_status']]['order_status_name']; ?></td><td><?php echo $vo['create_user_name']; ?></td>
	<td><input <?php if(!\think\Request::instance()->get('hide_form')): ?> style="width:500px" <?php endif; ?> onchange="changeOperating(this,<?php echo $vo['id']; ?>)" value="<?php echo $vo['remark']; ?>"></input></td>
	</tr>
	<?php endforeach; endif; else: echo "" ;endif; ?>
	</table>
	</div>
</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
<script src='/static/javascript/system/customer.js'></script>
<script>

var layer = layui.layer;

var form = layui.form;

//加载日期控件
layui.laydate.render({
    elem: '#follow_time' ,//input的id
    trigger: 'click',//解决一闪而过的问题
});

function orderFollowAdd(){

    $.ajax({
        type: "POST",
        url: '/order/addOrderFollowAjax',
        data: $('#form1').serializeArray(),
        success: function(data){
            // console.log(data);
            if(data.code!=200){
                layer.msg(data.msg);
                return false;
            }else if(data.code==200) {
            	layer.msg('修改成功'); 
				window.parent.layer.closeAll();
            }
        },
        error:function(XMLHttpRequest, textStatus, errorThrown){
            layer.msg('验证失败')
        }
    });
    return false;
}
function changeOperating(obj,id){
$.ajax({
url:"/order/changeOperatingAjax",
data:{'id':id,'remark':$(obj).val()},
success:function(e){
layer.msg("修改成功")
}

})
}
</script>
</body>
</html>
