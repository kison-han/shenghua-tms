<?php
namespace app\index\controller;
use app\common\help\Contents;
use app\common\help\Help;
use app\index\model\b2b_operate\B2bOperate as B2bOperateModel;

/*****
* Hugh 
*2020-01-03
***/
class B2bOperate extends Base
{
	public function __construct()
    {
        parent::__construct();
    }

	public function approvePlan(){
		$params = $this->input(); 
		
		$mB2bOperate = new B2bOperateModel();
		$result = $mB2bOperate->approvePlan($params);

		if($params['page']){
			unset($params['page']);
			$dd['count'] =  count($mB2bOperate->approvePlan($params));
			$page_count  = ceil($dd['count']/$params['page_size']);
			$dd['page_count'] = $page_count;
			$dd['list'] = $result; 
			$this->outPut($dd);
		}

		$this->outPut($result);
	}

	public function updateApprovePlan(){
		$params = $this->input(); 
		
		$mB2bOperate = new B2bOperateModel();
		$result = $mB2bOperate->updateApprovePlan($params); 
		$this->outPut($result);
	}


	public function top10AgentsByState(){
		$params = $this->input();
		$mB2bOperate = new B2bOperateModel();
		$result = $mB2bOperate->top10AgentsByState($params); 
		$this->outPut($result);
	}


	public function reportBookings(){

		$params = $this->input();
		$mB2bOperate = new B2bOperateModel();
		$result = $mB2bOperate->reportBookings($params); 
		$this->outPut($result);
	}

	//获取代理的 State
	public function getDistributorState(){
		$mB2bOperate = new B2bOperateModel();
		$result = $mB2bOperate->getDistributorState(); 
		$this->outPut($result);
	}
	
	public function reportTours(){
		$params = $this->input();
		$mB2bOperate = new B2bOperateModel();
		$result = $mB2bOperate->reportTours($params); 
		$this->outPut($result);
	}

	public function reportAgents(){
		$params = $this->input();
		$mB2bOperate = new B2bOperateModel();
		$result = $mB2bOperate->reportAgents($params); 
		$this->outPut($result);
	}

	public function noOfPax(){
		$params = $this->input();
		$mB2bOperate = new B2bOperateModel();
		$result = $mB2bOperate->noOfPax($params); 
		$this->outPut($result);
	}

	public function dashboard(){
		$params = $this->input();
		$mB2bOperate = new B2bOperateModel();
		$result = $mB2bOperate->dashboard($params); 
		$this->outPut($result);
	}


	public function export_to_msword(){
		$params = $this->input();
		$mB2bOperate = new B2bOperateModel();
		$result = $mB2bOperate->export_to_msword($params); 
		$this->outPut($result);
	}

	public function invoice(){
		$params = $this->input();
		$mB2bOperate = new B2bOperateModel();
		$result = $mB2bOperate->invoice($params); 
		$this->outPut($result);
	}

	public function upInvoice(){
		$params = $this->input();
		$mB2bOperate = new B2bOperateModel();
		$result = $mB2bOperate->upInvoice($params); 
		$this->outPut($result);
	}

}

