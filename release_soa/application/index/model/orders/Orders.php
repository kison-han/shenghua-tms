<?php

namespace app\index\model\orders;


use app\index\model\shipment\Shipment;
use app\index\model\shipment\ShipmentGoods;
use app\index\model\source\AcceptGoods;
use app\index\model\source\Goods;
use app\index\model\source\Project;
use app\index\model\source\ProjectPricingConfigure;
use app\index\model\source\SendGoods;
use app\index\model\system\User;
use think\Exception;
use think\Model;
use app\common\help\Help;
use app\index\service\PublicService;
use think\config;
use think\Db;

class Orders extends Model
{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'orders';
    private $_public_service;

    public function initialize()
    {
        $this->_public_service = new PublicService();
        parent::initialize();

    }

    public function upload()
    {
        return $this->hasMany('OrdersUpload');
    }

    public function goods()
    {
        return $this->hasMany('Ordersgoods');
    }


//获取回单图片
    public function getReceipt($params)
    {
        $orders_id = $params['orders_id'];
        $where['orders_id'] = $orders_id;


        $order_info = $this->with(['goods'])->where($where)->find();
        $where['type'] = isset($params['type']) ? $params['type'] : 1;
        $order_info['upload'] = (new OrdersUpload())->where($where)->select();

        return ['list' => $order_info];
    }

//添加回单
    public function addReceipt($params)
    {

        foreach ($params['image'] as $k => $v) {

            $OrdersUpload = new OrdersUpload();
            $OrdersUpload->insert([
                'orders_id' => $params['orders_id'],
                'type' => isset($params['type']) ? $params['type'] : 1,
                'url' => $v,
                'create_time' => time()
            ]);
        }

        $orderInfo = $this->where('orders_id', $params['orders_id'])->find();
        if ($orderInfo) {

            if(isset($params['image']))
            {
            if (isset($params['type']))
                $orderInfo->is_upload = 2 | $orderInfo->is_upload;
            else
                $orderInfo->is_upload = 1 | $orderInfo->is_upload;
            }

            $orderInfo->receipt_status=1;
            $orderInfo->receipt_count=count($params['image']);
            $orderInfo->update_time=time();
            $orderInfo->receipt_accept_time=time();
            $orderInfo->receipt_accept_user_id=$params['user_id'];
            $orderInfo->update_user_id=$params['user_id'];
            $orderInfo->save();
        }

        if (isset($params['type']))
            return;

        $orderInfo = $this->where('orders_id', $params['orders_id'])->find();

        if (count($params['image']) > 0)
            $orderInfo->receipt_status = 1;
        else
            $orderInfo->receipt_status = 0;

        //$orderInfo->order_status = 7;
        if (isset($params['abnormal_money'])) {
            $orderInfo->abnormal_status = 1;

            $abnormal_model = new OrdersAbnormal();
            $abnormal_model->add($params);

        }
        Db::startTrans();
        try {
            $data['receipt_status'] = 1;
            $this->where('orders_id=' . $params['orders_id'])->update($data);
            $orderInfo->insert();
            $result = 1;
            Db::commit();
        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
            \think\Response::create(['code' => '400', 'msg' => $result], 'json')->send();
            exit();

        }

        return $result;

    }


    /**
     * 添加订单
     * 胡
     */
    public function addOrders($params)
    {
        $t = time();
        $send_goods_model = new SendGoods();
        $send_goods_info = $send_goods_model->parseSendGoods($params);
        $params = array_merge($params, $send_goods_info);
        $accept_goods_model = new AcceptGoods();
        $accept_goods_info = $accept_goods_model->parseAcceptGoods($params);
        $params = array_merge($params, $accept_goods_info);

        $data['project_id'] = $params['project_id'];
        $p_model = new Project();
        $p_info = $p_model->where('project_id', $params['project_id'])->find();

        $data['send_goods_company'] = $params['send_goods_company'];
        $data['accept_goods_company'] = $params['accept_goods_company'];

        $data['is_free'] = $params['is_free'];//是否免费
        $data['pay_type'] = $params['pay_type'];//支付方式
        $data['bargain_type'] = $params['bargain_type'];
        // if ($data['bargain_type'] == 2) {

        $data['delivery_method'] = $params['delivery_method'];

        $data['company_id'] = $params['choose_company_id'];
        $data['transportation_type'] = $params['transportation_type'];

        $data['scb_number'] = $params['scb_number'];

        $data['follow_remark'] = " ";
        $data['send_location_id'] = $params['send_location_id'];
        $data['accept_location_id'] = $params['accept_location_id'];
        $data['send_goods_id'] = $params['send_goods_id'];
        $data['send_province_id'] = $params['send_province_id'];
        $data['send_city_id'] = $params['send_city_id'];
        $data['send_area_id'] = $params['send_area_id'];
        $data['send_address'] = $params['send_address'];
        $data['send_name'] = $params['send_name'];
        $data['send_cellphone'] = $params['send_cellphone'];
        $data['accept_goods_id'] = $params['accept_goods_id'];
        $data['accept_province_id'] = $params['accept_province_id'];
        $data['accept_city_id'] = $params['accept_city_id'];
        $data['accept_area_id'] = $params['accept_area_id'];
        $data['accept_address'] = $params['accept_address'];
        $data['accept_name'] = $params['accept_name'];
        $data['accept_cellphone'] = $params['accept_cellphone'];
        $data['dfk'] = $params['dfk'];
        //客户单号
        if (!empty($params['customer_order_number'])) {
            $data['customer_order_number'] = $params['customer_order_number'];
        }if (!empty($params['khydh'])) {
            $data['khydh'] = $params['khydh'];
        }


        //代收货款
        if (!empty($params['replacement_prive'])) {
            $data['replacement_prive'] = $params['replacement_prive'];

        }

        //保险货值
        if (!empty($params['insurance_goods'])) {
            $data['insurance_goods'] = $params['insurance_goods'];

        }
        //提货时间
        if (!empty($params['pickup_time'])) {
            $data['pickup_time'] = $params['pickup_time'];

        }
        //送货时间
        if (!empty($params['send_time'])) {
            $data['send_time'] = $params['send_time'];

        }
        if (!empty($params['remark'])) {
            $data['remark'] = $params['remark'];

        }if (!empty($params['customer_remark'])) {
            $data['customer_remark'] = $params['customer_remark'];

        }


        $data['orders_number'] = $params['orders_number'];
        $data['order_status'] = 1;
        $data['create_time'] = $t;
        $data['create_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        $data['update_user_id'] = $params['user_id'];
        $data['status'] = 1;


        Db::startTrans();
        try {

            $pickuptime = date('Ymd', $params['pickup_time']);

            $times = Db::name('orders')->where("FROM_UNIXTIME(pickup_time,'%Y%m%d') =$pickuptime and project_id =" . $params['project_id'])->count() + 1;
            $pk_id = Db::name('orders')->insertGetId($data);


            $times = sprintf('%04d', $times);

            $orders_number = $p_info['zjm'] . $pickuptime . $times;


            $this->_public_service->setNumber('orders', 'orders_id', $pk_id, 'orders_number', $data['orders_number'], $orders_number);
            if (!empty($params['scb_number'])) {


                $equipmentParams['equipment_number_in'] = $params['scb_number'];
                $equipmentWhere['equipment_status'] = 1;
                $equipmentWhere['orders_number'] = $orders_number;
                Db::name('equipment')->where($equipmentParams)->update($equipmentWhere);

            }

            $price_count=[];
            $transport_fee=0;
            $send_fee=0;
            $pack_fee=0;

            $goods_model = new Goods();
            $income_model=new OrderIncome();
            //添加完成后再添加到订单商品表中
            for ($i = 0; $i < count($params['goods_name']); $i++) {
                $orders_goods = [];
                $orders_goods['orders_id'] = $pk_id;

                if ($params['goods_id'][$i]) {
                    $orders_goods['goods_id'] = $params['goods_id'][$i];
                } else {
                    $goods_data = [];
                    $goods_data['goods_name'] = $params['goods_name'][$i];
                    $goods_data['project_id'] = $params['project_id'];
                    $goods_data['goods_packaging_type'] = $params['estimated_pack_count'][$i];
                    $goods_data['goods_cost_unit'] = 1;
                    $goods_data['goods_specification'] = "";
                    $goods_data['goods_weight'] = $params['estimated_count'][$i] > 0 ? ($params['estimated_weight'][$i] / $params['estimated_count'][$i]) : 0;
                    $goods_data['goods_volume'] = $params['estimated_count'][$i] > 0 ? ($params['estimated_volume'][$i] / $params['estimated_count'][$i]) : 0;
                    $goods_data['remark'] = "自动添加";
                    $goods_data['calc_way'] = 1;
                    $goods_data['status'] = 1;
                    $goods_data['create_time'] = time();
                    $goods_data['create_user_id'] = $params['user_id'];

                    $base_goods_info = $goods_model->where('project_id', $params['project_id'])->where('goods_name', 'like', trim($goods_data['goods_name']) )->find();
                    if ($base_goods_info) {
                        $orders_goods['goods_id'] = $base_goods_info->goods_id;
                    } else {
                        $orders_goods['goods_id'] = $goods_model->pubInsert($goods_data);
                    }


                }




                $price_data=[];
                $base_goods_info=$goods_model->where('goods_id',$orders_goods['goods_id'])->find();
                if($base_goods_info)
                if($base_goods_info->billing_unit)
                {
                    $price_data['billing_unit']=$base_goods_info->billing_unit;
                    $price_data['project_id']=$params['project_id'];
                    $price_data['departure_id']=$params['send_location_id'];
                    $price_data['arrival_id']=$params['accept_location_id'];
                    $price_data['is_self_raise']=$params['delivery_method']==1?2:1;
                    switch ($price_data['billing_unit']){
                        case '1':$price_data['interval']=$params['estimated_weight'][$i];break;
                        case '2':$price_data['interval']=$params['estimated_volume'][$i];break;
                        case '3':$price_data['interval']=$params['estimated_count'][$i];break;
                        case '4':$price_data['interval']=$params['estimated_pack_count'][$i];break;
                        case '7':$price_data['interval']=$params['estimated_pack_count'][$i];break;
                        default: $price_data['interval']=1; break;
                    }

                    $ProjectPricingConfigure=new ProjectPricingConfigure();
                    $price_config=$ProjectPricingConfigure->getProjectPricingConfigureAndPrice($price_data);


                    $transport_fee+=intval($price_data['interval'])* floatval($price_config['starting_price']);
                    $send_fee+=intval($price_config['delivery_price']);
                    $pack_fee+=intval($price_config['packing_price']);


                }






                $orders_goods['estimated_count'] = $params['estimated_count'][$i];
                $orders_goods['estimated_pack_count'] = $params['estimated_pack_count'][$i];
                $orders_goods['estimated_pack_unit'] = $params['estimated_pack_unit'][$i];
                $orders_goods['estimated_weight'] = $params['estimated_weight'][$i];
                $orders_goods['estimated_volume'] = $params['estimated_volume'][$i];
                $orders_goods['realy_count'] = $params['realy_count'][$i];
                $orders_goods['realy_pack_count'] = $params['realy_pack_count'][$i];
                $orders_goods['realy_pack_unit'] = $params['realy_pack_unit'][$i];
                $orders_goods['realy_weight'] = $params['realy_weight'][$i];
                $orders_goods['realy_volume'] = $params['realy_volume'][$i];
                $orders_goods['create_time'] = $t;
                $orders_goods['create_user_id'] = $params['user_id'];
                $orders_goods['update_time'] = $t;
                $orders_goods['update_user_id'] = $params['user_id'];
                $orders_goods['status'] = 1;

                Db::name('orders_goods')->insertGetId($orders_goods);
            }

            $fee_data['create_user_id']=$params['user_id'];
            $fee_data['update_user_id']=$params['user_id'];
            $fee_data['create_time']=time();
            $fee_data['update_time']=time();
            $fee_data['status']=1;
            $fee_data['remark']="计价配置添加";
            $fee_data['orders_id']=$pk_id;


            if($transport_fee)
            {
                $fee_data['money']=$transport_fee;
                $fee_data['cost_name']="运费";
                $fee_data['cost_id']=1;
                $income_model->insert($fee_data);
            }if($send_fee)
            {
                $fee_data['money']=$send_fee;
                $fee_data['cost_name']="送货费";
                $fee_data['cost_id']=3;
                $income_model->insert($fee_data);
            }if($pack_fee)
            {
                $fee_data['money']=$pack_fee;
                $fee_data['cost_name']="包装费";
                $fee_data['cost_id']=12;
                $income_model->insert($fee_data);
            }
           $order_inf=$this->where('orders_id',$pk_id)->find();
            $order_inf->money=$pack_fee+$send_fee+$transport_fee;
            $order_inf->save();

            $result = 1;
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
            //\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            //exit();

        }





        return $result;
    }

    /**
     * 获取订单
     * 胡
     */
    public function getOrder($params, $is_count = false, $is_page = false, $page = null, $page_size = 20)
    {


        $data = "1=1 ";

        if(is_numeric($params['is_over_time']))
        {
            $t=time();
            if ($params['is_over_time']==1)
            $data.=" and true_time is null and send_time < $t";
            if ($params['is_over_time']==0)
                $data.=" and true_time is not null ";
        }

        if (is_numeric($params['sign'])) {
            if ($params['sign'] == 1)
                $data .= " and order_status = 6 ";
            else
                $data .= " and order_status < 6 ";

        }
        if (is_numeric($params['total_fee'])) {

            if ($params['total_fee'] == 1) {
                $data .= " and (money>0 or is_free=1) ";
            }
            if ($params['total_fee'] == 0) {

                $data .= " and (money=0 and is_free=0) ";

            }


        }


        if (is_numeric($params['is_free'])) {
            $data .= " and orders.is_free = " . $params['is_free'];
        }
        /*
        if (is_numeric($params['send_goods_id'])) {
            $data .= " and orders.send_goods_id = " . $params['send_goods_id'];
        }
        if (is_numeric($params['accept_goods_id'])) {
            $data .= " and orders.accept_goods_id = " . $params['accept_goods_id'];
        }
        */

        if (is_numeric($params['follow_level'])) {
            $data .= " and orders.follow_level = " . $params['follow_level'];

        }
        if (is_numeric($params['is_upload'])) {
            if ($params['is_upload'] > 0)
                $data .= " and orders.is_upload >= " . 2;
            else
                $data .= " and (orders.is_upload <=1 or orders.is_upload is null) ";

        }
        if (is_numeric($params['verify_status'])) {
            $data .= " and orders.verify_status = " . $params['verify_status'];
        }
        if (is_numeric($params['shipment_status'])) {
            $data .= " and orders.shipment_status = " . $params['shipment_status'];
        }
        if (is_numeric($params['abnormal_status'])) {
            $data .= " and orders.abnormal_status = " . $params['abnormal_status'];
        }
        if (is_numeric($params['status'])) {
            $data .= " and orders.status = " . $params['status'];
        }

        if (isset($params['create_time'])) {
            if (strtotime($params['create_time'])) {
                $data .= " and orders.create_time >= " . strtotime($params['create_time']);
                $data .= " and orders.create_time < " . (strtotime($params['create_time_end']) + 86400);
            }
        }
        if (isset($params['pickup_time'])) {
            if (strtotime($params['pickup_time'])) {
                $data .= " and orders.pickup_time >= " . strtotime($params['pickup_time']);
            }
        }
        if (!empty($params['pickup_time_end']))
            $data .= " and orders.pickup_time < " . (strtotime($params['pickup_time_end']) + 86400);


        if (isset($params['send_time'])) {
            if (strtotime($params['send_time'])) {
                $data .= " and orders.send_time >= " . strtotime($params['send_time']);
            }
        }
        if (!empty($params['send_time_end']))
            $data .= " and orders.send_time < " . (strtotime($params['send_time_end']) + 86400);


        if (isset($params['follow_time'])) {
            if (strtotime($params['follow_time'])) {
                $data .= " and orders.follow_time >= " . strtotime($params['follow_time']);
            }
        }
        if (!empty($params['follow_time_end']))
            $data .= " and orders.follow_time < " . (strtotime($params['follow_time_end']) + 86400);

        if (isset($params['keywords'])) {

            $data .= " and concat(orders.orders_number,orders.customer_order_number,orders.accept_name,orders.send_name,orders.send_cellphone,orders.accept_cellphone,orders.accept_address) like '%" . $params['keywords'] . "%'";
        }
        if (is_numeric($params['receipt_status'])) {
            $data .= " and orders.receipt_status =" . (int)($params['receipt_status']);
        }
        if (!empty($params['pay_type'])) {
            $data .= " and orders.pay_type =" . $params['pay_type'];
        }

        if (!empty($params['project_id'])) {
            $data .= " and orders.project_id =" . $params['project_id'];
        }

        if (!empty($params['multi_project_id'])) {
            $data .= " and orders.project_id in (" . $params['multi_project_id'] . ")";
        }


        if (!empty($params['orders_number'])) {
            $data .= " and orders.orders_number like '%" . trim($params['orders_number']) . "%'";
        }
        if (!empty($params['customer_order_number'])) {
            $data .= " and orders.customer_order_number like '%" . trim($params['customer_order_number']) . "%'";
        }
        if (!empty($params['supplier_shipment_number'])) {
            $data .= " and shipment.supplier_shipment_number like '%" . trim($params['supplier_shipment_number']) . "%'";
        }

        if (!empty($params['accept_name'])) {
            $data .= ' and orders.accept_name like "%' . $params['accept_name'] . '%"';
        }
        if (!empty($params['send_goods_company'])) {
            $data .= ' and orders.send_goods_company like "%' . $params['send_goods_company'] . '%"';
        }
        if (!empty($params['accept_goods_company'])) {
            $data .= ' and orders.accept_goods_company like "%' . $params['accept_goods_company'] . '%"';
        }


        if (!empty($params['accept_cellphone'])) {
            $data .= ' and orders.accept_cellphone like "%' . trim($params['accept_cellphone']) . '%"';
        }
        if (!empty($params['supplier_uuid'])) {
            $supplier_array = explode(",", $params['supplier_uuid']);
            $supplier_str = "";
            foreach ($supplier_array as $k1 => $v1) {
                $supplier_array[$k1] = "'" . $v1 . "'";
            }
            $supplier_str = implode(',', $supplier_array);

            $data .= ' and orders.supplier_uuid in (' . $supplier_str . ')';
        }
        if (!empty($params['shipment_number'])) {
            $data .= ' and orders.shipment_number like "%' . $params['shipment_number'] . '%"';
        }
        if (is_numeric($params['order_status'])) {
            if (is_array($params['order_status'])) {
                $data .= " and orders.order_status " . $params['order_status'][0] . $params['order_status'][1];
            } else
                $data .= " and orders.order_status =" . $params['order_status'];
        }

        if (is_numeric($params['choose_company_id'])) {

            $data .= " and orders.company_id =" . $params['choose_company_id'];
        }
        if (!empty($params['multi_order_status'])) {

            $data .= " and orders.order_status in (" . $params['multi_order_status'] . ")";
        }


        if (is_numeric($params['orders_id_is_like']) && $params['orders_id_is_like'] == 1) {
            $data .= " and orders.orders_id in (" . $params['orders_id'] . ")";

        } else {
            if (is_numeric($params['orders_id'])) {
                $data .= " and orders.orders_id =" . $params['orders_id'];
            }
        }


        if ($is_count == true) {
            $result = $this->table("orders")->where($data)->count();
        } else {
            if ($is_page == true) {
                $result = $this->table("orders")->

                join("project", 'project.project_id= orders.project_id', 'left')->
                join("transport", 'transport.order_number= orders.orders_number', 'left')->

                where($data)->limit($page, $page_size)->order('orders.create_time desc')->
                field(['orders.*', 'project.project_name',
                    //'send_goods_company', 'accept_goods.accept_goods_company',
                    "(select nickname  from user where user.user_id = orders.receipt_send_user_id)" => 'receipt_send_user_name',
                    "(select nickname  from user where user.user_id = orders.receipt_accept_user_id)" => 'receipt_accept_user_name',
                    "(select nickname  from user where user.user_id = orders.create_user_id)" => 'create_user_name',
                    "(select nickname  from user where user.user_id = orders.update_user_id)" => 'update_user_name',
                ])->select();


            } else {
                $result = $this->table("orders")->alias('orders')->

                join("project", 'project.project_id= orders.project_id')->
                join("transport", 'transport.order_number= orders.orders_number', 'left')->

                where($data)->order('orders.create_time desc')->
                field(['orders.*', 'project.project_name',
                    // 'send_goods_company', 'accept_goods.accept_goods_company',

                    "(select nickname  from user where user.user_id = orders.create_user_id)" => 'create_user_name',
                    "(select nickname  from user where user.user_id = orders.update_user_id)" => 'update_user_name',

                ])->select();
            }
        }
        for ($i = 0; $i < count($result); $i++) {
            if (!empty($result[$i]['supplier_uuid'])) {
                $arr = explode(',', $result[$i]['supplier_uuid']);
                $kk = '';
                for ($j = 0; $j < count($arr); $j++) {
                    $arrw['supplier_uuid'] = $arr[$j];
                    $r = $this->table("supplier")->where($arrw)->select();
                    if ($j == 0) {
                        $kk = $r[0]['supplier_name'];
                    } else {
                        $kk .= ',' . $r[0]['supplier_name'];
                    }
                }

                $result[$i]['supplier_name'] = $kk;
            } else {
                $result[$i]['supplier_name'] = '';
            }
        }

        foreach ($result as $k => $v) {
//             $fee = DispatchFee::where('orders_id', $v['orders_id'])->sum("true_money");
            $result[$k]['real_money'] = OrderIncome::where('orders_id', $v['orders_id'])->sum('money');
            $result[$k]['shipment_info'] = Db::name("shipment")->where('orders_number', $v['orders_number'])->select();

            $shipment_count=0;
            $shipment_pack_count=0;
            $shipment_weight=0;
            $shipment_volume=0;
            if($result[$k]['shipment_info'])
            {   $shipmen_uuids=array_column($result[$k]['shipment_info'],'shipment_uuid');
                $shipment_goods=(new ShipmentGoods())->whereIn('shipment_uuid',$shipmen_uuids)->where('status',1)->select();
                $shipment_count=array_sum(array_column($shipment_goods,'shipment_count'));
                $shipment_pack_count=array_sum(array_column($shipment_goods,'shipment_pack_count'));
                $shipment_weight=array_sum(array_column($shipment_goods,'shipment_weight'));
                $shipment_volume=array_sum(array_column($shipment_goods,'shipment_volume'));

            }
            $result[$k]['shipment_count']=$shipment_count?$shipment_count:0;
            $result[$k]['shipment_pack_count']=$shipment_pack_count?$shipment_pack_count:0;
            $result[$k]['shipment_weight']=$shipment_weight?$shipment_weight:0;
            $result[$k]['shipment_volume']=$shipment_volume?$shipment_volume:0;

            $result[$k]['is_upload_photo'] = OrdersUpload::where('orders_id', $v['orders_id'])->where('type', 2)->count();

            $result[$k]['operating_info'] = OrdersOperating::where('orders_id', $result[$k]['orders_id'])->select();

            //异常订单计算
            $abnormal_count = OrdersAbnormal::where('orders_id', $v['orders_id'])->where('status',1)->count();
            $result[$k]['abnormal_count'] = $abnormal_count;
            //异常金额
            $abnormal_money = OrdersAbnormal::where('orders_id', $v['orders_id'])->where('status',1)->sum('abnormal_money');
            $result[$k]['abnormal_money'] = $abnormal_money;

        }


        return $result;

    }

    /**
     * 获取订单
     * 胡
     */
    public function getShipmentNeedOrder($params, $is_count = false, $is_page = false, $page = null, $page_size = 20)
    {
        $data = "1=1 ";

        if (isset($params['orders_number'])) {
            $data .= " and orders.orders_number like '%" . $params['orders_number'] . "%'";
        }

        if (isset($params['shipment_status'])) {
            $data .= " and orders.shipment_status = '" . $params['shipment_status'] . "'";
        }
        if (is_numeric($params['start_pickup_time'])) {
            $data .= " and orders.pickup_time >= " . $params['start_pickup_time'];
        }
        if (is_numeric($params['end_pickup_time'])) {
            $data .= " and orders.pickup_time <= " . $params['end_pickup_time'];
        }
        if (is_numeric($params['project_id'])) {
            $data .= " and project.project_id = " . $params['project_id'];
        }
        if (is_numeric($params['status'])) {
            $data .= " and orders.status = " . $params['status'];
        }
        if (is_numeric($params['is_short_barge'])) {
            $data .= " and orders.is_short_barge = " . $params['is_short_barge'];
        }
        if (is_numeric($params['choose_company_id'])) {
            $data .= " and orders.company_id = " . $params['choose_company_id'];
        }
        if (!empty($params['orders_number_array'])) {
            $data .= " and orders.orders_number in(" . $params['orders_number_array'] . ")";
        }

        if (!empty($params['send_goods_company'])) { //发货方
            $data .= ' and orders.send_goods_company like "%' . $params['send_goods_company'] . '%"';
        }
        if (!empty($params['accept_location_name'])) { //到站
            $data .= ' and city.city_name like "%' . $params['accept_location_name'] . '%"';
        }
        if (!empty($params['accept_goods_company'])) { //收货方
            $data .= ' and orders.accept_goods_company like "%' . $params['accept_goods_company'] . '%"';
        }
//        error_log(print_r($data, 1));
        if ($is_count == true) {
            $result = $this->table("orders")->
            join("project", 'project.project_id= orders.project_id', 'left')->
            join("city city", 'city.city_id = orders.accept_location_id')->
            where($data)->count();
        } else {
            if ($is_page == true) {
                $result = $this->table("orders")->

                join("project", 'project.project_id= orders.project_id', 'left')->
                join("city city", 'city.city_id = orders.accept_location_id')->


                where($data)->limit($page, $page_size)->order('orders.create_time desc')->
                field(['orders.*', 'project.project_name',

                    "(select nickname  from user where user.user_id = orders.create_user_id)" => 'create_user_name',
                    "(select nickname  from user where user.user_id = orders.update_user_id)" => 'update_user_name',

                ])->select();


            } else {
                $result = $this->table("orders")->alias('orders')->

                join("project", 'project.project_id= orders.project_id')->
                join("city city", 'city.city_id = orders.accept_location_id')->


                where($data)->order('orders.create_time desc')->
                field(['orders.*', 'project.project_name',

                    "(select nickname  from user where user.user_id = orders.create_user_id)" => 'create_user_name',
                    "(select nickname  from user where user.user_id = orders.update_user_id)" => 'update_user_name',

                ])->select();
            }
        }


        return $result;

    }
    /**
     * 获取简易订单
     * 胡
     */
    public function getEasyOrder($params)
    {
        $data = "1=1 and orders.status = 1";

        if (!empty($params['orders_number'])) {
            $data .= " and orders.orders_number ='" . $params['orders_number'] . "'";
        }

        $result = $this->table("orders")->alias('orders')->

 
                where($data)->
                field(['orders.*',

                    "(select nickname  from user where user.user_id = orders.create_user_id)" => 'create_user_name',
                    "(select nickname  from user where user.user_id = orders.update_user_id)" => 'update_user_name',

                ])->select();



        return $result;

    }
    public function orderGoods()
    {
        return $this->hasMany('Ordersgoods', 'orders_id', 'orders_id');
    }

//获取状态列表
    public function getUpdateStatusList($params)
    {
        $data = $this->with(['orderGoods'])->whereIn('orders_id', $params['orders_id'])->select();
        return $data;
    }

//修改状态
    public function updateStatus($params)
    {
        $orderInfo = $this->whereIn('orders_id', $params['orders_id'])->
        where('order_status', '<', 8)
            ->update(['order_status' => $params['order_status']]);

        return true;
    }

    /**
     * 修改运单
     */
    public function updateOrderByOrderId($params)
    {

        $t = time();
        $send_goods_model = new SendGoods();
        $send_goods_info = $send_goods_model->parseSendGoods($params);
        $params = array_merge($params, $send_goods_info);
        $accept_goods_model = new AcceptGoods();
        $accept_goods_info = $accept_goods_model->parseAcceptGoods($params);
        $params = array_merge($params, $accept_goods_info);

        $data['send_goods_company'] = $params['send_goods_company'];
        $data['accept_goods_company'] = $params['accept_goods_company'];
        $data['project_id'] = $params['project_id'];
        $data['pay_type'] = $params['pay_type'];//支付方式
        $data['bargain_type'] = $params['bargain_type'];
        $data['dfk'] = $params['dfk'];
        if ($data['bargain_type'] == 2) {
            // $data['bargain_price'] = $params['bargain_price'];
        } else {
            //   $data['bargain_price'] = 0;
        }
        $data['company_id'] = $params['choose_company_id'];
        $data['scb_number'] = $params['scb_number'];
        $data['is_free'] = $params['is_free'];
        $data['delivery_method'] = $params['delivery_method'];
        $data['transportation_type'] = $params['transportation_type'];
        $data['send_location_id'] = $params['send_location_id'];
        $data['accept_location_id'] = $params['accept_location_id'];
        $data['send_goods_id'] = $params['send_goods_id'];
        $data['send_province_id'] = $params['send_province_id'];
        $data['send_city_id'] = $params['send_city_id'];
        $data['send_area_id'] = $params['send_area_id'];
        $data['send_address'] = $params['send_address'];
        $data['send_name'] = $params['send_name'];
        $data['send_cellphone'] = $params['send_cellphone'];
        $data['accept_goods_id'] = $params['accept_goods_id'];
        $data['accept_province_id'] = $params['accept_province_id'];
        $data['accept_city_id'] = $params['accept_city_id'];
        $data['accept_area_id'] = $params['accept_area_id'];
        $data['accept_address'] = $params['accept_address'];
        $data['accept_name'] = $params['accept_name'];
        $data['accept_cellphone'] = $params['accept_cellphone'];
        //客户单号

        $data['customer_order_number'] = $params['customer_order_number'];
        $data['customer_remark'] = $params['customer_remark'];
        $data['khydh'] = $params['khydh'];


        //代收货款

        $data['replacement_prive'] = $params['replacement_prive'];


        //保险货值

        $data['insurance_goods'] = $params['insurance_goods'];


        //提货时间
        /*  if ($params['pickup_time'] == '') {
              $data['pickup_time'] = null;

          } else
              $data['pickup_time'] = $params['pickup_time'];*/


        //送货时间
        if ($params['send_time'] == '') {
            $data['send_time'] = null;
        } else
            $data['send_time'] = $params['send_time'];


        $data['remark'] = $params['remark'];


        $data['update_user_id'] = $params['user_id'];
        $data['update_time'] = $t;


        $source_price = [];
        Db::startTrans();
        try {


            $orderResult = Db::name('orders')->where("orders_id = " . $params['orders_id'])->find();
            if (!empty($params['scb_number'])) {
                if ($params['scb_number'] != $orderResult['scb_number']) {
                    $equipmentParams['orders_number'] = $orderResult['orders_number'];
                    $equipmentWhere['equipment_status'] = 0;
                    $equipmentWhere['orders_number'] = '';
                    Db::name('equipment')->where($equipmentParams)->update($equipmentWhere);
                    $equipmentParams2['equipment_number_in'] = $params['scb_number'];
                    $equipmentWhere2['equipment_status'] = 1;
                    $equipmentWhere2['orders_number'] = $orderResult['orders_number'];

                    Db::name('equipment')->where($equipmentParams2)->update($equipmentWhere2);

                }
            } else {
                $equipmentParams['orders_number'] = $orderResult['orders_number'];
                $equipmentWhere['equipment_status'] = 0;
                $equipmentWhere['orders_number'] = '';
                Db::name('equipment')->where($equipmentParams)->update($equipmentWhere);

            }
            /*
            if(!empty($orderResult['scb_number'])){
                $equipmentParams['equipment_number_in'] = $params['scb_number'];
                if(empty($params['scb_number'])){

                    $equipmentWhere['orders_number']='';
                    $equipmentWhere['equipment_status']=0;


                }else{
                    $equipmentWhere['orders_number']='';
                    Db::name('equipment')->where($equipmentParams)->update($equipmentWhere);
                    unset($equipmentWhere['orders_number']);
                    $equipmentWhere['orders_number']=$orderResult['orders_number'];

                }

                Db::name('equipment')->where($equipmentParams)->update($equipmentWhere);
            }else{
                if(!empty($params['scb_number'])){


                    $equipmentParams['equipment_number_in'] = $params['scb_number'];
                    $equipmentWhere['equipment_status'] = 1;
                    $equipmentWhere['orders_number'] = $orderResult['orders_number'];
                    Db::name('equipment')->where($equipmentParams)->update($equipmentWhere);


                }else{

                }

            }
            */
            Db::name('orders')->where("orders_id = " . $params['orders_id'])->update($data);
            Db::name('orders_goods')->where("orders_id = " . $params['orders_id'])->delete();
            for ($i = 0; $i < count($params['goods_name']); $i++) {
                $orders_goods = [];
                $orders_goods['orders_id'] = $params['orders_id'];


                if ($params['goods_id'][$i]) {
                    $orders_goods['goods_id'] = $params['goods_id'][$i];
                } else {
                    $goods_data = [];
                    $goods_data['goods_name'] = $params['goods_name'][$i];
                    $goods_data['project_id'] = $params['project_id'];
                    $goods_data['goods_packaging_type'] = $params['estimated_pack_count'][$i];
                    $goods_data['goods_cost_unit'] = 1;
                    $goods_data['goods_specification'] = "";
                    $goods_data['goods_weight'] = $params['estimated_count'][$i] > 0 ? ($params['estimated_weight'][$i] / $params['estimated_count'][$i]) : 0;
                    $goods_data['goods_volume'] = $params['estimated_count'][$i] > 0 ? ($params['estimated_volume'][$i] / $params['estimated_count'][$i]) : 0;
                    $goods_data['remark'] = "自动添加";
                    $goods_data['calc_way'] = 1;
                    $goods_data['status'] = 1;
                    $goods_data['create_time'] = time();
                    $goods_data['create_user_id'] = $params['user_id'];
                    $goods_model = new Goods();
                    $base_goods_info = $goods_model->where('project_id', $params['project_id'])->where('goods_name', 'like', $goods_data['goods_name'])->find();
                    if ($base_goods_info) {
                        $orders_goods['goods_id'] = $base_goods_info->goods_id;
                    } else {
                        $orders_goods['goods_id'] = $goods_model->pubInsert($goods_data);
                    }
                }


                $orders_goods['estimated_count'] = $params['estimated_count'][$i];
                $orders_goods['estimated_pack_count'] = $params['estimated_pack_count'][$i];
                $orders_goods['estimated_pack_unit'] = $params['estimated_pack_unit'][$i];
                $orders_goods['estimated_weight'] = $params['estimated_weight'][$i];
                $orders_goods['estimated_volume'] = $params['estimated_volume'][$i];
                $orders_goods['realy_count'] = $params['realy_count'][$i];
                $orders_goods['realy_pack_count'] = $params['realy_pack_count'][$i];
                $orders_goods['realy_pack_unit'] = $params['realy_pack_unit'][$i];
                $orders_goods['realy_weight'] = $params['realy_weight'][$i];
                $orders_goods['realy_volume'] = $params['realy_volume'][$i];
                $orders_goods['create_time'] = $t;
                $orders_goods['create_user_id'] = $params['user_id'];
                $orders_goods['update_time'] = $t;
                $orders_goods['update_user_id'] = $params['user_id'];
                $orders_goods['status'] = 1;

                Db::name('orders_goods')->insertGetId($orders_goods);
            }
            $result = 1;
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();

        }
        return $result;
    }


    //修改运单
    public function updateOrders($params)
    {

        if (is_numeric($params['true_time'])) {
            $data['true_time'] = $params['true_time'];
        }

        if (is_numeric($params['is_invoice'])) {
            $data['is_invoice'] = $params['is_invoice'];
        }
        if (is_numeric($params['verify_status'])) {
            $data['verify_status'] = $params['verify_status'];
        }
        if (isset($params['follow_time'])) {
            if (!empty($params['follow_time'])) {
                $data['follow_time'] = strtotime($params['follow_time']);
            } else {
                $data['follow_time'] = '';
            }

        }

        if (is_numeric($params['follow_level'])) {
            $data['follow_level'] = $params['follow_level'];

        }

        if (isset($params['follow_remark'])) {
            $data['follow_remark'] = $params['follow_remark'];
        }

        Db::startTrans();
        try {
            Db::name('orders')->where("orders_id = " . $params['orders_id'])->update($data);

            $result = 1;
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();

        }

        return $result;

    }

    //修改运单
    public function updateOrdersByOrdersNumber($params)
    {


        if (is_numeric($params['status'])) {
            $data['status'] = $params['status'];
        }
        if (!empty($params['del_remark'])) {
            $data['del_remark'] = $params['del_remark'];
        }

        if (!empty($params['orderStatus']))
            $data['order_status'] = $params['orderStatus'];

        if (isset($params['receipt_count']))
            $data['receipt_count'] = $params['receipt_count'];
        if (isset($params['receipt_status']))
            $data['receipt_status'] = $params['receipt_status'];
        if (!empty($params['update_time']))
            $data['update_time'] = $params['update_time'];
        if (!empty($params['update_user_id']))
            $data['update_user_id'] = $params['update_user_id'];
        if (isset($params['receipt_accept_time']))
            $data['receipt_accept_time'] = $params['receipt_accept_time'];
        if (isset($params['receipt_accept_user_id']))
            $data['receipt_accept_user_id'] = $params['receipt_accept_user_id'];
        if (isset($params['receipt_send_time']))
            $data['receipt_send_time'] = $params['receipt_send_time'];
        if (isset($params['receipt_send_user_id']))
            $data['receipt_send_user_id'] = $params['receipt_send_user_id'];
        if (!empty($params['true_time']))
            $data['true_time'] = $params['true_time'];
        if (!empty($params['departure_time']))
            $data['departure_time'] = $params['departure_time'];
        $where['orders_number'] = $params['orders_number'];
        Db::startTrans();
        try {
            Db::name('orders')->where($where)->update($data);

            $result = 1;
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            return $result;
            // 回滚事务
            Db::rollback();

        }
        if (!isset($params['true_time']))
            return 1;

        $order_info = $this->where($where)->find();
        if ($data['true_time'] > $order_info->send_time) {

            $abnormal_model = new OrdersAbnormal();
            $abnormal_info = $abnormal_model->where('add_type', 1)->
            where('orders_id', $params['orders_id'])->find();
            if ($abnormal_info) {
                $abnormal_info->abnormal_type_id = 12;
                $abnormal_info->save();
            } else {
                $data1 = [];
                $data1['orders_id'] = $order_info->orders_id;
                $data1['orders_number'] = $order_info->orders_number;
                $data1['project_id'] = $order_info->project_id;

                $project_info = (new Project())->where('project_id', $order_info->project_id)->find();
                $data1['project_name'] = $project_info->project_name;
                $times = (new OrdersAbnormal())->whereTime("create_time", 'month')->count() + 1;
                $times = sprintf('%04d', $times);
                $data1['abnormal_number'] = $order_info->abnormal_number = 'PK' . date('Ym', time()) . $times;
                $data1['customer_order_number'] = $order_info->customer_order_number;
                $data1['solve_way'] = 2;
                $data1['abnormal_money'] = 0;
                $data1['abnormal_status'] = 0;
                $data1['abnormal_type_id'] = 12;
                $data1['remark'] =(!empty($params['over_time_remark']))?$params['over_time_remark']: "超时自动添加";
                $data1['create_user_id'] = $params['user_id'];
                $data1['update_user_id'] = $params['user_id'];
                $data1['create_time'] = time();
                $data1['update_time'] = time();
                $data1['status'] = 1;
                $data1['add_type'] = 1;
                $data1['pickup_time'] = $order_info['pickup_time'];
                Db::startTrans();
                try {
                    (new OrdersAbnormal())->insert($data1);
// 提交事务
                    Db::commit();
                    $result = 1;
                } catch (\Exception $e) {
                    Db::rollback();
                    $result = $e->getMessage();

                }


            }


            return $result;

        }
    }

    /**
     * 获取运单及详细数据
     * 韩
     */
    public function getOrdersInfo($params)
    {

        $data = "1=1 ";

        $result = $this->table("orders")->
        where($data)->order('create_time desc')->
        field("*")->select();

        if ($result) {

        }

        return $result;
    }

    /*配比*/
    public function getPeibi($params, $is_count = false, $is_page = false, $page = null, $page_size = 20)
    {
        $data = '1=1 and orders.verify_status>3';
        if (!empty($params['orders_number'])) {
            $data .= " and orders.orders_number ='" . $params['orders_number'] . "'";
        }
        if (is_numeric($params['project_id'])) {
            $data .= " and orders.project_id =" . $params['project_id'];
        }
        if (!empty($params['send_goods_company'])) {
            $data .= " and orders.send_goods_company like '%" . $params['send_goods_company']."%'";
        }
        if (!empty($params['accept_goods_company'])) {
            $data .= " and orders.accept_goods_company like '%" . $params['accept_goods_company']."%'";
        }		
        if (is_numeric($params['start_pickup_time'])) {
            $data .= " and orders.pickup_time >=" . $params['start_pickup_time'];
        }	
        if (is_numeric($params['end_pickup_time'])) {
            $data .= " and orders.pickup_time <=" . $params['end_pickup_time'];
        }		
        if ($is_count == true) {
            $result = $this->table("orders")->alias('orders')->

            join("project", 'project.project_id= orders.project_id')->
            where($data)->order('orders.create_time desc')->
            field(['orders.*', 'project.project_name',

            ])->count();

        } else {
            if ($is_page == true) {

                $result = $this->table("orders")->
                join("project", 'project.project_id= orders.project_id')->
                where($data)->limit($page, $page_size)->order('orders.create_time desc')->
                field(['orders.*', 'project.project_name',
                    "(select city_name from city where city.city_id = orders.accept_location_id)" => 'accept_location_name',
                    "(select nickname  from user where user.user_id = orders.create_user_id)" => 'create_user_name',
                    "(select nickname  from user where user.user_id = orders.update_user_id)" => 'update_user_name',
                ])->select();


            } else {
                $result = $this->table("orders")->alias('orders')->

                join("project", 'project.project_id= orders.project_id')->
                where($data)->order('orders.create_time desc')->
                field(['orders.*', 'project.project_name',

                    "(select city_name from city where city.city_id = orders.accept_location_id)" => 'accept_location_name',
                    "(select nickname  from user where user.user_id = orders.create_user_id)" => 'create_user_name',
                    "(select nickname  from user where user.user_id = orders.update_user_id)" => 'update_user_name',
                ])->select();

            }

            for ($i = 0; $i < count($result); $i++) {
				//获取运单运费明细
                $result[$i]['orders_money_detile'] = $this->table("orders_income")->
                where("orders_income.status =1 and orders_income.orders_id = " . $result[$i]['orders_id'])->field(['orders_income.*'
                ])->select();				
				//获取发运成本明细
                $result[$i]['shipment_money_detile'] = $this->query("select * from shipment_cost where shipment_cost.status = 1 and shipment_cost.shipment_uuid in (select shipment_uuid from shipment where shipment.status =1 and shipment.orders_number = '".$result[$i]['orders_number']."')");					
				
				
                //获取运单的货物信息
                $result[$i]['orders_goods'] = $this->table("orders_goods")->join('goods', 'goods.goods_id=orders_goods.goods_id')->
                where("orders_goods.orders_id = " . $result[$i]['orders_id'])->field(['orders_goods.*', 'goods.goods_name',
                ])->select();

                //获取发运信息
                $shipment_info = $this->table('shipment')->where("status =1 and orders_number ='" . $result[$i]['orders_number'] . "'")->
				field(['shipment.*','(select supplier_name from supplier where supplier.supplier_uuid = shipment.supplier_uuid)'=>'supplier_name'
				
				
				])->select();
				//开始 查询 supplier_bill_number
				
				
				for($i3=0;$i3<count($shipment_info);$i3++){
					
					//开始查询supplier_bill_number
				
					
					$supplier_result = $this->query("select * from supplier_bill where supplier_bill.supplier_bill_status !=3 and supplier_bill_number in(select supplier_bill_number from supplier_bill_info where supplier_bill_info.shipment_id=".$shipment_info[$i3]['shipment_id'].")");
					if(!empty($supplier_result)){
						$shipment_info[$i3]['supplier_bill_tax'] = $supplier_result[0]['supplier_bill_tax'];
					}else{
						$shipment_info[$i3]['supplier_bill_tax'] = 0;
					}
	
				}
				//获取发运的货物信息
                for ($i2 = 0; $i2 < count($shipment_info); $i2++) {
                    $shipment_info[$i2]['shipment_goods'] = $this->table("shipment_goods")->
                    where("shipment_goods.shipment_uuid = '" . $shipment_info[$i2]['shipment_uuid'] . "'")->field(['shipment_goods.*'])->select();

                }
                $result[$i]['shipment_info'] = $shipment_info;
                //获取收入赔款

                $result[$i]['orders_abnormal'] = $this->table('orders_abnormal')->where("status =1 and orders_number ='" . $result[$i]['orders_number'] . "'")->select();
                //获取税率
                $tax = $this->table('customer_bill_info')->join("customer_bill", 'customer_bill.customer_bill_id=customer_bill_info.customer_bill_id')->where("customer_bill_info.status =1 and customer_bill_info.orders_id=" . $result[$i]['orders_id'])->field([
                    'customer_bill.customer_bill_tax', 'customer_bill.customer_bill_tax_type'
                ])->select();
                $result[$i]['orders_tax_type'] = $tax[0]['customer_bill_tax_type'];
                $result[$i]['orders_tax'] = $tax[0]['customer_bill_tax'];
                //获取短驳信息
                $short_barge = $this->table('shipment_short_barge')->where("status =1 and orders_number ='" . $result[$i]['orders_number'] . "'")->
				field(['shipment_short_barge.*','(select supplier_name from supplier where supplier.supplier_uuid = (select shipment.supplier_uuid from shipment where shipment.shipment_uuid = shipment_short_barge.shipment_uuid))'=>'supplier_name',
				"(select shipment_finance_status from shipment where shipment.shipment_uuid = shipment_short_barge.shipment_uuid and shipment.status=1)"=>'shipment_finance_status',
				])->
				select();
				for($kk=0;$kk<count($short_barge);$kk++){
					
					 $supplier_bill_tax = $this->query("select supplier_bill_tax from supplier_bill where supplier_bill.supplier_bill_number = (select supplier_bill_number from supplier_bill_info where supplier_bill_info.shipment_id=(select shipment_id from shipment where shipment.shipment_uuid='".$short_barge[$kk]['shipment_uuid']."'"."))");					
					if(count($supplier_bill_tax)>0){
						$short_barge[$kk]['supplier_bill_tax'] = $supplier_bill_tax[0]['supplier_bill_tax'];
					}else{
						$short_barge[$kk]['supplier_bill_tax'] = 0;
					}
				
				}
				
                $result[$i]['short_barge_info'] = $short_barge;
            }
        }


        return $result;


    }

    public function changeAbnormalDescribe($params)
    {
        $order_info = $this->where('orders_id', $params['orders_id'])->find();
        $order_info->order_abnormal_describe_id = $params['order_abnormal_describe_id'];
        $order_info->save();

        $abnormal_model = new OrdersAbnormal();
        $abnormal_info = $abnormal_model->where('add_type', 2)->
        where('orders_id', $params['orders_id'])->find();
        if ($abnormal_info) {
            $abnormal_info->abnormal_type_id = $params['order_abnormal_describe_id'];
            $abnormal_info->save();
        } else {
            $data = [];
            $data['orders_id'] = $order_info->orders_id;
            $data['orders_number'] = $order_info->orders_number;
            $data['project_id'] = $order_info->project_id;

            $project_info = (new Project())->where('project_id', $order_info->project_id)->find();
            $data['project_name'] = $project_info->project_name;
            $times = (new OrdersAbnormal())->whereTime("create_time", 'month')->count() + 1;
            $times = sprintf('%04d', $times);
            $data['abnormal_number'] = $order_info->abnormal_number = 'PK' . date('Ym', time()) . $times;
            $data['customer_order_number'] = $order_info->customer_order_number;
            $data['solve_way'] = 2;
            $data['abnormal_money'] = 0;
            $data['abnormal_status'] = 0;
            $data['abnormal_type_id'] = $params['order_abnormal_describe_id'];
            $data['remark'] = "自动添加";
            $data['create_user_id'] = $params['user_id'];
            $data['update_user_id'] = $params['user_id'];
            $data['create_time'] = time();
            $data['update_time'] = time();
            $data['status'] = 1;
            $data['add_type'] = 2;
            $data['pickup_time'] = $order_info['pickup_time'];
            Db::startTrans();
            try {
                (new OrdersAbnormal())->insert($data);
                // 提交事务
                Db::commit();
                $result = 1;
            } catch (\Exception $e) {
                Db::rollback();
                $result = $e->getMessage();

            }


        }
        return $result;

    }

    public function changeShipmentNumber($params)
    {
        $order_info = $this->where('orders_id', $params['orders_id'])->find();
        $order_info->shipment_number = $params['shipment_number'];
        $order_info->save();

    }

    //驾驶舱
    public function getIndexOrder($params)
    {
        $data = '1=1 and orders.status =1';
        if (is_numeric($params['month'])) {
            $data .= " and FROM_UNIXTIME(pickup_time,'%Y%m') = FROM_UNIXTIME(" . $params['month'] . ",'%Y%m')";

        }
        if (is_numeric($params['year'])) {
            $data .= " and FROM_UNIXTIME(pickup_time,'%Y') = FROM_UNIXTIME(" . $params['year'] . ",'%Y')";

        }
        $result = $this->table('orders')->where($data)->field([
            "sum(orders.money) as sum_orders_money",

        ])->select();

        return $result;

    }


    public function orderTrackEdit($param)
    {
        if (is_numeric($param['orders_id'])) {
            $order_info = $this->where('orders_id', $param['orders_id'])->find();
            if ($order_info) {

                if(strtotime($param['true_time']) && !$order_info->true_time)
                    if (strtotime($param['true_time']) > $order_info->send_time) {
                        $params=$param;
                        $abnormal_model = new OrdersAbnormal();
                        $abnormal_info = $abnormal_model->where('add_type', 1)->
                        where('orders_id', $params['orders_id'])->find();
                        if ($abnormal_info) {
                            $abnormal_info->abnormal_type_id = 12;
                            $abnormal_info->save();
                        } else {
                            $data1 = [];
                            $data1['orders_id'] = $order_info->orders_id;
                            $data1['orders_number'] = $order_info->orders_number;
                            $data1['project_id'] = $order_info->project_id;

                            $project_info = (new Project())->where('project_id', $order_info->project_id)->find();
                            $data1['project_name'] = $project_info->project_name;
                            $times = (new OrdersAbnormal())->whereTime("create_time", 'month')->count() + 1;
                            $times = sprintf('%04d', $times);
                            $data1['abnormal_number'] =  'PK' . date('Ym', time()) . $times;
                            $data1['customer_order_number'] = $order_info->customer_order_number;
                            $data1['solve_way'] = 2;
                            $data1['abnormal_money'] = null;
                            $data1['abnormal_status'] = 0;
                            $data1['abnormal_type_id'] = 12;


                            $data1['remark'] =(!empty($params['over_time_remark']))?"超时(".$params['over_time_remark'].")": "超时自动添加";
                           // $data1['remark'] ="超时自动添加:".$params['over_time_remark'];
                            $data1['create_user_id'] = $params['user_id'];
                            $data1['update_user_id'] = $params['user_id'];
                            $data1['create_time'] = time();
                            $data1['update_time'] = time();
                            $data1['status'] = 1;
                            $data1['add_type'] = 1;
                            $data1['pickup_time'] = $order_info['pickup_time'];
                            Db::startTrans();
                            try {
                                (new OrdersAbnormal())->insert($data1);
                                // 提交事务
                                Db::commit();
                                $result = 1;
                            } catch (\Exception $e) {
                                Db::rollback();
                                $result = $e->getMessage();

                            }


                        }}





                if(strtotime($param['departure_time']))
                $order_info->departure_time = strtotime($param['departure_time']);
                $order_info->shipment_number = $param['shipment_number'];
                $order_info->order_status = $param['order_status'];
                if(strtotime($param['true_time']))
                $order_info->true_time = strtotime($param['true_time']);
                if($param['order_status']!=6)
                    $order_info->true_time =null;
                $order_info->remark = $param['user_reamrk'];
                if (is_numeric($param['orderabnormalStatus']))
                    $order_info->order_abnormal_describe_id = $param['orderabnormalStatus'];
                $order_info->save();
                $operating_model = new OrdersOperating();
                Db::startTrans();
                try {
                    $operating_model->insert(['orders_id' => $param['orders_id'],
                        'order_status' => $param['order_status'],
                        'create_user_id' => $param['user_id'],
                        'create_time' => time(),
                        'remark' => $param['operating_reamrk'],
                        'create_user_name' => User::get($param['user_id'])->nickname


                    ]);

                    Db::commit();
                } catch (Exception $e) {
                    Db::rollback();
                    return $e->getMessage();

                }






            }
            return 1;
        } else
            return 0;

    }
    public function delOrder($params){
          $order_info=$this->find($params['orders_id']);
          if($order_info)
          {
              $order_info->status=0;
              $order_info->save();

              $remark_model=new OrdersRemark();
              $remark_info=$remark_model->where(['orders_id'=>$params['orders_id'],'type'=>4])->find();
              if($remark_info)
              {
                  $remark_info->content=$params['del_remark'];
                   $remark_info->save();
              }
              else
              {
                  $insertData['orders_id']=$params['orders_id'];
                  $insertData['content']=$params['del_remark'];
                  $insertData['type']=4;
                  $remark_model->pubInsert($insertData);

              }
              return 1;
          }
return 0;
    }

}