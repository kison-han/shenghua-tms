<?php
/**
 * Created by PhpStorm.
 * User: Hugh
 * Date: 2019/11/04
 * Time: 13:40
 */

namespace app\index\controller;
use app\common\help\Contents;
use \Underscore\Types\Arrays;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
use app\common\help\Help;

class AdminBooking extends Base
{
    //控制面板
    public function dashboard(){
        $date               = time(); // Change to whatever date you need
        $dotw               = date('w', $date);
        $end                = ($dotw == 0 /* Sunday */) ? $date : strtotime('next sunday', $date);
        $start              = $end - (6 * 24*60*60);
        // Find start and end date of last week
        $lw_end = strtotime('last sunday');
        $lw_start = $lw_end - (6 * 24*60*60);

        $w['start'] =date('Y-m-d',$start);
        $w['end'] = date('Y-m-d',$end);
        $w['lw_start'] =  date('Y-m-d',$lw_start);
        $w['lw_end'] =  date('Y-m-d',$lw_end);

        $d = $this->callSoaErp('post','/b2b_operate/dashboard',$w);
        $this->assign('dateWeek',$w);
        $this->assign('d',$d['data']);
        unset($w);
//        echo '<pre>';print_r($d);exit;

        $w = [
            'page'=>$this->page(),
            'page_size'=>$this->_page_size,
        ];
        $w['booking_status'] = '2,8';
        $w['company_id'] = session('user')['company_id'];
        $result = $this->callSoaErp('post', '/b2b_booking/getB2bBooking',$w);
        $this->getPageParams($result);
//        echo '<pre>';print_r($result);exit;

        $b2bBookingStatus = Contents::b2bBookingStatus();
        $this->assign('b2bBookingStatus',$b2bBookingStatus);


        return $this->fetch('dashboard');
    }


    public function index(){
        $b2bBookingStatus = Contents::b2bBookingStatus();
        $b2bOfficeStatus = Contents::b2bOfficeStatus();
        $b2bPayment = Contents::b2bPayment();

        $this->assign('b2bBookingStatus',$b2bBookingStatus);
        $this->assign('b2bOfficeStatus',$b2bOfficeStatus);
        $this->assign('b2bPayment',$b2bPayment);
        $params = $_GET;

        $w = [
            'page'=>$this->page(),
            'page_size'=>$this->_page_size,
        ];
        $w['company_id'] = session('user')['company_id'];
        if(!empty($params['s_company_order_number'])){
           $w['company_order_number'] = $params['s_company_order_number'];
        }
        if(!empty($params['s_create_time'])){
            $w['create_time'] = strtotime($params['s_create_time']);
        }
        if(!empty($params['s_begin_time'])){
            $w['begin_time'] = strtotime($params['s_begin_time']);
        }
        if(!empty($params['s_distributor_name'])){
            $w['agent_name'] = $params['s_distributor_name'];
        }
        if(!empty($params['s_distributor_code'])){
            $w['agent_code'] = $params['s_distributor_code'];
        }
        if(!empty($params['s_booking_status'])){
            $w['booking_status'] = $params['s_booking_status'];
        }
        if(!empty($params['s_office_status'])){
            $w['office_status'] = $params['s_office_status'];
        }
        if(!empty($params['s_payment_status'])){
            $w['payment_status'] = $params['s_payment_status'];
        }

//        var_dump($w);exit;

        $result = $this->callSoaErp('post', '/b2b_booking/getB2bBooking',$w);
        $this->getPageParams($result);
//        var_dump($result);exit;

        $b2bBookingStatus = Contents::b2bBookingStatus();
        $b2bOfficeStatus = Contents::b2bOfficeStatus();
        $b2bPayment = Contents::b2bPayment();
        $this->assign('b2bBookingStatus',$b2bBookingStatus);
        $this->assign('b2bOfficeStatus',$b2bOfficeStatus);
        $this->assign('b2bPayment',$b2bPayment);

        return $this->fetch('list');
    }

    public function updateBookingStatusAjax(){
        $data['b2b_booking_id'] = $_POST['b2b_booking_id'];
        $data['booking_status'] = $_POST['booking_status'];
        return $this->callSoaErp('post','/b2b_booking/updateB2bBookingGeneral',$data);
    }

    public function updateOfficeStatusAjax(){
        $data['b2b_booking_id'] = $_POST['b2b_booking_id'];
        $data['office_status'] = $_POST['office_status'];
        return $this->callSoaErp('post','/b2b_booking/updateB2bBookingGeneral',$data);
    }

    public function updatePaymentStatusAjax(){
        $data['b2b_booking_id'] = $_POST['b2b_booking_id'];
        $data['payment_status'] = $_POST['payment_status'];
        return $this->callSoaErp('post','/b2b_booking/updateB2bBookingGeneral',$data);
    }


    //General
    public function updateBookingFormGeneral(){

        $b2b_booking_id = $_GET['b2b_booking_id'];

        //获取基本信息
        $where['b2b_booking_id'] = $b2b_booking_id;
        $General = $this->callSoaErp('post','/b2b_booking/getB2bBookingGeneral',$where);
        $this->assign('general',$General['data']);
//        var_dump($General);exit;

        $btb_tour_id = $General['data']['btb_tour_id'];
        $where['btb_tour_id'] = $btb_tour_id;
        $result = $this->callSoaErp('post','/b2b_tour/getB2bTourSetting',$where);
//        var_dump($result['data']);exit;
        $this->assign('is_tour_voucher',$result['data']['is_tour_voucher']);
        unset($where);unset($result);

        //获取团日期
        $where['status'] = 1;
        $where['btb_tour_id'] = $General['data']['btb_tour_id'];
        $result = $this->callSoaErp('post','/b2b_tour/getB2bTourDates',$where);
        $dateOj = [];
        foreach($result['data'] as $date){
            if(date('Y-m-d')<=$date['arrival_date'])
                $dateOj[] = date('d-m-Y',strtotime($date['arrival_date']));
        }
        sort($dateOj);
        $this->assign('dateOj',$dateOj);
        unset($date);unset($result);

        //获取语言
        $where['btb_tour_id'] = $General['data']['btb_tour_id'];
        $result = $this->callSoaErp('post', '/b2b_tour/getB2bTour',$where);
        $this->assign('sLanguage',explode(',',$result['data'][0]['tour_languages']));
        unset($date);unset($result);
        $where = ['status'=>1];
        $Language = $this->callSoaErp('post','/system/getLanguage',$where);
        $this->assign('Language',$Language['data']);
//        var_dump($Language);exit;
        unset($date);


        //获取所有的 agent
        $params['choose_company_id'] = session('user')['company_id'];
        $params['status'] = 1;
        $result = $this->callSoaErp('post', '/btob/getBtoBDistributor', $params);
        $this->assign('all_agent',$result['data']);
        //var_dump($result);exit;
        unset($params);unset($result);


        $b2bBookingStatus = Contents::b2bBookingStatus();
        $b2bOfficeStatus = Contents::b2bOfficeStatus();
        $b2bPayment = Contents::b2bPayment();
        $this->assign('b2bBookingStatus',$b2bBookingStatus);
        $this->assign('b2bOfficeStatus',$b2bOfficeStatus);
        $this->assign('b2bPayment',$b2bPayment);


        return $this->fetch('update_booking_form_general');
    }




    public function updateBookingFormGeneralAjax(){
         $post = $_POST;

        $data['agent_id'] = Arrays::get($post,'agent_id');
        $data['agent_reference_id'] = Arrays::get($post,'agent_reference_id');
//        $data['tour_code'] = Arrays::get($post,'tour_code');
//        $data['tour_name'] = Arrays::get($post,'tour_name');
        $data['begin_time'] = strtotime(Arrays::get($post,'begin_time'));
        $data['language_id'] = Arrays::get($post,'language_id');
        $data['tour_voucher'] = Arrays::get($post,'tour_voucher');
        $data['bus_no'] = Arrays::get($post,'bus_no');
        $data['seat_group'] = Arrays::get($post,'seat_group');
        $data['emergency_name'] = Arrays::get($post,'emergency_name');
        $data['emergency_phone'] = Arrays::get($post,'emergency_phone');
        $data['emergency_email'] = Arrays::get($post,'emergency_email');
        $data['lead_pax_mobile'] = Arrays::get($post,'lead_pax_mobile');
        $data['tour_guide'] = Arrays::get($post,'tour_guide');
        $data['tour_contact'] = Arrays::get($post,'tour_contact');
        $data['is_pre_paid'] = Arrays::get($post,'is_pre_paid');
        $data['special_requests'] = Arrays::get($post,'special_requests');
        $data['office_status'] = Arrays::get($post,'office_status');
        $data['booking_status'] = Arrays::get($post,'booking_status');
        $data['payment_status'] = Arrays::get($post,'payment_status');
        $data['remark'] = Arrays::get($post,'remark');
        $data['b2b_booking_id'] = $_GET['b2b_booking_id'];

        return $this->callSoaErp('post','/b2b_booking/updateB2bBookingGeneral',$data);

    }


    //Passenger
    public function updateBookingFormPassenger(){
        //获取国家
        $where['status'] = 1;
        $where['level'] = 1;
        $nations = $this->callSoaErp('post','/system/getCountry',$where);
        $this->assign('all_nations',$nations['data']);
//        var_dump($nations);exit;
        unset($where);

        //获取订单游客
        $where['b2b_booking_id'] = $_GET['b2b_booking_id'];
        $Customer = $this->callSoaErp('post','/b2b_booking/getB2bBookingCustomer',$where);

        $CustomerSort = Arrays::sort($Customer['data'],'temp_id');
        $CustomerGroup = Arrays::group($Customer['data'],'age_group');

        $this->assign('customer',$CustomerSort);
        $this->assign('customerGroup',$CustomerGroup);
//        var_dump($Customer);exit;

        return $this->fetch('update_booking_form_passenger');
    }


    public function updateBookingFormPassengerAjax(){
        $post = $_POST;

        $age = Arrays::get($post,'age');
        $age_group = Arrays::get($post,'age_group');
        $company_order_customer_id = Arrays::get($post,'company_order_customer_id');
        $birthday = Arrays::get($post,'birthday');
        $customer_first_name = Arrays::get($post,'customer_first_name');
        $customer_last_name = Arrays::get($post,'customer_last_name');
        $ethnicity = Arrays::get($post,'ethnicity');
        $gender = Arrays::get($post,'gender');
        $nationality = Arrays::get($post,'nationality');
        $seat_no = Arrays::get($post,'seat_no');
        $speaking_fluent = Arrays::get($post,'speaking_fluent');
        $temp_id = Arrays::get($post,'temp_id');
        $term_of_validity = Arrays::get($post,'term_of_validity');
        $passport_number = Arrays::get($post,'passport_number');

        $data['b2b_booking_id'] = $_GET['b2b_booking_id'];
        $data['customer'] = [];
        foreach($temp_id as $k=>$v){
            $ar['passport_number'] = $passport_number[$k];
            $ar['customer_first_name'] = $customer_first_name[$k];
            $ar['customer_last_name'] = $customer_last_name[$k];
            $ar['gender'] = $gender[$k];
            $ar['birthday'] = $birthday[$k];
            $ar['speaking_fluent'] = $speaking_fluent[$k];
            $ar['ethnicity'] = $ethnicity[$k];
            $ar['nationality'] = $nationality[$k];
            $ar['age_group'] = $age_group[$k];
            $ar['term_of_validity'] = strtotime($term_of_validity[$k]);

            $ar['seat_no'] = $seat_no[$k];
            $ar['temp_id'] = $temp_id[$k];
            $ar['company_order_customer_id'] = $company_order_customer_id[$k];
            $data['customer'][] = $ar;
            unset($ar);
        }

        return $this->callSoaErp('post','/b2b_booking/updateB2bBookingCustomer',$data);


    }


    //Room
    public function updateBookingFormRoom(){
        //获取基本信息
        $where['b2b_booking_id'] = $_GET['b2b_booking_id'];
        $General = $this->callSoaErp('post','/b2b_booking/getB2bBookingGeneral',$where);
        $this->assign('general',$General['data']);
        //获取房型
        $where['status'] =1;
        $where['language_id'] = session('user')['language_id'];
        $result = $this->callSoaErp('post', '/system/getRoomTypeAjax', $where);
        $roomType = Arrays::group($result['data'],'room_type_id');
//        var_dump($roomType);
        $this->assign('roomType',$roomType);
        unset($where);unset($result);

        $where['status'] =1;
        $where['btb_tour_id'] = $General['data']['btb_tour_id'];
        $result = $this->callSoaErp('post','/b2b_tour/getB2bTourRoom',$where);
        $this->assign('all_rooms',$result['data']);
//        var_dump($result);exit;
        unset($where);unset($result);

        //获取用房信息
        $where['b2b_booking_id'] = $_GET['b2b_booking_id'];
        $room = $this->callSoaErp('post','/b2b_booking/getB2bBookingRoom',$where);
//        var_dump($room); exit;
        $this->assign('roomData',$room['data']);

        return $this->fetch('update_booking_form_room');
    }


    public function updateBookingFormRoomAjax(){
//        return $_POST;

        $post = $_POST;
        $temp_group = Arrays::get($post,'temp_group');
        $seat_group = Arrays::get($post,'seat_group');
        $room_type = Arrays::get($post,'room_type');
        $charge_type = Arrays::get($post,'charge_type');
        $charge_num = Arrays::get($post,'charge_num');
        $b2b_booking_room_id = Arrays::get($post,'b2b_booking_room_id');

        $ar['b2b_booking_id'] = $_GET['b2b_booking_id'];
        $ar['room'] = [];
        foreach($temp_group as $k=>$v){
            $ay['temp_group'] = $temp_group[$k];
            $ay['seat_group'] = $seat_group[$k];
            $ay['room_type'] = $room_type[$k];
            $ay['room_code'] = $k+1;
            $ay['charge_num'] = $charge_num[$k];
            $ay['charge_type'] = $charge_type[$k];

            if($b2b_booking_room_id[$k]){
                $ay['b2b_booking_room_id'] = $b2b_booking_room_id[$k];
            }
            $ar['room'][] = $ay;

            unset($ay);
        }
       return $this->callSoaErp('post','/b2b_booking/updateB2bBookingRoom',$ar);


    }



    //Transfer
    public function updateBookingFormTransfer(){
        //获取基本信息
        $where['b2b_booking_id'] = $_GET['b2b_booking_id'];
        $General = $this->callSoaErp('post','/b2b_booking/getB2bBookingGeneral',$where);
        $this->assign('general',$General['data']);
        unset($where);

        //获取航班信息
        $where['status'] =1;
        $where['btb_tour_id'] = $General['data']['btb_tour_id'];
        $result = $this->callSoaErp('post','/b2b_tour/getB2bTourTransfer',$where);
        $tourTransfer = Arrays::group($result['data'],'type');
//        var_dump($tourTransfer);
        $this->assign('tourTransfer',$tourTransfer);
        unset($where);

        //获取接送机信息
        $where['b2b_booking_id'] = $_GET['b2b_booking_id'];
        $transfer = $this->callSoaErp('post','/b2b_booking/getB2bBookingTransfer',$where);
        $transfer['data'] = Arrays::sort($transfer['data'],'transfer_type',asc);
        $this->assign('transfer',$transfer['data']);
//        var_dump($transfer);

//        exit;
        return $this->fetch('update_booking_form_transfer');
    }

    public function updateBookingFormTransferAjax(){
//        return $_POST;

        $post = $_POST;
        $b2b_booking_transfer_id = Arrays::get($post,'b2b_booking_transfer_id');
        $transfer_type = Arrays::get($post,'transfer_type');
        $airport = Arrays::get($post,'airport');
        $flight = Arrays::get($post,'flight');
        $date = Arrays::get($post,'date');
        $time = Arrays::get($post,'time');
        $charge_num = Arrays::get($post,'charge_num');
        $temp_group = Arrays::get($post,'temp_group');
        $seat_group = Arrays::get($post,'seat_group');
        $bus_pax = Arrays::get($post,'bus_pax');

        $data['b2b_booking_id'] = $_GET['b2b_booking_id'];
        $data['transfer'] = [];
        foreach($airport as $k=>$v){
            $ar['temp_group'] = $temp_group[$k];
            $ar['seat_group'] = $seat_group[$k];
            $ar['airport'] = $airport[$k];
            $ar['transfer_type'] = $transfer_type[$k];
            $ar['flight'] = $flight[$k];
            $ar['charge_num'] = $charge_num[$k];
            $ar['date'] = $date[$k];
            $ar['time'] = $time[$k];
            $ar['bus_pax'] =$bus_pax[$k];
            if($b2b_booking_transfer_id[$k]){
                $ar['b2b_booking_transfer_id'] = $b2b_booking_transfer_id[$k];
            }

            $data['transfer'][] = $ar;
            unset($ar);

        }
        return $this->callSoaErp('post','/b2b_booking/updateB2bBookingTransfer',$data);
    }


    //option
    public function  updateBookingFormOption(){

        //获取自费项目
        $where['b2b_booking_id'] = $_GET['b2b_booking_id'];
        $General = $this->callSoaErp('post','/b2b_booking/getB2bBookingGeneral',$where);
        $this->assign('general',$General['data']);
        unset($where);

        $where['btb_tour_id'] = $General['data']['btb_tour_id'];
        $result = $this->callSoaErp('post','/b2b_tour/getB2bTourOptions',$where);
        $this->assign('TourOptions',$result['data']); unset($where);
//        var_dump($result);exit;

        $where['b2b_booking_id'] = $_GET['b2b_booking_id'];
        $General = $this->callSoaErp('post','/b2b_booking/getB2bBookingGeneral',$where);
//        var_dump($General);exit;
        $this->assign('General',$General['data']);


        return $this->fetch('update_booking_form_option');
    }

    //
    public function updateBookingFormOptionAjax(){
        $d['options_id'] = $_POST['options_id'];
        $d['b2b_booking_id'] = $_GET['b2b_booking_id'];

        return $this->callSoaErp('post','/b2b_booking/updateB2bBookingGeneral',$d);
    }


    //Accommodation
    public function  updateBookingFormAccommodation(){
        //获取基本信息
        $where['b2b_booking_id'] = $_GET['b2b_booking_id'];
        $General = $this->callSoaErp('post','/b2b_booking/getB2bBookingGeneral',$where);
        $this->assign('general',$General['data']);
        unset($where);

        //获取房型
        $where['btb_tour_id'] = $General['data']['btb_tour_id'];
        $result = $this->callSoaErp('post','/b2b_tour/getB2bTourRoom',$where);
        $this->assign('all_rooms',$result['data']);
//        var_dump($result['data']);
        unset($where);unset($result);

        $where['status'] =1;
        $where['language_id'] = session('user')['language_id'];
        $result = $this->callSoaErp('post', '/system/getRoomTypeAjax', $where);
        $roomType = Arrays::group($result['data'],'room_type_id');
        $this->assign('roomType',$roomType);
//        var_dump($roomType);exit;
        unset($where);unset($result);


        //获取前后入住数据
        $where['b2b_booking_id'] = $_GET['b2b_booking_id'];
        $accommodation = $this->callSoaErp('post','/b2b_booking/getB2bBookingAccommodation/',$where);
//        var_dump($accommodation);exit;
        $accommodation['data'] = Arrays::sort($accommodation['data'],'accommodation_type',asc);
        $this->assign('accommodationAll',$accommodation['data']);


        return $this->fetch('update_booking_form_accommodation');
    }

    public function updateBookingFormAccommodationAjax(){
//        return $_POST;

        $post = $_POST;
        $accommodation_type = Arrays::get($post,'accommodation_type');
        $b2b_booking_accommodation_id = Arrays::get($post,'b2b_booking_accommodation_id');
        $nights = Arrays::get($post,'nights');
        $room_type = Arrays::get($post,'room_type');
        $seat_group = Arrays::get($post,'seat_group');
        $temp_group = Arrays::get($post,'temp_group');

        $ary['b2b_booking_id'] = $_GET['b2b_booking_id'];
        $ary['accommodation'] = [];
        foreach($room_type as $k=>$v){
            $ar = [];
            $ar['room_type'] = $room_type[$k];
            $ar['temp_group'] = $temp_group[$k];
            $ar['seat_group'] = $seat_group[$k];
            $ar['nights'] = $nights[$k];
            $ar['room_type'] = $room_type[$k];
            $ar['accommodation_type'] = $accommodation_type[$k];

            if($b2b_booking_accommodation_id[$k]){
                $ar['b2b_booking_accommodation_id'] = $b2b_booking_accommodation_id[$k];
            }
            array_push($ary['accommodation'], $ar);
        }


        return $this->callSoaErp('post','/b2b_booking/updateB2bBookingAccommodation',$ary);
    }




    //Hotel
    public function updateBookingFormHotel(){
        //获取B2B里的所有酒店
        $result = $this->callSoaErp('post','/source/getB2bHotel',[]);
        $this->assign('hotelAll',$result['data']);
//        var_dump($result);exit;
        unset($result);

        //获取基本信息
        $where['b2b_booking_id'] = $_GET['b2b_booking_id'];
        $General = $this->callSoaErp('post','/b2b_booking/getB2bBookingGeneral',$where);
        $this->assign('general',$General['data']);
//        var_dump($General);exit;
        unset($where);

        return $this->fetch('update_booking_form_hotel');
    }

    public function updateBookingFormHotelAjax(){
//        return $_POST;

        $post = $_POST;
        $ary['b2b_booking_id'] = $_GET['b2b_booking_id'];
        $ary['hotel_both_day'] = Arrays::get($post,'hotel_both_day');
        $ary['hotel_last_day'] = Arrays::get($post,'hotel_last_day');
        $ary['hotel_first_day'] = Arrays::get($post,'hotel_first_day');

        return $this->callSoaErp('post','/b2b_booking/updateB2bBookingGeneral',$ary);

    }



    //File
    public function updateBookingFormFile(){
        //获取附件
        $where['b2b_booking_id'] = $_GET['b2b_booking_id'];
        $annex = $this->callSoaErp('post','/b2b_booking/getB2bBookingAnnex',$where);
        $this->assign('annex',$annex['data']);
//        var_dump($annex);exit;

        return $this->fetch('update_booking_form_file');
    }

    public function updateBookingFormFileAjax()
    {
        $post = $_POST;
        $fileName = Arrays::get($post, 'fileName');
        $fileUrl = Arrays::get($post, 'fileUrl');
        $company_order_annex_id = Arrays::get($post,'company_order_annex_id');
        $data['b2b_booking_id'] = $_GET['b2b_booking_id'];
        $data['annex'] = [];
        foreach($fileUrl as $k=>$v){
            $ar['annex_name'] = $fileName[$k];
            $ar['url'] = $fileUrl[$k];

            if($company_order_annex_id[$k]){
                $ar['company_order_annex_id'] = $company_order_annex_id[$k];
            }
            $data['annex'][] = $ar;
            unset($ar);
        }

        return $this->callSoaErp('post','/b2b_booking/updateB2bBookingAnnex',$data);
    }



    public function updateBookingFormSales(){
        $where['b2b_booking_id'] = $_GET['b2b_booking_id'];
        $bookingSales = $this->callSoaErp('post','/b2b_booking/getB2bBookingSales',$where);
        $this->assign('bookingSales',$bookingSales['data']);
        unset($where);
//        echo '<pre>'; print_r($bookingSales);exit;

        $where['company_id'] =  session('user')['company_id'];
        $productAll = $this->callSoaErp('post', '/btob/getProduct',$where);
        $this->assign('productAll',$productAll['data']);
        $productGroup = Arrays::group($productAll['data'],'product_id');
        $this->assign('productGroup',$productGroup);
//        var_dump($productAll);exit;

        return $this->fetch('update_booking_form_sales');
    }


    public function updateBookingFormSalesAjax(){

        $post = $_POST;

        $ar['invoice']['invoice_type'] = Arrays::get($post,'invoice_type');
        $ar['invoice']['is_ready'] = Arrays::get($post,'is_ready');
        $ar['invoice']['invoice_type'] = Arrays::get($post,'invoice_type');
        $ar['invoice']['status'] = Arrays::get($post,'status');

        $ar['sales'] = [];
        $b2b_booking_sales_id = Arrays::get($post,'b2b_booking_sales_id');
        $description = Arrays::get($post,'description');
        $is_gst = Arrays::get($post,'is_gst');
        $net_unit_price = Arrays::get($post,'net_unit_price');
        $product_code_id = Arrays::get($post,'product_code_id');
        $qty = Arrays::get($post,'qty');
        $sales_status = Arrays::get($post,'sales_status');

        $ar['b2b_booking_id'] = $_GET['b2b_booking_id'];
        foreach($product_code_id as $k=>$v){
            $ary['description'] = $description[$k];
            $ary['qty'] = $qty[$k];
            $ary['net_unit_price'] = $net_unit_price[$k];
            $ary['status'] = $sales_status[$k];
            $ary['is_gst'] = $is_gst[$k];
            $ary['product_code_id'] = $product_code_id[$k];
            if($b2b_booking_sales_id[$k]){
                $ary['b2b_booking_sales_id'] = $b2b_booking_sales_id[$k];
            }
            $ar['sales'][] = $ary;
            unset($ary);
        }

        return $this->callSoaErp('post','/b2b_booking/updateB2bBookingInvoice',$ar);

    }

    //收款
    public function payMent(){
        return $this->fetch('pay_ment');
    }

    //留言板
    public function messageBoard(){
        $b2b_booking_id = $_GET['b2b_booking_id'];

        $w['b2b_booking_id'] = $b2b_booking_id;
        $bookingComment = $this->callSoaErp('post','/b2b_booking/getB2bBookingComment',$w);
        $this->assign('bookingComment',$bookingComment['data']);
//        var_dump($bookingComment);exit;

        return $this->fetch('message_board');
    }

    //添加留言
    public function addMessageBoardAjax(){
        $b2b_booking_id = $_GET['b2b_booking_id'];
        $mess =$_POST['mess'];

        $d['comment'] = $mess;
        $d['create_user_id'] = session('user')['user_id'];
        $d['b2b_booking_id'] = $b2b_booking_id;
        return $this->callSoaErp('post','/b2b_booking/addB2bBookingComment',$d);

    }


    //后台创建订单
    public function createBookingForm(){
//        echo '<pre>';print_r($_SESSION);exit;

        //获取所有的 agent
        $params['choose_company_id'] = session('user')['company_id'];
        $params['status'] = 1;
        $result = $this->callSoaErp('post', '/btob/getBtoBDistributor', $params);
        $this->assign('all_agent',$result['data']);

        unset($params);unset($result);
        //获取B2Btour
        $data['status'] = 1;
        $data['company_id'] =  session('user')['company_id'];
        $result = $this->callSoaErp('post', '/b2b_tour/getB2bTour',$data);
        $this->assign('all_tours',$result['data']);
        unset($data);unset($result);

        return $this->fetch('create_booking_form');

    }

    public function createBookingFormAjax(){
        //获取产品信息
        $where['btb_tour_id'] = $_POST['s_tour'];
        $result = $this->callSoaErp('post','/b2b_tour/getB2bTourGeneral',$where);
        session::delete('adminBooking_select_tour');
        session::delete('adminBooking_createBookingFormAjax');
        session::set('adminBooking_select_tour',$result['data']);
        session::set('adminBooking_createBookingFormAjax',$_POST);
        return ['code'=>200];
    }

    //后台创建订单
    public function addBookingAjax()
    {

    }

    public function select_date(){
//        echo '<pre>';print_r($_SESSION);exit;

        //获取团日期
        $where['status'] = 1;
        $where['btb_tour_id'] = session('adminBooking_createBookingFormAjax')['s_tour'];
        $result = $this->callSoaErp('post','/b2b_tour/getB2bTourDates',$where);
        $dateOj = [];
        foreach($result['data'] as $date){
            if(date('Y-m-d')<=$date['arrival_date'])
                $dateOj[] = date('d-m-Y',strtotime($date['arrival_date']));
        }
        sort($dateOj);
        $this->assign('dateOj',$dateOj);
//        var_dump($dateOj);exit;
        unset($date);unset($result);

        //获取语言
        $where['btb_tour_id'] = session('adminBooking_createBookingFormAjax')['s_tour'];
        $result = $this->callSoaErp('post', '/b2b_tour/getB2bTour',$where);
        $this->assign('sLanguage',explode(',',$result['data'][0]['tour_languages']));
        unset($date);unset($result);
        $where = ['status'=>1];
        $Language = $this->callSoaErp('post','/system/getLanguage',$where);
        $this->assign('Language',$Language['data']);
//        var_dump($Language);exit;
        unset($date);

        return $this->fetch('select_date');
    }

    public function select_dateAjax(){
        session::delete('adminBooking_select_dateAjax');
        session::set('adminBooking_select_dateAjax',$_POST);
        return ['code'=>200];
    }

    public function passenger_details(){
//        echo '<pre>';print_r($_SESSION);exit;
        $where['status'] = 1;
        $where['level'] = 1;
        $nations = $this->callSoaErp('post','/system/getCountry',$where);
        $this->assign('all_nations',$nations['data']);
//        var_dump($nations);exit;

        return $this->fetch('passenger_details');
    }


    //获取年龄
    public function check_age($dob, $tour_date, $tour_id){

        $date_format = date('Y',strtotime($tour_date));
    
        $bday = date('Y',strtotime($dob));

        $age = ($date_format-$bday)?($date_format-$bday):1;
        

        $infant = session('adminBooking_select_tour')['infant'];
        $child  = session('adminBooking_select_tour')['child'];

        if($age <= $infant) {
            return "Infant";
        }
        if($age > $child) {
            return "Adult";
        } else {
            return "Child";
        }

    }

    public function passenger_detailsAjax(){
        $post = Request::instance()->param();

        $lname 			=		Arrays::get($post,'lname');
        $fname 			= 		Arrays::get($post,'fname');
        $dob 			= 		Arrays::get($post,'dob');
        $passport 		= 		Arrays::get($post,'passport');
        $nationality 	= 		Arrays::get($post,'nationality');
        $ethnicity 		= 		Arrays::get($post,'ethnicity');
        $gender 		= 		Arrays::get($post,'gender');
        $speaking 		= 		Arrays::get($post,'speaking');
        $temp_id 		= 		array();

        for($i=0;$i<count($lname);$i++){
            $age = $this->check_age($dob[$i],session('adminBooking_select_dateAjax')['s_date'],session('adminBooking_createBookingFormAjax')['s_tour']);
            $age_cat[]  = $age;
            $allow_nbed_arr[] = 1;
            $nation_type[] = 0;
            $temp_id[$i] = $i+1;
        }

        $passenger_info = [
            'temp_id'		=>  $temp_id,
            'lname' 		=> 	$lname,
            'fname' 		=> 	$fname,
            'dob' 			=> 	$dob,
            'passport' 		=> 	$passport,
            'nation_type'	=>  $nation_type,  // Use for pricing
            'nationality' 	=> 	$nationality,
            'ethnicity' 	=> 	$ethnicity,
            'gender' 		=> 	$gender,
            'age_cat'		=> 	$age_cat,
            'allow_nbed'    =>  $allow_nbed_arr,
            'speaking'		=> 	$speaking
        ];

        session::set('adminBooking_passenger_detailsAjax',$passenger_info);
        return ['code'=>200];
    }

    public function room_config(){
//        echo '<pre>';print_r($_SESSION);exit;

        //获取房型
        $where['btb_tour_id'] = session('adminBooking_createBookingFormAjax')['s_tour'];
        $result = $this->callSoaErp('post','/b2b_tour/getB2bTourRoom',$where);
        $this->assign('all_rooms',$result['data']);
//        var_dump($result);
        unset($where);unset($result);

        $where['status'] =1;
        $where['language_id'] = session('user')['language_id'];
        $result = $this->callSoaErp('post', '/system/getRoomTypeAjax', $where);
        $roomType = Arrays::group($result['data'],'room_type_id');
//        var_dump($roomType);exit;
        $this->assign('roomType',$roomType);

        return $this->fetch('room_config');
    }


    public function room_configAjax(){
        session::delete('adminBooking_room_configAjax');
        session::set('adminBooking_room_configAjax',$_POST);
        return ['code'=>200];
    }

    public function clear_room_configAjax(){
        session::delete('adminBooking_room_configAjax');
        return ['code'=>200];
    }


    public function service_requests(){
//        echo '<pre>';print_r($_SESSION);exit;

        //获取接送机信息
        $where['btb_tour_id'] = session('adminBooking_select_tour')['btb_tour_id'];
        $result = $this->callSoaErp('post','/b2b_tour/getB2bTourTransfer',$where);
        $tourTransfer = Arrays::group($result['data'],'type');
//        var_dump($tourTransfer);exit;
        $this->assign('tourTransfer',$tourTransfer);
        unset($where);

        //获取房型
        $where['btb_tour_id'] = session('adminBooking_select_tour')['btb_tour_id'];
        $result = $this->callSoaErp('post','/b2b_tour/getB2bTourRoom',$where);
        $this->assign('all_rooms',$result['data']);
//        var_dump($result['data']);
        unset($where);unset($result);
        $where['status'] =1;
        $where['language_id'] = session('user')['language_id'];
        $result = $this->callSoaErp('post', '/system/getRoomTypeAjax', $where);
        $roomType = Arrays::group($result['data'],'room_type_id');
//        var_dump($roomType);exit;
        $this->assign('roomType',$roomType);

        return $this->fetch('service_requests');
    }


    public function service_requestsAjax(){
        session::delete('adminBooking_service_requestsAjax');
        session::set('adminBooking_service_requestsAjax',$_POST);
        return ['code'=>200];
    }

    public function clear_service_requestsAjax(){
        session::delete('adminBooking_service_requestsAjax');
        return ['code'=>200];
    }

    public function booking_summary(){
//        echo '<pre>';print_r($_SESSION);exit;
        $agentId = session('adminBooking_createBookingFormAjax')['s_agent'];
        $data['distributor_id'] = $agentId;
        $distributorData = $this->callSoaErp('post', '/btob/getDistributorConsultant', $data);
//        var_dump($distributorData);exit;
        $this->assign('distributorData',$distributorData['data']);

        return $this->fetch('booking_summary');
    }

    public function booking_summaryAjax(){
        session::delete('adminBooking_service_booking_summaryAjax');
        session::set('adminBooking_service_booking_summaryAjax',$_POST);
        return ['code'=>200];
    }

    /**
     *  Function to check if the customer
     *  the customer is single suppliment
     *  @param $room_type -- string
     *  @param $max_capacity -- integer
     *  @param $pax_num -- integer -- number of pax
     *
     *
     *  @return integer
     **/
    public function check_single_suppliment($room_type)
    {
        $single_suppliment = 0;
        // check is single suppliment
        if($room_type == "Single")
        {
            return $single_suppliment = 1;
        }
        else
        {
            return $single_suppliment;
        }
    }

    public function createBookingAjax()
    {

        session::delete('adminBooking_service_booking_summaryAjax');
        session::set('adminBooking_service_booking_summaryAjax',$_POST);

        $post = $_SESSION;

        $data['begin_time'] = strtotime($post['think']['adminBooking_select_dateAjax']['s_date']); //开团日期
        $data['emergency_name'] = $post['think']['adminBooking_service_booking_summaryAjax']['emergecy_name'];      //紧急联系人
        $data['emergency_phone'] = $post['think']['adminBooking_service_booking_summaryAjax']['emergecy_phone'];      //紧急联系人tel
        $data['emergency_email'] = $post['think']['adminBooking_service_booking_summaryAjax']['emergecy_email'];      //紧急联系人email
        $data['btb_tour_id'] = $post['think']['adminBooking_createBookingFormAjax']['s_tour'];    //b2b产品id
        $data['language_id'] = $post['think']['adminBooking_select_dateAjax']['s_language'];      //语言id
        $data['special_requests'] = $post['think']['special_requests_text']['special_requests'];       //特殊要求
        $data['lead_pax_mobile'] = $post['think']['adminBooking_service_booking_summaryAjax']['emergecy_phone'];          //游客联系人
        $data['agent_reference_id'] = $post['think']['adminBooking_service_booking_summaryAjax']['reference_no'];     //代理商相关ID
        $data['agent_id'] = $post['think']['adminBooking_createBookingFormAjax']['s_agent'];          //代理id
        $data['consultant'] = $post['think']['adminBooking_service_booking_summaryAjax']['consultant_name'];          //代理商顾问
        //游客信息
        $data['customer'] = [];
        foreach ($post['think']['adminBooking_passenger_detailsAjax']['temp_id'] as $customer_k => $customer_v) {
            $arr = [];
            $arr['seat_no'] = $customer_v;           //座位号
            $arr['customer_last_name'] = $post['think']['adminBooking_passenger_detailsAjax']['lname'][$customer_k];        //名
            $arr['customer_first_name'] = $post['think']['adminBooking_passenger_detailsAjax']['fname'][$customer_k];       //姓
            $arr['gender'] = $post['think']['adminBooking_passenger_detailsAjax']['gender'][$customer_k] == 'Male' ? 1 : 2;       //性别
            $arr['passport_number'] = $post['think']['adminBooking_passenger_detailsAjax']['passport'][$customer_k]; //护照号
            $arr['birthday'] = $post['think']['adminBooking_passenger_detailsAjax']['dob'][$customer_k]; //生日
            $arr['age_group'] = $post['think']['adminBooking_passenger_detailsAjax']['age_cat'][$customer_k]; //age_group
            $arr['speaking_fluent'] = $post['think']['adminBooking_passenger_detailsAjax']['speaking'][$customer_k]; //speaking_fluent
            $arr['ethnicity'] = $post['think']['adminBooking_passenger_detailsAjax']['ethnicity'][$customer_k]; //种族
            $arr['nationality'] = $post['think']['adminBooking_passenger_detailsAjax']['nationality'][$customer_k];  //国籍
            array_push($data['customer'], $arr);

            $data['is_pre_paid'] = $_POST['is_prepaid_tip'] ?: 0;  //两者都允许
        }

        //房间数组
        $data['room'] = [];
        foreach ($post['think']['adminBooking_room_configAjax']['room_type'] as $k => $room_v) {
            $arr = [];
            $arr['temp_group'] = $post['think']['adminBooking_room_configAjax']['pax_tmp_ids'][$k];           //座位号字符串
            $arr['room_type'] = $post['think']['adminBooking_room_configAjax']['room_type'][$k];        //房间类型
            $arr['charge_num'] = 0;
            $arr['room_code'] = $k + 1;
            $arr['charge_type'] = 1;
            array_push($data['room'], $arr);
        }

        $data['transfer'] = [];
        $data['airport_transfer'] = $data['hotel_transfer'] = 0;
        //接机

        if ($post['think']['adminBooking_service_requestsAjax']['hotel_transfer'] == 1) {
            foreach ($post['think']['adminBooking_service_requestsAjax']['hotel_transfer_date'] as $k => $hotel_transfer_info_v) {
                $arr = [];
                $arr['transfer_type'] = 1;
                $arr['airport'] = $post['think']['adminBooking_service_requestsAjax']['service_requests_select_hotel'][$k];    //机场
                $arr['flight'] = $post['think']['adminBooking_service_requestsAjax']['hotel_transfer_flight'][$k];    //航班号
                $arr['temp_group'] = $post['think']['adminBooking_service_requestsAjax']['hotel_transfer_tour_pax'][$k];  //游客号
                $arr['date'] = $post['think']['adminBooking_service_requestsAjax']['hotel_transfer_date'][$k];      //日期
                $arr['time'] = $post['think']['adminBooking_service_requestsAjax']['hotel_transfer_time'][$k];      //时间
                $data['hotel_transfer'] += count(explode(',', $post['think']['adminBooking_service_requestsAjax']['hotel_transfer_tour_pax'][$k]));
                array_push($data['transfer'], $arr);
            }
        }

        //送机
        if ($post['think']['adminBooking_service_requestsAjax']['airport_transfer'] == 1) {
            foreach ($post['think']['adminBooking_service_requestsAjax']['airport_transfer_date'] as $k => $airport_transfer_info_v) {
                $arr = [];
                $arr['transfer_type'] = 2;
                $arr['airport'] = $post['think']['adminBooking_service_requestsAjax']['service_requests_select_airport'][$k];    //机场
                $arr['flight'] = $post['think']['adminBooking_service_requestsAjax']['airport_transfer_flight'][$k];    //航班号
                $arr['temp_group'] = $post['think']['adminBooking_service_requestsAjax']['airport_transfer_tour_pax'][$k];  //游客号
                $arr['date'] = $post['think']['adminBooking_service_requestsAjax']['airport_transfer_date'][$k];      //日期
                $arr['time'] = $post['think']['adminBooking_service_requestsAjax']['airport_transfer_time'][$k];      //时间
                $data['airport_transfer'] += count(explode(',', $post['think']['adminBooking_service_requestsAjax']['airport_transfer_tour_pax'][$k]));
                array_push($data['transfer'], $arr);
            }
        }


        $data['accommodation'] = [];

        //提前
        if ($post['think']['adminBooking_service_requestsAjax']['pre_accommodation'] == 1) {
            foreach ($post['think']['adminBooking_service_requestsAjax']['pre_accommodation_nights'] as $k => $pre_tour_accommodation_info_v) {
                $arr = [];
                $arr['accommodation_type'] = 1;
                $arr['room_type'] = $post['think']['adminBooking_service_requestsAjax']['pre_accommodation_room_type'][$k];    //房型
                $arr['nights'] = $post['think']['adminBooking_service_requestsAjax']['pre_accommodation_nights'][$k];    //几晚
                $arr['temp_group'] = $post['think']['adminBooking_service_requestsAjax']['pre_accommodation_pax'][$k];  //游客号
                array_push($data['accommodation'], $arr);
            }
        }

        //延后
        if ($post['think']['adminBooking_service_requestsAjax']['post_accommodation'] == 1) {
            foreach ($post['think']['adminBooking_service_requestsAjax']['post_accommodation_nights'] as $k => $post_tour_accommodation_info_v) {
                $arr = [];
                $arr['accommodation_type'] = 2;
                $arr['room_type'] = $post['think']['adminBooking_service_requestsAjax']['post_accommodation_room_type'][$k];    //房型
                $arr['nights'] = $post['think']['adminBooking_service_requestsAjax']['post_accommodation_nights'][$k];    //几晚
                $arr['temp_group'] = $post['think']['adminBooking_service_requestsAjax']['post_accommodation_pax'][$k];  //游客号
                array_push($data['accommodation'], $arr);
            }
        }

        //附件
        $passports_name = $post['think']['adminBooking_service_booking_summaryAjax']['passports_name'];
        $passports = $post['think']['adminBooking_service_booking_summaryAjax']['passports'];
        $data['upload_passport'] = [];
        foreach($passports as $k=>$v){
            $arr = [];
            $arr['url'] = $passports[$k];
            $arr['annex_name'] = $passports_name[$k];
            array_push($data['upload_passport'], $arr);
        }

        $result = $this->callSoaErp('post', '/b2b_booking/addB2bBooking', $data);

        if ($result['code'] == 200) {
            $product_arr['company_order_number'] = $result['data']['company_order_number']; //订单标号
            //分公司产品
            $product_arr['branch_product_array'][0]['branch_product_number'] = $result['data']['branch_product_number'];  //产品编号
            $product_arr['branch_product_array'][0]['branch_product_name'] = $result['data']['branch_product_name'];      //产品名称
            $product_arr['branch_product_array'][0]['branch_product_params_array'][0]['team_product_id'] = $result['data']['team_product_id'];      //团队产品id
            $product_arr['branch_product_array'][0]['branch_product_params_array'][0]['route_template_number'] = $result['data']['route_template_number'];      //线路模板编号

            $result_product = $this->callSoaErp('post', '/branchcompany/addCompanyOrderProduct', $product_arr);
/*
            if ($result_product['code'] == 400) {
                return $result_product;
            }*/
        }

//        session::delete('adminBooking_select_tour');
//        session::delete('adminBooking_createBookingFormAjax');
//        session::delete('adminBooking_select_dateAjax');
//        session::delete('adminBooking_passenger_detailsAjax');
//        session::delete('adminBooking_room_configAjax');
//        session::delete('adminBooking_service_requestsAjax');
//        session::delete('adminBooking_service_booking_summaryAjax');

//        print_r($result);die;
        return $result;

    }


    //下载表格
    public function approvePlanExcel(){
        $b2bBookingStatus = Contents::b2bBookingStatus();
        $b2bOfficeStatus = Contents::b2bOfficeStatus();
        $b2bPayment = Contents::b2bPayment();

        $this->assign('b2bBookingStatus',$b2bBookingStatus);
        $this->assign('b2bOfficeStatus',$b2bOfficeStatus);
        $this->assign('b2bPayment',$b2bPayment);


        $w['s_tour'] = $_GET['s_tour'];
        $w['s_agentCode'] = $_GET['s_agentCode'];
        $w['s_createdFrom'] = $_GET['s_createdFrom'];
        $w['s_createdTo'] = $_GET['s_createdTo'];
        $w['s_bkId'] = $_GET['s_bkId'];
        $w['s_office_status'] = $_GET['s_office_status'];
        $w['s_booking_status'] = $_GET['s_booking_status'];
        $w['is_inbound'] = $_GET['is_inbound'];

        $app =  $this->callSoaErp('post', '/b2b_operate/approvePlan',$w);
        $this->assign('d',$app['data']);


        $fileName = "Approve Plan " .date("Y-m-d His");
        header("HTTP/1.0 200 OK");
        header("HTTP/1.1 200 OK");
        header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
        header("Expires: 0");
        header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
        header('Cache-Control: max-age=0'); //no cache
        header("Content-disposition: attachment; filename=\"{$fileName}.xls\"");
        return $this->fetch('approve_plan_msxls');

    }


    //Approve Plan
    public function approvePlan(){
        //处理状态 //付款状态
        $b2bBookingStatus = Contents::b2bBookingStatus();
        $b2bOfficeStatus = Contents::b2bOfficeStatus();
        $b2bPayment = Contents::b2bPayment();

        $this->assign('b2bBookingStatus',$b2bBookingStatus);
        $this->assign('b2bOfficeStatus',$b2bOfficeStatus);
        $this->assign('b2bPayment',$b2bPayment);

        $data['status'] = 1;
        $data['company_id'] =  session('user')['company_id'];
        $result = $this->callSoaErp('post', '/b2b_tour/getB2bTour',$data);
        $this->assign('all_tours',$result['data']);


        $w = [
            'page'=>$this->page(),
            'page_size'=>$this->_page_size,
        ];

        $w['s_tour'] = $_GET['s_tour'];
        $w['s_agentCode'] = $_GET['s_agentCode'];
        $w['s_createdFrom'] = $_GET['s_createdFrom'];
        $w['s_createdTo'] = $_GET['s_createdTo'];
        $w['s_bkId'] = $_GET['s_bkId'];
        $w['s_office_status'] = $_GET['s_office_status'];
        $w['s_booking_status'] = $_GET['s_booking_status'];
        $w['is_inbound'] = $_GET['is_inbound'];
        $app =  $this->callSoaErp('post', '/b2b_operate/approvePlan',$w);
        $this->assign('d',$app['data']['list']);
//        echo '<pre>';print_r($app);
//        exit;
//        print_r($w);exit;
        $this->getPageParams($app);
        return $this->fetch('approve_plan');
    }

    public function updateApprovePlanAjax(){
        $post = $_POST;
        $bus_no = Arrays::get($post,'bus_no');
        $seat = Arrays::get($post,'seat');
        $booking_status = Arrays::get($post,'booking_status');
        $office_status = Arrays::get($post,'office_status');

        $d=[];
        foreach($bus_no as $k=>$v){
            $ar['BKID'] = $k;
            $ar['bus_no'] = $bus_no[$k];
            $ar['booking_status'] = $booking_status[$k];
            $ar['office_status'] = $office_status[$k];

            $seat_ar = explode(',',$seat[$k]);
            $oj = [];
            foreach($seat_ar as $vv){
                $a = explode('-',$vv);
                if(count($a)==2){
                    for($ii=$a[0];$ii<=$a[1];$ii++){
                        $oj[] = $ii;
                    }
                }else{
                    $oj[] = $vv;
                }
            }
            $ar['seat'] = $oj;
            $d[] = $ar;
            unset($ar);
        }

//        return $d;
        return $this->callSoaErp('post','/b2b_operate/updateApprovePlan',$d);


    }





    public function getTaxInvoiceAjax()
    {
        $params = Request::instance()->param();
        //$params['b2b_booking_id'] = 3;
        $result = $this->callSoaErp('post', '/b2b_booking/getB2bBookingSales', $params);

        if ($result['code'] == 200)
        {
            $result['data']['invoice']['total_inc_gst'] = bcadd($result['data']['invoice']['myob_sales'],$result['data']['invoice']['myob_cost'],2);
            $result['data']['invoice']['gst'] = 0;
            foreach ($result['data']['sales'] as $sales_v)
            {
                if ($sales_v['is_gst'] == 1)
                {
                    $result['data']['invoice']['gst'] = bcadd($result['data']['invoice']['gst'], bcdiv($sales_v['net_total'], 11,2),2);
                }
            }
        }
        unset($result['data']['receivable_cope']);
        $result['data']['invoice']['amount_paid'] = 0.00;
        
        return $result;
    }

    public function getVoucherAjax()
    {
        $params = Request::instance()->param();
        //$params['b2b_booking_id'] = 250;

        $general = $this->callSoaErp('post', '/b2b_booking/getB2bBookingGeneral', $params);

        $general['data']['customer'] = $this->callSoaErp('post', '/b2b_booking/getB2bBookingCustomer', $params)['data'];

        $general['data']['booking_notice'] = $this->callSoaErp('post', '/system/getReturnReceiptInfo', ['return_receipt_id' => $general['data']['booking_notice']])['data'][0];

        $general['data']['itinerary'] = $this->callSoaErp('post', '/b2b_tour/getB2bTourItinerary', ['btb_tour_id' => $general['data']['btb_tour_id']])['data'];

        $general['data']['room'] = $this->callSoaErp('post', '/b2b_booking/getB2bBookingRoom', $params)['data'];

        return $general;
    }

    //
    public function export_to_msword(){

        header("HTTP/1.0 200 OK");
        header("HTTP/1.1 200 OK");
        header("Content-Type: application/vnd.ms-word; charset=UTF-8");
        //header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header("Expires: 0");
        header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
        header('Cache-Control: max-age=0'); //no cache
        header("Content-disposition: attachment; filename=\"" . $_GET['b2b_booking_id'] . "_AUS.doc\"");

        $d['b2b_booking_id'] = $_GET['b2b_booking_id'];
        $d =  $this->callSoaErp('post','/b2b_operate/export_to_msword',$d);
        $d['data']['home'] = Arrays::group($d['data']['home'],'room_type');
        $d['data']['transfer'] = Arrays::group($d['data']['transfer'],'transfer_type');
        $this->assign('d',$d['data']);

//        echo '<pre>';print_r($d);exit;

        return $this->fetch('booking_form_msword');
    }

}