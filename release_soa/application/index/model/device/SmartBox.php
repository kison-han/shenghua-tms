<?php


namespace app\index\model\device;
use app\common\help\Contents;
use think\Db;

use think\Model;

class SmartBox extends Model
{



    protected $table = "device_box";

    public function getInfo($params){
        return $this->where('device_number',$params['device_number'])->find();
    }

    public function updateInfo($params)
    {

        $data=$params;
        $data['update_time']=time();
        $device_number = $params['device_number'];
        $datainfo = $this->where('device_number', $device_number)->find();;
        
        $data['create_time']=time();
        if($datainfo)
        $datainfo->allowField(true)->save($data);
        Db::name("device_box_log")->strict(false)->insert($data);
		/*  Db::startTrans();
        try {


		 Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
            //\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            //exit();

        }*/

        return $result;
		
		
  //Db::name("device_box_log")->allowField(true)->save($data);
    }
    public function getList($params,$openCount=false){
        $where = [];
        //$where['status'] = 1;
        if($openCount) {
            $page = isset($params['page']) ? $params['page'] : 1;
            $pageSize = isset($params['$pageSize']) ? $params['$pageSize'] :Contents::PAGE_SIZE;
            $data['count'] = $this->where($where)->count();
            $data['page_count']=ceil($data['count']/$pageSize);
            $data['list']=$this->where($where)->page($page,$pageSize)->order('device_id','desc')->select();
            return $data;
        }
        else
        { $data['list']=$this->where($where)->order('device_id','desc')->select();
        return $data;}
    }

}