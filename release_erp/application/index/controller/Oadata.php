<?php

namespace app\index\controller;

use app\common\help\Help;
use think\Request;

class Oadata extends Base
{
    /**
     *  圣华
     *  办公自动化
     */
    public function index(){

		return $this->fetch('index');

    }

    /**
     *  圣华
     *  资料分类显示页面
     */
    public function showDataGroupManage(){

        $params = Request::instance()->param();

        $data = [
            'page'=>$this->page(),
            'page_size'=>$this->_page_size,
        ];
        $data_all = array_merge($params,$data);

        $result = $this->callSoaErp('post','/oadatagroup/getDataGroup',$data_all);
        $this->assign('warehouseResult',$result['data']['list']);
        $this->getPageParams($result);

        return $this->fetch('oadatagroup_manage');

    }

    /**
     *  圣华
     *  资料分类新增页面
     */
    public function showDataGroupAdd(){

        if(is_numeric(input('group_id'))) {
            //修改界面
            $data = [
                'group_id'=>input('group_id')
            ];
            //获取资料
            $oadata = $this->callSoaErp('post','/oadatagroup/getDataGroup',$data);
            $this->assign("oadataResult", $oadata['data'][0]);
        }

        return $this->fetch('oadatagroup_add');

    }

    /**
     * 圣华
     * 资料分类新增AJAX / 修改AJAX
     */
    public function addDataGroupAjax(Request $request)
    {

        $data = $request->param();

        if(is_numeric($data['group_id'])){
            //修改资料
            $data['user_id'] = session("user_id");
            $result = $this->callSoaErp('post', '/oadatagroup/updateDataGroupByGroupId',$data);
            return $result;
        }else {
            //新增资料
            $data['user_id'] = session("user_id");
            $result = $this->callSoaErp('post', '/oadatagroup/addDataGroup', $data);
            return $result; //['code' => '400', 'msg' => $data];
        }

    }

    /**
     *  圣华
     *  资料显示页面
     */
    public function showDataManage(){

        $params = Request::instance()->param();

        $data = [
            'page'=>$this->page(),
            'page_size'=>$this->_page_size,
        ];
        $data_all = array_merge($params,$data);

        $result = $this->callSoaErp('post','/oadata/getData',$data_all);

        $this->assign('warehouseResult',$result['data']['list']);
        $this->getPageParams($result);

        return $this->fetch('oadata_manage');

    }

    /**
     *  圣华
     *  资料新增页面
     */
    public function showDataAdd(){

        if(is_numeric(input('data_id'))) {
            //修改界面
            $data = [
                'data_id'=>input('data_id')
            ];
            //获取资料
            $oadata = $this->callSoaErp('post','/oadata/getData',$data);
            $this->assign("oadataResult", $oadata['data'][0]);
        }

        return $this->fetch('oadata_add');

    }

    /**
     * 圣华
     * 资料新增AJAX / 修改AJAX
     */
    public function addDataAjax(Request $request)
    {

        $data = $request->param();

        if(is_numeric($data['data_id'])){
            //修改资料
            $data['user_id'] = session("user_id");
            $result = $this->callSoaErp('post', '/oadata/updateDataByDataId',$data);
            return $result;
        }else {
            //新增资料
            $data['user_id'] = session("user_id");
            $result = $this->callSoaErp('post', '/oadata/addData', $data);
            return $result; //['code' => '400', 'msg' => $data];
        }

    }

    /**
     * 获取资料分类组装json数据
     */
    public function getDataGroupAjax(){
        $result = $this->callSoaErp('post','/oadatagroup/getDataGroupAjax');
        return $result;
    }
}