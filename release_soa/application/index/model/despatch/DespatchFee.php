<?php


namespace app\index\model\despatch;


use app\index\model\Base;

class DespatchFee extends Base
{
protected $table="despatch_fee";

public function addDespatchFee($params){
    foreach ($params['fee_id_zhengche'] as $k=>$v)
    {
        $data['cost_id']=$v;
        $data['despatch_id']=$params['despatch_id'];
        $data['cost_fee']=$params['money_zhengche'][$k];
        $data['remark']=$params['remark_zhengche'][$k];
        $this->pubInsert($data);
    }
}

}