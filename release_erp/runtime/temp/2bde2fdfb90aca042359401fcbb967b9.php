<?php if (!defined('THINK_PATH')) exit(); /*a:5:{s:79:"/var/www/html/test_erp/public/../application/index/view/bill/customer_bill.html";i:1666324175;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:64:"/var/www/html/test_erp/application/index/view/public/header.html";i:1658978091;s:67:"/var/www/html/test_erp/application/index/view/public/left_menu.html";i:1665286673;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






    <title>账单管理</title>
    <link href="/static/css/source.css" rel="stylesheet">
    <style>
        .xm-select-parent .xm-select {
            min-height: 16px;
        }
        .xm-select-parent div {
            height: 30px;
        }
    </style>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
            <div class="layui-logo"></div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left">
        <li class="layui-nav-item">
            <a href="/turnoverbox/index">智能周转箱</a>
        </li>
        <li class="layui-nav-item">
            <a href="/">TMS</a>
        </li>
        <li class="layui-nav-item">
 
            <a href="/warehouse/index">WMS</a>
        </li>
        <li class="layui-nav-item">
            <a href="/wisdompark/index">智慧园区</a>

        </li>
        <li class="layui-nav-item">
            <a href="/Oa/index">OA</a>

        </li>   
         <li class="layui-nav-item">
            <a href="/Bms/index">BMS</a>

        </li>
        <li class="layui-nav-item">
            <a href="/equipment/equipmentManage">设备</a>

        </li>
    </ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item tips-system-message" style="cursor: pointer"><?php echo $language_tag['index_nav_system_message']; ?> <span class="system-message-quantity" style="color: red"> 1</span> </li>
      <li class="layui-nav-item">
        <a href="javascript:;">
          <!-- <img src="http://t.cn/RCzsdCq" class="layui-nav-img"> -->
          <?php echo \think\Session::get('user.nickname'); ?>
        </a>
        <dl class="layui-nav-child head-top">
          <dd><a href="/system/setUserInfo/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_basicDocument']; ?></a></dd>
          <dd><a href="/system/showChangePassword/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_resetPassword']; ?></a></dd>
        </dl>
      </li>
      <li class="layui-nav-item"><a href="/login/loginOut">退出登录</a></li>
    </ul>

    <div id="tips-system-message-js" style='display:none;'>
        <div  style="max-height:350px;width: 450px">
             <div style="color: #000;text-align: center;height: 10px;padding: 10px">你有 <font class="f-tips-system-message"></font> 条未读消息</div>
             <hr>
             <div class="tips-system-message-div" style="height:220px;overflow-y: auto">
           
                 <li style="color: #000; padding: 10px;cursor: pointer" data-href="" data-id="" class="aUrl" onclick="Aurl(this)" ></li>
                 <hr>
             
             </div>
            <div style="color: #000;text-align: center;height: 40px;padding-top: 15px"><a href="/reminderManagement/allInStationLetter"><?php echo $language_tag['index_nav_viewAll_messa']; ?></a></div>
        </div>
    </div>

    <!--  下列保存用户SESSION信息 -->
	
    <input type="hidden" id='now_url'  value="<?php echo $now_url; ?>" />
	<input type="hidden" id='after_url'  value="<?php echo $after_url; ?>" />
    <input type="hidden" id='user_company_id' value="<?php echo \think\Session::get('user.company_id'); ?>" />
	<!--  下列保存其他信息 -->
	<input type="hidden" id='http_referer' value="<?php echo $http_referer; ?>" />
	
	<script type='text/javascript'>
		$('.tips-system-message').on('click',function(){
			var html = $('#tips-system-message-js').html();
		    layer.tips(html, '.tips-system-message', {
		        tips: [3, '#fff'],
		        padding:'20',
		        tipsMore: false,
		        area: ['450px', 'auto'],
		        shade: [0.01, '#fff'],
		        shadeClose:true,
		        time:0
			
		    });
		
		});
		
		$(document).ready(function(){ 
			$(document).mousemove(function(e){ 
				
				if(e.pageX<=10){
					$('.layui-bg-black').show()
				}
				
				if(e.pageX>200){
					$('.layui-bg-black').hide()
				
				}
				if(e.pageY<=10){
					$('.layui-header').show()
				}
				
				if(e.pageY>40){
					$('.layui-header').hide()
				
				}			
			}); 
			
		}); 
	

	</script>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
                  <ul class="layui-nav layui-nav-tree" id="left-nav" lay-filter="test">
          <li class="layui-nav-item"><a href="/"><i class="layui-icon layui-icon-chart-screen"></i>   <em>控制面板</em></a></li>

          <li <?php if($controller_name == 'order'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-form"></i><em>运单管理</em></a>
        <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"showordermanage,showorderadd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderManage?multi_order_status=1">运单管理</a></dd>
			<!--<dd <?php if(in_array(($function_name), explode(',',"showordertrackmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderTrackManage">运单跟踪</a></dd>-->
			 <?php if(\think\Session::get('user.role_id') == 1 || \think\Session::get('user.role_id') == 15): ?>  <dd <?php if(in_array(($function_name), explode(',',"showordertrackmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderTrackManage?sign=0&multi_order_status=2,3,4,5">运单跟踪</a></dd><?php endif; ?> 
            <dd <?php if(in_array(($function_name), explode(',',"showorderreceiptmanage,showorderreceiptadd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderReceiptManage?receipt_status=0">回单管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showorderabnormalmanage,showorderabnormaladd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderAbnormalManage?handle=1">异常运单</a></dd>
  			
  			<dd <?php if(in_array(($function_name), explode(',',"incomeaccountingmanage,addorderincome"))): ?> class="layui-this"<?php endif; ?>><a href="/order/incomeAccountingManage?verify_status=1">收入核算</a></dd>


        </dl>

        </li>
           <li style='display:none' <?php if($controller_name == 'dispatch' or $controller_name == 'despatch' or $controller_name == 'shortbarge'): ?> class="layui-nav-item layui-nav-itemed" <?php elseif($controller_name == 'transport'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-console"></i><em>调度管理</em></a>
        <dl class="layui-nav-child" >
            <dd <?php if(in_array(($function_name), explode(',',"showpickupordermanage,showpickuporderadd,showdespatchmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showPickupOrderManage">发运安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"shortbargemanage,shortbargelist"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/shortBargeManage?short_barge=1">短驳安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"abnormalmanner,addabnormal"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/abnormalManner">异常管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"accountingmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/accountingManage">成本核算</a></dd>

		   <!-- <dd <?php if(in_array(($function_name), explode(',',"showtransportmanage,uploadtransport"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showTransportManage">运单管理</a></dd>
 			<dd <?php if(in_array(($function_name), explode(',',"showdispatchmanage,uploaddispatch"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showDispatchManage">调度管理</a></dd> -->

        </dl>

        </li>
            <li <?php if($controller_name == 'shipment'): ?> class="layui-nav-item layui-nav-itemed" <?php elseif($controller_name == 'transport'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-console"></i><em>发运管理</em></a>
        <dl class="layui-nav-child" >
            <dd <?php if(in_array(($function_name), explode(',',"showlinemanage,addline,showlineovermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/showLineManage">发运安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showshortbargemanage,showshortbargeovermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/showShortBargeManage">短驳安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"abnormalmanner,addabnormal,abnormalinfomanner"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/abnormalManner">异常管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"getshipmentcostcheck"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/getShipmentCostCheck">成本审核</a></dd>

		   <!-- <dd <?php if(in_array(($function_name), explode(',',"showtransportmanage,uploadtransport"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showTransportManage">运单管理</a></dd>
 			<dd <?php if(in_array(($function_name), explode(',',"showdispatchmanage,uploaddispatch"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showDispatchManage">调度管理</a></dd> -->

        </dl>

        </li>
        <li <?php if($controller_name == 'source'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-website"></i><em>资源管理</em></a>
          <dl class="layui-nav-child">
    		<dd <?php if(in_array(($function_name), explode(',',"showcustomermanage,showcustomeradd"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerManage">客户</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showprojectmanage,showprojectadd,showacceptgoodsmanage,showacceptgoodsadd,showsendgoodsmanage,showsendgoodsadd,showgoodsmanage,showgoodsadd"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showProjectManage">项目</a></dd>

             <dd <?php if(in_array(($function_name), explode(',',"showsuppliermanage,showsupplieradd,showsupplieredit,showsupplierinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showSupplierManage">承运商</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showvehicletypemanage,showvehicletypemanageadd,showvehicletypeedit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showVehicleTypeManage">车辆类型</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showvehiclemanage,showvehicleadd,showvehicleedit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showVehicleManage">车辆</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showdrivermanage,showdriveradd,showdriveredit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showDriverManage">司机</a></dd>

              <dd <?php if(in_array(($function_name), explode(',',"showcustomersuppliermanage,showcustomersupplieradd,showcustomersupplieredit,showcustomersupplierinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerSupplierManage">客服用承运商</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showcustomersendgoodsmanage,showcustomersendgoodsadd,showcustomersendgoodsedit,showcustomersendgoodsinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerSendGoodsManage">客服用发货客户</a></dd>

          </dl>
          <li <?php if($controller_name == 'bill'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-file-b"></i><em>账单管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showcustomerbillmanage,showcustomerbilladd,showcustomerbillmissinvoicemanage,showcustomerbilldoneinvoicemanage,showcustomerbillsendinvoicemanage,showcustomerbillcloseinvoicemanage"))): ?> class="layui-this"<?php endif; ?>><a href="/bill/showCustomerBillManage">客户账单</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showsupplierbillmanage,showsupplierbilladd,showsupplierbillovermanage,supplierbillmissinvoice,supplierbilldoneinvoice,supplierbillfinancegetinvoice,supplierbilladdcostinfo,supplierbillaggrecostinfo,supplierbilldonepay"))): ?> class="layui-this"<?php endif; ?>><a href="/bill/showSupplierBillManage">承运商账单</a></dd>



          </dl>

          <li <?php if($controller_name == 'form'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-file-b"></i><em>报表管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showorderformmanage,showorderformadd"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showOrderFormManage">运单报表</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showshipmentlineoverformmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showShipmentLineOverFormManage">发运报表</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showshortbargeformmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showShortBargeFormManage">短驳报表</a></dd>



          </dl>

        </li>  <li <?php if($controller_name == 'device.smartbox'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>设备管理</em></a>
          <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"smartboxmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/baidumap/index">百度地图(demo)</a></dd>

             <dd <?php if(in_array(($function_name), explode(',',"smartboxmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/device.Smartbox/smartboxManage">智能周转箱</a></dd>





          </dl>
        </li>
	<!--
        <li <?php if($controller_name == 'customer'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-friends"></i><em>客户管理</em></a>
        <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"showcustomermanage,showcustomeradd"))): ?> class="layui-this"<?php endif; ?>><a href="/customer/showCustomerManage">客户</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showprojectmanage,showprojectadd,showacceptgoodsmanage,showacceptgoodsadd,showsendgoodsmanage,showsendgoodsadd,showgoodsmanage,showgoodsadd"))): ?> class="layui-this"<?php endif; ?>><a href="/customer/showProjectManage">项目</a></dd>


        </dl>

        </li>
 -->



		<!-- 系统管理 -->

          <li <?php if($controller_name == 'system'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-set-fill"></i><em>系统管理</em></a>
          <dl class="layui-nav-child">
                 <dd <?php if(in_array(($function_name), explode(',',"taxratemanage,taxrateadd"))): ?> class="layui-this"<?php endif; ?>><a href="/system/taxratemanage">费用管理</a></dd>
				 <dd><a href='/system/showAuthManage'>权限管理</a></dd>

          </dl>
          </li>



      </ul>
        </div>
    </div>

    <div class="layui-body layui-body-bg">

        <!-- 内容主体区域 -->
        <div class="content_body">
            <div class="body-top">
                <div class='layui-form-item'>
                    <span class="layui-breadcrumb" lay-separator="-">
                        <a>首页</a>
                        <a>账单管理</a>
                        <a><cite>客户账单</cite></a>
                    </span>
                </div>
                <div class='layui-block all-search-bg'>
                    <div class="layui-tab">
                        <ul class="layui-tab-title">
                            <li class="layui-this"><a href="/bill/showCustomerBillManage">未对账</a></li>
                            <li class=""><a href="/bill/showCustomerBillManage/status/2">已对账未审核</a></li>
                            <li class=""><a href="/bill/showCustomerBillMissInvoiceManage">已审核待开票</a></li>
                            <li class=""><a href="/bill/showCustomerBillDoneInvoiceManage">已开票/不开票</a></li>
                            <li class=""><a href="/bill/showCustomerBillSendInvoiceManage">已送出代收款</a></li>
                            <li class=""><a href="/bill/showCustomerBillCloseInvoiceManage">已收款</a></li>
                        </ul>
                    </div>

                    <form class="layui-form" method='get' action='/bill/showCustomerBillManage'>
                        <div class="layui-row">
                            <div class="layui-input-inline">
                                <label class="layui-form-label">运单编号:</label>
                                <div class="layui-input-block">
                                    <input type="text" name="orders_number" autocomplete="off" value="<?php echo \think\Request::instance()->get('orders_number'); ?>"  class="layui-input">
                                </div>
                            </div>
                            <div class="layui-input-inline">
                                <label class="layui-form-label">客户编号:</label>
                                <div class="layui-input-block">
                                    <input type="text" name="customer_order_number" autocomplete="off" value="<?php echo \think\Request::instance()->get('customer_order_number'); ?>"  class="layui-input">
                                </div>
                            </div>
                            <div class="layui-input-inline">
                                <label class="layui-form-label">项目名称:</label>
                                <div class="layui-input-block" style="width:159px">
                                    <select name="multi_project_id" lay-verify="required" id='multi_project_id'
                                            lay-verify="required" xm-select="select1" xm-select-direction="down"
                                            xm-select-search="/order/getProjectAjax" xm-select-search-type="dl">
                                        <?php if(is_array($projectResult) || $projectResult instanceof \think\Collection || $projectResult instanceof \think\Paginator): if( count($projectResult)==0 ) : echo "" ;else: foreach($projectResult as $key=>$vo): ?>
                                        <option value="<?php echo $vo['project_id']; ?>" selected><?php echo $vo['project_name']; ?></option>
                                        <?php endforeach; endif; else: echo "" ;endif; ?>

                                    </select>
                                </div>
                            </div>
                            <div class="layui-input-inline">
                                <label class="layui-form-label">发货方:</label>
                                <div class="layui-input-block">
                                    <input type="text" name="send_goods_name" autocomplete="off" value="<?php echo \think\Request::instance()->get('send_goods_name'); ?>"  class="layui-input">
                                </div>
                            </div>
                            <div class="layui-input-inline">
                                <label class="layui-form-label">收货方:</label>
                                <div class="layui-input-block">
                                    <input type="text" name="accept_goods_name" autocomplete="off" value="<?php echo \think\Request::instance()->get('accept_goods_name'); ?>"  class="layui-input">
                                </div>
                            </div>
                            <div class="layui-input-inline">
                                <label class="layui-form-label">收货人:</label>
                                <div class="layui-input-block">
                                    <input type="text" name="accept_name" autocomplete="off" value="<?php echo \think\Request::instance()->get('accept_name'); ?>"  class="layui-input">
                                </div>
                            </div>
                            <div class="layui-input-inline">
                                <label class="layui-form-label">收货电话:</label>
                                <div class="layui-input-block">
                                    <input type="text" name="accept_cellphone" autocomplete="off" value="<?php echo \think\Request::instance()->get('accept_cellphone'); ?>"  class="layui-input">
                                </div>
                            </div>
                            <div class="layui-input-inline">
                                <label class="layui-form-label">收货地址:</label>
                                <div class="layui-input-block">
                                    <input type="text" name="accept_address" autocomplete="off" value="<?php echo \think\Request::instance()->get('accept_address'); ?>"  class="layui-input">
                                </div>
                            </div>
                            <div class="layui-input-inline">
                                <label class="layui-form-label">运单时间:</label>
                                <div class="layui-input-block">
                                    <input type="text" id="start_order_time" autocomplete="off" value="<?php echo \think\Request::instance()->get('start_order_time'); ?>" name="start_order_time" style="width:100px;display:inline-block" value=""  class="layui-input">
                                    <input type="text" id="end_order_time" autocomplete="off" value="<?php echo \think\Request::instance()->get('start_order_time'); ?>"  name="end_order_time" style="width:100px;display:inline-block" value=""  class="layui-input">
                                </div>
                            </div>
                            <div class="layui-input-inline">
                                <label class="layui-form-label">支付方式:</label>
                                <div class="layui-input-block">
                                    <select id='pay_type' name='pay_type' >
                                        <option value=''>--全部--</option>
                                        <option value='1' <?php if(\think\Request::instance()->get('pay_type') == 1): ?>selected <?php endif; ?>>现付</option>
                                        <option value='2' <?php if(\think\Request::instance()->get('pay_type') == 2): ?>selected <?php endif; ?>>到付</option>
                                        <option value='3' <?php if(\think\Request::instance()->get('pay_type') == 3): ?>selected <?php endif; ?>>月结</option>
                                    </select>
                                </div>
                            </div>
                                <div class="layui-input-inline" >
                                    <label class="layui-form-label">分公司:</label>
                                    <div class="layui-input-block" style="width: 158px;">
                                         <select id='company_id' name='choose_company_id' >
                                            <option value=''>--全部--</option>
											<?php if(is_array($comapnyResult) || $comapnyResult instanceof \think\Collection || $comapnyResult instanceof \think\Paginator): if( count($comapnyResult)==0 ) : echo "" ;else: foreach($comapnyResult as $key=>$v): ?>
												<option value="<?php echo $v['company_id']; ?>" 
												
												<?php if(\think\Request::instance()->get('choose_company_id') != ''): if(\think\Request::instance()->get('choose_company_id') == $v['company_id']): ?>selected <?php endif; else: if(\think\Session::get('user.company_id') == $v['company_id']): ?>selected <?php endif; endif; ?>
												>

												<?php echo $v['company_name']; ?></option>
											<?php endforeach; endif; else: echo "" ;endif; ?>                                           
                                        </select>
                                    </div>
                                </div>							
                        </div>
                        <div class="layui-row">
                            <div class="layui-col-md4 layui-col-md-offset4">
                                <div class='input-inline all-button-center pages-search-margin'>
                                    <button class="layui-btn nav-search" >搜索</button>
                                    <button type="reset" class="layui-btn layui-btn-primary swiper-right-form-reset" >重置</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <div class="content-bg">
                <div class="content-bg" >
                    <table class="layui-hide" id="test" lay-filter="test" ></table>
                    <script type="text/html" id="toolbarDemo">
                        <div class="layui-inline" lay-event="add" style="width:auto"> 添加账单 </div>
                    </script>
                </div>
            </div>

        </div>
    </div>


</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
<script src='/static/javascript/system/source.js'></script>
<script>
    layui.use(['layer','table','form','formSelects'], function() {
        var table = layui.table //表格
            , layer = layui.layer //弹层
            , form = layui.form
            , formSelects = layui.formSelects

        //加载日期控件
        layui.laydate.render({
            elem: '#start_order_time' ,//input的id

        });
        layui.laydate.render({
            elem: '#end_order_time' ,//input的id

        });

        var isShow = true;  //定义⼀个标志位
        $('.kit-side-fold').click(function () {
            //选择出所有的span，并判断是不是hidden
            $('.layui-nav-item span').each(function () {
                if ($(this).is(':hidden')) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
            //判断isshow的状态
            if (isShow) {
                $('.layui-side.layui-bg-black').width(48); //设置宽度
                $('.kit-side-fold i').css('margin-right', '70%');  //修改图标的位置
                //将footer和body的宽度修改
                $('.layui-body').css('left', 60 + 'px');
                $('.layui-footer').css('left', 60 + 'px');
                //将⼆级导航栏隐藏
                $('dd span').each(function () {
                    $(this).hide();
                });
                //修改标志位
                isShow = false;
            } else {
                $('.layui-side.layui-bg-black').width(200);
                $('.kit-side-fold i').css('margin-right', '10%');
                $('.layui-body').css('left', 200 + 'px');
                $('.layui-footer').css('left', 200 + 'px');
                $('dd span').each(function () {
                    $(this).show();
                });
                isShow = true;
            }
        });

        var s_value = formSelects.value('select1', 'valStr');

        table.render({
            elem: '#test'
            ,url:'/bill/getCustomerBillData?orders_number=<?php echo \think\Request::instance()->get('orders_number'); ?>&customer_order_number=<?php echo \think\Request::instance()->get('customer_order_number'); ?>&send_goods_name=<?php echo \think\Request::instance()->get('send_goods_name'); ?>&accept_goods_name=<?php echo \think\Request::instance()->get('accept_goods_name'); ?>&accept_name=<?php echo \think\Request::instance()->get('accept_name'); ?>&accept_cellphone=<?php echo \think\Request::instance()->get('accept_cellphone'); ?>&accept_address=<?php echo \think\Request::instance()->get('accept_address'); ?>&start_order_time=<?php echo \think\Request::instance()->get('start_order_time'); ?>&end_order_time=<?php echo \think\Request::instance()->get('end_order_time'); ?>&verify_status=2&choose_company_id=<?php echo \think\Request::instance()->get('choose_company_id'); ?>&pay_type=<?php echo \think\Request::instance()->get('pay_type'); ?>&multi_project_id='+s_value
            ,toolbar: '#toolbarDemo' //开启头部工具栏，并为其绑定左侧模板
            ,defaultToolbar: ['filter', 'exports', 'print']
            ,title: '用户数据表'
			,height:tableHeight
            ,totalRow: true
            ,cols: [[
                {type:'checkbox',fixed:'left',name:'layTableCheckbox'},
                {field: '', title: '序号', width: 80, type: 'numbers', totalRowText: '合计'},
                {field:'orders_number', title:'运单编号', width:130},
                {
                    field: 'pickup_time', title: '运单日期', width: 130, templet: function (d) {
                        return layui.util.toDateString(d.pickup_time * 1000, 'yyyy-MM-dd');
                    }
                },
                {field:'invoice_status', title:'财务状态', width:90 ,templet: function(d){

                            return '已核算';
                    
                    }
                },
                {field:'pay_type', title:'支付方式', width:90 ,templet: function(d){
                        if(d.pay_type==1){
                            return '现付';
                        }else if(d.pay_type==2){
                            return '到付';
                        }else if(d.pay_type==3){
                            return '月结';
                        }
                    }
                },
                {field:'project_name', title:'项目名称', width:130},
                {field:'send_goods_company', title:'发货方', width:130},
                {field:'send_city_name', title:'发站', width:90},
                {field:'accept_city_name', title:'到站', width:90},
                {field:'accept_goods_company', title:'收货方', width:90},
                {field:'goods_name', title:'货物名称', width:120},
                {field:'realy_count', title:'件数', width:90},
                {field:'realy_pack_count', title:'数量', width:90},
                {field:'realy_weight', title:'重量', width:90},
                {field:'realy_volume', title:'体积', width:90},
                {field:'delivery_method', title:'送货方式', width:90, templet: function(d){
                        if(d.delivery_method==1){
                            return '送货';
                        }else if(d.delivery_method=2){
                            return '自提';
                        }
                    }
                },
                {field:'abnormal_status', title:'异常', width:90, templet: function(d){
                        if(d.abnormal_status==0){
                            return '否';
                        }else if(d.abnormal_status=1){
                            return '是';
                        }
                    }
                },
                {field:'money2', title:'总费用', width:120, totalRow: true, sort: true},
                {field:'abnormal_money', title:'赔款', width:120, totalRow: true, sort: true},
                {field:'finance_id',title:'序号', width:30, hide:true}
            ]]
			,limits:[100,200,300,400,500]
		    ,limit:100
            ,page: true
            ,response: {
                statusCode: 200 //重新规定成功的状态码为 200，table 组件默认为 0
            }
 
        });

        //头工具栏事件
        table.on('toolbar(test)', function(obj){
            var checkStatus = table.checkStatus(obj.config.id);
            switch(obj.event){
                case 'add':
                    var data = checkStatus.data;
                    var info_array = [];
                    var info_array2= [];
                    var id_str = "";

                    for(var j =0;j<data.length;j++){
                        var id = data[j]['finance_id'];
                        var project_name = data[j]['project_name'];
                        info_array.push({id: id,project_name:project_name });
                        // info_array2.push({finance_id: id});
                        id_str += id+",";
                    }
					
                    var modifiedString = id_str.replace(/,\s*$/, '');
                    // console.log(modifiedString);return false;
                    for (var i=0;i<info_array.length;i++)
                    {
                        if(info_array[0]['project_name'] == info_array[i]['project_name']){
                            //匹配
                            layer.msg("项目匹配!");
                            var status = 1;
                        }else{
                            //不匹配
                            layer.msg("项目不匹配!");
                            var status = 2;
							return false;
                        }
                    }

                    if(status==1){
                        //跳转对账单页面
                        window.location.href='/bill/showCustomerBillInfoManage?data='+JSON.stringify(modifiedString);
                    }else if(status==2){

                    }
                    info_array = [];
                    info_array2 = [];
                    break;
            };
        });

        //监听行工具事件
        table.on('tool(test)', function(obj){
            var data = obj.data;
            //console.log(obj)
            if(obj.event === 'del'){
                layer.confirm('真的删除行么', function(index){
                    obj.del();
                    layer.close(index);
                });
            } else if(obj.event === 'edit'){
                layer.prompt({
                    formType: 2
                    ,value: data.email
                }, function(value, index){
                    obj.update({
                        email: value
                    });
                    layer.close(index);
                });
            }
        });

    });

</script>
</body>
</html>
