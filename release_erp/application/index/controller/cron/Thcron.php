<?php

namespace app\index\controller\cron;

use app\index\controller\Base;
use app\index\controller\Email;

class Thcron extends Base
{

    /**
     *  圣华
     *  每次计算当前时间前一天的所有温湿度度数据
     *  定时任务-发送温湿度邮件
     *  Kison
     */
    public function SendTHEmail(){
	
		//获取定时任务信息
        $cron_data = [
            "method"=>"/cron.thcron/SendTHEmail",
            "channel"=>"WMS",
            "status"=>1
        ];
        $return_cron = $this->callSoaErp('post','/Cron/getCron',$cron_data);

        if(!empty($return_cron['data'])){
            if($return_cron['data']['type']==1){
                //获取发送人信息
                $senduser_data['status'] = 1;
                $return_senduser = $this->callSoaErp('post','/Senduser/getSendUser',$senduser_data);

                //调用发送邮件接口
                $email = new Email();
                $email->sendMail($return_senduser['data']);

            }else{
                echo "定时任务未开启";
            }
        }else{
            echo "定时任务不存在!";
        }
    }
}