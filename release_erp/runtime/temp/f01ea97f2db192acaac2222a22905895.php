<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:71:"/var/www/html/test_erp/public/../application/index/view/login/show.html";i:1655359244;}*/ ?>
﻿<!doctype html>

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>登录</title>
	<link rel="stylesheet" type="text/css" href="/static/login/css/styles.css">
	<link rel="stylesheet" href="/static/layui/css/layui.css">
</head>
<body>
<div class="htmleaf-container">
	<div class="login-title"><img src="/static/image/login/login-logo.png" width="40px" height="auto" /></div>
	<div class="wrapper">
		<div class="login-box">
			<div class="login-bg layui-anim layui-anim-up"></div>
			<div class="container">
				<h3>用户登录</h3>
				<form class="form" onsubmit="return false;">
					<div class="login-pos">
						<input type="text" required="required" placeholder="用户名称" id='username' />
						<i class="layui-icon layui-icon-username"></i>
					</div>
					<div class="login-pos">
						<input type="password" required="required" placeholder="密码" id='password' />
						<i class="layui-icon layui-icon-password"></i>
					</div>
					<div class="login-pos" id='yzm_show' style='display:none'>
						<input type="text" required="required" id='yzm' style='width:100px;margin:0px;background-color:white;display:inline' />
					<img src="/publicshow/showYzm" alt="" width="175" height="48" id='yzm_img' class="passcode" onclick="javascript:this.src='/publicshow/showYzm?tm='+Math.random()" />
					</div>
					<div class="login-button">
						<!-- <span class="login-forget">忘记密码</span>  -->
						<input type='submit' value='登录' id='login_check'>
					</div>
				</form>
			</div>
		</div>
	</div> 
	<div class="login-footer">Copyright © 2021 圣华物流版权所有</div>
</div>

<script src="/static/layui/layui.all.js"></script>
<script src="/static/login/js/jquery-2.1.1.min.js" type="text/javascript"></script>
<script src="/static/login/js/login.js"></script>
</body>
</html>