<?php

namespace app\index\model\examination;

use think\Db;
use think\Model;

class Examination extends Model
{
    protected $table = 'examination';

    /**
     * 获取题库数据
     * 韩
     */
    public function getExamination($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
        $data = "1=1 ";

        if(isset($params['examination_id'])){ //分类ID
            $data.= " and examination.examination_id = ".$params['examination_id'];
        }

        $result = $this->table("examination")->where($data)->order('examination.create_time desc')->field("*")->find();
        if($is_count==true){
            $result = $this->table("examination")->where($data)->count();
        }else{
            if($is_page == true){
                $result = $this->table("examination")->
                where($data)->limit($page, $page_size)->order('examination.create_time desc')->
                field(['examination_id','title','timeout','remark','binary','single','multiple','fill','brief','fraction',
                    "(select nickname from user where user.user_id = examination.create_user_id)"=> 'create_user_name',
                    "(select nickname from user where user.user_id = examination.update_user_id)"=> 'update_user_name',
                    'status',
                    'from_unixtime(create_time)'=> 'create_time',
                    'from_unixtime(update_time)'=> 'update_time'
                ])->select();
            }else{
                $result = $this->table("examination")->
                where($data)->order('examination.create_time desc')->
                field(['examination_id','title','timeout','remark','binary','single','multiple','fill','brief','fraction',
                    "(select nickname from user where user.user_id = examination.create_user_id)"=> 'create_user_name',
                    "(select nickname from user where user.user_id = examination.update_user_id)"=> 'update_user_name',
                    'status',
                    'from_unixtime(create_time)'=> 'create_time',
                    'from_unixtime(update_time)'=> 'update_time'
                ])->select();
            }
        }
        return $result;
    }

    /**
     * 获取题库和题目数据
     * 韩
     */
    public function getExaminationData($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
        $data = "1=1 ";

        if(isset($params['examination_id'])){ //分类ID
            $data.= " and examination.examination_id = ".$params['examination_id'];
        }

        $result = $this->table("examination")->where($data)->order('examination.create_time desc')->field("*")->find();
        if(count($result)>0){
            $result_data = $this->table("subject")->where("examination_id={$result['examination_id']}")->field("*")->select();

            $returnArr = [];

            foreach($result_data as $k1=>$v1) { //for1
                $returnArr['examination_id'] = $result['examination_id'];
                $returnArr['title'] = $result['title'];
                $returnArr['timeout'] = $result['timeout'];
                $returnArr['remark'] = $result['remark'];
                $returnArr['binary'] = $result['binary'];
                $returnArr['single'] = $result['single'];
                $returnArr['multiple'] = $result['multiple'];
                $returnArr['fill'] = $result['fill'];
                $returnArr['brief'] = $result['brief'];
                $returnArr['fraction'] = $result['fraction'];

                if($v1['subject_type']==1){
                    $result_subject_data = $this->table("subject")->where("subject_type={$v1['subject_type']} and examination_id={$result['examination_id']}")->field("*")->select();

                    foreach ($result_subject_data as $kk=>$vv){
                        $returnArr['data']['binary']['name'] = '判断题';
                        $returnArr['data']['binary']['score'] = $result['binary'];
                        $returnArr['data']['binary']['data'][$kk]['question'] = $vv['question'];
                        $returnArr['data']['binary']['data'][$kk]['answer'] = $vv['answer'];
                    }
                }else if($v1['subject_type']==2){
                    $result_subject_data = $this->table("subject")->where("subject_type={$v1['subject_type']} and examination_id={$result['examination_id']}")->field("*")->select();

                    foreach ($result_subject_data as $kk=>$vv){
                        $returnArr['data']['single']['name'] = '单选题';
                        $returnArr['data']['single']['score'] = $result['single'];
                        $returnArr['data']['single']['data'][$kk]['question'] = $vv['question'];
                        $returnArr['data']['single']['data'][$kk]['option'] = $vv['option'];
                        $returnArr['data']['single']['data'][$kk]['answer'] = $vv['answer'];
                    }

                }else if($v1['subject_type']==3){
                    $result_subject_data = $this->table("subject")->where("subject_type={$v1['subject_type']} and examination_id={$result['examination_id']}")->field("*")->select();

                    foreach ($result_subject_data as $kk=>$vv){
                        $asd = explode("",$vv['option']);
                        $returnArr['data']['multiple']['name'] = '多选题';
                        $returnArr['data']['multiple']['score'] = $result['multiple'];
                        $returnArr['data']['multiple']['data'][$kk]['question'] = $vv['question'];
                        $returnArr['data']['multiple']['data'][$kk]['option'] = $vv['option'];
                        $returnArr['data']['multiple']['data'][$kk]['answer'] = $vv['answer'];
                    }

                }else if($v1['subject_type']==4){
                    $result_subject_data = $this->table("subject")->where("subject_type={$v1['subject_type']} and examination_id={$result['examination_id']}")->field("*")->select();

                    foreach ($result_subject_data as $kk=>$vv){
                        $returnArr['data']['fill']['name'] = '填空题';
                        $returnArr['data']['fill']['score'] = $result['fill'];
                        $returnArr['data']['fill']['data'][$kk]['question'] = $vv['question'];
                        $returnArr['data']['fill']['data'][$kk]['answer'] = $vv['answer'];
                    }

                }else if($v1['subject_type']==5){
                    $result_subject_data = $this->table("subject")->where("subject_type={$v1['subject_type']} and examination_id={$result['examination_id']}")->field("*")->select();

                    foreach ($result_subject_data as $kk=>$vv){
                        $returnArr['data']['brief']['name'] = '简答题';
                        $returnArr['data']['brief']['score'] = $result['brief'];
                        $returnArr['data']['brief']['data'][$kk]['question'] = $vv['question'];
                        $returnArr['data']['brief']['data'][$kk]['answer'] = $vv['answer'];
                    }

                }
            }
        }

        return $returnArr;

        $count = [];
        $score = [];
        foreach($returnArr['data'] as $k=>$v){
            $count[$k] = count($v['data']);
            $score[$k] = round($v['score']/$count[$k]);
        }

        $sum = 0;
        //用户答案
        $user = [
            'binary' => [
                1 => "yes",
                2 => "no"
            ],
            'single' => [
                1 => "D",
                2 => "D"
            ],
            'multiple' => [
                1 => "[A,B,C]",
                2 => "[A,B,C]"
            ],
            'fill' => [
                1 => "上海",
                2 => "蛤蟆"
            ],
            'brief' => [
                1 => "xxx",
                2 => "xxx"
            ]
        ];
        $dui = 0;
        $cuo = 0;
        foreach($returnArr['data'] as $type=>$each){
            foreach($each['data'] as $kk=>$vv){
                //获取用户提交答案
                $answer = $user[$type][$kk+1];
                //判断答案是否正确
                if($vv['answer']==$answer){
                    $total[$type][$kk] = true;
                    $sum += $score[$type];
                    $dui += count($total[$type][$kk]);
                }else{
                    $total[$type][$kk] = false;
                    $cuo += count($total[$type][$kk]);
                }
            }
        }

        echo "总分：100分, 得分：".$sum."分";
        echo "<br/>";
        echo "共 ".($dui+$cuo)." 题 "."&nbsp;&nbsp;答对：".$dui."题 &nbsp;&nbsp; 答错：".$cuo."题";
        echo "<br/>";
        echo "正确率：".number_format($dui/($dui+$cuo)*100,2)."%";
        echo "<br/>";
        if($sum>=$result['fraction']){
            echo "<span style='color:green'>本次考试通过！</span>";
        }else{
            echo "<span style='color:red'>本次考试未通过，还需努力！</span>";
        }
        echo "<br/>";
        return "总分：100分, 得分：".$sum."分";
    }

    /**
     * 添加题库和题目历史数据
     * 韩
     */
    public function addHistoryExaminationData($params){
        $t = time();
//        echo "<pre>";
//        var_dump($params['data']);exit;
//        echo "</pre>";

        foreach($params['data'] as $key=>$val){

        }

        //插入题库历史表
        $data['examination_id'] = $params['examination_id'];
        $data['title'] = $params['title'];
        $data['timeout'] = $params['timeout'];
        $data['remark'] = $params['remark'];
        $data['binary'] = $params['binary'];
        $data['single'] = $params['single'];
        $data['multiple'] = $params['multiple'];
        $data['fill'] = $params['fill'];
        $data['brief'] = $params['brief'];
        $data['fraction'] = $params['fraction'];
        $data['score'] = $params['score'];
        $data['create_time'] = $t;
        $data['create_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        $data['update_user_id'] = $params['user_id'];
        $data['status'] = $params['status'];

        Db::startTrans();
        try{
            $this->table("examination_log")->insertGetId($data);

            if($data['data']){

            }

            $result = 1;
            // 提交事务
            Db::commit();
        }catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return $result;
    }
}