<?php
namespace app\index\controller;
use app\common\help\Help;
use think\Controller;
use think\Session;

use think\config;
class BookingLogin extends Controller
{

    protected $_soaerpConfig;

    /**
     * 
     * 登陆显示
     * 
     */
    public function index(){

		return $this->fetch('booking_login/index');
    }
    
    /**
     * 验证登陆
     * 胡
     * 
     */
    public function checkLogin(){
//		var_dump($_POST);exit;
    	$username  = input('username');
    	$password = input('password');
    	$data = [
    		'username'=>$username,
    		'password'=>$password,
    	];
    	$result = Help::callSoaErp('post','/user/bookingLogin',$data);


    	if($result['code']==200){
    		Session::set('booking_user',$result['data'][0]);
			echo "<script>location.href='/b2b_news'</script>";
    	}
		return $this->fetch('booking_login/index2');

    	
    }

	/**
	 * 退出登陆
	 */
    public function loginOut(){
        Session::delete('booking_user');
    	echo "<script>location.href='/booking_login/index'</script>";
    }

}
