<?php if (!defined('THINK_PATH')) exit(); /*a:5:{s:89:"/var/www/html/test_erp/public/../application/index/view/form/shipment_live_over_form.html";i:1660560797;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:64:"/var/www/html/test_erp/application/index/view/public/header.html";i:1658978091;s:67:"/var/www/html/test_erp/application/index/view/public/left_menu.html";i:1665286673;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






    <title>已发运报表</title>
    <style>
        /*  .layui-table-cell { height: auto; white-space: normal; }*/
        table a{
            text-decoration: underline}
    </style>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
            <div class="layui-logo"></div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left">
        <li class="layui-nav-item">
            <a href="/turnoverbox/index">智能周转箱</a>
        </li>
        <li class="layui-nav-item">
            <a href="/">TMS</a>
        </li>
        <li class="layui-nav-item">
 
            <a href="/warehouse/index">WMS</a>
        </li>
        <li class="layui-nav-item">
            <a href="/wisdompark/index">智慧园区</a>

        </li>
        <li class="layui-nav-item">
            <a href="/Oa/index">OA</a>

        </li>   
         <li class="layui-nav-item">
            <a href="/Bms/index">BMS</a>

        </li>
        <li class="layui-nav-item">
            <a href="/equipment/equipmentManage">设备</a>

        </li>
    </ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item tips-system-message" style="cursor: pointer"><?php echo $language_tag['index_nav_system_message']; ?> <span class="system-message-quantity" style="color: red"> 1</span> </li>
      <li class="layui-nav-item">
        <a href="javascript:;">
          <!-- <img src="http://t.cn/RCzsdCq" class="layui-nav-img"> -->
          <?php echo \think\Session::get('user.nickname'); ?>
        </a>
        <dl class="layui-nav-child head-top">
          <dd><a href="/system/setUserInfo/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_basicDocument']; ?></a></dd>
          <dd><a href="/system/showChangePassword/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_resetPassword']; ?></a></dd>
        </dl>
      </li>
      <li class="layui-nav-item"><a href="/login/loginOut">退出登录</a></li>
    </ul>

    <div id="tips-system-message-js" style='display:none;'>
        <div  style="max-height:350px;width: 450px">
             <div style="color: #000;text-align: center;height: 10px;padding: 10px">你有 <font class="f-tips-system-message"></font> 条未读消息</div>
             <hr>
             <div class="tips-system-message-div" style="height:220px;overflow-y: auto">
           
                 <li style="color: #000; padding: 10px;cursor: pointer" data-href="" data-id="" class="aUrl" onclick="Aurl(this)" ></li>
                 <hr>
             
             </div>
            <div style="color: #000;text-align: center;height: 40px;padding-top: 15px"><a href="/reminderManagement/allInStationLetter"><?php echo $language_tag['index_nav_viewAll_messa']; ?></a></div>
        </div>
    </div>

    <!--  下列保存用户SESSION信息 -->
	
    <input type="hidden" id='now_url'  value="<?php echo $now_url; ?>" />
	<input type="hidden" id='after_url'  value="<?php echo $after_url; ?>" />
    <input type="hidden" id='user_company_id' value="<?php echo \think\Session::get('user.company_id'); ?>" />
	<!--  下列保存其他信息 -->
	<input type="hidden" id='http_referer' value="<?php echo $http_referer; ?>" />
	
	<script type='text/javascript'>
		$('.tips-system-message').on('click',function(){
			var html = $('#tips-system-message-js').html();
		    layer.tips(html, '.tips-system-message', {
		        tips: [3, '#fff'],
		        padding:'20',
		        tipsMore: false,
		        area: ['450px', 'auto'],
		        shade: [0.01, '#fff'],
		        shadeClose:true,
		        time:0
			
		    });
		
		});
		
		$(document).ready(function(){ 
			$(document).mousemove(function(e){ 
				
				if(e.pageX<=10){
					$('.layui-bg-black').show()
				}
				
				if(e.pageX>200){
					$('.layui-bg-black').hide()
				
				}
				if(e.pageY<=10){
					$('.layui-header').show()
				}
				
				if(e.pageY>40){
					$('.layui-header').hide()
				
				}			
			}); 
			
		}); 
	

	</script>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <div title="菜单收缩" class="kit-side-fold"><i class="layui-icon layui-icon-spread-left" aria-hidden="true"></i></div>
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
                  <ul class="layui-nav layui-nav-tree" id="left-nav" lay-filter="test">
          <li class="layui-nav-item"><a href="/"><i class="layui-icon layui-icon-chart-screen"></i>   <em>控制面板</em></a></li>

          <li <?php if($controller_name == 'order'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-form"></i><em>运单管理</em></a>
        <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"showordermanage,showorderadd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderManage?multi_order_status=1">运单管理</a></dd>
			<!--<dd <?php if(in_array(($function_name), explode(',',"showordertrackmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderTrackManage">运单跟踪</a></dd>-->
			 <?php if(\think\Session::get('user.role_id') == 1 || \think\Session::get('user.role_id') == 15): ?>  <dd <?php if(in_array(($function_name), explode(',',"showordertrackmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderTrackManage?sign=0&multi_order_status=2,3,4,5">运单跟踪</a></dd><?php endif; ?> 
            <dd <?php if(in_array(($function_name), explode(',',"showorderreceiptmanage,showorderreceiptadd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderReceiptManage?receipt_status=0">回单管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showorderabnormalmanage,showorderabnormaladd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderAbnormalManage?handle=1">异常运单</a></dd>
  			
  			<dd <?php if(in_array(($function_name), explode(',',"incomeaccountingmanage,addorderincome"))): ?> class="layui-this"<?php endif; ?>><a href="/order/incomeAccountingManage?verify_status=1">收入核算</a></dd>


        </dl>

        </li>
           <li style='display:none' <?php if($controller_name == 'dispatch' or $controller_name == 'despatch' or $controller_name == 'shortbarge'): ?> class="layui-nav-item layui-nav-itemed" <?php elseif($controller_name == 'transport'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-console"></i><em>调度管理</em></a>
        <dl class="layui-nav-child" >
            <dd <?php if(in_array(($function_name), explode(',',"showpickupordermanage,showpickuporderadd,showdespatchmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showPickupOrderManage">发运安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"shortbargemanage,shortbargelist"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/shortBargeManage?short_barge=1">短驳安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"abnormalmanner,addabnormal"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/abnormalManner">异常管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"accountingmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/accountingManage">成本核算</a></dd>

		   <!-- <dd <?php if(in_array(($function_name), explode(',',"showtransportmanage,uploadtransport"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showTransportManage">运单管理</a></dd>
 			<dd <?php if(in_array(($function_name), explode(',',"showdispatchmanage,uploaddispatch"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showDispatchManage">调度管理</a></dd> -->

        </dl>

        </li>
            <li <?php if($controller_name == 'shipment'): ?> class="layui-nav-item layui-nav-itemed" <?php elseif($controller_name == 'transport'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-console"></i><em>发运管理</em></a>
        <dl class="layui-nav-child" >
            <dd <?php if(in_array(($function_name), explode(',',"showlinemanage,addline,showlineovermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/showLineManage">发运安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showshortbargemanage,showshortbargeovermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/showShortBargeManage">短驳安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"abnormalmanner,addabnormal,abnormalinfomanner"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/abnormalManner">异常管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"getshipmentcostcheck"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/getShipmentCostCheck">成本审核</a></dd>

		   <!-- <dd <?php if(in_array(($function_name), explode(',',"showtransportmanage,uploadtransport"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showTransportManage">运单管理</a></dd>
 			<dd <?php if(in_array(($function_name), explode(',',"showdispatchmanage,uploaddispatch"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showDispatchManage">调度管理</a></dd> -->

        </dl>

        </li>
        <li <?php if($controller_name == 'source'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-website"></i><em>资源管理</em></a>
          <dl class="layui-nav-child">
    		<dd <?php if(in_array(($function_name), explode(',',"showcustomermanage,showcustomeradd"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerManage">客户</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showprojectmanage,showprojectadd,showacceptgoodsmanage,showacceptgoodsadd,showsendgoodsmanage,showsendgoodsadd,showgoodsmanage,showgoodsadd"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showProjectManage">项目</a></dd>

             <dd <?php if(in_array(($function_name), explode(',',"showsuppliermanage,showsupplieradd,showsupplieredit,showsupplierinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showSupplierManage">承运商</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showvehicletypemanage,showvehicletypemanageadd,showvehicletypeedit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showVehicleTypeManage">车辆类型</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showvehiclemanage,showvehicleadd,showvehicleedit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showVehicleManage">车辆</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showdrivermanage,showdriveradd,showdriveredit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showDriverManage">司机</a></dd>

              <dd <?php if(in_array(($function_name), explode(',',"showcustomersuppliermanage,showcustomersupplieradd,showcustomersupplieredit,showcustomersupplierinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerSupplierManage">客服用承运商</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showcustomersendgoodsmanage,showcustomersendgoodsadd,showcustomersendgoodsedit,showcustomersendgoodsinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerSendGoodsManage">客服用发货客户</a></dd>

          </dl>
          <li <?php if($controller_name == 'bill'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-file-b"></i><em>账单管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showcustomerbillmanage,showcustomerbilladd,showcustomerbillmissinvoicemanage,showcustomerbilldoneinvoicemanage,showcustomerbillsendinvoicemanage,showcustomerbillcloseinvoicemanage"))): ?> class="layui-this"<?php endif; ?>><a href="/bill/showCustomerBillManage">客户账单</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showsupplierbillmanage,showsupplierbilladd,showsupplierbillovermanage,supplierbillmissinvoice,supplierbilldoneinvoice,supplierbillfinancegetinvoice,supplierbilladdcostinfo,supplierbillaggrecostinfo,supplierbilldonepay"))): ?> class="layui-this"<?php endif; ?>><a href="/bill/showSupplierBillManage">承运商账单</a></dd>



          </dl>

          <li <?php if($controller_name == 'form'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-file-b"></i><em>报表管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showorderformmanage,showorderformadd"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showOrderFormManage">运单报表</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showshipmentlineoverformmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showShipmentLineOverFormManage">发运报表</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showshortbargeformmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showShortBargeFormManage">短驳报表</a></dd>



          </dl>

        </li>  <li <?php if($controller_name == 'device.smartbox'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>设备管理</em></a>
          <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"smartboxmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/baidumap/index">百度地图(demo)</a></dd>

             <dd <?php if(in_array(($function_name), explode(',',"smartboxmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/device.Smartbox/smartboxManage">智能周转箱</a></dd>





          </dl>
        </li>
	<!--
        <li <?php if($controller_name == 'customer'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-friends"></i><em>客户管理</em></a>
        <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"showcustomermanage,showcustomeradd"))): ?> class="layui-this"<?php endif; ?>><a href="/customer/showCustomerManage">客户</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showprojectmanage,showprojectadd,showacceptgoodsmanage,showacceptgoodsadd,showsendgoodsmanage,showsendgoodsadd,showgoodsmanage,showgoodsadd"))): ?> class="layui-this"<?php endif; ?>><a href="/customer/showProjectManage">项目</a></dd>


        </dl>

        </li>
 -->



		<!-- 系统管理 -->

          <li <?php if($controller_name == 'system'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-set-fill"></i><em>系统管理</em></a>
          <dl class="layui-nav-child">
                 <dd <?php if(in_array(($function_name), explode(',',"taxratemanage,taxrateadd"))): ?> class="layui-this"<?php endif; ?>><a href="/system/taxratemanage">费用管理</a></dd>
				 <dd><a href='/system/showAuthManage'>权限管理</a></dd>

          </dl>
          </li>



      </ul>
        </div>
    </div>

    <div class="layui-body layui-body-bg">

        <!-- 内容主体区域 -->
        <div class="content_body">
            <div class="body-top">
                <div class='layui-form-item'>
                    <span class="layui-breadcrumb" lay-separator="-">
                        <a>首页</a>
                        <a>报表管理</a>
                        <a><cite>发运报表</cite></a>
                    </span>
                </div>
                <div class='layui-block all-search-bg'>
                    <div class="layui-tab">
                        <ul class="layui-tab-title">
<!--                            <li class=""><a href='/shipment/showLineManage'>未发运</a></li>-->
                            <li class="layui-this"><a>已发运</a></li>


                        </ul>

                    </div>


                    <form class="layui-form" method='get' action='/shipment/showLineOverManage'>
                        <div class="layui-row">

                            <div class="layui-input-inline">
                                <label class="layui-form-label">项目名称:</label>
                                <div class="layui-input-inline">
                                    <select id='project_id' name='project_id'  lay-search>
                                        <option value=''>--全部--</option>
                                        <?php if(is_array($projectResult) || $projectResult instanceof \think\Collection || $projectResult instanceof \think\Paginator): if( count($projectResult)==0 ) : echo "" ;else: foreach($projectResult as $key=>$vo): ?>
                                        <option value='<?php echo $vo['project_id']; ?>' <?php if($vo['project_id'] == \think\Request::instance()->get('project_id')): ?>selected<?php endif; ?>><?php echo $vo['project_name']; ?></option>
                                        <?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>
                                </div>

                            </div>

                            <div class="layui-input-inline">
                                <label class="layui-form-label">运单号:</label>
                                <div class="layui-input-inline">
                                    <input type="text" id="" name="orders_number" maxlength="300" autocomplete="off" value="<?php echo \think\Request::instance()->get('orders_number'); ?>"  class="layui-input">
                                </div>
                            </div>

                            <div class="layui-input-inline">
                                <label class="layui-form-label">合同号:</label>
                                <div class="layui-input-inline">
                                    <input type="text"  name="finance_number" maxlength="300" autocomplete="off" value="<?php echo \think\Request::instance()->get('finance_number'); ?>"   class="layui-input ">

                                </div>
                            </div>
                            <div class="layui-input-inline">
                                <label class="layui-form-label">发货方:</label>
                                <div class="layui-input-inline">
                                    <input type="text"  name="send_goods_company" maxlength="300" autocomplete="off" value="<?php echo \think\Request::instance()->get('send_goods_company'); ?>"   class="layui-input ">

                                </div>
                            </div>
                            <div class="layui-input-inline">
                                <label class="layui-form-label">收货方:</label>
                                <div class="layui-input-inline">
                                    <input type="text"  name="accept_goods_company" maxlength="300" autocomplete="off" value="<?php echo \think\Request::instance()->get('accept_goods_company'); ?>"   class="layui-input ">

                                </div>
                            </div>
                            <div class="layui-input-inline">
                                <label class="layui-form-label">发站:</label>
                                <div class="layui-input-inline">
                                    <input type="text"  name="send_location_name" maxlength="300" autocomplete="off" value="<?php echo \think\Request::instance()->get('send_location_name'); ?>"   class="layui-input ">

                                </div>
                            </div>
                            <div class="layui-input-inline">
                                <label class="layui-form-label">到站:</label>
                                <div class="layui-input-inline">
                                    <input type="text"  name="accept_location_name" maxlength="300" autocomplete="off" value="<?php echo \think\Request::instance()->get('accept_location_name'); ?>"   class="layui-input ">

                                </div>
                            </div>
                            <div class="layui-input-inline">
                                <label class="layui-form-label">承运商:</label>
                                <div class="layui-input-inline">
                                    <input type="text"  name="supplier_name" maxlength="300" autocomplete="off" value="<?php echo \think\Request::instance()->get('supplier_name'); ?>"   class="layui-input ">

                                </div>
                            </div>
                            <div class="layui-input-inline">
                                <label class="layui-form-label">专线编号:</label>
                                <div class="layui-input-inline">
                                    <input type="text"  name="supplier_shipment_number" maxlength="300" autocomplete="off" value="<?php echo \think\Request::instance()->get('supplier_shipment_number'); ?>"   class="layui-input ">

                                </div>
                            </div>
                            <div class="layui-input-inline">

                                <label class="layui-form-label">运单:</label>
                                <div class="layui-input-inline" style="width:100px;">
                                    <input type="text" id="start_pickup_time" autocomplete="off" value="<?php echo \think\Request::instance()->get('start_pickup_time'); ?>" name="start_pickup_time" style="width:100px;" value=""  class="layui-input">

                                </div>

                                <div class="layui-input-inline" style="width:100px;">
                                    <input type="text" id="end_pickup_time" autocomplete="off" value="<?php echo \think\Request::instance()->get('end_pickup_time'); ?>"  name="end_pickup_time" style="width:100px;" value=""  class="layui-input">

                                </div>

                            </div>
                            <div class="layui-input-inline">

                                <label class="layui-form-label">发运时间:</label>
                                <div class="layui-input-inline" style="width:100px;">
                                    <input type="text" id="start_shipment_time" autocomplete="off" value="<?php echo \think\Request::instance()->get('start_shipment_time'); ?>" name="start_shipment_time" style="width:100px;" value=""  class="layui-input">

                                </div>

                                <div class="layui-input-inline" style="width:100px;">
                                    <input type="text" id="end_shipment_time" autocomplete="off" value="<?php echo \think\Request::instance()->get('end_shipment_time'); ?>"  name="end_shipment_time" style="width:100px;" value=""  class="layui-input">

                                </div>

                            </div>

                            <div class="layui-input-inline" >
                                <label class="layui-form-label">分公司:</label>
                                <div class="layui-input-inline" style="width: 87px;">
                                    <select id='company_id' name='choose_company_id' >
                                        <option value=''>--全部--</option>
                                        <?php if(is_array($comapnyResult) || $comapnyResult instanceof \think\Collection || $comapnyResult instanceof \think\Paginator): if( count($comapnyResult)==0 ) : echo "" ;else: foreach($comapnyResult as $key=>$v): ?>
                                        <option value="<?php echo $v['company_id']; ?>"

                                                <?php if(\think\Request::instance()->get('choose_company_id') != ''): if(\think\Request::instance()->get('choose_company_id') == $v['company_id']): ?>selected <?php endif; else: if(\think\Session::get('user.company_id') == $v['company_id']): ?>selected <?php endif; endif; ?>
                                        >

                                        <?php echo $v['company_name']; ?></option>
                                        <?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>
                                </div>
                            </div>
                            <button class="layui-btn nav-search search_button" >搜索</button>
                            <!--<div class='layui-input-inline'>-->
                            <!--<input type="text" id="" name=""  placeholder="电话、联系人、操作人" class="layui-input">-->
                            <!--</div>-->
                        </div>
                    </form>


                </div>
                <div class="content-bg" >



                    <table id="layui-table"  lay-filter="layui-filter">

                    </table>
                    <script type="text/html" id="toolbarDemo">
                        </div>
                        <!--	<div class="layui-inline" lay-event="delete"> <i class="layui-icon layui-icon-delete"></i> </div>-->
<!--                        <button class="layui-btn nav-add layui-btn-sm" lay-event="print_finance_number">打印合同单</button>-->
<!--                        <button class="layui-btn nav-add layui-btn-sm" lay-event="del_shipment">删除</button>-->
                        <span ><a href='/shipment/export2?project_id=<?php echo \think\Request::instance()->get('project_id'); ?>&orders_number=<?php echo \think\Request::instance()->get('orders_number'); ?>&finance_number=<?php echo \think\Request::instance()->get('finance_number'); ?>&send_goods_company=<?php echo \think\Request::instance()->get('send_goods_company'); ?>&accept_goods_company=<?php echo \think\Request::instance()->get('accept_goods_company'); ?>&send_location_name=<?php echo \think\Request::instance()->get('send_location_name'); ?>&accept_location_name=<?php echo \think\Request::instance()->get('accept_location_name'); ?>&supplier_name=<?php echo \think\Request::instance()->get('supplier_name'); ?>&supplier_shipment_number=<?php echo \think\Request::instance()->get('supplier_shipment_number'); ?>&start_pickup_time=<?php echo \think\Request::instance()->get('start_pickup_time'); ?>&end_pickup_time=<?php echo \think\Request::instance()->get('end_pickup_time'); ?>&start_shipment_time=<?php echo \think\Request::instance()->get('start_shipment_time'); ?>&end_shipment_time=<?php echo \think\Request::instance()->get('end_shipment_time'); ?>&export=1'><button class="layui-btn nav-add layui-btn-sm">导出</button></a></span>
                        <!--                    <a class="layui-btn layui-btn-sm layui-btn-normal" id="clear"><i class="layui-icon layui-icon-delete"></i>清除缓存</a>-->

<!--                        <button class="layui-btn layui-btn-sm" lay-event="savecol">保存排序</button>-->
                        <!--                    <button class="layui-btn layui-btn-danger layui-btn-sm" lay-event="clearcol">清除排序</button>-->
                        &nbsp;&nbsp;
                        <a href="javascript:void(0);">已选中&nbsp;<span class="layui-badge" id="nums">0</span></a>

                    </script>


                </div>

            </div>
        </div>


    </div>
    <?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
    <script src='/static/javascript/shipment/shipment.js'></script>
    <script>
        var layer = layui.layer;

        var form = layui.form;
        var table = layui.table;

        layui.config({

        }).extend({
            soulTable: '/static/layui-soul-table/ext/soulTable',
            tableChild: '/static/layui-soul-table/ext/tableChild',
            tableMerge: '/static/layui-soul-table/ext/tableMerge',
            tableFilter: '/static/layui-soul-table/ext/tableFilter',
            excel: '/static/layui-soul-table/ext/excel'
        });

        layui.use(['table','table','soulTable','element'], function(){
            var table = layui.table;
            var   laytable =layui.laytable;
            var soulTable = layui.soulTable;

            var isShow = true;  //定义⼀个标志位
            $('.kit-side-fold').click(function(){
                //选择出所有的span，并判断是不是hidden
                $('.layui-nav-item span').each(function(){
                    if($(this).is(':hidden')){
                        $(this).show();
                    }else{
                        $(this).hide();
                    }
                });
                //判断isshow的状态
                if(isShow){
                    $('.layui-side.layui-bg-black').width(50); //设置宽度
                    $('.kit-side-fold i').css('margin-right', '70%');  //修改图标的位置
                    //将footer和body的宽度修改
                    $('.layui-body').css('left', 60+'px');
                    $('.layui-footer').css('left', 60+'px');
                    //将⼆级导航栏隐藏
                    $('dd span').each(function(){
                        $(this).hide();
                    });
                    //修改标志位
                    isShow =false;
                }else{
                    $('.layui-side.layui-bg-black').width(200);
                    $('.kit-side-fold i').css('margin-right', '10%');
                    $('.layui-body').css('left', 200+'px');
                    $('.layui-footer').css('left', 200+'px');
                    $('dd span').each(function(){
                        $(this).show();
                    });
                    isShow =true;
                }
            });

            //加载日期控件
            layui.laydate.render({
                elem: '#start_shipment_time' ,//input的id

            });
            layui.laydate.render({
                elem: '#end_shipment_time' ,//input的id

            });
            layui.laydate.render({
                elem: '#start_pickup_time' ,//input的id

            });
            layui.laydate.render({
                elem: '#end_pickup_time' ,//input的id

            });

            var defaultcols = [ //表头
                {type: 'checkbox', fixed: 'left',height:22}
                ,{field: 'abnormal_id2', title: '序号',width:30,type:'numbers'}

                , {field: 'pickup_time', title: '运单日期',width:70,sort:true}
                , {field: 'shipment_time', title: '发运日期',width:70,sort:true}
                ,{field: 'project_name', title: '项目',width:70,sort:true}
                ,{field: 'orders_number', title: '运单号',width:110,sort:true,templet:function(d){
                        return '<span class="openOrders" orders_number="'+d.orders_number+'"><a href="javascript:void(0)">'+ d.orders_number +'</a></span>'
                    }}
                ,{field: 'finance_number', title: '合同号',width:85,sort:true,templet:function(d){

                        return '<span class="openShipment" orders_number="'+d.orders_number+'" shipment_uuid="'+d.shipment_uuid+'" ><a href="javascript:void(0)">'+ d.finance_number +'</a></span>'


                    }}
                ,{field: 'supplier_name', title: '承运商',sort:true,width:120}

                ,{field: 'supplier_shipment_number', title: '专线编号',sort:true,width:90}

                ,{field: 'send_location_name', title: '发站',sort:true,width:70}
                ,{field: 'accept_location_name', title: '到站',sort:true,width:70}

                ,{field: 'send_goods_company', title: '发货方',sort:true,width:80}
                ,{field: 'accept_goods_company', title: '收货方',sort:true,width:90}

                ,{field: 'accept_address', title: '收货地址',sort:true,width:130}

                ,{field: 'goods_name', title: '货物名称',sort:true,width:70}
                ,{field: 'shipment_count', title: '发运件数',width:70,sort:true,totalRow: true}
                ,{field: 'shipment_pack_unit', title: '发运数量',width:70,sort:true,totalRow: true}

                ,{field: 'shipment_volume', title: '发运体积',width:70,sort:true,totalRow: true}
                ,{field: 'shipment_weight', title: '发运重量', width:70,sort:true,totalRow: true}
                ,{field: 'pay_all_money', title: '总运费',width:60,sort:true,totalRow: true}
                ,{field: 'yunfei', title: '运费',width:50,sort:true,totalRow: true}
                ,{field: 'songhuofei', title: '送货费',width:60,sort:true,totalRow: true}
                ,{field: 'qitafei', title: '其他费',width:60,sort:true,totalRow: true}
                ,{field: 'xiehuofei', title: '卸货费',width:60,sort:true,totalRow: true}
                ,{field: 'qibufei', title: '起步费',width:60,sort:true,totalRow: true}
                ,{field: 'tihuofei', title: '提货费',width:60,sort:true,totalRow: true}

                ,{field: 'is_short_barge', title: '是否短驳',width:70,sort:true}
                ,{field: 'is_print', title: '是否打印',width:70,sort:true}
                ,{field: 'shipment_remark', title: '备注',width:120,sort:true}
                , {field: 'shipment_finance_name', title: '财务状态',width:70,sort:true}
            ];
            var cols = JSON.parse(localStorage.getItem('savecol-lineover'));

            if(cols == null){
                var cols = defaultcols;
            }else{
                cols[0].forEach(function (tmpcol, tmpindex){
                    if(tmpcol.field == 'orders_number'){
                        cols[0][tmpindex].templet = function(d){
                            return '<span class="openOrders" orders_number="'+d.orders_number+'"><a href="javascript:void(0)">'+ d.orders_number +'</a></span>'
                        }
                    }
                    if(tmpcol.field == 'finance_number'){
                        cols[0][tmpindex].templet = function(d){
                            return '<span class="openShipment" orders_number="'+d.orders_number+'" shipment_uuid="'+d.shipment_uuid+'" ><a href="javascript:void(0)">'+ d.finance_number +'</a></span>'
                        }
                    }
                });
            }

            var languagetable=table.render({
                elem: '#layui-table'
                ,height: tableHeight
                ,url: '/shipment/getLineOverAjax?orders_number=<?php echo \think\Request::instance()->get('orders_number'); ?>&project_id=<?php echo \think\Request::instance()->get('project_id'); ?>&start_pickup_time=<?php echo \think\Request::instance()->get('start_pickup_time'); ?>&end_pickup_time=<?php echo \think\Request::instance()->get('end_pickup_time'); ?>&start_shipment_time=<?php echo \think\Request::instance()->get('start_shipment_time'); ?>&end_shipment_time=<?php echo \think\Request::instance()->get('end_shipment_time'); ?>&send_goods_company=<?php echo \think\Request::instance()->get('send_goods_company'); ?>&accept_goods_company=<?php echo \think\Request::instance()->get('accept_goods_company'); ?>&send_location_name=<?php echo \think\Request::instance()->get('send_location_name'); ?>&finance_number=<?php echo \think\Request::instance()->get('finance_number'); ?>&accept_location_name=<?php echo \think\Request::instance()->get('accept_location_name'); ?>&supplier_name=<?php echo \think\Request::instance()->get('supplier_name'); ?>&supplier_shipment_number=<?php echo \think\Request::instance()->get('supplier_shipment_number'); ?>&choose_company_id=<?php echo \think\Request::instance()->get('choose_company_id'); ?>' //数据接口

                ,response: {

                    statusCode: 200 //规定成功的状态码，默认：0

                }
                ,page: true //开启分页
                ,limit:60
                ,toolbar: '#toolbarDemo' //开启工具栏，此处显示默认图标，可以自定义模板，详见文档
                ,drag: false // 关闭拖拽列功能
                ,defaultToolbar: ['exports']
                ,totalRow: true //开启合计行
                ,cols: [cols]
                ,filter: {
                    items:['column','data','condition','editCondition','excel','clearCache'],
                    cache: true
                }
                ,done: function (res, curr, count) {
                    line_counts = res.data.length;
                    soulTable.render(this)
                }
            });

            $(document).on('click','#clear',function(){

                soulTable.clearCache(languagetable.config.id)
                layer.msg('已还原！', {icon: 1, time: 1000})

            })

            // $('#clear').on('click', function() {

            // })

            //监听头工具栏事件
            table.on('toolbar(layui-filter)', function(obj){

                var checkStatus = table.checkStatus(obj.config.id)
                    ,data = checkStatus.data; //获取选中的数据
                switch(obj.event){
                    case 'add':
                        if(data.length === 0){
                            layer.msg('请选择一行');
                        } else if(data.length > 1){
                            layer.msg('只能选择一个');
                        } else {
                            var ordersNumber= checkStatus.data[0].orders_number
                            location.href='getLineOverAjax?orders_number='+ordersNumber
                        }

                        break;
                    case 'update':
                        if(data.length === 0){
                            layer.msg('请选择一行');
                        } else if(data.length > 1){
                            layer.msg('只能同时编辑一个');
                        } else {
                            var after_url = $('#now_url').val();
                            location.href="/shipment/addLine?orders_number="+checkStatus.data[0].orders_number+"&shipment_uuid="+checkStatus.data[0].shipment_uuid+'&after_url='+after_url

                        }
                        break;
                    case 'delete':
                        if(data.length === 0){
                            layer.msg('请选择一行');
                        } else {
                            layer.msg('删除');
                        }
                        break;
                    case "del_shipment":
                        if(data.length === 1){
                            if(data[0].shipment_finance_status!=1){
                                layer.msg('只能删除状态为未核算的数据');
                                return false;
                            }else{
                                layer.confirm('确认删除该数据？', function(index){
                                    $.ajax({
                                        url:"/shipment/delShipment",
                                        data:{orders_number:data[0].orders_number,finance_number:data[0].finance_number},
                                        success:function(e){
                                            location.reload()
                                        },


                                    })
                                    layer.close(index);
                                });
                            }






                            return false;
                        }else{
                            layer.msg('请选择一行');
                        }
                        break;
                    case 'savecol':
                        var cols = JSON.stringify(obj.config.cols);
                        localStorage.setItem('savecol-lineover',cols);
                        layer.msg('保存成功~');
                        break;
                    case 'clearcol':
                        layer.confirm('你确定要清除列的排序记忆吗？', {
                            btn: ['确认', '取消']
                        }, function () {
                            //tnnd不好使 localStorage.removeItem('savecol')
                            localStorage.clear();
                            delete cols;
                            layer.msg('清空成功~');
                            table.reload('layui-table',{cols: [defaultcols]})
                        });
                        break;
                    case "print_finance_number":
                        if(data.length === 0){
                            layer.msg('请选择一行');
                            return false;
                        }
                        layer.load(1);
                        var sendData=[];
                        for(var i=0;i<data.length;i++){
                            sendData.push(data[i].shipment_uuid);
                            $.ajax({
                                url:"/index/changeStatus",
                                data:{table_id:data[i].shipment_uuid,table_id_name:'shipment_uuid',table_name:"shipment",status:"1",field:"is_print"},
                                success:function(e){

                                },
                                async:false

                            })
                        }
                        layer.closeAll();
                        var url="/despatch/showPrintPreviewManageAll?shipment_uuid="+sendData.join();
                        window.open(url,'_blank');

                    //window.location.reload();


                };
            });

            var nums = 0;
            //多选事件
            table.on('checkbox(layui-filter)', function(obj){
                if(obj.type=="all"){ //全选
                    if(obj.checked==true){
                        $(".layui-table-body table.layui-table tbody tr").css('background','#99CCFF');
                        $("#nums").html(line_counts);
                    }else{
                        $(".layui-table-body table.layui-table tbody tr").css('background','white');
                        $("#nums").html(0);
                    }
                }else{ //单选
                    if(obj.checked==true){
                        obj.tr.css('background','#99CCFF');
                        nums = parseInt($("#nums").html())+1;
                        $("#nums").html(nums);
                    }else{
                        obj.tr.css('background','white');
                        nums = parseInt($("#nums").html())-1;
                        $("#nums").html(nums);
                    }
                }
            });

            //行点击事件
            table.on('row(layui-filter)', function(obj){

            });

        });

    </script>
    <script>
        //加载弹窗方法
        function load_window(v1,v2){
            var url = "/shipment/addLine?orders_number="+v1+"&shipment_uuid="+v2;
            layer.open({
                type:2,
                title: '修改发运',
                maxmin :true,
                success: function(layero,index){
                    //在回调方法中的第2个参数“index”表示的是当前弹窗的索引。
                    //通过layer.full方法将窗口放大。
                    layer.full(index);
                },
                content: url
            });
        }
    </script>
</body>
</html>


