<?php

namespace app\index\model\source;
use app\index\model\Base;
use think\Model;
use app\common\help\Help;
use app\common\help\Contents;
use app\index\service\PublicService;
use think\config;
use think\Db;
class Goods extends Base {
    //protected $connection = ['database' => 'erp'];
    protected $table = 'goods';
    private $_public_service;

    public function initialize()
    {
    	$this->_public_service = new PublicService();
    	parent::initialize();
    
    }

    public function  getAcceptgoodsname($params){
        if(isset($params['keyword']))
        {
            $data=$this->whereLike('accept_goods_company','%'.$params['keyword'].'%')
                ->limit(0,10)
                ->select();
            return $data;
        }

    }

    /**
     * 添加货物
     * 胡
     */
    public function addGoods($params){
    	$t = time();

    	$data['project_id'] = $params['project_id'];
    	$data['goods_name'] = $params['goods_name'];
    	$data['goods_packaging_type'] = $params['goods_packaging_type'];
    	if(!empty($params['goods_cost_unit'])){
    		$data['goods_cost_unit'] = $params['goods_cost_unit'];
    	}
    	if(!empty($params['billing_unit'])){
    		$data['billing_unit'] = $params['billing_unit'];
    	}
    	if(!empty($params['goods_specification'])){
    		$data['goods_specification'] = $params['goods_specification'];
    	}
    	if(!empty($params['goods_model'])){
    		$data['goods_model'] = $params['goods_model'];
    		 
    	}if(isset($params['calc_way'])){
    		$data['calc_way'] = $params['calc_way'];

    	}
    	if(!empty($params['goods_weight'])){
    		$data['goods_weight'] = $params['goods_weight'];    		 
    	}
    	if(!empty($params['goods_volume'])){
    		$data['goods_volume'] = $params['goods_volume'];
    		 
    	}
    	if(!empty($params['goods_length'])){
    		$data['goods_length'] = $params['goods_length'];
    		 
    	}    	
    	if(!empty($params['goods_width'])){
    		$data['goods_width'] = $params['goods_width'];
    		 
    	}   
    	if(!empty($params['goods_height'])){
    		$data['goods_height'] = $params['goods_height'];
    		 
    	}
        if(!empty($params['default_volume'])){
            $data['default_volume'] = $params['default_volume'];

        }
        if(!empty($params['default_weight'])){
            $data['default_weight'] = $params['default_weight'];

        }
    	if(!empty($params['remark'])){
    		$data['remark'] = $params['remark'];
    		 
    	}
    
    
    
    	$data['create_time'] = $t;
    	$data['create_user_id'] = $params['user_id'];
    	$data['update_time'] = $t;
    	$data['update_user_id'] = $params['user_id'];
    	$data['status'] = $params['status'];
    
    
    	Db::startTrans();
    	try{
    		$pk_id = Db::name('goods')->insertGetId($data);
    
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
    
    	return $result;
    }
    
    /**
     * 获取商品
     * 胡
     */
    public function getGoods($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    	$data = "1=1 ";
  
    	if(is_numeric($params['project_id'])){
    		$data.= " and goods.project_id = ".$params['project_id'];
    	}
    	if(is_numeric($params['status'])){
    		$data.= " and goods.status = ".$params['status'];
    	}    	
    	if(is_numeric($params['goods_packaging_type'])){
    		$data.= " and goods.goods_packaging_type = ".$params['goods_packaging_type'];
    	}
        if(is_numeric($params['goods_id'])){
            $data.= " and goods.goods_id = ".$params['goods_id'];
        }
    	if(!empty($params['goods_name'])){
    		$data.= ' and goods.goods_name like "%'.$params['goods_name'].'%"';
    	}
        if(!empty($params['goods_names'])){
            $data.=' and goods.goods_name ="'.$params['goods_names'].'"';
        }

    	if($is_count==true){
    		$result = $this->table("goods")->where($data)->count();
    	}else {
    		if ($is_page == true) {
    			$result = $this->table("goods")->
    			join("project",'project.project_id = goods.project_id')->
    			where($data)->limit($page, $page_size)->order('goods.create_time desc')->
    			field(['goods.*','project.project_name',

    					"(select nickname  from user where user.user_id = goods.create_user_id)"=> 'create_user_name',
    					"(select nickname  from user where user.user_id = goods.update_user_id)"=> 'update_user_name',
    					'goods.update_time', 'goods.create_time', "goods.status",
    			])->select();
    			 
    			 
    		}else{
    			$result = $this->table("goods")->alias('goods')->
    			join("project",'project.project_id = goods.project_id')->
    
    			where($data)->order('goods.create_time desc')->
    			field(['goods.*','project.project_name',

    					"(select nickname  from user where user.user_id = goods.create_user_id)"=> 'create_user_name',
    					"(select nickname  from user where user.user_id = goods.update_user_id)"=> 'update_user_name',
    					'goods.update_time', 'goods.create_time', "goods.status",
    			])->select();
    		}

    		
    	}
    
    	return $result;
    
    }
    
    /**
     * 修改货物
     */
    public function updateGoodsByGoodsId($params){
    
    	$t = time();
    	 
      	$data['goods_name'] = $params['goods_name'];
    	$data['goods_packaging_type'] = $params['goods_packaging_type'];
        if(!empty($params['billing_unit'])){
            $data['billing_unit'] = $params['billing_unit'];
        }
    	if(!empty($params['goods_specification'])){
    		$data['goods_specification'] = $params['goods_specification'];
    	}
    	if(!empty($params['goods_model'])){
    		$data['goods_model'] = $params['goods_model'];
    		 
    	}
        if(isset($params['calc_way'])){
            $data['calc_way'] = $params['calc_way'];

        }

    	if(!empty($params['goods_cost_unit'])){
    		$data['goods_cost_unit'] = $params['goods_cost_unit'];
    		 
    	}
    	//if(!empty($params['goods_weight'])){
    		$data['goods_weight'] = $params['goods_weight'];    		 
    	//}
    	//if(!empty($params['goods_volume'])){
    		$data['goods_volume'] = $params['goods_volume'];
    		 
    	//}
    	if(!empty($params['goods_length'])){
    		$data['goods_length'] = $params['goods_length'];
    		 
    	}    	
    	if(!empty($params['goods_width'])){
    		$data['goods_width'] = $params['goods_width'];
    		 
    	}   
    	if(!empty($params['goods_height'])){
    		$data['goods_height'] = $params['goods_height'];
    		 
    	}
        if(!empty($params['default_volume'])){
            $data['default_volume'] = $params['default_volume'];

        }
        if(!empty($params['default_weight'])){
            $data['default_weight'] = $params['default_weight'];

        }
    	if(!empty($params['remark'])){
    		$data['remark'] = $params['remark'];
    		 
    	}

        $data['status'] = $params['status'];
    	$data['update_user_id'] = $params['user_id'];
    	$data['update_time'] = $t;
    
    
    
    	$source_price=[];
    	Db::startTrans();
    	try{
    		Db::name('goods')->where("goods_id = ".$params['goods_id'])->update($data);
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    
    	}
    	return $result;
    }
    

}