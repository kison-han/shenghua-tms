<?php

namespace app\index\model\source;
use think\Model;
use app\common\help\Help;
use app\index\service\PublicService;
use think\config;
use think\Db;
class Customer extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'customer';
    private $_languageList;
    private $_public_service;
    public function initialize()
    {
    	$this->_languageList = config('systom_setting')['language_list'];
    	$this->_public_service = new PublicService();
    	parent::initialize();
    
    }

    /**
     * 添加客户
     * 胡
     */
    public function addCustomer($params){
    	$t = time();

    	$data['company_name'] = $params['company_name'];

    	
    	if(!empty($params['customer_name'])){
    		$data['customer_name'] = $params['customer_name'];
    	}
    	

    	if(!empty($params['cellphone'])){
    		$data['cellphone'] = $params['cellphone'];
    	
    	}





    	$data['create_time'] = $t;  	
    	$data['create_user_id'] = $params['user_id'];
    	$data['update_time'] = $t;
    	$data['update_user_id'] = $params['user_id'];
    	$data['status'] = $params['status'];

    
    	Db::startTrans();
    	try{
    		$pk_id = Db::name('customer')->insertGetId($data);
	
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
    
    	return $result;
    }
    
    /**
     * 获取客户
     * 胡
     */
    public function getCustomer($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    	$data = "1=1 ";

    	if(is_numeric($params['status'])){
    		$data.= " and status = ".$params['status'];
    	}
    	if(!empty($params['company_name'])){
    		$data.= ' and company_name like "%'.$params['company_name'].'%"';
    	}
    	if(!empty($params['customer_name'])){
    		$data.= ' and customer_name like "%'.$params['customer_name'].'%"';
    	}   	
    	if(!empty($params['cellphone'])){
    		$data.= ' and cellphone like "%'.$params['cellphone'].'%"';
    	}
        if(!empty($params['customer_id'])){
            $data.=" and customer_id =".$params['customer_id'];
        }



        if($is_count==true){
            $result = $this->table("customer")->where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("customer")->          
                where($data)->limit($page, $page_size)->order('create_time desc')->
                field(['customer_id','company_name','customer_name','cellphone',
                    "(select nickname  from user where user.user_id = customer.create_user_id)"=> 'create_user_name',
                    "(select nickname  from user where user.user_id = customer.update_user_id)"=> 'update_user_name',
                    'update_time', 'create_time', "status",
                ])->select();
               
               
            }else{
                $result = $this->table("customer")->alias('customer')->
                where($data)->order('create_time desc')->
                field(['customer_id','company_name','customer_name','cellphone',
                    "(select nickname  from user where user.user_id = customer.create_user_id)"=> 'create_user_name',
                    "(select nickname  from user where user.user_id = customer.update_user_id)"=> 'update_user_name',
                    'update_time', 'create_time', "status",
                ])->select();
            }
        }

        return $result;
    
    }

    /**
     * 修改客户
     */
    public function updateCustomerByCustomerId($params){
    
    	$t = time();
    	
    	if(!empty($params['company_name'])){
    		$data['company_name'] = $params['company_name'];
    	
    	}


    		 
    
        if(isset($params['customer_name'])){
            $data['customer_name'] = $params['customer_name'];
        }
        if(isset($params['cellphone'])){
            $data['cellphone'] = $params['cellphone'];
        }


    	if(!empty($params['status'])){
    		$data['status'] = $params['status'];
    		 
    	}



    	$data['update_user_id'] = $params['user_id'];   
    	$data['update_time'] = $t;

    
    
    	$source_price=[];
    	Db::startTrans();
    	try{
    		Db::name('customer')->where("customer_id = ".$params['customer_id'])->update($data);
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    
    	}
    	return $result;
    }

}