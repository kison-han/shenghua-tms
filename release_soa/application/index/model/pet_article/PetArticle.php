<?php

namespace app\index\model\pet_article;
use think\Model;
use think\config;
use app\common\help\Help;
use think\Db;
class PetArticle extends Model{


    public function initialize()
    {
        parent::initialize();
    }

    public function addPetArticle($params){
    
    	$t = time();
    
    	$data['pet_article_name'] = $params['pet_article_name'];
    	$data['pet_article_type_id'] = $params['pet_article_type_id'] ;
    	if($params['image']){
    		$data['image'] = $params['image'];
    	}
    	if($params['content']){
    		$data['content'] = $params['content'];
    	}
    	if($params['title']){
    		$data['title'] = $params['title'];
    	}
    	if($params['description']){
    		$data['description'] = $params['description'];
    	}
    	if($params['keywords']){
    		$data['keywords'] = $params['keywords'];
    	}



   
    
    	$data['status'] = $params['status'];
    	$data['update_user_id'] = $params['user_id'];
    	$data['update_time'] = $t;
    	$data['create_user_id'] = $params['user_id'];
    	$data['create_time'] = $t;
    
    	Db::startTrans();
    	try{
    		Db::name('pet_article')->insertGetId($data);
    
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
    
    	return $result;
    }
    
    
    public function getPetArticle($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    
    	$data = "1=1 ";
    	if(is_numeric($params['status'])){
    		$data.= " and pet_article.status= ".$params['status'];
    	}
    	if(is_numeric($params['pet_article_type_id'])){
    		$data.= " and pet_article.pet_article_type_id= ".$params['pet_article_type_id'];
    	}
    	if(!empty($params['pet_article_name'])){
    		$data.= " and pet_article_name like'%".$params['pet_article_name']."%'";
    	}
    
    	if($is_count==true){
    		$result = $this->table("pet_article")->where($data)->count();
    	}else {
    		if ($is_page == true) {
    
    			$result = $this->table("pet_article")->join('pet_article_type','pet_article_type.pet_article_type_id = pet_article.pet_article_type_id')
    			->where($data)
    			->limit($page, $page_size)
    			->field(['pet_article.*','pet_article_type.pet_article_type_name',
    					"(select nickname  from user where user.user_id = pet_article_type.create_user_id)"=>'create_user_name',
    					"(select nickname  from user where user.user_id = pet_article_type.update_user_id)"=>'update_user_name',
    			])
    			->select();
    
    		} else {
    
    			$result = $this->table("pet_article")->join('pet_article_type','pet_article_type.pet_article_type_id = pet_article.pet_article_type_id')
    			->where($data)
    			->field(['pet_article.*','pet_article_type.pet_article_type_name',
    					"(select nickname  from user where user.user_id = pet_article_type.create_user_id)"=>'create_user_name',
    					"(select nickname  from user where user.user_id = pet_article_type.update_user_id)"=>'update_user_name',
    
    
    
    			])
    			->select();
    
    		}
    	}
    
    	return $result;
    }
    
    
    public function updatePetArticleByPetArticleId($params){
    	$t = time();
    	
    	if($params['pet_article_name']){
    		$data['pet_article_name'] = $params['pet_article_name'];
    	}
    

    	//通过PID查找LEVEL
    
    
    	$data['pet_article_type_id'] = $params['pet_article_type_id'];
    	//        if(isset($params['image'])){
    	//            $data['image'] = $params['image'];
    	//        }
    	if(isset($params['content'])) {
    		$data['content'] = $params['content'];
    	}
    	$data['image'] = isset($params['image']) ? $params['image']: '';
    	$data['title'] = isset($params['title']) ? $params['title']: '';
    	$data['description'] = isset($params['description']) ? $params['description']: '';
    	$data['keywords'] = isset($params['keywords']) ? $params['keywords']: '';
    
    
    	$data['status'] = $params['status'];
    	$data['update_user_id'] = $params['user_id'];
    	$data['update_time'] = $t;
    
    	 
    	Db::startTrans();
    	try{
    		Db::name('pet_article')->where("pet_article_id = ".$params['pet_article_id'])->update($data);
    
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    
    	}
    	return $result;
    
    
    }
    
}