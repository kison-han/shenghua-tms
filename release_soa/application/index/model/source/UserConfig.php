<?php


namespace app\index\model\source;


use app\index\model\Base;
/**
 * 用户配置
 * 袁
 */
class UserConfig extends Base
{

    protected $table = 'user_config';

    public function getUserConfig($params){
    $data=$this->where('user_id',$params['user_id'])->select();
    return $data;
}
public function setUserConfig($params){
        $config_info=$this->where('user_id',$params['user_id'])
            ->where('key',$params['key'])
            ->find();
if($config_info)
{
    if (isset($params['isDel']))
        $config_info->delete();
    else
    {
        $config_info->value=$params['value'];
        $config_info->save();
    }

}else
{
    $insert_data['user_id']=$params['user_id'];
    $insert_data['key']=$params['key'];
    $insert_data['value']=$params['value'];
    $this->pubInsert($insert_data);

}

return 1;
}

}