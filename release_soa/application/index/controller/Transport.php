<?php


namespace app\index\controller;


use app\index\model\publicmodel\Check;
use think\Validate;

class Transport extends Base
{


    public function __construct()
    {
        $this->_transportModel=new \app\index\model\transport\Transport();
    }

    public function uploadTansport(){

        $params = $this->input();
         $validate=new Validate(['transport_id'=>'number|require']);
        if(!$validate->check($params))
         return  $this->outPutError($validate->getError());

        if(\app\index\model\transport\Transport::uploadTransport($params))
            return  $this->outPut('success');

    }

    public function getTransportById(){


       $params = $this->input();
       $validate=new Validate(['transport_id'=>'require|number']);
       if(!$validate->check($params))
       {
           $this->outPutError($validate->getError());
       }

        $data=$this->_transportModel->getTransportById($params);
       if($data)
           return $this->outPut($data);
       else
           return $this->outPutError("获取失败");

    } public function uploadTransport(){


        $params=  $params = $this->input();
        $data=$this->_transportModel->getTransport();

    }

}