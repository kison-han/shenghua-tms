<?php if (!defined('THINK_PATH')) exit(); /*a:6:{s:88:"/var/www/html/test_erp/public/../application/index/view/wisdomvehicle/cross_records.html";i:1661993979;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:64:"/var/www/html/test_erp/application/index/view/public/header.html";i:1658978091;s:78:"/var/www/html/test_erp/application/index/view/public/left_wisdompark_menu.html";i:1641775712;s:62:"/var/www/html/test_erp/application/index/view/public/page.html";i:1638857582;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






    <title>出入口列表</title>
    <link href="/static/css/source.css" rel="stylesheet">
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">

    <div class="layui-header">
            <div class="layui-logo"></div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left">
        <li class="layui-nav-item">
            <a href="/turnoverbox/index">智能周转箱</a>
        </li>
        <li class="layui-nav-item">
            <a href="/">TMS</a>
        </li>
        <li class="layui-nav-item">
 
            <a href="/warehouse/index">WMS</a>
        </li>
        <li class="layui-nav-item">
            <a href="/wisdompark/index">智慧园区</a>

        </li>
        <li class="layui-nav-item">
            <a href="/Oa/index">OA</a>

        </li>   
         <li class="layui-nav-item">
            <a href="/Bms/index">BMS</a>

        </li>
        <li class="layui-nav-item">
            <a href="/equipment/equipmentManage">设备</a>

        </li>
    </ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item tips-system-message" style="cursor: pointer"><?php echo $language_tag['index_nav_system_message']; ?> <span class="system-message-quantity" style="color: red"> 1</span> </li>
      <li class="layui-nav-item">
        <a href="javascript:;">
          <!-- <img src="http://t.cn/RCzsdCq" class="layui-nav-img"> -->
          <?php echo \think\Session::get('user.nickname'); ?>
        </a>
        <dl class="layui-nav-child head-top">
          <dd><a href="/system/setUserInfo/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_basicDocument']; ?></a></dd>
          <dd><a href="/system/showChangePassword/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_resetPassword']; ?></a></dd>
        </dl>
      </li>
      <li class="layui-nav-item"><a href="/login/loginOut">退出登录</a></li>
    </ul>

    <div id="tips-system-message-js" style='display:none;'>
        <div  style="max-height:350px;width: 450px">
             <div style="color: #000;text-align: center;height: 10px;padding: 10px">你有 <font class="f-tips-system-message"></font> 条未读消息</div>
             <hr>
             <div class="tips-system-message-div" style="height:220px;overflow-y: auto">
           
                 <li style="color: #000; padding: 10px;cursor: pointer" data-href="" data-id="" class="aUrl" onclick="Aurl(this)" ></li>
                 <hr>
             
             </div>
            <div style="color: #000;text-align: center;height: 40px;padding-top: 15px"><a href="/reminderManagement/allInStationLetter"><?php echo $language_tag['index_nav_viewAll_messa']; ?></a></div>
        </div>
    </div>

    <!--  下列保存用户SESSION信息 -->
	
    <input type="hidden" id='now_url'  value="<?php echo $now_url; ?>" />
	<input type="hidden" id='after_url'  value="<?php echo $after_url; ?>" />
    <input type="hidden" id='user_company_id' value="<?php echo \think\Session::get('user.company_id'); ?>" />
	<!--  下列保存其他信息 -->
	<input type="hidden" id='http_referer' value="<?php echo $http_referer; ?>" />
	
	<script type='text/javascript'>
		$('.tips-system-message').on('click',function(){
			var html = $('#tips-system-message-js').html();
		    layer.tips(html, '.tips-system-message', {
		        tips: [3, '#fff'],
		        padding:'20',
		        tipsMore: false,
		        area: ['450px', 'auto'],
		        shade: [0.01, '#fff'],
		        shadeClose:true,
		        time:0
			
		    });
		
		});
		
		$(document).ready(function(){ 
			$(document).mousemove(function(e){ 
				
				if(e.pageX<=10){
					$('.layui-bg-black').show()
				}
				
				if(e.pageX>200){
					$('.layui-bg-black').hide()
				
				}
				if(e.pageY<=10){
					$('.layui-header').show()
				}
				
				if(e.pageY>40){
					$('.layui-header').hide()
				
				}			
			}); 
			
		}); 
	

	</script>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
                  <ul class="layui-nav layui-nav-tree" id="left-nav" lay-filter="test">
          <li class="layui-nav-item"><a href="/"><i class="iconfont img-homepage_fill"></i><em>控制面板</em></a></li>
        <li <?php if($controller_name == 'wisdomdoor'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>门禁管理</em></a>
          <dl class="layui-nav-child">
             <dd <?php if(in_array(($function_name), explode(',',"showdoorlist"))): ?> class="layui-this"<?php endif; ?>><a href="/wisdomdoor/showDoorList">门禁</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showdoorevent"))): ?> class="layui-this"<?php endif; ?>><a href="/wisdomdoor/showDoorEvent">门禁点进出</a></dd>
          </dl>
        </li>
        
         <li <?php if($controller_name == 'wisdomvehicle'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>车闸管理</em></a>
          <dl class="layui-nav-child">
             <dd <?php if(in_array(($function_name), explode(',',"showparklist"))): ?> class="layui-this"<?php endif; ?>><a href="/wisdomvehicle/showParkList">停车库</a></dd>
             <dd <?php if(in_array(($function_name), explode(',',"showentrancelist"))): ?> class="layui-this"<?php endif; ?>><a href="/wisdomvehicle/showEntranceList">出入口</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showroadwaylist"))): ?> class="layui-this"<?php endif; ?>><a href="/wisdomvehicle/showRoadwayList">车道</a></dd>
             <dd <?php if(in_array(($function_name), explode(',',"showcrossrecords"))): ?> class="layui-this"<?php endif; ?>><a href="/wisdomvehicle/showCrossRecords">车辆进出</a></dd>             
          </dl>
        </li>       
        
        
        <li <?php if($controller_name == 'wisdompersion'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>人员管理</em></a>
          <dl class="layui-nav-child">
             <dd <?php if(in_array(($function_name), explode(',',"showpersionlist"))): ?> class="layui-this"<?php endif; ?>><a href="/wisdompersion/showPersionList">人员</a></dd>
 



          </dl>
        </li>
         
        <li <?php if($controller_name == 'wisdomcarpark'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>停车管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showparkingspace"))): ?> class="layui-this"<?php endif; ?>><a href="/wisdomcarpark/showParkingSpace">停车位信息</a></dd>
             <dd <?php if(in_array(($function_name), explode(',',"showappointment"))): ?> class="layui-this"<?php endif; ?>><a href="/wisdomcarpark/showAppointment">预约</a></dd>
 



          </dl>
        </li>   
          <!--<li <?php if($controller_name == 'otaarticle' or $controller_name == 'enquirty' or $controller_name == 'otasystem' or $controller_name == 'otaslide' or $controller_name == 'otaproduct' or $controller_name == 'otamember'): ?>-->
              <!--class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>-->
          <!--<a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-website"></i><em>官网管理</em></a>-->
          <!--<dl class="layui-nav-child">-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showotasystemmanage"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_system/showOtaSystemManage">网站设置</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showotamenumanage"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_system/showOtaMenuManage?status=1">菜单</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showarticletypemanage,showarticletypeadd,showarticletypeedit,showarticletypeinfo"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_article/showArticleTypeManage?status=1">文章分类</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showarticlemanage,showarticleadd,showarticleedit,showarticleinfo"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_article/showArticleManage?status=1">文章</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showslidemanage,showslideadd,showslideedit,showslideinfo"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_slide/showSlideManage?status=1">幻灯片</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showadvertmanage,showadvertadd,showadvertedit,showadvertinfo"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_slide/showAdvertManage?status=1">友情链接</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showadvertisingmanage"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_slide/showAdvertisingManage?status=1">广告位</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showcompanywebsitemanage"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_system/showCompanyWebsiteManage?status=1">域名管理</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"index,addenquirty,editenquirty"))): ?> class="layui-this" <?php endif; ?> ><a href="/enquirty/index">需求定制</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"lst,add,edit"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_member/lst?status=1">账号管理</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"types,addtype,edittype,gettypeinfo"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_product/types?status=1">旅游产品分类</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"productlists,addlist,editlist"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_product/productLists?status=1">旅游产品列表</a></dd>-->
              <!--&lt;!&ndash;<dd <?php if(in_array(($function_name), explode(',',"products,addlist,editlist"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_product/products?status=1">旅游产品</a></dd>&ndash;&gt;-->
          <!--</dl>-->

          <!--</li>-->
      </ul>
        </div>
    </div>

    <div class="layui-body layui-body-bg">

        <!-- 内容主体区域 -->
        <div class="content_body">
            <div class="body-top">
                <div class='layui-form-item'>
                    <span class="layui-breadcrumb" lay-separator="-">
                        <a>首页</a>
                        <a>车闸管理</a>
                        <a><cite>车辆进出</cite></a>
                    </span>
                </div>
                <div class='layui-block all-search-bg'>
                    <form class="layui-form" method='get' action='/wisdomvehicle/showCrossRecords'>




                                                                   
                        <div class='layui-input-inline'>
                            <input type="text" name="plateNo" class="layui-input" placeholder="车牌 "  value="<?php echo \think\Request::instance()->get('plateNo'); ?>">
                        </div>             
                                   
        

                        <div class='layui-button-inline layui-search-inline'>
                            <button class="layui-btn nav-search" >搜索</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="content-bg">
                <span></span>
                <hr>
                <div class="table-nont user-manage company-pageHeight">
                    <table class="layui-table layui-form" >
                        <thead>
                        <tr>
                       
                           <!--  <th>停车库唯一标识</th>            -->
                            <th>停车库名称</th>
							<th>出入口名称</th>
							<th>进/出</th>
							<th>进场类型</th>
                       		<th>牌照</th>
                          	<th>车辆类型</th>
                          	<th>车辆照片</th>
                          	<th>牌照照片</th>
                          	<th>通过时间</th>
                          	<th>放行结果</th>
                          	<th>车道名称</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(is_array($data) || $data instanceof \think\Collection || $data instanceof \think\Paginator): if( count($data)==0 ) : echo "" ;else: foreach($data as $key=>$vo): ?>
                        <tr>
                    
                            <!-- <th><?php echo $vo['parkSyscode']; ?></th> -->
                            <th><?php echo $vo['parkName']; ?></th>
                       		 <th><?php echo $vo['entranceName']; ?></th>
                            <th><?php if($vo['vehicleOut'] == 0): ?>进<?php else: ?>出<?php endif; ?></th>
                             <th>
                             <?php echo action('publicshow/HikReleaseMode',array('releaseMode'=>$vo['releaseMode'])); ?>
                             </th>
							<th><?php echo $vo['plateNo']; ?></th>
							<th> <?php echo action('publicshow/HikVehicleType',array('vehicleType'=>$vo['vehicleType'])); ?></th>
							<th><img src="<?php echo $hikApiUrl; ?><?php echo $vo['vehiclePicUri']; ?>"  class='vehiclePicBig' /></th>
							<th><img src="<?php echo $hikApiUrl; ?><?php echo $vo['plateNoPicUri']; ?>"  class='plantNoPicBig' /></th> 
							<th><?php echo date('Y-m-d H:i:s',strtotime($vo['crossTime'])); ?></th>         
							<th>
								<?php if($vo['releaseResult'] == 0): ?>
									未放行
								<?php elseif($vo['releaseResult'] == 1): ?>
									正常放行
								<?php elseif($vo['releaseResult'] == 2): ?>	
									离线放行
									<?php endif; ?>
							</th>
							<th><?php echo $vo['roadwayName']; ?></th>
							             
                        </tr>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                        </tbody>

                    </table>
                </div>
                <div class="listButtom">
                    <div id="demo7">
	<div class="layui-box layui-laypage layui-laypage-default" id="layui-laypage-10">
		<span class="layui-laypage-count">共 <?php echo $total; ?> 条</span>
		<?php if($page == 1): ?>
			<a href="javascript:;" class="layui-laypage-prev layui-disabled" >上一页</a>
		<?php else: ?>
			<a href="javascript:;" class="layui-laypage-prev page_href" data-page="<?php echo $page-1; ?>">上一页</a>
		<?php endif; if($page > 5): if($page > 5): $__FOR_START_1568945520__=$page-5;$__FOR_END_1568945520__=$page;for($i=$__FOR_START_1568945520__;$i < $__FOR_END_1568945520__;$i+=1){ if($i == $page): ?>
						<span class="layui-laypage-curr"><em class="layui-laypage-em"></em><em><?php echo $i; ?></em></span>
					<?php else: ?>
						<a href="javascript:;" data-page="<?php echo $i; ?>" class='page_href'><?php echo $i; ?></a>
					<?php endif; } endif; else: $__FOR_START_1589827814__=1;$__FOR_END_1589827814__=$page;for($i=$__FOR_START_1589827814__;$i < $__FOR_END_1589827814__;$i+=1){ ?>
	
						<a href="javascript:;" data-page="<?php echo $i; ?>" class='page_href'><?php echo $i; ?></a>
				
				
				<?php } endif; if(($total_page-$page) > 5): $__FOR_START_2070852181__=$page;$__FOR_END_2070852181__=$page + 5;for($i=$__FOR_START_2070852181__;$i <= $__FOR_END_2070852181__;$i+=1){ if($i == $page): ?>
					<span class="layui-laypage-curr"><em class="layui-laypage-em"></em><em><?php echo $i; ?></em></span>
				<?php else: ?>
					<a href="javascript:;" data-page="<?php echo $i; ?>" class='page_href'><?php echo $i; ?></a>
				<?php endif; } ?>
					<span class="layui-laypage-spr">…</span>
					<a href="javascript:;" class="layui-laypage-last page_href" title="尾页" data-page="<?php echo $total_page; ?>" ><?php echo $total_page; ?></a>		
		<?php else: $__FOR_START_1866624420__=$page;$__FOR_END_1866624420__=$total_page;for($i=$__FOR_START_1866624420__;$i <= $__FOR_END_1866624420__;$i+=1){ if($i == $page): ?>
					<span class="layui-laypage-curr"><em class="layui-laypage-em"></em><em><?php echo $i; ?></em></span>
				<?php else: ?>
					<a href="javascript:;" data-page="<?php echo $i; ?>" class='page_href'><?php echo $i; ?></a>
				<?php endif; } endif; if($page == $total_page): ?>
			<a href="javascript:;" class="layui-laypage-next layui-disabled" data-page="0">下一页</a>
		<?php else: ?>
			<a href="javascript:;" class="layui-laypage-next page_href" data-page="<?php echo $page+1; ?>" >下一页</a>
		<?php endif; ?>
		<!--  
		<span class="layui-laypage-curr"><em class="layui-laypage-em"></em><em>1</em></span>
		<a href="javascript:;" data-page="2">2</a>
		<a href="javascript:;" data-page="3">3</a>
		<a href="javascript:;" data-page="4">4</a>
		<a href="javascript:;" data-page="5">5</a>
		-->

		
		<!--  
		<span class="layui-laypage-limits">
			<select lay-ignore="">
				<option value="10" selected="">10 条/页</option><option value="20">20 条/页</option>
				<option value="30">30 条/页</option>
				<option value="40">40 条/页</option>
				<option value="50">50 条/页</option>
			</select>
		</span>
		
		<a href="javascript:;" data-page="1" class="layui-laypage-refresh"><i class="layui-icon layui-icon-refresh"></i></a>
		-->
		<span class="layui-laypage-skip">到第<input min="1" max='<?php echo $total_page; ?>'  value="" class="layui-input" type="text" id='page_value' >页<button type="button" class="layui-laypage-btn" id='page_button'>确定</button></span>
		</div>
</div>
<input type='hidden' id='page_url' value='<?php echo $page_url; ?>' />
<input type='hidden' id='total_page' value='<?php echo $total_page; ?>' />
<input type='hidden' id='url_params' value='<?php echo $url_params; ?>' />
<script src='/static/javascript/public/page.js'></script>
                </div>
            </div>

        </div>
    </div>
<div id='bigImage' style='width:400px;height:400px;display:none'>
	<img src=""  style='width:400px;height:400px'/>
 </div>

</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
<script src='/static/javascript/system/source.js'></script>
<script>
    var layer = layui.layer;

    var form = layui.form;
    form.on('switch(switchTest)', function(data){
        var table_id = $(data.elem).parents("tr").children(".driver_id").html();
        var table_id_name = "driver_id";
        var table_name = "driver";
        if(this.checked == true){
            var status = 1;
        }else{
            var status = 0;
        }

        $.ajax({
            type: "POST",
            url: '/index/changeStatus',
            data: {
                table_id:table_id,
                table_id_name:table_id_name,
                table_name:table_name,
                status:status,
            },
            success: function(data){
                //console.log(data);
                if(data.code!=200){
                    layer.msg(data.msg);
                    return false;
                }else if(data.code==200) {
                    layer.msg('操作成功',{time:1,end : function(layero, index){

                        }
                    });
                }
            },
            error:function(XMLHttpRequest, textStatus, errorThrown){
                layer.msg('验证失败')
            }
        });
    });
	$('.openDoor').click(function(){
		var dic = $(this).attr('key')
        $.ajax({
            type: "POST",
            url: '/wisdompark/doorControl',
            data: {
            	doorIndexCodes:dic

            },
            success: function(data){
               console.log(data);
                if(data.code!=0){
                    layer.msg(data.msg);
                    return false;
                }else if(data.code==0) {
                    layer.msg('操作成功',{time:3000,end : function(layero, index){

                        }
                    });
                }
            },
            error:function(XMLHttpRequest, textStatus, errorThrown){
                layer.msg('验证失败')
            }
        });		
		
		
		
	})

    function center(){

    	 var windowWidth = document.documentElement.clientWidth;   

    	 var windowHeight = document.documentElement.clientHeight;   
    	 var popupHeight =$('#bigImage').height();   
  
    	 var popupWidth = $('#bigImage').width();    
    	 $('#bigImage').css({   
    	  "position": "absolute",   
    	  "top": (windowHeight-popupHeight)/2+$(document).scrollTop(),   
    	  "left": (windowWidth-popupWidth)/2 ,
    	  'z-index':999
    	 });  
    	}
    center();

    $(".vehiclePicBig").hover(function(){
    	 $('#bigImage').find('img').attr('src',$(this).attr('src'))
    	  $('#bigImage').show()
    },function(){
    	$('#bigImage').hide()
    });
    $(".plantNoPicBig").hover(function(){
      	 $('#bigImage').find('img').attr('src',$(this).attr('src'))
      	  $('#bigImage').show()
      },function(){
      	$('#bigImage').hide()
      });   
    
</script>
</body>
</html>
