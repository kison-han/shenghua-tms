<?php


namespace app\index\model\despatch;


use app\common\help\Contents;
use app\index\model\Base;
use app\index\model\orders\Orders;
use app\index\model\shortbarge\Shortbarge;
use app\index\model\source\Goods;
use app\index\model\source\Supplier;
use app\index\model\system\FeeType;
use app\index\model\system\User;

class Despatch extends Base
{
protected $table="despatch";

public function getNumber($params){
    $data=$this->where('despatch_number','like','%'.$params['keyword'].'%')->limit(0,10)
        ->field(['despatch_number'=>'name','despatch_id'=>'value'])
        ->select();
return $data;
}


public function addDespatch($params){
    $despatchData["supplier_line_id"]=$params['supplier_line_id'];
    $despatchData["supplier_id"]=$params['supplier_id'];
    $despatchData["short_barge"]=$params['short_barge'];
    $despatchData["delivery"]=$params['delivery'];
    $despatchData["remark"]=$params['remark'];
    $despatchData["create_time"]=time();
    $despatchData["update_time"]=time();
    $despatchData["create_user_id"]=$params['user_id'];
    $despatchData["update_user_id"]=$params['user_id'];
    $despatchData["car_id"]=$params['car_id'];
    $despatchData["pay_type"]=$params['pay_type'];
    $despatchData["pay"]=$params['pay'];
    $despatchData["Unpaid"]=$params['Unpaid'];
    $despatchData["short_barge"]=isset($params['short_barge'])?1:0;
    $despatchData["delivery"]=isset($params['delivery'])?1:0;
    $despatchData["orders_id"]=$params['orders_id'];
    $despatchData["special_number"]=$params['special_number'];
    $despatchData["oil_card"]=$params['oil_card'];
    $despatchData["money"]=$params['money'];

    $params['despatch_id']=$this->pubInsert($despatchData);

    $des_info=$this->where('despatch_id',$params['despatch_id'])
        //->update(['despatch_number'=>'D-'.$params['despatch_id'],'contract_number'=>'C-'.$params['despatch_id']]);
       ->find();
    $des_info->despatch_number='D-'.$params['despatch_id'];
    $des_info->contract_number='C-'.$params['despatch_id'];
    $des_info->save();

    if(isset($params['jiesu']));
    {$order_model=new Orders();
        $o_info=$order_model->where('orders_id',$params['orders_id'])->find();
        $o_info->despatch_finish=1;
        $o_info->order_status=2;
        $o_info->save();
    }

    $despatchgoodsmodel=new DespatchGoods();
    $despatchgoodsmodel->addDespatchGoods($params);
    $despatchfeemodel = new DespatchFee();
    $despatchfeemodel->addDespatchFee($params);
    return 1;

}


public function getDespatchPrintData($params){


}

//获取发运
public function getDespatch($params){

        $where = [];

        if(isset($params['print'])) {
            $where['print'] = $params['print'];
        }
        if(isset($params['short_barge'])) {
            $where['short_barge'] = $params['short_barge'];
        }
        if(isset($params['despatch_id'])){
            $where['despatch_id'] = ['in',$params['despatch_id']];
        }

        $data = $this->pubGetData($where,true,['despatch_id'=>'desc']);

        $feeTypeModel = new FeeType();
        $DespatchFeeModel = new DespatchFee();
        $DespatchGoodsModel = new DespatchGoods();
        $OrdersModel = new Orders();
        $supplierModel = new Supplier();
        $goods_model = new Goods();
        $users = new User();

        $feeData = $feeTypeModel->where('cost_type',2)->select();
        foreach ($data['list'] as $k=>$v)
        {
            $data['list'][$k]['orders_data'] = $OrdersModel->where('orders_id',$v['orders_id'])->find();
            $users = $users->where('user_id',$v['create_user_id'])->find();
            $data['list'][$k]['orders_data']['user_name'] = $users['nickname'];

            $newFeeData = [];
            $feeInfo2 = [];
            foreach ($feeData as $k1=>$v1)
            {
                $feeInfo=$DespatchFeeModel->where(['despatch_id'=>$v['despatch_id'],'cost_id'=>$v1['cost_id']])->find();

                $newFeeData[] = $feeInfo['cost_fee']?$feeInfo['cost_fee']:0;

                $feeInfoRes = $DespatchFeeModel->
                join('cost','cost.cost_id = despatch_fee.cost_id','left')->
                where(['despatch_fee.despatch_id'=>$v['despatch_id'],'despatch_fee.cost_id'=>$v1['cost_id']])->find();

                 $feeInfo2[$k1]['despatch_goods_id'] = $feeInfoRes['despatch_goods_id'];
                 $feeInfo2[$k1]['despatch_id'] = $feeInfoRes['despatch_id'];
                 $feeInfo2[$k1]['cost_id'] = $feeInfoRes['cost_id'];
                 $feeInfo2[$k1]['cost_fee'] = $feeInfoRes['cost_fee'];
                 $feeInfo2[$k1]['cost_name'] = $feeInfoRes['cost_name'];
                 $feeInfo2[$k1]['cost_type'] = $feeInfoRes['cost_type'];
                 $feeInfo2[$k1]['remark'] = $feeInfoRes['remark'];
            }

             //计算总费用
             $all_nums = 11;
             foreach($newFeeData as $k2=>$v2)
             {
                 $all_nums += number_format($v2, 3, '.', '');
             }
            //转换大写总费用
            $all_china_nums = $this->RMB_Upper($all_nums);

            $data['list'][$k]['fee_data'] = $newFeeData;
            $data['list'][$k]['fee_info_data'] = $feeInfo2;
            $data['list'][$k]['all_nums'] = number_format($all_nums, 3, '.', '');
            $data['list'][$k]['all_china_nums'] = $all_china_nums;

            $goods_data = [];
            $goods_data['goods_info'] = $DespatchGoodsModel->where('despatch_id',$v['despatch_id'])->select();
            $goods_data['goods_num'] = $DespatchGoodsModel->where('despatch_id',$v['despatch_id'])->sum('despatch_number');
            $goods_data['package_number'] = $DespatchGoodsModel->where('despatch_id',$v['despatch_id'])->sum('package_number');
            $goods_data['goods_weight'] = number_format($DespatchGoodsModel->where('despatch_id',$v['despatch_id'])->sum('despatch_weight'), 3, '.', '');
            $goods_data['goods_volume'] = number_format($DespatchGoodsModel->where('despatch_id',$v['despatch_id'])->sum('despatch_volume'), 3, '.', '');
            $goods_data['goods_money'] = $DespatchGoodsModel->where('despatch_id',$v['despatch_id'])->sum('despatch_money');
            $goods_data['goods_name'] = implode(",",$DespatchGoodsModel->where('despatch_id',$v['despatch_id'])->column('goods_name'));
            $goods_data['goods_id'] = implode(",",$DespatchGoodsModel->where('despatch_id',$v['despatch_id'])->column('goods_id'));
            $goods_packaging_id = $goods_model->whereIn('goods_id',$goods_data['goods_id'])->column('goods_packaging_type');
            $goods_packaging_id = array_unique($goods_packaging_id);

            $goods_packaging_type_name = [];
            foreach ($goods_packaging_id as $k1=>$v1){
                foreach (Contents::goodsPackagingType() as $k2=>$v2)
                {
                    if ($v2['id'] == $v1) {
                        $goods_packaging_type_name[] = $v2['goods_packaging_type_name'];
                    }
                }
            }

            $goods_data['goods_packaging_type_name'] = implode(',',$goods_packaging_type_name);
            $goods_data['goods_packaging_type_id'] = implode(',',$goods_packaging_id);

            //$goods_data['goods_packaging_type'] = implode(",",$DespatchGoodsModel->where('despatch_id',$v['despatch_id'])->column('goods_id'));

            $data['list'][$k]['goods_data'] = $goods_data;
            $data['list'][$k]['supplier'] = $supplierModel->where('supplier_id',$data['list'][$k]['supplier_id'])->find();
            $data['list'][$k]['print_time'] = time();

        }
        return $data;

    }

    /**
     * @param $num
     * @return array|string|string[]
     */
    function RMB_Upper($num)
    {
        $num = round($num,2);  //取两位小数
        $num = ''.$num;  //转换成数字
        $arr = explode('.',$num);

        $str_left = $arr[0]; // 12345
        $str_right = $arr[1]; // 67

        $len_left = strlen($str_left); //小数点左边的长度
        $len_right = strlen($str_right); //小数点右边的长度

        //循环将字符串转换成数组，
        for($i=0;$i<$len_left;$i++)
        {
            $arr_left[] = substr($str_left,$i,1);
        }
        //print_r($arr_left);
        //output:Array ( [0] => 1 [1] => 2 [2] => 3 [3] => 4 [4] => 5 )

        for($i=0;$i<$len_right;$i++)
        {
            $arr_right[] = substr($str_right,$i,1);
        }
        //print_r($arr_right);
        //output：Array ( [0] => 6 [1] => 7 )

        //构造数组$daxie
        $daxie = array(
            '0'=>'零',
            '1'=>'壹',
            '2'=>'贰',
            '3'=>'叁',
            '4'=>'肆',
            '5'=>'伍',
            '6'=>'陆',
            '7'=>'柒',
            '8'=>'捌',
            '9'=>'玖',
        );

        //循环将数组$arr_left中的值替换成大写
        foreach($arr_left as $k => $v)
        {
            $arr_left[$k] = $daxie[$v];
            switch($len_left--)
            {
                //数值后面追加金额单位
                case 5:
                    $arr_left[$k] .= '万';break;
                case 4:
                    $arr_left[$k] .= '千';break;
                case 3:
                    $arr_left[$k] .= '百';break;
                case 2:
                    $arr_left[$k] .= '十';break;
                default:
                    $arr_left[$k] .= '元';break;
            }
        }
        //print_r($arr_left);
        //output :Array ( [0] => 壹万 [1] => 贰千 [2] => 叁百 [3] => 肆十 [4] => 伍元 )

        foreach($arr_right as $k =>$v)
        {
            $arr_right[$k] = $daxie[$v];
            switch($len_right--)
            {
                case 2:
                    $arr_right[$k] .= '角';break;
                default:
                    $arr_right[$k] .= '分';break;
            }
        }
        //print_r($arr_right);
        //output :Array ( [0] => 陆角 [1] => 柒分 )

        //将数组转换成字符串，并拼接在一起
        $new_left_str = implode('',$arr_left);
        $new_right_str = implode('',$arr_right);

        $new_str = $new_left_str.$new_right_str;

        //echo $new_str;
        //output :'壹万贰千叁百肆十伍元陆角柒分'

        //如果金额中带有0，大写的字符串中将会带有'零千零百零十',这样的字符串，需要替换掉
        $new_str = str_replace('零万','零',$new_str);
        $new_str = str_replace('零千','零',$new_str);
        $new_str = str_replace('零百','零',$new_str);
        $new_str = str_replace('零十','零',$new_str);
        $new_str = str_replace('零零零','零',$new_str);
        $new_str = str_replace('零零','零',$new_str);
        $new_str = str_replace('零元','元',$new_str);


        //echo'<br/>';
        return $new_str;
    }


}