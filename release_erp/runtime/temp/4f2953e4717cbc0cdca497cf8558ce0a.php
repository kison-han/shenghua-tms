<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:84:"/var/www/html/test_erp/public/../application/index/view/bill/supplier_bill_info.html";i:1665281101;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






	<title>
  		
									承运商账单详情
								
	
	</title>
<style>
	td{overflow: inherit!important;}
.addGoods{
	    color: blue;
	    display: block;
	    margin: 0 auto;
	    text-align: center;
	    font-size: 39px;
}
.delGoods{
	    color: red;
	    display: block;
	    margin: 0 auto;
	    text-align: center;
	    font-size: 39px;
}
.layui-form-item .layui-inline .layui-input-inline{

	margin-right:0px;

}	
.layui-form-item .layui-inline {

  margin-right: 0px;
  margin-bottom:0px;
}
.layui-inline{
float:left
}


</style>

</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">





	<div class="layui-body">

		<!-- 内容主体区域 -->
		<div class="content_body">


			<form class="layui-form layui-form-pane"  id="form1" onSubmit="return addSupplierBill()">
	
<fieldset class="layui-elem-field layui-field-title" style='margin:1px;' >
  <legend>基本信息</legend>
</fieldset>	

						<div class="layui-inline">
							<label class="layui-form-label">对账编号:</label>
							<div class="layui-input-inline ">
								
										<input  name="" value="<?php echo $supplier_bill_result['supplier_bill_number']; ?>" readonly placeholder="" autocomplete="off" class="layui-input" type="text">

							</div>
						</div>
 		

						<div class="layui-inline">
							<label class="layui-form-label input-required">对账名称:</label>
							<div class="layui-input-inline " style="width:320px;">
								 <input  name="supplier_bill_name" value="<?php echo $supplier_bill_result['supplier_bill_name']; ?>"  id='short_barge_time'  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
				
							</div>
						</div>
 						<div class="layui-inline">
							<label class="layui-form-label input-required">对账日期:</label>
							<div class="layui-input-inline ">
								
										<input  name="supplier_bill_date" id='supplier_date' lay-verify="required" value="<?php if($supplier_bill_result['supplier_bill_date'] != ''): ?><?php echo date('Y-m',$supplier_bill_result['supplier_bill_date']); endif; ?>" readonly placeholder="" autocomplete="off" class="layui-input" type="text">

							</div>
						</div>
 						<div class="layui-inline">
							<label class="layui-form-label input-required">是否含税:</label>
							<div class="layui-input-inline ">
								<select name='supplier_bill_tax_type'>
									<option value='2' <?php if($supplier_bill_result['supplier_bill_tax_type'] == 2 || $supplierResult['supplier_tax_type'] == 1): ?>selected<?php endif; ?>>是</option>
									<option value='1' <?php if($supplier_bill_result['supplier_bill_tax_type'] == 1): ?>selected<?php endif; ?>>否</option>
								</select>
								
							</div>
						</div>						
						<div class="layui-inline">
							<label class="layui-form-label input-required">税率:</label>
							<div class="layui-input-inline ">
								
									<select name='supplier_bill_tax' lay-verify="required">
										<option value=''>请选择</option>
										<option value='0' <?php if($supplierResult['supplier_tax'] == 0 || $supplier_bill_result['supplier_bill_tax'] == 0): ?> selected<?php endif; ?>>0</option>
										<option value='6' <?php if($supplierResult['supplier_tax'] == 6 || $supplier_bill_result['supplier_bill_tax'] == 6): ?> selected<?php endif; ?>>6</option>
										<option value='9' <?php if($supplierResult['supplier_tax'] == 9 || $supplier_bill_result['supplier_bill_tax'] == 9): ?> selected<?php endif; ?>>9</option>
									</select>
									

							</div>
						</div>
						<div class="layui-inline">
							<label class="layui-form-label input-required">分公司:</label>
							<div class="layui-input-inline ">
								
	                                    <select id='company_id' name='choose_company_id' >
                                          
											<?php if(is_array($comapnyResult) || $comapnyResult instanceof \think\Collection || $comapnyResult instanceof \think\Paginator): if( count($comapnyResult)==0 ) : echo "" ;else: foreach($comapnyResult as $key=>$v): ?>
												<option value="<?php echo $v['company_id']; ?>" 
												
												<?php if(\think\Request::instance()->get('choose_company_id') != ''): if(\think\Request::instance()->get('choose_company_id') == $v['company_id']): ?>selected <?php endif; else: if(\think\Session::get('user.company_id') == $v['company_id']): ?>selected <?php endif; endif; ?>
												>
												<?php echo $v['company_name']; ?></option>
											<?php endforeach; endif; else: echo "" ;endif; ?>                                           
                                        </select>
									

							</div>
						</div>							
      	
<div class='clear'></div>		
<fieldset class="layui-elem-field layui-field-title" style='margin:1px;' >
  <legend>费用</legend>
</fieldset>						




						<div class="layui-inline  clear">
							<label class="layui-form-label">运费:</label>
							<div class="layui-input-inline "   >
								 <input  id="vehicle_lenght"  value="<?php echo $pay_all_money; ?>" readonly name='pay_all_money' placeholder="" autocomplete="off" class="layui-input" type="text">
							</div>
						</div>	
							<div class="layui-inline">
							<label class="layui-form-label">理赔:</label>
							<div class="layui-input-inline "  >
								 <input id='vehicle_height'  value='<?php echo $abnormal_all_money; ?>' readonly  name='abnormal_all_money'  autocomplete="off" class="layui-input" type="text">
							</div>
						</div>	

						<div class="layui-inline"><label class="layui-form-label"><a href='/bill/getSupplierBillInfo?supplier_bill_number=<?php echo \think\Request::instance()->get('supplier_bill_number'); ?>&is_download=1'>导出</a>:</label></div>
																												
	<div class='clear'></div>
	<br />
  <div class="table-nont user-manage ">
                    <table class="layui-table layui-form" lay-filter="language-table" id="language-table">
               
					   <thead>
                        <tr>
       						 <th>财务状态</th>
                            <th>合同号编号</th>
 							 <th>发运类型</th>
 							 <th>运单编号</th>
                            <th>运单日期</th>
                            <th>承运商</th>
                          
                            <th>运费</th>
   							<th>赔款</th>
							<th>到付款</th>
                 		    <th>备注</th>        		                   		    

                            
                            <!--<th class="layui-table-width"><?php echo $language_tag['index_source_operation']; ?></th>-->
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(is_array($result) || $result instanceof \think\Collection || $result instanceof \think\Paginator): $i = 0; $__LIST__ = $result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                        <tr class='checknumber' finance_number='<?php echo $vo['shipment_uuid']; ?>'>

							<td ><?php echo $vo['shipment_finance_name']; ?></td>
                            <td ><?php echo $vo['finance_number']; ?></td>
                            
                            <td>
								<?php echo $vo['shipment_type_name']; ?>
                            </td>
                              <td>
								<?php echo $vo['orders_number']; ?>
                            
                              		
                              	</td>
                                <td><?php echo $vo['shipment_time']; ?></td>
                                 <td><?php echo $vo['supplier_name']; ?></td>
                                  <td><?php echo $vo['pay_all_money']; ?></td>
                                    <td><?php echo $vo['shipment_abnormal_money']; ?></td>
									<td><?php echo $vo['dfk']; ?></td>
                                <td>
									
                                <?php echo $vo['shipment_remark']; ?>
                                </td>
                                
                   


              
                            



                        </tr>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                        </tbody>

                    </table>
    </div>		    																																																					
<br />
<div class="layui-form-item ">

	<div class="layui-inline">
		<label class="layui-form-label">备注:</label>
		<div class="layui-input-inline " >
			 <input  name="supplier_bill_remark"  style='width:600px;' value="<?php echo $supplier_bill_result['supplier_bill_remark']; ?>" placeholder="" autocomplete="off" class="layui-input" type="text">
		</div>
	</div>						
  </div>				


			
		</div>

		
		
	</div>
				

				

</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
<script src='/static/javascript/bill/bill.js'></script>

</body>
</html>
<script>
layui.use(['form','laydate','table','element'], function(){
    var table = layui.table;
    // var laytable =layui.laytable;
    var layer = layui.layer;
    var form = layui.form;
    var soulTable = layui.soulTable;

    //加载日期控件
    layui.laydate.render({
        elem: '#supplier_date' ,//input的id
		 type: 'date'
    });
})
</script>



