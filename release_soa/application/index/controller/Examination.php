<?php

namespace app\index\controller;

use app\common\help\Help;
use think\Db;
use app\index\model\Examination\Examination as ex;

class Examination extends Base
{
    /**
     * 获取考试题库
     */
    public function getExamination(){

        $params = $this->input();

        $examination = new ex();
        $examinationResult = $examination->getExamination($params);

        $this->outPut($examinationResult);
    }

    /**
     * 获取
     */
    public function getOnlineExamination(){

        $params = $this->input();

        $examination = new ex();
        $examinationResult = $examination->getExaminationData($params);

        //组装数据
        $this->outPut($examinationResult);
    }

    /**
     * 添加题库和题目历史数据
     * 韩
     */
    public function addHistoryExaminationData(){

        $params = $this->input();

        $examination = new ex();
        $examinationResult = $examination->addHistoryExaminationData($params);

        $this->outPut($examinationResult);
    }
}