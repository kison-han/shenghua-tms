<?php


namespace app\index\controller\device;


use think\Controller;
use think\Validate;
use think\Db;

class Smartbox extends Controller
{

    public function test(){

        $dev_model=new \app\index\model\device\SmartBox();

        $data=Db::name("device_box_log")->where('device_id','>',31610)->select();
       // var_dump($data);
        foreach ($data as $v)
        {
            $loc_array=explode(',',$v['gps_location_old']);
            $lng=(float)$loc_array[0];
            $lat=(float)$loc_array[1];
            $lng=((float)$lng-(int)$lng + (int)($lng%100))/60 + (int)($lng/100);
            $lat=((float)$lat-(int)$lat + (int)($lat%100))/60 + (int)($lat/100);
            $gps=$lng.','.$lat;
            $data['gps_location']=$gps;
            $data['bd_location']=wgs84tobd09($lng,$lat)[0].','.wgs84tobd09($lng,$lat)[1];
            Db::name("device_box_log")->where('device_id',$v['device_id'])->update(['gps_location'=>$gps]);
            Db::name("device_box_log")->where('device_id',$v['device_id'])->update(['bd_location'=>$data['bd_location']]);


        }
        var_dump($v['gps_location_old']);

    }

    public function test1(){
        $params = request()->param();
        $date="today";
        if(isset($params['date']))
            $data=$params['date'];
        $data=Db::name("device_box_log")->where('device_id','>',31744)
           // ->whereTime('create_time','today')
            ->select();
        return json($data,256);
    }


    public function updateInfo()
    {

        $params = request()->param();

        $validate = new Validate(['device_number' => 'alphaDash|require']);
        if (!$validate->check($params)) {
            return $validate->getError();
        }

        $box_model = new  \app\index\model\device\SmartBox();
        $RES=$box_model->updateInfo($params);
        return $RES;
    }

    public function getBoxInfo(){
        $params = request()->param();
        $box_model = new  \app\index\model\device\SmartBox();
       $data=$box_model->getInfo($params);
       return json($data,256);
    }


}