<?php

namespace app\index\controller;

use app\common\help\Help;
use think\Request;

class Position extends Base
{

    /**
     *  圣华
     *  货区显示页面
     */
    public function showPosition(){

        $params = Request::instance()->param();

        $data = [
            'page'=>$this->page(),
            'page_size'=>$this->_page_size,
        ];
        $data_all = array_merge($params,$data);

        $result = $this->callSoaErp('post','/position/getPosition',$data_all);

        $this->assign('positionResult',$result['data']['list']);
        $this->getPageParams($result);

        return $this->fetch('position_manage');

    }

    /**
     *  圣华
     *  货区新增页面
     */
    public function showPositionAdd(){

        //获取仓库信息
        $data = [
            'status'=>1
        ];
        $warehouse_result = $this->callSoaErp('post', '/warehouse/getWarehouse',$data);
        $this->assign("warehouseResult",$warehouse_result['data']);

        if(is_numeric(input('position_id'))) {
            $data2 = [
                'position_id' => input('position_id'),
                'page' => $this->page(),
                'page_size' => $this->_page_size,
            ];
            //获取所有货位
            $position_result = $this->callSoaErp('post', '/position/getPosition', $data2);
            $this->assign("positionResult", $position_result['data']['list'][0]);
        }

        return $this->fetch('position_add');

    }

    /**
     * 圣华
     * 货区新增AJAX / 修改AJAX
     */
    public function addpositionAjax(Request $request)
    {

        $data = $request->param();

        if(is_numeric($data['position_id'])){
            //修改货位
            $data['user_id'] = session("user_id");
            $result = $this->callSoaErp('post', '/position/updatePositionByPositionId',$data);
            return $result;
        }else {
            //新增货位
            $data['user_id'] = session("user_id");
            $result = $this->callSoaErp('post', '/position/addPosition', $data);
            return $result; //['code' => '400', 'msg' => $data];
        }

    }
}