<?php


namespace app\index\controller;


use app\common\help\Contents;
use app\common\help\Help;
use think\Request;

class Transport extends Base
{



    public function uploadTransportAjax(){
        $params=Request::instance()->param();
        $result = $this->callSoaErp('post', '/transport/uploadTansport', $params);
        return  json($result);



    }

    //修改运单
    public function uploadTransport(){

        $params=Request::instance()->param();
        $result = $this->callSoaErp('post', '/transport/getTransportById', $params);

       // echo json_encode($result);exit();

        $supplierResult = $this->callSoaErp('post', '/source/getSupplier', $supplierData);

        $this->assign('supplierResult', $supplierResult['data']);


        //获取所有省
        $province_data = Help::getCity(0, 1);

        //获取所有市
        $city_data = Help::getCity(0, 2);
        //获取所有区
        $area_data = Help::getCity(0, 3);


        $this->assign('provinceResult', $province_data);
        $this->assign('cityResult', $city_data);
        $this->assign('areaResult', $area_data);

        $this->getPageParams($result);
        $zhongzhuan_total=0;
        $tihuo_total=0;
        foreach ($result['data']['list']['dispatch_info'] as $k=>$v){
            if($v['dispatch_type']==1){
                $zhongzhuan_total++;
            }if($v['dispatch_type']==2){
                $tihuo_total++;
            }

        }
        $this->assign('zhongzhuan_total', $zhongzhuan_total);
        $this->assign('tihuo_total', $tihuo_total);


        preg_match_all('/\d+/',$result['data']['list']['order_number'],$orders_id);


        $orderData['orders_id'] = implode(',',$orders_id[0]);


      ///  echo json_encode($orderData['orders_id'],256);exit();

        $orderData['orders_goods_id_is_like'] = 1;
        $orderGoodsResult = $this->callSoaErp('post', '/order/getOrderGoods', $orderData);
        $this->assign('orderGoodsResult', $orderGoodsResult['data']);

        return $this->fetch('upload_transport');
    }

    public function changeOrderStatus(){
        $params = Request::instance()->param();
        $order_number=$params['orders_number'];
        preg_match_all('/\d+/',$order_number,$order_number);
        $orders_id=implode(',',$order_number[0]);

        $data = [

            'orders_id'=>$orders_id

        ];


        $this->assign('orderStatus',Contents::orderStatus());
        $result = $this->callSoaErp('post', '/order/getOrder', $data);
        $result = Help::getOrder($result);
        $this->getPageParams($result);
        return $this->fetch('change_order_status');

    }

}