<?php

namespace app\index\model\source;
use think\Model;
use app\common\help\Help;
use app\index\service\PublicService;
use think\config;
use think\Db;
class City extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'city';
    private $_public_service;

    public function initialize()
    {
    	$this->_public_service = new PublicService();
    	parent::initialize();
    
    }
    public function  getAllCity(){

    return $this->where("status = 1")->select();

    }
    
    public function  aa(){
    	$result = $this->table("city_a")->
    	where("province_id>34")->select();
    	//where("parent_id>0 and is_check = 1")->select();
    	
    	for($i=0;$i<count($result);$i++){
     		$a = $this->table("city_a")->
     		where("city_id =".$result[$i]['province_id'])->find()->toArray();
     		//error_log(print_r($a,1));
    		if($a['level'] == 2 ){
    			$data['is_check'] = 2;
    			$data['province_id'] = $a['parent_id'];
    		}else{
    			$data['province_id'] = $a['parent_id'];
    		}
    	//	error_log(print_r($data,1));
     		$this->table("city_a")->where("city_id = ".$result[$i]['city_id'])->update($data);
     		
  
     		//error_log(print_r($data,1));
     		
    		
    	}
    }

    /**
     * 获取城市
     * 胡
     */
    public function getCity($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    	$data = "1=1 ";

    	if(is_numeric($params['level'])){
    		$data.= " and level = ".$params['level'];
    	}
    	if(is_numeric($params['city_id'])){
    		$data.= " and city_id = ".$params['city_id'];
    	}
    	if(is_numeric($params['parent_id'])){
    		$data.= " and parent_id = ".$params['parent_id'];
    	}
    	if(is_numeric($params['province_id'])){
    		$data.= " and province_id = ".$params['province_id'];
    	}
        if(!empty($params['city_name'])){
            $data.=' and city_name ="'.$params['city_name'].'"';
        }
	
        if($is_count==true){
            $result = $this->table("city")->
          where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("city")->
                where($data)->limit($page, $page_size)->order('city_id asc')->select();
               
               
            }else{
                $result = $this->table("city")->
                where($data)->order('city_id asc')->select();
            }
        }

        return $result;
    
    }

    /**
     * 获取城市
     * 胡
     */
    public function getProvinceCity($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
        $data = "1=1 ";

        if(is_numeric($params['level'])){
            $data.= " and level = ".$params['level'];
        }
        if(is_numeric($params['city_id'])){
            $data.= " and city_id = ".$params['city_id'];
        }
        if(is_numeric($params['parent_id'])){
            $data.= " and parent_id = ".$params['parent_id'];
        }
        if(!empty($params['city_name'])){
            $data.=' and city_name ="'.$params['city_name'].'"';
        }

        if($is_count==true){
            $result = $this->table("city")->
            where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("city")->
                where($data)->limit($page, $page_size)->order('city_id asc')->select();


            }else{
                $result = $this->table("city")->
                where($data)->order('city_id asc')->select();
            }
        }

        return $result;

    }

    /**
     * 修改客户
     */
    public function updateProjectByProjectId($params){
    
    	$t = time();
    	
    	if(!empty($params['project_name'])){
    		$data['project_name'] = $params['project_name'];
    	
    	}


    		 
    	if(isset($params['customer_id'])){
    		$data['customer_id'] = $params['customer_id'];
    	} 
        if(isset($params['zjm'])){
            $data['zjm'] = $params['zjm'];
        }
        if(isset($params['cellphone'])){
            $data['cellphone'] = $params['cellphone'];
        }
        if(isset($params['project_leader'])){
        	$data['project_leader'] = $params['project_leader'];
        }

    	if(is_numeric($params['status'])){
    		$data['status'] = $params['status'];
    		 
    	}



    	$data['update_user_id'] = $params['user_id'];   
    	$data['update_time'] = $t;

    

    	Db::startTrans();
    	try{
    		Db::name('project')->where("project_id = ".$params['project_id'])->update($data);
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    
    	}
    	return $result;
    }

    /**
     * 通过区县获取省和市
     * 韩冰
     */
    public function areaGetProvinceAndcityId($params){

        $result = $this->table("city")->where("city_id = ".$params['city_id'])->find();

        if($result['level']==3){ //查询level=2的市
            $data['city_id'] = $result['parent_id'];
            $data['level'] = 2;
            $result2 = $this->table("city")->where("city_id = ".$data['city_id']." and level = ".$data['level'])->find();

            if($result2['level']==2){ //查询level=1的省
                $data2['city_id'] = $result2['parent_id'];
                $data2['level'] = 1;
                $result3 = $this->table("city")->where("city_id = ".$data2['city_id']." and level = ".$data2['level'])->find();
            }

            //返回组装的省市区数据
            $result_data = ["province_id"=>$result3['city_id'],"city_id"=>$result2['city_id'],"area_id"=>$result['city_id']];

        }else if($result['level']==2){ //查询level=1的省
            $data2['city_id'] = $result['parent_id'];
            $data2['level'] = 1;
            $result3 = $this->table("city")->where("city_id = ".$data2['city_id']." and level = ".$data2['level'])->find();

            //返回组装的省市区数据
            $result_data = ["province_id"=>$result3['city_id'],"city_id"=>$result['city_id'],"area_id"=>0];

        }
        return $result_data;
    }

    /**
     * 通过省获取市区县数据
     * 韩冰
     */
    public function getCityAndArea($params){

        if($params['level']==2){
            $data1 = [];
            $data2 = [];

            $result = $this->table("city")->where("city_id = ".$params['city_id']." and status = 1")->find();
            $data2[0] = $result;
            if($result){
                $result_info = $this->table("city")->where("parent_id = ".$result['city_id']." and status = 1")->select();
                foreach($result_info as $k1=>$v1){
                    $result_info2[] = $this->table("city")->where("parent_id = ".$v1['city_id']." and status = 1")->select();
                    $data1 = array_reduce($result_info2, 'array_merge', array());
                }
            }

            $new_array = array_merge($result_info,$data1);
            $new_array2 = array_merge($data2,$new_array);

            return $new_array2;
        }else{
            //获取市数据
            $result = $this->table("city")->where("province_id = ".$params['city_id']." and level>1")->select();
            return $result;
        }



    }
}