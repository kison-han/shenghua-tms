<?php

namespace app\index\model\b2b_booking;
use think\Exception;
use think\Model;
use think\Db;
class B2bBookingAccommodation extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'b2b_booking_accommodation';
    public function initialize()
    {
    	parent::initialize();
    
    }


    public function addB2bBookingAccommodation($params){

        $data['b2b_booking_id'] = $params['b2b_booking_id'];
        $data['temp_group'] = $params['temp_group'];
        $data['accommodation_type'] = $params['accommodation_type'];
        $data['room_type'] = $params['room_type'];
        $data['nights'] = $params['nights'];
        $data['status'] = 1;
    	try{
    		Db::name('b2b_booking_accommodation')->insert($data);
    	} catch (\Exception $e) {

            $result = $e->getMessage();
    		\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		exit();
    	}

    }


    public function updateB2bBookingAccommodation($params)
    {

        try{
            $this->startTrans();

            $this->where(['b2b_booking_id' => $params['b2b_booking_id']])->update(['status'=>0]);
            foreach ($params['accommodation'] as $v)
            {
                $data['temp_group'] = $v['temp_group'];
                $data['seat_group'] = $v['seat_group'];
                $data['accommodation_type'] = $v['accommodation_type'];
                $data['room_type'] = $v['room_type'];
                $data['nights'] = $v['nights'];
                $data['status'] = 1;
                if ($v['b2b_booking_accommodation_id'])
                {
                    $where = " b2b_booking_accommodation_id = ". $v['b2b_booking_accommodation_id'];
                    $this->where($where)->update($data);
                }
                else
                {
                    $data['b2b_booking_id'] = $params['b2b_booking_id'];
                    $this->insert($data);
                }
            }
            $this->commit();
            $result = 1;
        } catch (\Exception $e) {
            $this->rollback();
            $result = $e->getMessage();
            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }
        return $result;
    }


    public function getB2bBookingAccommodationByBtbBookingId($b2b_booking_id)
    {
        try
        {
            $where['b2b_booking_id'] = $b2b_booking_id;
            $where['status'] = 1;
            $result = $this->table("b2b_booking_accommodation")->alias('b2b_booking_accommodation')->where($where)->select();
        }
        catch (Exception $e)
        {
            $result = $e->getMessage();
            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }

        return $result;
    }


}