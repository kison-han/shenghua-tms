<?php

namespace app\index\model\wmsmonitor;

use think\Db;
use think\Model;
use app\common\help\Help;

class TH extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'wms_th';

    /**
     * 获取温湿度点位
     * 韩
     */
    public function getTH($params,$is_count=false,$is_page=false,$page=null,$page_size=20){

        $data = "1=1 ";

        if(!empty($params['wms_th_point_uuid'])){ //uuid
            $data.=' and wms_th.wms_th_point_uuid ="'.$params['wms_th_point_uuid'].'"';
        }

        if(!empty($params['temperature'])){ //温度
            $data.=' and wms_th.temperature ="'.$params['temperature'].'"';
        }

        if(!empty($params['humidity'])){ //湿度
            $data.=' and wms_th.humidity ="'.$params['humidity'].'"';
        }

        if($params['s_time']){ //开始时间
            $s_time = strtotime($params['s_time']);
            $data .= " and wms_th.create_time >={$s_time}";
        }
        if($params['e_time']){ //结束时间
            $e_time = strtotime($params['e_time']);
            $data .= " and wms_th.create_time <={$e_time}" ;
        }

        if($is_count==true){
            $result = $this->table("wms_th")->where($data)->count();
        }else{
            if($is_page == true){
                $result = $this->table("wms_th")->
                where($data)->limit($page, $page_size)->order('wms_th.create_time desc')->
                field(["*",'from_unixtime(wms_th.create_time)'=> 'create_time'])->select();
            }else{
                $result = $this->table("wms_th")->
                where($data)->order('wms_th.create_time desc')->
                field(["*",'from_unixtime(wms_th.create_time)'=> 'create_time'])->select();
            }
        }
        return $result;
    }

    /**
     * 添加温湿度点位
     * 韩
     */
    public function addTHPoint($params){
        $t = time();

        $data['position_id'] = $params['position_id'];
        $data['th_point_name'] = $params['th_point_name'];
        $data['remark'] = $params['remark'];
        $data['wms_th_point_uuid'] = Help::getUuid(); //获取uuid
        $data['create_time'] = $t;
        $data['create_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        $data['update_user_id'] = $params['user_id'];
        $data['status'] = $params['status'];

        Db::startTrans();
        try{
            $this->table("wms_th_point")->insertGetId($data);
            $result = 1;
            // 提交事务
            Db::commit();
        }catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return $result;
    }

    /**
     * 修改温湿度点位
     * 韩
     */
    public function updateTHPointByTHPointId($params){
        $t = time();

        $data['wms_th_point_id'] = $params['wms_th_point_id'];
        $data['position_id'] = $params['position_id'];
        $data['th_point_name'] = $params['th_point_name'];
        $data['remark'] = $params['remark'];
//        $data['create_time'] = $t;
//        $data['create_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        $data['update_user_id'] = $params['user_id'];
        $data['status'] = $params['status'];

        Db::startTrans();
        try {
            Db::name('wms_th_point')->where('wms_th_point_uuid = "'.$params['wms_th_point_uuid'].'"')->update($data);

            $result = 1;
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return $result;
    }
}