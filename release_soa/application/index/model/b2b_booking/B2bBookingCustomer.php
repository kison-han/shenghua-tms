<?php

namespace app\index\model\b2b_booking;
use app\index\model\branchcompany\Customer;
use think\Exception;
use think\Model;
use think\Db;
class B2bBookingCustomer extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'b2b_booking_customer';
    public function initialize()
    {
    	parent::initialize();
    
    }


    public function addB2bBookingCustomer($params){

        $data['b2b_booking_id'] = $params['b2b_booking_id'];
        $data['seat_no'] = $params['seat_no'];
        $data['temp_id'] = $params['temp_id'];
        $data['customer_id'] = $params['customer_id'];
        $data['status'] = 1;

    	try{
    		Db::name('b2b_booking_customer')->insert($data);
    	} catch (\Exception $e) {

            $result = $e->getMessage();
    		\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		exit();
    	}

    }

    public function updateB2bBookingCustomer($params)
    {
        try{
            Db::name('b2b_booking_customer')->startTrans();
            Db::name('b2b_booking_customer')->where(['b2b_booking_id' => $params['b2b_booking_id']])->update(['status' => 0]);
            $customer_model = new Customer();
            foreach ($params['customer'] as $v)
            {
                $customer_info = $customer_model->getOneCustomerByPassport($v['passport_number']);
                $v['now_user_id'] = $params['now_user_id'];
                if ($customer_info)
                {
                    //有 修改
                    $v['customer_id'] = $customer_info['customer_id'];
                    $customer_model->updateCustomerByCustomerId($v);
                }
                else
                {
                    //无 添加
                    $customer_id = $customer_model->addCustomer($v);
                    $v['customer_id'] = $customer_id;
                }
                $data['b2b_booking_id'] = $params['b2b_booking_id'];
                $data['seat_no'] = $v['seat_no'];
                $data['customer_id'] = $v['customer_id'];
                $data['temp_id'] = $v['temp_id'];
                $data['status'] = 1;
                if ($v['b2b_booking_customer_id'])
                {
                    $where = " b2b_booking_customer_id = ". $v['b2b_booking_customer_id'];
                    Db::name('b2b_booking_customer')->where($where)->update($data);
                }
                else
                {
                    $data['b2b_booking_id'] = $params['b2b_booking_id'];
                    Db::name('b2b_booking_customer')->insert($data);
                }
            }
            Db::name('b2b_booking_customer')->commit();
            $result = 1;
        } catch (\Exception $e) {
            Db::name('b2b_booking_customer')->rollback();
            $result = $e->getMessage();
            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }
        return $result;
    }


    public function getB2bBookingCustomerByBtbBookingId($b2b_booking_id)
    {
        try
        {
            $where['b2b_booking_id'] = $b2b_booking_id;
            $where['b2b_booking_customer.status'] = 1;
            $result = $this->table("b2b_booking_customer")->alias('b2b_booking_customer')->
                join("customer", "b2b_booking_customer.customer_id = customer.customer_id")->
                field([
                'b2b_booking_customer.*', 'customer.customer_first_name', 'customer.customer_last_name', 'customer.gender',
                'customer.birthday', 'customer.age_group', 'customer.speaking_fluent', 'customer.ethnicity', 'customer.nationality',
                'customer.passport_number', 'customer.term_of_validity'
            ])->where($where)->select();
        }
        catch (Exception $e)
        {
            $result = $e->getMessage();
            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }

        return $result;
    }


}