<?php


namespace app\index\validate\transport;


use think\Validate;

class Transport extends Validate
{

    protected $rule =   [
        'order_number'=>'alphaDash|unique:transport,order_number',
        'accept_province_id'=>'alphaDash|require',
        'accept_city_id'=>'number|require',
        'accept_area_id'=>'number|require',
        'accept_address'=>'chsAlphaNum',
        'accept_name'=>"chsAlphaNum",
        'accept_cellhone'=>"chsDash",
        'send_province_id'=>"number",
        'send_city_id'=>"number",
        'send_area_id'=>"number",
        'send_address'=>"chsDash",
        'remark'=>"chsDash",
        'send_name'=>"chsDash",
        'send_cellphone'=>"chsDash",
        'create_user_id'=>"number",
        'update_user_id'=>"number",
        'create_time'=>"number|require",
        'update_time'=>"number|require",
        'status'=>"number|require"
    ];

    protected $message  =   [

    ];

    protected $scene = [
        'edit'  =>  ['order_number','accept_province_id'],
    ];

}