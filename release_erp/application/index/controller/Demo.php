<?php

namespace app\index\controller;

use app\common\help\Help;
use think\Request;

class Demo extends Base
{

    public function Demo()
    {

    }

    /**
     * excel 承运商计价规则文件上传到系统
     * 韩
     */
    public function uploadExcelFileToSystem()
    {

        $file = request()->file('file');
        $filename = $file->getInfo()['name'];

        vendor("phpexcel.PHPExcel");
        vendor("phpexcel.PHPExcel.IOFactory");

        $objPHPExcel = new \PHPExcel();

        $objPHPExcel = \PHPExcel_IOFactory::load($file->getInfo()['tmp_name']);//获取sheet表格数目
        $excel_array = $objPHPExcel->getsheet(0)->toArray();   //转换为数组格式
        array_shift($excel_array);  //删除第一个数组(标题);

//        echo "<pre>";
//        var_dump($excel_array);exit;
//        echo "</pre>";

        $data = [];
        $data_departure = [];
        foreach ($excel_array as $k => $v) {

            $data_departure2['city_name'] = $v[0];
            //转换省市区id
            $departureReturn = $this->callSoaErp('post', '/city/getCityNameToCityId', $data_departure2);
            $data[$k]['city_start_id'] = $departureReturn['data'][0]['city_id'];

            $data_departure1['city_name'] = $v[1];
            //转换省市区id
            $departureReturn = $this->callSoaErp('post', '/city/getCityNameToCityId', $data_departure1);
            $data[$k]['departure_id'] = $departureReturn['data'][0]['city_id'];

            $departureReturn22 = $this->callSoaErp('post', '/city/getCityNameToCityId', $data_departure2);
            $data[$k]['province_start_id'] = $departureReturn22['data'][0]['parent_id'];

            $data_departure3['city_name'] = $v[2];
            //转换省市区id
            $departureReturn = $this->callSoaErp('post', '/city/getCityNameToCityId', $data_departure3);
            $data[$k]['city_end_id'] = $departureReturn['data'][0]['city_id'];

            $data_departure4['city_name'] = $v[3];
            //转换省市区id
            $departureReturn = $this->callSoaErp('post', '/city/getCityNameToCityId', $data_departure4);
            $data[$k]['arrival_id'] = $departureReturn['data'][0]['city_id'];

            $departureReturn33 = $this->callSoaErp('post', '/city/getCityNameToCityId', $data_departure3);
            $data[$k]['province_end_id'] = $departureReturn33['data'][0]['parent_id'];

            //获取承运商id
            $data_suppliers['supplier_names'] = $v[4];
            $suppliersReturn = $this->callSoaErp('post', '/source/getSupplier', $data_suppliers);
            $data[$k]["supplier_id"] = $suppliersReturn['data'][0]['supplier_id'];//承运商

            //获取货物id
            $data_goods['goods_names'] = $v[5];
            $goodsReturn = $this->callSoaErp('post', '/source/getGoods', $data_goods);
            $data[$k]["goods_id"] = $goodsReturn['data'][0]['goods_id'];//货物名称

            $tags = [1 => "重量", 2 => "体积", 3 => "件数", 4 => "数量", 5 => "按单"];
            $data[$k]["billing_unit"] = array_search($v[6], $tags);//计价单位
            $data[$k]["interval_start"] = $v[7];//区间(起)
            $data[$k]["interval_end"] = $v[8];//区间(终)
            $data[$k]["interval_value"] = $v[8];//区间值
            $data[$k]["unit_price"] = $v[9];//单价
            $data[$k]["starting_price"] = $v[10];//起步价
            $data[$k]["picking_rules"] = array_search($v[11], $tags);//提货规则
            $data[$k]["picking_price"] = $v[12];//提货费
            $data[$k]["delivery_rules"] = array_search($v[13], $tags);//送货规则
            $data[$k]["delivery_price"] = $v[14];//送货费
            $data[$k]["unloading_rules"] = array_search($v[15], $tags);//装卸规则
            $data[$k]["unloading_price"] = $v[16];//装卸费

            $data_picking_name['cost_name'] = '提货费';
            $data_delivery_name['cost_name'] = '送货费';
            $data_unloading_name['cost_name'] = '装卸费';
            $pickingReturn = $this->callSoaErp('post', '/cost/getCostIdToCostName', $data_picking_name);
            $data[$k]['picking_id'] = $pickingReturn['data'][0]['cost_id'];
            $deliveryReturn = $this->callSoaErp('post', '/cost/getCostIdToCostName', $data_delivery_name);
            $data[$k]['delivery_id'] = $deliveryReturn['data'][0]['cost_id'];
            $unloadingReturn = $this->callSoaErp('post', '/cost/getCostIdToCostName', $data_unloading_name);
            $data[$k]['unloading_id'] = $unloadingReturn['data'][0]['cost_id'];

            $tags2 = [1 => "普通", 2 => "特殊"];
            $data[$k]["goods_type"] = array_search($v[17], $tags2);//货物类型
            $data[$k]["orders_type"] = array_search($v[18], $tags2);//订单类型

            $tags3 = [1 => "启用", 2 => "禁用"];
            $data[$k]["status"] = array_search($v[19], $tags3);//状态1启用0禁用

            $data[$k]["project_id"] = 0;//项目ID
            $t = time();
            $data[$k]["create_time"] = $t; //创建时间
            $data[$k]["update_time"] = $t; //更新时间
            $data[$k]['create_user_id'] = $_GET['user_id'];
            $data[$k]['update_user_id'] = $_GET['user_id'];

        }

        //匹配添加数据
        $resullt_info = $this->callSoaErp('post', '/source/addPricingConfigure', $data);
        return $resullt_info;

    }

    /**
     * excel 承运商计价规则文件上传到系统
     * 韩
     */
    public function uploadExcelProjectFileToSystem()
    {

        $file = request()->file('file');
        $filename = $file->getInfo()['name'];

        vendor("phpexcel.PHPExcel");
        vendor("phpexcel.PHPExcel.IOFactory");

        $objPHPExcel = new \PHPExcel();

        $objPHPExcel = \PHPExcel_IOFactory::load($file->getInfo()['tmp_name']);//获取sheet表格数目
        $excel_array = $objPHPExcel->getsheet(0)->toArray();   //转换为数组格式
        array_shift($excel_array);  //删除第一个数组(标题);

//        echo "<pre>";
//        var_dump($excel_array);exit;
//        echo "</pre>";

        $data = [];
        $data_departure = [];
        foreach ($excel_array as $k => $v) {
            if (!empty($v[1])) {
                $data_departure['city_name'] = $v[1];
                //转换省市区id
                $departureReturn = $this->callSoaErp('post', '/city/getCityNameToCityId', $data_departure);
                $data[$k]['departure_id'] = $departureReturn['data'][0]['city_id'];
            } else {
                $data_departure['city_name'] = $v[0];
                //转换省市区id
                $departureReturn = $this->callSoaErp('post', '/city/getCityNameToCityId', $data_departure);
                $data[$k]['departure_id'] = $departureReturn['data'][0]['city_id'];
            }

            if (!empty($v[2])) {
                $data_departure['city_name'] = $v[2];
                //转换省市区id
                $departureReturn = $this->callSoaErp('post', '/city/getCityNameToCityId', $data_departure);
                $data[$k]['arrival_id'] = $departureReturn['data'][0]['city_id'];
            } else {
                $data_departure['city_name'] = $v[3];
                //转换省市区id
                $departureReturn = $this->callSoaErp('post', '/city/getCityNameToCityId', $data_departure);
                $data[$k]['arrival_id'] = $departureReturn['data'][0]['city_id'];
            }

            //获取项目id
            $data_projects['project_names'] = $v[4];
            $projectReturn = $this->callSoaErp('post', '/source/getProject', $data_projects);
            $data[$k]["project_id"] = $projectReturn['data'][0]['project_id'];//项目

            //获取承运商id
            $data_suppliers['supplier_names'] = $v[5];
            $suppliersReturn = $this->callSoaErp('post', '/source/getSupplier', $data_suppliers);
            $data[$k]["supplier_id"] = $suppliersReturn['data'][0]['supplier_id'];//承运商

            //获取货物id
            $data_goods['goods_names'] = $v[6];
            $goodsReturn = $this->callSoaErp('post', '/source/getGoods', $data_goods);
            $data[$k]["goods_id"] = $goodsReturn['data'][0]['goods_id'];//货物名称

            $tags = [1 => "重量", 2 => "体积", 3 => "件数", 4 => "数量", 5 => "按单", 6 => "包车", 7 => "区间件数", 8 => "区间重量", 9 => "区间体积"];
            $data[$k]["billing_unit"] = array_search($v[7], $tags);//计价单位
            $data[$k]["interval_start"] = $v[8];//区间(起)
            $data[$k]["interval_end"] = $v[9];//区间(终)
            $data[$k]["unit_price"] = $v[10];//单价
            $data[$k]["starting_price"] = $v[11];//起步价
            $data[$k]["picking_rules"] = array_search($v[12], $tags);//提货规则
            $data[$k]["picking_price"] = $v[13];//提货费
            $data[$k]["delivery_rules"] = array_search($v[14], $tags);//送货规则
            $data[$k]["delivery_price"] = $v[15];//送货费
            $data[$k]["unloading_rules"] = array_search($v[16], $tags);//卸货规则
            $data[$k]["unloading_price"] = $v[17];//卸货费

            $tags2 = [1 => "普通", 2 => "特殊"];
            $data[$k]["goods_type"] = array_search($v[18], $tags2);//货物类型
            $data[$k]["orders_type"] = array_search($v[19], $tags2);//订单类型

            $tags3 = [1 => "启用", 2 => "禁用"];
            $data[$k]["status"] = array_search($v[20], $tags3);//状态1启用0禁用

            $t = time();
            $data[$k]["create_time"] = $t; //创建时间
            $data[$k]["update_time"] = $t; //更新时间
            $data[$k]['create_user_id'] = $_GET['user_id'];
            $data[$k]['update_user_id'] = $_GET['user_id'];

        }

        //匹配添加数据
        $resullt_info = $this->callSoaErp('post', '/source/addPricingConfigureProjectAll', $data);
        return $resullt_info;

    }

    /**
     * 多选案例
     * 韩
     */
    public function formSelects()
    {
        return $this->fetch('formselects');
    }

    /**
     * 文件上传案例-界面
     * 韩
     */
    public function showUpload()
    {
        return $this->fetch('upload');
    }

    /**
     * 文件上传案例-执行
     * 韩
     */
    public function uploadFile()
    {

        $file = request()->file('file');

        if ($file) {
            $file_result = $file->getInfo();
            $info = $file->move(ROOT_PATH . 'public' . DS . 'static' . DS . 'uploads' . DS . 'images');
            $image_name = $file_result['name'];
            //获取后缀名
            $image_name = substr($image_name, strpos($image_name, '.') + 1);
            $temp_name = $file_result['tmp_name'];
            $temp_name = str_replace('tmp', $image_name, $temp_name);
            $url = config('soaupload')['ip'] . ':' . config('soaupload')['port'];
            $result = help::curlImages($info->getPathname(), $url . "/index/uploadImages");

            if ($result) {
                $result = json_decode($result, true);
                $result['get'] = $_GET;
                $result['name'] = $file_result['name'];
                return json_encode($result);

            } else {
                $d['data'] = '/static/uploads/images/' . $info->getSaveName();
                $d['name'] = $file_result['name'];
                $d['code'] = 200;
                $d['get'] = $_GET;
                echo json_encode($d);
            }
        } else {
            // 上传失败获取错误信息
            echo $file->getError();
        }

    }


    /**
     * 文件上传案例-执行
     * 韩
     */
    public function uploadFileZip()
    {

        $file = request()->file('file');

        if ($file) {
            $file_result = $file->getInfo();
            $info = $file->move(ROOT_PATH . 'public' . DS . 'static' . DS . 'uploads' . DS . 'zips');
            $image_name = $file_result['name'];

            //获取后缀名
            $image_name = substr($image_name, strpos($image_name, '.') + 1);
            $temp_name = $file_result['tmp_name'];
            $temp_name = str_replace('tmp', $image_name, $temp_name);
            $url = config('soaupload')['ip'] . ':' . config('soaupload')['port'];
            $result = help::curlImages($info->getPathname(), $url . "/index/uploadImages");

            if ($result) {
                $result = json_decode($result, true);
                $result['get'] = $_GET;
                $result['name'] = $file_result['name'];
                return json_encode($result);

            } else {
                $d['data'] = '/static/uploads/zips/' . $info->getSaveName();
                $d['name'] = $file_result['name'];
                $d['code'] = 200;
                $d['get'] = $_GET;
                echo json_encode($d);
            }
        } else {
            // 上传失败获取错误信息
            echo $file->getError();
        }

    }


    public function imgUploadFile()
    {

        $file = request()->file('file');

        if ($file) {
            $file_result = $file->getInfo();
            $info = $file->move(ROOT_PATH . 'public' . DS . 'static' . DS . 'uploads' . DS . 'images');

            $image_name = $file_result['name'];
            //获取后缀名
            $image_name = substr($image_name, strpos($image_name, '.') + 1);


            $temp_name = $file_result['tmp_name'];
            $temp_name = str_replace('tmp', $image_name, $temp_name);
            $url = config('soaupload')['ip'] . ':' . config('soaupload')['port'];


            $result2 = help::curlImages($info->getPathname(), $url . "/index/uploadImages");

            if ($result2) {
                $result2 = json_decode($result2, true);
                $result['code'] = 0;
                $result['msg'] = '';
                $result['data']['src'] = $result2['data'];
                $result['data']['title'] = $_GET;

                return json_encode($result);
            } else {
                $d['data'] = '/static/uploads/images/' . $info->getSaveName();
                $d['code'] = 200;
                $d['get'] = $_GET;
                echo json_encode($d);
            }
        } else {
            // 上传失败获取错误信息
            echo $file->getError();
        }

    }


    public function uploadOtaFile()
    {

        $file = request()->file('file');

        if ($file) {
            $file_result = $file->getInfo();
            $info = $file->move(ROOT_PATH . 'public' . DS . 'static' . DS . 'uploads' . DS . 'images');

            $image_name = $file_result['name'];
            //获取后缀名
            $image_name = substr($image_name, strpos($image_name, '.') + 1);


            $temp_name = $file_result['tmp_name'];
            $temp_name = str_replace('tmp', $image_name, $temp_name);
            $url = config('soaupload')['ip'] . ':' . config('soaupload')['port'];


            $result = help::curlImages($info->getPathname(), $url . "/index/uploadImages");


            $a = json_decode($result, true);


            $data['url'] = $a['data'];
            $data['size'] = $file_result['size'];
            $data['choose_company_id'] = session('user')['company_id'];
            $data['website_uuid'] = session('website_uuid');
            $data['user_id'] = session('user')['user_id'];
            /* $this->callSoaErp('post','/ota_system/addOtaMediaPool',$data);



             if($result){
                 $result = json_decode($result,true);
                 $result['get'] = $_GET;
                 return json_encode($result);

             }else*/

            {
                $d['data'] = '/static/uploads/images/' . $info->getSaveName();
                $d['code'] = 200;
                $d['get'] = $_GET;
                echo json_encode($d);
            }
        } else {


            // 上传失败获取错误信息
            echo $file->getError();
        }

    }


    public function uploadImages()
    {

        $file = request()->file('file');

        if ($file) {
            $file_result = $file->getInfo();
            $info = $file->move(ROOT_PATH . 'public' . DS . 'static' . DS . 'uploads' . DS . 'images');
            $image_name = $file_result['name'];
            //获取后缀名
            $image_name = substr($image_name, strpos($image_name, '.') + 1);
            $temp_name = $file_result['tmp_name'];
            $temp_name = str_replace('tmp', $image_name, $temp_name);
            $url = config('soaupload')['ip'] . ':' . config('soaupload')['port'];


            $result = help::curlImages($info->getPathname(), $url . "/index/uploadImages");

            if ($result) {
                $result = json_decode($result, true);
                $result['code'] = 0;
                $result['msg'] = "ok";
                $result['file'] = $result['data'];
//                $result['size'] = $file['size'];
                return json_encode($result);

            } else {
                $d['data'] = '/static/uploads/images/' . $info->getSaveName();
                $d['code'] = 1;
                $d['msg'] = "false";
                $d['file'] = $d['data'];
                echo json_encode($d);
            }
        } else {
            // 上传失败获取错误信息
            echo $file->getError();
        }

    }

    /*
     * 导出收货方
     */
    public static function export_accept_goods($xAxis, $title1, $header1, $data1, $name, $filter = [])
    {
        ini_set('max_execution_time', '0');

        date_default_timezone_set('Europe/London');

        vendor('PHPExcel.PHPExcel');

        vendor("phpexcel.PHPExcel.IOFactory");
        array_unshift($data1, $header1);

//        var_dump($data1);exit;

        $cellName = [
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'Q',
            'R',
            'S',
            'T',
            'U',
            'V',
            'W',
            'X',
            'Y',
            'Z',
            "AA",
            "AB",
            "AC",
            "AD",
            "AE",
            "AF",
            "AG",
            "AH",
			"AI"
        ];

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getActiveSheet()->setTitle($title1);

        /* @func 设置文档基本属性 */
        $obpe_pro = $objPHPExcel->getProperties();
        $obpe_pro->setCreator('baiqiu')//设置创建者
        ->setLastModifiedBy('2022/07/05')//设置时间
        ->setTitle('data')//设置标题
        ->setSubject('beizhu')//设置备注
        ->setDescription('miaoshu')//设置描述
        ->setKeywords('keyword')//设置关键字 | 标记
        ->setCategory('catagory');//设置类别

        /* 设置宽度 */
        //$obpe->getActiveSheet()->getColumnDimension()->setAutoSize(true);
        //$obpe->getActiveSheet()->getColumnDimension('B')->setWidth(10);

        //设置当前sheet索引,用于后续的内容操作
        //一般用在对个Sheet的时候才需要显示调用
        //缺省情况下,PHPExcel会自动创建第一个SHEET被设置SheetIndex=0

        //设置居中
        //        $objPHPExcel->getActiveSheet()->getStyle('A10')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        // 合并
        //        $objPHPExcel->getActiveSheet()->mergeCells('A10:G10');

        //        $objPHPExcel->setactivesheetindex(0)->setCellValue('A10', $title1);

        $h = 0;

        foreach ($xAxis as $v) {

            $objPHPExcel->getactivesheet()->setcellvalue('A' . $h, $v);
            //  $objPHPExcel->getactivesheet()->setcellvalue($v['code'], $v['value']);
            // $objPHPExcel->getactivesheet()->setcellvalue('A' . $h, $v);
            //  $objPHPExcel->getActiveSheet()->getStyle('A' . $h)->getNumberFormat()->setFormatCode('00');
            $h += 2;
        }

        foreach ($cellName as $k => $v) {
            /*if($v==$filter[$k]['code']){
                $objPHPExcel->getActiveSheet()->getColumnDimension($v)->setWidth($filter[$k]['value']);
            }*/
            // $objPHPExcel->getActiveSheet()->getStyle($v)->getNumberFormat()->setFormatCode('G');
        }
        $new_rule = [];
        foreach ($xAxis as $v) {

            $objPHPExcel->getactivesheet()->setcellvalue('A' . $h, $v);
          //  $objPHPExcel->getactivesheet()->setcellvalue($v['code'], $v['value']);
           // $objPHPExcel->getactivesheet()->setcellvalue('A' . $h, $v);
          //  $objPHPExcel->getActiveSheet()->getStyle('A' . $h)->getNumberFormat()->setFormatCode('00');
            $h += 2;
        }

        foreach ($cellName as $k=>$v)
        {
            /*if($v==$filter[$k]['code']){
                $objPHPExcel->getActiveSheet()->getColumnDimension($v)->setWidth($filter[$k]['value']);
            }*/
            $objPHPExcel->getActiveSheet()->getStyle($v)->getNumberFormat()->setFormatCode('@');
        }
        foreach ($filter as $k=>$v){
            if(in_array($v['code'],$cellName))
            $objPHPExcel->getActiveSheet()->getColumnDimension($v['code'])->setWidth($v['value']);

        }


        //写入多行数据
        foreach ($data1 as $k => $v) {
            $h++;
            /* @func 设置列 */
            $kk = 0;
            foreach ($v as $vv) {

                $objPHPExcel->getactivesheet()->setcellvalue($cellName[$kk] . $h, strval($vv));
                $kk++;
            }
        }


        //写入类容
        $obwrite = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control:must-revalidate,post-check=0,pre-check=0');
        header('Content-Type:application/force-download');
        header('Content-Type:application/vnd.ms-execl');
        header('Content-Type:application/octet-stream');
        header('Content-Type:application/download');
        header('Content-Disposition: attachment;filename="' . $name . '.xls"');
        header('Content-Transfer-Encoding:binary');

        $obwrite->save('php://output');
        exit;
    }

}