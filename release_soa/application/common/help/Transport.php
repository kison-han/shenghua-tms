<?php


namespace app\common\help;

use app\index\controller\Order;
use  think\db;
use app\index\model\orders\Orders;
use app\index\model\transport\Transport as Transmodel;
use app\index\model\orders\Ordersgoods;
use app\index\model\source\Supplier;
use think\Request;

class Transport
{

    public  static function filter_fee($params){


        //处理配送
        $data=[];
       foreach ($params['money_peisong'] as $k=>$v)
       {
           $data[]=['money'=>$v,
               'fee_id'=>$params['fee_id_peisong'][$k],
               'tax_rate'=>$params['tax_rate_peisong'][$k],
               'true_money'=>$params['true_money_peisong'][$k],
               'remark'=>$params['remark_peisong'][$k],

           ];

       }
       $fee['peisong']=$data;
       //处理干线
       $data=[];
       foreach ($params['money_ganxian'] as $k=>$v)
       {
           $data[]=['money'=>$v,
               'fee_id'=>$params['fee_id_ganxian'][$k],
               'tax_rate'=>$params['tax_rate_ganxian'][$k],
               'true_money'=>$params['true_money_ganxian'][$k],
               'remark'=>$params['remark_ganxian'][$k],

           ];

       }
       $fee['ganxian']=$data;
       //处理提货
       $data=[];
       foreach ($params['money_tihuo'] as $k=>$v)
       {
           foreach ($params['money_tihuo'][$k] as $k1=>$v1)
               //array_push($data[$k],);
           $data[$k][]=['money'=>$v1,
               'fee_id'=>$params['fee_id_tihuo'][$k][$k1],
               'tax_rate'=>$params['tax_rate_tihuo'][$k][$k1],
               'true_money'=>$params['true_money_tihuo'][$k][$k1],
               'remark'=>$params['remark_tihuo'][$k][$k1],

           ];

       }
       $fee['tihuo']=$data;

       //处理中转
       $data=[];
       foreach ($params['money_zhongzhuan'] as $k=>$v)
       {
           foreach ($params['money_zhongzhuan'][$k] as $k1=>$v1)
           $data[$k][]=['money'=>$v1,
               'fee_id'=>$params['fee_id_zhongzhuan'][$k][$k1],
               'tax_rate'=>$params['tax_rate_zhongzhuan'][$k][$k1],
               'true_money'=>$params['true_money_zhongzhuan'][$k][$k1],
               'remark'=>$params['remark_zhongzhuan'][$k][$k1],

           ];

       }
       $fee['zhongzhuan']=$data;
       //处理整车
       $data=[];
       foreach ($params['money_zhengche'] as $k=>$v)
       {
           foreach ($params['money_zhengche'][$k] as $k1=>$v1)
           $data[$k][]=['money'=>$v1,
               'fee_id'=>$params['fee_id_zhengche'][$k][$k1],
               'tax_rate'=>$params['tax_rate_zhengche'][$k][$k1],
               'true_money'=>$params['true_money_zhengche'][$k][$k1],
               'remark'=>$params['remark_zhengche'][$k][$k1],

           ];

       }
       $fee['zhengche']=$data;
       return $fee;
       error_log(json_encode($fee,256));exit();






    }

    public static function filter_zhongzhuan($param)
    {

        if(!isset($param['zhongzhuan_supplier']))
            return false;

        $data = [];


        $acceptSupplierInfo = Supplier::get(current($param['tihuo_supplier']));
        foreach ($param['zhongzhuan_supplier'] as $k => $v) {
            $tihuoSupplierInfo = Supplier::get($v);

            $data[] = ['supplier_id' => $v,
                'dispatch_type' => 1,
                'accept_province_id' => current($param['zhongzhuan_accept_province_id']),
                'accept_city_id' => current($param['zhongzhuan_accept_city_id']),
                'accept_area_id' => current($param['zhongzhuan_accept_area_id']),
                'accept_address' => current($param['zhongzhuan_accept_address']),
                'accept_name' => $acceptSupplierInfo->supplier_name,
                'accept_cellphone' => $acceptSupplierInfo->phone,
                'send_province_id' => $param['zhongzhuan_send_province_id'][$k],
                'send_city_id' => $param['zhongzhuan_send_city_id'][$k],
                'send_area_id' => $param['zhongzhuan_send_area_id'][$k],
                'send_address' => $param['zhongzhuan_send_address'][$k],
                'remark' => $param['zhongzhuan_remark'][$k],
                'send_name' => $tihuoSupplierInfo->supplier_name,
                'send_cellphone' => $tihuoSupplierInfo->phone,
                'pickup_time' => strtotime($param['zhongzhuan_pickup_date'][$k]),
                'send_time' => strtotime($param['zhongzhuan_send_date'][$k]),
                'create_user_id' => $param['user_id'],
                'update_user_id' => $param['user_id'],
                'create_time' => time(),
                'update_time' => time(),
                'status' => 1,
                'goods' => ['order_number' => $param['zhongzhuan_huowu_name'][$k],
                    'goods_number' => $param['zhongzhuan_huowu_number'][$k],
                    'goods_pack_number' => $param['zhongzhuan_huowu_pack_number'][$k],
                    'goods_weight' => $param['zhongzhuan_huowu_weight'][$k],
                    'goods_volume' => $param['zhongzhuan_huowu_volume'][$k],
                    'user_id' => $param['user_id'],

                ]
            ];
        }




        return $data;
    }

    public function goods2transNum($goods)
    {
        $orderids = [];
        if (isset($goods['order_number'])) {
            foreach ($goods['order_number'] as $k => $v) {
                $orderids[] = 'O-' . explode('-', $v)[0];
            }
        } else {
            foreach ($goods as $k => $v) {
                $orderids[] = 'O-' . $v['orders_id'];
            }
        }

        $transportnum = Transmodel::whereIn('order_number', $orderids)->column('transport_number');

        return implode(',', $transportnum);
    }


    public function filter_tihuo($param)
    {


        if(!isset($param['accept_city_id']) && !isset($param['tihuo_accept_city_id']))
            return false;

        $data = [];

        if (!isset($param['pickup_order_add_type'])) {
            $acceptSupplierInfo = Supplier::get($param['ganxian_supplier']);
            foreach ($param['tihuo_supplier'] as $k => $v) {
                $tihuoSupplierInfo = Supplier::get($v);

                $data[] = ['supplier_id' => $v,
                    'dispatch_type' => 2,
                    'accept_province_id' => current($param['tihuo_accept_province_id']),
                    'accept_city_id' => current($param['tihuo_accept_city_id']),
                    'accept_area_id' => current($param['tihuo_accept_area_id']),
                    'accept_address' => current($param['tihuo_accept_address']),
                    'accept_name' => $acceptSupplierInfo->supplier_name,
                    'accept_cellphone' => $acceptSupplierInfo->phone,
                    'send_province_id' => $param['tihuo_send_province_id'][$k],
                    'send_city_id' => $param['tihuo_send_city_id'][$k],
                    'send_area_id' => $param['tihuo_send_area_id'][$k],
                    'send_address' => $param['tihuo_send_address'][$k],
                    'remark' => $param['tihuo_remark'][$k],
                    'send_name' => $tihuoSupplierInfo->supplier_name,
                    'send_cellphone' => $tihuoSupplierInfo->phone,
                    'pickup_time' => strtotime($param['tihuo_pickup_date'][$k]),
                    'send_time' => strtotime($param['tihuo_send_date'][$k]),
                    'create_user_id' => $param['user_id'],
                    'update_user_id' => $param['user_id'],
                    'create_time' => time(),
                    'update_time' => time(),
                    'status' => 1,
                    'goods' => ['order_number' => $param['tihuo_huowu_name'][$k],
                        'goods_number' => $param['tihuo_huowu_number'][$k],
                        'goods_pack_number' => $param['tihuo_huowu_pack_number'][$k],
                        'goods_weight' => $param['tihuo_huowu_weight'][$k],
                        'goods_volume' => $param['tihuo_huowu_volume'][$k],
                        'user_id' => $param['user_id'],

                    ]
                ];
            }
        } else {

            foreach ($param['supplier_id'] as $k => $v) {
                $orderId = preg_replace('/\D+/', '', $param['orders_number'][$k]);
                $orderInfo = Orders::get($orderId);
                $Ordersgoodsmodel = new Ordersgoods();
                $Ordersgoodslist = $Ordersgoodsmodel->field("goods_id,orders_id,realy_count as goods_number,realy_pack_count as goods_pack_number,realy_weight as goods_weight,realy_volume as goods_volume")->where('orders_id', $orderId)->select();

                foreach ($Ordersgoodslist as $k1 => $v1) {
                    $Ordersgoodslist[$k1]['user_id'] = $param['user_id'];
                }


                $data[] = [
                    'supplier_id' => $param['supplier_id'][$k],
                    // 'orders_number'=>$param['order_number'][$k],
                    'dispatch_type' => 5,
                    'accept_province_id' => $param['accept_province_id'][$k],
                    'accept_city_id' => $param['accept_city_id'][$k],
                    'accept_area_id' => $param['accept_area_id'][$k],
                    'accept_address' => $param['accept_address'][$k],
                    'accept_name' => $orderInfo->accept_name,
                    'accept_cellphone' => $orderInfo->accept_cellphone,
                    'send_province_id' => $param['send_province_id'][$k],
                    'send_city_id' => $param['send_city_id'][$k],
                    'send_area_id' => $param['send_area_id'][$k],
                    'send_address' => $param['send_address'][$k],
                    'send_name' => $orderInfo->send_name,
                    'send_cellphone' => $orderInfo->send_cellphone,
                    'pickup_time' => strtotime($param['pickup_time'][$k]),
                    'send_time' => strtotime($param['send_time'][$k]),
                    'remark' => $param['remark'][$k],
                    'create_user_id' => $param['user_id'],
                    'update_user_id' => $param['user_id'],
                    'create_time' => time(),
                    'update_time' => time(),
                    'status' => 1,
                    'goods' => $Ordersgoodslist

                ];
            }


        }


        return $data;
    }


    public static function filter_ganxian($param)
    {
        if(!isset($param['ganxian_supplier']))
        return false;

        $acceptSupplierInfo = Supplier::get($param['peisong_supplier']);
        $sendSupplierInfo = Supplier::get($param['ganxian_supplier']);

        preg_match('/\d+/', $param['ganxian_order_ids'], $orderIds);

        $Ordersgoodsmodel = new Ordersgoods();
        $Ordersgoodslist = $Ordersgoodsmodel->field("goods_id,orders_id,realy_count as goods_number,realy_pack_count as goods_pack_number,realy_weight as goods_weight,realy_volume as goods_volume")->whereIn('orders_id', $orderIds)->select();

        foreach ($Ordersgoodslist as $k1 => $v1) {
            $Ordersgoodslist[$k1]['user_id'] = $param['user_id'];
        }



        $data = [
            'supplier_id' => $param['ganxian_supplier'],
            //'order_number'=>$param['ganxian_order_ids'],
            'dispatch_type' => 3,
            'accept_province_id' => $param['ganxian_accept_province_id'],
            'accept_city_id' => $param['ganxian_accept_city_id'],
            'accept_area_id' => $param['ganxian_accept_area_id'],
            'accept_address' => $param['ganxian_accept_address'],
            'accept_name' => $acceptSupplierInfo->supplier_name,
            'accept_cellphone' => $acceptSupplierInfo->cellphone,
            'send_province_id' => $param['send_province_id'],
            'send_city_id' => $param['ganxian_send_city_id'],
            'send_area_id' => $param['ganxian_send_area_id'],
            'send_address' => $param['ganxian_send_address'],
            'remark' => $param['ganxian_remark'],
            'send_name' => $sendSupplierInfo->supplier_name,
            'send_cellphone' => $sendSupplierInfo->cellphone,
            'pickup_time' => strtotime($param['ganxian_pickup_date']),
            'send_time' => strtotime($param['ganxian_send_date']),
            'create_user_id' => $param['user_id'],
            'update_user_id' => $param['user_id'],
            'create_time' => time(),
            'update_time' => time(),
            'status' => 1,
            'goods'=>$Ordersgoodslist

        ];

        return $data;
    }

    public static function filter_peisong($param)
    {
        if(!isset($param['peisong_supplier']))
            return false;



        $ordermodel = new Orders();
        preg_match('/\d+/', $param['peisong_order_ids'], $orderIds);
        $acceptSupplierInfo = $ordermodel->whereIn('orders_id', $orderIds)->find();

        $sendSupplierInfo = Supplier::get($param['peisong_supplier']);


        $Ordersgoodsmodel = new Ordersgoods();
        $Ordersgoodslist = $Ordersgoodsmodel->field("goods_id,orders_id,realy_count as goods_number,realy_pack_count as goods_pack_number,realy_weight as goods_weight,realy_volume as goods_volume")->whereIn('orders_id', $orderIds)->select();

        foreach ($Ordersgoodslist as $k1 => $v1) {
            $Ordersgoodslist[$k1]['user_id'] = $param['user_id'];
        }

        $data = [
            'supplier_id' => $param['peisong_supplier'],
            //'order_number'=>$param['peisong_order_ids'],
            'dispatch_type' => 4,
            'accept_province_id' => $param['peisong_accept_province_id'],
            'accept_city_id' => $param['peisong_accept_city_id'],
            'accept_area_id' => $param['peisong_accept_area_id'],
            'accept_address' => $param['peisong_accept_address'],
            'accept_name' => $acceptSupplierInfo->accept_name,
            'accept_cellphone' => $acceptSupplierInfo->accept_cellphone,
            'send_province_id' => $param['peisong_send_province_id'],
            'send_city_id' => $param['peisong_send_city_id'],
            'send_area_id' => $param['peisong_send_area_id'],
            'remark' => $param['peisong_remark'],
            'send_address' => $param['peisong_send_address'],
            'send_name' => $sendSupplierInfo->supplier_name,
            'send_cellphone' => $sendSupplierInfo->cellphone,
            'pickup_time' => strtotime($param['peisong_pickup_date']),
            'send_time' => strtotime($param['peisong_send_date']),
            'create_user_id' => $param['user_id'],
            'update_user_id' => $param['user_id'],
            'create_time' => time(),
            'update_time' => time(),
            'status' => 1,
            'goods'=>$Ordersgoodslist

        ];

        return $data;
    }

    public static function filter_transport($param)
    {

        $orders_number=[];
        if(isset($param['pickup_order_add_type'])){
            foreach ($param['orders_number'] as $k=>$v)
                $orders_number[]=$v;
        }
        else
            $orders_number=explode(',',$param['peisong_order_ids']);

        $orderInfo = Orders::whereIn('orders_number', $orders_number)->select();
        $transportType=isset($param['pickup_order_add_type'])?1:2;
        foreach ($orderInfo as $k => $v) {
            $data[] = [
                "transport_type" => $transportType,
                "order_number" => $v['orders_number'],
                'accept_province_id' => $v['accept_province_id'],
                'accept_city_id' => $v['accept_city_id'],
                'accept_area_id' => $v['accept_area_id'],
                'accept_address' => $v['accept_address'],
                'accept_name' => $v['accept_name'],
                'accept_cellphone' => $v['accept_cellphone'],
                'send_province_id' => $v['send_province_id'],
                'send_city_id' => $v['send_city_id'],
                'send_area_id' => $v['send_area_id'],
                'send_address' => $v['send_address'],
                'remark' => $v['remark'],
                'send_name' => $v['send_name'],
                'send_cellphone' => $v['send_cellphone'],
                'create_user_id' => $param['user_id'],
                'update_user_id' => $param['user_id'],
                'create_time' => time(),
                'update_time' => time(),
                'status' => 1
            ];
        }

        return $data;
    }

}