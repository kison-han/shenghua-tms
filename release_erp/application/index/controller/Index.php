<?php
namespace app\index\controller;
use app\common\help\Help;
use app\index\controller\Base;
use think\Request;
use think\Session;
use think\Cache;
class Index extends Base
{
    public function __initialize()
    {
	
    }

    public function test(){


        return $this->fetch('index');
    }
    public function index()
    {

		
		if(!empty(Cache::get('dashboard_top'))){
		
			$result = Cache::get('dashboard_top');
		}else{	
		
			$result = [];
			
			//先获取当月的数据
			$month['month']=time();
			//获取收入

			$orders_money_now_month = $this->callSoaErp('post','/order/getIndexOrder',$month);
			$orders_money_now_month = (float) $orders_money_now_month['data'][0]['sum_orders_money'];
			
			//获取赔款
			$orders_abnormal_money_now_month = $this->callSoaErp('post','/order/getIndexOrderAbnormal',$month);
			$orders_abnormal_money_now_month = (float) $orders_abnormal_money_now_month['data'][0]['sum_orders_abnormal_money'];
		
			//获取发运费
			$shipment_money_now_month = $this->callSoaErp('post','/shipment/getIndexShipment',$month);
			$shipment_money_now_month = (float) $shipment_money_now_month['data'][0]['sum_shipment_money'];		

			//获取理赔
			$shipment_abnormal_money_now_month = $this->callSoaErp('post','/shipment/getIndexShipmentAbnormal',$data);
			$shipment_abnormal_money_now_month = (float) $shipment_abnormal_money_now_month['data'][0]['sum_shipment_abnormal_money'];				
			
			//获取收入
			$year['year']=time();
			$orders_money = $this->callSoaErp('post','/order/getIndexOrder',$year);
			$orders_money = (float) $orders_money['data'][0]['sum_orders_money'];
			
			//获取赔款
			$orders_abnormal_money = $this->callSoaErp('post','/order/getIndexOrderAbnormal',$year);
			$orders_abnormal_money = (float) $orders_abnormal_money['data'][0]['sum_orders_abnormal_money'];
			
			//获取发运费
			$shipment_money = $this->callSoaErp('post','/shipment/getIndexShipment',[]);
			$shipment_money = (float) $shipment_money['data'][0]['sum_shipment_money'];		
			//获取理赔
			$shipment_abnormal_money = $this->callSoaErp('post','/shipment/getIndexShipmentAbnormal',$year);
			$shipment_abnormal_money = (float) $shipment_abnormal_money['data'][0]['sum_shipment_abnormal_money'];			
			
			$result['now_month_shouru'] = $orders_money_now_month+$shipment_abnormal_money_now_month;
			$result['now_month_chengben'] = $orders_abnormal_money_now_month+$shipment_money_now_month;
			$result['now_month_jingshouru'] =$result['now_month_shouru']-$result['now_month_chengben'];
			$result['shouru'] = $orders_money+$shipment_abnormal_money;
			$result['chengben'] = $orders_abnormal_money+$shipment_money;
			$result['jingshouru'] =$result['shouru']-$result['chengben'];
			Cache::set('dashboard_top',$result,60);
		}
		$this->assign('result',$result);
        return $this->fetch('index');
    }

    public function index2()
    {
        return $this->fetch('index2');
    }

    public function setUserConfigAjax(){
        $data = Request::instance()->param();
        $res=$this->callSoaErp('post','/index/setUserConfig',$data);
      return $res;
    }
    public function test1(){
        //测试语言
        /*
        $data=[

            'name_zh-cn'=>'中文',
            'user_id'=>123,
            'name_en-us'=>'chinese',
            'lang'=>'zh-cn',
            'language_id'=>1

        ];
        */

        //测试货币
        $data = [
            /*
                'name_zh-cn'=>'人名币',
                'user_id'=>213,
                'name_en-us'=>'rmb',
                'symbol'=>'¥',
                'unit'=>'cny',¥
                */
            //'symbol'=>'¥',
            'user_id'=>1423,
            'language_id'=>1

        ];
        $result =  $this->callSoaErp('post','/config/updateLanguageByLanguageId',$data);
        return json_encode($result);
    }

    public function changeStatus(){
        $params = Request::instance()->param();
		error_log(print_r($params,1));
        $table_name = input("table_name");



        $table_id = $params['table_id'];
        $table_id_name = input("table_id_name");
        $status = input("status");
		$field=(input('field'))?input('field'):'status';
		
        $data = [
            "table_name"=>"$table_name",
            "table_id"=>$table_id,
            'table_id_name'=>"$table_id_name",
            'status'=>$status,
			'field'=>$field
        ];

        return $this->callSoaErp('post','/index/changeStatus',$data);
    }
}
