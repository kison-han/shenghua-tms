<?php
namespace app\index\service;

use app\index\model\system\Tax;
use app\index\model\branchcompany\CompanyOrder;
use app\index\model\branchcompany\CompanyOrderCustomer;
use app\index\model\branchcompany\CompanyOrderProduct;
use app\index\model\branchcompany\CompanyOrderProductTeam;
use app\index\model\branchcompany\CompanyOrderDiy;
use app\index\model\branchcompany\CompanyOrderProductSource;
use app\index\model\branchcompany\CompanyOrderRelation;
use app\index\model\branchcompany\CompanyOrderProductDiy;

use app\index\model\source\OwnExpense;
use app\index\model\publicmodel\Common;
use think\Model;
use app\common\help\Help;
use think\Hook;

class PublicService{
	
	
	private $_company_order_product;
	private $_company_order_customer;
	private $_company_order_product_team;
	private $_company_order_product_source;
	private $_company_order_product_diy;
	private $_hotel;
	private $_team_product;
	private $_team_product_allocation;
	private $_common;
	private $_route_type;
	private $_company;
	public function __construct(){
		

		$this->_common = new Common();
	
	}

	/**
	 * 设置表的编号
	 * @param unknown $table_name 表明
	 * @param unknown $major_key 主键
	 * @param unknown $major_key_val 主键值
	 * @param unknown $number_field  编号的字段名称
	 * @param unknown $prefix 前缀
	 * @param unknown $number 编号
	 */
	public function setNumber($table_name,$major_key,$major_key_val,$number_field,$prefix,$number){
		
		$number = $number;

		$this->_common->setNumber($table_name, $major_key, $major_key_val, $number_field,$number);
		return $number;
	}
	
	/**
	 * 通过线路类型ID获取父级递归的编码
	 */
	public function getRouteTypeRecursion($route_type_id){
		$route_type_params = [
			'route_type_id'=>$route_type_id	
		];
		$number=[];
		
		while($result = $this->_route_type->getOneRouteType($route_type_params)){
			
			$number[]=$result['route_type_code'];
			$route_type_params = [
				'route_type_id'=>$result['pid']	
			];
			$result = $this->_route_type->getOneRouteType($route_type_params);
			
		}
		//$number = strrev($number);
	
		$number_string = '';
		for($i=count($number);$i>=0;$i--){
			$number_string.=$number[$i];
		}
		
		return $number_string;
	}




}