<?php


namespace app\index\model\orders;


use app\index\model\Base;

use think\Db;
class OrdersCustomerFollow extends Base
{
    protected $table="orders_follow";


    /**
     * 获取客户财务信息
     * 胡
     */
    public function getOrdersCustomerFollow($params,$is_count=false,$is_page=true,$page=null,$page_size=20){

        $data = "1=1 ";


        if(isset($params['orders_number'])){ //状态
        	$data.= " and orders_number = '". trim($params['orders_number'])."'";
        }

        $result = $this->table("orders_customer_follow")->where($data)->select();


       
    
        return  $result;
    }

    //修改
    public function updateOrdersCustomerFollow($params){
    	$t = time();
    		
    		
    	if(isset($params['orders_customer_follow_content'])){
    		$data['orders_customer_follow_content'] = $params['orders_customer_follow_content'];
    	}
    	$data['update_time'] = $t;
    	$data['update_user_id'] = $params['user_id'];


    $orders_number = $params['orders_number'];
    $orderModel = new Orders();
    $orderInfo = $orderModel->where('orders_number', $orders_number)->find();
    if ($orderInfo) {

        $orderInfo->update_time = time();
        $orderInfo->update_user_id = $params['user_id'];
        $orderInfo->save();
    }

    	Db::startTrans();
    	try {
    		Db::name('orders_customer_follow')->where("orders_customer_follow_id = " . $params['orders_customer_follow_id'])->update($data);
    
    
    		$result = 1;
    		// 提交事务
    		Db::commit();
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    	}
    	return $result;

    }
    //添加
    public function addOrdersCustomerFollow($params){
    	$t = time();



    	$data['orders_number'] = trim($params['orders_number']);
    	$data['orders_customer_follow_content'] = $params['orders_customer_follow_content'];

    	$data['create_time'] = $t;
    	$data['create_user_id'] = $params['user_id'];
    	$data['update_time'] = $t;
    	$data['update_user_id'] = $params['user_id'];


    	$orderModel=new Orders();
    	$orderInfo=$orderModel->where('orders_number',$data['orders_number'])->find();
    	 if($orderInfo)
         {   $orderInfo->update_time=time();
             $orderInfo->update_user_id=$params['user_id'];
             $orderInfo->save();
         }

    
    	
    	Db::startTrans();
    	try {
    		$where['orders_number'] =  $params['orders_number'];
    		$ocf_result = Db::name('orders_customer_follow')->where($where)->count();
    		if($ocf_result==0){
    			Db::name('orders_customer_follow')->insert($data);
    		}
    		else
                Db::name('orders_customer_follow')->where($where)->update($data);
    		
    
    
    		$result = 1;
    		// 提交事务
    		Db::commit();
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    	}
    	
    	return $result;

    }	
}