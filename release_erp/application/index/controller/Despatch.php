<?php


namespace app\index\controller;


use app\common\help\Help;
use think\Request;

class Despatch extends Base
{

    public function addAbnormal(){
        $data= Request::instance()->param();
        $orderResult = $this->callSoaErp('post', '/DespatchAbnormal/addAbnormal', $data);
        return json($orderResult);
    }

public function despatchAddAjax(){
    $data= Request::instance()->param();
    $orderResult = $this->callSoaErp('post', '/despatch/addDespatch', $data);
    return json($orderResult);
}


public function showDespatchManage(){

    $data= Request::instance()->param();
    $orderResult = $this->callSoaErp('post', '/despatch/getDespatch', $data);
    $result = $this->callSoaErp('post','/system/getTaxrate',['status'=>1,'cost_type'=>2]);

    if(isset($data['look_data'])){
     echo json_encode($orderResult['data']['list']);exit();}

    $this->getPageParams($orderResult);

    $this->assign('fee', $result['data']['list']);
    return $this->fetch('despatch_manage');
    //return json($orderResult);
}

    /**
     * 打印预览
     * 袁毅
     */
    public function showPrintPreviewManageAll(){

        //获取相关数据
        $data= Request::instance()->param();
        $data['shipment_uuid_array']=explode(',',$data['shipment_uuid']);

        $orderResult = $this->callSoaErp('post', '/Shipment/print_all', $data);
		
	
       // echo json_encode($orderResult['data'][0],256);
        $this->assign('orderResult_all', $orderResult['data']);

        return $this->fetch('despatch_print_preview_manage_all');
    }    /**
     * 打印预览
     * 韩冰
     */
    public function showPrintPreviewManage(){

        //获取相关数据
        $data= Request::instance()->param();
        $orderResult = $this->callSoaErp('post', '/Shipment/getShipmentPrintData', $data);
        //echo json_encode($orderResult['data'][0],256);
        $this->assign('orderResult', $orderResult['data'][0]);

        return $this->fetch('despatch_print_preview_manage');
    }

    public function showPickupOrderAdd(){
        $orders_id = input('orders_id');
        //获取所有城市
        //如果选择了项目
        if (!empty($orders_id)) {
            //获取订单信息
            $orderData['orders_id'] = $orders_id;
            $orderData['orders_id_is_like'] = 1;
            //获取订单信息
            $orderResult = $this->callSoaErp('post', '/order/getOrder', $orderData);

            $orderList=$orderResult['data'];



            //echo json_encode($orderList,256);exit();


            $this->assign('orderResult', $orderResult['data']);
            $this->assign('or', $orderResult['data'][0]);


            $orderGoodsData['orders_id'] = $orders_id;
            $orderGoodsData['orders_goods_id_is_like'] = 1;
            $orderGoodsResult = $this->callSoaErp('post', '/order/getOrderGoods', $orderGoodsData);
            $this->assign('orderGoodsResult', $orderGoodsResult['data']);

            //获取货物信息
            //获取所有省
            $province_data = Help::getCity(0, 1);
            //获取所有市
            $city_data = Help::getCity(0, 2);
            //获取所有区
            $area_data = Help::getCity(0, 3);
            $this->assign('provinceResult', $province_data);
            $this->assign('cityResult', $city_data);
            $this->assign('areaResult', $area_data);
            //获取供应商信息

            $supplierData['status'] = 1;
            $supplierResult = $this->callSoaErp('post', '/source/getSupplier', $supplierData);
            $supplierData['start_city_id'] = $orderResult['data'][0]['send_city_id'];
            $supplierData['end_city_id'] = $orderResult['data'][0]['accept_city_id'];
           // $supplierData['start_city_id'] = 2;
          //  $supplierData['end_city_id'] = 61;
            $supplierResult1 = $this->callSoaErp('post', '/source/getSupplierLine', $supplierData);
//echo json_encode($orderGoodsResult['data']);exit();
            $this->assign('supplierResult', $supplierResult1['data']);
        }
        $result = $this->callSoaErp('post','/system/getTaxrate',['status'=>1,'cost_type'=>2]);
      //  echo json_encode($result['data']['list']);exit();
        $this->assign('fee', $result['data']['list']);

        return $this->fetch('pickup_order_add');

    }
}