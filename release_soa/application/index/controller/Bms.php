<?php

namespace app\index\controller;

use app\common\help\Help;
use app\index\model\despatch\Despatch;
use app\index\model\orders\AbnormalType;
use app\index\model\orders\OrderIncome;
use app\index\model\orders\Orders;
use app\index\model\orders\OrdersAbnormal;
use app\index\model\orders\OrdersFinance;
use app\index\model\orders\Ordersgoods;
use app\index\model\orders\OrdersOperating;
use app\index\model\orders\OrdersRemark;
use app\index\model\orders\OrdersUpload;
use app\index\model\shipment\Shipment as ShipmentModel;
use app\index\model\orders\OrdersCustomerFollow;
use app\index\model\source\AcceptGoods;
use app\index\model\source\SendGoods;
use think\config;

use app\index\service\SourceService;
use app\index\service\BranchProductService;

use app\index\service\InStationLetterService;
use think\Db;
use think\Model;
use think\Controller;
use app\common\help\Contents;
use app\index\service\PublicService;
use think\Validate;

class Bms extends Base
{

    private $_orders;
    private $_ordersGoods;
    //_lang Base里的属性，
    public function __construct()
    {

        $this->_orders = new Orders();
        $this->_ordersGoods = new Ordersgoods();
        parent::__construct();
    }


    public function getTmsPeibi()
    {
        $params = $this->input();

        $order = new Orders();

        if (isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : Contents::PAGE_SIZE;
            $page = ($params['page'] - 1) * $page_size;
            $count = $this->_orders->getPeibi($params, true);
	
            $result = $this->_orders->getPeibi($params, false, 'true', $page, $page_size);
            // return  $this->outPutError($result);


            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];

            return $this->output($data);
            // return $this->output($result,'',$count);
        }
        $result = $this->_orders->getPeibi($params);



        return $this->output($result, '');


    }

}
