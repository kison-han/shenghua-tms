<?php


namespace app\index\controller;


use app\common\help\Help;
use think\Request;

class Shortbarge extends Base
{
//获取订单号
    public function getNumberAjax(){
        $data=Request::instance()->param();
        $res=$this->callSoaErp('post','/shortbarge/getNumber',$data);
        $res['code']=0;
        return json($res);

    }
//成本核算
    public function accountingManage(){

        $data=Request::instance()->param();
        if(isset($data['income_type']) && $data['income_type']==1)
        {
            $result=$this->callSoaErp('post','/shortbarge/getShortBarge',$data);

        }
        else
        {
            $result = $this->callSoaErp('post', '/despatch/getDespatch', $data);

        }
//echo "<pre>";
//var_dump($result);exit();
//echo "</pre>";

        $this->getPageParams($result);
        return $this->fetch('accounting_manage');
    }
//添加异常
    public function addAbnormal(){


        return $this->fetch('add_abnormal');
    }
//异常管理
    public function abnormalManner(){
        $data=Request::instance()->param();

        $result=$this->callSoaErp('post','/DespatchAbnormal/getAbnormal',$data);
        $this->getPageParams($result);
        return $this->fetch('short_barge_abnormal_manage');
    }

//短驳商品
    public function shortBargeGoods(){
        $data=Request::instance()->param();
        $result=$this->callSoaErp('post','/shortbarge/getShortBargeGoods',$data);
        $this->getPageParams($result);
        return $this->fetch('short_barge_goods');
    }
//短驳列表
    public function shortBargeList(){
        $data=Request::instance()->param();
        $data['status']=1;
        $result=$this->callSoaErp('post','/shortbarge/getShortBarge',$data);
        $this->assign('short_barge_record',$result['data']['short_barge_record']);
        $this->assign('short_barge_print_record',$result['data']['short_barge_print_record']);
        $this->getPageParams($result);
       return $this->fetch('short_barge_list');
    }

//添加短驳ajax
    public function addShortbargeAjax(){
        $data=Request::instance()->param();
        $result=$this->callSoaErp('post','/shortbarge/addShortBarge',$data);
        return $result;
    }

//添加短驳
    public function addShortbarge(){
        $data=Request::instance()->param();
        $result=$this->callSoaErp('post','/despatch/getDespatch',$data);
        $this->getPageParams($result);
        $supplierData['status'] = 1;
        $supplierResult = $this->callSoaErp('post', '/source/getSupplier', $supplierData);

        $this->assign('supplierResult', $supplierResult['data']);

        $vehicle_data['status']=1;
        $vehicleType= $this->callSoaErp('post','/source/getVehicleType',$vehicle_data);
        $this->assign('vehicleType', $vehicleType['data']);
        $province_data = Help::getCity(0, 1);
        //获取所有市
        $city_data = Help::getCity(0, 2);
        //获取所有区
        $area_data = Help::getCity(0, 3);
        $this->assign('provinceResult', $province_data);
        $this->assign('cityResult', $city_data);
        $this->assign('areaResult', $area_data);
        return $this->fetch('add_short_barge');

    }
//短驳管理
   public function shortBargeManage(){

       $data= Request::instance()->param();
       $orderResult = $this->callSoaErp('post', '/despatch/getDespatch', $data);
       $result = $this->callSoaErp('post','/system/getTaxrate',['status'=>1,'cost_type'=>2]);

       $count = $this->callSoaErp('post', '/Shortbarge/getCount', $data);
       $this->assign('short_barge_record',$count['data']['short_barge_record']);
       $this->assign('short_barge_print_record',$count['data']['short_barge_print_record']);

       if(isset($data['look_data'])){
           echo json_encode($orderResult['data']['list']);exit();}

       $this->getPageParams($orderResult);

       $this->assign('fee', $result['data']['list']);

       return $this->fetch('short_barage_manage');
   }


}