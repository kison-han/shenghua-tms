<?php if (!defined('THINK_PATH')) exit(); /*a:5:{s:84:"/var/www/html/test_erp/public/../application/index/view/order/income_accounting.html";i:1664417310;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:64:"/var/www/html/test_erp/application/index/view/public/header.html";i:1658978091;s:67:"/var/www/html/test_erp/application/index/view/public/left_menu.html";i:1665286673;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






    <title>收入核算</title>

</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
            <div class="layui-logo"></div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left">
        <li class="layui-nav-item">
            <a href="/turnoverbox/index">智能周转箱</a>
        </li>
        <li class="layui-nav-item">
            <a href="/">TMS</a>
        </li>
        <li class="layui-nav-item">
 
            <a href="/warehouse/index">WMS</a>
        </li>
        <li class="layui-nav-item">
            <a href="/wisdompark/index">智慧园区</a>

        </li>
        <li class="layui-nav-item">
            <a href="/Oa/index">OA</a>

        </li>   
         <li class="layui-nav-item">
            <a href="/Bms/index">BMS</a>

        </li>
        <li class="layui-nav-item">
            <a href="/equipment/equipmentManage">设备</a>

        </li>
    </ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item tips-system-message" style="cursor: pointer"><?php echo $language_tag['index_nav_system_message']; ?> <span class="system-message-quantity" style="color: red"> 1</span> </li>
      <li class="layui-nav-item">
        <a href="javascript:;">
          <!-- <img src="http://t.cn/RCzsdCq" class="layui-nav-img"> -->
          <?php echo \think\Session::get('user.nickname'); ?>
        </a>
        <dl class="layui-nav-child head-top">
          <dd><a href="/system/setUserInfo/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_basicDocument']; ?></a></dd>
          <dd><a href="/system/showChangePassword/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_resetPassword']; ?></a></dd>
        </dl>
      </li>
      <li class="layui-nav-item"><a href="/login/loginOut">退出登录</a></li>
    </ul>

    <div id="tips-system-message-js" style='display:none;'>
        <div  style="max-height:350px;width: 450px">
             <div style="color: #000;text-align: center;height: 10px;padding: 10px">你有 <font class="f-tips-system-message"></font> 条未读消息</div>
             <hr>
             <div class="tips-system-message-div" style="height:220px;overflow-y: auto">
           
                 <li style="color: #000; padding: 10px;cursor: pointer" data-href="" data-id="" class="aUrl" onclick="Aurl(this)" ></li>
                 <hr>
             
             </div>
            <div style="color: #000;text-align: center;height: 40px;padding-top: 15px"><a href="/reminderManagement/allInStationLetter"><?php echo $language_tag['index_nav_viewAll_messa']; ?></a></div>
        </div>
    </div>

    <!--  下列保存用户SESSION信息 -->
	
    <input type="hidden" id='now_url'  value="<?php echo $now_url; ?>" />
	<input type="hidden" id='after_url'  value="<?php echo $after_url; ?>" />
    <input type="hidden" id='user_company_id' value="<?php echo \think\Session::get('user.company_id'); ?>" />
	<!--  下列保存其他信息 -->
	<input type="hidden" id='http_referer' value="<?php echo $http_referer; ?>" />
	
	<script type='text/javascript'>
		$('.tips-system-message').on('click',function(){
			var html = $('#tips-system-message-js').html();
		    layer.tips(html, '.tips-system-message', {
		        tips: [3, '#fff'],
		        padding:'20',
		        tipsMore: false,
		        area: ['450px', 'auto'],
		        shade: [0.01, '#fff'],
		        shadeClose:true,
		        time:0
			
		    });
		
		});
		
		$(document).ready(function(){ 
			$(document).mousemove(function(e){ 
				
				if(e.pageX<=10){
					$('.layui-bg-black').show()
				}
				
				if(e.pageX>200){
					$('.layui-bg-black').hide()
				
				}
				if(e.pageY<=10){
					$('.layui-header').show()
				}
				
				if(e.pageY>40){
					$('.layui-header').hide()
				
				}			
			}); 
			
		}); 
	

	</script>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <div title="菜单收缩" class="kit-side-fold"><i class="layui-icon layui-icon-spread-left" aria-hidden="true"></i></div>
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
                  <ul class="layui-nav layui-nav-tree" id="left-nav" lay-filter="test">
          <li class="layui-nav-item"><a href="/"><i class="layui-icon layui-icon-chart-screen"></i>   <em>控制面板</em></a></li>

          <li <?php if($controller_name == 'order'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-form"></i><em>运单管理</em></a>
        <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"showordermanage,showorderadd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderManage?multi_order_status=1">运单管理</a></dd>
			<!--<dd <?php if(in_array(($function_name), explode(',',"showordertrackmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderTrackManage">运单跟踪</a></dd>-->
			 <?php if(\think\Session::get('user.role_id') == 1 || \think\Session::get('user.role_id') == 15): ?>  <dd <?php if(in_array(($function_name), explode(',',"showordertrackmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderTrackManage?sign=0&multi_order_status=2,3,4,5">运单跟踪</a></dd><?php endif; ?> 
            <dd <?php if(in_array(($function_name), explode(',',"showorderreceiptmanage,showorderreceiptadd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderReceiptManage?receipt_status=0">回单管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showorderabnormalmanage,showorderabnormaladd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderAbnormalManage?handle=1">异常运单</a></dd>
  			
  			<dd <?php if(in_array(($function_name), explode(',',"incomeaccountingmanage,addorderincome"))): ?> class="layui-this"<?php endif; ?>><a href="/order/incomeAccountingManage?verify_status=1">收入核算</a></dd>


        </dl>

        </li>
           <li style='display:none' <?php if($controller_name == 'dispatch' or $controller_name == 'despatch' or $controller_name == 'shortbarge'): ?> class="layui-nav-item layui-nav-itemed" <?php elseif($controller_name == 'transport'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-console"></i><em>调度管理</em></a>
        <dl class="layui-nav-child" >
            <dd <?php if(in_array(($function_name), explode(',',"showpickupordermanage,showpickuporderadd,showdespatchmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showPickupOrderManage">发运安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"shortbargemanage,shortbargelist"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/shortBargeManage?short_barge=1">短驳安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"abnormalmanner,addabnormal"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/abnormalManner">异常管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"accountingmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/accountingManage">成本核算</a></dd>

		   <!-- <dd <?php if(in_array(($function_name), explode(',',"showtransportmanage,uploadtransport"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showTransportManage">运单管理</a></dd>
 			<dd <?php if(in_array(($function_name), explode(',',"showdispatchmanage,uploaddispatch"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showDispatchManage">调度管理</a></dd> -->

        </dl>

        </li>
            <li <?php if($controller_name == 'shipment'): ?> class="layui-nav-item layui-nav-itemed" <?php elseif($controller_name == 'transport'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-console"></i><em>发运管理</em></a>
        <dl class="layui-nav-child" >
            <dd <?php if(in_array(($function_name), explode(',',"showlinemanage,addline,showlineovermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/showLineManage">发运安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showshortbargemanage,showshortbargeovermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/showShortBargeManage">短驳安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"abnormalmanner,addabnormal,abnormalinfomanner"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/abnormalManner">异常管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"getshipmentcostcheck"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/getShipmentCostCheck">成本审核</a></dd>

		   <!-- <dd <?php if(in_array(($function_name), explode(',',"showtransportmanage,uploadtransport"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showTransportManage">运单管理</a></dd>
 			<dd <?php if(in_array(($function_name), explode(',',"showdispatchmanage,uploaddispatch"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showDispatchManage">调度管理</a></dd> -->

        </dl>

        </li>
        <li <?php if($controller_name == 'source'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-website"></i><em>资源管理</em></a>
          <dl class="layui-nav-child">
    		<dd <?php if(in_array(($function_name), explode(',',"showcustomermanage,showcustomeradd"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerManage">客户</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showprojectmanage,showprojectadd,showacceptgoodsmanage,showacceptgoodsadd,showsendgoodsmanage,showsendgoodsadd,showgoodsmanage,showgoodsadd"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showProjectManage">项目</a></dd>

             <dd <?php if(in_array(($function_name), explode(',',"showsuppliermanage,showsupplieradd,showsupplieredit,showsupplierinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showSupplierManage">承运商</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showvehicletypemanage,showvehicletypemanageadd,showvehicletypeedit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showVehicleTypeManage">车辆类型</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showvehiclemanage,showvehicleadd,showvehicleedit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showVehicleManage">车辆</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showdrivermanage,showdriveradd,showdriveredit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showDriverManage">司机</a></dd>

              <dd <?php if(in_array(($function_name), explode(',',"showcustomersuppliermanage,showcustomersupplieradd,showcustomersupplieredit,showcustomersupplierinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerSupplierManage">客服用承运商</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showcustomersendgoodsmanage,showcustomersendgoodsadd,showcustomersendgoodsedit,showcustomersendgoodsinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerSendGoodsManage">客服用发货客户</a></dd>

          </dl>
          <li <?php if($controller_name == 'bill'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-file-b"></i><em>账单管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showcustomerbillmanage,showcustomerbilladd,showcustomerbillmissinvoicemanage,showcustomerbilldoneinvoicemanage,showcustomerbillsendinvoicemanage,showcustomerbillcloseinvoicemanage"))): ?> class="layui-this"<?php endif; ?>><a href="/bill/showCustomerBillManage">客户账单</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showsupplierbillmanage,showsupplierbilladd,showsupplierbillovermanage,supplierbillmissinvoice,supplierbilldoneinvoice,supplierbillfinancegetinvoice,supplierbilladdcostinfo,supplierbillaggrecostinfo,supplierbilldonepay"))): ?> class="layui-this"<?php endif; ?>><a href="/bill/showSupplierBillManage">承运商账单</a></dd>



          </dl>

          <li <?php if($controller_name == 'form'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-file-b"></i><em>报表管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showorderformmanage,showorderformadd"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showOrderFormManage">运单报表</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showshipmentlineoverformmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showShipmentLineOverFormManage">发运报表</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showshortbargeformmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showShortBargeFormManage">短驳报表</a></dd>



          </dl>

        </li>  <li <?php if($controller_name == 'device.smartbox'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>设备管理</em></a>
          <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"smartboxmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/baidumap/index">百度地图(demo)</a></dd>

             <dd <?php if(in_array(($function_name), explode(',',"smartboxmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/device.Smartbox/smartboxManage">智能周转箱</a></dd>





          </dl>
        </li>
	<!--
        <li <?php if($controller_name == 'customer'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-friends"></i><em>客户管理</em></a>
        <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"showcustomermanage,showcustomeradd"))): ?> class="layui-this"<?php endif; ?>><a href="/customer/showCustomerManage">客户</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showprojectmanage,showprojectadd,showacceptgoodsmanage,showacceptgoodsadd,showsendgoodsmanage,showsendgoodsadd,showgoodsmanage,showgoodsadd"))): ?> class="layui-this"<?php endif; ?>><a href="/customer/showProjectManage">项目</a></dd>


        </dl>

        </li>
 -->



		<!-- 系统管理 -->

          <li <?php if($controller_name == 'system'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-set-fill"></i><em>系统管理</em></a>
          <dl class="layui-nav-child">
                 <dd <?php if(in_array(($function_name), explode(',',"taxratemanage,taxrateadd"))): ?> class="layui-this"<?php endif; ?>><a href="/system/taxratemanage">费用管理</a></dd>
				 <dd><a href='/system/showAuthManage'>权限管理</a></dd>

          </dl>
          </li>



      </ul>
        </div>
    </div>

    <div class="layui-body layui-body-bg">

        <!-- 内容主体区域 -->
        <div class="content_body">
            <div class="body-top">
                <div class='layui-form-item'>
                    <span class="layui-breadcrumb" lay-separator="-">
                        <a>首页</a>
                        <a>运单管理</a>
                        <a><cite>收入核算</cite></a>
                    </span>
                </div>
                <div class='layui-block all-search-bg'>
				
					 <ul class="layui-tab-title">
    <li   <?php if(\think\Request::instance()->get('verify_status') == 1): ?> class="layui-this"  <?php endif; ?> ><a href='/order/incomeAccountingManage?verify_status=1'>未核算</a></li>
    <li <?php if(\think\Request::instance()->get('verify_status') == 2): ?> class="layui-this"  <?php endif; ?>><a href='/order/incomeAccountingManage?verify_status=2'>已核算</a></li>
    <!--<li <?php if(\think\Request::instance()->get('verify_status') == 3): ?> class="layui-this"  <?php endif; ?>><a href='/order/incomeAccountingManage?verify_status=3'>已对账</a></li>
    <li <?php if(\think\Request::instance()->get('verify_status') == 4): ?> class="layui-this"  <?php endif; ?>><a href='/order/incomeAccountingManage?verify_status=4'>已开票</a></li>
    <li <?php if(\think\Request::instance()->get('verify_status') == 5): ?> class="layui-this"  <?php endif; ?>><a href='/order/incomeAccountingManage?verify_status=5'>已收款</a></li>-->



  </ul>
				
                    <form class="layui-form" method='get' action=''>
                        <div class="layui-row" style="    margin: 5px;">
                       
                                <div class="layui-input-inline">
                                    <label class="layui-form-label">项目:</label>
                                    <div class="layui-input-inline">
                                    <select name='project_id' lay-search>
                                    	<option value=''>--请选择--</option>
                                    	<?php if(is_array($projectResult) || $projectResult instanceof \think\Collection || $projectResult instanceof \think\Paginator): if( count($projectResult)==0 ) : echo "" ;else: foreach($projectResult as $key=>$v): ?>
                                    	<option value='<?php echo $v['project_id']; ?>' <?php if(\think\Request::instance()->get('project_id') == $v['project_id']): ?>selected<?php endif; ?>><?php echo $v['project_name']; ?></option>
                                    	<?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>
                                    </div>
                                </div>
                       
                          
                                <div class="layui-input-inline">
                                    <label class="layui-form-label">运单号:</label>
                                    <div class="layui-input-inline">
                                    		<input type="text" id="orders_number" class="layui-input" name="orders_number" value="<?php echo \think\Request::instance()->get('orders_number'); ?>" />
                                    </div>
                                </div>
                                                    
                            <!--
                                <div class="layui-input-inline">
                                    <label class="layui-form-label">财务状态:</label>
                                    <div class="layui-input-inline">
                                     <select id="" name="verify_status">
										<option value=''>全部</option>
										 <?php if(is_array($baseConfig['order']['verify_status']) || $baseConfig['order']['verify_status'] instanceof \think\Collection || $baseConfig['order']['verify_status'] instanceof \think\Paginator): if( count($baseConfig['order']['verify_status'])==0 ) : echo "" ;else: foreach($baseConfig['order']['verify_status'] as $key3=>$vo3): ?>
										 <option value="<?php echo $key3; ?>" <?php if($key3==\think\Request::instance()->get('verify_status')): ?> selected="selected" <?php endif; ?>><?php echo $vo3; ?></option>
										 <?php endforeach; endif; else: echo "" ;endif; ?>
										 
                                          </select>    </div>
                                </div>
                           -->
                            
                                <div class="layui-input-inline">
                                    <label class="layui-form-label">运单日期:</label>
                                    <div class="layui-input-inline">
										<div class="layui-inline" style="width: 85px;">
											<input type="text" class="pickup_time layui-input" name="pickup_time" maxlength="300" autocomplete="off" value="<?php echo \think\Request::instance()->get('pickup_time'); ?>"  placeholder="开始时间" class="layui-input">
											
										</div>
										
                                 
										<div class="layui-inline" style="width: 85px;">
											<input type="text" class="pickup_time layui-input" name="pickup_time_end" maxlength="300" autocomplete="off" value="<?php echo \think\Request::instance()->get('pickup_time_end'); ?>"  placeholder="结束时间" class="layui-input">
											
										</div>
									</div>
                                </div>
                           

                                <div class="layui-input-inline">
                                    <label class="layui-form-label">是否计费:</label>
                                    <div class="layui-input-inline">
                                    <select name='total_fee' lay-search>

                                    	<option value='2' <?php if(\think\Request::instance()->get('total_fee')==2): ?>selected="selected"<?php endif; ?>>全部</option> 
                                    	<option value='1' <?php if(\think\Request::instance()->get('total_fee')==1): ?>selected="selected"<?php endif; ?>>已计费</option>
                                    	<option value='0' <?php if(is_numeric(\think\Request::instance()->get('total_fee'))): if(\think\Request::instance()->get('total_fee')==0): ?>selected="selected"<?php endif; endif; ?>>未计费</option>

                                    
                                 
                                    </select>
                                    </div>
                                </div>
                              <div class="layui-input-inline" >
                                    <label class="layui-form-label">分公司:</label>
                                    <div class="layui-input-inline" style="width: 87px;">
                                         <select id='company_id' name='choose_company_id' >
                                            <option value=''>--全部--</option>
											<?php if(is_array($comapnyResult) || $comapnyResult instanceof \think\Collection || $comapnyResult instanceof \think\Paginator): if( count($comapnyResult)==0 ) : echo "" ;else: foreach($comapnyResult as $key=>$v): ?>
												<option value="<?php echo $v['company_id']; ?>" 
												
												<?php if(\think\Request::instance()->get('choose_company_id') != ''): if(\think\Request::instance()->get('choose_company_id') == $v['company_id']): ?>selected <?php endif; else: if(\think\Session::get('user.company_id') == $v['company_id']): ?>selected <?php endif; endif; ?>
												>

												<?php echo $v['company_name']; ?></option>
											<?php endforeach; endif; else: echo "" ;endif; ?>                                           
                                        </select>
                                    </div>
                                </div>	               						
                                 <input type='hidden' name='verify_status' value="<?php echo \think\Request::instance()->get('verify_status'); ?>" />
                         <button class="layui-btn nav-search search_button">查询</button>
                        <a type="reset" href="/order/incomeAccountingManage"
                           class="layui-btn layui-btn-primary search_button">重置</a>

                             					
                        </div>
                        <!--<div class='layui-input-inline'>-->
                        <!--<input type="text" id="" name=""  placeholder="电话、联系人、操作人" class="layui-input">-->
                        <!--</div>-->
                     
                    </form>

                </div>
            </div>
          
		  <div class="content-bg" >
		              
		               
		                 <div class="table-nont user-manage " >
		                     <table id="language-table"  lay-filter="language-table">
		                
		                     </table>
		                   <script type="text/html" id="toolbarDemo" lay-filter="toolbar1">
		             			<div class="layui-inline" lay-event="add"> <i class="layui-icon layui-icon-add-1"></i> </div>
		             
		             	</script>
		                 </div>
		             
		             </div>

        </div>
    </div>


   


</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
<script src='/static/javascript/system/base.js'></script>
<script src='/static/javascript/system/source.js'></script>

<script type="text/html" id="tool_bar">

	              
	<span ><a href='javascript:void(0);' lay-event="sub"><button  class="layui-btn nav-add layui-btn-sm">提交</button></a></span>
	<span ><a href='javascript:void(0);' lay-event="resub"><button  class="layui-btn nav-add layui-btn-sm">驳回</button></a></span>
	<span ><a target="blank" href='incomeAccountingManage?download=1&<?php echo http_build_query($_GET); ?>'><button onclick="" class="layui-btn nav-add layui-btn-sm">下载</button></a></span>
	<span ><button onclick="openlayer('/order/incomeAccountingImport','导入','750px')" class="layui-btn nav-add layui-btn-sm">运费批量导入</button></span>
	<a class="layui-btn layui-btn-sm layui-btn-normal" id="clear"><i class="layui-icon layui-icon-delete"></i>清除缓存</a>
    <a class="layui-btn layui-btn-sm layui-btn-normal" lay-event="save" >保存设置</a>
	
</script>
<script>
var disableIds=[];

 layui.config({}).extend({
        soulTable: '/static/layui-soul-table/ext/soulTable',
        tableChild: '/static/layui-soul-table/ext/tableChild',
        tableMerge: '/static/layui-soul-table/ext/tableMerge',
        tableFilter: '/static/layui-soul-table/ext/tableFilter',
        excel: '/static/layui-soul-table/ext/excel'
    });
layui.use(['form', 'laydate', 'table', 'soulTable', 'element'], function(){
    var table = layui.table;

    var   laytable =layui.laytable
var soulTable = layui.soulTable;
    layui.use('element', function(){
        var element = layui.element;
    });
    var isShow = true;  //定义⼀个标志位
    $('.kit-side-fold').click(function(){
        //选择出所有的span，并判断是不是hidden
        $('.layui-nav-item span').each(function(){
            if($(this).is(':hidden')){
                $(this).show();
            }else{
                $(this).hide();
            }
        });
        //判断isshow的状态
        if(isShow){
            $('.layui-side.layui-bg-black').width(50); //设置宽度
            $('.kit-side-fold i').css('margin-right', '70%');  //修改图标的位置
            //将footer和body的宽度修改
            $('.layui-body').css('left', 60+'px');
            $('.layui-footer').css('left', 60+'px');
            //将⼆级导航栏隐藏
            $('dd span').each(function(){
                $(this).hide();
            });
            //修改标志位
            isShow =false;
        }else{
            $('.layui-side.layui-bg-black').width(200);
            $('.kit-side-fold i').css('margin-right', '10%');
            $('.layui-body').css('left', 200+'px');
            $('.layui-footer').css('left', 200+'px');
            $('dd span').each(function(){
                $(this).show();
            });
            isShow =true;
        }
    });

	let newtable=global_table=  table.render({
		    elem: '#language-table'
		    ,height: tableHeight
		    ,url: '/order/incomeAccountingManage?<?php echo http_build_query($_GET); ?>'///shipment/showLineAjax?orders_number=<?php echo \think\Request::instance()->get('orders_number'); ?>&start_pickup_time=<?php echo \think\Request::instance()->get('start_pickup_time'); ?>&end_pickup_time=<?php echo \think\Request::instance()->get('end_pickup_time'); ?>' //数据接口
		
		    ,response: {

		           statusCode: 200 //规定成功的状态码，默认：0

		          } 
		    ,page: true //开启分页
			,limits:[100,200,300,400,500]
		    ,limit:100
		    ,toolbar: '#tool_bar' //开启工具栏，此处显示默认图标，可以自定义模板，详见文档
		    ,totalRow: true //开启合计行
		    ,cols: [[ //表头
		              	
		             	 {type: 'checkbox', fixed: 'left',field:"checkbox"}
                        ,{field: 'orders_id', title: '序号',width:80,type:'numbers'}
                        ,{field: 'pickup_time', title: '运单时间',width:110,sort:true,templet:function(e){
							return php_date('Y-m-d', parseInt(e.pickup_time));
						}}
		      			,{field: 'orders_number', title: '运单编号',width:160,sort:true}
		      			,{field: 'customer_order_number', title: '客户编号',width:120,sort:true}
		      			,{field: 'send_goods_company', title: '发货方',width:120,sort:true}
		      			,{field: 'send_location_name', title: '发站',width:120,sort:true}
		      			,{field: 'accept_location_name', title: '到站',width:120,sort:true}
		      			,{field: 'accept_goods_company', title: '收货方',width:120,sort:true}
		             	,{field: 'orders_id', title: '运单ID',width:80}
							,{field: 'goods_name', title: '货物名称',width:190,sort:true,templet:function(e){
							var info="";if(!e.orders_goods_info)
							return ""
							else
							e.orders_goods_info.map(function(i){
								info=info+i.goods_name+"/";
							})
							return info.substring(0,info.length-1)
						}}
						
						
						,{field: 'accept_goods_company', title: '包装',width:90,sort:true,templet:function(e){
							var info="";if(!e.orders_goods_info)
							return ""
							else
							e.orders_goods_info.map(function(i){
								info=info+ baseConfig['order']['goods_pack'][i.estimated_pack_unit]+"/";
							})
							return info.substring(0,info.length-1)
						}}
		      			,{field: 'estimated_count', title: '件数',width:90,sort:true,templet:function(e){
							var info="";if(!e.orders_goods_info)
							return ""
							else
							e.orders_goods_info.map(function(i){
								info=info+i.estimated_count+"/";
							})
							return info.substring(0,info.length-1)
						}}
		      			,{field: 'accept_cellphone', title: '数量',width:90,sort:true,templet:function(e){
							var info="";if(!e.orders_goods_info)
							return ""
							else
							e.orders_goods_info.map(function(i){
								info=info+i.estimated_pack_count+"/";
							})
							return info.substring(0,info.length-1)
						}}
		      			,{field: 'accept_all_address', title: '重量',width:190,sort:true,templet:function(e){
							var info="";if(!e.orders_goods_info)
							return ""
							else
							e.orders_goods_info.map(function(i){
								info=info+i.estimated_weight+"/";
							})
							return info.substring(0,info.length-1)
						}}
		      			,{field: 'goods_name', title: '体积',width:130,sort:true,templet:function(e){
							var info="";if(!e.orders_goods_info)
							return ""
							else
							e.orders_goods_info.map(function(i){
								info=info+i.estimated_volume+"/";
							})
							return info.substring(0,info.length-1)
						}}
						
						, {
                    field: 'shipment_count', title: '发运件数', width: 60, sort: true, totalRow: true

                }, {
                    field: 'shipment_pack_count', title: '发运数量', width: 60, sort: true, totalRow: true

                }
				, {
                    field: 'shipment_weight', title: '发运重量', width: 60, sort: true, totalRow: true
                }
                , {
                    field: 'shipment_volume', title: '发运体积', width: 60, sort: true, totalRow: true
                }
                
						
						,{field: 'yun_money', title: '运费',width:90,sort:true}
						,{field: 'ti_money', title: '提货费',width:90,sort:true}
						,{field: 'song_money', title: '送货费',width:90,sort:true}
						,{field: 'xie_money', title: '卸货费',width:90,sort:true}
						,{field: 'pack_money', title: '打包费',width:90,sort:true}
						
						,{field: 'money', title: '总运费',width:90,totalRow: true,sort:true}
						,{field: 'abnormal_money', title: '赔款',width:90,totalRow: true,sort:true}
						
						
						
		             	, {field: 'project_name',hide:true, title: '项目名称',width:90,sort:true}
		      			,{field: 'send_location_name', hide:true,title: '支付方式',width:90,sort:true,templet:function(e){
							return baseConfig['order']['pay_type_name'][e.pay_type]
						}}
		      			,{field: 'bargain_type',hide:true, title: '计价方式',width:90,sort:true,templet:function(e){
							return baseConfig['order']['bargain_type'][e.bargain_type]
						}}
		      			,{field: 'bargain_price',hide:true, title: '预计收入',width:90,sort:true}
					
		      			,{field: 'send_cellphone', hide:true,title: '运输类型',width:130,sort:true,templet:function(e){
							return baseConfig['order']['transportation_type_name'][e.transportation_type]
						}}
		      		
		      			
		      			,{field: 'realy_count', title: '计费件数',hide:true,width:90,totalRow: true,sort:true,templet:function(e){
							var info="";
							if(!e.orders_goods_info)
							return ""
							else
							e.orders_goods_info.map(function(i){
								info=info+i.realy_count.toString()+"/";
							})
							return info.substring(0,info.length-1)
						}}
		      			,{field: 'realy_pack_count', title: '计费数量',hide:true,width:90,totalRow: true,sort:true,templet:function(e){
							var info="";if(!e.orders_goods_info)
							return ""
							else
							e.orders_goods_info.map(function(i){
								info=info+i.realy_pack_count+"/";
							})
							return info.substring(0,info.length-1)
						}}
		      			,{field: 'realy_pack_unit', title: '计费包装单位',hide:true,width:90,sort:true,templet:function(e){
							var info="";if(!e.orders_goods_info)
							return ""
							else
							e.orders_goods_info.map(function(i){
								info=info+baseConfig['order']['goods_pack'][i.realy_pack_unit]+"/";
							})
							return info.substring(0,info.length-1)
						}}
		      			,{field: 'realy_weight', title: '计费重量',width:90,hide:true,totalRow: true,sort:true,templet:function(e){
							var info="";if(!e.orders_goods_info)
							return ""
							else
							e.orders_goods_info.map(function(i){
								info=info+i.realy_weight+"/";
							})
							return info.substring(0,info.length-1)
						}}
		      			,{field: 'realy_volume', title: '计费体积',hide:true, width:90,totalRow: true,sort:true,templet:function(e){
							var info="";if(!e.orders_goods_info)
							return ""
							else
							e.orders_goods_info.map(function(i){
								info=info+i.realy_volume+"/";
							})
							return info.substring(0,info.length-1)
						}}
		      			,{field: 'bargain_type', title: '发货信息',hide:true,width:90,sort:true,templet:function(e){
							return e.send_province_name+e.send_city_name+e.send_area_name+e.send_address
						}}
		      			,{field: 'send_goods_company', title: '发货方',hide:true,width:90,sort:true}
		      			,{field: 'send_cellphone', title: '发货方电话',hide:true,width:90,sort:true}
		      			,{field: 'remark56', title: '收货信息',hide:true,width:90,sort:true,templet:function(e){
							return e.accept_province_name+e.accept_city_name+e.accept_area_name+e.accept_address
						}},
						{field:'is_free',title:'免运费',templet:function(e){
						return e.is_free>0?"是":"否";
						
						}
						}
		      			,{field: 'accept_goods_company', title: '收货方',hide:true,width:90,sort:true}
		      			,{field: 'accept_cellphone', title: '收货电话',hide:true,width:90,sort:true}
		      			,{field: 'replacement_prive', title: '代收货款',hide:true,width:90,sort:true}
		      			,{field: 'remark1', title: '代收状态',width:90,hide:true,sort:true,templet:function(e){
							return e.replacement_prive?"已收":"未收"
						}}
		      			,{field: 'insurance_goods', title: '货值',hide:true,width:90,sort:true}
		      			,{field: 'remark2', title: '送达时间',width:110,hide:true,sort:true,templet:function(e){
							return php_date('Y-m-d',e.send_time);
						}}
		      			,{field: 'remark', title: '备注',hide:true,width:90}
		      			,{field: 'create_user_name',hide:true, title: '创建人',width:90,sort:true}
		      			,{field: 'create_time',hide:true, title: '创建时间',width:90,sort:true,templet:function(e){
							return php_date('Y-m-d',e.create_time);
						}}
		      			,{field: 'order_status',hide:true, title: '当前进度',width:90,sort:true,templet:function(e){
							if(e.order_status)
						return	baseConfig['order']['order_status'][e.order_status]['order_status_name'];
						else
						return "无";
						
						}}
		      			,{field: 'remark4',hide:true, title: '回单状态',width:90,sort:true,templet:function(e){
							if (e.order_status>=7){
							            					
							                                if (e.receipt_status==0)
							                                return ` <a href="javascript:void(0);" onclick="openlayer('/order/addReceipt?orders_id=${e.orders_id}','运单回执')" style="color: #f00">未回</a>`
							                                else if(e.receipt_status==1)
							                               return  `<a href="javascript:void(0);" onclick="openlayer('/order/orderUploadView?orders_id=${e.orders_id}','运单回执')" style="color: #0f0">已回</a>`
							                                else if(e.receipt_status==2)
							                               return  `<a href="javascript:void(0);" onclick="openlayer('/order/orderUploadView?orders_id=${e.orders_id}','运单回执')" style="color: #0f0">已送</a>`
							                                else if(e.receipt_status==3)
							                               return  `<a href="javascript:void(0);" onclick="openlayer('/order/orderUploadView?orders_id=${e.orders_id}','运单回执')" style="color: #0f0">其他</a>`
							                               
							                               }
														   return ""
						}}
		      			,{field: 'remark5', title: '是否异常',width:90,sort:true,templet:function(e){
							return e.abnormal_count>0?"是":"否"
						}}
		      			,{field: 'remark6',hide:true, title: '发运跟踪',width:90,templet:function(e){
							return "<a href='/order/orderFollow?orders_id="+e.orders_id+"&orders_number="+e.orders_number+"'  class='orderFollow' follow_remark='"+e.follow_remark+"'  follow_time='"+e.follow_time+"'>查看</a>"
						}},{field: 'remark7', title: '处理状态',width:90,sort:true,templet:function(e){
							return baseConfig['order']['verify_status'][e.verify_status]
							}},{field: 'remark8', title: '收入核算备注',width:90,sort:true,templet:function(e){
							if(e.income_remark)
							return e.income_remark.content;
							return ""
							}}
		      			,{title: '操作',width:100,fixed:'right',templet:function(e){
							
							return `<span><a href='javascript:openNewLayer("/order/addOrderIncome?project_id=${e.project_id}&orders_id=${e.orders_id}","修改");'><button class="layui-btn layui-btn-sm hover-edit layui-btn-primary">修改</button></a></span>`
						}}
		     

		    ]],
			 filter: {
                items: ['column', 'data', 'condition', 'editCondition', 'excel', 'clearCache'],
                cache: true
            },
			 done: function (res, curr, count) {
			  soulTable.render(this)
			    //如果是异步请求数据方式，res即为你接口返回的信息。
			    //如果是直接赋值的方式，res即为：{data: [], count: 99} data为当前页数据、count为数据总长度
			    console.log(res);
			  
			    //得到当前页码
			    console.log(curr);
			  var that = this.elem.next();
                    
			    //得到数据总量
			    console.log(count);
				disableIds=[];
			  res.data.forEach(function(item,index){
				  if(item.verify_status>=3)
				  {
					  disableIds.push(item.orders_id)
					  
					  var tr = that.find(".layui-table-fixed-l tbody tr[data-index='" + index + "']");
				
					  $(tr.find(".layui-table-col-special"))[0].innerHTML=`<div class="layui-table-cell laytable-cell-1-0-1" style="width: 48px;"> </div>`;
					// var tr1 = that.find(".layui-table-box tbody tr[data-index='" + index + "']");
					  // tr1.find(".laytable-cell-checkbox").remove();
					
                         //   tr.css("background-color", "#FF5722");
                          //  tr.css("color", "white");

				  }
				  
				  
			  })
			
			  }
		  });
	

  $(document).on('click', '#clear', function () {
            soulTable.clearCache(newtable.config.id)
            layer.msg('已还原！', {icon: 1, time: 1000})
        })
		
		
		
		
		
    //多选事件
    table.on('checkbox(language-table)', function(obj){
        if(obj.type=="all"){ //全选
            if(obj.checked==true){
                $(".layui-table-body table.layui-table tbody tr").css('background','#99CCFF');
            }else{
                $(".layui-table-body table.layui-table tbody tr").map(function(i,d){
				$(d).css('background','#fff');
				})	
            }
        }else{ //单选
            if(obj.checked==true){
                obj.tr.css('background','#99CCFF');
            }else{
                obj.tr.css('background','#fff');
            }
        }

    });
		
		
    //监听头工具栏事件
    table.on('toolbar(language-table)', function(obj){
	
	var myTable=obj.config;
 var storeKey = location.pathname + location.hash + myTable.id;
 var colsStr=JSON.stringify(obj.config.cols);
		console.log(obj)
		console.log(obj.event)
      var checkStatus = table.checkStatus(obj.config.id)
      var data = checkStatus.data; //获取选中的数据
	  
	 var  order_ids=[];
		 data.forEach(function(e){
			 if(disableIds.indexOf(e.orders_id)==-1)
	         order_ids.push(e.orders_id)
		 	
		 })
	  
	  
      switch(obj.event){
	  case 'save':layer.msg("保存成功");
			    localStorage.setItem( storeKey, colsStr);
	  break;
	      case 'resub': 
		   if(order_ids.length==0)
		  {layer.msg("请选择一行");return;}
		   data.forEach(function(e){
		   console.log(e)
			 if(e.verify_status>2)
	         layer.msg('已对账不能修改');
			 else
		 	$.ajax({
		 				   	url:"/index/changeStatus",
							method:"post",
		 				   	data:{table_id:order_ids,table_id_name:'orders_id',table_name:"orders",status:1,field:"verify_status"},
		 				   	success:function(e){
		 				   		
								layer.msg('提交成功', {
								  icon: 1,
								  time: 2000 
								}, function(){
								  newtable.reload();
								});   
								
								
		 				   	},
		 					async:false
		 				   	
		 				   })
		 })
		  
		  break;
		  case 'sub':
		   if(order_ids.length==0)
		  {layer.msg("请选择一行");return;}
		  
		  if(data.length>0)
		  {
			  
		

		 $.ajax({
		 				   	url:"/index/changeStatus",
							method:"post",
		 				   	data:{table_id:order_ids,table_id_name:'orders_id',table_name:"orders",status:2,field:"verify_status"},
		 				   	success:function(e){
		 				   		
								layer.msg('提交成功', {
								  icon: 1,
								  time: 2000 
								}, function(){
								  newtable.reload();
								});   
								
								
		 				   	},
		 					async:false
		 				   	
		 				   })
		 	
		 
		 
		// window.location.reload()
		 
		 }
		 else
		 layer.msg("请选择一行");
		  
		  break;
        
      };
    });
})
	  //第一个实例

</script>
<script>



    var layer = layui.layer;

    var form = layui.form;
	
	form.on('select(replacement_prive_type)',function(data){
		var table_id = $(data.elem).parents("tr").children(".order_id").html();
		
		var table_id_name = "orders_id";
		var table_name = "orders";
		status=data.value;
		
		$.ajax({
		    type: "POST",
		    url: '/index/changeStatus',
		    data: {
		        table_id:table_id,
		        table_id_name:table_id_name,
		        table_name:table_name,
		        status:status,
				field:"replacement_prive_type"
		    },
		    success: function(data){
		        //console.log(data);
		        if(data.code!=200){
		            layer.msg(data.msg);
		            return false;
		        }else if(data.code==200) {
		            layer.msg('操作成功',{time:1,end : function(layero, index){
		
		                }
		            });
		        }
		    },
		    error:function(XMLHttpRequest, textStatus, errorThrown){
		        layer.msg('验证失败')
		    }
		});
		
		
		
		console.log(data)
	})
	
	
    form.on('switch(switchTest)', function(data){
        var table_id = $(data.elem).parents("tr").children(".order_id").html();

        var table_id_name = "order_id";
        var table_name = "order";
        if(this.checked == true){
            var status = 1;
        }else{
            var status = 0;
        }

        $.ajax({
            type: "POST",
            url: '/index/changeStatus',
            data: {
                table_id:table_id,
                table_id_name:table_id_name,
                table_name:table_name,
                status:status,
            },
            success: function(data){
                //console.log(data);
                if(data.code!=200){
                    layer.msg(data.msg);
                    return false;
                }else if(data.code==200) {
                    layer.msg('操作成功',{time:1,end : function(layero, index){

                        }
                    });
                }
            },
            error:function(XMLHttpRequest, textStatus, errorThrown){
                layer.msg('验证失败')
            }
        });
    });


form.on('checkbox(checkAll)',function(e){
	
	$(".income_select").map(function(i,d){
	
		d.checked=e.elem.checked;
	})
	form.render();
	
})

function subIncome(){

	var chk_value =[];
	$('input[name="income_select"]:checked').each(function(){
	chk_value.push($(this).val());
	});
	console.log(chk_value)
	if(chk_value.length==0)
	alert("你还没有选择");
    else{
		
		chk_value.map(function(d,i){
			
			
			
		})
		
		
		$.ajax({
					   	url:"/index/changeStatus",
					   	data:{table_id:chk_value,table_id_name:'orders_id',table_name:"orders",status:1,field:"verify_status"},
					   	success:function(e){
					   	
					   	},
						async:false
					   	
					   })
		
		
		window.location.reload()
		 
		
	
		
	}
	
}
 function openNewLayer(url,title){
  layer.open({
            type: 2,
			maxmin: true,
            title: title,
            content: url,
            area: ["100%", "600px"]
        })
  
  }
  
 
</script>
</body>
</html>


