<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/25
 * Time: 10:00
 */
namespace app\index\controller;

use http\Client\Curl\User;
use think\Cache;
use think\Cookie;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
use app\common\help\Contents;
use app\common\help\Help;
use \Underscore\Types\Arrays;

class Bmsbaobiao extends Base
{
	private $_language;
	public function __initialize(){
		$this->_language = $this->callSoaErp('post','/system/getLanguage',[])['data'];


	}

	public function tmspeibi(){
		
	

	
		$this->assign('projectResult',$result['data']);
		return $this->fetch('tmspeibi');
        
    }

	public function tmspeibiAjax(){
		
		$params = Request::instance()->param();
		
		
		$data = [
			'page' => $params['page'],
			'page_size' => $params['limit'],
		
				//'orders_id_is_like'=>1,
		];
		if(!empty($params['orders_number'])){
			$data['orders_number'] = $params['orders_number'];
		}
		if(!empty($params['send_goods_company'])){
			$data['send_goods_company'] = $params['send_goods_company'];
		}
		if(!empty($params['accept_goods_company'])){
			$data['accept_goods_company'] = $params['accept_goods_company'];
		}		
		if(!empty($params['start_pickup_time'])){
			$data['start_pickup_time'] = strtotime($params['start_pickup_time']);
		}	
		if(!empty($params['end_pickup_time'])){
			$data['end_pickup_time'] = strtotime($params['end_pickup_time']);
		}			
		$result = $this->callSoaErp('post','/bms/getTmsPeibi',$data);
		

		if($params['is_export']==1){//下载
		
			$result = $result['data'];
		}else{
			$result_count = $result['data']['count'];
			$result = $result['data']['list'];
		
		}
		$maolilv_jisuan = 0;
			
		$array_result_new = [];
		for($i=0;$i<count($result);$i++){
			$pp=0;
			//这里开始判断 干线和短驳是否都已经开票
			for($ll=0;$ll<count($result[$i]['shipment_info']);$ll++){
				if($result[$i]['shipment_info'][$ll]['shipment_finance_status']<4){
				
					unset($result[$i]);
					$pp=1;
					break;
				}
				
			}

			for($mm=0;$mm<count($result[$i]['short_barge_info']);$mm++){
				if($result[$i]['short_barge_info'][$mm]['shipment_finance_status']<4){
				
					unset($result[$i]);
					$pp=1;
					break;
				}
				
			}
		

			 
			
			//发运时间
			if(is_numeric($result[$i]['pickup_time'])){
				$result[$i]['pickup_time'] = date('Y-m-d',$result[$i]['pickup_time']);
			}else{
				$result[$i]['pickup_time']='';
			}
			//货物名称
			$orders_goods_name='';
			
			$jfsl=0;//计费数量
			$jfjs =0;//计费件数
			$jfzl=0;//计费重量
			$jftj=0;//计费体积
			for($j=0;$j<count($result[$i]['orders_goods']);$j++){
				if($j==0){
					$orders_goods_name=$result[$i]['orders_goods'][$j]['goods_name'];
				}else{
					$orders_goods_name.='/'.$result[$i]['orders_goods'][$j]['goods_name'];
				}
				$jfsl+=$result[$i]['orders_goods'][$j]['realy_pack_count'];
				$jfjs+=$result[$i]['orders_goods'][$j]['realy_count'];
				$jfzl+=$result[$i]['orders_goods'][$j]['realy_weight'];
				$jftj+=$result[$i]['orders_goods'][$j]['realy_volume'];
			}
			$result[$i]['orders_goods_name']=$orders_goods_name;
			$result[$i]['jfsl']=$jfsl;
			$result[$i]['jfjs']=$jfjs;
			$result[$i]['jfzl']=$jfzl;
			$result[$i]['jftj']=$jftj;
			
			$fysl =0;//发运数量
			$fyjs=0;//发运件数
			$fyzl=0;//发运重量
			$fytj=0;//发运体积
			$shipment_name='';
			for($i2=0;$i2<count($result[$i]['shipment_info']);$i2++){
				for($i22=0;$i22<count($result[$i]['shipment_info'][$i2]['shipment_goods']);$i22++){
					$fysl+=$result[$i]['shipment_info'][$i2]['shipment_goods'][$i22]['shipment_pack_count'];
					$fyjs+=$result[$i]['shipment_info'][$i2]['shipment_goods'][$i22]['shipment_count'];
					$fyzl+=$result[$i]['shipment_info'][$i2]['shipment_goods'][$i22]['shipment_weight'];
					$fytj+=$result[$i]['shipment_info'][$i2]['shipment_goods'][$i22]['shipment_volume'];
				}
				if($i2==0){
					$shipment_name=$result[$i]['shipment_info'][$i2]['supplier_name'];
				}else{
					$shipment_name.='/'.$result[$i]['shipment_info'][$i2]['supplier_name'];
					
				}
			}
		
			$result[$i]['shipment_supplier_name'] = $shipment_name;
			$result[$i]['fysl']=$fysl;			
			$result[$i]['slc'] = $fysl-$jfsl;
			$result[$i]['fyjs'] = $fyjs;
			$result[$i]['jsc'] = $fyjs-$jfjs;
			$result[$i]['fyzl'] = $fyzl;
			$result[$i]['zlc'] = $fyzl-$jfzl;
			
			$result[$i]['fytj'] = $fytj;
			$result[$i]['tjc'] = number_format($fytj-$jftj,2);
			
			//代表税前
			//if($result[$i]['orders_tax_type']==1){
			//	if($result[$i]['orders_tax']>0){
			//		$result[$i]['yfzj'] = round($result[$i]['money']*(1+(0.01*$result[$i]['orders_tax'])),2);
			//	}else{
			//		$result[$i]['yfzj'] = $result[$i]['money'];
			//	}
				
				
			//}else{
			$result[$i]['yfzj'] = $result[$i]['money'];
			//}
			
			
			//收入调整
			$result[$i]['srtz'] = 0;
			
			//获取运单理赔金额
			$orders_abnormal_money = 0;
			for($i3=0;$i3<count($result[$i]['orders_abnormal']);$i3++){
				$orders_abnormal_money+=$result[$i]['orders_abnormal'][$i3]['abnormal_money'];
			}
			$result[$i]['orders_abnormal_money'] = $orders_abnormal_money;
			
			$xiaoshoushui = $result[$i]['yfzj']-$result[$i]['orders_abnormal_money'];
			
			//获取到付款
			$result[$i]['dfk'] = $result[$i]['dfk'];
			//获取销售税
			if($result[$i]['orders_tax']>0){
				$result[$i]['xiaoshoushui'] = round($xiaoshoushui/(1+($result[$i]['orders_tax']/100))*($result[$i]['orders_tax']*0.01),2);
				
			}else{
				$result[$i]['xiaoshoushui']=0;
			}
			
			//开始获取明细
			$orders_yunfei =0;
			$orders_tihuofei=0;
			$orders_songhuofei=0;
			$orders_zhuangxiefei=0;
			$orders_dabaofei=0;
			for($o3=0;$o3<count($result[$i]['orders_money_detile']);$o3++){
				if($result[$i]['orders_money_detile'][$o3]['cost_id']==1){
					$orders_yunfei = $result[$i]['orders_money_detile'][$o3]['money'];
				}else if($result[$i]['orders_money_detile'][$o3]['cost_id']==2){
					$orders_tihuofei = $result[$i]['orders_money_detile'][$o3]['money'];
				}else if($result[$i]['orders_money_detile'][$o3]['cost_id']==3){
					$orders_songhuofei = $result[$i]['orders_money_detile'][$o3]['money'];
				}else if($result[$i]['orders_money_detile'][$o3]['cost_id']==4){
					$orders_zhuangxiefei = $result[$i]['orders_money_detile'][$o3]['money'];
				}else if($result[$i]['orders_money_detile'][$o3]['cost_id']==12){
					$orders_dabaofei = $result[$i]['orders_money_detile'][$o3]['money'];
				}
				
			}
			$result[$i]['orders_yunfei'] = $orders_yunfei;
			$result[$i]['orders_tihuofei'] = $orders_tihuofei;
			$result[$i]['orders_songhuofei'] = $orders_songhuofei;
			$result[$i]['orders_zhuangxiefei'] = $orders_zhuangxiefei;
			$result[$i]['orders_dabaofei'] = $orders_dabaofei;
			
			//获取收入 后改称开票金额
			$result[$i]['shouru'] = $result[$i]['yfzj']-$result[$i]['orders_abnormal_money'];
			//不含税收入
			$result[$i]['no_tax_money'] =$result[$i]['shouru']-$result[$i]['xiaoshoushui'];
			
			//开始获取成本明细
			$shipment_yunfei =0;
			$shipment_tihuofei=0;
			$shipment_songhuofei=0;
			$shipment_zhuangxiefei=0;
			$shipment_qibufei=0;
			$shipment_qitafei=0;
		
			for($o4=0;$o4<count($result[$i]['shipment_money_detile']);$o4++){
				if($result[$i]['shipment_money_detile'][$o4]['cost_id']==5){
					$shipment_yunfei +=$result[$i]['shipment_money_detile'][$o4]['cost_money'];
				}else if($result[$i]['shipment_money_detile'][$o4]['cost_id']==6){
					$shipment_tihuofei+=$result[$i]['shipment_money_detile'][$o4]['cost_money'];
				}else if($result[$i]['shipment_money_detile'][$o4]['cost_id']==7){
					$shipment_songhuofei+=$result[$i]['shipment_money_detile'][$o4]['cost_money'];
				}else if($result[$i]['shipment_money_detile'][$o4]['cost_id']==8){
					$shipment_zhuangxiefei+=$result[$i]['shipment_money_detile'][$o4]['cost_money'];
				}else if($result[$i]['shipment_money_detile'][$o4]['cost_id']==9){
					$shipment_qibufei+=$result[$i]['shipment_money_detile'][$o4]['cost_money'];
				}else if($result[$i]['shipment_money_detile'][$o4]['cost_id']==10){
					$shipment_qitafei+=$result[$i]['shipment_money_detile'][$o4]['cost_money'];
				}
				
			}		
			
			$result[$i]['shipment_yunfei'] = $shipment_yunfei;
			$result[$i]['shipment_tihuofei'] = $shipment_tihuofei;
			$result[$i]['shipment_songhuofei'] = $shipment_songhuofei;
			$result[$i]['shipment_zhuangxiefei'] = $shipment_zhuangxiefei;
			$result[$i]['shipment_qibufei'] = $shipment_qibufei;			
			$result[$i]['shipment_qitafei'] = $shipment_qitafei;	
			$yunfei = 0;
			$ganxianpeikuan=0;
			$ganxianxiaoxiangshui=0;
			//开始获取运费
			for($i4=0;$i4<count($result[$i]['shipment_info']);$i4++){
				$yunfei+=$result[$i]['shipment_info'][$i4]['pay_all_money'];
				$ganxianpeikuan+=$result[$i]['shipment_info'][$i4]['shipment_abnormal_money'];
				if(is_numeric($result[$i]['shipment_info'][$i4]['supplier_bill_tax'])&&$result[$i]['shipment_info'][$i4]['supplier_bill_tax']>0){
					$shui = $result[$i]['shipment_info'][$i4]['supplier_bill_tax'];
					
					$jisuanfei = $result[$i]['shipment_info'][$i4]['pay_all_money']+$result[$i]['shipment_info'][$i4]['shipment_abnormal_money'];
			
					$ganxianxiaoxiangshui =round($jisuanfei/(1+($shui/100))*($shui*0.01),2);
				}

			}
			$result[$i]['yunfei'] = $yunfei;
			//获取干线的赔款
			$result[$i]['ganxianpeikuan'] =$ganxianpeikuan;
			
			//干线开票金额
			$result[$i]['ganxiankaipiao'] =$yunfei-$ganxianpeikuan;
			//干线销售税
			$result[$i]['ganxianxiaoxiangshui'] =$ganxianxiaoxiangshui;
			//干线税后
			$result[$i]['ganxianshuihou'] = $result[$i]['ganxiankaipiao'] -$result[$i]['ganxianxiaoxiangshui'];
			//开始获取短驳费
			$duanbofei=0;
			$short_borge_supplier_name = '';
			//开始获取短驳赔款
			$duanbopeikuan = 0;
			for($i5=0;$i5<count($result[$i]['short_barge_info']);$i5++){
				$duanbofei+=$result[$i]['short_barge_info'][$i5]['short_barge_money'];
				$short_borge_supplier_name = $result[$i]['short_barge_info'][$i5]['supplier_name'];
				$duanbopeikuan+=$result[$i]['short_barge_info'][$i5]['short_barge_abnormal_money'];
			}	
			$result[$i]['short_borge_supplier_name'] = $short_borge_supplier_name;	
			$result[$i]['duanbofei'] = $duanbofei;	
			$result[$i]['duanbopeikuan'] = $duanbopeikuan;	
			$result[$i]['duanbokaipiao'] = $duanbofei-$duanbopeikuan;
		
			$result[$i]['duanbotax'] = $result[$i]['short_barge_info'][0]['supplier_bill_tax'];
			
			if(is_numeric($result[$i]['duanbotax'])&&$result[$i]['duanbotax']>0){
					$shui =$result[$i]['duanbotax'];
					
					$jisuanfei =$result[$i]['duanbokaipiao'];
			
					$duanboxiaoxiangshui =round($jisuanfei/(1+($shui/100))*($shui*0.01),2);
			}else{
				$duanboxiaoxiangshui = 0;
			}
			//短驳销项税
			$result[$i]['duanboxiaoxiangshui'] = $duanboxiaoxiangshui;
			//短驳税后
			$result[$i]['duanboshuihou'] = $result[$i]['duanbokaipiao']-$result[$i]['duanboxiaoxiangshui'];
			$result[$i]['cbhj'] = $result[$i]['ganxianshuihou']+$result[$i]['duanboshuihou'];	
			$result[$i]['lirun'] = number_format($result[$i]['no_tax_money']-$result[$i]['cbhj'],2);
			
		
			if($result[$i]['no_tax_money']==0){
				$result[$i]['maolilv'] = 0;
			}else{
				$result[$i]['maolilv'] = round($result[$i]['lirun']/$result[$i]['no_tax_money'],2);
			}
			if($pp==0){
				$array_result_new[] = $result[$i];
				
			}
		
		}
		
		$result = $array_result_new;
		$new_result =[];
		for($i=0;$i<count($result);$i++){
		
			$new_result[$i]['pickup_time'] = $result[$i]['pickup_time'];
			$new_result[$i]['send_goods_company'] = $result[$i]['send_goods_company'];
			$new_result[$i]['orders_number'] = $result[$i]['orders_number'];
			$new_result[$i]['orders_goods_name'] = $result[$i]['orders_goods_name'];
			$new_result[$i]['accept_goods_company'] = $result[$i]['accept_goods_company'];
			$new_result[$i]['accept_address'] = $result[$i]['accept_address'];
			$new_result[$i]['jfsl'] = $result[$i]['jfsl'];
			$new_result[$i]['fysl'] = $result[$i]['fysl'];
			$new_result[$i]['slc'] = $result[$i]['slc'];
			$new_result[$i]['jfjs'] = $result[$i]['jfjs'];
			$new_result[$i]['fyjs'] = $result[$i]['fyjs'];
			$new_result[$i]['jsc'] = $result[$i]['jsc'];			
			$new_result[$i]['jfzl'] = $result[$i]['jfzl'];
			$new_result[$i]['fyzl'] = $result[$i]['fyzl'];
			$new_result[$i]['zlc'] = $result[$i]['zlc'];
			$new_result[$i]['jftj'] = $result[$i]['jftj'];
			$new_result[$i]['fytj'] = $result[$i]['fytj'];
			$new_result[$i]['tjc'] = $result[$i]['tjc'];
			
			$new_result[$i]['yfzj'] = $result[$i]['yfzj'];
			$new_result[$i]['orders_tax'] = $result[$i]['orders_tax'];
			$new_result[$i]['xiaoshoushui'] = $result[$i]['xiaoshoushui'];
			$new_result[$i]['orders_abnormal_money'] = $result[$i]['orders_abnormal_money'];
			$new_result[$i]['dfk'] = $result[$i]['dfk'];
			$new_result[$i]['orders_yunfei'] = $result[$i]['orders_yunfei'];
			$new_result[$i]['orders_tihuofei'] = $result[$i]['orders_tihuofei'];
			$new_result[$i]['orders_songhuofei'] = $result[$i]['orders_songhuofei'];
			$new_result[$i]['orders_zhuangxiefei'] = $result[$i]['orders_zhuangxiefei'];
			$new_result[$i]['orders_dabaofei'] = $result[$i]['orders_dabaofei'];
			$new_result[$i]['shouru'] = $result[$i]['shouru'];
			$new_result[$i]['no_tax_money'] = $result[$i]['no_tax_money'];
			
			
			$new_result[$i]['shipment_yunfei'] = $result[$i]['shipment_yunfei'];
			$new_result[$i]['shipment_tihuofei'] = $result[$i]['shipment_tihuofei'];
			$new_result[$i]['shipment_songhuofei'] = $result[$i]['shipment_songhuofei'];
			$new_result[$i]['shipment_zhuangxiefei'] = $result[$i]['shipment_zhuangxiefei'];
			$new_result[$i]['shipment_qibufei'] = $result[$i]['shipment_qibufei'];		
			$new_result[$i]['shipment_qitafei'] = $result[$i]['shipment_qitafei'];				

			
			
			
			$new_result[$i]['yunfei'] = $result[$i]['yunfei'];
			$new_result[$i]['ganxianpeikuan'] = $result[$i]['ganxianpeikuan'];
			$new_result[$i]['ganxiankaipiao'] = $result[$i]['ganxiankaipiao'];
			$new_result[$i]['ganxianxiaoxiangshui'] = $result[$i]['ganxianxiaoxiangshui'];
			$new_result[$i]['ganxianshuihou'] = $result[$i]['ganxianshuihou'];
		
			$new_result[$i]['shipment_supplier_name'] = $result[$i]['shipment_supplier_name'];
			$new_result[$i]['duanbofei'] = $result[$i]['duanbofei'];
			$new_result[$i]['duanbopeikuan'] = $result[$i]['duanbopeikuan'];
			$new_result[$i]['duanbokaipiao'] = $result[$i]['duanbokaipiao'];
			
			$new_result[$i]['duanbotax'] = $result[$i]['duanbotax'];
			$new_result[$i]['duanboxiaoxiangshui'] = $result[$i]['duanboxiaoxiangshui'];
			$new_result[$i]['duanboshuihou'] = $result[$i]['duanboshuihou'];
			$new_result[$i]['short_borge_supplier_name'] = $result[$i]['short_borge_supplier_name'];
			
			$new_result[$i]['cbhj'] = $result[$i]['cbhj'];
			$new_result[$i]['lirun'] = $result[$i]['lirun'];
			$m = $result[$i]['maolilv']*100;
			$new_result[$i]['maolilv'] = $m."%";
			
			$maolilv_jisuan+=$m;
			$new_result[$i]['accept_location_name'] = $result[$i]['accept_location_name'];
		}
		if(count($result)==0){
			$maolilv_jisuan  = 0;
		}else{
			$maolilv_jisuan = $maolilv_jisuan/count($result);
		}
	
		
		$result = $new_result;
		//托运日期 pickup_time
		//发货方 send_goods_company
		//运单编号 orders_number
		//货物名称 orders_goods_name
		//收货方 accept_goods_company
		//收货地址 accept_address
		//计费数量 jfsl
		//发运数量 fysl
		//数量差 slc
		//计费件数 jfjs
		//发运件数fyjs
		//件数差 jsc
		//计费重量 jfzl
		//发运重量 fyzl
		//重量差 zlc
		//计费体积 jftj
		//发运体积 fytj
		//体积差 tjc
		//运费总计 yfzj
		//税率 orders_tax
		//销售税 xiaoshoushui
		//赔款 orders_abnormal_money
		//到付款 dfk
		//收入-运费	
		//收入-提货费	
		//收入-送货费	
		//收入-装卸费	
		//收入-打包费		
		//收入 shouru
		//不含税收入 no_tax_money
		//运费-发运费 shipment_yunfei
		//运费- 提货费 shipment_tihuofei
		//运费- 送货费 shipment_songhuofei
		//运费- 装卸费 shipment_zhuangxiefei
		//运费-- 起步费 shipment_qibufei
		//运费-- 其他费 shipment_qitafei
		
		
		
		//运费 yunfei
		//干线赔款 ganxianpeikuan
		//干线开票金额  ganxiankaipiao
		//干线销项税 ganxianxiaoxiangshui
		//干线税后 ganxianshuihou
		//干线承运商
		//短驳费 duanbofei
		//短驳赔款 duanbopeikuan
		//短驳开票 duanbokaipiao
		//短驳税 duanbotax
		//短驳销项税 duanboxiaoxiangshui
		//短驳税后 duanboshuihou
		//短驳承运商
		//成本合计 cbhj
		//利润 lirun
		//毛利率 maolilv
		//运输方向 accept_location_name
		

		if($params['is_export']==1){//下载
		
			return $result;
		}else{
			$r = [
				'code'=>200,
				'data'=>$result,
				'count'=>	count($result),
				'totalRow'=>[
					'maolilv'=>$maolilv_jisuan
				
				]

			];
			
			return $r;			
		}

        
    }

    //导出
    public function export(){

        $params = Request::instance()->param();
		
			
        $data = $this->tmspeibiAjax($params);
	
        $name = "营收配比表";
        $xAxis = [];
        $title = "营收配比表";
        $header = array('托运日期', '发货方', '运单编号', '货物名称', '收货方', '收货地址', '计费数量', '发运数量', '数量差', '计费件数', '发运件数', '件数差', '计费重量', '发运重量', '重量差', '计费体积', '发运体积', '体积差', '运费总计', '税率', '销售税', '赔款','到付款', '收入-运费','收入-提货费','收入-送货费','-收入-装卸费','收入-打包费','收入', '不含税收入', '成本-运费','成本-提货费','成本-送货费','成本-装卸费','成本-起步费','成本-其他费','成本运费总计','干线赔款','干线开票金额','干线销项税','干线税后','发运承运商', '短驳费','短驳赔款','短驳开票', '短驳税','短驳销项税','短驳税后','短驳承运商','成本合计','利润','毛利率', '运输方向');

        //导出excel
        $excel = Demo::export_accept_goods($xAxis, $title, $header, $data, $name);

    }
}

