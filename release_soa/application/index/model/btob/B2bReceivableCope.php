<?php
namespace app\index\model\btob;
use think\Model;
class B2bReceivableCope extends Model{

    protected $table = 'b2b_receivable_cope';
    private $_languageList;
    public function initialize()
    {
    	$this->_languageList = config('systom_setting')['language_list'];
    	parent::initialize();
    
    }

    public function addB2bReceivableCope($params)
    {
        $data['fee_type_type'] = $params['fee_type_type'];
        $data['fee_type_code'] = $params['fee_type_code'];
        $data['supplier_company_id'] = $params['supplier_company_id'];
        $data['receivable_cope_name'] = $params['receivable_cope_name'];
        $data['total_price'] = $params['total_price'];
        $data['currency_id'] = $params['currency_id'];
        $data['receivable_cope_type'] = $params['receivable_cope_type'];
        $data['status'] = 1;
    	try{
            $info = $this->where(['erp_number'=> $params['erp_number']])->find();
            if (empty($info))
            {

                //添加
                if ($params['receivable_cope_type'] == 1)
                {
                    $data['number'] = 'S-';
                }

                if ($params['receivable_cope_type'] == 2)
                {
                    $data['number'] = 'F-';
                }
                $data['number'] = $data['number'] . ($this->count() + 1);
                $data['create_user_id']= $data['update_user_id'] = $params['now_user_id'];
                $data['created_at'] = $data['updated_at'] = time();
                $data['erp_number'] = $params['erp_number'];
                $data['b2b_booking_id'] = $params['b2b_booking_id'];
                $this->insertGetId($data);
            }
            else
            {
                //修改
                $where['erp_number'] = $params['erp_number'];
                $where['b2b_booking_id'] = $params['b2b_booking_id'];
                $data['update_user_id'] = $params['now_user_id'];
                $data['updated_at'] = time();
                $this->where($where)->update($data);
            }

            //影响到invoice

    		$result = 1;
    
    	} catch (\Exception $e) {
    	    $this->rollback();
    		$result = $e->getMessage();
    		\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		exit();
    
    	}
    
    	return $result;
    }

    public function getB2bReceivableCope($params)
    {
        $where = " 1=1 ";
        if (!empty($params['b2b_booking_id']))
        {
            $where .= " and b2b_booking_id = ". $params['b2b_booking_id'];
        }

        if (!empty($params['number']))
        {
            $where .= " and number = ". $params['number'];
        }

        $result = $this->table("b2b_receivable_cope")->alias("b2b_receivable_cope")
            ->where($where)
            ->field([
                'b2b_receivable_cope.*',
                "(select company_name  from company where b2b_receivable_cope.supplier_company_id = company.company_id)"=>'company_name',
                ])->select();
        return $result;
    }



}