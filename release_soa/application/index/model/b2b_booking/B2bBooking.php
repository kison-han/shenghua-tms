<?php

namespace app\index\model\b2b_booking;
use app\index\model\branchcompany\CompanyOrder;
use app\index\model\branchcompany\CompanyOrderAnnex;
use app\index\model\branchcompany\CompanyOrderCustomer;
use app\index\model\branchcompany\Customer;
use app\index\model\btob\B2bReceivableCope;
use app\index\model\finance\Receivable;
use think\Exception;
use think\Model;
use think\Db;
class B2bBooking extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'b2b_booking';
    private $_languageList;
    private $_public_service;
    public function initialize()
    {
    	$this->_languageList = config('systom_setting')['language_list'];
    	parent::initialize();
    
    }

    /**
     * 添加供应商
     * 胡
     */
    public function addB2bBooking($params){

        $data['btb_tour_id'] = $params['btb_tour_id'];

        $data['agent_id'] = $params['agent_id'];
        $data['language_id'] = $params['language_id'];
        $data['status'] = 1;

        $company_order = new CompanyOrder();


        if ($params['agent_reference_id'])
        {
            $data['agent_reference_id'] = $params['agent_reference_id'];        //代理商相关ID
        }


        if ($params['tour_voucher'])
        {
            $data['tour_voucher'] = $params['tour_voucher'];        //UPLOAD TOUR VOUCHE
        }

        if ($params['bus_no'])
        {
            $data['bus_no'] = $params['bus_no'];
        }

        if ($params['seat_group'])
        {
            $data['seat_group'] = $params['seat_group'];
        }


        if ($params['emergency_name'])
        {
            $data['emergency_name'] = $params['emergency_name'];      //代理商联系人
        }

        if ($params['emergency_phone'])
        {
            $data['emergency_phone'] = $params['emergency_phone'];      //代理商联系电话
        }

        if ($params['emergency_email'])
        {
            $data['emergency_email'] = $params['emergency_email'];      //代理商邮箱
        }

        if ($params['lead_pax_mobile'])
        {
            $data['lead_pax_mobile'] = $params['lead_pax_mobile'];      //游客联系人
        }

        if ($params['tour_guide'])
        {
            $data['tour_guide'] = $params['tour_guide'];
        }

        if ($params['tour_contact'])
        {
            $data['tour_contact'] = $params['tour_contact'];
        }

        if ($params['is_pre_paid'])
        {
            $data['is_pre_paid'] = $params['is_pre_paid'];
        }

        if ($params['special_requests'])
        {
            $data['special_requests'] = $params['special_requests'];
        }

        if ($params['office_status'])
        {
            $data['office_status'] = $params['office_status'];
        }

        $data['booking_status'] = $params['booking_status']?:2;

        if ($params['payment_status'])
        {
            $data['payment_status'] = $params['payment_status'];
        }

        if ($params['remark'])
        {
            $data['remark'] = $params['remark'];
        }

    	Db::startTrans();
    	try{
            $data['company_order_number'] = $company_order->addB2bOrder($params);


    		$b2b_booking_id = Db::name('b2b_booking')->insertGetId($data);
            $params['b2b_booking_id'] = $b2b_booking_id;
    		//游客
            $company_order_customer_model = new CompanyOrderCustomer();
            $customer_model = new Customer();
            $company_order_customer = [];
            foreach ($params['customer'] as $customer_v)
            {
                $customer_info = $customer_model->getOneCustomerByPassport($customer_v['passport_number']);
                $customer_v['now_user_id'] = $params['now_user_id'];
                $customer_v['company_order_number'] = $data['company_order_number'];
                $customer_v['birthday'] = date('Y-m-d',strtotime($customer_v['birthday']));
                if ($customer_info)
                {
                    //有 修改
                    $customer_v['customer_id'] = $customer_info['customer_id'];
                    $customer_model->updateCustomerByCustomerId($customer_v);
                }
                else
                {
                    //无 添加
                    $customer_id = $customer_model->addCustomer($customer_v);
                    $customer_v['customer_id'] = $customer_id;
                }
                $customer_v['b2b_booking_id'] = $b2b_booking_id;
                $company_order_customer[] = $company_order_customer_model->addB2bOrderCustomer($customer_v);
            }

            //房间
            $b2b_booking_room_model = new B2bBookingRoom();
            foreach ($params['room'] as $room_v)
            {
                $room_v['b2b_booking_id'] = $b2b_booking_id;
                $b2b_booking_room_model->addB2bBookingRoom($room_v);
            }
            //接送机
            $b2b_booking_transfer_model = new B2bBookingTransfer();
            foreach ($params['transfer'] as $transfer_v)
            {
                $transfer_v['b2b_booking_id'] = $b2b_booking_id;
                $b2b_booking_transfer_model->addB2bBookingTransfer($transfer_v);
            }

            //提前延后
            $b2b_booking_accommodation_model = new B2bBookingAccommodation();
            foreach ($params['accommodation'] as $accommodation_v)
            {
                $accommodation_v['b2b_booking_id'] = $b2b_booking_id;
                $b2b_booking_accommodation_model->addB2bBookingAccommodation($accommodation_v);
            }


            //附件
            $annex_model = new CompanyOrderAnnex();
            foreach ($params['upload_passport'] as $upload_passport_v)
            {
                $arr = [];
                $arr['company_order_number'] = $data['company_order_number'];
                $arr['url'] = $upload_passport_v['url'];
                $arr['annex_name'] = $upload_passport_v['annex_name'];
                $arr['create_user_id'] = $params['now_user_id'];
                $annex_model->addB2bAnnex($arr);
            }

            //明细
            $b2b_booking_sales_model = new B2bBookingSales();
            $params['myob_cost'] = $params['myob_sales'] = 0.00;
            foreach ($params['sales'] as $sales_v)
            {
                $sales_v['b2b_booking_id'] = $b2b_booking_id;
                $b2b_booking_sales_model->addB2bBookingSales($sales_v);
                if ($sales_v['net_total'] > 0.00)
                {
                    $params['myob_sales'] = bcadd($sales_v['net_total'], $params['myob_sales'],2);
                }
                else
                {
                    $params['myob_cost'] = bcadd($sales_v['net_total'], $params['myob_cost'],2);
                }
            }

            //erp产生一条应收
            $receviable_model = new Receivable();
            $params['order_number'] = $data['company_order_number'];
            $params['receivable_money'] = $params['cope']['total_price'];
            $params['receivable_currency_id'] = $params['cope']['currency_id'];
            $params['is_auto'] = 2;
            $params['fee_type_code'] = 201;
            $params['fee_type_type'] = 34;
            $params['company_id'] = $params['user_company_id'];
            $params['company_order_customer_id'] = implode(',', $company_order_customer);

            $receivable_cope = new B2bReceivableCope();
            //b2b应付
            $params['cope']['erp_number'] = $receviable_model->addReceivable($params);
            $params['cope']['fee_type_type'] = 109;
            $params['cope']['fee_type_code'] = 316;
            $params['cope']['receivable_cope_type'] = 2;
            $params['cope']['b2b_booking_id'] = $b2b_booking_id;
            $params['cope']['total_price'] = bcmul(count($params['customer']), $params['cope']['price'],2);
            $params['cope']['now_user_id'] = $params['now_user_id'];
            $params['cope']['receivable_cope_name'] = '团费';
            $receivable_cope->addB2bReceivableCope($params['cope']);

            //invoice
            $params['total_cost'] = bcsub($params['myob_cost'], $params['cope']['total_price'],2);
            $params['total_sales'] = $params['myob_sales'];
            $b2b_booking_invoice_model = new B2bBookingInvoice();
            $b2b_booking_invoice_model->addB2bBookingInvoice($params);

            $result['b2b_booking_id'] = $b2b_booking_id;
            $result['company_order_number'] = $data['company_order_number'];
            $result['branch_product_number'] = $params['branch_product_number'];

            $voucher = $result['b2b_booking_id'].'-'.$data['btb_tour_id'];

            $this->where(['b2b_booking_id' => $result['b2b_booking_id']])->update(['voucher_number' => $voucher]);
            $result['branch_product_name'] = $params['product_name'];
            $result['route_template_number'] = $params['route_template_number'];
            $result['team_product_id'] = $params['team_product_id'];
    		// 提交事务
    		Db::commit();

    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		exit();
    	}
    	
    	return $result;
    }

    public function getB2bBooking($params,$is_count=false,$is_page=false,$page=null,$page_size=20)
    {
        $data = "1=1 and company_order.order_type = 3 and b2b_booking.status=1 ";
        //订单编号
        if(!empty($params['company_order_number']))
        {
            $data.= " and company_order.company_order_number= ".$params['company_order_number'];
        }

        //创建时间
        if(!empty($params['create_time']))
        {
            $data.= " and company_order.create_time > ". $params['create_time'] . " and company_order.create_time < " . ($params['create_time'] + 86400);
        }
        //代理商
        if(!empty($params['agent_name']))
        {
            $data.= " and distributor.distributor_name like '%".$params['agent_name']."%' and distributor.part=2";
        }
        //代理商编号
        if(!empty($params['agent_code'])){
            $data.= " and distributor.distributor_code like '%".$params['agent_code']."%'";
        }

        //出发日期
        if(!empty($params['begin_time']))
        {
            $data.= " and company_order.begin_time = ".$params['begin_time'];
        }
        //处理状态
        if(!empty($params['office_status']))
        {
            $data.= " and b2b_booking.office_status = ".$params['office_status'];
        }
        //订单状态
        if(!empty($params['booking_status']))
        {
            $data.= " and b2b_booking.booking_status in ({$params['booking_status']})";
        }
        //付款状态
        if(!empty($params['payment_status']))
        {
            $data.= " and b2b_booking.payment_status = ".$params['payment_status'];
        }

        //公司id
        if(!empty($params['company_id']))
        {
            $data.= " and company_order.company_id = ".$params['company_id'];
        }

        //代理id
        if(!empty($params['agent_id']))
        {
            $data.= " and b2b_booking.agent_id = ".$params['agent_id'];
        }

        //booking_id
        if(!empty($params['b2b_booking_id']))
        {
            $data.= " and b2b_booking.b2b_booking_id = ".$params['b2b_booking_id'];
        }

        //agent_reference_id
        if(!empty($params['agent_reference_id']))
        {
            $data.= " and b2b_booking.agent_reference_id = ".$params['agent_reference_id'];
        }

        //consultant的id
        if(!empty($params['consultant']))
        {
            $data.= " and b2b_booking.consultant = ".$params['consultant'];
        }

       

        try
        {
            if($is_count==true){
                 $result = $this->table("b2b_booking")->alias('b2b_booking')->
                    join("company_order", "company_order.company_order_number = b2b_booking.company_order_number")->
                    join("b2b_tour", "b2b_tour.btb_tour_id = b2b_booking.btb_tour_id")->
                    join("distributor", "distributor.distributor_id = b2b_booking.agent_id")->
                    join("b2b_booking_invoice", "b2b_booking_invoice.b2b_booking_id = b2b_booking.b2b_booking_id")->
                    where($data)->count();
            }else {
                if ($is_page == true) {
                    $result = $this->table("b2b_booking")->alias('b2b_booking')->
                    join("company_order", "company_order.company_order_number = b2b_booking.company_order_number")->
                    join("b2b_tour", "b2b_tour.btb_tour_id = b2b_booking.btb_tour_id")->
                    join("distributor", "distributor.distributor_id = b2b_booking.agent_id")->
                    join("b2b_booking_invoice", "b2b_booking_invoice.b2b_booking_id = b2b_booking.b2b_booking_id")->
                    where($data)->limit($page, $page_size)->order('company_order.create_time desc')->
                    field(['b2b_booking.*', 'distributor.distributor_name', 'distributor.distributor_code', 'company_order.create_time', 'b2b_booking_invoice.invoice_number', 'b2b_booking_invoice.balance_due',
                        "(select count(company_order_customer_id) from company_order_customer where company_order_customer.b2b_booking_id = b2b_booking.b2b_booking_id and company_order_customer.type=2)"=>'customer_count', 'b2b_tour.tour_name', 'b2b_tour.tour_name_ch', 'b2b_tour.tour_code', 'company_order.begin_time'
                    ])->select();
                }else{
                    $result = $this->table("b2b_booking")->alias('b2b_booking')->
                    join("company_order", "company_order.company_order_number = b2b_booking.company_order_number")->
                    join("b2b_tour", "b2b_tour.btb_tour_id = b2b_booking.btb_tour_id")->
                    join("distributor", "distributor.distributor_id = b2b_booking.agent_id")->
                    join("b2b_booking_invoice", "b2b_booking_invoice.b2b_booking_id = b2b_booking.b2b_booking_id")->
                    where($data)->order('company_order.create_time desc')->
                    field(['b2b_booking.*', 'distributor.distributor_name', 'distributor.distributor_code', 'company_order.create_time', 'b2b_booking_invoice.invoice_number', 'b2b_booking_invoice.balance_due',
                        "(select count(company_order_customer_id) from company_order_customer where company_order_customer.b2b_booking_id = b2b_booking.b2b_booking_id and company_order_customer.type=2)"=>'customer_count', 'b2b_tour.tour_name', 'b2b_tour.tour_name_ch', 'b2b_tour.tour_code', 'company_order.begin_time'
                    ])->select();
                }
            }
        }
        catch (Exception $e)
        {
            $result = $e->getMessage();
            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }
    
        $sql = $this->table('b2b_booking')->getLastSql();
     

        return $result;
    }


    public function updateB2bBookingGeneral($params){
        if ($params['agent_id'])
        {
            $data['agent_id'] = $params['agent_id'];        //代理商id
        }
        if ($params['agent_reference_id'])
        {
            $data['agent_reference_id'] = $params['agent_reference_id'];        //代理商相关ID
        }

        if ($params['language_id'])
        {
            $data['language_id'] = $params['language_id'];        //language_id
        }

        if ($params['tour_voucher'])
        {
            $data['tour_voucher'] = $params['tour_voucher'];        //UPLOAD TOUR VOUCHE
        }

        if ($params['bus_no'])
        {
            $data['bus_no'] = $params['bus_no'];
        }

        if ($params['seat_group'])
        {
            $data['seat_group'] = $params['seat_group'];
        }

        if ($params['emergency_name'])
        {
            $data['emergency_name'] = $params['emergency_name'];      //代理商联系人
        }

        if ($params['emergency_phone'])
        {
            $data['emergency_phone'] = $params['emergency_phone'];      //代理商联系电话
        }

        if ($params['emergency_email'])
        {
            $data['emergency_email'] = $params['emergency_email'];      //代理商邮箱
        }

        if ($params['lead_pax_mobile'])
        {
            $data['lead_pax_mobile'] = $params['lead_pax_mobile'];      //游客联系人
        }

        if ($params['tour_guide'])
        {
            $data['tour_guide'] = $params['tour_guide'];
        }

        if ($params['tour_contact'])
        {
            $data['tour_contact'] = $params['tour_contact'];
        }

        if ($params['is_pre_paid'])
        {
            $data['is_pre_paid'] = $params['is_pre_paid'];
        }

        if ($params['special_requests'])
        {
            $data['special_requests'] = $params['special_requests'];
        }

        if ($params['office_status'])
        {
            $data['office_status'] = $params['office_status'];
        }

        if ($params['booking_status'])
        {
            $data['booking_status'] = $params['booking_status'];
        }

        if ($params['payment_status'])
        {
            $data['payment_status'] = $params['payment_status'];
        }

        if ($params['remark'])
        {
            $data['remark'] = $params['remark'];
        }

        if ($params['hotel_first_day'])
        {
            $data['hotel_first_day'] = $params['hotel_first_day'];
        }

        if ($params['hotel_last_day'])
        {
            $data['hotel_last_day'] = $params['hotel_last_day'];
        }

        if ($params['hotel_both_day'])
        {
            $data['hotel_both_day'] = $params['hotel_both_day'];
        }

        if ($params['options_id'])
        {
            $data['options_id'] = $params['options_id'];
        }

        $company_order_data['update_time'] = time();
        $company_order_data['update_user_id'] = $params['now_user_id'];
        if ($params['begin_time'])
        {
            $company_order_data['begin_time'] = $params['begin_time'];
        }

        $where = " b2b_booking_id = ". $params['b2b_booking_id'];

    	$this->startTrans();
    	try{
    		Db::name('b2b_booking')->where($where)->update($data);
    		$b2b_booking = $this->where($where)->find();
            Db::name('company_order')->where(['company_order_number' => $b2b_booking['company_order_number']])->update($company_order_data);

    		$result = 1;
    		// 提交事务
            $this->commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
            $this->rollback();
    
    	}
    	return $result;
    }


    public function getB2bBookingGeneralById($b2b_booking_id)
    {
        try
        {
            $where['b2b_booking_id'] = $b2b_booking_id;
            $result = $this->table("b2b_booking")->alias('b2b_booking')->
            join("company_order", "company_order.company_order_number = b2b_booking.company_order_number")->
            join("b2b_tour", "b2b_tour.btb_tour_id = b2b_booking.btb_tour_id")->
            where($where)->
            field([
                'b2b_booking.*', 'company_order.begin_time', 'company_order.create_time', 'company_order.update_time',
                'b2b_tour.tour_name', 'b2b_tour.tour_name_ch', 'b2b_tour.tour_code',
                "(select nickname from user where user.user_id = company_order.create_user_id)"=>'create_user_name',
                "(select nickname from user where user.user_id = company_order.update_user_id)"=>'update_user_name',
                "(select count(b2b_tour_itinerary_id) from b2b_tour_itinerary where b2b_tour_itinerary.btb_tour_id = b2b_tour.btb_tour_id and b2b_tour_itinerary.status = 1)"=>'itinerary_count',
                "(select language_name from language where language.language_id = b2b_booking.language_id)"=>'language_name'
            ])->find();
        }
        catch (Exception $e)
        {
            $result = $e->getMessage();
            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }

        return $result;
    }

}