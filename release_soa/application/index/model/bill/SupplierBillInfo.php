<?php

namespace app\index\model\bill;

use think\Db;
use think\Model;
use app\common\help\Help;
use app\index\model\bill\CustomerBillInfo;
use app\index\model\orders\Orders;
use app\index\model\orders\OrdersFinance;

class SupplierBillInfo extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'supplier_bill_info';


    /**
     * 获取承运商账单明细
     * 胡
     */
    public function getSupplierBillInfo($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    
    
    	$data = "1=1 ";

    	if(!empty($params['supplier_bill_number'])){
    		$data.= " and supplier_bill_info.supplier_bill_number= '".$params['supplier_bill_number']."'";
    	}

        if($is_count==true){
            $result = $this->table("supplier_bill_info")->alias("supplier_bill_info")->

            where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("supplier_bill_info")->alias('supplier_bill_info')->

                where($data)->limit($page, $page_size)->order('supplier_bill_info.create_time desc')->
                field([
                		'supplier_bill_info.*',

		                ])->select();
            }else{
                $result = $this->table("supplier_bill_info")->alias('supplier_bill_info')->

                where($data)->
                field([
                		'supplier_bill_info.*',

                    	])->select();
            }
        }
    	 
 
    
    	return $result;
    
    }

}