<?php
/**
 * Created by PhpStorm.
 * User: Hugh
 * Date: 2019/11/04
 * Time: 13:40
 */

namespace app\index\controller;
use \Underscore\Types\Arrays;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
use app\common\help\Help;

class CommissionTable extends BookingBase
{
    public function index()
    {

        $data['status'] = 1;
        $data['company_id'] =  session('booking_user')['company_id'];
        $result = $this->callSoaErp('post', '/btob/getCommission',$data);
        $this->assign('tables',$result['data']);
//        var_dump($result);exit;
        $this->assign('site_title','Commission');

        return $this->fetch('index');
    }

}