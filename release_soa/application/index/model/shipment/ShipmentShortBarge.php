<?php

namespace app\index\model\shipment;
use think\Model;
use app\common\help\Help;

use think\config;
use think\Db;
class ShipmentShortBarge extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'shipment_short_barge';

    public function initialize()
    {
    

    	parent::initialize();
    
    }


    /**
     * 获取短驳详情
     * 胡
     */
    public function getShipmentShortBargeInfo($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    
    
    	$data = "1=1 ";

    	if(is_numeric($params['status'])){
    		$data.= " and shipment_short_barge.status= ".$params['status'];
    	}   
		if(is_numeric($params['start_shipment_time'])){
    		$data.= " and shipment.shipment_time>=".$params['start_shipment_time'];
    	}  
		if(is_numeric($params['end_shipment_time'])){
    		$data.= " and shipment.shipment_time<=".$params['end_shipment_time'];
    	}
/* 		
		if(is_numeric($params['is_confirm'])){
    		$data.= " and shipment_short_barge.is_confirm=".$params['is_confirm'];
    	} 
*/		
    	if(!empty($params['orders_number'])){
    		$data.= " and shipment_short_barge.orders_number= '".$params['orders_number']."'";
    	}
    	if(!empty($params['shipment_uuid'])){
    		$data.= " and shipment_short_barge.shipment_uuid= '".$params['shipment_uuid']."'";
    	}	
    	if(!empty($params['supplier_uuid'])){
    		$data.= " and shipment.supplier_uuid= '".$params['supplier_uuid']."'";
    	}			
    	if(!empty($params['send_goods_company'])){
    		$data.= " and orders.send_goods_company like '%".$params['send_goods_company']."%'";
    	}  
    	if(!empty($params['finance_number'])){
    		$data.= " and shipment.finance_number like '%".$params['finance_number']."%'";
    	}  		
    	if(is_numeric($params['project_id'])){
    		$data.= " and orders.project_id =".$params['project_id'];
    	}  		
        if($is_count==true){
            $result = $this->table("shipment_short_barge")->alias("shipment_short_barge")->
			join("orders","shipment_short_barge.orders_number = orders.orders_number")->
			join('shipment','shipment.shipment_uuid=shipment_short_barge.shipment_uuid')->
            where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("shipment_short_barge")->alias('shipment_short_barge')->
				join("orders","shipment_short_barge.orders_number = orders.orders_number")->
				join('shipment','shipment.shipment_uuid=shipment_short_barge.shipment_uuid')->
				join('supplier','supplier.supplier_uuid=shipment.supplier_uuid')->
				
                where($data)->limit($page, $page_size)->order('shipment_short_barge.create_time desc')->
                field([
                		'shipment_short_barge.*','orders.send_location_id','orders.accept_location_id',
                		'orders.pickup_time','orders.send_goods_company','orders.accept_goods_company',
                		'orders.send_province_id','orders.send_city_id','orders.send_area_id',
                		'orders.accept_province_id','orders.accept_city_id','orders.accept_area_id',
     					'orders.send_address','orders.orders_id','orders.remark as orders_remark',
                		'shipment.shipment_remark',
                		'shipment.shipment_time','shipment.finance_number','shipment.shipment_type',
            		  	'supplier.supplier_name as short_barge_supplier_name',         
						'(select project_name from project where project.project_id = orders.project_id)'=>'project_name',						
						'(select supplier_name from supplier where supplier.supplier_uuid = shipment.supplier_uuid)'=>'shipment_supplier_name',
	                	"(select nickname  from user where user.user_id = shipment_short_barge.create_user_id)"=> 'create_user_name',
	                    "(select nickname  from user where user.user_id = shipment_short_barge.update_user_id)"=> 'update_user_name'
                		
		                ])->select();
						
				for($i=0;$i<count($result);$i++){
					$result[$i]['shipment_goods'] =Db::query("select shipment_goods.*,(select goods_name from goods where goods.goods_id = shipment_goods.goods_id) as goods_name from shipment_goods  where shipment_goods.status = 1 and shipment_uuid in (select shipment_uuid from shipment where shipment.status = 1 and orders_number = '".$result[$i]['orders_number']."')");
					
				}		
            }else{
                $result = $this->table("shipment_short_barge")->alias('shipment_short_barge')->
				join("orders","shipment_short_barge.orders_number = orders.orders_number")->
				join('shipment','shipment.shipment_uuid=shipment_short_barge.shipment_uuid')->
				join('supplier','supplier.supplier_uuid=shipment.supplier_uuid')->			
                where($data)->order('shipment_short_barge.create_time desc')->
                field([
                		'shipment_short_barge.*','orders.send_location_id','orders.accept_location_id',
                		'orders.pickup_time','orders.send_goods_company','orders.accept_goods_company',
                		'orders.send_province_id','orders.send_city_id','orders.send_area_id',
                		'orders.accept_province_id','orders.accept_city_id','orders.accept_area_id',
     					'orders.send_address','orders.orders_id','orders.remark as orders_remark',
                		'shipment.shipment_remark',
                		'shipment.shipment_time','shipment.finance_number','shipment.shipment_type',
            		  	'supplier.supplier_name as short_barge_supplier_name',
						'(select project_name from project where project.project_id = orders.project_id)'=>'project_name',	
                		'(select supplier_name from supplier where supplier.supplier_uuid = shipment.supplier_uuid)'=>'shipment_supplier_name',
                
	                	"(select nickname  from user where user.user_id = shipment_short_barge.create_user_id)"=> 'create_user_name',
	                    "(select nickname  from user where user.user_id = shipment_short_barge.update_user_id)"=> 'update_user_name'
                		
                ])->select();
				for($i=0;$i<count($result);$i++){
					$result[$i]['shipment_goods'] =Db::query("select shipment_goods.*,(select goods_name from goods where goods.goods_id = shipment_goods.goods_id) as goods_name from shipment_goods  where shipment_goods.status = 1 and shipment_uuid in (select shipment_uuid from shipment where shipment.status = 1 and orders_number = '".$result[$i]['orders_number']."')");
					
				}			
            }
        }
    	 
 
    
    	return $result;
    
    }

    /**
     * 获取短驳
     * 胡
     */
    public function getShipmentShortBarge($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    
    
    	$data = "shipment.shipment_type=3 ";

    	if(is_numeric($params['status'])){
    		$data.= " and shipment.status= ".$params['status'];
    	}   
    	if(is_numeric($params['project_id'])){
    		$data.= " and project.project_id= ".$params['project_id'];
    	}

    	if(!empty($params['finance_number'])){
    		$data.= " and shipment.finance_number= '".$params['finance_number']."'";
    	}  	
  
    	if(!empty($params['supplier_uuid'])){
    		$data.= " and shipment.supplier_uuid= '".$params['supplier_uuid']."'";
    	}  	
    	if(!empty($params['shipment_uuid'])){
    		$data.= " and shipment.shipment_uuid= '".$params['shipment_uuid']."'";
    	}  
      	if(is_numeric($params['start_shipment_time'])){
    		$data.=" and shipment.shipment_time >= ".$params['start_shipment_time'];
    	}
    	if(is_numeric($params['end_shipment_time'])){
    		$data.=" and shipment.shipment_time <= ".$params['end_shipment_time'];
    	}
    	if(is_numeric($params['choose_company_id'])){
    		$data.=" and shipment.company_id = ".$params['choose_company_id'];
    	}		 	
        if($is_count==true){
            $result = $this->table("shipment")->alias("shipment")->
			
                join("supplier","supplier.supplier_uuid = shipment.supplier_uuid")->
            where($data)->count();
			
        }else {
            if ($is_page == true) {
                $result = $this->table("shipment")->alias('shipment')->
	
			
                join("supplier","supplier.supplier_uuid = shipment.supplier_uuid")->
                where($data)->limit($page, $page_size)->order('shipment.create_time desc')->
                field([
                		'shipment.shipment_uuid','shipment.finance_number','shipment.orders_number',
                		'shipment.shipment_time','shipment.need_time','shipment.supplier_uuid','shipment.supplier_line_uuid',
                		'shipment.supplier_shipment_number','shipment.shipment_type','shipment.pay_all_money','shipment.shipment_remark',
                		'shipment.create_time','shipment.create_user_id','shipment.update_time','shipment.update_user_id',
                		'shipment.status','shipment.is_print','shipment.shipment_finance_status',
						'shipment.distribution_type',
                	
						"supplier.supplier_name","supplier.cellphone","supplier.linkman","supplier.phone",'supplier.supplier_type',
       
	                	"(select nickname  from user where user.user_id = shipment.create_user_id)"=> 'create_user_name',
	                    "(select nickname  from user where user.user_id = shipment.update_user_id)"=> 'update_user_name'
                		
		                ])->select();
            }else{
                $result = $this->table("shipment")->alias('shipment')->

                join("supplier","supplier.supplier_uuid = shipment.supplier_uuid")->
                where($data)->order('create_time desc')->
                field([
                		'shipment.shipment_uuid','shipment.finance_number','shipment.orders_number',
                		'shipment.shipment_time','shipment.need_time','shipment.supplier_uuid','shipment.supplier_line_uuid',
                		'shipment.supplier_shipment_number','shipment.shipment_type','shipment.pay_all_money','shipment.shipment_remark',
                		'shipment.create_time','shipment.create_user_id','shipment.update_time','shipment.update_user_id',
                		'shipment.status','shipment.is_print','shipment.shipment_finance_status',
						'shipment.distribution_type',
                	
                		"supplier.supplier_name","supplier.cellphone","supplier.linkman","supplier.phone",'supplier.supplier_type',

	                	"(select nickname  from user where user.user_id = shipment.create_user_id)"=> 'create_user_name',
	                    "(select nickname  from user where user.user_id = shipment.update_user_id)"=> 'update_user_name'
                    	])->select();
            }
        }
    	 

    
    	return $result;
    
    }


    /**
     * 修改发运
     */
    public function updateShipment($params){
    
    	$t = time();



    	
    	if(is_numeric($params['shipment_time'])){
    		$data['shipment_time'] = $params['shipment_time'];
    	}
    	if(is_numeric($params['need_time'])){
    		$data['need_time'] = $params['need_time'];
    	}
    	if(isset($params['supplier_uuid'])){
    		$data['supplier_uuid'] = $params['supplier_uuid'];
    	}
    	if(isset($params['supplier_line_uuid'])){
    		$data['supplier_line_uuid'] = $params['supplier_line_uuid'];
    	}
    	$data['shipment_type'] = $params['shipment_type'];
    	$data['orders_number'] = $params['orders_number'];
    	$data['supplier_shipment_number'] = $params['supplier_shipment_number'];
    	$data['pay_all_money'] = $params['pay_all_money'];
    	$data['shipment_remark'] = $params['shipment_remark'];

    	$data['update_time'] = $t;

    	$data['update_user_id'] = $params['user_id'];



 
    	Db::startTrans();
    	try{

    		Db::name('shipment')->where("shipment_uuid='".$params['shipment_uuid']."'")->update($data);
			$statusParams['status'] = 0;
			//开始插入成本
			
    		Db::name('shipment_cost')->where("shipment_uuid='".$params['shipment_uuid']."'")->update($statusParams);
    		
    		$shipmentCost = $params['shipment_cost'];

    		for($i=0;$i<count($shipmentCost);$i++){
    			$shipmentCostParams['shipment_uuid'] = $params['shipment_uuid'];
    			$shipmentCostParams['shipment_cost_uuid'] = Help::getUuid();
    			$shipmentCostParams['cost_id'] = $shipmentCost[$i]['cost_id'];
    			$shipmentCostParams['cost_money'] = $shipmentCost[$i]['cost_money'];
    			$shipmentCostParams['create_time'] = $t;
    			$shipmentCostParams['update_time'] = $t;
    			$shipmentCostParams['create_user_id'] = $params['user_id'];
    			$shipmentCostParams['update_user_id'] = $params['user_id'];
    			$shipmentCostParams['status'] = 1;
    			Db::name('shipment_cost')->insertGetId($shipmentCostParams);
    		}
    		//开始插入商品
    		Db::name('shipment_goods')->where("shipment_uuid='".$params['shipment_uuid']."'")->update($statusParams);
    		$shipmentGoods = $params['shipment_goods'];
    		for($i=0;$i<count($shipmentGoods);$i++){
    			$shipmentGoodsParams['shipment_uuid'] = $params['shipment_uuid'];
    			$shipmentGoodsParams['shipment_goods_uuid'] = Help::getUuid();
    			$shipmentGoodsParams['goods_id'] = $shipmentGoods[$i]['goods_id'];
    			$shipmentGoodsParams['shipment_count'] = $shipmentGoods[$i]['shipment_count'];
    			$shipmentGoodsParams['shipment_pack_unit'] = $shipmentGoods[$i]['shipment_pack_unit'];
    			$shipmentGoodsParams['shipment_pack_count'] = $shipmentGoods[$i]['shipment_pack_count'];    	
    			$shipmentGoodsParams['shipment_weight'] = $shipmentGoods[$i]['shipment_weight'];
    			$shipmentGoodsParams['shipment_volume'] = $shipmentGoods[$i]['shipment_volume'];
    			
    			$shipmentGoodsParams['shipment_charge_type'] = $shipmentGoods[$i]['shipment_charge_type'];
    			$shipmentGoodsParams['unit_price'] = $shipmentGoods[$i]['unit_price'];
    			$shipmentGoodsParams['shipment_money'] = $shipmentGoods[$i]['shipment_money'];    			
    			$shipmentGoodsParams['create_time'] = $t;
    			$shipmentGoodsParams['update_time'] = $t;
    			$shipmentGoodsParams['create_user_id'] = $params['user_id'];
    			$shipmentGoodsParams['update_user_id'] = $params['user_id'];
    			$shipmentGoodsParams['status'] = 1;
    			Db::name('shipment_goods')->insertGetId($shipmentGoodsParams);
    		}   		
    		//开始插入支付方式
    		Db::name('shipment_pay')->where("shipment_uuid='".$params['shipment_uuid']."'")->update($statusParams);
    		$shipmentPay = $params['shipment_pay'];
    		for($i=0;$i<count($shipmentPay);$i++){
    			$shipmentPayParams['shipment_uuid'] = $params['shipment_uuid'];
    			$shipmentPayParams['shipment_pay_uuid'] = Help::getUuid();
    			$shipmentPayParams['pay_type'] = $shipmentPay[$i]['pay_type'];
    			$shipmentPayParams['pay_money'] = $shipmentPay[$i]['pay_money'];
    			$shipmentPayParams['create_time'] = $t;
    			$shipmentPayParams['update_time'] = $t;
    			$shipmentPayParams['create_user_id'] = $params['user_id'];
    			$shipmentPayParams['update_user_id'] = $params['user_id'];
    			$shipmentPayParams['status'] = 1;
    			Db::name('shipment_pay')->insertGetId($shipmentPayParams);
    		}    		
    		

    		$ordersParams = [];
    		//假如shipment_status = 1说明结束安排
    		if($params['shipment_status'] ==1){

   
    			$ordersParams['shipment_status'] = 1;
    		}
    		if($params['is_short_barge']==1){
    
    			$ordersParams['is_short_barge'] = 1;
    		}else{//如果不需要短驳
    			$ordersParams['is_short_barge']=0;
    			Db::name('shipment')->where("shipment_type = 3 and shipment_uuid='".$params['shipment_uuid']."'")->update($statusParams);
    			//Db::name('orders')->where("orders_number='".$params['orders_number']."'")->update($ordersParams);
    		} 		
    		$ordersWhere['orders_number'] = $params['orders_number'];

    		Db::name('orders')->where($ordersWhere)->update($ordersParams);
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
    
    	return $result;
    }

    /**
     * 添加短驳
     * 胡
     */
    public function addShortBarge($params){
    	$t = time();
    	if(is_numeric($params['shipment_time'])){
    		$data['shipment_time'] = $params['shipment_time'];
    	}

    	if(isset($params['supplier_uuid'])){
    		$data['supplier_uuid'] = $params['supplier_uuid'];
    	}
    	if(isset($params['supplier_vehicle_id'])){
    		$data['supplier_vehicle_id'] = $params['supplier_vehicle_id'];
    	}
    	 
    	 
    	$data['shipment_type'] = 3;
    	 
    	if(!empty($params['pay_all_money'])){
    		$data['pay_all_money'] = $params['pay_all_money'];
    	}
    	if(is_numeric($params['distribution_type'])){
    		$data['distribution_type'] = $params['distribution_type'];
    	}
    	
    	if(!empty($params['shipment_remark'])){
    		$data['shipment_remark'] = $params['shipment_remark'];
    	}
    	 
    	$data['create_time'] = $t;
    	$data['update_time'] = $t;
    	$data['create_user_id'] = $params['user_id'];
    	$data['update_user_id'] = $params['user_id'];
    	$data['status'] = 1;
    
    
    	$data['shipment_uuid']= Help::getUuid();
    
    	Db::startTrans();
    	try{
    		//$data['finance_number']
    
    		//开始计算合同号
    		//先计算当天的合同数量
    		$dayCountParams = "shipment_type =3 and FROM_UNIXTIME(create_time,'%Y%m') =".date('Ym');
    		$dayCount = Db::name('shipment')->where($dayCountParams)->count();
    		$data['finance_number'] = "ADB".date("ym").str_pad(($dayCount+1),4,0,STR_PAD_LEFT);
    		 
    		$pk_id = Db::name('shipment')->insertGetId($data);
    		//插入运单明细
    		$shipmentShortBarge = $params['shipment_short_barge'];
    		for($i=0;$i<count($shipmentShortBarge);$i++){
    			$shipmentShortBargeParams['shipment_uuid'] = $data['shipment_uuid'];
    			$shipmentShortBargeParams['shipment_short_barge_uuid'] = Help::getUuid();
    			$shipmentShortBargeParams['orders_number'] = $shipmentShortBarge[$i]['orders_number'];
    		//orders表的短驳状态进行变更

    			$ordersParams['is_short_barge'] = 2;
    			
    			$ordersWhere['orders_number'] = $shipmentShortBarge[$i]['orders_number'];
    			
    			Db::name('orders')->where($ordersWhere)->update($ordersParams);
    	
    			$shipmentShortBargeParams['short_barge_money'] = $shipmentShortBarge[$i]['short_barge_money'];
    			$shipmentShortBargeParams['create_time'] = $t;
    			$shipmentShortBargeParams['update_time'] = $t;
    			$shipmentShortBargeParams['create_user_id'] = $params['user_id'];
    			$shipmentShortBargeParams['update_user_id'] = $params['user_id'];
    			$shipmentShortBargeParams['status'] = 1;
    			Db::name('shipment_short_barge')->insertGetId($shipmentShortBargeParams);
    		}
    		
    		$shipmentCostParams['shipment_uuid'] =$data['shipment_uuid'];
			$shipmentCostParams['shipment_cost_uuid'] = Help::getUuid();
			$shipmentCostParams['cost_id'] =9;
			$shipmentCostParams['cost_money'] =$params['pay_all_money'];
			$shipmentCostParams['create_time'] = $t;
			$shipmentCostParams['update_time'] = $t;
			$shipmentCostParams['create_user_id'] = $params['user_id'];
			$shipmentCostParams['update_user_id'] = $params['user_id'];
			$shipmentCostParams['status'] = 1;
			Db::name('shipment_cost')->insertGetId($shipmentCostParams);
			
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
    	 
    	return $result;
    }
}