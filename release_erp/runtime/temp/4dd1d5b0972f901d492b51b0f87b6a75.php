<?php if (!defined('THINK_PATH')) exit(); /*a:5:{s:87:"/var/www/html/test_erp/public/../application/index/view/order/order_receipt_manage.html";i:1665643657;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:64:"/var/www/html/test_erp/application/index/view/public/header.html";i:1658978091;s:67:"/var/www/html/test_erp/application/index/view/public/left_menu.html";i:1665286673;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






    <title>回单管理</title>
    <style>

        .xm-select {
            min-height: 25px !important;
        }

        .xm-select-title {
            min-height: 25px !important;
        }

        .xm-select--suffix .xm-input {
            height: 25px !important;
        }
		.layui-table-fixed-r .layui-table-cell{
		
	overflow:visible;
		}
		.layui-input{
		padding-left:0;
		}
		.layui-table-body table{
		    margin-bottom: 0px!important;
		
		}
		
		
    </style>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
            <div class="layui-logo"></div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left">
        <li class="layui-nav-item">
            <a href="/turnoverbox/index">智能周转箱</a>
        </li>
        <li class="layui-nav-item">
            <a href="/">TMS</a>
        </li>
        <li class="layui-nav-item">
 
            <a href="/warehouse/index">WMS</a>
        </li>
        <li class="layui-nav-item">
            <a href="/wisdompark/index">智慧园区</a>

        </li>
        <li class="layui-nav-item">
            <a href="/Oa/index">OA</a>

        </li>   
         <li class="layui-nav-item">
            <a href="/Bms/index">BMS</a>

        </li>
        <li class="layui-nav-item">
            <a href="/equipment/equipmentManage">设备</a>

        </li>
    </ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item tips-system-message" style="cursor: pointer"><?php echo $language_tag['index_nav_system_message']; ?> <span class="system-message-quantity" style="color: red"> 1</span> </li>
      <li class="layui-nav-item">
        <a href="javascript:;">
          <!-- <img src="http://t.cn/RCzsdCq" class="layui-nav-img"> -->
          <?php echo \think\Session::get('user.nickname'); ?>
        </a>
        <dl class="layui-nav-child head-top">
          <dd><a href="/system/setUserInfo/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_basicDocument']; ?></a></dd>
          <dd><a href="/system/showChangePassword/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_resetPassword']; ?></a></dd>
        </dl>
      </li>
      <li class="layui-nav-item"><a href="/login/loginOut">退出登录</a></li>
    </ul>

    <div id="tips-system-message-js" style='display:none;'>
        <div  style="max-height:350px;width: 450px">
             <div style="color: #000;text-align: center;height: 10px;padding: 10px">你有 <font class="f-tips-system-message"></font> 条未读消息</div>
             <hr>
             <div class="tips-system-message-div" style="height:220px;overflow-y: auto">
           
                 <li style="color: #000; padding: 10px;cursor: pointer" data-href="" data-id="" class="aUrl" onclick="Aurl(this)" ></li>
                 <hr>
             
             </div>
            <div style="color: #000;text-align: center;height: 40px;padding-top: 15px"><a href="/reminderManagement/allInStationLetter"><?php echo $language_tag['index_nav_viewAll_messa']; ?></a></div>
        </div>
    </div>

    <!--  下列保存用户SESSION信息 -->
	
    <input type="hidden" id='now_url'  value="<?php echo $now_url; ?>" />
	<input type="hidden" id='after_url'  value="<?php echo $after_url; ?>" />
    <input type="hidden" id='user_company_id' value="<?php echo \think\Session::get('user.company_id'); ?>" />
	<!--  下列保存其他信息 -->
	<input type="hidden" id='http_referer' value="<?php echo $http_referer; ?>" />
	
	<script type='text/javascript'>
		$('.tips-system-message').on('click',function(){
			var html = $('#tips-system-message-js').html();
		    layer.tips(html, '.tips-system-message', {
		        tips: [3, '#fff'],
		        padding:'20',
		        tipsMore: false,
		        area: ['450px', 'auto'],
		        shade: [0.01, '#fff'],
		        shadeClose:true,
		        time:0
			
		    });
		
		});
		
		$(document).ready(function(){ 
			$(document).mousemove(function(e){ 
				
				if(e.pageX<=10){
					$('.layui-bg-black').show()
				}
				
				if(e.pageX>200){
					$('.layui-bg-black').hide()
				
				}
				if(e.pageY<=10){
					$('.layui-header').show()
				}
				
				if(e.pageY>40){
					$('.layui-header').hide()
				
				}			
			}); 
			
		}); 
	

	</script>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <div title="菜单收缩" class="kit-side-fold"><i class="layui-icon layui-icon-spread-left" aria-hidden="true"></i>
            </div>
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
                  <ul class="layui-nav layui-nav-tree" id="left-nav" lay-filter="test">
          <li class="layui-nav-item"><a href="/"><i class="layui-icon layui-icon-chart-screen"></i>   <em>控制面板</em></a></li>

          <li <?php if($controller_name == 'order'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-form"></i><em>运单管理</em></a>
        <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"showordermanage,showorderadd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderManage?multi_order_status=1">运单管理</a></dd>
			<!--<dd <?php if(in_array(($function_name), explode(',',"showordertrackmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderTrackManage">运单跟踪</a></dd>-->
			 <?php if(\think\Session::get('user.role_id') == 1 || \think\Session::get('user.role_id') == 15): ?>  <dd <?php if(in_array(($function_name), explode(',',"showordertrackmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderTrackManage?sign=0&multi_order_status=2,3,4,5">运单跟踪</a></dd><?php endif; ?> 
            <dd <?php if(in_array(($function_name), explode(',',"showorderreceiptmanage,showorderreceiptadd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderReceiptManage?receipt_status=0">回单管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showorderabnormalmanage,showorderabnormaladd"))): ?> class="layui-this"<?php endif; ?>><a href="/order/showOrderAbnormalManage?handle=1">异常运单</a></dd>
  			
  			<dd <?php if(in_array(($function_name), explode(',',"incomeaccountingmanage,addorderincome"))): ?> class="layui-this"<?php endif; ?>><a href="/order/incomeAccountingManage?verify_status=1">收入核算</a></dd>


        </dl>

        </li>
           <li style='display:none' <?php if($controller_name == 'dispatch' or $controller_name == 'despatch' or $controller_name == 'shortbarge'): ?> class="layui-nav-item layui-nav-itemed" <?php elseif($controller_name == 'transport'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-console"></i><em>调度管理</em></a>
        <dl class="layui-nav-child" >
            <dd <?php if(in_array(($function_name), explode(',',"showpickupordermanage,showpickuporderadd,showdespatchmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showPickupOrderManage">发运安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"shortbargemanage,shortbargelist"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/shortBargeManage?short_barge=1">短驳安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"abnormalmanner,addabnormal"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/abnormalManner">异常管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"accountingmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shortbarge/accountingManage">成本核算</a></dd>

		   <!-- <dd <?php if(in_array(($function_name), explode(',',"showtransportmanage,uploadtransport"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showTransportManage">运单管理</a></dd>
 			<dd <?php if(in_array(($function_name), explode(',',"showdispatchmanage,uploaddispatch"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showDispatchManage">调度管理</a></dd> -->

        </dl>

        </li>
            <li <?php if($controller_name == 'shipment'): ?> class="layui-nav-item layui-nav-itemed" <?php elseif($controller_name == 'transport'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-console"></i><em>发运管理</em></a>
        <dl class="layui-nav-child" >
            <dd <?php if(in_array(($function_name), explode(',',"showlinemanage,addline,showlineovermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/showLineManage">发运安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showshortbargemanage,showshortbargeovermanage"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/showShortBargeManage">短驳安排</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"abnormalmanner,addabnormal,abnormalinfomanner"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/abnormalManner">异常管理</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"getshipmentcostcheck"))): ?> class="layui-this"<?php endif; ?>><a href="/shipment/getShipmentCostCheck">成本审核</a></dd>

		   <!-- <dd <?php if(in_array(($function_name), explode(',',"showtransportmanage,uploadtransport"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showTransportManage">运单管理</a></dd>
 			<dd <?php if(in_array(($function_name), explode(',',"showdispatchmanage,uploaddispatch"))): ?> class="layui-this"<?php endif; ?>><a href="/dispatch/showDispatchManage">调度管理</a></dd> -->

        </dl>

        </li>
        <li <?php if($controller_name == 'source'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-website"></i><em>资源管理</em></a>
          <dl class="layui-nav-child">
    		<dd <?php if(in_array(($function_name), explode(',',"showcustomermanage,showcustomeradd"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerManage">客户</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showprojectmanage,showprojectadd,showacceptgoodsmanage,showacceptgoodsadd,showsendgoodsmanage,showsendgoodsadd,showgoodsmanage,showgoodsadd"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showProjectManage">项目</a></dd>

             <dd <?php if(in_array(($function_name), explode(',',"showsuppliermanage,showsupplieradd,showsupplieredit,showsupplierinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showSupplierManage">承运商</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showvehicletypemanage,showvehicletypemanageadd,showvehicletypeedit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showVehicleTypeManage">车辆类型</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showvehiclemanage,showvehicleadd,showvehicleedit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showVehicleManage">车辆</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showdrivermanage,showdriveradd,showdriveredit"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showDriverManage">司机</a></dd>

              <dd <?php if(in_array(($function_name), explode(',',"showcustomersuppliermanage,showcustomersupplieradd,showcustomersupplieredit,showcustomersupplierinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerSupplierManage">客服用承运商</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showcustomersendgoodsmanage,showcustomersendgoodsadd,showcustomersendgoodsedit,showcustomersendgoodsinfo"))): ?> class="layui-this"<?php endif; ?>><a href="/source/showCustomerSendGoodsManage">客服用发货客户</a></dd>

          </dl>
          <li <?php if($controller_name == 'bill'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-file-b"></i><em>账单管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showcustomerbillmanage,showcustomerbilladd,showcustomerbillmissinvoicemanage,showcustomerbilldoneinvoicemanage,showcustomerbillsendinvoicemanage,showcustomerbillcloseinvoicemanage"))): ?> class="layui-this"<?php endif; ?>><a href="/bill/showCustomerBillManage">客户账单</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showsupplierbillmanage,showsupplierbilladd,showsupplierbillovermanage,supplierbillmissinvoice,supplierbilldoneinvoice,supplierbillfinancegetinvoice,supplierbilladdcostinfo,supplierbillaggrecostinfo,supplierbilldonepay"))): ?> class="layui-this"<?php endif; ?>><a href="/bill/showSupplierBillManage">承运商账单</a></dd>



          </dl>

          <li <?php if($controller_name == 'form'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-file-b"></i><em>报表管理</em></a>
          <dl class="layui-nav-child">
              <dd <?php if(in_array(($function_name), explode(',',"showorderformmanage,showorderformadd"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showOrderFormManage">运单报表</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showshipmentlineoverformmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showShipmentLineOverFormManage">发运报表</a></dd>
              <dd <?php if(in_array(($function_name), explode(',',"showshortbargeformmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/form/showShortBargeFormManage">短驳报表</a></dd>



          </dl>

        </li>  <li <?php if($controller_name == 'device.smartbox'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>设备管理</em></a>
          <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"smartboxmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/baidumap/index">百度地图(demo)</a></dd>

             <dd <?php if(in_array(($function_name), explode(',',"smartboxmanage"))): ?> class="layui-this"<?php endif; ?>><a href="/device.Smartbox/smartboxManage">智能周转箱</a></dd>





          </dl>
        </li>
	<!--
        <li <?php if($controller_name == 'customer'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-friends"></i><em>客户管理</em></a>
        <dl class="layui-nav-child">
            <dd <?php if(in_array(($function_name), explode(',',"showcustomermanage,showcustomeradd"))): ?> class="layui-this"<?php endif; ?>><a href="/customer/showCustomerManage">客户</a></dd>
            <dd <?php if(in_array(($function_name), explode(',',"showprojectmanage,showprojectadd,showacceptgoodsmanage,showacceptgoodsadd,showsendgoodsmanage,showsendgoodsadd,showgoodsmanage,showgoodsadd"))): ?> class="layui-this"<?php endif; ?>><a href="/customer/showProjectManage">项目</a></dd>


        </dl>

        </li>
 -->



		<!-- 系统管理 -->

          <li <?php if($controller_name == 'system'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-set-fill"></i><em>系统管理</em></a>
          <dl class="layui-nav-child">
                 <dd <?php if(in_array(($function_name), explode(',',"taxratemanage,taxrateadd"))): ?> class="layui-this"<?php endif; ?>><a href="/system/taxratemanage">费用管理</a></dd>
				 <dd><a href='/system/showAuthManage'>权限管理</a></dd>

          </dl>
          </li>



      </ul>
        </div>
    </div>

    <div class="layui-body layui-body-bg">

        <!-- 内容主体区域 -->
        <div class="content_body">
            <div class="body-top">
                <div class='layui-form-item'>
                    <span class="layui-breadcrumb" lay-separator="-">
                        <a>首页</a>
                        <a>运单管理</a>
                        <a><cite>回单管理</cite></a>
                    </span>
                </div>
                <div class='layui-block all-search-bg'>
			
				 <ul class="layui-tab-title">
    <li   <?php if(is_numeric(\think\Request::instance()->get('receipt_status'))): if(\think\Request::instance()->get('receipt_status') == 0): ?> class="layui-this"  <?php endif; endif; ?> ><a href='/order/showOrderReceiptManage?receipt_status=0'>未回</a></li>
    <li <?php if(\think\Request::instance()->get('receipt_status') == 1): ?> class="layui-this"  <?php endif; ?>><a href='/order/showOrderReceiptManage?receipt_status=1'>已回</a></li>
    <li <?php if(\think\Request::instance()->get('receipt_status') == 2): ?> class="layui-this"  <?php endif; ?>><a href='/order/showOrderReceiptManage?receipt_status=2'>已送</a></li>
    <li <?php if(\think\Request::instance()->get('receipt_status') == 3): ?> class="layui-this"  <?php endif; ?>><a href='/order/showOrderReceiptManage?receipt_status=3'>其他</a></li>
<li <?php if(\think\Request::instance()->get('receipt_status') == ''): ?> class="layui-this"  <?php endif; ?>><a href='/order/showOrderReceiptManage?receipt_status='>全部</a></li>

  </ul>
				
				
                    <form class="layui-form" method='get' action=''>


                        <div class="layui-input-inline">
                            <label class="layui-form-label">项目名称:</label>
                            <div class="layui-input-inline">
                                <select id='project_id' name='project_id' lay-search>
                                    <option value=''>--全部--</option>
                                    <?php if(is_array($projectResult) || $projectResult instanceof \think\Collection || $projectResult instanceof \think\Paginator): if( count($projectResult)==0 ) : echo "" ;else: foreach($projectResult as $key=>$vo): ?>
                                    <option value='<?php echo $vo['project_id']; ?>' <?php if($vo['project_id'] == \think\Request::instance()->get('project_id')): ?>selected<?php endif; ?>><?php echo $vo['project_name']; ?></option>
                                    <?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>


                        <div class="layui-input-inline">
                            <label class="layui-form-label">运单时间:</label>
                            <div class="layui-input-inline">
                                <div class="layui-input-inline" style="width: 65px;">

                                    <input type="text" id="pickup_time" name="pickup_time" maxlength="300"
                                           autocomplete="off" value="<?php echo \think\Request::instance()->get('pickup_time'); ?>"
                                           placeholder="开始时间" class="layui-input pickup_time">
                                </div>

                                <div style="width: 65px;" class="layui-input-inline">
                                    <input type="text" name="pickup_time_end" type="text"
                                           class="pickup_time layui-input" value="<?php echo \think\Request::instance()->get('pickup_time_end'); ?>"
                                           placeholder="结束时间">
                                </div>
                            </div>
                        </div>


                        <div class="layui-input-inline">
                            <label class="layui-form-label">运单编号:</label>
                            <div class="layui-input-inline">


                                <input type="text" class="layui-input" value="<?php echo \think\Request::instance()->get('orders_number'); ?>"
                                       name="orders_number" id=""/>


                            </div>
                        </div>

          <input type="hidden" name="receipt_status" value="<?php echo \think\Request::instance()->get('receipt_status'); ?>" ></input>
                        <!--<div class="layui-input-inline">
                            <label class="layui-form-label">回单状态:</label>
                            <div class="layui-input-inline">
                                <select id='receipt_status' name='receipt_status'>
                                    <option value=''>--全部--</option>
                                    <?php if(is_array($baseConfig['order']['receipt_status']) || $baseConfig['order']['receipt_status'] instanceof \think\Collection || $baseConfig['order']['receipt_status'] instanceof \think\Paginator): if( count($baseConfig['order']['receipt_status'])==0 ) : echo "" ;else: foreach($baseConfig['order']['receipt_status'] as $key1=>$vo): ?>

                                    <option value='<?php echo $key1; ?>' <?php if(\think\Request::instance()->get('receipt_status') !=null): if($key1==\think\Request::instance()->get('receipt_status')): ?> selected="selected" <?php endif; endif; ?>><?php echo $vo; ?></option>
                                    <?php endforeach; endif; else: echo "" ;endif; ?>


                                </select>
                            </div>
                        </div>-->


                        <div class="layui-input-inline">
                            <label class="layui-form-label">客户编号:</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" value="<?php echo \think\Request::instance()->get('customer_order_number'); ?>"
                                       name="customer_order_number" id="" value=""/>
                            </div>
                        </div>


                        <div class="layui-input-inline">
                            <label class="layui-form-label">专线编号:</label>
                            <div class="layui-input-inline">


                                <input type="text" class="layui-input" value="<?php echo \think\Request::instance()->get('shipment_number'); ?>"
                                       name="shipment_number" id="" value=""/>


                            </div>
                        </div>

                        <div class="layui-input-inline">
                            <label class="layui-form-label">分公司:</label>
                            <div class="layui-input-inline" style="width: 87px;">
                                <select id='company_id' name='choose_company_id'>
                                    <option value=''>--全部--</option>
                                    <?php if(is_array($comapnyResult) || $comapnyResult instanceof \think\Collection || $comapnyResult instanceof \think\Paginator): if( count($comapnyResult)==0 ) : echo "" ;else: foreach($comapnyResult as $key=>$v): ?>
                                    <option value="<?php echo $v['company_id']; ?>"

                                            {if condition="$Think.get.choose_company_id neq ''" }
                                            <?php if(\think\Request::instance()->get('choose_company_id') == $v['company_id']): ?>selected {
                                    /if}
                                    <?php else: if(\think\Session::get('user.company_id') == $v['company_id']): ?>selected <?php endif; endif; ?>
                                    >

                                    <?php echo $v['company_name']; ?></option>
                                    <?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>


                        <div class="layui-input-inline">
                            <label class="layui-form-label">承运商:</label>
                            <div class="layui-input-inline">
                                <select id='supplier_uuid' name='supplier_uuid' lay-search>
                                    <option value=''>--全部--</option>
                                    <?php if(is_array($supplierResult) || $supplierResult instanceof \think\Collection || $supplierResult instanceof \think\Paginator): if( count($supplierResult)==0 ) : echo "" ;else: foreach($supplierResult as $key=>$vo): ?>
                                    <option value='<?php echo $vo['supplier_uuid']; ?>' <?php if($vo['supplier_uuid'] == \think\Request::instance()->get('supplier_uuid')): ?>selected<?php endif; ?>><?php echo $vo['supplier_name']; ?></option>
                                    <?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>


                        <div class="layui-input-inline">
                            <label class="layui-form-label">发货方:</label>
                            <div class="layui-input-inline">

                                <input type="text" class="layui-input" value="<?php echo \think\Request::instance()->get('send_goods_company'); ?>"
                                       name="send_goods_company" id="" value=""/>

                            </div>
                        </div>


                        <div class="layui-input-inline">
                            <label class="layui-form-label">收货方:</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" value="<?php echo \think\Request::instance()->get('accept_goods_company'); ?>"
                                       name="accept_goods_company" id="" value=""/>
                            </div>
                        </div>

                        <button class="layui-btn nav-search search_button">搜索</button>
                        <a type="reset" href="/order/showOrderReceiptManage"
                           class="layui-btn layui-btn-primary search_button">重置</a>


                        <!--<div class='layui-input-inline'>-->
                        <!--<input type="text" id="" name=""  placeholder="电话、联系人、操作人" class="layui-input">-->
                        <!--</div>-->

                    </form>


                </div>
                <div class="content-bg">


                    <div class="table-nont user-manage ">
                        <table id="language-table" lay-filter="language-table">

                        </table>
                        <script type="text/html" id="toolbarDemo" lay-filter="toolbar1">
                            <div class="layui-inline" lay-event="add"><i class="layui-icon layui-icon-add-1"></i></div>

                        </script>
                    </div>

                </div>

            </div>


        </div>


    </div>


    <script type="text/html" id="tool_bar">

        <span><a target="blank" href='ShowOrderReceiptManage?download=1&<?php echo http_build_query($_GET); ?>'><button
                onclick="" class="layui-btn nav-add layui-btn-sm">下载</button></a></span>

        <span><a href='javascript:;'><button onclick="sign_update()"
                                             class="layui-btn nav-add layui-btn-sm">回单上传</button></a></span>
        <a class="layui-btn layui-btn-sm layui-btn-normal" id="clear"><i
                class="layui-icon layui-icon-delete"></i>清除缓存</a>
    <a class="layui-btn layui-btn-sm layui-btn-normal" lay-event="save" >保存设置</a>
    </script>

    <?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
    <script src='/static/javascript/system/base.js'></script>
    <script src='/static/javascript/system/source.js'></script>
    <script src='/static/javascript/public/formSelects-v4.js'></script>
    <script>

        layui.config({}).extend({
            soulTable: '/static/layui-soul-table/ext/soulTable',
            tableChild: '/static/layui-soul-table/ext/tableChild',
            tableMerge: '/static/layui-soul-table/ext/tableMerge',
            tableFilter: '/static/layui-soul-table/ext/tableFilter',
            excel: '/static/layui-soul-table/ext/excel'
        });

        layui.use('element', function () {
            var element = layui.element;
        });
        var isShow = true;  //定义⼀个标志位
        $('.kit-side-fold').click(function () {
            //选择出所有的span，并判断是不是hidden
            $('.layui-nav-item span').each(function () {
                if ($(this).is(':hidden')) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
            //判断isshow的状态
            if (isShow) {
                $('.layui-side.layui-bg-black').width(50); //设置宽度
                $('.kit-side-fold i').css('margin-right', '70%');  //修改图标的位置
                //将footer和body的宽度修改
                $('.layui-body').css('left', 60 + 'px');
                $('.layui-footer').css('left', 60 + 'px');
                //将⼆级导航栏隐藏
                $('dd span').each(function () {
                    $(this).hide();
                });
                //修改标志位
                isShow = false;
            } else {
                $('.layui-side.layui-bg-black').width(200);
                $('.kit-side-fold i').css('margin-right', '10%');
                $('.layui-body').css('left', 200 + 'px');
                $('.layui-footer').css('left', 200 + 'px');
                $('dd span').each(function () {
                    $(this).show();
                });
                isShow = true;
            }
        });

        var g_searchVal = "";

        layui.formSelects.config('choose_send_goods_name', {
            searchUrl: '/order/getSendGoodsNameAjax',
            beforeSearch: function (id, url, val) {
                //console.log(this)

                if (!val) {
                    console.log('内容为空, 不进行远程搜索')
                    return false;
                }
                return true;
            },
            success: function (e) {
                e.searchVal = "s";
                console.log(layui.formSelects.value(e, 'name'))
                console.log(this)
            },
            beforeSearch: function (id, url, searchVal) {         //搜索前调用此方法, return true将触发搜索, 否则不触发
                g_searchVal = searchVal;
                return true;
            }
        });


        layui.formSelects.config('choose_accept_goods_name', {
            searchUrl: '/order/getAcceptGoodsNameAjax',
            clearInput: false,
            beforeSearch: function (id, url, val) {
                //console.log(this)


                return 1;
            },
            success: function (e) {

                //	console.log(layui.formSelects.value(e, 'name'))
                console.log(this)
            }
        }, false);


        layui.use(['form', 'laydate', 'table', 'soulTable', 'element'], function () {
            var table = layui.table;
            // var laytable =layui.laytable;
            var layer = layui.layer;
            var form = layui.form;
            var soulTable = layui.soulTable;

            var languagetable = newtable = table.render({
                elem: '#language-table'
                , height: tableHeight
                , url: '/order/showOrderReceiptManage?<?php echo http_build_query($_GET); ?>'///shipment/showLineAjax?orders_number=<?php echo \think\Request::instance()->get('orders_number'); ?>&start_pickup_time=<?php echo \think\Request::instance()->get('start_pickup_time'); ?>&end_pickup_time=<?php echo \think\Request::instance()->get('end_pickup_time'); ?>' //数据接口

                , response: {

                    statusCode: 200 //规定成功的状态码，默认：0

                }
                , page: true //开启分页
                , limits: [100, 200, 300, 400, 500]
                , limit: 100
                , toolbar: '#tool_bar' //开启工具栏，此处显示默认图标，可以自定义模板，详见文档
                , totalRow: true //开启合计行
                , cols: [[ //表头
                    {type: 'checkbox', fixed: 'left',field:'checkbox'}
					
					, {
                        field: 'receipt_status', title: '回单状态',sort: true, width: 80,  templet: function (e) {

                            var opthionHtml = "";
                            var selectHtml = "";
                            var selected = "selected";
                            baseConfig['order']['receipt_status'].map(function (d, i) {
                                if (i == e.receipt_status)
                                    selected = "selected";
                                else
                                    selected = "";

                                opthionHtml += "<option value=\"" + i + "\" " + selected + ">" + d + "</option>";
                            })

                            selectHtml = "<select lay-filter=\"receiptStatus\" class=\"changeStatus \" orders-number=\"" + e.orders_number + "\" data-id=\"" + e.orders_id + "\" >" + opthionHtml + "</select>"
                            return selectHtml;

                        }
                    },
					
                    {field: 'orders_id', title: '序号', width: 80, type: 'numbers'}
                   
                    , {
                        field: 'pickup_time', title: '运单日期', width: 110, sort: true, templet: function (e) {
                            return php_date('Y-m-d', parseInt(e.pickup_time));
                        }
                    }
                    , {field: 'customer_order_number', title: '客户编号', width: 120, sort: true}
                    , {
                        field: 'orders_number', title: '运单编号', width: 160, sort: true, templet: function (e) {

                            if (e.follow_level == 1)
                                return "<span style=\"color:#b5b900\">" + e.orders_number + "</span>";
                            return e.orders_number;
                        }
                    }
                    , {field: 'send_goods_company', title: '发货方', width: 90, sort: true}
                    , {field: 'send_location_name', title: '发站', width: 90, sort: true}
                    , {field: 'accept_location_name', title: '到站', width: 90, sort: true}
                    , {
                        field: 'shipment_time', title: '发运日期', width: 110, sort: true, templet: function (e) {
                            if (e.shipment_info.length > 0)
                                return php_date('Y-m-d', e.shipment_info[0].shipment_time);
                            else
                                return "";
                        }
                    }, {field: 'shipment_number', title: '专线编号', width: 120, sort: true}
                    , {field: 'supplier_name', title: '承运商', width: 120, sort: true}
                    , {field: 'receipt_count', title: '回单份数', width: 120, sort: true}
                    , {field: 'accept_goods_company', title: '收货方', width: 90, sort: true}
, {field: 'accept_address', title: '收货方地址', width: 90}
                    , {
                        field: 'goods_name', title: '货物名称', width: 190, sort: true, templet: function (e) {
                            var info = "";
                            if (!e.orders_goods_info)
                                return ""
                            else
                                e.orders_goods_info.map(function (i) {
                                    info = info + i.goods_name + "/";
                                })
                            return info.substring(0, info.length - 1)
                        }
                    }
                    , {
                        field: 'estimated_pack_unit', title: '包装', width: 90, sort: true, templet: function (e) {
                            var info = "";
                            if (!e.orders_goods_info)
                                return ""
                            else
                                e.orders_goods_info.map(function (i) {
                                    info = info + baseConfig['order']['goods_pack'][i.estimated_pack_unit] + "/";
                                })
                            return info.substring(0, info.length - 1)
                        }
                    }
                    , {
                        field: 'accept_name', title: '件数', width: 90, sort: true, templet: function (e) {
                            var info = 0;
                            if (!e.orders_goods_info)
                                return ""
                            else
                                e.orders_goods_info.map(function (i) {
                                    info = info + i.estimated_count;
                                })
                            return info
                        }
                    }
					
					, {
                    field: 'xdjs', title: '下单件数', width: 60, sort: true, totalRow: true

                }, {
                    field: 'xdsl', title: '下单数量', width: 60, sort: true, totalRow: true

                }
                , {
                    field: 'xdtj', title: '下单体积', width: 60, sort: true, totalRow: true
                }
                , {
                    field: 'xdzl', title: '下单重量', width: 60, sort: true, totalRow: true
                }, {
                    field: 'shipment_count', title: '发运件数', width: 60, sort: true, totalRow: true

                }, {
                    field: 'shipment_pack_count', title: '发运数量', width: 60, sort: true, totalRow: true

                }
                , {
                    field: 'shipment_volume', title: '发运体积', width: 60, sort: true, totalRow: true
                }
                , {
                    field: 'shipment_weight', title: '发运重量', width: 60, sort: true, totalRow: true
                }
					
					
                    , {
                        field: 'is_upload', title: '电子回单', width: 90, sort: true, templet: function (e) {
                            if (e.is_upload & 1)
                                return '<a href="javascript:void(0);" onclick="openlayer(\'/order/orderUploadView?orders_id=' + e.orders_id + '\',\'运单回执\')" >查看</a>';
                            else
                                return "";
                        }
                    },
                    {
                        field: "receipt_accept_time", title: "收回日期", width: 130, templet: function (e) {

                            return "<span class=\"receipt_accept_time\">" + php_date('Y-m-d H:i:s', e.receipt_accept_time) + "</span>";

                        }
                    },
                    {
                        field: "receipt_accept_user_name", title: "操作员", templet: function (e) {
                            var user_name = (e.receipt_accept_user_name != null) ? e.receipt_accept_user_name : "";
                            return "<span class=\"receipt_accept_user_name\">" + user_name + "</span>";
                        }
                    },
                    {
                        field: "receipt_send_time", title: "送回日期", width: 130, templet: function (e) {
                            return "<span class=\"receipt_send_time\">" + php_date('Y-m-d H:i:s', e.receipt_send_time) + "</span>";

                        }
                    },
                    {
                        field: "receipt_send_user_name", title: "操作员", templet: function (e) {

                            var user_name = e.receipt_send_user_name ? e.receipt_send_user_name : ""

                            return "<span class=\"receipt_send_user_name\">" + user_name + "</span>";


                        }
                    }
                    , {
                        field: 'abnormal_status',
                        title: '是否异常',
                        width: 70,
                        align: 'center',
                        sort: true,
                        templet: function (e) {

                            if (parseInt(e.abnormal_count) > 0)
                                return "<a href=\"javascript:void(0);\"     onclick=\"javascript:openlayerMax('/order/showOrderAbnormalManage?hide_menu=1&orders_number=" + e.orders_number + "','回单异常')\">" + e.abnormal_count + "</a>";
                            return "";
                        }
                    }


                    , {
                        field: 'update_time', title: '更新时间', width: 130, sort: true, templet: function (e) {

                            return "<span class=\"update_time\">" + php_date('Y-m-d H:i:s', e.update_time) + "</span>";
                        }
                    }, {
                        field: 'update_user_name', title: '更新人', width: 90, sort: true, templet: function (e) {

                            return "<span class=\"update_user_name\">" + e.update_user_name + "</span>";
                        }
                    }


                    , {
                        field: 'orders_receipt_remark', title: '回单备注', width: 90, templet: function (e) {
                            var content = "编辑";//e.customfollow.orders_customer_follow_content?e.customfollow.orders_customer_follow_content:"";
                          if (e.orders_receipt_remark != null)
                             content = e.orders_receipt_remark.orders_customer_follow_content;
/*
                            if (e.orders_receipt_remark != null)
                              {  content = e.orders_receipt_remark.orders_customer_follow_content;
                            return "<input class=\"layui-input\" value='" + content + "'  onchange=\"changeFollow('" + e.orders_number + "',this)\" ></input>";}
							else*/
							  return "<a href=\"javascript:void(0);\" onclick=\"openlayer('/order/orderCustomerFollow?orders_number=" + e.orders_number + "')\" >"+content+"</a>";
                        }
                    }
 

                ]],
                filter: {
                    items: ['column', 'data', 'condition', 'editCondition', 'excel', 'clearCache'],
                    cache: true
                },
                done: function (res, curr, count) {
                    //如果是异步请求数据方式，res即为你接口返回的信息。
                    //如果是直接赋值的方式，res即为：{data: [], count: 99} data为当前页数据、count为数据总长度
                    // console.log(res);
                    soulTable.render(this)

                    $(".changeStatus").parent().css("overflow", "visible");

                    //得到当前页码
                    console.log(curr);
                    var that = this.elem.next();

                    //得到数据总量
                    console.log(count);
                    /*
                  res.data.forEach(function(item,index){
                      if(item.follow_level==1)
                      {
                          var tr = that.find(".layui-table-box tbody tr[data-index='" + index + "']");
                                tr.css("background-color", "#faff00");
                               // tr.css("color", "white");

                      }


                  })
                  */

                }
            });

            $('#clear').on('click', function () {
                soulTable.clearCache(languagetable.config.id)
                layer.msg('已还原！', {icon: 1, time: 1000})
            })

            form.on('select(receiptStatus)', function (obj) {

                var orders_id = $(obj.elem).attr("data-id");
                var orders_number = $(obj.elem).attr("orders-number");
                var value = obj.value;

                var send_data = {};
                var now_timestamp = parseInt((new Date).getTime() / 1000)
                var user_id = "<?php echo \think\Session::get('user.user_id'); ?>";
                var user_name = "<?php echo \think\Session::get('user.nickname'); ?>";
                var now_date = php_date('Y-m-d H:i:s', now_timestamp);
                send_data.receipt_status = value;
                send_data.update_time = now_timestamp;
                send_data.update_user_id = user_id;
                send_data.orders_number = orders_number;


                $(obj.elem).parents("tr").find(".update_time").html(now_date);
                $(obj.elem).parents("tr").find(".update_user_name").html(user_name);

                switch (value) {
				    case "0":$(obj.elem).parents("tr").find(".receipt_count").html(0);
                                $(obj.elem).parents("tr").find(".receipt_accept_time").html("");
                                $(obj.elem).parents("tr").find(".receipt_accept_user_name").html("");
					            $(obj.elem).parents("tr").find(".receipt_send_time").html("");
                                $(obj.elem).parents("tr").find(".receipt_send_user_name").html("");
					    send_data.receipt_accept_time = null;
                        send_data.receipt_accept_user_id = null;
						send_data.receipt_send_time = null;
                        send_data.receipt_send_user_id = null;
						$.ajax({
                            url: "/order/updateOrdersByOrdersNumber",
                            data: send_data,
                            success: function (e) {
                                layer.msg("修改成功");
                            }

                        });
					break;
                    case "1":
                        send_data.receipt_accept_time = now_timestamp;
                        send_data.receipt_accept_user_id = user_id;
                        layer.prompt({
                            formType: 0,
                            value: '1',
                            title: '请输入回单份数',
                            area: ['80px', '30px'] //自定义文本域宽高
                        }, function (value, index, elem) {
                            if (!(/^\d+$/.test(value))) {
                                layer.msg("请输入数字")
                            } else {
                                send_data.receipt_count = value;
                                $(obj.elem).parents("tr").find(".receipt_count").html(value);
                                $(obj.elem).parents("tr").find(".receipt_accept_time").html(now_date);
                                $(obj.elem).parents("tr").find(".receipt_accept_user_name").html(user_name);

                                $.ajax({
                                    url: "/order/updateOrdersByOrdersNumber",
                                    data: send_data,
                                    success: function (e) {
                                        layer.msg("修改成功");
                                    }

                                })

                                layer.close(index);
                            }

                        });

                        break;
                    case "2":
                        send_data.receipt_send_time = now_timestamp;
                        send_data.receipt_send_user_id = user_id;
                        $(obj.elem).parents("tr").find(".receipt_send_time").html(now_date);
                        $(obj.elem).parents("tr").find(".receipt_send_user_name").html(user_name);

                    default:
                        $.ajax({
                            url: "/order/updateOrdersByOrdersNumber",
                            data: send_data,
                            success: function (e) {
                                layer.msg("修改成功");
                            }

                        })


                        break;
                }


            })


            table.on('checkbox(language-table)', function (obj) {
			
		
			
                if (obj.type == "all") { //全选
                    if (obj.checked == true) {
                        $(".layui-table-body table.layui-table tbody tr").css('background', '#99CCFF');
                    } else {
                        $(".layui-table-body table.layui-table tbody tr").map(function (i, d) {
                            $(d).css('background', '#fff');
                        })


                        $("#nums").html(0);
                    }
                } else { //单选
                    if (obj.checked == true) {
                        obj.tr.css('background', '#99CCFF');

                    } else {
                        obj.tr.css('background', '#fff');

                    }
                }

            });


           
		    table.on('sort(language-table)',function(obj){
		
			layui.form.render()
			
			
			})

            //监听头工具栏事件
            table.on('toolbar(language-table)', function (obj) {
			var myTable=obj.config;
 var storeKey = location.pathname + location.hash + myTable.id;
 var colsStr=JSON.stringify(obj.config.cols);
                var checkStatus = table.checkStatus(obj.config.id)
                    , data = checkStatus.data; //获取选中的数据
                switch (obj.event) {
				  case 'save':
				layer.msg("保存成功");
			    localStorage.setItem( storeKey, colsStr);
				break;
                    case 'addImportant':
                        if (data.length > 0)
                            data.forEach(function (e) {
                                $.ajax({
                                    url: "/index/changeStatus",
                                    data: {
                                        table_id: e.orders_id,
                                        table_id_name: 'orders_id',
                                        table_name: "orders",
                                        status: 1, field: "follow_level"
                                    },
                                    success: function (e) {
                                        alert("标记成功");
                                        window.location.reload();
                                    }

                                })

                            })
                        else
                            layer.msg("请选择一行");

                        break;

                }
                ;
            });
        })
        //第一个实例

    </script>


    <script>
        function openlayer(url, title) {
            layer.open({
                type: 2,
                title: title,
                content: url,
                area: ["500px", "600px"]
            })
        }


        var layer = layui.layer;

        var form = layui.form;

        form.on('select(replacement_prive_type)', function (data) {
            var table_id = $(data.elem).parents("tr").children(".order_id").html();

            var table_id_name = "orders_id";
            var table_name = "orders";
            status = data.value;

            $.ajax({
                type: "POST",
                url: '/index/changeStatus',
                data: {
                    table_id: table_id,
                    table_id_name: table_id_name,
                    table_name: table_name,
                    status: status,
                    field: "replacement_prive_type"
                },
                success: function (data) {
                    //console.log(data);
                    if (data.code != 200) {
                        layer.msg(data.msg);
                        return false;
                    } else if (data.code == 200) {
                        layer.msg('操作成功', {
                            time: 1, end: function (layero, index) {

                            }
                        });
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    layer.msg('验证失败')
                }
            });


            console.log(data)
        })


        form.on('switch(switchTest)', function (data) {
            var table_id = $(data.elem).parents("tr").children(".order_id").html();

            var table_id_name = "order_id";
            var table_name = "order";
            if (this.checked == true) {
                var status = 1;
            } else {
                var status = 0;
            }

            $.ajax({
                type: "POST",
                url: '/index/changeStatus',
                data: {
                    table_id: table_id,
                    table_id_name: table_id_name,
                    table_name: table_name,
                    status: status,
                },
                success: function (data) {
                    //console.log(data);
                    if (data.code != 200) {
                        layer.msg(data.msg);
                        return false;
                    } else if (data.code == 200) {
                        layer.msg('操作成功', {
                            time: 1, end: function (layero, index) {

                            }
                        });
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    layer.msg('验证失败')
                }
            });
        });


        function sign_update() {
            var data_array = layui.table.checkStatus("language-table")
            data_array.data.map(function (e, i) {

                openlayer("/order/addReceipt?orders_id=" + e.orders_id, '上传回单')

            })
        }

        function changeFollow(orders_number, Obj) {
            var value = $(Obj).val();

            $.ajax({
                url: "/order/addOrderCustomerFollowAjax",
                data: {orders_number: orders_number, orders_customer_follow_content: value},
                success: function (e) {
                    layer.msg("修改成功");
                },
                error: function (e) {
                    layer.msg("修改失败");
                }
            })


        }


    </script>
</body>
</html>


