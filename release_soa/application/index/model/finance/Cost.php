<?php


namespace app\index\model\finance;


use think\Model;

class Cost extends Model
{
protected $table="cost";

public function getAll(){
    return $this->where(['cost_type'=>1,'status'=>1])->select();
}

public function getCostIdToCostName($params){
    $data = "1=1 ";

    if(!empty($params['cost_name'])){
        $data.=' and cost_name ="'.$params['cost_name'].'" and cost_type = 2 and status = 1';

    }

    $result = $this->table("cost")->where($data)->select();

    return $result;
}
}