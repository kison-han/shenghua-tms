<?php

namespace app\index\model\source;
use think\Model;
use app\common\help\Help;
use app\index\service\PublicService;
use think\config;
use think\Db;
class PricingConfigure extends Model{

    protected $table = 'pricing_configure';

    /**
     * 添加承运商计价配置
     * 韩
     */
    public function addPricingConfigureOne($params){

        $t = time();

        $data['supplier_id'] = $params['supplier_id'];
        $data['province_start_id'] = $params['province_start_id'];
        $data['city_start_id'] = $params['city_start_id'];
        $data['departure_id'] = $params['departure_id'];
        $data['province_end_id'] = $params['province_end_id'];
        $data['city_end_id'] = $params['city_end_id'];
        $data['arrival_id'] = $params['arrival_id'];
        $data['billing_unit'] = $params['billing_unit'];
        $data['project_id'] = 0;
        $data['goods_id'] = $params['goods_id'];
        $data['interval_start'] = number_format($params['interval_start'], 1, '.', '');
        $data['interval_end'] = number_format($params['interval_end'], 1, '.', '');
        $data['interval_value'] = number_format($params['interval_end'], 1, '.', '');
        $data['unit_price'] = number_format($params['unit_price'], 3, '.', '');
        $data['starting_price'] = number_format($params['starting_price'], 3, '.', '');
        $data['picking_rules'] = $params['picking_rules'];
        $data['picking_price'] = number_format($params['picking_price'], 3, '.', '');
        $data['delivery_rules'] = $params['delivery_rules'];
        $data['delivery_price'] = number_format($params['delivery_price'], 3, '.', '');
        $data['unloading_rules'] = $params['unloading_rules'];
        $data['unloading_price'] = number_format($params['unloading_price'], 3, '.', '');
        $data['goods_type'] = $params['goods_type'];
        $data['orders_type'] = $params['orders_type'];
        $data['create_user_id'] = $params['user_id'];
        $data['create_time'] = $t;
        $data['update_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        $data['status'] = $params['status'];
        $picking = $this->table("cost")->where("cost_name = '提货费' and cost_type=2")->find();
        $data['picking_id'] = $picking['cost_id'];
        $picking = $this->table("cost")->where("cost_name = '送货费' and cost_type=2")->find();
        $data['delivery_id'] = $picking['cost_id'];
        $unloading = $this->table("cost")->where("cost_name = '装卸费' and cost_type=2")->find();
        $data['unloading_id'] = $unloading['cost_id'];

        Db::startTrans();
        try{
            $this->table("pricing_configure")->insertGetId($data);
            $result = 1;
            // 提交事务
            Db::commit();
        }catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return $result;
    }

    /**
     * 添加承运商计价配置
     * 韩
     */
    public function addPricingConfigure($params){

        Db::startTrans();
        try{
            $asd = $this->table("pricing_configure")->insertAll($params);

            $result = $asd;
            // 提交事务
            Db::commit();
        }catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return $result;

    }

    /**
     * 修改承运商商计价配置
     * 韩
     */
    public function editPricingConfigure($params){
        $t = time();

        $data['project_id'] = $params['project_id'];
        $data['supplier_id'] = $params['supplier_id'];
        $data['province_start_id'] = $params['province_start_id'];
        $data['city_start_id'] = $params['city_start_id'];
        $data['departure_id'] = $params['departure_id'];
        $data['province_end_id'] = $params['province_end_id'];
        $data['city_end_id'] = $params['city_end_id'];
        $data['arrival_id'] = $params['arrival_id'];
        $data['billing_unit'] = $params['billing_unit'];
        $data['goods_id'] = $params['goods_id'];
        $data['interval_start'] = number_format($params['interval_start'], 1, '.', '');
        $data['interval_end'] = number_format($params['interval_end'], 1, '.', '');
        $data['interval_value'] = number_format($params['interval_end'], 1, '.', '');
        $data['unit_price'] = number_format($params['unit_price'], 3, '.', '');
        $data['starting_price'] = number_format($params['starting_price'], 3, '.', '');
        $data['picking_rules'] = $params['picking_rules'];
        $data['picking_price'] = number_format($params['picking_price'], 3, '.', '');
        $data['delivery_rules'] = $params['delivery_rules'];
        $data['delivery_price'] = number_format($params['delivery_price'], 3, '.', '');
        $data['unloading_rules'] = $params['unloading_rules'];
        $data['unloading_price'] = number_format($params['unloading_price'], 3, '.', '');
        $data['goods_type'] = $params['goods_type'];
        $data['orders_type'] = $params['orders_type'];
        $data['create_user_id'] = $params['user_id'];
        $data['create_time'] = $t;
        $data['update_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        $data['status'] = $params['status'];

        Db::startTrans();
        try {
            Db::name('pricing_configure')->where('pricing_configure_id = "'.$params['pricing_configure_id'].'"')->update($data);

            $result = 1;
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return $result;
    }

    /**
     * 添加项目商计价配置
     * 韩
     */
    public function addPricingConfigureProject($params){

        $t = time();

        $data['project_id'] = $params['project_id'];
        $data['supplier_id'] = $params['supplier_id'];
        $data['departure_id'] = $params['departure_id'];
        $data['arrival_id'] = $params['arrival_id'];
        $data['billing_unit'] = $params['billing_unit'];
        $data['goods_id'] = $params['goods_id'];
        $data['interval_start'] = number_format($params['interval_start'], 1, '.', '');
        $data['interval_end'] = number_format($params['interval_end'], 1, '.', '');
        $data['interval_value'] = number_format($params['interval_end'], 1, '.', '');
        $data['unit_price'] = number_format($params['unit_price'], 3, '.', '');
        $data['starting_price'] = number_format($params['starting_price'], 3, '.', '');
        $data['picking_rules'] = $params['picking_rules'];
        $data['picking_price'] = number_format($params['picking_price'], 3, '.', '');
        $data['delivery_rules'] = $params['delivery_rules'];
        $data['delivery_price'] = number_format($params['delivery_price'], 3, '.', '');
        $data['unloading_rules'] = $params['unloading_rules'];
        $data['unloading_price'] = number_format($params['unloading_price'], 3, '.', '');
        $data['goods_type'] = $params['goods_type'];
        $data['orders_type'] = $params['orders_type'];
        $data['create_user_id'] = $params['user_id'];
        $data['create_time'] = $t;
        $data['update_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        $data['status'] = $params['status'];

        Db::startTrans();
        try{
            $this->table("pricing_configure")->insertGetId($data);
            $result = 1;
            // 提交事务
            Db::commit();
        }catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return $result;

    }

    /**
     * 修改项目商计价配置
     * 韩
     */
    public function editPricingConfigureProject($params){

        $t = time();

        $data['project_id'] = $params['project_id'];
        $data['supplier_id'] = $params['supplier_id'];
        $data['departure_id'] = $params['departure_id'];
        $data['arrival_id'] = $params['arrival_id'];
        $data['billing_unit'] = $params['billing_unit'];
        $data['goods_id'] = $params['goods_id'];
        $data['interval_start'] = number_format($params['interval_start'], 1, '.', '');
        $data['interval_end'] = number_format($params['interval_end'], 1, '.', '');
        $data['interval_value'] = number_format($params['interval_end'], 1, '.', '');
        $data['unit_price'] = number_format($params['unit_price'], 3, '.', '');
        $data['starting_price'] = number_format($params['starting_price'], 3, '.', '');
        $data['picking_rules'] = $params['picking_rules'];
        $data['picking_price'] = number_format($params['picking_price'], 3, '.', '');
        $data['delivery_rules'] = $params['delivery_rules'];
        $data['delivery_price'] = number_format($params['delivery_price'], 3, '.', '');
        $data['unloading_rules'] = $params['unloading_rules'];
        $data['unloading_price'] = number_format($params['unloading_price'], 3, '.', '');
        $data['goods_type'] = $params['goods_type'];
        $data['orders_type'] = $params['orders_type'];
        $data['create_user_id'] = $params['user_id'];
        $data['create_time'] = $t;
        $data['update_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        $data['status'] = $params['status'];

        Db::startTrans();
        try {
            Db::name('pricing_configure')->where('pricing_configure_id = "'.$params['pricing_configure_id'].'"')->update($data);

            $result = 1;
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return $result;
    }

    /**
     * 批量获取可用的计价配置区间（起）值
     * 韩
     */
    public function checkBatchInterval($params){

        if(count($params)>0){
            $asd_s = 0;
            $asd_e = 0;
            foreach($params as $key=>$val){

                $data['supplier_id'] = $val['supplier_id'];
                $data['billing_unit'] = $val['billing_unit'];
                $data['departure_id'] = $val['departure_id'];
                $data['arrival_id'] = $val['arrival_id'];
                $data['status'] = 1;

                $max = $this->table("pricing_configure")->alias('pricing_rules')->where($data)->field('interval_value')->max("interval_value");
                $min = $this->table("pricing_configure")->alias('pricing_rules')->where($data)->field('interval_value')->min("interval_start");

                $result_data = $this->table("pricing_configure")->alias('pricing_rules')->where($data)->find();

                //组装验证区间值
                if($key>0){
                    $nums = $asd_e;
                    $result[$key]['e'] = $nums;
                    $asd_e = $val['interval_end'];

                    $nums2 = $min;
                    $result[$key]['s'] = $nums2;
                    $asd_s = $val['interval_end'] + $nums2;

                    $value= number_format($val['interval_start'], 1, '.', '');
                    $result[$key]['value'] = $value;

                }else{
                    $nums = $max;
                    $result[$key]['e'] = $nums;
                    $asd_e = $val['interval_end'];

                    $nums2 = $min;
                    $result[$key]['s'] = $nums2;
                    $asd_s = $val['interval_end'] + $nums2;
                    $value= number_format($val['interval_start'], 1, '.', '');
                    $result[$key]['value'] = $value;

                }
            }

            //循环验证判断
            foreach($result as $key=>$val){
                $value = number_format($val['value'], 1, '.', '');
                if($value>$val['e'] || $value<$val['s']){
                    $result2[$key] = 1;
                }else{
                    $result2[$key] = 2;
                }
            }

            if(in_array(2,$result2,true)){
                //数据不匹配;
                $check = 2;
            }else{
                //数据匹配
                $check = 1;
            }
            return $check;
        }
    }

    /**
     * 获取承运商计价配置
     * 韩
     */
    public function getPricingConfigure($params,$is_count=false,$is_page=false,$page=null,$page_size=20){

    	$data = "1=1 ";

        if($params['search']=="search"){
            $data.= " and pricing_rules.project_id = ".$params['project_id']." and pricing_rules.supplier_id=".$params['supplier_id'];
        }

        if(isset($params['pricing_configure_id'])){ //计价配置ID
            $data.= " and pricing_rules.pricing_configure_id = ".$params['pricing_configure_id'];
        }

        if(isset($params['project_id'])){ //项目ID
            $data.= " and pricing_rules.project_id = ".$params['project_id'];
        }

        if(isset($params['supplier_id'])){ //承运商ID
            $data.= " and pricing_rules.supplier_id = ".$params['supplier_id']." and project_id = 0";
        }

        if(isset($params['departure_id'])){ //发站市区ID
            $data.= " and pricing_rules.departure_id = ".$params['departure_id'];
        }

        if(isset($params['arrival_id'])){ //到站市区ID
            $data.= " and pricing_rules.arrival_id = ".$params['arrival_id'];
        }

        if(isset($params['interval'])){ //区间值
            $data.= " and ".$params['interval']." between pricing_rules.interval_start and pricing_rules.interval_end";
        }

        if(isset($params['billing_unit'])){ //计费单位
            $data.= " and pricing_rules.billing_unit = ".$params['billing_unit'];
        }

        if(is_numeric($params['status'])){ //状态
            $data.= " and pricing_rules.status = ".$params['status'];
        }

        if($is_count==true){
            $result = $this->table("pricing_configure")->alias("pricing_rules")->where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("pricing_configure")->alias('pricing_rules')->
                where($data)->limit($page, $page_size)->order('pricing_rules.departure_id,pricing_rules.arrival_id,billing_unit desc')->
                field(['pricing_configure_id','project_id','supplier_id','province_start_id','city_start_id','departure_id','province_end_id','city_end_id','arrival_id','billing_unit','goods_id','interval_start',
                    'interval_end','interval_value','unit_price','starting_price','picking_rules','picking_price',
                    'delivery_rules','delivery_price', 'unloading_rules','unloading_price','goods_type','orders_type',
                    "(select city_name from city where city_id=pricing_rules.departure_id)"=> 'departure_name',

                    "(select goods_name from goods where goods_id=pricing_rules.goods_id)"=> 'goods_name',
                    "pricing_rules.create_user_id","pricing_rules.update_user_id",
                    "pricing_rules.picking_id","pricing_rules.unloading_id","pricing_rules.delivery_id",
					"(select city_name from city where city.city_id = pricing_rules.province_end_id)"=>'province_end_name',
					"(select city_name from city where city.city_id = pricing_rules.city_end_id)"=>'city_end_name',
					"(select city_name from city where city.city_id = pricing_rules.arrival_id)"=>'arrival_name',
                    "(select nickname  from user where user.user_id = pricing_rules.create_user_id)"=> 'create_user_name',
                    "(select nickname  from user where user.user_id = pricing_rules.update_user_id)"=> 'update_user_name',
                    'from_unixtime(pricing_rules.update_time)'=>'update_time', 'from_unixtime(pricing_rules.create_time)'=>'create_time', "pricing_rules.status"])->select();
            }else{
                $result = $this->table("pricing_configure")->alias('pricing_rules')->
                where($data)->order('pricing_rules.departure_id,pricing_rules.arrival_id,billing_unit desc')->
                field(['pricing_configure_id','project_id','supplier_id','province_start_id','city_start_id','departure_id','province_end_id','city_end_id','arrival_id','billing_unit','goods_id','interval_start',
                    'interval_end','interval_value','unit_price','starting_price','picking_rules','picking_price',
                    'delivery_rules','delivery_price', 'unloading_rules','unloading_price','goods_type','orders_type',
                    "(select city_name from city where city_id=pricing_rules.departure_id)"=> 'departure_name',
 
                    "(select goods_name from goods where goods_id=pricing_rules.goods_id)"=> 'goods_name',
                    "pricing_rules.create_user_id","pricing_rules.update_user_id",
                    "pricing_rules.picking_id","pricing_rules.unloading_id","pricing_rules.delivery_id",
					"(select city_name from city where city.city_id = pricing_rules.province_end_id)"=>'province_end_name',
					"(select city_name from city where city.city_id = pricing_rules.city_end_id)"=>'city_end_name',
					"(select city_name from city where city.city_id = pricing_rules.arrival_id)"=>'arrival_name',					
                    "(select nickname  from user where user.user_id = pricing_rules.create_user_id)"=> 'create_user_name',
                    "(select nickname  from user where user.user_id = pricing_rules.update_user_id)"=> 'update_user_name',
                    'from_unixtime(pricing_rules.update_time)'=>'update_time', 'from_unixtime(pricing_rules.create_time)'=>'create_time', "pricing_rules.status"])->select();
            }
        }
    
    	return $result;
    
    }

    /**
     * 获取可用的计价配置区间（起）值
     * 韩
     */
    public function checkInterval($params){

        $data['supplier_id'] = $params['supplier_id'];
        $data['project_id'] = 0;
        $data['billing_unit'] = $params['billing_unit'];
        $data['departure_id'] = $params['departure_id'];
        $data['arrival_id'] = $params['arrival_id'];
        $data['status'] = 1;

        $result_data = $this->table("pricing_configure")->alias('pricing_rules')->where($data)->field('interval_value')->max("interval_value");

        if(!empty($result_data)){ //有值
            $result = ($result_data+0.1);
        }else{ //没有值
            $result = "区间可取任意值";
        }

        return $result;

    }

    /**
     * 承运商计价配置区间值批量修改AJAX
     * 韩
     */
    public function updatePricingConfigureBatchByPricingConfigureTypeId($params){

        $data['pricing_configure_id'] = $params['pricing_configure_id'];
        $data['interval_start'] = $params['interval_start'];
        $data['interval_end'] = $params['interval_end'];

        //通过当前数据查找相似数据
        $result_data = $this->table("pricing_configure")->alias('pricing_rules')->where("pricing_configure_id = ".$data['pricing_configure_id'])->find();
        $data_info['supplier_id'] = $result_data['supplier_id'];
        $data_info['project_id'] = 0;
        $data_info['billing_unit'] = $result_data['billing_unit'];
        $data_info['departure_id'] = $result_data['departure_id'];
        $data_info['arrival_id'] = $result_data['arrival_id'];
        $data_info['status'] = 1;

        $max = $this->table("pricing_configure")->alias('pricing_rules')->where($data_info)->field('interval_value')->max("interval_value");
        $min = $this->table("pricing_configure")->alias('pricing_rules')->where($data_info)->field('interval_value')->min("interval_start");
        if($data['interval_start']>$max || $data['interval_start']<$min){  //值满足条件
            //修改当前数据

            $data_up['interval_start'] = $data['interval_start'];
            $data_up['interval_end'] = $data['interval_end'];
            $data_up['interval_value'] = $data['interval_end'];

            Db::startTrans();
            try {
                Db::name('pricing_configure')->where("pricing_configure_id = " . $data['pricing_configure_id'])->update($data_up);
                $result = 1;
                // 提交事务
                Db::commit();
            } catch (\Exception $e) {
                $result = $e->getMessage();
                // 回滚事务
                Db::rollback();
            }
            return $result;
        }else{
            //返回错误信息
            $result = 2;
            return $result;
        }

    }

}