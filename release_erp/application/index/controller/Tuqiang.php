<?php

namespace app\index\controller;
use app\common\help\Contents;
use http\Client;
use think\Cookie;
use Underscore\Types\Arrays;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
class Tuqiang extends Base
{
	public $pre_url = "http://open.aichezaixian.com/route/rest";
	protected $app_key = "8FB345B8693CCD00FA6D4EFB446D28D1";
	protected $app_secret = "b8f59ae57d00448ca2c62343a9a9d0e4";

	/**
	 * 获取图强 access_token
     * method: jimi.oauth.token.get
     * kison
	 */
	public function get_access_token(){
        $rrr = [

        ];
        $rrr2 = [
            "app_key" => $this->app_key,
            "v" => "1.0",
            "timestamp" => date('Y-m-d H:i:s', time()),
            "sign_method" => "md5",
            "format" => "json",
            "method" => "jimi.oauth.token.get",
            "user_id" => "上海圣华",
            "user_pwd_md5" => md5("Aa123456"),
            "expires_in" => 7200
        ];
//        $asd = $this->generateSign($rrr2);
        $asd = $this->encrypt_curl($this->pre_url,$rrr,$rrr2);
        $info_array = json_decode($asd,true);

//        return $info_array;

        $rrr3 = [
            "equipment_id" => $_REQUEST['equipment_id'],
            "token" => $info_array['result']['accessToken'],
            "expiresin" => $info_array['result']['expiresIn'],
            "time" => $info_array['result']['time']
        ];

        //修改设备信息
        $result = $this->callSoaErp('post','/equipment/editEquipment',$rrr3);

        return $result;

	}

    /**
     * 获取设备列表
     * method: jimi.user.device.list
     */
    public function get_device_list(){
        $rrr = [

        ];
        $rrr2 = [
            "app_key" => $this->app_key,
            "v" => "1.0",
            "timestamp" => date('Y-m-d H:i:s', time()),
            "sign_method" => "md5",
            "format" => "json",
            "method" => "jimi.user.device.list",
            "access_token" => $_REQUEST['token'],
            "target"=>"上海圣华"
        ];
//        $asd = $this->generateSign($rrr2);
        $asd = $this->encrypt_curl($this->pre_url,$rrr,$rrr2);

        $info_array = json_decode($asd,true);

//        return $info_array;

        $rrr3 = [
            "equipment_id" => $_REQUEST['equipment_id'],
            "mc_type_use_scope" => $info_array['result'][0]['mcTypeUseScope'],
            "equip_ype" => $info_array['result'][0]['equipType'],
            "sim" => $info_array['result'][0]['sim'],
            "activation_time" => $info_array['result'][0]['activationTime']
        ];

        //修改设备信息
        $result = $this->callSoaErp('post','/equipment/editEquipment',$rrr3);

        return $result;
    }

    /**
     * 获取该账户下所有IMEI的最新定位数据
     * method: jimi.user.device.location.list
     */
    public function get_device_location_list(){
        $rrr = [

        ];
        $rrr2 = [
            "app_key" => $this->app_key,
            "v" => "1.0",
            "timestamp" => date('Y-m-d H:i:s', time()),
            "sign_method" => "md5",
            "format" => "json",
            "method" => "jimi.user.device.location.list",
            "access_token" => $_REQUEST['token'],
            "target"=>"上海圣华",
            "map_type"=>"BAIDU"
        ];
//        $asd = $this->generateSign($rrr2);
        $asd = $this->encrypt_curl($this->pre_url,$rrr,$rrr2);

        $info_array = json_decode($asd,true);

//        return $info_array;

        $rrr3 = [
            "equipment_id" => $_REQUEST['equipment_id'],
            "imei_status" => $info_array['result'][0]['status'], //设备状态
            "lng" => $info_array['result'][0]['lng'], //经度
            "lat" => $info_array['result'][0]['lat'], //纬度
            "elect_quantity" => $info_array['result'][0]['electQuantity'] //电量
        ];

        //修改设备信息
        $result = $this->callSoaErp('post','/equipment/editEquipment',$rrr3);

        $rrr4 = [
            "equipment_id" => $_REQUEST['equipment_id'],
            "imei" => $info_array['result'][0]['imei'],
            "device_name" => $info_array['result'][0]['deviceName'],
            "icon" => $info_array['result'][0]['icon'],
            "imei_status" => $info_array['result'][0]['status'], //设备状态
            "lng" => $info_array['result'][0]['lng'], //经度
            "lat" => $info_array['result'][0]['lat'], //纬度
            "expire_flag" => $info_array['result'][0]['expireFlag'],
            "activation_flag" => $info_array['result'][0]['activationFlag'],
            "pos_type" => $info_array['result'][0]['posType'],
            "gps_time" => $info_array['result'][0]['gpsTime'],
            "hb_time" => $info_array['result'][0]['hbTime'],
            "speed" => $info_array['result'][0]['speed'],
            "acc_status" => $info_array['result'][0]['accStatus'],
            "elect_quantity" => $info_array['result'][0]['electQuantity'],
            "power_value" => $info_array['result'][0]['powerValue'],
            "gps_num" => $info_array['result'][0]['gpsNum'],
            "direction" => $info_array['result'][0]['direction'],
            "mileage" => $info_array['result'][0]['mileage'],
        ];

        //插入历史表
        $result2 = $this->callSoaErp('post','/equipment/addEquipmentAll',$rrr4);

        return $result2;
    }

    /**
     * 生成sign请求
     * kison
     */
    public function encrypt_curl( $url2, $post = [], $get = [])
    {
        if (!$get) {
            return false;
        }
        ksort($get);
        $string = $this->app_secret;

        foreach ($get as $key => $value) {
            $string .= $key . $value;
            $url    .= $key . '=' . $value . '&';
        }
        $new_url    = $string.$this->app_secret;
        $sign       = strtoupper(md5($new_url));
        $url        .= 'sign=' . $sign;

        foreach($get as $k1=>$v1){
            $params['app_key'] = $get['app_key'];
            $params['v'] = $get['v'];
            $params['timestamp'] = $get['timestamp'];
            $params['sign_method'] = $get['sign_method'];
            $params['format'] = $get['format'];
            $params['method'] = $get['method'];
            if($params['method']=="jimi.oauth.token.get"){
                //获取token
                $params['user_id'] = $get['user_id'];
                $params['user_pwd_md5'] = $get['user_pwd_md5'];
                $params['expires_in'] = $get['expires_in'];
            }else if($params['method']=="jimi.user.device.list"){
                //设备列表
                $params['access_token'] = $get['access_token'];
                $params['target'] = $get['target'];
            }else if($params['method']=="jimi.user.device.location.list"){
                //IMEI的最新定位数据
                $params['access_token'] = $get['access_token'];
                $params['target'] = $get['target'];
                $params['map_type'] = $get['map_type'];
            }

        }
        $params['sign'] = $sign;
//        return $params['sign'];

        if (!empty($post)) {
            $all_params = array_merge($params,$post);
        }else{
            $all_params = $params;
        }

//        return $asd;

//        $client     = new \GuzzleHttp\Client();
//        $response   = $client->request('post', $url2, [
//            'form_params'=>$params,
//            'verify' => false
//        ]);
//        $respString = $response->getBody()->getContents();
//        $respData   = $respString;
        $options = array(
            CURLOPT_HTTPHEADER => array(
                "Content-Type:application/x-www-form-urlencoded",
            )
        );
        $respData   = $this->curlPost($url2,$all_params,$options);

        return $respData;
    }

    /**
     * curl方法
     * kison
     */
    public function curlPost($url = '', $postData = '', $options = array())
    {
        if (is_array($postData)) {
            $postData = http_build_query($postData);
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //设置cURL允许执行的最长秒数
        if (!empty($options)) {
            curl_setopt_array($ch, $options);
        }
        //https请求 不验证证书和host
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    /**
     * 奇门加密-生成sign
     * kison
     */
    public function generateSign($data)
    {
        ksort($data);

        $str = $this->app_secret;
        foreach ($data as $key => $datum) {
            $str .= $key . $datum;
        }
        $str .= $this->app_secret;

        $sign = strtoupper(md5($str));

        return $sign;
    }
	
}

