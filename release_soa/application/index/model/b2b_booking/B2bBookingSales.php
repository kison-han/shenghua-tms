<?php

namespace app\index\model\b2b_booking;
use app\index\model\btob\B2bReceivableCope;
use think\Exception;
use think\Model;
use think\Db;
class B2bBookingSales extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'b2b_booking_sales';
    public function initialize()
    {
    	parent::initialize();
    
    }


    public function addB2bBookingSales($params){

        $data['b2b_booking_id'] = $params['b2b_booking_id'];
        $data['product_code_id'] = $params['product_code_id'];
        $data['description'] = $params['description'];
        $data['qty'] = $params['qty'];
        $data['is_gst'] = $params['is_gst'];
        $data['net_total'] = $params['net_total'];
        $data['net_unit_price'] = $params['net_unit_price'];
    	$data['status'] = 'enable';

    	try{
    		$this->insert($data);
    	} catch (\Exception $e) {

            $result = $e->getMessage();
    		\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		exit();
    	}

    }

    public function updateB2bBookingSales($params)
    {
        try{
            $this->startTrans();
            foreach ($params['sales'] as $v)
            {
                $data['description'] = $v['description'];
                $data['qty'] = $v['qty'];
                $data['net_unit_price'] = $v['net_unit_price'];
                $data['net_total'] = bcmul($data['net_unit_price'], $data['qty']);
                $data['status'] = $v['status'];
                $data['is_gst'] = $v['is_gst'];

                $params['myob_sales'] = $params['myob_cost'] = 0.00;
                if ($data['net_total'] > 0.00)
                {
                    $params['invoice']['myob_sales'] = bcadd($data['net_total'], $params['myob_sales'],2);
                }
                else
                {
                    $params['invoice']['myob_cost'] = bcadd($data['net_total'], $params['myob_sales'],2);
                }

                if ($v['b2b_booking_sales_id'])
                {
                    $where = " b2b_booking_sales_id = ". $v['b2b_booking_sales_id'];
                    $this->where($where)->update($data);
                }
                else
                {
                    $data['b2b_booking_id'] = $params['b2b_booking_id'];
                    $data['product_code_id'] = $v['product_code_id'];
                    $this->insert($data);
                }
            }

            //获取应收应付
            $receivable_cope_model = new B2bReceivableCope();
            $receivable_cope = $receivable_cope_model->getB2bReceivableCope($params);
            foreach ($receivable_cope as $receivable_cope_v)
            {
                //应收
                if ($receivable_cope_v['receivable_cope_type'] == 1)
                {
                    $params['invoice']['myob_sales'] = bcadd($params['invoice']['myob_sales'], $receivable_cope_v['total_price'],2);
                }
                //应付
                if ($receivable_cope_v['receivable_cope_type'] == 2)
                {
                    $params['invoice']['myob_cost'] = bcsub($params['invoice']['myob_cost'], $receivable_cope_v['total_price'],2);
                }
            }

            //更改invoice
            $invoice_model = new B2bBookingInvoice();
            $params['invoice']['b2b_booking_id'] = $params['b2b_booking_id'];
            $invoice_model->updateB2bBookingInvoice($params['invoice']);

            $this->commit();
            $result = 1;
        } catch (\Exception $e) {
            $this->rollback();
            $result = $e->getMessage();

            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }
        return $result;
    }

    public function getB2bBookingSalesByBtbBookingId($b2b_booking_id)
    {
        try
        {
            $where['b2b_booking_id'] = $b2b_booking_id;

            $result = $this->table("b2b_booking_sales")->alias('b2b_booking_sales')
                ->join('b2b_product', 'b2b_product.product_id = b2b_booking_sales.product_code_id')
                ->field(['b2b_booking_sales.*', 'b2b_product.product_name', 'b2b_product.product_code'])
                ->where($where)->select();
        }
        catch (Exception $e)
        {
            $result = $e->getMessage();
            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }

        return $result;
    }

}