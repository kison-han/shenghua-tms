<?php

namespace app\index\model\finance;

use think\Model;
use app\common\help\Help;
use app\index\service\PublicService;
use think\config;
use think\Db;

class PriceItem extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'price_item';
    private $_languageList;
    private	$_cope;
    private $_public_service;
    public function initialize()
    {
    	
   

        $this->_public_service = new PublicService();
        parent::initialize();

    }

    /**
     * 添加price item
     * 胡
     */
    public function addPriceItem($params){
	
	
        $t = time();
		$data['price_item_name'] = $params['price_item_name'];
       
		if(!empty($params['remark'])){
			$data['remark'] = $params['remark'];
		}

		$data['status'] = 1;
		$data['price_item_code'] = $params['price_item_code'];
		
		
		
	
        $this->startTrans();
        try{
            $pk_id = $this->insertGetId($data);
           
			
            // 提交事务
            $this->commit();
            $result = $pk_id;

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            $this->rollback();

        }
		
        return $result;
    }
    /**
     * 修改cost type
     * 胡
     */
    public function updatePriceItem($params){
	
	
 
		$data['price_item_name'] = $params['price_item_name'];
       
		if(!empty($params['remark'])){
			$data['remark'] = $params['remark'];
		}

		$data['status'] = $params['status'];
		
		
		
		
	
        $this->startTrans();
        try{
            $pk_id = $this->where("price_item_id =".$params['price_item_id'])->update($data);
           
			
            // 提交事务
            $this->commit();
            $result = $pk_id;

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            $this->rollback();

        }
		
        return $result;
    }
    /**
     * 获取
     * 胡
     */
    public function getPriceItem($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
		


    	if(is_numeric($params['status'])){
    		$data['status']= $params['status'];
    	}

		if(!empty($params['price_item_name'])){
			$data['price_item_name']= array('like',"%".$params['price_item_name']."%");
		}
		if(!empty($params['price_item_id'])){
			$data['price_item_id']= $params['price_item_id'];
		}

		
        if($is_count==true){
				$result = $this->
                where($data)->count();
        }else {
            if ($is_page == true) {
                $result= $this->
                where($data)->limit($page, $page_size)->order('price_item_id desc')->
				select();
            } else {
                $result = $this->where($data)->order('price_item_id desc')->select();
            }
        }
		
				
		
		return $result;
    }

    
}