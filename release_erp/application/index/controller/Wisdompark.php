<?php

namespace app\index\controller;
use app\common\help\H;
use think\Cookie;
use Underscore\Types\Arrays;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
class Wisdompark extends Base
{


  //  $data = Request::instance()->param();
    
    /**
     *  圣华
     *  智慧园区驾驶舱
     *  
     */
	 public function index(){

        return $this->fetch('index');		 
		 
		 
	 }

    
    /**
     * 获取出入口列表
     */
    public function get_park_list(){
    	
    	$params = Request::instance()->param();
    	
    	$data=[

    	];

    	
    	
    	$result = action('haikang/get_park_list',['postData' => $data]);
    	
    	
    	dump($result);
    }
    
    /**
     * 获取停车库列表
     */
    public function showParkList(){
    	
    	//获取门
    	$data=[
    			'pageNo'=>$this->page(),
    			'pageSize'=>$this->_page_size,
    			 
    	];
    	$result = action('haikang/park_list',['postData' => $data]);
		    	
    	$this->assign('data',$result['data']);


    	return $this->fetch('park_list');
    }
    
    /**
     * 查询过车记录
     */
    public function showCrossRecords(){
    	//获取门
    	$data=[
    			'pageNo'=>$this->page(),
    			'pageSize'=>$this->_page_size,    		
    	];
    	$result = action('haikang/cross_records',['postData' => $data]);
		
//     	$this->assign('data',$result['data']);
    	
    	$this->getHaikangPageParams($result);
     	return $this->fetch('cross_records');
    	
    }
}

