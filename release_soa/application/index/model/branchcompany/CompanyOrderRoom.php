<?php

namespace app\index\model\branchcompany;
use app\common\help\Help;
use think\Exception;
use think\Model;
use think\Db;
class CompanyOrderRoom extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'company_order_room';
    public function initialize()
    {
    	parent::initialize();
    
    }



    public function updateCompanyOrderRoom($params)
    {
        try{
            $this->startTrans();
            $this->where(['company_order_number' => $params['company_order_number']])->update(['status'=>0]);
            $params['accommodation'] = [];
            foreach ($params['room'] as $k=>$v)
            {
                $data['temp_group'] = $v['temp_group'];
                $data['seat_group'] = trim($v['seat_group'],',');
                $data['room_type'] = $v['room_type'];
                $data['room_code'] = $v['room_code'];
                $data['charge_num'] = $v['charge_num'];
                $data['charge_type'] = $v['charge_type'];
                $data['status'] = 1;

                if ($v['company_order_room_id'])
                {
                    $where = " company_order_room_id = ". $v['company_order_room_id'];
                    $this->where($where)->update($data);
                }
                else
                {
                    $data['company_order_number'] = $params['company_order_number'];
                    $this->insert($data);
                }
                $temp_group = explode(',', $v['temp_group']);
                foreach ($temp_group as $temp_group_v)
                {
                    $company_order_customer_info = $this->table('company_order_customer')->where(['company_order_customer_id' => $temp_group_v])->find();
                    $data['customer_id'] = $company_order_customer_info['customer_id'];
                    $data['company_order_customer_id'] = $temp_group_v;

                    array_push($params['accommodation'], $data);
                    unset($data['company_order_customer_id']);
                    unset($data['customer_id']);
                }
            }

            $company_order_accommodation = new CompanyOrderAccommodation();
            $company_order_accommodation->updateAccommodation($params, 'room');

            $this->commit();
            $result = 1;
        } catch (\Exception $e) {
            $this->rollback();
            $result = $e->getMessage();
            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }

        return $result;
    }

    public function getRoomByOrderNumber($company_order_number)
    {
        try
        {
            $where['company_order_number'] = $company_order_number;
            $where['status'] = 1;
            $result = $this->where($where)->alias('company_order_room')
                ->field(['company_order_room.*',
                    "(select room_type_name from room_type where company_order_room.room_type = room_type.room_type_id)"=>'room_name'
                    ])
                ->select();
        }
        catch (Exception $e)
        {
            $result = $e->getMessage();
            \think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
            exit();
        }

        return $result;
    }

}