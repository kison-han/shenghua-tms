<?php if (!defined('THINK_PATH')) exit(); /*a:5:{s:85:"/var/www/html/test_erp/public/../application/index/view/financetmsprice/invoiced.html";i:1664504127;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:64:"/var/www/html/test_erp/application/index/view/public/header.html";i:1658978091;s:75:"/var/www/html/test_erp/application/index/view/public/left_finance_menu.html";i:1657100018;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






    <title>收入管理</title>

</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
            <div class="layui-logo"></div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left">
        <li class="layui-nav-item">
            <a href="/turnoverbox/index">智能周转箱</a>
        </li>
        <li class="layui-nav-item">
            <a href="/">TMS</a>
        </li>
        <li class="layui-nav-item">
 
            <a href="/warehouse/index">WMS</a>
        </li>
        <li class="layui-nav-item">
            <a href="/wisdompark/index">智慧园区</a>

        </li>
        <li class="layui-nav-item">
            <a href="/Oa/index">OA</a>

        </li>   
         <li class="layui-nav-item">
            <a href="/Bms/index">BMS</a>

        </li>
        <li class="layui-nav-item">
            <a href="/equipment/equipmentManage">设备</a>

        </li>
    </ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item tips-system-message" style="cursor: pointer"><?php echo $language_tag['index_nav_system_message']; ?> <span class="system-message-quantity" style="color: red"> 1</span> </li>
      <li class="layui-nav-item">
        <a href="javascript:;">
          <!-- <img src="http://t.cn/RCzsdCq" class="layui-nav-img"> -->
          <?php echo \think\Session::get('user.nickname'); ?>
        </a>
        <dl class="layui-nav-child head-top">
          <dd><a href="/system/setUserInfo/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_basicDocument']; ?></a></dd>
          <dd><a href="/system/showChangePassword/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_resetPassword']; ?></a></dd>
        </dl>
      </li>
      <li class="layui-nav-item"><a href="/login/loginOut">退出登录</a></li>
    </ul>

    <div id="tips-system-message-js" style='display:none;'>
        <div  style="max-height:350px;width: 450px">
             <div style="color: #000;text-align: center;height: 10px;padding: 10px">你有 <font class="f-tips-system-message"></font> 条未读消息</div>
             <hr>
             <div class="tips-system-message-div" style="height:220px;overflow-y: auto">
           
                 <li style="color: #000; padding: 10px;cursor: pointer" data-href="" data-id="" class="aUrl" onclick="Aurl(this)" ></li>
                 <hr>
             
             </div>
            <div style="color: #000;text-align: center;height: 40px;padding-top: 15px"><a href="/reminderManagement/allInStationLetter"><?php echo $language_tag['index_nav_viewAll_messa']; ?></a></div>
        </div>
    </div>

    <!--  下列保存用户SESSION信息 -->
	
    <input type="hidden" id='now_url'  value="<?php echo $now_url; ?>" />
	<input type="hidden" id='after_url'  value="<?php echo $after_url; ?>" />
    <input type="hidden" id='user_company_id' value="<?php echo \think\Session::get('user.company_id'); ?>" />
	<!--  下列保存其他信息 -->
	<input type="hidden" id='http_referer' value="<?php echo $http_referer; ?>" />
	
	<script type='text/javascript'>
		$('.tips-system-message').on('click',function(){
			var html = $('#tips-system-message-js').html();
		    layer.tips(html, '.tips-system-message', {
		        tips: [3, '#fff'],
		        padding:'20',
		        tipsMore: false,
		        area: ['450px', 'auto'],
		        shade: [0.01, '#fff'],
		        shadeClose:true,
		        time:0
			
		    });
		
		});
		
		$(document).ready(function(){ 
			$(document).mousemove(function(e){ 
				
				if(e.pageX<=10){
					$('.layui-bg-black').show()
				}
				
				if(e.pageX>200){
					$('.layui-bg-black').hide()
				
				}
				if(e.pageY<=10){
					$('.layui-header').show()
				}
				
				if(e.pageY>40){
					$('.layui-header').hide()
				
				}			
			}); 
			
		}); 
	

	</script>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
                  <ul class="layui-nav layui-nav-tree" id="left-nav" lay-filter="test">
          <li class="layui-nav-item"><a href="/"><i class="iconfont img-homepage_fill"></i><em>控制面板</em></a></li>
        <li <?php if($controller_name == 'bmscost'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>成本管理</em></a>
          <dl class="layui-nav-child">
              	<dd <?php if(in_array(($function_name), explode(',',"tmscost,addtmscost"))): ?> class="layui-this" <?php endif; ?> ><a href="/bmscost/tmscost">TMS</a></dd>
          </dl>
        </li>

        <li <?php if($controller_name == 'financetmsprice'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>收入管理</em></a>
          <dl class="layui-nav-child">
				<dd <?php if(in_array(($function_name), explode(',',"tmsprice"))): ?> class="layui-this" <?php endif; ?> ><a href="/financetmsprice/pricereceipt">TMS</a></dd>
          </dl>
        </li>  
        <li <?php if($controller_name == 'bmspeibi'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
          <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>报表</em></a>
          <dl class="layui-nav-child">
				<dd <?php if(in_array(($function_name), explode(',',"tmsprice"))): ?> class="layui-this" <?php endif; ?> ><a href="/bmsbaobiao/tmspeibi">营收配比表</a></dd>
          </dl>
        </li> 		
          <!--<li <?php if($controller_name == 'otaarticle' or $controller_name == 'enquirty' or $controller_name == 'otasystem' or $controller_name == 'otaslide' or $controller_name == 'otaproduct' or $controller_name == 'otamember'): ?>-->
              <!--class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>-->
          <!--<a class="" href="javascript:void(0)"><i class="layui-icon layui-icon-website"></i><em>官网管理</em></a>-->
          <!--<dl class="layui-nav-child">-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showotasystemmanage"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_system/showOtaSystemManage">网站设置</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showotamenumanage"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_system/showOtaMenuManage?status=1">菜单</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showarticletypemanage,showarticletypeadd,showarticletypeedit,showarticletypeinfo"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_article/showArticleTypeManage?status=1">文章分类</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showarticlemanage,showarticleadd,showarticleedit,showarticleinfo"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_article/showArticleManage?status=1">文章</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showslidemanage,showslideadd,showslideedit,showslideinfo"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_slide/showSlideManage?status=1">幻灯片</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showadvertmanage,showadvertadd,showadvertedit,showadvertinfo"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_slide/showAdvertManage?status=1">友情链接</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showadvertisingmanage"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_slide/showAdvertisingManage?status=1">广告位</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"showcompanywebsitemanage"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_system/showCompanyWebsiteManage?status=1">域名管理</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"index,addenquirty,editenquirty"))): ?> class="layui-this" <?php endif; ?> ><a href="/enquirty/index">需求定制</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"lst,add,edit"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_member/lst?status=1">账号管理</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"types,addtype,edittype,gettypeinfo"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_product/types?status=1">旅游产品分类</a></dd>-->
              <!--<dd <?php if(in_array(($function_name), explode(',',"productlists,addlist,editlist"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_product/productLists?status=1">旅游产品列表</a></dd>-->
              <!--&lt;!&ndash;<dd <?php if(in_array(($function_name), explode(',',"products,addlist,editlist"))): ?> class="layui-this" <?php endif; ?> ><a href="/ota_product/products?status=1">旅游产品</a></dd>&ndash;&gt;-->
          <!--</dl>-->

          <!--</li>-->
      </ul>
        </div>
    </div>

    <div class="layui-body layui-body-bg">

        <!-- 内容主体区域 -->
        <div class="content_body">
            <div class="body-top">
                <div class='layui-form-item'>
                    <span class="layui-breadcrumb" lay-separator="-">
                        <a>首页</a>
                        <a>财务</a>
                        <a>收入管理</a>
                        <a><cite>TMS</cite></a>
                    </span>
                </div>
                <div class='layui-block all-search-bg'>
		        <div class="layui-tab">
				  <ul class="layui-tab-title">

					<li ><a href="/financetmsprice/pricereceipt">待开票</a></li>
					<li class="layui-this"><a href="/financetmsprice/invoiced">已开票/不开票</a></li>
					<li ><a href="/financetmsprice/sendout">已送出待收款</a></li>
					<li ><a href="/financetmsprice/received">已收款</a></li>		
											
				  </ul>
				
				</div>				
                    <form class="layui-form" method='get' action='/financetmsprice/invoiced'>
                        <div class="layui-row">

                          
                                <div class="layui-input-inline">
                                    <label class="layui-form-label">对账编号:</label>
                                    <div class="layui-input-inline">
                                        <input type="text" id="" name="customer_bill_number" maxlength="300" autocomplete="off" value="<?php echo \think\Request::instance()->get('customer_bill_number'); ?>"   class="layui-input">
                                    </div>
                                </div>
                    

                         
                                <div class="layui-input-inline">
                                    <label class="layui-form-label">对账日期:</label>
                                    <div class="layui-input-inline">
                                        <input type="text" id="customer_bill_date" name="customer_bill_date" maxlength="300" autocomplete="off" value="<?php echo \think\Request::instance()->get('customer_bill_date'); ?>"   class="layui-input">
                                    </div>
                                </div>
                      						
							
                          
                                <div class="layui-input-inline">
                                    <label class="layui-form-label">项目名称:</label>
                                    <div class="layui-input-inline">
                                        <select id="" name="project_id"  lay-search>
                                            <option value="" >-状态-</option>
                                            <?php if(is_array($projectResult) || $projectResult instanceof \think\Collection || $projectResult instanceof \think\Paginator): if( count($projectResult)==0 ) : echo "" ;else: foreach($projectResult as $key=>$v): ?>
                                              	<option value="<?php echo $v['project_id']; ?>" <?php if(\think\Request::instance()->get('project_id') == $v['project_id']): ?>selected<?php endif; ?>><?php echo $v['project_name']; ?></option>
                                            <?php endforeach; endif; else: echo "" ;endif; ?>
                                        </select>                                    
                                        
                                        </div>
                                </div>
                      
                         
             
                       
								<button class="layui-btn nav-search search_button" >搜索</button>


                        </div>
                        <!--<div class='layui-input-inline'>-->
                        <!--<input type="text" id="" name=""  placeholder="电话、联系人、操作人" class="layui-input">-->
                        <!--</div>-->
    
                    </form>

                </div>
            </div>
            <div class="content-bg" >                  
				<table id="layui-table"  lay-filter="layui-filter">
               
                </table>
     
            </div>

        </div>
    </div>


</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
<script src='/static/javascript/system/source.js'></script>
<script>


layui.use(['table'], function(){
    var table = layui.table;
    var   laytable =layui.laytable
 layui.laydate.render({
    elem: '#customer_bill_date' ,//input的id
	type:'month'
});	
   
table.render({
	    elem: '#layui-table'
	    ,height: tableHeight
	    ,url: '/financetmsprice/getPricereceiptAjax?customer_bill_number=<?php echo \think\Request::instance()->get('customer_bill_number'); ?>&project_id=<?php echo \think\Request::instance()->get('project_id'); ?>&customer_invoice_status=2&customer_bill_date=<?php echo \think\Request::instance()->get('customer_bill_date'); ?>' //数据接口
	
	    ,response: {

	           statusCode: 200 //规定成功的状态码，默认：0

	          } 
	    ,page: true //开启分页
		,limits:[100,200,300,400,500]
	    ,limit:100
	    ,toolbar: '#toolbarDemo' //开启工具栏，此处显示默认图标，可以自定义模板，详见文档
	    ,totalRow: true //开启合计行
	    ,cols: [[ //表头
         	
	             	 {type: 'checkbox', fixed: 'left'}	
					,{field: 'customer_bill_status_name', title: '账单状态',width:100}	 
					,{field: 'customer_invoice_status_name', title: '发票状态',width:100}						
					,{field: 'customer_bill_number', title: '账单编号',width:100}
					,{field: 'customer_bill_name', title: '账单名称',width:100}
					,{field: 'create_time', title: '对账日期',width:70}
					,{field: 'project_name', title: '项目名称',width:70}
					,{field: 'money', title: '运费总计',width:110,totalRow:true}					 
	             	
	             
	             	,{field: 'abnormal_money', title: '赔款',width:70,totalRow:true}
					,{field: 'customer_bill_money', title: '开票金额',width:70,totalRow:true}
	      			
	      			
					,{field: 'customer_bill_tax_type_name', title: '是否含税',width:70}
					
					,{field: 'customer_bill_tax', title: '税率',width:70}
					,{field: 'pay_type', title: '支付方式',width:70}
					
	      			,{field: 'create_user_name', title: '提交账单负责人',width:100}
					,{field: 'create_invoice_time', title: '提交账单时间',width:80}
					,{field: 'customer_invoice_name', title: '开票人',width:80}

	    ]]  

	  });
    //监听头工具栏事件
    table.on('toolbar(layui-filter)', function(obj){
      var checkStatus = table.checkStatus(obj.config.id)
      ,data = checkStatus.data; //获取选中的数据
	  
      switch(obj.event){
        case 'add':
            if(data.length !=1){
				layer.msg('请选择一行数据');
			
                
            }else {
				if(data[0].customer_bill_status!=2){
					layer.msg('请选择财务确认状态的数据');
					return false;
				}
				if(data[0].customer_invoice_status!=1){
					layer.msg('请选择未开票的数据进行操作');
				}else{
					$.ajax({
						type: "post",
						url: "/financetmsprice/updatepricereceiptAjax",
						data: {customer_bill_id:data[0].customer_bill_id,customer_invoice_status:2},
						dataType: "json",
						success: function(data){

							if(data.code==200){
								layer.msg('操作成功',function(){
									
									location.reload();
									
								},500)
							}
						},
						error:function(XMLHttpRequest, textStatus, errorThrown){
							layer.msg('验证失败')
						}
					});						
				}
			
	
			}
        break;
        case 'update':
          if(data.length === 0){
            layer.msg('请选择一行');
          } else if(data.length > 1){
            layer.msg('只能同时编辑一个');
          } else {
 
        	  location.href="/shipment/addLine?orders_number="+checkStatus.data[0].orders_number+"&shipment_uuid="+checkStatus.data[0].shipment_uuid
      
          }
        break;
        case 'delete':
          if(data.length === 0){
            layer.msg('请选择一行');
          } else {
            layer.msg('删除');
          }
        break;
        case "print_finance_number":
  
        	for(var i=0;i<data.length;i++){
              	
            	$.ajax({
            		url:"/index/changeStatus",
            		data:{table_id:data[i].shipment_uuid,table_id_name:'shipment_uuid',table_name:"shipment",status:"1",field:"is_print"},
            		success:function(e){
            			window.location.reload();
            		}
            		
            	})    
        	}
      
  	
      };
    });
    
    
    $(document).on('click','.guanzhang',function(){
    	var customer_bill_id =$(this).attr('customer_bill_id')
    	layer.open({
    		  title: '客户关账'
    		 ,btn: ['确认', '取消',]
    		 ,content: '请仔细核对账目，一经确认无法修改'
    		  ,yes: function(index, layero){
    			    data = {customer_bill_id:customer_bill_id,customer_bill_status:4}
    			    $.ajax({
    			        type: "post",
    			        url: "/financetmsprice/updatepricereceiptAjax",
    			        data: data,
    			        dataType: "json",
    			        success: function(data){

 							location.reload();
    			        },
    			        error:function(XMLHttpRequest, textStatus, errorThrown){
    			            layer.msg('验证失败')
    			        }
    			    });		

    				    
    		  }
    		  ,btn2: function(index, layero){
    		    //按钮【按钮二】的回调
    		    
    		    //return false 开启该代码可禁止点击该按钮关闭
    		  }
    		});     
    		    	
    	
    })
    
});

</script>
</body>
</html>


