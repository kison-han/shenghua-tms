<?php

namespace app\index\controller;
use app\common\help\H;
use think\Cookie;
use Underscore\Types\Arrays;
use think\Session;
use think\Paginator;
use think\Request;
use think\Controller;
class Wisdompersion extends Base
{


  //  $data = Request::instance()->param();
    
    /**
     *  圣华
     *  智慧园区驾驶舱
     *  
     */
	 public function index(){

        return $this->fetch('index');		 
		 
		 
	 }
	 /**
	  * 获取人员列表
	  */
	 public function showPersionList(){
	 
	 	$data=[
	 			'pageNo'=>$this->page(),
	 			'pageSize'=>$this->_page_size,
	 
	 	];
	 	$result = action('haikang/get_person_list',['postData' => $data]);
	 	 
	 	 
	 	$this->getHaikangPageParams($result);
	 	 
	 
	 	return $this->fetch('persion_list');
	 }
	   
    /**
     * 获取门禁列表
     */
    public function showDoorList(){

       $data=[
            'pageNo'=>$this->page(),
            'pageSize'=>$this->_page_size,

        ];
    	$result = action('haikang/get_door_list',['postData' => $data]);
    	
    	
    	$this->getHaikangPageParams($result);
    	

       return $this->fetch('door_list');
    }    
    /**
     * 获取门禁点事件
     */
    public function showDoorEvent(){
    
    	$data=[
    			'pageNo'=>$this->page(),
    			'pageSize'=>$this->_page_size,
    			'startTime'=>date(DATE_ISO8601,strtotime("2021-11-10 00:00:00")),
    			'endTime'=>date(DATE_ISO8601,strtotime("2021-12-20 00:00:00"))
    	];
    	if(input('door_index_code') !=''){
    		$data['doorIndexCodes'] =array(input('door_index_code'));
    	}
	

    	$result = action('haikang/get_door_event',['postData' => $data]);
    	 
    	
    	$this->getHaikangPageParams($result);
    	
    	//获取门
    	$data=[
    			'pageNo'=>$this->page(),
    			'pageSize'=>$this->_page_size,
    	
    	];
    	$doorResult = action('haikang/get_door_list',['postData' => $data]);
    	$this->assign('doorResult',$doorResult['data']['list']);
    	return $this->fetch('door_event');
    } 
    
    //门禁点反控
    public function doorControl(){
    	$params = Request::instance()->param();

    	$data=[
    		'controlType'=>2
    	];

    	$data['doorIndexCodes'] =array($params['doorIndexCodes']);

    	$result = action('haikang/door_control',['postData' => $data]);
    	return $result;
    }
    
    /**
     * 获取出入口列表
     */
    public function get_park_list(){
    	
    	$params = Request::instance()->param();
    	
    	$data=[

    	];

    	
    	
    	$result = action('haikang/get_park_list',['postData' => $data]);
    	
    	
    	dump($result);
    }
    
    /**
     * 获取停车库列表
     */
    public function showParkList(){
    	
    	//获取门
    	$data=[
    			'pageNo'=>$this->page(),
    			'pageSize'=>$this->_page_size,
    			 
    	];
    	$result = action('haikang/park_list',['postData' => $data]);
		    	
    	$this->assign('data',$result['data']);


    	return $this->fetch('park_list');
    }
    
    /**
     * 查询过车记录
     */
    public function showCrossRecords(){
    	//获取门
    	$data=[
    			'pageNo'=>$this->page(),
    			'pageSize'=>$this->_page_size,    		
    	];
    	$result = action('haikang/cross_records',['postData' => $data]);
		
//     	$this->assign('data',$result['data']);
    	
    	$this->getHaikangPageParams($result);
     	return $this->fetch('cross_records');
    	
    }
}

