<?php


namespace app\index\model\dispatch;


use app\common\help\Contents;
use app\index\model\orders\Orders;
use app\index\model\system\FeeType;
use think\Model;

class DispatchFee extends Model
{
    protected $table = "dispatch_fee";


    public function getFeeList($params, $openCount = false)
    {

        $where = [];

        if (isset($params['orders_number'])) {
            $order_ids = Orders::where('orders_number', $params['orders_number'])->column('orders_id');
            if ($order_ids)
                $where['orders_id'] = ['in', $order_ids];
        }
        if (isset($params['fee_name'])) {
            $where['fee_name'] = ['like', '%' . $params['fee_name'] . '%'];
        }


        if (isset($params['transport_number'])) {

            $dispatch_ids = Dispatch::where('transport_number', $params['transport_number'])->column('dispatch_id');
            if ($dispatch_ids)
                $where['dispatch_id'] = ['in', $dispatch_ids];


        }


        $where['status'] = 1;
        if ($openCount) {
            $page = isset($params['page']) ? $params['page'] : 1;
            $pageSize = isset($params['$pageSize']) ? $params['$pageSize'] : Contents::PAGE_SIZE;
            $data['count'] = $this->where($where)->count();
            $data['page_count'] = ceil($data['count'] / $pageSize);
            $tinfo = $this->where($where)->page($page, $pageSize)->order('dispatch_fee_id', 'desc')->select();
            foreach ($tinfo as $k => $v) {
                $tinfo[$k]['dispatch'] = Dispatch::where('dispatch_id', $v['dispatch_id'])->find();

            }

            $data['list'] = $tinfo;
            return $data;
        } else
            return $this->where($where)->order('dispatch_id', 'desc')->select();


    }


    public function add($data, $dispatch_id, $orders_id, $params)
    {

        error_log(json_encode($data));

        foreach ($data as $k => $v) {
            foreach ($v as $k1 => $v1) {
                if (isset($v1['money'])) {
                    $v1['orders_id'] = $orders_id;
                    $v1['dispatch_id'] = $dispatch_id;
                    $v1['create_time'] = time();
                    $v1['user_id'] = isset($params['user_id']) ? $params['user_id'] : 0;
                    $fee_info = FeeType::get($v1['fee_id']);
                    $v1['fee_name'] = $fee_info['fee_name'];
                    $this->allowField(true)->insert($v1);
                }

            }

            if (isset($v['money'])) {
                $v['orders_id'] = $orders_id;
                $v['dispatch_id'] = $dispatch_id;
                $v['create_time'] = time();
                $v['user_id'] = isset($params['user_id']) ? $params['user_id'] : 0;
                $fee_info = FeeType::get($v['fee_id']);
                $v['fee_name'] = $fee_info['fee_name'];
                $this->allowField(true)->insert($v);
            }


        }

    }

}