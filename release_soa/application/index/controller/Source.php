<?php
namespace app\index\controller;
use app\common\help\Contents;
use app\common\help\Help;

use app\index\model\orders\OrdersOperating;
use think\config ;

use app\index\model\source\Vehicle;
use app\index\model\source\VehicleType;
use app\index\model\source\Supplier;
use app\index\model\source\CustomerSupplier as cs;
use app\index\model\source\CustomerSendGoods as csg;
use app\index\model\source\SupplierLine;
use app\index\model\source\Driver;
use app\index\model\source\Customer;
use app\index\model\source\Project;
use app\index\model\source\City;
use app\index\model\source\AcceptGoods;
use app\index\model\source\SendGoods;
use app\index\model\source\Goods;
use app\index\model\source\PricingConfigure as pc;
use app\index\model\source\ProjectPricingConfigure as ppc;
use app\index\model\source\ProjectRule as pr;
use think\Model;
class Source extends Base
{

	private $_vehicle;
	private $_vehicleType;
	private $_customer;
	private $_driver;
	private $_project;
	private $_acceptGoods;
	private $_sendGoods;
	private $_goods;
	private $_supplierLine;
	private $_city;
    //_lang Base里的属性，
    public function __construct()
    {
    	$this->_language = config("systom_setting")['language_default'];
    	$this->_vehicleType = new VehicleType();
		$this->_vehicle = new Vehicle();
		$this->_driver = new Driver();
		$this->_customer = new Customer();
		$this->_project = new Project();
		$this->_city = new City();
		$this->_acceptGoods = new AcceptGoods();
		$this->_sendGoods = new SendGoods();
		$this->_goods = new Goods();
		$this->_supplierLine = new SupplierLine();
        parent::__construct();
    }
    /**
     * 获取客户
     * 胡
     */
    public function getCustomer(){
	
    	$params = $this->input();
        
    	$customer = new Customer();

        if(isset($params['page'])){
            $page_size =  isset($params['page_size'])?$params['page_size']:Contents::PAGE_SIZE;
            $page = ($params['page']-1)*$page_size;
            $count = $customer->getCustomer($params, true);
            $result = $customer->getCustomer($params,false,'true',$page,$page_size);
            $data = [
                'count'=>$count,
                'list'=>$result,
                'page_count'=>ceil($count/$page_size)
            ];

            return $this->output($data);
        }
    	$customerResult = $customer->getCustomer($params);
    	$this->outPut($customerResult);
    
    }  
    /**
     * 添加客户数据
     * 胡
     */
    public function addCustomer(){
    
    	$params = $this->input();

    	$paramRule = [
    			
    			'customer_name'=>'string',
    			'company_name'=>'string',
    			'cellphone'=>'string',
    			'status'=>'status',
    	];
    
    	$this->paramCheckRule($paramRule,$params);

    	//开始判断名字是否重复
    	$data = [
    			'company_name'=>$params['company_name'],
    	
    	];
    	$this->checkNameIsRepetition('customer',$data);
    	//结束判断名字重复

    	$customerResult = $this->_customer->addCustomer($params);
    
    	$this->outPut($customerResult);
    }
    /**
     * 修改客户数据
     * 胡
     */
    public function updateCustomerByCustomerId(){
    
    	$params = $this->input();
    
    	$paramRule = [
    			 
    			'customer_name'=>'string',
    			'company_name'=>'string',
    			'cellphone'=>'string',
    			'customer_id'=>'string',
    	];
    	$this->paramCheckRule($paramRule,$params);
    	
    	//开始判断名字是否重复


    
    	$customer_data['customer_id'] = $params['customer_id'];
    	$customerResult = $this->_customer->getCustomer($customer_data);

    	//重复性验证
    	if($customerResult[0]['company_name'] == $params['company_name']){
    	}else{
    		//开始判断名字是否重复
    		$data = [
    				'company_name'=>$params['company_name'],
    	
    		];
    		$this->checkNameIsRepetition('customer',$data);
    		//结束判断名字重复
    	}

    	$customerResult = $this->_customer->updateCustomerByCustomerId($params);
    
    	$this->outPut($customerResult);
    }  
    /**
     * 添加供应商数据
     * 胡
     */
    public function addSupplier(){
    
    	$params = $this->input();

    	$paramRule = [
 			'supplier_name' => 'string',

    		'status'=>'number',
    	];
    		
    	$this->paramCheckRule($paramRule,$params);
        error_log(print_r($params,1));
    	//开始判断名字是否重复
    	$data = [
    		'supplier_name'=>$params['supplier_name'],

    	];
    	$this->checkNameIsRepetition('supplier',$data);
    	//结束判断名字重复
    
    	$supplier = new Supplier();

    	$params['supplier_number'] = Help::getNumber(51);
		
    	$supplierResult = $supplier->addSupplier($params);
    
    	$this->outPut($supplierResult);
    }

    /**
     * 添加供应商数据和复制数据
     * 胡
     */
    public function addSupplierAndCopyOther(){

        $params = $this->input();

        $paramRule = [
            'supplier_name' => 'string',
            'status'=>'number',
        ];

        $this->paramCheckRule($paramRule,$params);

        $supplier = new Supplier();

        $params['supplier_number'] = Help::getNumber(51);

        $supplierResult = $supplier->addSupplierAndCopyOther($params);

        $this->outPut($supplierResult);
    }

    /**
     * 获取供应商数据
     * 胡
     */
    public function getSupplier(){

    	$params = $this->input();
       
    	$supplier = new Supplier();
    	/*
    	 if(isset($params['lang'])){
    	 $lang = $params['lang'];
    	 }else{
    	 $lang = [];
    	 }
    	*/
        if(isset($params['page'])){
            $page_size =  isset($params['page_size'])?$params['page_size']:Contents::PAGE_SIZE;
            $page = ($params['page']-1)*$page_size;
            $count = $supplier->getSupplier($params, true);
            $result = $supplier->getSupplier($params,false,'true',$page,$page_size);
            $data = [
                'count'=>$count,
                'list'=>$result,
                'page_count'=>ceil($count/$page_size)
            ];

            return $this->output($data);
        }
    
    	$supplierResult = $supplier->getSupplier($params);
    	$this->outPut($supplierResult);
    
    }



    /**
     * 修改供应商
     * 胡
     */
    
    public function updateSupplierBySupplierId(){
    	$params = $this->input();
        $supplier = new Supplier();
    	$supplierInfo = $supplier->getOneSupplier($params['supplier_id']);
		
		
        //重复性验证
    	if($supplierInfo['supplier_name'] == $params['supplier_name']){
        }else{
            //开始判断名字是否重复
            $data = [
                'supplier_name'=>$params['supplier_name'],

            ];
            $this->checkNameIsRepetition('supplier',$data);
            //结束判断名字重复
        }

    	$paramRule = [
 			'supplier_id' => 'string',
    		'user_id'=>'string',
    	];
    	$this->paramCheckRule($paramRule,$params);

    	$supplierResult = $supplier->updateSupplierBySupplierId($params);
    	$this->outPut($supplierResult);
    
    
    }

    /**
     * 获取计价配置数据
     * 韩
     */
    public function getPricingConfigure(){

        $params = $this->input();

        $pricing_configure = new pc();

        if(isset($params['page'])){
            $page_size =  isset($params['page_size'])?$params['page_size']:Contents::PAGE_SIZE;
            $page = ($params['page']-1)*$page_size;
            $count = $pricing_configure->getPricingConfigure($params, true);
            $result = $pricing_configure->getPricingConfigure($params,false,'true',$page,$page_size);
            $data = [
                'count'=>$count,
                'list'=>$result,
                'page_count'=>ceil($count/$page_size)
            ];

            return $this->output($data);
        }

        $pricingConfigureResult = $pricing_configure->getPricingConfigure($params);
        $this->outPut($pricingConfigureResult);

    }

    /**
     * 获取计价配置和计算值数据
     * 韩
     */
    public function getPricingConfigureAndPrice(){

        $params = $this->input();

        $pricing_configure = new pc();
        $pricingConfigureResult = $pricing_configure->getPricingConfigure($params);

        $result_data = [];
        if(!empty($pricingConfigureResult[0]['interval_value'])){
            //计算总金额
            foreach($pricingConfigureResult as $key=>$val) {
                $result_data[$key]['pricing_configure_id'] = $val['pricing_configure_id'];
                $result_data[$key]['supplier_id'] = $val['supplier_id'];
                $result_data[$key]['departure_id'] = $val['departure_id'];
                $result_data[$key]['departure_name'] = $val['departure_name'];
                $result_data[$key]['arrival_id'] = $val['arrival_id'];
                $result_data[$key]['arrival_name'] = $val['arrival_name'];
                $result_data[$key]['billing_unit'] = $val['billing_unit'];
                $result_data[$key]['goods_id'] = $val['goods_id'];
                $result_data[$key]['goods_name'] = $val['goods_name'];
                $result_data[$key]['interval_start'] = $val['interval_start'];
                $result_data[$key]['interval_end'] = $val['interval_end'];
                $result_data[$key]['interval_value'] = $val['interval_value'];
                $result_data[$key]['unit_price'] = $val['unit_price'];
                $result_data[$key]['starting_price'] = $val['starting_price'];
                $result_data[$key]['picking_id'] = $val['picking_id'];
                $result_data[$key]['picking_rules'] = $val['picking_rules'];
                $result_data[$key]['picking_price'] = $val['picking_price'];
                $result_data[$key]['delivery_id'] = $val['delivery_id'];
                $result_data[$key]['delivery_rules'] = $val['delivery_rules'];
                $result_data[$key]['delivery_price'] = $val['delivery_price'];
                $result_data[$key]['unloading_id'] = $val['unloading_id'];
                $result_data[$key]['unloading_rules'] = $val['unloading_rules'];
                $result_data[$key]['unloading_price'] = $val['unloading_price'];
                $result_data[$key]['goods_type'] = $val['goods_type'];
                $result_data[$key]['orders_type'] = $val['orders_type'];
                $result_data[$key]['create_user_name'] = $val['create_user_name'];
                $result_data[$key]['update_user_name'] = $val['update_user_name'];
                $result_data[$key]['create_user_id'] = $val['create_user_id'];
                $result_data[$key]['update_user_id'] = $val['update_user_id'];
                $result_data[$key]['update_time'] = $val['update_time'];
                $result_data[$key]['create_time'] = $val['create_time'];
                $result_data[$key]['status'] = $val['status'];
                $result_data[$key]['total'] = number_format($params['interval'] * $val['unit_price'] + $val['starting_price'],3,'.', '');
            }
            $this->outPut($result_data);
        }else{
            $this->outPut($pricingConfigureResult);
        }

    }

    /**
     * 计价配置区间验证
     * 韩
     */
    public function IntervalCheck(){

        $params = $this->input();

        $pricing_configure = new pc();

        if($params['check_type_hidden']==1){
            //校验区间
            $result = $pricing_configure->checkInterval($params);
            $this->outPut($result);
        }else if($params['check_type_hidden']==2){
            //添加计价配置
            $result = $pricing_configure->addPricingConfigureOne($params);
            $this->outPut($result);
        }

    }

    /**
     * 添加承运商计价配置
     * 韩
     */
    public function addPricingConfigure(){

        $params = $this->input();

        unset($params["appKey"]);
        unset($params['appSecret']);
        unset($params['user_id']);

        $pricing_configure = new pc();
        //验证计价配置是否匹配
        $pricingConfigureResult = $pricing_configure->checkBatchInterval($params);

        if($pricingConfigureResult==1){ //满足
            //批量添加
            $addpricingConfigureResult = $pricing_configure->addPricingConfigure($params);
            $this->outPut($addpricingConfigureResult);

        }else if($pricingConfigureResult==2){ //不满足
            $this->outPut(2);
        }

    }

    /**
     * 添加承运商计价配置
     * 韩
     */
    public function addPricingConfigureProjectAll(){

        $params = $this->input();

        unset($params["appKey"]);
        unset($params['appSecret']);
        unset($params['user_id']);

        $pricing_configure = new pc();
        //批量添加
        $addpricingConfigureResult = $pricing_configure->addPricingConfigure($params);
        $this->outPut($addpricingConfigureResult);

    }

    /**
     * 添加项目计价配置
     * 韩
     */
    public function addPricingConfigureProject(){

        $params = $this->input();

        $pricing_configure = new pc();

        $addpricingConfigureResult = $pricing_configure->addPricingConfigureProject($params);
        $this->outPut($addpricingConfigureResult);

    }

    /**
     * 修改承运商计价配置
     * 韩
     */
    public function editPricingConfigure(){

        $params = $this->input();

        $pricing_configure = new pc();

        $addpricingConfigureResult = $pricing_configure->editPricingConfigure($params);
        $this->outPut($addpricingConfigureResult);
    }

    /**
     * 修改项目计价配置
     * 韩
     */
    public function editPricingConfigureProject(){

        $params = $this->input();

        $pricing_configure = new pc();

        $addpricingConfigureResult = $pricing_configure->editPricingConfigureProject($params);
        $this->outPut($addpricingConfigureResult);
    }

    /**
     * 承运商计价配置区间值批量修改AJAX
     * 韩
     */
    public function updatePricingConfigureBatchByPricingConfigureTypeId(){

        $params = $this->input();

        $pricing_configure = new pc();
        $pricingConfigureResult = $pricing_configure->updatePricingConfigureBatchByPricingConfigureTypeId($params);

        $this->outPut($pricingConfigureResult);
    }

    /**
     * 获取车辆类型
     * 胡
     */
    public function getVehicleType(){
    
    
    	$params = $this->input();
    
  
        if(isset($params['page'])){
            $page_size =  isset($params['page_size'])?$params['page_size']:Contents::PAGE_SIZE;
            $page = ($params['page']-1)*$page_size;
            $count = $this->_vehicleType->getVehicleType($params, true);
            $result = $this->_vehicleType->getVehicleType($params,false,'true',$page,$page_size);
            $data = [
                'count'=>$count,
                'list'=>$result,
                'page_count'=>ceil($count/$page_size)
            ];

            return $this->output($data);
        }
   
    	$vehicleTypeResult = $this->_vehicleType->getVehicleType($params);
    	
    	$this->outPut($vehicleTypeResult);
    
    }
    /**
     * 添加车辆类型
     * 胡
     */
    public function addVehicleType(){
    
    	$params = $this->input();
   
    	$paramRule = [
    			'vehicle_type_name' => 'string',
    
    			'status'=>'number',
    	];
    
    	$this->paramCheckRule($paramRule,$params);

    	//开始判断名字是否重复
    	$data = [
    			'vehicle_type_name'=>$params['vehicle_type_name'],
    
    	];
    	$this->checkNameIsRepetition('vehicle_type',$data);
    	//结束判断名字重复
    	
    
    
    	$vehicleTypeResult = $this->_vehicleType->addVehicleType($params);
    	
    	$this->outPut($vehicleTypeResult);
    }    
    /**
     * 修改车辆类型根据车辆类型ID
     * 胡
     */
    
    public function updateVehicleTypeByVehicleTypeId(){
    								  
    	$params = $this->input();

    	$vehicleTypeParams['vehicle_type_id'] = $params['vehicle_type_id'];
        $vehicleTypeResult = $this->_vehicleType->getVehicleType($vehicleTypeParams);
        

        if($vehicleTypeResult[0]['vehicle_type_name'] == $params['vehicle_type_name']){
        }else{

            //开始判断名字是否重复
            $data = [
                'vehicle_type_name'=>$params['vehicle_type_name'],
        
            ];
            $this->checkNameIsRepetition('vehicle_type',$data);
            //结束判断名字重复
        }

    	$paramRule = [
    		'vehicle_type_id' => 'string',
    		'user_id'=>'string',
    
    	];
    
    	$this->paramCheckRule($paramRule,$params);
		
    	$vehicleTypeResult = $this->_vehicleType->updateVehicleTypeByVehicleTypeId($params);
    	$this->outPut($vehicleTypeResult);
    
    
    }	 

    /**
     * 获取车辆
     * 胡
     */
    public function getVehicle(){
    
    
    	$params = $this->input();
    
    
    	if(isset($params['page'])){
    		$page_size =  isset($params['page_size'])?$params['page_size']:Contents::PAGE_SIZE;
    		$page = ($params['page']-1)*$page_size;
    		$count = $this->_vehicle->getVehicle($params, true);
    		$result = $this->_vehicle->getVehicle($params,false,'true',$page,$page_size);
    		$data = [
    				'count'=>$count,
    				'list'=>$result,
    				'page_count'=>ceil($count/$page_size)
    		];
    
    		return $this->output($data);
    	}
    	 
    	$vehicleResult = $this->_vehicle->getVehicle($params);
    	 
    	$this->outPut($vehicleResult);
    
    }
    /**
     * 添加车辆
     * 胡
     */
    public function addVehicle(){

    	$params = $this->input();
    	 
    	$paramRule = [
    			
    			'vehicle_type_id' => 'number',
    			'status'=>'number',
    	];
    
    	$this->paramCheckRule($paramRule,$params);

    	//开始判断名字是否重复
    	$data = [
    			'number_plate'=>$params['number_plate'],
    
    	];
    	$this->checkNameIsRepetition('vehicle',$data);
    	//结束判断名字重复
   
   
    	$vehicleTypeResult = $this->_vehicle->addVehicle($params);
    	 
    	$this->outPut($vehicleTypeResult);
    }

    /**
     * 修改车辆类型根据车辆类型ID
     * 胡
     */
    
    public function updateVehicleByVehicleId(){
    								  
    	$params = $this->input();

    	$vehicleParams['vehicle_id'] = $params['vehicle_id'];
        $vehicleResult = $this->_vehicle->getVehicle($vehicleParams);
        

        if($vehicleResult[0]['number_plate'] == $params['number_plate']){
        }else{

            //开始判断名字是否重复
            $data = [
                'number_plate'=>$params['number_plate'],
        
            ];
            $this->checkNameIsRepetition('vehicle',$data);
            //结束判断名字重复
        }

    	$paramRule = [
    		'vehicle_id' => 'string',
    		'number_plate'=>'string',
    
    	];
    
    	$this->paramCheckRule($paramRule,$params);
		
    	$vehicleResult = $this->_vehicle->updateVehicleByVehicleId($params);
    	$this->outPut($vehicleResult);
    
    
    }	 
    /**
     * 获取司机
     * 胡
     */
    public function getDriver(){
    
    
    	$params = $this->input();
    

    	if(isset($params['page'])){
    		$page_size =  isset($params['page_size'])?$params['page_size']:Contents::PAGE_SIZE;
    		$page = ($params['page']-1)*$page_size;
    		$count = $this->_driver->getDriver($params, true);
  
    		$result = $this->_driver->getDriver($params,false,'true',$page,$page_size);
   
    		$data = [
    				'count'=>$count,
    				'list'=>$result,
    				'page_count'=>ceil($count/$page_size)
    		];
    
    		return $this->output($data);
    	}
    
    	$driverResult =  $this->_driver->getDriver($params);
    
    	$this->outPut($driverResult);
    
    }    
    /**
     * 添加司机
     * 胡
     */
    public function addDriver(){
    
    	$params = $this->input();
    
    	$paramRule = [
    			'supplier_id' => 'number',

    			'status'=>'number',
    	];
    
    	$this->paramCheckRule($paramRule,$params);
    

    	 
    	$vehicleTypeResult = $this->_driver->addDriver($params);
    
    	$this->outPut($vehicleTypeResult);
    }    
    /**
     * 修改司机根据司机ID
     * 胡
     */
    
    public function updateDriverByDriverId(){
    
    	$params = $this->input();

    

    
    	$paramRule = [
    			'driver_id' => 'string',

    
    	];
    
    	$this->paramCheckRule($paramRule,$params);
    
    	$vehicleResult = $this->_driver->updateDriverByDriverId($params);
    	$this->outPut($vehicleResult);
    
    
    }    
    /**
     * 获取项目
     * 胡
     */
    public function getProject(){
    
    	$params = $this->input();
    
    	$project = new Project();

    	if(isset($params['page'])){
    		$page_size =  isset($params['page_size'])?$params['page_size']:Contents::PAGE_SIZE;
    		$page = ($params['page']-1)*$page_size;
    		$count = $this->_project->getProject($params, true);
    		$result = $this->_project->getProject($params,false,'true',$page,$page_size);
    		$data = [
    				'count'=>$count,
    				'list'=>$result,
    				'page_count'=>ceil($count/$page_size)
    		];
    
    		return $this->output($data);
    	}
    	$customerResult =  $this->_project->getProject($params);
		
    	$this->outPut($customerResult);
    
    }
    /**
     * 添加项目
     * 胡
     */
    public function addProject(){
    
    	$params = $this->input();
    
    	$paramRule = [
    			 
    			'customer_id'=>'string',
    			'project_name'=>'string',

    	];
    
    	$this->paramCheckRule($paramRule,$params);
    
    	//开始判断名字是否重复
    	$data = [
    			'project_name'=>$params['project_name'],
    			'zjm'=>$params['zjm'],
    			 
    	];
    	$this->checkNameIsRepetition('project',$data);
    	//结束判断名字重复
    	$params['project_number'] = Help::getNumber(1);
    	
  
    	$result = $this->_project->addProject($params);
    
    	$this->outPut($result);
    }   
    /**
     * 修改项目数据根据项目ID
     * 胡
     */
    public function updateProjectByProjectId(){
    
    	$params = $this->input();
    
    	$paramRule = [
    
    			'project_id'=>'string',
    	

    	];
    	$this->paramCheckRule($paramRule,$params);
    	 
    	//开始判断名字是否重复
    
    
    
    	$project_data['project_id'] = $params['project_id'];
    	$projectResult = $this->_project->getProject($project_data);
    
    	//重复性验证
//     	if($projectResult[0]['project_name'] == $params['project_id']){
//     	}else{
//     		//开始判断名字是否重复
//     		$data = [
//     				'project_name'=>$params['project_name'],
    				 
//     		];
//     		$this->checkNameIsRepetition('project',$data);
//     		//结束判断名字重复
//     	}
    
    	$result = $this->_project->updateProjectByProjectId($params);
    		
    	$this->outPut($result);
    }    
    
    /**
     * 获取收货
     * 胡
     */
    public function getAcceptGoods(){
    
    	$params = $this->input();
    
   
    
    	if(isset($params['page'])){
    		$page_size =  isset($params['page_size'])?$params['page_size']:Contents::PAGE_SIZE;
    		$page = ($params['page']-1)*$page_size;
    		$count = $this->_acceptGoods->getAcceptGoods($params, true);
    		$result = $this->_acceptGoods->getAcceptGoods($params,false,'true',$page,$page_size);
    		$data = [
    				'count'=>$count,
    				'list'=>$result,
    				'page_count'=>ceil($count/$page_size)
    		];
    
    		return $this->output($data);
    	}
    	$customerResult =  $this->_acceptGoods->getAcceptGoods($params);
    	$this->outPut($customerResult);
    
    }

    /**
     * 获取收货-导出
     * 韩
     */
    public function getAcceptGoodsExport(){

        $params = $this->input();
        $customerResult =  $this->_acceptGoods->getAcceptGoodsExport($params);
        $this->outPut($customerResult);

    }

    /**
     * 添加收货
     * 胡
     */
    public function addAcceptGoods(){
    
    	$params = $this->input();
    
    	$paramRule = ['project_id'=>'string'];
    
 
    	$this->paramCheckRule($paramRule,$params);
    

    
 
    	$result = $this->_acceptGoods->addAcceptGoods($params);
    
    	$this->outPut($result);
    }
    /**
     * 修改收货数据根据收货ID
     * 胡
     */
    public function updateAcceptGoodsByAcceptGoodsId(){
    
    	$params = $this->input();
    
    	$paramRule = [
    
    			'accept_goods_id'=>'string',
    			'project_id'=>'string',
    
    	];
    	$this->paramCheckRule($paramRule,$params);
    


    	$result = $this->_acceptGoods->updateAcceptGoodsByAcceptGoodsId($params);
    
    	$this->outPut($result);
    }    
    /**
     * 获取发货
     * 胡
     */
    public function getSendGoods(){
    
    	$params = $this->input();
    
    	 
    
    	if(isset($params['page'])){
    		$page_size =  isset($params['page_size'])?$params['page_size']:Contents::PAGE_SIZE;
    		$page = ($params['page']-1)*$page_size;
    		$count = $this->_sendGoods->getSendGoods($params, true);
    		$result = $this->_sendGoods->getSendGoods($params,false,'true',$page,$page_size);
    		$data = [
    				'count'=>$count,
    				'list'=>$result,
    				'page_count'=>ceil($count/$page_size)
    		];
    
    		return $this->output($data);
    	}
    	$customerResult =  $this->_sendGoods->getSendGoods($params);
    	$this->outPut($customerResult);
    
    }
    /**
     * 添加发货
     * 胡
     */
    public function addSendGoods(){
    
    	$params = $this->input();
    
    	$paramRule = [
    
    			'project_id'=>'string',
    
    
    	];
    
    
    	$this->paramCheckRule($paramRule,$params);
    
    
    
    
    	$result = $this->_sendGoods->addSendGoods($params);

    	$this->outPut($result);
    }
    /**
     * 修改发货数据根据发货ID
     * 胡
     */
    public function updateSendGoodsBySendGoodsId(){
    
    	$params = $this->input();
    
    	$paramRule = [
    
    			'send_goods_id'=>'string',
    			'project_id'=>'string',
    
    	];
    	$this->paramCheckRule($paramRule,$params);
    
    
    
    	$result = $this->_sendGoods->updateSendGoodsBySendGoodsId($params);
    
    	$this->outPut($result);
    }  
    
    /**
     * 获取货物
     * 胡
     */
    public function getGoods(){
    
    	$params = $this->input();
    
    
    
    	if(isset($params['page'])){
    		$page_size =  isset($params['page_size'])?$params['page_size']:Contents::PAGE_SIZE;
    		$page = ($params['page']-1)*$page_size;
    		$count = $this->_goods->getGoods($params, true);
    		$result = $this->_goods->getGoods($params,false,'true',$page,$page_size);
    		$data = [
    				'count'=>$count,
    				'list'=>$result,
    				'page_count'=>ceil($count/$page_size)
    		];
    
    		return $this->output($data);
    	}
    	$result =  $this->_goods->getGoods($params);
    	
    	$this->outPut($result);
    
    }
    /**
     * 添加货物
     * 胡
     */
    public function addGoods(){
    
    	$params = $this->input();
    
    	$paramRule = [
    
    			'project_id'=>'string',
    			'goods_name'=>'string',
    			'goods_packaging_type',
    
    
    	];
    
    
    	$this->paramCheckRule($paramRule,$params);
    
    
    
    
    	$result = $this->_goods->addGoods($params);
    
    	$this->outPut($result);
    }
    /**
     * 修改货物数据根据货物ID
     * 胡
     */
    public function updateGoodsByGoodsId(){
    
    	$params = $this->input();
    
    	$paramRule = [
    
    			'goods_id'=>'string',
    
    
    	];
    	$this->paramCheckRule($paramRule,$params);
    
    
    
    	$result = $this->_goods->updateGoodsByGoodsId($params);
    
    	$this->outPut($result);
    }    

    
    
    
    /**
     * 获取承运商线路
     * 胡
     */
    public function getSupplierLine(){
    
    	$params = $this->input();
    
    
    
    	if(isset($params['page'])){
    		$page_size =  isset($params['page_size'])?$params['page_size']:Contents::PAGE_SIZE;
    		$page = ($params['page']-1)*$page_size;
    		$count = $this->_supplierLine->getSupplierLine($params, true);
    		$result = $this->_supplierLine->getSupplierLine($params,false,'true',$page,$page_size);
    		$data = [
    				'count'=>$count,
    				'list'=>$result,
    				'page_count'=>ceil($count/$page_size)
    		];
    
    		return $this->output($data);
    	}
    	$result =  $this->_supplierLine->getSupplierLine($params);
    	 
    	$this->outPut($result);
    
    }
    /**
     * 添加承运商线路
     * 胡
     */
    public function addSupplierLine(){
    
    	$params = $this->input();
    
    	$paramRule = [
    
    			'supplier_uuid'=>'string',

    
    
    	];
    
    
    	$this->paramCheckRule($paramRule,$params);
    
    
    
    
    	$result = $this->_supplierLine->addSupplierLine($params);
    
    	$this->outPut($result);
    }
    /**
     * 修改承运商线路ID
     * 胡
     */
    public function updateSupplierLine(){
    
    	$params = $this->input();
    

    
    	$result = $this->_supplierLine->updateSupplierLine($params);
    
    	$this->outPut($result);
    }
    
    //获取城市
    public function getCity(){
    	$params = $this->input();
    	

    	$result =  $this->_city->getCity($params);
    	$this->outPut($result);
    	
    	
    }
	//获取市以及市下面的所有城市
	public function getCityInfo(){
		
		$params = $this->input();
	
		$result = Help::getCityInfo($params['city_id']);
		$this->outPut($result);		
		
		
	}
	public function addOrderOperating(){
        $params = $this->input();
        $opModel=new OrdersOperating();
        $result = $opModel->addInfo($params);
        $this->outPut($result);
    }

    public function getOrderOperating(){
        $params = $this->input();
        $opModel=new OrdersOperating();
        $result = $opModel->getInfo($params);
        $this->outPut($result,'',$opModel->getInfo($params,true));
    }

    //获取客服用承运商
    public function getCustomerSupplier(){
        $params = $this->input();

        //实例化模型
        $customerSupplier = new cs();

        if(isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : 20;
            $page = ($params['page'] - 1) * $page_size;
            $count = $customerSupplier->getCustomerSupplier($params, true);
            $result = $customerSupplier->getCustomerSupplier($params, false, 'true', $page, $page_size);
            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];
            return $this->output($data);
        }

        $dataResult = $customerSupplier->getCustomerSupplier($params);
        $this->outPut($dataResult);
    }

    public function getCustomerSendGoods(){
        $params = $this->input();

        //实例化模型
        $customerSendGoods = new csg();

        if(isset($params['page'])) {
            $page_size = isset($params['page_size']) ? $params['page_size'] : 20;
            $page = ($params['page'] - 1) * $page_size;
            $count = $customerSendGoods->getCustomerSendGoods($params, true);
            $result = $customerSendGoods->getCustomerSendGoods($params, false, 'true', $page, $page_size);
            $data = [
                'count' => $count,
                'list' => $result,
                'page_count' => ceil($count / $page_size)
            ];
            return $this->output($data);
        }

        $dataResult = $customerSendGoods->getCustomerSendGoods($params);
        $this->outPut($dataResult);
    }

    public function addCustomerSupplier(){
        $params = $this->input();

        $paramRule = [
            'supplier_name' => 'string',
            'status'=>'number',
        ];

        $this->paramCheckRule($paramRule,$params);


        //实例化模型
        $customerSupplier = new cs();

        $customersupplierResult = $customerSupplier->addCustomerSupplier($params);

        $this->outPut($customersupplierResult);
    }

    public function updateCustomerSupplierByCustomerSupplierId(){
        $params = $this->input();

        $paramRule = [
            'supplier_id' => 'string'
        ];
        $this->paramCheckRule($paramRule,$params);

        //实例化模型
        $customerSupplier = new cs();

        $supplierResult = $customerSupplier->updateCustomerSupplierByCustomerSupplierId($params);
        $this->outPut($supplierResult);
    }

    //项目计价配置
    public function getProjectPricingConfigure(){

        $params = $this->input();

        $project_pricing_configure = new ppc();

        if(isset($params['page'])){
            $page_size =  isset($params['page_size'])?$params['page_size']:Contents::PAGE_SIZE;
            $page = ($params['page']-1)*$page_size;
            $count = $project_pricing_configure->getProjectPricingConfigure($params, true);
            $result = $project_pricing_configure->getProjectPricingConfigure($params,false,'true',$page,$page_size);
            $data = [
                'count'=>$count,
                'list'=>$result,
                'page_count'=>ceil($count/$page_size)
            ];

            return $this->output($data);
        }

        $pricingConfigureResult = $project_pricing_configure->getProjectPricingConfigure($params);
        $this->outPut($pricingConfigureResult);

    }

    public function getProjectRule(){

        $params = $this->input();

        $project_rule = new pr();

        if(isset($params['page'])){
            $page_size =  isset($params['page_size'])?$params['page_size']:Contents::PAGE_SIZE;
            $page = ($params['page']-1)*$page_size;
            $count = $project_rule->getProjectRule($params, true);
            $result = $project_rule->getProjectRule($params,false,'true',$page,$page_size);
            $data = [
                'count'=>$count,
                'list'=>$result,
                'page_count'=>ceil($count/$page_size)
            ];

            return $this->output($data);
        }

        $projectRuleResult = $project_rule->getProjectRule($params);
        $this->outPut($projectRuleResult);

    }

    /**
     * 获取项目计价配置和计算值数据
     * 韩
     */
    public function getProjectPricingConfigureAndPrice(){

        $params = $this->input();

        $project_pricing_configure = new ppc();
        $ProjectpricingConfigureResult = $project_pricing_configure->getProjectPricingConfigureAndPrice($params);

        $this->outPut($ProjectpricingConfigureResult);

    }
}
