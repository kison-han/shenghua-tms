<?php
namespace app\index\model\Btob;
use think\Model;
use app\common\help\Help;
use think\config;
use think\Db;
class News extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'b2b_news';
    private $_languageList;
    public function initialize()
    {
    	$this->_languageList = config('systom_setting')['language_list'];
    	parent::initialize();
    
    }


    public function addNews($params){

    	$t = time();
    	$data['title'] = $params['title'];
        $data['cn_title'] = $params['cn_title'];
        $data['sub_title'] = $params['sub_title'];
        $data['date'] = $params['date'];
        $data['images'] = $params['images'];
        $data['content'] = $params['content'];
        $data['company_id'] = $params['choose_company_id'];
    	$data['create_time'] = $t;
    	$data['create_user_id'] = $params['now_user_id'];
    	$data['update_time'] = $t;
    	$data['update_user_id'] = $params['now_user_id'];
    	$data['status'] = $params['status'];

    	Db::startTrans();
    	try{
    		$pk_id = Db::name('b2b_news')->insertGetId($data);
		
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
    
    	return $result;
    }
    
    /**
     * 获取路线类型
     * 胡
     */
    public function getNews($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
        $data = "1=1";
        if(!empty($params['title'])){
            $data.= " and b2b_news.title like '%".$params['title']."%'";
        }
        if($params['status']<2 && is_numeric($params['status'])){
            $data.= " and b2b_news.status = ".$params['status'];
        }
        if(is_numeric($params['company_id'])){
            $data.= " and b2b_news.company_id = ".$params['company_id'];
        }
        if($is_count==true){
            $result = $this->table("b2b_news")->where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("b2b_news")
                    ->where($data)->limit($page, $page_size)
                    ->field(['b2b_news.*'])
                    ->select();

            }else{
                $result = $this->table("b2b_news")
                    ->where($data)
                    ->field(['b2b_news.*'])
                    ->select();
            }
        }
      
        return $result;
    }

    public function getOneNews($params){

        $data = ['news_id' => $params['news_id']];

        $result = $this->table("b2b_news")
            ->field(['b2b_news.*'])
            ->where($data)
            ->find();

        return $result;
    }

    public function updateNewsByNewsId($params){
    
    	$t = time();
    	
    	if(isset($params['title'])){
    		$data['title'] = $params['title'];
    	}

        if(isset($params['cn_title'])){
            $data['cn_title'] = $params['cn_title'];
        }

        if(isset($params['sub_title'])){
            $data['sub_title'] = $params['sub_title'];
        }

    	$data['status'] = $params['status'];

    	if(isset($params['date'])){
    		$data['date'] = $params['date'];
    	}

        if(isset($params['choose_company_id'])){
            $data['company_id'] = $params['choose_company_id'];
        }

//        if(isset($params['images'])){
//            $data['images'] = $params['images'];
//        }
        $data['images'] = isset($params['images']) ? $params['images']: '';
        if(isset($params['content'])){
            $data['content'] = $params['content'];
        }

    	$data['update_user_id'] = $params['user_id'];   
    	$data['update_time'] = $t;

    	Db::startTrans();
    	try{
    		Db::name('b2b_news')->where("news_id = ".$params['news_id'])->update($data);
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    
    	}
    	return $result;
    }


    public function getB2bNews($params){
        $data['status'] = 1;
        $data['date'] = ['elt',date('Y-m-d')];
        if($params['company_id']){
            $data['company_id'] = $params['company_id'];
        }

        if($params['news_id']){
            $data['news_id'] = $params['news_id'];
        }  
        $page = $params['page']?:1;  
        $page_size = ($page-1)*8;
        $result = $this->table("b2b_news")
                    ->where($data)->limit($page_size,8)
                    ->field(['b2b_news.*'])->order('date desc,news_id desc')
                    ->select();

        return $result;
    }

    public function getb2bNewsCount($params){
        $data['status'] = 1;
        $data['date'] = ['elt',date('Y-m-d')];
        $data['company_id'] = $params['company_id'];
        $result = $this->table("b2b_news")
                    ->where($data)
                    ->field(['b2b_news.*'])
                    ->count();

        return $result;
    }

}