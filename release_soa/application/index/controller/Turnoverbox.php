<?php

namespace app\index\controller;

use app\index\model\turnoverbox\Turnoverbox as tb;

class Turnoverbox extends Base
{
    /**
     * 获取周转箱信息
     */
    public function getTurnoverbox(){
        $params = $this->input();

        //实例化模型
        $turnoverbox = new tb();

        $turnoverboxResult = $turnoverbox->getTurnoverbox($params);
        $this->outPut($turnoverboxResult);
    }
}