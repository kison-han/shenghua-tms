<?php if (!defined('THINK_PATH')) exit(); /*a:5:{s:100:"/var/www/html/test_erp/public/../application/index/view/equipment/show_equipment_history_manage.html";i:1663296491;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:64:"/var/www/html/test_erp/application/index/view/public/header.html";i:1658978091;s:77:"/var/www/html/test_erp/application/index/view/public/left_equipment_menu.html";i:1658299220;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






    <title>智能设备管理</title>

</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
            <div class="layui-logo"></div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left">
        <li class="layui-nav-item">
            <a href="/turnoverbox/index">智能周转箱</a>
        </li>
        <li class="layui-nav-item">
            <a href="/">TMS</a>
        </li>
        <li class="layui-nav-item">
 
            <a href="/warehouse/index">WMS</a>
        </li>
        <li class="layui-nav-item">
            <a href="/wisdompark/index">智慧园区</a>

        </li>
        <li class="layui-nav-item">
            <a href="/Oa/index">OA</a>

        </li>   
         <li class="layui-nav-item">
            <a href="/Bms/index">BMS</a>

        </li>
        <li class="layui-nav-item">
            <a href="/equipment/equipmentManage">设备</a>

        </li>
    </ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item tips-system-message" style="cursor: pointer"><?php echo $language_tag['index_nav_system_message']; ?> <span class="system-message-quantity" style="color: red"> 1</span> </li>
      <li class="layui-nav-item">
        <a href="javascript:;">
          <!-- <img src="http://t.cn/RCzsdCq" class="layui-nav-img"> -->
          <?php echo \think\Session::get('user.nickname'); ?>
        </a>
        <dl class="layui-nav-child head-top">
          <dd><a href="/system/setUserInfo/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_basicDocument']; ?></a></dd>
          <dd><a href="/system/showChangePassword/user_id/<?php echo \think\Session::get('user.user_id'); ?>"><?php echo $language_tag['index_nav_resetPassword']; ?></a></dd>
        </dl>
      </li>
      <li class="layui-nav-item"><a href="/login/loginOut">退出登录</a></li>
    </ul>

    <div id="tips-system-message-js" style='display:none;'>
        <div  style="max-height:350px;width: 450px">
             <div style="color: #000;text-align: center;height: 10px;padding: 10px">你有 <font class="f-tips-system-message"></font> 条未读消息</div>
             <hr>
             <div class="tips-system-message-div" style="height:220px;overflow-y: auto">
           
                 <li style="color: #000; padding: 10px;cursor: pointer" data-href="" data-id="" class="aUrl" onclick="Aurl(this)" ></li>
                 <hr>
             
             </div>
            <div style="color: #000;text-align: center;height: 40px;padding-top: 15px"><a href="/reminderManagement/allInStationLetter"><?php echo $language_tag['index_nav_viewAll_messa']; ?></a></div>
        </div>
    </div>

    <!--  下列保存用户SESSION信息 -->
	
    <input type="hidden" id='now_url'  value="<?php echo $now_url; ?>" />
	<input type="hidden" id='after_url'  value="<?php echo $after_url; ?>" />
    <input type="hidden" id='user_company_id' value="<?php echo \think\Session::get('user.company_id'); ?>" />
	<!--  下列保存其他信息 -->
	<input type="hidden" id='http_referer' value="<?php echo $http_referer; ?>" />
	
	<script type='text/javascript'>
		$('.tips-system-message').on('click',function(){
			var html = $('#tips-system-message-js').html();
		    layer.tips(html, '.tips-system-message', {
		        tips: [3, '#fff'],
		        padding:'20',
		        tipsMore: false,
		        area: ['450px', 'auto'],
		        shade: [0.01, '#fff'],
		        shadeClose:true,
		        time:0
			
		    });
		
		});
		
		$(document).ready(function(){ 
			$(document).mousemove(function(e){ 
				
				if(e.pageX<=10){
					$('.layui-bg-black').show()
				}
				
				if(e.pageX>200){
					$('.layui-bg-black').hide()
				
				}
				if(e.pageY<=10){
					$('.layui-header').show()
				}
				
				if(e.pageY>40){
					$('.layui-header').hide()
				
				}			
			}); 
			
		}); 
	

	</script>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree" id="left-nav" lay-filter="test">

    <li <?php if($controller_name == 'device.smartbox'): ?> class="layui-nav-item layui-nav-itemed"<?php else: ?> class="layui-nav-item" <?php endif; ?>>
    <a class="" href="javascript:void(0)"><i class="iconfont img-task_fill"></i><em>设备管理</em></a>
    <dl class="layui-nav-child">
        <dd <?php if(in_array(($function_name), explode(',',"equipmentManage"))): ?> class="layui-this"<?php endif; ?>><a href="/equipment/equipmentManage">智能设备</a></dd>
    </dl>
    </li>
</ul>
        </div>
    </div>

    <div class="layui-body layui-body-bg">

        <!-- 内容主体区域 -->
        <div style="padding: 15px 15px 0px;">
            <div class="body-top">
                <div class='layui-form-item'>
                    <span class="layui-breadcrumb" lay-separator="-">
                        <a>首页</a>
                        <a>设备管理</a>
                        <a>智能设备</a>
                        <a><cite>历史定位</cite></a>
                    </span>
                </div>
                <div class='layui-block all-search-bg'>
                    <form class="layui-form" method='get' action='/Wms_Warehouse/showRegion'>
                        <div class="layui-collapse" lay-accordion="">
                            <div class="layui-colla-item">
                                <h2 class="layui-colla-title">搜索功能</h2>
                                <div class="layui-colla-content">
                                    <div class="layui-row">
                                        <div class="layui-col-md4">
                                            <div class="layui-form-item">
                                                <label class="layui-form-label">设备号-内:</label>
                                                <div class="layui-input-block">
                                                    <input type="text" id="" name="equipment_number_out" maxlength="300" autocomplete="off" value="<?php echo \think\Request::instance()->get('region_code'); ?>" placeholder="设备号-内" class="layui-input">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-col-md4">
                                            <div class="layui-form-item">
                                                <label class="layui-form-label">设备号-外:</label>
                                                <div class="layui-input-block">
                                                    <input type="text" id="" name="equipment_number_in" maxlength="300" autocomplete="off" value="<?php echo \think\Request::instance()->get('region_code'); ?>" placeholder="设备号-外" class="layui-input">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-col-md4">
                                            <div class="layui-form-item">
                                                <label class="layui-form-label">批次号:</label>
                                                <div class="layui-input-block">
                                                    <input type="text" id="" name="batch_number" maxlength="300" autocomplete="off" value="<?php echo \think\Request::instance()->get('contacts_phone'); ?>"  placeholder="批次号" class="layui-input">
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md4 layui-col-md-offset4">
                                            <div class='input-inline all-button-center pages-search-margin'>
                                                <button class="layui-btn nav-search" >搜索</button>
                                                <button type="reset" class="layui-btn layui-btn-primary swiper-right-form-reset" >重置</button>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>

            <div class="content-bg" >
                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                    <legend>历史定位</legend>
                </fieldset>
                <ul class="layui-timeline">
                    <?php if(is_array($equimentResult) || $equimentResult instanceof \think\Collection || $equimentResult instanceof \think\Paginator): if( count($equimentResult)==0 ) : echo "" ;else: foreach($equimentResult as $key=>$vo): ?>
                    <li class="layui-timeline-item">
                        <i class="layui-icon layui-timeline-axis"></i>
                        <div class="layui-timeline-content layui-text">
                            <h3 class="layui-timeline-title" style="color: #00a0e9">定位时间: <?php echo $vo['gps_time']; ?></h3>
                            <p>
                                设备名称: <?php echo $vo['device_name']; ?>
                                <br>设备图标: <?php echo $vo['icon']; ?>
                                <br>设备状态: <?php if($vo['imei_status'] == 0): ?>离线<?php elseif($vo['imei_status'] == 1): ?>在线<?php else: ?>无<?php endif; ?>
                                <br>经度:  <?php echo $vo['lng']; ?>&nbsp;&nbsp;&nbsp;&nbsp;纬度:  <?php echo $vo['lat']; ?>
                                <br>是否过期: <?php if($vo['expire_flag'] == 0): ?>过期<?php elseif($vo['expire_flag'] == 1): ?>未过期<?php else: ?>无<?php endif; ?>&nbsp;&nbsp;&nbsp;&nbsp;是否激活: <?php if($vo['activation_flag'] == 0): ?>未激活<?php elseif($vo['activation_flag'] == 1): ?>激活<?php else: ?>无<?php endif; ?>
                                <br>卫星定位: <?php echo $vo['pos_type']; ?>
                                <br><span style="color:green">心跳时间: <?php echo $vo['hb_time']; ?></span>
                                <br>Acc状态: <?php if($vo['acc_status'] == 0): ?>关闭<?php elseif($vo['acc_status'] == 1): ?>打开<?php else: ?>无<?php endif; ?>&nbsp;&nbsp;&nbsp;&nbsp;速度: <?php echo $vo['speed']; ?>
                                <br>设备电量（0-100）: <?php echo $vo['elect_quantity']; ?>&nbsp;&nbsp;&nbsp;&nbsp;外电电压（0-100）: <?php echo $vo['power_value']; ?>
                                <br>定位卫星数: <?php echo $vo['gps_num']; ?>&nbsp;&nbsp;&nbsp;&nbsp;角度0~360: <?php echo $vo['direction']; ?>&nbsp;&nbsp;&nbsp;&nbsp;里程统计: <?php echo $vo['mileage']; ?>
                            </p>
                        </div>
                    </li>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <a href='/Equipment/equipmentManage'><button type="button" class="layui-btn layui-btn-primary">返回</button></a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <script type="text/html" id="checkboxTpl">
        <!-- 这里的 checked 的状态只是演示 -->
        <input type="checkbox" name="{{d.equipment_id}}" value="{{d.status}}" title="启用" lay-filter="lockDemo" {{ d.status == 1 ? 'checked' : '' }}>
    </script>
    <script type="text/html" id="barDemo">
        <a class="layui-btn layui-btn-xs" lay-event="token">获取token</a>
        <a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="imei">获取imei</a>
        <a class="layui-btn layui-btn-xs layui-btn-warm" lay-event="location">获取最新定位</a>
        <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="history">历史定位</a>
        <a class="layui-btn layui-btn-xs" style="background: black" lay-event="edit">编辑</a>
    </script>
</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
<!--<script src='/static/javascript/wms_warehouse/warehouse.js'></script>-->
<script>
    layui.use(['layer','table','form'], function() {
        var table = layui.table //表格
            , layer = layui.layer //弹层
            , form = layui.form

        table.render({
            elem: '#test'
            ,url:'/equipment/getEquipmentAjax'
            ,toolbar: '#toolbarDemo' //开启头部工具栏，并为其绑定左侧模板
            ,defaultToolbar: ['filter', 'exports', 'print']
            ,title: '智能设备数据表'
            ,totalRow: true
            ,height: tableHeight
            ,cellMinWidth: 480
            ,cols: [[
                {type:'checkbox',name:'layTableCheckbox',fixed: 'left'},
                {field:'equipment_id',title:'设备序号', width:50},
                {field:'equipment_number_out', title:'设备号-内', width:180},
                {field:'equipment_number_in', title:'设备号-外', width:180},
				{field:'equipment_type_name', title:'设备类型', width:150},
                {field:'status', title:'是否启用', templet: '#checkboxTpl',width:100},
                {field:'token', title:'token', width:250},
                {field:'expiresin', title:'token有效期', width:150},
                {field:'time', title:'token申请时间', width:180},
                {field:'mc_type_use_scope', title:'使用范围', width:180, templet:function (d) {
                        if(d.mc_type_use_scope=='aotomobile'){
                            return "汽车";
                        }else if(d.mc_type_use_scope=='electromobile'){
                            return "电动车";
                        }else if(d.mc_type_use_scope=='personal'){
                            return "个人";
                        }else if(d.mc_type_use_scope=='pet'){
                            return "宠物";
                        }else if(d.mc_type_use_scope=='plane'){
                            return "飞机";
                        }else if(d.mc_type_use_scope=='others'){
                            return "其他";
                        }else if(d.mc_type_use_scope==null){
                            return "";
                        }
                    }
                },
                {field:'equip_ype', title:'设备类型', width:180, templet:function (d) {
                        if(d.equip_ype=='WIRED'){
                            return "有线";
                        }else if(d.equip_ype=='WIRELESS'){
                            return "无线";
                        }else if(d.equip_ype==null){
                            return "";
                        }
                    }
                },
                {field:'sim', title:'Sim卡号', width:180},
                {field:'activation_time', title:'激活时间', width:180},
                {field:'imei_status', title:'设备状态', width:180, templet:function (d) {
                        if(d.imei_status=='0'){
                            return "离线";
                        }else if(d.imei_status=='1'){
                            return "在线";
                        }else if(d.imei_status==null){
                            return "";
                        }
                    }
                },
                {field:'lng', title:'经度', width:180},
                {field:'lat', title:'纬度', width:180},
                {field:'remark', title:'备注', width:200},
				{field: 'create_time',title: '创建时间',width:150},
                {field:'create_user_name', title:'创建人', width:150},
                {field:'AAA', title:'操作', toolbar: '#barDemo', width:400,fixed: 'right'}
            ]]
            ,limit:50
            ,page: true
            ,response: {
                statusCode: 200 //重新规定成功的状态码为 200，table 组件默认为 0
            }

        });

        //头工具栏事件
        table.on('toolbar(test)', function(obj){
            var checkStatus = table.checkStatus(obj.config.id);
            switch(obj.event){
                case 'add':
                    //新增区域
                    window.location.href='/equipment/addEquipment';
            };
        });

        //监听行工具事件
        table.on('tool(test)', function(obj){
            var data = obj.data;
            var remark = data['remark'];
            var equipment_id = data['equipment_id'];
            var token = data['token'];

            //console.log(obj)
            if(obj.event === 'del'){
                layer.confirm('真的删除行么', function(index){
                    obj.del();
                    layer.close(index);
                });
            } else if(obj.event === 'edit'){
                //修改仓库
                window.location.href='/Wms_Warehouse/showRegionEdit?region_id='+ data.region_id;
            } else if(obj.event === 'token'){
                if(remark === "图强"){
                    $.ajax({
                        type: "POST",
                        url: '/Tuqiang/get_access_token',
                        data: {
                            "equipment_id":equipment_id
                        },
                        success: function(data){
                            // console.log(data);
                            if(data.code!=200){
                                layer.msg("获取失败!");
                                return false;
                            }else if(data.code==200) {
                                layer.msg('操作成功',{time:1,end : function(layero, index){
                                        layui.table.reload('test');
                                    }
                                });
                            }
                        },
                        error:function(XMLHttpRequest, textStatus, errorThrown){
                            layer.msg('验证失败')
                        }
                    });
                }else{
                    layer.alert("没用相应权限!");
                }
            }else if(obj.event === 'imei'){
                if(remark === "图强"){
                    $.ajax({
                        type: "POST",
                        url: '/Tuqiang/get_device_list',
                        data: {
                            "equipment_id":equipment_id,
                            "token":token
                        },
                        success: function(data){
                            // console.log(data);
                            if(data.code!=200){
                                layer.msg("获取失败!");
                                return false;
                            }else if(data.code==200) {
                                layer.msg('操作成功',{time:1,end : function(layero, index){
                                        layui.table.reload('test');
                                    }
                                });
                            }
                        },
                        error:function(XMLHttpRequest, textStatus, errorThrown){
                            layer.msg('验证失败')
                        }
                    });
                }else{
                    layer.alert("没用相应权限!");
                }
            }else if(obj.event === 'location'){
                if(remark === "图强"){
                    $.ajax({
                        type: "POST",
                        url: '/Tuqiang/get_device_location_list',
                        data: {
                            "equipment_id":equipment_id,
                            "token":token
                        },
                        success: function(data){
                            console.log(data);
                            if(data.code!=200){
                                layer.msg("获取失败!");
                                return false;
                            }else if(data.code==200) {
                                layer.msg('操作成功',{time:1,end : function(layero, index){
                                        layui.table.reload('test');
                                    }
                                });
                            }
                        },
                        error:function(XMLHttpRequest, textStatus, errorThrown){
                            layer.msg('验证失败')
                        }
                    });
                }else{
                    layer.alert("没用相应权限!");
                }
            }else if(obj.event === 'history'){
                if(remark === "图强"){
                    window.location.href='/equipment/showEquipmentHistoryManage?equipment_id='+equipment_id;
                }else{
                    layer.alert("没用相应权限!");
                }
            }

        });

        //监听行单击事件（双击事件为：rowDouble）
        table.on('rowDouble(test)', function(obj){
            var data = obj.data;

            layer.alert(JSON.stringify(data), {
                title: '当前行数据：'
            });

            //标注选中样式
            obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');
        });

        //监听锁定操作
        form.on('checkbox(lockDemo)', function(obj){
            // layer.tips(this.value + ' ' + this.name + '：'+ obj.elem.checked, obj.othis);

            var table_id_name = "equipment_id";
            var table_name = "equipment";
            if(this.value==1){
                var status = 0;
            }else{
                var status = 1;
            }
            var table_id = this.name;
            var status = status;

            $.ajax({
                type: "POST",
                url: '/index/changeStatus',
                data: {
                    table_id:table_id,
                    table_id_name:table_id_name,
                    table_name:table_name,
                    status:status,
                },
                success: function(data){
                    // console.log(data);return false;
                    if(data.code!=200){
                        layer.msg(data.msg);
                        return false;
                    }else if(data.code==200) {
                        layer.msg('操作成功',{time:1,end : function(layero, index){
                                table.reload('test');
                            }
                        });
                    }
                },
                error:function(XMLHttpRequest, textStatus, errorThrown){
                    layer.msg('验证失败')
                }
            });
        });

    });

</script>

</body>
</html>


