<?php

namespace app\index\model\bill;

use think\Db;
use think\Model;
use app\common\help\Help;
use app\index\model\bill\CustomerBillInfo;
use app\index\model\orders\Orders;
use app\index\model\orders\OrdersFinance;

class SupplierBill extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'supplier_bill';


    /**
     * 添加承运商账单
     * 韩
     */
    public function addSupplierBill($params){
    	$t = time();


    	$data['supplier_bill_name'] = $params['supplier_bill_name'];

    	 
		$data['abnormal_all_money'] = $params['abnormal_all_money'];
    	$data['supplier_bill_tax'] = $params['supplier_bill_tax']; 
		$data['supplier_bill_tax_type'] = $params['supplier_bill_tax_type'];
        $data['supplier_bill_type'] = $params['supplier_bill_type'];
    	$data['supplier_bill_date'] = $params['supplier_bill_date'];
    	$data['supplier_bill_dfk'] = $params['supplier_bill_dfk'];
		$data['supplier_uuid'] = $params['supplier_uuid'];
		$data['supplier_bill_money'] = $params['supplier_bill_money'];
    	$data['pay_all_money'] = $params['pay_all_money'];
     	$data['company_id'] = $params['choose_company_id'];   	
        if(!empty($params['supplier_bill_remark'])){
    		$data['supplier_bill_remark'] = $params['supplier_bill_remark'];
    	}
    	$data['create_time'] = $t;
    	$data['update_time'] = $t;
    	$data['create_user_id'] = $params['user_id'];
    	$data['update_user_id'] = $params['user_id'];
    	$data['status'] = 1;

    	Db::startTrans();
    	try{
    		//$data['finance_number']
    	
    		//开始计算合同号
			
    		//先计算账单日期的数量
    		$dayCountParams = " FROM_UNIXTIME(supplier_bill_date,'%Y%m') =".date('Ym',$data['supplier_bill_date']);
    		$dayCount = Db::name('supplier_bill')->where($dayCountParams)->count();
    		$data['supplier_bill_number'] = "CD".date('Ym',$data['supplier_bill_date']).str_pad(($dayCount+1),3,0,STR_PAD_LEFT);
    		$shipment_id_array = $params['shipment_id_array'];
    		for($i=0;$i<count($shipment_id_array);$i++){
    		 	//把每个shipment_uuid的财务状态变更为已对账，并插入到对账账单的明细表中
    		 	$shipmentParams['shipment_finance_status'] =3;
    		 	$shipmentWhere['shipment_id'] = $shipment_id_array[$i];
    		 	Db::name('shipment')->where($shipmentWhere)->update($shipmentParams);
    		 	
   
    		 	$supplierBillInfoData['supplier_bill_number'] = $data['supplier_bill_number'];
    		 	 
    		 	$supplierBillInfoData['shipment_id'] =$shipment_id_array[$i];
    		 	Db::name('supplier_bill_info')->insertGetId($supplierBillInfoData);
    		 }
  
    		 Db::name('supplier_bill')->insertGetId($data);
    		//开始插入成本
    		
    		$result = 1;
    		// 提交事务
    		Db::commit();
    	
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    	
    	}
    	 

        return $result;
    }

    /**
     * 修改承运商账单
     * 韩
     */
    public function updateSupplierBill($params){
    	$t = time();
    	if(isset($params['supplier_bill_name'])){
    		$data['supplier_bill_name'] = $params['supplier_bill_name'];
    	}
    	if(isset($params['supplier_bill_remark'])){
    		$data['supplier_bill_remark'] = $params['supplier_bill_remark'];
    	}  
    	if(isset($params['supplier_bill_tax'])){
    		$data['supplier_bill_tax'] = $params['supplier_bill_tax'];
    	}
    	if(isset($params['supplier_bill_date'])){
    		$data['supplier_bill_date'] = $params['supplier_bill_date'];
    	}  
		if(is_numeric($params['supplier_bill_tax_type'])){
			$data['supplier_bill_tax_type'] = $params['supplier_bill_tax_type'];   
		}
			
	
    	
		
		if(!empty($params['supplier_bill_uuid'])){
    		$where['supplier_bill_uuid'] = $params['supplier_bill_uuid'];
    	}
    	if(!empty($params['supplier_bill_number'])){
    		$where['supplier_bill_number'] = $params['supplier_bill_number'];
    	}
      	if(is_numeric($params['status'])){
    		$data['status'] = $params['status'];
    	}  
		if(is_numeric($params['choose_company_id'])){
			$data['company_id'] = $params['choose_company_id'];   
		}
		if(is_numeric($params['shipment_invoice_status'])){
			$data['shipment_invoice_status'] = $params['shipment_invoice_status']; 

			if($params['shipment_invoice_status']==4){
				$data['supplier_bill_pay_info'] = $params['supplier_bill_pay_info']; 
				
			}
		}		
    	if(is_numeric($params['supplier_bill_status'])){
    		$data['supplier_bill_status'] = $params['supplier_bill_status'];
    		if($params['supplier_bill_status']==3){ //代表驳回
    			$data['back_user_id'] = $params['user_id'];
    			$data['back_time'] = $t;
    			if(!empty($params['back_remark'])){
    				$data['back_remark'] = $params['back_remark'];
    			}
    		}
    	}
    	$data['update_time'] = $t;

    	$data['update_user_id'] = $params['user_id'];

	
    	Db::startTrans();
    	try{
			
    		//$data['finance_number']
    		 if(!empty($params['supplier_bill_number_array'])){ //批量提交
    	
    		 	for($i=0;$i<count($params['supplier_bill_number_array']);$i++){
    		 		$supplier_bill_status_params['supplier_bill_status'] = $params['supplier_bill_status'];
    		 		$supplier_bill_status_where['supplier_bill_number'] = $params['supplier_bill_number_array'][$i];
    		 		Db::name('supplier_bill')->where($supplier_bill_status_where)->update($supplier_bill_status_params);
    		 	}
    		 	
    		 }else{
    		 	Db::name('supplier_bill')->where($where)->update($data);
		
    		 	if($data['status'] ===0){ //假如删除了数据那么所有账单就要变成 未提交
						
    		 		$supplier_bill_info_result = Db::name('supplier_bill_info')->where($where)->select();
    		 		for($i=0;$i<count($supplier_bill_info_result);$i++){
    		 			$shipmentParams['shipment_finance_status'] = 2;
    		 			$shipmentWhere['shipment_id'] = $supplier_bill_info_result[$i]['shipment_id'];
    		 			Db::name('shipment')->where($shipmentWhere)->update($shipmentParams);
    		 		}
    		 	}   
    		 	if($params['supplier_bill_status']==2){
    		 		//代表运营通过确认
    		 		$supplier_bill_info_result = Db::name('supplier_bill_info')->where($where)->select();
					
					//变成财务确认
    		 		for($i=0;$i<count($supplier_bill_info_result);$i++){
    		 			$shipmentParams['shipment_finance_status'] = 4;
    		 			$shipmentWhere['shipment_id'] = $supplier_bill_info_result[$i]['shipment_id'];
						
						
    		 			Db::name('shipment')->where($shipmentWhere)->update($shipmentParams);
						
						//开始判断是否是短驳，如果是短驳则把短驳的数据变成已确认
						/*
						$shortResult = Db::name('shipment')->where("shipment_id=".$supplier_bill_info_result[$i]['shipment_id'])->find();
						
						if($shortResult['shipment_type']==3){
								
							$short_where['shipment_uuid'] = $shortResult['shipment_uuid'];
							$short_params['is_confirm'] = 2;
							Db::name('shipment_short_barge')->where($short_where)->update($short_params);
						
						}
						*/
    		 		}
					
					
					
    		 	}
    		 	if($params['supplier_bill_status']==3){
    		 		//驳回
    		 		$supplier_bill_info_result = Db::name('supplier_bill_info')->where($where)->select();
    		 		for($i=0;$i<count($supplier_bill_info_result);$i++){
    		 			$shipmentParams['shipment_finance_status'] = 2;
    		 			$shipmentWhere['shipment_id'] = $supplier_bill_info_result[$i]['shipment_id'];
    		 			Db::name('shipment')->where($shipmentWhere)->update($shipmentParams);
    		 		}
    		 	}		 	
    		 	if($params['supplier_bill_status']==4){
					//关账
    		 		$supplier_bill_info_result = Db::name('supplier_bill_info')->where($where)->select();
    		 		for($i=0;$i<count($supplier_bill_info_result);$i++){
    		 			$shipmentParams['shipment_finance_status'] = 5;
    		 			$shipmentWhere['shipment_id'] = $supplier_bill_info_result[$i]['shipment_id'];	
    		 			Db::name('shipment')->where($shipmentWhere)->update($shipmentParams);
    		 		}
    		 	}   		 	
    		 }


    		
    
    		$result = 1;
    		// 提交事务
    		Db::commit();
    		 
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    		 
    	}
	
    	return $result;
    }
    
    /**
     * 获取客户已对账单
     * 韩
     */
    /**
     * 获取发运
     * 胡
     */
    public function getSupplierBill($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    
    
    	$data = "1=1 ";

    	if(is_numeric($params['status'])){
    		$data.= " and supplier_bill.status= ".$params['status'];
    	}   
    	if(is_numeric($params['supplier_bill_status'])){
    		$data.= " and supplier_bill.supplier_bill_status= ".$params['supplier_bill_status'];
    	}
    	if(!empty($params['supplier_bill_number'])){
    		$data.= " and supplier_bill.supplier_bill_number like '%".$params['supplier_bill_number']."%'";
    	}
    	if(!empty($params['supplier_bill_name'])){
    		$data.= " and supplier_bill.supplier_bill_name like '%".$params['supplier_bill_name']."%'";
    	}
    	if(!empty($params['supplier_bill_date'])){
    		$data.= " and FROM_UNIXTIME(supplier_bill.supplier_bill_date,'%Y-%m')  ='".$params['supplier_bill_date']."'";
    	}		
     	if(is_numeric($params['choose_company_id'])){
    		$data.= " and supplier_bill.company_id= ".$params['choose_company_id'];
    	} 
     	if(is_numeric($params['shipment_invoice_status'])){
    		$data.= " and supplier_bill.shipment_invoice_status= ".$params['shipment_invoice_status'];
    	} 	
	
        if($is_count==true){
            $result = $this->table("supplier_bill")->alias("supplier_bill")->

            where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("supplier_bill")->alias('supplier_bill')->
				join('company','company.company_id = supplier_bill.company_id')->
                where($data)->limit($page, $page_size)->order('supplier_bill.create_time desc')->
                field([
                		'supplier_bill.*','company.company_name',
						"(select supplier_name  from supplier where supplier.supplier_uuid = supplier_bill.supplier_uuid)"=> 'supplier_name',
						"(select nickname  from user where user.user_id = supplier_bill.back_user_id)"=> 'back_user_name',
	                	"(select nickname  from user where user.user_id = supplier_bill.create_user_id)"=> 'create_user_name',
	                    "(select nickname  from user where user.user_id = supplier_bill.update_user_id)"=> 'update_user_name'
		                ])->select();
            }else{
                $result = $this->table("supplier_bill")->alias('supplier_bill')->
				join('company','company.company_id = supplier_bill.company_id')->
                where($data)->order('create_time desc')->
                field([
                		'supplier_bill.*','company.company_name',
						"(select supplier_name  from supplier where supplier.supplier_uuid = supplier_bill.supplier_uuid)"=> 'supplier_name',
						"(select nickname  from user where user.user_id = supplier_bill.back_user_id)"=> 'back_user_name',
	                	"(select nickname  from user where user.user_id = supplier_bill.create_user_id)"=> 'create_user_name',
	                    "(select nickname  from user where user.user_id = supplier_bill.update_user_id)"=> 'update_user_name'
                    	])->select();
            }
        }
    	 
 
    
    	return $result;
    
    }
	public function getSupplierBillMoney($params){
		
		$data = "1=1 ";
		
		if(is_numeric($params['supplier_bill_status'])){
			
			$data.=' and supplier_bill_status='.$params['supplier_bill_status'];
		}
		
		if(is_numeric($params['start_update_time'])){
			
			$data.=' and update_time>='.$params['start_update_time'];
			
		}
		if(is_numeric($params['end_update_time'])){
			
			$data.=' and update<='.$params['end_update_time'];
			
		}		
		
		$result = $this->table("supplier_bill")->
      
                        where($data)->field("sum(pay_all_money) as money,sum(abnormal_all_money) as abnormal")->select();
		return $result;
	}
}