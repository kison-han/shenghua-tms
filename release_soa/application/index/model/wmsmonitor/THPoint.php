<?php

namespace app\index\model\wmsmonitor;

use think\Db;
use think\Model;
use app\common\help\Help;

class THPoint extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'wms_th_point';

    /**
     * 获取温湿度点位
     * 韩
     */
    public function getTHPoint($params,$is_count=false,$is_page=false,$page=null,$page_size=20){

        $data = "1=1 ";

        if(!empty($params['wms_th_point_uuid'])){ //uuid
            $data.=' and wms_th_point.wms_th_point_uuid ="'.$params['wms_th_point_uuid'].'"';
        }

        if(!empty($params['th_point_name'])){ //点位名称
            $data.=' and wms_th_point.th_point_name ="'.$params['th_point_name'].'"';
        }

        if(!empty($params['position_name'])){ //货区名称
            $data.=' and wms_position.position_name ="'.$params['position_name'].'"';
        }

        if(!empty($params['remark'])){ //备注
            $data.=' and wms_th_point.remark ="'.$params['remark'].'"';
        }

        if(is_numeric($params['status'])){ //状态
            $data.=' and wms_th_point.status ="'.$params['status'].'"';
        }

        if($is_count==true){
            $result = $this->table("wms_th_point")->where($data)->count();
        }else{
            if($is_page == true){
                $result = $this->table("wms_th_point")->
                join("wms_position",'wms_position.position_id= wms_th_point.position_id')->
                where($data)->limit($page, $page_size)->order('wms_th_point.create_time desc')->
                field(['wms_th_point.wms_th_point_id','wms_th_point.wms_th_point_uuid','wms_th_point.position_id',
                    'wms_position.position_name'=>'position_name',
                    'wms_th_point.th_point_name','wms_th_point.remark','wms_th_point.temperature','wms_th_point.humidity',
                    "(select nickname from user where user.user_id = wms_th_point.create_user_id)"=> 'create_user_name',
                    "(select nickname from user where user.user_id = wms_th_point.update_user_id)"=> 'update_user_name',
                    'wms_th_point.status',
                    'from_unixtime(wms_th_point.create_time)'=> 'create_time',
                    'from_unixtime(wms_th_point.update_time)'=> 'update_time'
                ])->select();
            }else{
                $result = $this->table("wms_th_point")->
                join("wms_position",'wms_position.position_id= wms_th_point.position_id')->
                where($data)->order('wms_th_point.create_time desc')->
                field(['wms_th_point.wms_th_point_id','wms_th_point.wms_th_point_uuid','wms_th_point.position_id',
                    'wms_position.position_name'=>'position_name',
                    'wms_th_point.th_point_name','wms_th_point.remark','wms_th_point.temperature','wms_th_point.humidity',
                    "(select nickname from user where user.user_id = wms_th_point.create_user_id)"=> 'create_user_name',
                    "(select nickname from user where user.user_id = wms_th_point.update_user_id)"=> 'update_user_name',
                    'wms_th_point.status',
                    'from_unixtime(wms_th_point.create_time)'=> 'create_time',
                    'from_unixtime(wms_th_point.update_time)'=> 'update_time'
                ])->select();
            }
        }
        return $result;
    }

    /**
     * 添加温湿度点位
     * 韩
     */
    public function addTHPoint($params){
        $t = time();

        $data['position_id'] = $params['position_id'];
        $data['th_point_name'] = $params['th_point_name'];
        $data['remark'] = $params['remark'];
        $data['wms_th_point_uuid'] = Help::getUuid(); //获取uuid
        $data['create_time'] = $t;
        $data['create_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        $data['update_user_id'] = $params['user_id'];
        $data['status'] = $params['status'];

        Db::startTrans();
        try{
            $this->table("wms_th_point")->insertGetId($data);
            $result = 1;
            // 提交事务
            Db::commit();
        }catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return $result;
    }

    /**
     * 修改温湿度点位
     * 韩
     */
    public function updateTHPointByTHPointId($params){
        $t = time();

        $data['wms_th_point_id'] = $params['wms_th_point_id'];
        $data['position_id'] = $params['position_id'];
        $data['th_point_name'] = $params['th_point_name'];
        $data['remark'] = $params['remark'];
//        $data['create_time'] = $t;
//        $data['create_user_id'] = $params['user_id'];
        $data['update_time'] = $t;
        $data['update_user_id'] = $params['user_id'];
        $data['status'] = $params['status'];

        Db::startTrans();
        try {
            Db::name('wms_th_point')->where('wms_th_point_uuid = "'.$params['wms_th_point_uuid'].'"')->update($data);

            $result = 1;
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return $result;
    }
}