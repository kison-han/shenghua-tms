<?php

/**
 * 助手函数类，主要就是解决一些公用的函数所用
 */

namespace app\common\help;

use phpmailer\phpmailer;
use think\Cache;
use think\Db;
use app\index\model\source\City;

use think\Model;
/**
 * Description of Helper
 *
 * @author 胡
 */
class Help {

    public static function cityIdToNname($city_id){
        //	$cityResult = session('city');
        $cityString=file_get_contents('map.json');
        $cityResult=json_decode($cityString,true);
        if(!$cityString) {
            $cityData = Db::name('city')->select();
            file_put_contents('map.json',json_encode($cityData,256));
            $cityResult=$cityData;
        }

        $cityinfo = '';

        foreach ($cityResult as $k=>$v)
        {
            if($v['city_id']==$city_id)
                $cityinfo=$v;
        }

        return $cityinfo;
    }

    public static function http($method,$url,$data=null)
    {

        //1初始化，创建一个新cURL资源

        $ch = curl_init();
        if($method == 'post'){
            curl_setopt($ch, CURLOPT_POST, 1);
            // Accept:application/json; charset=utf-8;Content-Type:application/x-www-form-urlencoded;charset=utf-8
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept:application/json'));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    // https请求 不验证证书和hosts
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);//http_build_query

        }
        //2设置URL和相应的选项
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_HEADER, 0);

        //3抓取URL并把它传递给浏览器

        $output = curl_exec($ch);

        return $output;
        //4关闭cURL资源，并且释放系统资源

        curl_close($ch);
        exit();

    }

    public static function toTree($array, $pid =0, $level = 0, $id = 'route_type_id')
    {
        $array = json_decode(json_encode($array),true);
//
//        $a = array();
//        foreach($items as $value){
//            $a[$value['route_type_id']] = $value;
//        }
//        //第二部 遍历数据 生成树状结构
//        $tree = array();
//        foreach($a as $key => $value){
//            if(isset($a[$key['pid']])){
//                $a[$key['pid']]['son'][] = &$a[$key];
//            }else{
//                $tree[] = &$a[$key];
//            }
//        }
//        return $tree;

        //声明静态数组,避免递归调用时,多次声明导致数组覆盖
        static $list = [];
        foreach ($array as $key => $value){
            //第一次遍历,找到父节点为根节点的节点 也就是pid=0的节点
            if ($value['pid'] == $pid){
                //父节点为根节点的节点,级别为0，也就是第一级
                $value['level'] = $level;
                //把数组放到list中
                $list[] = $value;
                //把这个节点从数组中移除,减少后续递归消耗
                unset($array[$key]);
                //开始递归,查找父ID为该节点ID的节点,级别则为原级别+1
                Help::toTree($array, $value[$id], $level+1, $id);
            }
        }
        return $list;

    }



    /**
     * 模型数据转数组
     * @param $data
     * @return mixed
     */
    public static function modelDataToArr($data){
        return json_decode(json_encode($data),true);
    }
    
    /**
     * 发送邮箱
     * @param type $data 邮箱队列数据 包含邮箱地址 内容
     */
    public static function  sendEmail($address,$title,$message) {
    	Vendor('phpmailer.phpmailer');
    	$mail = new \PHPMailer(); //实例化
		$mail->IsSMTP();
		// 设置邮件的字符编码，若不指定，则为'UTF-8'
		$mail->CharSet='UTF-8';
		// 添加收件人地址，可以多次使用来添加多个收件人
		$mail->AddAddress($address);
		// 设置邮件正文
		$mail->Body=$message;
		// 设置邮件头的From字段。
		$mail->From='736605639@qq.com';
		// 设置发件人名字
		$mail->FromName='胡伊敏测试';
		// 设置邮件标题
		$mail->Subject=$title;
		// 设置SMTP服务器。
		$mail->Host='smtp.qq.com';
		// 设置为“需要验证”
		$mail->SMTPAuth=true;
		// 设置用户名和密码。
		$mail->Username='736605639@qq.com';
		$mail->Password='zeozqefaigicbeab';
		// 发送邮件。
		return($mail->Send());
    	
    }
    
    
    //获取编号
    public static function getNumber($key,$type=1){//type =1只返回编号 type=2 返回完整编号
    	//
    	switch ($key){
    		case 1://订单
    			$name = 'O';
    			break;
    		case 2://订单
    				$name = 'T';
    			break;
    		break;
    	}
    	if($type==1){
    		return $name;
    		exit();
    	}

  
    	//首先获取缓存
    	$result = Cache::get($name);
    	//JSON字符串转成数组
    	$result =json_decode($result,true);

    	//如果为空则
    	if(empty($result)){
    		$result = [];
    	}
    	$product_number = str_pad(mt_rand(1, 999999), 6, '0', STR_PAD_LEFT);
    	//开始判断是否存在数组
    	while(in_array($product_number,$result)){
    		
    		$product_number =str_pad(mt_rand(1, 999999), 6, '0', STR_PAD_LEFT);
    	}
    	//把新获得的 产品订单号插入数组
    	$result[] = $product_number;
    	
    	//再把数组转换成JSON
    	$result = json_encode($result);
    	
    	//再把数据插入缓存 每天的凌晨会重置
    	Cache::set($name,$result,date('Y-m-d 00:00:00',strtotime('+1 day')));
    	return $name.date('ymd').$product_number;
    	
    }

    public static function getCityInfo($city_id){
	
		if(!empty(Cache::get('CityInfo'.$city_id))){
		
			$cityArray = Cache::get('CityInfo'.$city_id);
		}else{
			
			$city = new City();
			$cityParams['city_id'] = $city_id;
		
			$cityResult = $city->getCity($cityParams);
			$cityProvinceParams['province_id'] = $cityResult[0]['province_id'];
		
			$cityResult = $city->getCity($cityProvinceParams);
			$result = Help::modelDataToArr($cityResult);	
			$cityArray = [];
			for($i=0;$i<count($result);$i++){
				if(is_numeric(array_search($result[$i]['parent_id'], array_column($cityArray, 'city_id'))) || $result[$i]['city_id'] == $city_id ){			
					if( $result[$i]['status'] ==1 ){
						array_push($cityArray,$result[$i]);
					}
				
				}
			}			
			Cache::set('CityInfo'.$city_id,$cityArray,strtotime('+10 day'));
		}
		


		return $cityArray;
    }
	
	/**
	 * 二维数组根据某字段进行排序
	 * sort 1升序 2倒序
	 */
	public function arraySort($array,$string,$sort=1){
		if($sort ==1){
			$sort_sting='SORT_ASC';
		}else{
			$sort_sting='SORT_DESC';
		}
		
		return array_multisort($array,$sort_sting,array_column($array,$string));
	}


    /**
     * 替换数组中的值
     * Created by PhpStorm.
     * User: yyy
     * Date: 2019/4/23
     * Time: 13:59
     * @param array $arr  数组
     * @param mixed $from 要替换的值
     * @param mixed $to  替换后的值
     * @return array $arr 替换后的数组
     *
     */
    public static function replaceNull(&$arr, $from, $to)
    {
        foreach ($arr as $key => $val) {
            if (is_array($val)) {
                self::replaceNull($arr[$key], $from, $to);
            }
            else
            {
                $val === $from && $arr[$key] = $to;
            }
        }
        return $arr;
    }

    public static function sendOperationsEmail($params){
    	
    	$mail_SMTPSecure = config('send_mail')['mail_SMTPSecure'];
    	$mail_port = config('send_mail')['mail_port'];

     
        Vendor('phpmailer.phpmailer');
         
        $mail = new \PHPMailer(); //实例化
        $mail->IsSMTP();
        // 设置邮件的字符编码，若不指定，则为'UTF-8'
        $mail->CharSet='UTF-8';
        // 添加收件人地址，可以多次使用来添加多个收件人
        $mail->AddAddress($params['to_email']);
        // 设置邮件正文
        $mail->Body=$params['content'];
        // 设置邮件头的From字段。
        $mail->From= $params['from_email'] ? $params['from_email'] : 'system@nexusholidays.com';
        // 设置发件人名字
        $mail->FromName='联谊假期';
        // 设置邮件标题
        $mail->Subject=$params['subject'];
        //设置附件
        foreach($params['email_attachment'] as $v){
            $mail->addAttachment($v);
        }
         
        // 设置SMTP服务器。
        $mail->Host='secure.emailsrvr.com';  
        // $mail->Host='smtp.gmail.com';  
       // $mail->Host='smtp.office365.com';
 
        // 设置为“需要验证”
        $mail->SMTPAuth=true; 
        	
        $mail->SMTPSecure ='ssl';
        $mail->Port = 465;
         
        
        // 设置用户名和密码。
        $mail->Username='system@nexusholidays.com';
        $mail->Password='NeXus_20!9';

        // $mail->Username='nexus.jiye@gmail.com';
        // $mail->Password='Aa123456!@#';

        // $mail->Username='nexus.us@outlook.com';
        // $mail->Password='Aa123456!@#';

        // 发送邮件。
        if($mail->Send()){
            return 1;
        }else{
            return $mail->ErrorInfo;
        }

    }

    /**
     * 获取uuid
     */
    public static function getUuid()
    {
        return date("YmdHis").md5(uniqid(md5(microtime(true)),true));
    }
	/**
	 * 获取6位随机数包含大小写数字
	 */
    public static function  getRandomNumber($length){
    	
    	//生成一个包含 大写英文字母, 小写英文字母, 数字 的数组    	
    	$shuzi = array_merge(range(0, 9));
    	$xiaoxie = array_merge(range('a', 'z'));
    	$daxie = array_merge(range('A', 'Z'));
    	$str = $xiaoxie[rand(0,25)].$xiaoxie[rand(0,25)].$daxie[rand(0,25)].$daxie[rand(0,25)].$shuzi[rand(0,9)].$shuzi[rand(0,9)];    	
    	
    	return $str;
    	
    	
    }
	/**
	 * 获取邮箱文案
	 */
    public static function getEmailContent($type){
    	if($type == 1){
			$content= '您的验证码为/you code is';		
    	}
    	
    	return $content;
    	
    }
	
    
    //转换时区
    /**
     * 
     * @param unknown $date_time 请传2019-11-10 22:10:10 格式不要传时间戳
     * @param string $format 
     * @param string $to 转换后的时区格式 Europe/Rome
     * @param string $from 转换前的时区格式 Asia/Shanghai
     */
	public static function changeTimeZone($date_time, $format = 'Y-m-d H:i:s', $from,$to='Asia/Shanghai') {
    	$datetime = new \DateTime($date_time, new \DateTimeZone($from));
    	$datetime->setTimezone(new \DateTimeZone($to));
    	return $datetime->format($format);
	}
	
	/**
	 * 计算生日
	 */
	public static function howOld($birth) {
		$nowY = date('Y');
		$nowM = date('m');
		$nowD = date('d');
		$birthY = date('Y',$birth);
		$birthM = date('m',$birth);
		$birthD = date('d',$birth);
	
		//首先确定年龄
		$age = $nowY-$birthY;
	
		if($age ==0){
			return 0;
		}else{
			//再确定当前月日是否大于生日月日
			if(($nowM.$nowD) <($birthM.$birthD)){
				$age--;
			}
		}
	
		return $age;
	}
	
	//获取城市数据通过ID
	
	public static function getCityByCityId($city_id){
		$city = new City();
		$cityParams['city_id'] = $city_id;
		if(!empty(Cache::get('cityResult'.$city_id))){
			$cityResult = Cache::get('cityResult'.$city_id);
		}else{
			$cityResult = $city->getCity($cityParams);
			Cache::set('cityResult'.$city_id,$cityResult,strtotime('+10 day'));
		}
		
			return $cityResult;
		
	}
	
	//获取省市区三种数据
	public static function getCityLevelInfo($level){
		$city = new City();
		if($level == 1){
			if(!empty(Cache::get('provinceCity'))){
	
				$cityResult = Cache::get('provinceCity');
			}else{
				$cityParams['level'] = 1;
				$cityResult = $city->getCity($cityParams);
				Cache::set('provinceCity',$cityResult,strtotime('+1 day'));
			}
		}elseif($level ==2){
			if(!empty(Cache::get('cityCity'))){
			
				$cityResult = Cache::get('cityCity');
			}else{
				$cityParams['level'] = 1;
				$cityResult = $city->getCity($cityParams);
				Cache::set('cityCity',$cityResult,strtotime('+1 day'));
			}			
		}else{
			if(!empty(Cache::get('areaCity'))){
		
				$cityResult = Cache::get('areaCity');
			}else{
				$cityParams['level'] = 3;
				$cityResult = $city->getCity($cityParams);
				Cache::set('areaCity',$cityResult,strtotime('+1 day'));
			}			
		}


		return $cityResult;
	}
	
	
}

