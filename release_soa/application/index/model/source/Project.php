<?php

namespace app\index\model\source;
use think\Model;
use app\common\help\Help;
use app\index\service\PublicService;
use think\config;
use think\Db;
class Project extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'project';
    private $_public_service;

    public function initialize()
    {
    	$this->_public_service = new PublicService();
    	parent::initialize();
    
    }

    /**
     * 添加项目
     * 胡
     */
    public function addProject($params){
    	$t = time();

    	$data['project_name'] = $params['project_name'];


    	$data['customer_id'] = $params['customer_id'];
    	   	

    	if(!empty($params['zjm'])){
    		$data['zjm'] = $params['zjm'];
    		 
    	}

    	if(!empty($params['project_leader'])){
    		$data['project_leader'] = $params['project_leader'];
    	
    	}
    	if(!empty($params['cellphone'])){
    		$data['cellphone'] = $params['cellphone'];
    		 
    	}
        if(!empty($params['tax_rate'])){
            $data['tax_rate'] = $params['tax_rate'];

        }
        if(is_numeric($params['tax_type'])){
            $data['tax_type'] = $params['tax_type'];

        }


    	$data['create_time'] = $t;  	
    	$data['create_user_id'] = $params['user_id'];
    	$data['update_time'] = $t;
    	$data['update_user_id'] = $params['user_id'];
    	$data['status'] = $params['status'];

    
    	Db::startTrans();
    	try{
    		$pk_id = Db::name('project')->insertGetId($data);
    		$this->_public_service->setNumber('project', 'project_id', $pk_id, 'project_number', $params['project_number'], $pk_id);
    		
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
    
    	return $result;
    }
    
    /**
     * 获取项目
     * 胡
     */
    public function getProject($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    	$data = "1=1 ";

    	if(isset($params['keyword']))
        {
            $data.= " and project.project_name like  '%".$params['keyword']."%'";
        }
		if(empty($params['user_id'])){
			$params['user_id'] =1;
		}
    	if(is_numeric($params['status'])){
    		$data.= " and project.status = ".$params['status'];
    	}
    	if(is_numeric($params['project_id'])){
    		$data.= " and project.project_id = ".$params['project_id'];
    	}
    	if(!empty($params['project_name'])){
    		$data.= ' and project.project_name like "%'.$params['project_name'].'%"';
    	}
        if(!empty($params['project_names'])){
            $data.=' and project.project_name ="'.$params['project_names'].'"';
        }
    	if(!empty($params['zjm'])){
    		$data.= ' and project.zjm like "%'.$params['zjm'].'%"';
    	}   	
    	if(!empty($params['cellphone'])){
    		$data.= ' and project.cellphone like "%'.$params['cellphone'].'%"';
    	}  if(!empty($params['multi_project_id'])){
    		$data.= ' and project.project_id in ('.$params['multi_project_id'].')';
    	}
        if(is_numeric($params['tax_rate'])){
            $data.= " and project.tax_rate = ".$params['tax_rate'];
        }

        if($is_count==true){
            $result = $this->table("project")->join("customer",'customer.customer_id = project.customer_id')->
          where($data)->count();
        }else {
            if ($is_page == true) {
                $result = $this->table("project")->join("customer",'customer.customer_id = project.customer_id')->
          
                where($data)->limit($page, $page_size)->order('create_time desc')->
                field(['default_send_goods_id','project.project_id','project.project_name','project.customer_id','project.tax_rate',
                		'project.project_leader','project.cellphone','project.project_number','project.zjm','project.default_send_goods_id',
                		'customer.company_name','project.tax_type',
						"(select count(*) from orders where orders.project_id = project.project_id and orders.create_user_id=".$params['user_id'].")"=>'orders_count',
                    "(select nickname  from user where user.user_id = project.create_user_id)"=> 'create_user_name',
                    "(select nickname  from user where user.user_id = project.update_user_id)"=> 'update_user_name',
                    'project.update_time', 'project.create_time', "project.status",
                ])->select();
               
               
            }else{
                $result = $this->table("project")->alias('project')->join("customer",'customer.customer_id = project.customer_id')->
                where($data)->order('create_time desc')->
                field(['default_send_goods_id','project.project_id','project.project_name','project.customer_id','project.tax_rate',
                		'project.project_leader','project.cellphone','project.project_number','project.zjm','project.default_send_goods_id',
                		'customer.company_name','project.tax_type',
					"(select count(*) from orders where orders.project_id = project.project_id and orders.create_user_id=".$params['user_id'].")"=>'orders_count',
                    "(select nickname  from user where user.user_id = project.create_user_id)"=> 'create_user_name',
                    "(select nickname  from user where user.user_id = project.update_user_id)"=> 'update_user_name',
                    'project.update_time', 'project.create_time', "project.status",
                ])->select();
            }

			
        }
		
        return $result;
    
    }

    /**
     * 修改客户
     */
    public function updateProjectByProjectId($params){
    
    	$t = time();
    	
    	if(!empty($params['project_name'])){
    		$data['project_name'] = $params['project_name'];
    	
    	}


    		 
    	if(isset($params['customer_id'])){
    		$data['customer_id'] = $params['customer_id'];
    	} 
        if(isset($params['zjm'])){
            $data['zjm'] = $params['zjm'];
        }
        if(isset($params['cellphone'])){
            $data['cellphone'] = $params['cellphone'];
        }
        if(isset($params['project_leader'])){
        	$data['project_leader'] = $params['project_leader'];
        }
        if(isset($params['default_send_goods_id'])){
            $data['default_send_goods_id'] = $params['default_send_goods_id'];
        }
        if(isset($params['tax_rate'])){
            $data['tax_rate'] = $params['tax_rate'];
        }
    	if(is_numeric($params['status'])){
    		$data['status'] = $params['status'];
    		 
    	}

        if(is_numeric($params['tax_type'])){
            $data['tax_type'] = $params['tax_type'];

        }

    	$data['update_user_id'] = $params['user_id'];   
    	$data['update_time'] = $t;

    

    	Db::startTrans();
    	try{
    		Db::name('project')->where("project_id = ".$params['project_id'])->update($data);
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    
    	}
		
    	return $result;
    }

}