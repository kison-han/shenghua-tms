<?php


namespace app\index\model\dispatch;
use app\common\help\Contents;
use app\common\help\Help;
use app\index\model\despatch\Despatch;
use app\index\model\source\Supplier;
use app\index\model\transport\Transport;
use app\index\model\system\User;
use think\Db;
use think\Model;

class Dispatch extends Model
{
    protected $table = 'dispatch';
    protected $autoWriteTimestamp = true;
//新增字段
    protected $append = [
        'status_text', 'create_time_text', 'update_time_text', 'accept_province', 'accept_city', 'accept_area',
        'send_province', 'send_city', 'send_area', 'dispatch_type_text', 'create_user', 'update_user', 'supplier'
    ];

    protected function base($query)
    {
        $query->where('status',1);
    }
    public function getDispatchById($params){
        $dispatch=$this->where('dispatch_id',$params['dispatch_id'])->select();
        $dispatch_goods=DispathGoods::where('dispatch_id',$params['dispatch_id'])->select();
        $dispatch[0]['goods']=$dispatch_goods;
        $data=Transport::where('transport_number','like',$dispatch[0]['transport_number'])->find();
        $data['dispatch_info']=$dispatch;
        return ['list' => $data];
    }

    //处理用户id转用户名
    public function getCreateUserAttr($value, $data)
    {
        $userid = $data['create_user_id'];
        $userinfo = User::get($userid);
        return $userinfo['nickname'];
    }

    //处理用户id转用户名
    public function getUpdateUserAttr($value, $data)
    {
        $userid = $data['update_user_id'];
        $userinfo = User::get($userid);
        return $userinfo['nickname'];
    }

    //处理供应商id转用户名
    public function getSupplierAttr($value, $data)
    {
        $supplierInfo = Supplier::get($data['supplier_id']);
        return $supplierInfo['supplier_name'];
    }

    //处理用户id转用户名
    public function getDispatchTypeTextAttr($value, $data)
    {
        $status = [1 => '中转', 2 => '提货', 3 => '干线', 4 => '配送', 5 => '整车'];
        return $status[$data['dispatch_type']];
    }

    //获取状态
    public function getStatusTextAttr($value, $data)
    {
        $status = [-1 => '删除', 0 => '禁用', 1 => '正常', 2 => '待审核'];
        return $status[$data['status']];
    }

    //时间戳转时间
    public function getUpdateTimeTextAttr($value, $data)
    {
        $time = $data['update_time'];
        return date('Y-m-d H:i:s', $time);
    }

    //时间戳转时间
    public function getCreateTimeTextAttr($value, $data)
    {
        $time = $data['create_time'];
        return date('Y-m-d H:i:s', $time);
    }

    //转换省市区
    public function getAcceptProvinceAttr($value, $data)
    {
        return Help::cityIdToNname($data['accept_province_id'])['city_name'];
    }

    //转换省市区
    public function getAcceptCityAttr($value, $data)
    {
        return Help::cityIdToNname($data['accept_city_id'])['city_name'];
    }

    //转换省市区
    public function getAcceptAreaAttr($value, $data)
    {
        return Help::cityIdToNname($data['accept_area_id'])['city_name'];
    }

    //转换省市区
    public function getSendProvinceAttr($value, $data)
    {
        return Help::cityIdToNname($data['accept_province_id'])['city_name'];
    }

    //转换省市区
    public function getSendCityAttr($value, $data)
    {
        return Help::cityIdToNname($data['accept_city_id'])['city_name'];
    }

    //转换省市区
    public function getSendAreaAttr($value, $data)
    {
        return Help::cityIdToNname($data['accept_area_id'])['city_name'];
    }

    public function upload($params){
        unset($params['goods']);
        $this->allowField(true)->update($params);


        Db::startTrans();
        try {


            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }
        return true;
    }



    //添加
    public function add($params)
    {
        unset($params['goods']);
        $id = $this->allowField(true)->insertGetId($params);
        Db::startTrans();
        try {


            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            $result = $e->getMessage();
            // 回滚事务
            Db::rollback();
        }

        return $id;
    }

    public function getDispatchHasPage($params){

    }


//获取数据
    public function getDispatch($params,$openCount=false)
    {
        $where = [];
        $where['status'] = 1;

        if($openCount) {
            $page = isset($params['page']) ? $params['page'] : 1;
            $pageSize = isset($params['$pageSize']) ? $params['$pageSize'] :Contents::PAGE_SIZE;
            $data['count'] = $this->where($where)->count();
            $data['page_count']=ceil($data['count']/$pageSize);
            $tinfo=$this->where($where)->page($page,$pageSize)->order('dispatch_id','desc')->select();
            foreach ($tinfo as $k=>$v)
            {
                $tinfo[$k]['cost']=  round(DispatchFee::where('dispatch_id',$v['dispatch_id'])->sum('true_money'),3);
               // $tinfo[$k]['cost']=DispatchFee::where('dispatch_id',$v['dispatch_id'])->select();
                $tinfo[$k]['order_number']=Transport::where('transport_number',$tinfo[$k]['transport_number'])->column("order_number")[0];
            }

            $data['list']=$tinfo;

             return $data;
        }
        else
           return $this->where($where)->order('dispatch_id','desc')->select();

    }


}