<?php

/**
 * 助手函数类，主要就是解决一些公用的函数所用
 */

namespace app\common\help;



/**
 * Description of Helper
 *
 * @author 胡
 */
class Contents {
	
	/**
	 * 分页显示数量
	 */
	CONST PAGE_SIZE = 20;
//允许修改状态的字段
	public static  function statusArray(){
	    return ['status','replacement_prive_type','order_status','print','verify_status','follow_level','receipt_status'];
    }


	//商品货物包装
	public static function goodsPackagingType(){
		$data =[
				'0'=>['id'=>1,'goods_packaging_type_name'=>'纸箱'],
				'1'=>['id'=>2,'goods_packaging_type_name'=>'木箱'],
				'2'=>['id'=>3,'goods_packaging_type_name'=>'桶'],
				'3'=>['id'=>4,'goods_packaging_type_name'=>'袋装'],
	
		];
	
		return $data;

	}

	public static function transporType(){

        $status = [1=>'整车',2=>'零担'];
        return $status;
    }

	//商品状态名称
	public static function orderStatus(){
		$data =[
				'0'=>['id'=>1,'statusName'=>'客服提交'],
				'1'=>['id'=>2,'statusName'=>'调度'],
				'2'=>['id'=>3,'statusName'=>'提货'],
				'3'=>['id'=>4,'statusName'=>'装车'],
				'4'=>['id'=>5,'statusName'=>'发运'],
				'5'=>['id'=>6,'statusName'=>'到达'],
				'6'=>['id'=>7,'statusName'=>'签收'],
				'7'=>['id'=>8,'statusName'=>'回单上传']
	
		];
	
		return $data;
	
	}

}
