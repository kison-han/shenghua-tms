<?php
namespace app\index\service;
use app\index\model\finance\FinanceApprove;
use app\index\model\finance\Receivable;
use app\index\model\finance\ReceivableInfo;
use think\Console;
use think\Exception;
use app\common\help\Help;
use think\Model;
class ReceivableInfoService{
	private $_finance_approve;
	public function __construct(){
		$this->_finance_approve = new FinanceApprove();
	}

    /**
     * 添加or修改ReceivableInfo（目前只有添加逻辑）
     * Created by PhpStorm.
     * User: Administrator
     * Date: 2019/4/26
     * Time: 9:11
     * @param mixed
     */
    public function editReceivableInfo($data)
    {
        $receivable_info_model = new ReceivableInfo();

        $data['payment_currency_id'] && $save_data['payment_currency_id'] = $data['payment_currency_id'];       //币种id
        $data['payment_type'] && $save_data['payment_type'] = $data['payment_type'];                      //支付类型
        $data['payment_stage'] && $save_data['payment_stage'] = $data['payment_stage'];                   //支付方式
        $data['sn_number'] && $save_data['sn_number'] = $data['sn_number'];                               //sn_number
        $data['supplier_name'] && $save_data['supplier_name'] = $data['supplier_name'];       //供应商
        $data['exg_rate_gain'] && $save_data['exg_rate_gain'] = $data['exg_rate_gain'];       //汇兑损溢
        $data['payment_time'] && $save_data['payment_time'] = $data['payment_time'];         //付款时间

        $data['account_number'] && $save_data['account_number'] = $data['account_number'];     //account_number
        $data['remark'] && $save_data['remark'] = $data['remark'];                              //备注
        $data['receivable_info_type'] && $save_data['receivable_info_type'] = $data['receivable_info_type'];
        $data['supplier_id'] && $save_data['supplier_id'] = $data['supplier_id'];       //供应商id
        //付款编号
        if ($save_data['receivable_info_type'] == 1)
        {
            //财务手填
            $save_data['payment_number'] = $data['payment_number'];
        }
        else
        {
            //销售 自动
            $save_data['payment_number'] = Help::getNumber(201,2);
        }


        $save_data['create_user_id'] = $data['now_user_id'];         //创建人


        $receivable_number_arr = explode(',', $data['receivable_number']); //应收编号数组

        if($data['receivable_info_id'])
        {
            //修改
        }
        else
        {
            //添加
            try
            {
                $save_data['status'] = 1;     //status
                $receivable_model = new Receivable();
                $receivable_info_model->startTrans();
                foreach ($receivable_number_arr as $value)
                {
                    //如果金额为0 结束循环
                    if($data['payment_money'] == 0){
                        break;
                    }

                    //首先 查询应收
                    $receivable_params = [
                        'receivable_number' => $value
                    ];
                    $receivable_result = $receivable_model->getReceivable($receivable_params);

                    //开始算剩余的应收
                    $miss_receivable =$receivable_result[0]['receivable_money'] - $receivable_result[0]['true_receipt'];

                    if($miss_receivable<=0){
                        continue;
                    }else{
                        //一条应收编号
                        $save_data['receivable_number'] = $value;
                        $save_data['payment_money'] = $data['payment_money'];
                        if($save_data['payment_money'] > $miss_receivable){
                            $save_data['payment_money'] = $miss_receivable;
                        }
						//添加到财务审批
																																												
                        $save_data['finance_type'] = 1;
                        
                        $save_data['receivable_info_type'] = 2;
                        //走审批
                        $save_data['now_user_id'] = $data['now_user_id'];
                        $save_data['user_company_id'] = $data['user_company_id'];
                        $save_data['order_number'] = $data['order_number'];
                        $this->_finance_approve->addFinanceApprove($save_data);
                        //$receivable_info_model->addOrUpdateOne($save_data);

                        $data['payment_money'] -= $save_data['payment_money'];

                    }
                }

                $receivable_info_model->commit();
                return true;
            }
            catch (Exception $e)
            {
                $receivable_info_model->rollback();
                return $e->getMessage();
            }
        }
    }

    /**
     * 添加or修改ReceivableInfo（目前只有添加逻辑） 郭丹娜版本审批默认不开启
     * Created by PhpStorm.
     * User: Administrator
     * Date: 2019/4/26
     * Time: 9:11
     * @param mixed
     */
    public function editReceivableInfoNa($data)
    {
    	
       $receivable_info_model = new ReceivableInfo();

        $data['payment_currency_id'] && $save_data['payment_currency_id'] = $data['payment_currency_id'];       //币种id
        $data['payment_type'] && $save_data['payment_type'] = $data['payment_type'];                      //支付类型
        $data['payment_stage'] && $save_data['payment_stage'] = $data['payment_stage'];                   //支付方式
                               //sn_number
        $data['supplier_name'] && $save_data['supplier_name'] = $data['supplier_name'];       //供应商
        $data['supplier_id'] && $save_data['supplier_id'] = $data['supplier_id'];       //供应商id
        $data['exg_rate_gain'] && $save_data['exg_rate_gain'] = $data['exg_rate_gain'];       //汇兑损溢
        $data['payment_time'] && $save_data['voucher_time'] = $data['payment_time'];         //付款时间

        $data['account_number'] && $save_data['account_number'] = $data['account_number'];     //account_number
        $data['remark'] && $save_data['remark'] = $data['remark'];                              //备注
        $data['receivable_info_type'] && $save_data['receivable_info_type'] = $data['receivable_info_type'];
        $save_data['attachment'] = $data['attachment'];
        //付款编号
  
            //财务手填
            $save_data['receivable_voucher'] = $data['payment_number'];



        $receivable_info = new ReceivableInfo();

        
        if($data['receivable_info_id'])
        {
            $receivable_info = $receivable_info_model->where(['receivable_info_id' => $data['receivable_info_id']])->find();
            //修改单条info
            try
            {
                $save_data['status'] = $data['status'];     //status
                $receivable_model = new Receivable();

                $receivable_info_model->startTrans();

                //如果金额为0 结束循环
                if($data['payment_money'] == 0){
                    return true;
                }

                //首先 查询应收
                $receivable_params = [
                    'receivable_number' => $receivable_info['receivable_number']
                ];
                $receivable_result = $receivable_model->getReceivable($receivable_params);

                //开始算剩余的应收
                $miss_receivable =$receivable_result[0]['receivable_money'] - $receivable_result[0]['true_receipt'];
                if($miss_receivable<=0){
                    return true;
                }else{

                    $save_data['payment_money'] = $data['payment_money'];
                    $save_data['update_user_id'] = $data['now_user_id'];
                    $save_data['update_time'] = time();
                    $receivable_info_model->where(['receivable_info_id' => $data['receivable_info_id']])->update($save_data);
                }

                $receivable_info_model->commit();
                return true;

            }
            catch (Exception $e)
            {
                $receivable_info_model->rollback();

                return $e->getMessage();
            }
        }
        else
        {
            //添加
            try
            {
                $save_data['create_user_id'] = $data['now_user_id'];         //创建人
                $save_data['create_time'] = time();
                $receivable_number_arr = explode(',', $data['receivable_number']); //应收编号数组
                //通过应收编号查询下面有几条实收
                $receivable_info_params =[
                    'receivable_number'=>$data['receivable_number']
                ];

                $receivable_info_result = $receivable_info->getReceivableInfoList($receivable_info_params);
                $save_data['sn_number']  = count($receivable_info_result)+1;
                $save_data['status'] = 1;     //status
                $receivable_model = new Receivable();
               
                $receivable_info_model->startTrans();
                foreach ($receivable_number_arr as $value)
                {
                    //如果金额为0 结束循环
                    if($data['payment_money'] == 0){
                        break;
                    }

                    //首先 查询应收
                    $receivable_params = [
                        'receivable_number' => $value
                    ];
                    $receivable_result = $receivable_model->getReceivable($receivable_params);
					
                    
                    
                    
                    
                    //开始算剩余的应收
                    $miss_receivable =$receivable_result[0]['receivable_money'] - $receivable_result[0]['true_receipt'];

                    if($miss_receivable<=0){
                        continue;
                    }else{
                        //一条应收编号
                        $save_data['receivable_number'] = $value;

						//添加到财务审批
																																												
                       // $save_data['finance_type'] = 1;
                        
                        $save_data['receivable_info_type'] = 2;
                        $save_data['payment_money'] = $data['payment_money'];
                        $save_data['create_user_id'] = $data['now_user_id'];
                       // $save_data['user_company_id'] = $data['user_company_id'];
                        //$save_data['order_number'] = $data['order_number'];
                        //$this->_finance_approve->addFinanceApprove($save_data);
                        
                       
                        $receivable_info_model->addOrUpdateOne($save_data);

                        //$data['payment_money'] -= $save_data['payment_money'];

                    }
                }

                $receivable_info_model->commit();
                return true;
            }
            catch (Exception $e)
            {
                $receivable_info_model->rollback();
                return $e->getMessage();
            }
        }
    }

    /**
     * 添加or修改ReceivableInfo（目前只有添加逻辑） 郭丹娜版本审批默认不开启
     * Created by PhpStorm.
     * User: Administrator
     * Date: 2019/4/26
     * Time: 9:11
     * @param mixed
     */
    public function editCopeInfoNa($data)
    {
    	 
    	$receivable_info_model = new ReceivableInfo();
    
    	$data['payment_currency_id'] && $save_data['payment_currency_id'] = $data['payment_currency_id'];       //币种id
    	$data['payment_type'] && $save_data['payment_type'] = $data['payment_type'];                      //支付类型
    	$data['payment_stage'] && $save_data['payment_stage'] = $data['payment_stage'];                   //支付方式
    	//sn_number
    	$data['supplier_name'] && $save_data['supplier_name'] = $data['supplier_name'];       //供应商
    	$data['supplier_id'] && $save_data['supplier_id'] = $data['supplier_id'];       //供应商id
    	$data['exg_rate_gain'] && $save_data['exg_rate_gain'] = $data['exg_rate_gain'];       //汇兑损溢
    	$data['payment_time'] && $save_data['voucher_time'] = $data['payment_time'];         //付款时间
    
    	$data['account_number'] && $save_data['account_number'] = $data['account_number'];     //account_number
    	$data['remark'] && $save_data['remark'] = $data['remark'];                              //备注
    	$data['receivable_info_type'] && $save_data['receivable_info_type'] = $data['receivable_info_type'];
    	$save_data['attachment'] = $data['attachment'];
    	//付款编号
    
    	//财务手填
    	$save_data['receivable_voucher'] = $data['payment_number'];
    
    
    
    	$receivable_info = new ReceivableInfo();
    
    
    	if($data['receivable_info_id'])
    	{
    		$receivable_info = $receivable_info_model->where(['receivable_info_id' => $data['receivable_info_id']])->find();
    		//修改单条info
    		try
    		{
    			$save_data['status'] = $data['status'];     //status
    			$receivable_model = new Receivable();
    
    			$receivable_info_model->startTrans();
    
    			//如果金额为0 结束循环
    			if($data['payment_money'] == 0){
    				return true;
    			}
    
    			//首先 查询应收
    			$receivable_params = [
    					'receivable_number' => $receivable_info['receivable_number']
    			];
    			$receivable_result = $receivable_model->getReceivable($receivable_params);
    
    			//开始算剩余的应收
    			$miss_receivable =$receivable_result[0]['receivable_money'] - $receivable_result[0]['true_receipt'];
    			if($miss_receivable<=0){
    				return true;
    			}else{
    
    				$save_data['payment_money'] = $data['payment_money'];
    				$save_data['update_user_id'] = $data['now_user_id'];
    				$save_data['update_time'] = time();
    				$receivable_info_model->where(['receivable_info_id' => $data['receivable_info_id']])->update($save_data);
    			}
    
    			$receivable_info_model->commit();
    			return true;
    
    		}
    		catch (Exception $e)
    		{
    			$receivable_info_model->rollback();
    
    			return $e->getMessage();
    		}
    	}
    	else
    	{
    		//添加
    		try
    		{
    			$save_data['create_user_id'] = $data['now_user_id'];         //创建人
    			$save_data['create_time'] = time();
    			$receivable_number_arr = explode(',', $data['receivable_number']); //应收编号数组
    			//通过应收编号查询下面有几条实收
    			$receivable_info_params =[
    					'receivable_number'=>$data['receivable_number']
    			];
    
    			$receivable_info_result = $receivable_info->getReceivableInfoList($receivable_info_params);
    			$save_data['sn_number']  = count($receivable_info_result)+1;
    			$save_data['status'] = 1;     //status
    			$receivable_model = new Receivable();
    			 
    			$receivable_info_model->startTrans();
    			foreach ($receivable_number_arr as $value)
    			{
    				//如果金额为0 结束循环
    				if($data['payment_money'] == 0){
    					break;
    				}
    
    				//首先 查询应收
    				$receivable_params = [
    						'receivable_number' => $value
    				];
    				$receivable_result = $receivable_model->getReceivable($receivable_params);
    					
    
    
    
    
    				//开始算剩余的应收
    				$miss_receivable =$receivable_result[0]['receivable_money'] - $receivable_result[0]['true_receipt'];
    
    				if($miss_receivable<=0){
    					continue;
    				}else{
    					//一条应收编号
    					$save_data['receivable_number'] = $value;
    
    					//添加到财务审批
    
    					// $save_data['finance_type'] = 1;
    
    					$save_data['receivable_info_type'] = 2;
    					$save_data['payment_money'] = $data['payment_money'];
    					$save_data['create_user_id'] = $data['now_user_id'];
    					// $save_data['user_company_id'] = $data['user_company_id'];
    					//$save_data['order_number'] = $data['order_number'];
    					//$this->_finance_approve->addFinanceApprove($save_data);
    
    					 
    					$receivable_info_model->addOrUpdateOne($save_data);
    
    					//$data['payment_money'] -= $save_data['payment_money'];
    
    				}
    			}
    
    			$receivable_info_model->commit();
    			return true;
    		}
    		catch (Exception $e)
    		{
    			$receivable_info_model->rollback();
    			return $e->getMessage();
    		}
    	}
    }
    public function delReceivableInfo($receivable_info_id)
    {
        $receivable_info_model = new ReceivableInfo();
        $receivable_info_model->where(['receivable_info_id' => $receivable_info_id])->update(['status'=>0]);
        return true;
    }
	
}