<?php

namespace app\index\controller;
use app\common\help\Contents;
use app\common\help\Help;
use think\Request;
use think\Session;
use think\Cache;
class Bmscost extends Base
{


    //TMS成本已经开票
    public function tmscost(){
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);
    	return $this->fetch('tmscost');
    }
	//已收票
    public function getInvoice(){
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);
    	return $this->fetch('get_invoice');
    }	
	//已上传付款凭证
    public function postPay(){
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);
    	return $this->fetch('post_pay');
    }	
	//已确认付款凭证
    public function agreePay(){
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);
    	return $this->fetch('agree_pay');
    }
	//已确认付款凭证
    public function payed(){
		//获取分公司

        if (!empty(Cache::get('company_cache'))) {


            $companyResult = Cache::get('company_cache');
        } else {
            $comapnyResult = $this->callSoaErp('post', '/system/getCompany', []);
            $comapnyResult = $comapnyResult['data'];
            Cache::set('company_cache', $companyResult, strtotime('+10 day'));

        }

        $this->assign('comapnyResult', $comapnyResult);
    	return $this->fetch('payed');
    }		
    //TMS成本
    public function getTmscostAjax(){
    	$params = Request::instance()->param();
    	$data = [
			'page' => $params['page'],
			'page_size' => $params['limit'],
			'status'=>1
	
    	];
		if(is_numeric($params['choose_company_id'])){
            $data['choose_company_id'] =$params['choose_company_id'];
        }	
    	if(!empty($params['supplier_bill_name'])){
    		$data['supplier_bill_name'] = $params['supplier_bill_name'];
    	}
    	if(!empty($params['supplier_bill_status'])){
    		$data['supplier_bill_status'] = $params['supplier_bill_status'];
    	}
    	if(!empty($params['supplier_bill_date'])){
    		$data['supplier_bill_date'] = $params['supplier_bill_date'];
    	}
    	if(!empty($params['supplier_bill_number'])){
    		$data['supplier_bill_number'] = $params['supplier_bill_number'];
    	}		
		if(is_numeric($params['shipment_invoice_status'])){
            $data['shipment_invoice_status'] =$params['shipment_invoice_status'];
        }	
		
    	$result = $this->callSoaErp('post', '/bill/getSupplierBill',$data);
		
	
    	for($i=0;$i<count($result['data']['list']);$i++){
    		$result['data']['list'][$i]['supplier_bill_status_name']=Contents::baseConfig()['bill']['supplier_bill_status'][$result['data']['list'][$i]['supplier_bill_status']];
    		if(is_numeric($result['data']['list'][$i]['back_time'])){
				$result['data']['list'][$i]['back_time'] = date('Y-m-d H:i:s',$result['data']['list'][$i]['back_time']);
			}else{
				$result['data']['list'][$i]['back_time'] = '';
			}
			$result['data']['list'][$i]['supplier_bill_date'] = date('Y-m-d',$result['data']['list'][$i]['supplier_bill_date']);
			
			if($result['data']['list'][$i]['supplier_bill_tax_type']==1){
				$result['data']['list'][$i]['supplier_bill_tax_type_name']='否';
			}else{
				$result['data']['list'][$i]['supplier_bill_tax_type_name']='是';
			}
			
			$result['data']['list'][$i]['shipment_invoice_status_name']=Contents::baseConfig()['shipment']['shipment_invoice_status'][$result['data']['list'][$i]['shipment_invoice_status']];
		
			
    	}
		
 		$r = [
			'code'=>200,
			'data'=>$result['data']['list'],
			'count'=>	$result['data']['count'],

		];	
 		return $r;
    }
	public function addTmsCost(){
		
		$params = Request::instance()->param();

		//获取shipment_uuid
		$supplierBillNumberParams['supplier_bill_number'] = $params['supplier_bill_number'];

		//获取对账信息

		$supplier_bill_result  = $this->callSoaErp('post', '/bill/getSupplierBill', $supplierBillNumberParams);
	
		$supplier_bill_number_result = $supplier_bill_result['data'][0]['supplier_bill_info_array'];
	
		for($i=0;$i<count($supplier_bill_number_result);$i++){
			if($i==0){
				$shipment_id_string="'".$supplier_bill_number_result[$i]['shipment_id']."'";
			}else{
				$shipment_id_string.=",'".$supplier_bill_number_result[$i]['shipment_id']."'";
			}
		}

		$this->assign('supplier_bill_result',$supplier_bill_result['data'][0]);

		

		
		$data = [

				'status'=>1,

				'shipment_id_string'=>$shipment_id_string
		];


		$result = $this->callSoaErp('post', '/shipment/getShipmentInfo', $data);
	


		$pay_all_money = 0;
		$abnormal_all_money = 0;
		for($i=0;$i<count($result['data']);$i++){

			if(is_numeric($result['data'][$i]['shipment_time'])){//发运日期
				$result['data'][$i]['shipment_time'] = date('Y-m-d',$result['data'][$i]['shipment_time']);
			}else{
				$result['data'][$i]['shipment_time']='';
			}
			$result['data'][$i]['shipment_type_name']=Contents::baseConfig()['shipment']['shipment_type'][$result['data'][$i]['shipment_type']];
			$result['data'][$i]['shipment_finance_name']=Contents::baseConfig()['shipment']['shipment_finance_status'][$result['data'][$i]['shipment_finance_status']];
		
			$sh_ab = $result['data'][$i]['shipment_abnormal'];
			$ab_money = 0;
			for($j=0;$j<count($sh_ab);$j++){
				$ab_money+=$sh_ab[$j]['shipment_abnormal_money'];
			}
			$result['data'][$i]['shipment_abnormal_money'] = $ab_money;
			$pay_all_money = $pay_all_money+$result['data'][$i]['pay_all_money'];

			$abnormal_all_money=$abnormal_all_money+$ab_money;
		}
		$this->assign('pay_all_money',$pay_all_money);
		$this->assign('abnormal_all_money',$abnormal_all_money);

		$this->assign('result',$result['data']);
		return $this->fetch('tms_update_cost');		
		
	}
	//修改TMS成本ajax
    public function updateTmsCostAjax(){

        $data = Request::instance()->param();
        
     
		$result = $this->callSoaErp('post', '/bill/updateSupplierBill',$data);

	
        return $result;

    }
    //修改承运商对账
    public function updateSupplierBillAjax(){
    	$data = Request::instance()->param();
    	
    	if(!empty($data['supplier_bill_number_array'])){
    		$data1 = explode(',',$data['supplier_bill_number_array']);
    		unset($data['supplier_bill_number_array']);
    		$data['supplier_bill_number_array'] = $data1;
    	}

   
    	$result = $this->callSoaErp('post', '/bill/updateSupplierBill',$data);


    	return $result;
    }
    //已对账
    public function showSupplierBillOverManage(){
    	$params = Request::instance()->param();
    	$data = [
    			'page' => $this->page(),
    			'page_size' => 50,
    			'status'=>1
	
    	];
    	if(!empty($params['supplier_bill_name'])){
    		$data['supplier_bill_name'] = $params['supplier_bill_name'];
    	}
    	if(!empty($params['supplier_bill_status'])){
    		$data['supplier_bill_status'] = $params['supplier_bill_status'];
    	}
    	$result = $this->callSoaErp('post', '/bill/getSupplierBill',$data);
    	for($i=0;$i<count($result['data']['list']);$i++){
    		$result['data']['list'][$i]['supplier_bill_status_name']=Contents::baseConfig()['bill']['supplier_bill_status'][$result['data']['list'][$i]['supplier_bill_status']];
    		
    		
    	}
   
    
    	
		$this->getPageParams($result);
    	return $this->fetch('supplier_bill_over');
    }

}