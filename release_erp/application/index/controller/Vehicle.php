<?php


namespace app\index\controller;


use think\Request;

class Vehicle extends  Base
{
  public function getVehicleAjax(){
      $data=Request::instance()->param();
      $data['status']=1;
      $result=$this->callSoaErp('post','/source/getVehicle',$data);
      return json($result);

  }
}