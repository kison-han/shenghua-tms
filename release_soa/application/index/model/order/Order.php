<?php

namespace app\index\model\order;
use think\Model;
use app\common\help\Help;
use app\index\service\PublicService;
use think\config;
use think\Db;
class Order extends Model{
    //protected $connection = ['database' => 'erp'];
    protected $table = 'order';
    private $_public_service;

    public function initialize()
    {
    	$this->_public_service = new PublicService();
    	parent::initialize();
    
    }


    /**
     * 添加订单
     * 胡
     */
    public function addOrder($params){
    	$t = time();

    	$data['project_id'] = $params['project_id'];
    	$data['pay_type'] = $params['pay_type'];//支付方式
    	$data['bargain_type'] = $params['bargain_type'];
    	if($data['bargain_type'] == 2){
    		$data['bargain_price'] = $params['bargain_price'];
    	}
    	
    	
    	$data['transportation_type'] = $params['transportation_type'];
    	
    	$data['goods_id'] = $params['goods_id'];
    	$data['send_province_id'] = $params['send_province_id'];
    	$data['send_city_id'] = $params['send_city_id'];
    	$data['send_area_id'] = $params['send_area_id'];
    	$data['send_address'] = $params['send_address'];
    	$data['send_name'] = $params['send_name'];
    	$data['send_cellphone'] = $params['send_cellphone'];
    	
    	$data['accept_province_id'] = $params['accept_province_id'];
    	$data['accept_city_id'] = $params['accept_city_id'];
    	$data['accept_area_id'] = $params['accept_area_id'];
    	$data['accept_address'] = $params['accept_address'];
    	$data['accept_name'] = $params['accept_name'];
    	$data['accept_cellphone'] = $params['accept_cellphone'];
    	//客户单号
    	if(!empty($params['customer_order_number'])){
    		$data['customer_order_number'] = $params['customer_order_number'];
    	}
    	 
    
    	if(is_numeric($params['estimated_count'])){
    		$data['estimated_count'] = $params['estimated_count'];
    		$data['realy_count'] = $params['realy_count'];

    		
    		 
    	}
    	if(is_numeric($params['estimated_pack_count'])){
    		$data['estimated_pack_count'] = $params['estimated_pack_count'];
    		$data['realy_pack_count'] = $params['realy_pack_count'];
    		 
    	}
    	if(is_numeric($params['estimated_weight'])){
    		$data['estimated_weight'] = $params['estimated_weight'];
    		$data['realy_weight'] = $params['realy_weight'];
    		
    		 
    	}
    	if(is_numeric($params['estimated_volume'])){
    		$data['estimated_volume'] = $params['estimated_volume'];
    		$data['realy_volume'] = $params['realy_volume'];
    		
    	}
    	
    	//代收货款
    	if(!empty($params['replacement_prive'])){
    		$data['replacement_prive'] = $params['replacement_prive'];
    		 
    	}
    	
    	//保险货值
    	if(!empty($params['insurance_goods'])){
    		$data['insurance_goods'] = $params['insurance_goods'];
    		 
    	}    	
    	//提货时间
    	if(!empty($params['pickup_time'])){
    		$data['pickup_time'] = $params['pickup_time'];
    		 
    	}
    	//送货时间
    	if(!empty($params['send_time'])){
    		$data['send_time'] = $params['send_time'];
    		 
    	}
    	if(!empty($params['remark'])){
    		$data['remark'] = $params['remark'];
    		 
    	}
    
    
    
    	$data['create_time'] = $t;
    	$data['create_user_id'] = $params['user_id'];
    	$data['update_time'] = $t;
    	$data['update_user_id'] = $params['user_id'];
    	$data['status'] = $params['status'];
    
    
    	Db::startTrans();
    	try{
    		$pk_id = Db::name('order')->insertGetId($data);
    
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    		//\think\Response::create(['code' => '400', 'msg' =>$result], 'json')->send();
    		//exit();
    
    	}
    
    	return $result;
    }
    
    /**
     * 获取客户
     * 胡
     */
    public function getSendGoods($params,$is_count=false,$is_page=false,$page=null,$page_size=20){
    	$data = "1=1 ";
    	if(is_numeric($params['send_goods_id'])){
    		$data.= " and send_goods.send_goods_id = ".$params['send_goods_id'];
    	} 
    	if(is_numeric($params['project_id'])){
    		$data.= " and send_goods.project_id = ".$params['project_id'];
    	}
    	if(is_numeric($params['province_id'])){
    		$data.= " and send_goods.province_id = ".$params['province_id'];
    	}
    	if(is_numeric($params['city_id'])){
    		$data.= " and send_goods.city_id = ".$params['city_id'];
    	}
    	if(is_numeric($params['area_id'])){
    		$data.= " and send_goods.area_id = ".$params['area_id'];
    	}
    	if(is_numeric($params['status'])){
    		$data.= " and send_goods.status = ".$params['status'];
    	}
    	

    	if(!empty($params['send_goods_name'])){
    		$data.= ' and send_goods.send_goods_name like "%'.$params['send_goods_name'].'%"';
    	}
    
    
    	if(!empty($params['send_goods_cellphone'])){
    		$data.= ' and send_goods.send_goods_cellphone like "%'.$params['send_goods_cellphone'].'%"';
    	}

    
    	if($is_count==true){
    		$result = $this->table("send_goods")->where($data)->count();
    	}else {
    		if ($is_page == true) {
    			$result = $this->table("send_goods")->
    			join("project",'project.project_id = send_goods.project_id')->
    			where($data)->limit($page, $page_size)->order('send_goods.create_time desc')->
    			field(['send_goods.*','project.project_name',
    					"(select city_name  from city where city.city_id= send_goods.province_id)"=> 'province_name',    					 						
    					"(select city_name  from city where city.city_id= send_goods.city_id)"=> 'city_name',
						"(select city_name  from city where city.city_id= send_goods.area_id)"=> 'area_name',
    					"(select nickname  from user where user.user_id = send_goods.create_user_id)"=> 'create_user_name',
    					"(select nickname  from user where user.user_id = send_goods.update_user_id)"=> 'update_user_name',
    					'send_goods.update_time', 'send_goods.create_time', "send_goods.status",
    			])->select();
    			 
    			 
    		}else{
    			$result = $this->table("send_goods")->alias('send_goods')->
    			join("project",'project.project_id = send_goods.project_id')->
    
    			where($data)->order('send_goods.create_time desc')->
    			field(['send_goods.*','project.project_name',
    					"(select city_name  from city where city.city_id= send_goods.province_id)"=> 'province_name',    					 						
    					"(select city_name  from city where city.city_id= send_goods.city_id)"=> 'city_name',
						"(select city_name  from city where city.city_id= send_goods.area_id)"=> 'area_name',
    					"(select nickname  from user where user.user_id = send_goods.create_user_id)"=> 'create_user_name',
    					"(select nickname  from user where user.user_id = send_goods.update_user_id)"=> 'update_user_name',
    					'send_goods.update_time', 'send_goods.create_time', "send_goods.status",
    			])->select();
    		}
    	}
    
    	return $result;
    
    }
    
    /**
     * 修改发货方
     */
    public function updateSendGoodsBySendGoodsId($params){
    
    	$t = time();
    	 
        $data['project_id'] = $params['project_id'];
    	$data['province_id'] = $params['province_id'];
    	$data['area_id'] = $params['area_id'];
    	$data['city_id'] = $params['city_id'];
    	 
    	if(!empty($params['send_goods_address'])){
    		$data['send_goods_address'] = $params['send_goods_address'];
    	}
    	 
    
    	if(!empty($params['send_goods_name'])){
    		$data['send_goods_name'] = $params['send_goods_name'];
    		 
    	}
    
    	if(!empty($params['send_goods_cellphone'])){
    		$data['send_goods_cellphone'] = $params['send_goods_cellphone'];
    		 
    	}
    	if(!empty($params['remark'])){
    		$data['remark'] = $params['remark'];
    		 
    	}
    
    
    
    	$data['update_user_id'] = $params['user_id'];
    	$data['update_time'] = $t;
    
    
    
    	$source_price=[];
    	Db::startTrans();
    	try{
    		Db::name('send_goods')->where("send_goods_id = ".$params['send_goods_id'])->update($data);
    		$result = 1;
    		// 提交事务
    		Db::commit();
    
    	} catch (\Exception $e) {
    		$result = $e->getMessage();
    		// 回滚事务
    		Db::rollback();
    
    	}
    	return $result;
    }
    

}