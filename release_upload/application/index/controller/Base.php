<?php
namespace app\index\controller;
use app\common\help\Help;

use think\Controller;
use think\Model;
class Base extends Controller
{
   
    public function __construct()
    {
        
    	// $params = $this->input();
    	// if($params['appKey']!='nexus' || $params['appSecret']!='nexusIt'){
    	// 	$this->outPutError(['msg' => "appKey or appSecret is error"],$params);
    	// 	exit();
    	// }
    	
    	
        parent::__construct();
    }

    //错误日志
    protected function outPutError($errorMsg,$params) {
        //插入日志数据库
  
        \think\Response::create(['code' => '400', 'msg' => $errorMsg['msg']], 'json')->send();
        //防止意外发生
        exit;
    }
    //输出
    public function outPut($result,$msg=null) {
    	if((empty($result) && !is_array($result)) || !is_null($msg)){
    		$this->outPutError(['msg' => $msg],$this->input());
    		exit();
    		
    	}
       
 
        if(empty($result)){
            $result = [];
        }
      
        \think\Response::create(['code' => '200', 'msg'=>'success','data' => $result], 'json')->send();

        //防止意外发生
        exit;
    }
    protected function input() {
        $contents = file_get_contents("php://input");

        $param = json_decode($contents, TRUE);


        if (empty($param)){
            \think\Response::create(['code' => '400', 'msg' => 'json error'], 'json')->send();
            exit();
        }

        return $param;
    }

}
