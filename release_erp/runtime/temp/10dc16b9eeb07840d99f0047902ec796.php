<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:76:"/var/www/html/test_erp/public/../application/index/view/order/order_add.html";i:1662444485;s:62:"/var/www/html/test_erp/application/index/view/public/head.html";i:1657177003;s:65:"/var/www/html/test_erp/application/index/view/public/foot_js.html";i:1658978091;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/css/formSelects-v4.css">
    <link rel="stylesheet" href="/static/layui-v2.6.8/css/layui.css">

    <link rel="stylesheet" href="/static/layui/icon/iconfont.css">
    <link rel="stylesheet" href="/static/layui/multilingual/iconfont.css">
    <link rel="stylesheet" href="/static/css/public.css">
    <link rel="stylesheet" href="/static/layui-soul-table/soulTable.css">
    <!--公共CSS样式-->
    <!--  <link rel="stylesheet" href="/static/css/public_style.css"> -->
    <script src='/static/javascript/public/jquery-2.1.1.min.js'></script>
    <!-- 加载echarts -->
    <script src='/static/echarts/dist/echarts.js'></script>
	<script>
	   let	baseConfig=<?php echo json_encode($baseConfig);?>

	</script>






	<title>
  										<?php if(is_numeric(\think\Request::instance()->get('orders_id'))): ?>
										修改运单
										<?php else: ?>
										新增运单
										<?php endif; ?>	
	
	</title>
<style>
td{
	overflow: inherit!important;
}
.addGoods{
	color: blue;
	display: block;
	margin: 0 auto;
	text-align: center;
	font-size: 39px;
}
.delGoods{
	color: red;
	display: block;
	margin: 0 auto;
	text-align: center;
	font-size: 39px;
}
.ren_add{
	position: absolute;
    z-index: 100;
    top: 0;
    right: -29px;
}
.layui-anim-upbit{
height:max-content;
}
.smart-option *{
    color: #838383;
}
</style>

</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">

	<div class="layui-body">

		<!-- 内容主体区域 -->
		<div class="content_body">
		<!-- 
			<div class='layui-form-item'>
		   		<span class="layui-breadcrumb" lay-separator="-">
					<a>首页</a>
					<a>运单管理</a>
					<a >运单 </a>	
		
					<a><cite>
					
					  				 <?php if(\think\Request::instance()->get('order_id_id') ==  ''): ?>
										新增运单
										<?php else: ?>
										修改运单
										<?php endif; ?>	
					
					</cite></a>
				</span>
			</div>
			-->

			<form class="layui-form layui-form-pane"  id="form1" onSubmit="return orderAdd()">
	
	
					
					
						<div class="layui-inline">
							<label class="layui-form-label input-required">项目:</label>
							<div class="layui-input-inline " >
								<select name="project_id" id='project_id'  lay-verify="required"   lay-filter="orderProject" lay-search>
										<option value=''>请选择</option>
									<?php if(is_array($projectResult) || $projectResult instanceof \think\Collection || $projectResult instanceof \think\Paginator): if( count($projectResult)==0 ) : echo "" ;else: foreach($projectResult as $key=>$vo): ?>
										<option value="<?php echo $vo['project_id']; ?>" <?php if($vo['project_id'] == \think\Request::instance()->get('project_id')): ?>selected<?php endif; ?>> <?php echo $vo['project_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>
						
						<?php if(is_numeric(\think\Request::instance()->get('orders_id'))): ?>
						<div class="layui-inline">
							<label class="">运单编号:</label>
							<div class="layui-input-inline " >
								<?php echo $result['orders_number']; ?>
							</div>
						</div>
						<?php endif; ?>
			
					<hr/>
				<div class="layui-row layui-col-space12" >
					<div class="layui-form-item">
						<div class="layui-inline">
							<label class="layui-form-label" style="width:100px;">发货方列表:</label>
							<div class="layui-input-inline">
								<select name="send_goods_id" id='send_goods_id'  lay-filter="orderSend" lay-search>
									<option value="">请选择</option>
									<?php if(is_array($sendResult) || $sendResult instanceof \think\Collection || $sendResult instanceof \think\Paginator): if( count($sendResult)==0 ) : echo "" ;else: foreach($sendResult as $key=>$vo): ?>
										<option value="<?php echo $vo['send_goods_id']; ?>" <?php if(\think\Request::instance()->get('order_id') == undefined): if($vo['send_goods_id']==$projectData['default_send_goods_id']): ?> selected="selected"<?php endif; endif; if($vo['send_goods_id']==$result['send_goods_id']): ?>selected="selected"<?php endif; ?> > <?php echo $vo['send_goods_company']; ?>-<?php echo $vo['send_goods_name']; ?>-<?php echo $vo['send_goods_address']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
							
								</select>	
								<!--<a href="javascript:void(0);" class="ren_add" style="color:red;" onclick="openlayer('/source/showSendGoodsAdd?nomenu=1&project_id=<?php echo \think\Request::instance()->get('project_id'); ?>')" >&nbsp;新增&nbsp;</a>-->
							</div>
						</div>
						<div class="layui-inline" style="margin-left:10px;">
							<label class="layui-form-label input-required" style="width:100px;">发货方:</label>
							<div class="layui-input-inline">
								 <input id="send_goods_company" name="send_goods_company" value="<?php echo $result['send_goods_company']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
					
							</div>
						</div><div class="layui-inline" style="margin-left:10px;">
							<label class="layui-form-label input-required" style="width:100px;">联系人:</label>
							<div class="layui-input-inline">
								 <input id="send_name" name="send_name" value="<?php echo $result['send_name']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
					
							</div>
						</div>
		
						<div class="layui-inline">
							<label class="layui-form-label input-required" style="width:100px;">联系方式:</label>
							<div class="layui-input-inline">
								 <input id="send_cellphone"  name="send_cellphone" value="<?php echo $result['send_cellphone']; ?>"  lay-verify="required"  class="layui-input" type="text">
				
							</div>
						</div>					
					
						<div class="layui-inline">
							<label class="layui-form-label input-required">省:</label>
							<div class="layui-input-inline " >
								<select name="send_province_id" id='send_province_id' class="province_id"  lay-verify="required"   lay-filter="sheng-shi"  lay-search>
									<option value=''>请选择</option>
									<?php if(is_array($provinceResult) || $provinceResult instanceof \think\Collection || $provinceResult instanceof \think\Paginator): if( count($provinceResult)==0 ) : echo "" ;else: foreach($provinceResult as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $result['send_province_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>
				
			
						<div class="layui-inline">
							<label class="layui-form-label input-required">市:</label>
							<div class="layui-input-inline">
								<select name="send_city_id" id='send_city_id' class="city_id"  lay-verify="required" lay-filter="shi-qu">
									<option value=''>请选择</option>
									<?php if(is_array($cityResult) || $cityResult instanceof \think\Collection || $cityResult instanceof \think\Paginator): if( count($cityResult)==0 ) : echo "" ;else: foreach($cityResult as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $result['send_city_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>
		
		
						<div class="layui-inline">
							<label class="layui-form-label ">区:</label>
							<div class="layui-input-inline">
								<select name="send_area_id" id='send_area_id' class="area_id"   lay-filter="qu">
									<option value=''>请选择</option>
									<?php if(is_array($areaResult) || $areaResult instanceof \think\Collection || $areaResult instanceof \think\Paginator): if( count($areaResult)==0 ) : echo "" ;else: foreach($areaResult as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $result['send_area_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>
							
			
						<div class="layui-inline">
							<label class="layui-form-label input-required">地址:</label>
							<div class="layui-input-inline" style="width:570px;">
								 <input id="send_address" name="send_address" value="<?php echo $result['send_address']; ?>"  lay-verify="required" placeholder="地址" autocomplete="off" class="layui-input" type="text">
													
							</div>
						</div>
			
			<div class="layui-inline">
				<label class="layui-form-label input-required">发站:</label>
				<div class="layui-input-inline">
					<select name="send_location_id" id='send_location_id'  lay-verify="required"  lay-search>
						<option value=''>请选择</option>
						<?php if(is_array($faResult) || $faResult instanceof \think\Collection || $faResult instanceof \think\Paginator): if( count($faResult)==0 ) : echo "" ;else: foreach($faResult as $key=>$vo): ?>
							<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $result['send_location_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
						<?php endforeach; endif; else: echo "" ;endif; ?>
					</select>							
				</div>
			</div>
			

					
																								
				</div>
			</div>			
			
				<div class="layui-row layui-col-space10" >
					<div class="layui-form-item">
						<div class="layui-inline">
							<label class="layui-form-label" style="width:100px;">收货方列表:</label>
							<div class="layui-input-inline">
								<select name="accept_goods_id" id='accept_goods_id'   lay-filter="orderAccept" lay-search>
										<option value=''>请选择</option>
										<?php if(is_array($acceptResult) || $acceptResult instanceof \think\Collection || $acceptResult instanceof \think\Paginator): if( count($acceptResult)==0 ) : echo "" ;else: foreach($acceptResult as $key=>$vo): ?>
										<option <?php if($vo['accept_goods_id']==$result['accept_goods_id']): ?> selected="selected"<?php endif; ?> value="<?php echo $vo['accept_goods_id']; ?>" ><?php echo $vo['accept_goods_company']; ?>-<?php echo $vo['accept_goods_name']; ?>-<?php echo $vo['accept_goods_address']; ?></option>
										<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>		

								<!--<a href="javascript:void(0);" class="ren_add" style="color:red;" onclick="openlayer('/source/showAcceptGoodsAdd?nomenu=1&project_id=<?php echo \think\Request::instance()->get('project_id'); ?>')" >&nbsp;新增&nbsp;</a>
									-->					
								<!--  <?php echo $vo['province_name']; ?><?php echo $vo['city_name']; ?><?php echo $vo['area_name']; ?><?php echo $vo['accept_goods_address']; ?><?php echo $vo['accept_goods_name']; ?><?php echo $vo['accept_goods_cellphone']; ?> -->				
							</div>
						</div>
				
						<div class="layui-inline" style="margin-left:10px;">
							<label class="layui-form-label input-required" style="width:100px;">收货方:</label>
							<div class="layui-input-inline">
								 <input id="accept_goods_company" name="accept_goods_company" value="<?php echo $result['accept_goods_company']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
					
							</div>
						</div><div class="layui-inline" style="margin-left:10px;">
							<label class="layui-form-label input-required" style="width:100px;">联系人:</label>
							<div class="layui-input-inline">
								 <input id="accept_name" name="accept_name" value="<?php echo $result['accept_name']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
					
							</div>
						</div>
				
				
						<div class="layui-inline">
							<label class="layui-form-label input-required" style="width:100px;">联系方式:</label>
							<div class="layui-input-inline">
								 <input id="accept_cellphone" name="accept_cellphone" value="<?php echo $result['accept_cellphone']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" type="text">
				
							</div>
						</div>
						
						<div class="layui-inline">
							<label class="layui-form-label input-required">省:</label>
							<div class="layui-input-inline" >
								<select name="accept_province_id" id='accept_province_id' class='province_id'  lay-verify="required"   lay-filter="sheng-shi"  lay-search>
									<option value=''>请选择</option>
									<?php if(is_array($provinceResult) || $provinceResult instanceof \think\Collection || $provinceResult instanceof \think\Paginator): if( count($provinceResult)==0 ) : echo "" ;else: foreach($provinceResult as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $result['accept_province_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>

						<div class="layui-inline">
							<label class="layui-form-label input-required">市:</label>
							<div class="layui-input-inline">
								<select name="accept_city_id" id='accept_city_id'  lay-verify="required" class='city_id' lay-filter="shi-qu" lay-search placeholder='请选择市'>
									<option value=''>请选择</option>
									<?php if(is_array($cityResult_accept) || $cityResult_accept instanceof \think\Collection || $cityResult_accept instanceof \think\Paginator): if( count($cityResult_accept)==0 ) : echo "" ;else: foreach($cityResult_accept as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $result['accept_city_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>

						<div class="layui-inline">
							<label class="layui-form-label ">区:</label>
							<div class="layui-input-inline">
								<select name="accept_area_id" id='accept_area_id'   class='area_id'  lay-filter="qu" lay-search placeholder='请选择区'>
									<option value=''>请选择</option>
									<?php if(is_array($areaResult_accept) || $areaResult_accept instanceof \think\Collection || $areaResult_accept instanceof \think\Paginator): if( count($areaResult_accept)==0 ) : echo "" ;else: foreach($areaResult_accept as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>" <?php if($vo['city_id'] == $result['accept_area_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>
							<div class="layui-inline">
							<label class="layui-form-label input-required">地址:</label>
							<div class="layui-input-inline" style="width:570px;">
								 <input id="accept_address" name="accept_address" value="<?php echo $result['accept_address']; ?>"  lay-verify="required" placeholder="地址" autocomplete="off" class="layui-input" type="text">
													
							</div>
						</div>					
						<div class="layui-inline">
							<label class="layui-form-label input-required">到站:</label>
							<div class="layui-input-inline">
								<select name="accept_location_id" id='accept_location_id' lay-verify="required" lay-search>
									<?php if(is_array($daoResult) || $daoResult instanceof \think\Collection || $daoResult instanceof \think\Paginator): if( count($daoResult)==0 ) : echo "" ;else: foreach($daoResult as $key=>$vo): ?>
										<option value="<?php echo $vo['city_id']; ?>"  <?php if($vo['city_id'] == $result['accept_location_id']): ?>selected<?php endif; ?>> <?php echo $vo['city_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
								</select>							
							</div>
						</div>
		

	

																					
					</div>
				</div>
				<hr/>
			

			
		
			
				<div class="layui-row layui-col-space12" >
	
							<div class="layui-form-item">
							         <div class="layui-inline">
							      		<label class="layui-form-label">客户单号</label>
							      			<div class="layui-input-inline">
								  				<input id="customer_order_number" name="customer_order_number" value="<?php echo $result['customer_order_number']; ?>"   placeholder="" autocomplete="off" class="layui-input" type="text">
							     			 </div>
							    	</div>	
									<?php if(\think\Session::get('user.company_id')==4): ?>
									<div class="layui-inline">
							      		<label class="layui-form-label">客户运单号</label>
							      			<div class="layui-input-inline">
								  				<input id="khydh" name="khydh" value="<?php echo $result['khydh']; ?>"   placeholder="" autocomplete="off" class="layui-input" type="text">
							     			 </div>
							    	</div><?php endif; ?>									
								<div class="layui-inline">
									<label class="layui-form-label input-required" style="width:120px;">支付方式:</label>
									<div class="layui-input-inline">
										<select name="pay_type" id='pay_type'  lay-verify="required">
												<option value="3" <?php if($result['pay_type'] == 3): ?>selected<?php endif; ?>> 月结</option>
												<option value="1" <?php if($result['pay_type'] == 1): ?>selected<?php endif; ?>> 现付</option>
												<option value="2" <?php if($result['pay_type'] == 2): ?>selected<?php endif; ?>> 到付</option>

										</select>							
									</div>
								</div>
						
						
						<div class="layui-inline">
									<label class="layui-form-label " style="width:120px;">到付款:</label>
									<div class="layui-input-inline">
									<input name="dfk" class="layui-input" value="<?php echo $result['dfk']; ?>"></input>			
									</div>
								</div>
						
					
								<div class="layui-inline">
									<label class="layui-form-label input-required" style="width:100px;">计价方式:</label>
									<div class="layui-input-inline">
										<select name="bargain_type" id='bargain_type'  lay-verify="required"  lay-filter="bargain_type">
												<option value="1" <?php if($result['bargain_type'] ==  1): ?>selected<?php endif; ?>> 合同</option>
												<option value="2" <?php if($result['bargain_type'] == 2): ?>selected<?php endif; ?>> 单票</option>
				
												

										</select>					
									</div>
								</div>

								<br/>
								<div class="layui-inline">
									<label class="layui-form-label input-required" style="width:100px;">运单时间</label>
									<div class="layui-input-inline">
										<input id="pickup_time" name="pickup_time" <?php if(\think\Request::instance()->get('orders_id')): ?> disabled="disabled" <?php endif; ?> lay-verify="required" value="<?php if($result['pickup_time'] != ''): ?><?php echo date('Y-m-d',$result['pickup_time']); endif; ?>" placeholder="" autocomplete="off" class="layui-input" type="text">
									</div>
								</div>
								<div class="layui-inline">
									<label class="layui-form-label">送达时间</label>
									<div class="layui-input-inline">
										<input id="send_time" name="send_time" value="<?php if($result['send_time'] != ''): ?><?php echo date('Y-m-d',$result['send_time']); endif; ?>"  placeholder="" autocomplete="off" class="layui-input" type="text">
									</div>
								</div>

								<div class="layui-inline">
							      		<label class="layui-form-label " id="bargain_price_title">分公司</label>
											<div class="layui-input-inline">
											<select name="choose_company_id" id='choose_company_id'  lay-verify="required"  >
										      <?php if(is_array($comapnyResult) || $comapnyResult instanceof \think\Collection || $comapnyResult instanceof \think\Paginator): if( count($comapnyResult)==0 ) : echo "" ;else: foreach($comapnyResult as $key=>$cr): ?>
										
													<option value="<?php echo $cr['company_id']; ?>" <?php if($company_id == $cr['company_id']): ?>selected<?php endif; ?>><?php echo $cr['company_name']; ?></option>
											
											  
											  
											  <?php endforeach; endif; else: echo "" ;endif; ?>
											</select>
											</div>
							    	</div>						
									<div class="layui-inline">
										<label class="layui-form-label input-required" style="width:100px;">运输类型:</label>
										<div class="layui-input-inline">
											<select name="transportation_type" id='transportation_type'  lay-verify="required"  lay-filter="bargain_type">
											
													<option value="1" <?php if($result['transportation_type'] == 1): ?>selected<?php endif; ?>> 公路</option>
													<option value="2" <?php if($result['transportation_type'] == 2): ?>selected<?php endif; ?>> 铁路</option>
													<option value="3" <?php if($result['transportation_type'] == 3): ?>selected<?php endif; ?>> 水运</option>
													<option value="4" <?php if($result['transportation_type'] == 4): ?>selected<?php endif; ?>> 航空</option>		
													<option value="5" <?php if($result['transportation_type'] == 5): ?>selected<?php endif; ?>> 快递</option>												
											</select>												
										
										</div>			
									</div>
									
								

								  <div class="layui-inline">
							      		<label class="layui-form-label">代收货款</label>
							      			<div class="layui-input-inline">
								  				<input id="replacement_prive" type="number" name="replacement_prive" value="<?php echo $result['replacement_prive']; ?>"   placeholder="" autocomplete="off" class="layui-input" type="text">
							     			 </div>
							    	</div>
								<br/>
								  <div class="layui-inline">
							      		<label class="layui-form-label">货值</label>
							      			<div class="layui-input-inline">
								  				<input id="insurance_goods" name="insurance_goods" value="<?php echo $result['insurance_goods']; ?>"   placeholder="" autocomplete="off" class="layui-input" type="text">
							     			 </div>
							    	</div>

									<div class="layui-inline">
										<label class="layui-form-label input-required" style="width:100px;">交货方式:</label>
										<div class="layui-input-inline">
									<select name="delivery_method" id='delivery_method'  lay-verify="required"  lay-filter="delivery_method">
										      <?php if(is_array($baseConfig['order']['delivery_method']) || $baseConfig['order']['delivery_method'] instanceof \think\Collection || $baseConfig['order']['delivery_method'] instanceof \think\Paginator): if( count($baseConfig['order']['delivery_method'])==0 ) : echo "" ;else: foreach($baseConfig['order']['delivery_method'] as $key3=>$vo3): ?>
											  <option value="<?php echo $key3; ?>" <?php if($result['delivery_method'] == $key3): ?>selected<?php endif; ?>> <?php echo $vo3; ?></option>
											  
											  <?php endforeach; endif; else: echo "" ;endif; ?>
												
										</select>
										</div>
									</div>

									<div class="layui-inline">
							      		<label class="layui-form-label">免运费:</label>
							      			<div class="layui-input-inline">
								  			<select name="is_free" id='is_free'  lay-verify="required"  lay-filter="is_free">
										      <option value="0" >否</option>
											  <option value="1" <?php if($result['is_free'] == 1): ?>selected<?php endif; ?>>是</option>
												
										</select>	 </div>
							    	</div>
									<div class="layui-inline">
							      		<label class="layui-form-label">随车宝:</label>
							      			<div class="layui-input-inline">
												<select name="scb_number" lay-search   lay-filter="is_free">
													<option value=''>--请选择--</option>
													<?php if(is_array($equipmentResult) || $equipmentResult instanceof \think\Collection || $equipmentResult instanceof \think\Paginator): if( count($equipmentResult)==0 ) : echo "" ;else: foreach($equipmentResult as $key=>$er): ?>
														<option value='<?php echo $er['equipment_number_in']; ?>' <?php if($er['equipment_number_in'] == $result['scb_number']): ?>selected<?php endif; ?>>
														<?php echo $er['equipment_number_in']; ?>-<?php echo $er['remark']; ?>-<?php echo $er['orders_number']; ?>
														</option>
													<?php endforeach; endif; else: echo "" ;endif; ?>
												
												</select>	 
											</div>
							    	</div>

							</div>
							
							<?php if(\think\Session::get('user.company_id')==4): ?>
				 					<div class="layui-form-inline">
							      		<label class="layui-form-label">客户备注:</label>
							      			<div class="layui-input-block">
								  			
							     			 	<textarea style="position:relative;left:-30px;width:90%;height:38px;" id="customer_remark" name="customer_remark"   autocomplete="off" class="layui-input"   rows="" cols=""><?php echo $result['customer_remark']; ?></textarea>
											 
											 </div>
							    	</div>
									<?php endif; ?>
									<div class="layui-form-inline">
							      		<label class="layui-form-label">备注:</label>
							      			<div class="layui-input-block">
								  			
							     			 	<textarea style="position:relative;left:-30px;width:90%;height:38px;" id="remark" name="remark"   autocomplete="off" class="layui-input"   rows="" cols=""><?php echo $result['remark']; ?></textarea>
											 
											 </div>
							    	</div>


						</div>	
						<hr />



    <table class="layui-table">
      <colgroup>
        <col width="150">
        <col width="100">
        <col>
      </colgroup>
      <thead>
        <tr>
          <th>货物信息</th>
          <th>下单件数</th>
          <th>下单数量</th>
          <th>下单单位</th>
          <th>下单重量</th>
           <th>下单体积</th>
           
          
          <th>计费件数</th>
          <th>计费数量</th>
           <th>计费单位</th>
           <th>计费重量</th>           
            <th>计费体积</th>    
          
            <th>操作</th>    
        </tr> 
      </thead>
      <tbody class="goods-contents-body">
		  <?php if(!$result['orders_goods_info'][0]): ?>
        <tr>
          <td style="overflow: inherit;width:200px;">
          <div style="display:flex">
		  <div style="width:100px">
												<select name="goods_id[]"   lay-filter="goods_name" lay-search>
												<option value="">快速选择</option>
												
														<?php if(is_array($goodsResult) || $goodsResult instanceof \think\Collection || $goodsResult instanceof \think\Paginator): if( count($goodsResult)==0 ) : echo "" ;else: foreach($goodsResult as $key=>$vo): ?>
														<option value="<?php echo $vo['goods_id']; ?>" <?php if($vo['goods_id'] == $vv['goods_id']): ?>selected<?php endif; ?>> <?php echo $vo['goods_name']; ?></option>
								
														<?php endforeach; endif; else: echo "" ;endif; ?>
												</select>
											</div>
										<input style="width:100px" placeholder="货物名称" class="layui-input goods_name" name="goods_name[]" lay-verify="required"  lay-filter="goods_name_input" ></input>
				<!--							
			  <a href="javascript:void(0);" class="ren_add" style="color:red;display:block;position:relative;left:0px;" onclick="openlayer('/source/showGoodsAdd?nomenu=1&project_id=<?php echo \think\Request::instance()->get('project_id'); ?>')" >&nbsp;新增货物&nbsp;</a>
-->
		  </div>
		  </td>
          <td>									  				
          	<input  name="estimated_count[]"    value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdjs" type="text">
          </td><td>
          	<input  name="estimated_pack_count[]"    value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdsl" type="text">
          </td>
       <td><select name="estimated_pack_unit[]" lay-filter="estimated_pack_unit" id="">
														<?php if(is_array($baseConfig['order']['goods_pack']) || $baseConfig['order']['goods_pack'] instanceof \think\Collection || $baseConfig['order']['goods_pack'] instanceof \think\Paginator): if( count($baseConfig['order']['goods_pack'])==0 ) : echo "" ;else: foreach($baseConfig['order']['goods_pack'] as $key2=>$vo2): ?>
														<option value="<?php echo $key2; ?>"><?php echo $vo2; ?></option>
														<?php endforeach; endif; else: echo "" ;endif; ?>
													</select></td>
	   <td><input name="estimated_weight[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdzl" type="text">
								     		</td>
											<td><input name="estimated_volume[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdtj" type="text">
								     		</td>

											<td>	<input  name="realy_count[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input jfjs" type="text">
								     			</td>
											<td>	<input  name="realy_pack_count[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input jfsl" type="text">
								     			</td>
												<td>

									<select name="realy_pack_unit[]" id="">
																							<?php if(is_array($baseConfig['order']['goods_pack']) || $baseConfig['order']['goods_pack'] instanceof \think\Collection || $baseConfig['order']['goods_pack'] instanceof \think\Paginator): if( count($baseConfig['order']['goods_pack'])==0 ) : echo "" ;else: foreach($baseConfig['order']['goods_pack'] as $key2=>$vo2): ?>
																							<option value="<?php echo $key2; ?>"><?php echo $vo2; ?></option>
																							<?php endforeach; endif; else: echo "" ;endif; ?>
																						</select>
												</td>
									<td>
										<input  name="realy_weight[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input jfzl" type="text">

									</td>
										<td>
									<input  name="realy_volume[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input jftj" type="text">
											
										</td>
										
										<td><a href="javascript:void();" class="addGoods">+</a></td>
        </tr>
		<?php endif; if(is_array($result['orders_goods_info']) || $result['orders_goods_info'] instanceof \think\Collection || $result['orders_goods_info'] instanceof \think\Paginator): if( count($result['orders_goods_info'])==0 ) : echo "" ;else: foreach($result['orders_goods_info'] as $key3=>$vv): ?>
		<tr>
		   <td style="overflow: inherit;display:flex" >
		   <div style="width:100px" class="smart-option">
														<select name="goods_id[]"   lay-filter="goods_name" lay-search>
														<option value="">快速选择</option>
																<?php if(is_array($goodsResult) || $goodsResult instanceof \think\Collection || $goodsResult instanceof \think\Paginator): if( count($goodsResult)==0 ) : echo "" ;else: foreach($goodsResult as $key=>$vo): ?>
																<option value="<?php echo $vo['goods_id']; ?>" <?php if($vo['goods_id'] == $vv['goods_id']): ?>selected<?php endif; ?>> <?php echo $vo['goods_name']; ?></option>
										
																<?php endforeach; endif; else: echo "" ;endif; ?>
														</select>
		   </div>
		   
		   <input style="width:100px" placeholder="货物名称" value="<?php echo $vv['goods_name']; ?>" goods-id="<?php echo $vv['goods_id']; ?>" class="layui-input goods_name" name="goods_name[]" lay-verify="required"  lay-filter="goods_name_input" ></input>
								
		   
		   </td>
		   <td>									  				
		   	<input  name="estimated_count[]"    value="<?php echo $vv['estimated_count']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdjs" type="text">
		   </td> <td>									  				
		   	<input  name="estimated_pack_count[]"    value="<?php echo $vv['estimated_pack_count']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdsl" type="text">
		   </td>
		<td><select name="estimated_pack_unit[]" id="">
																<?php if(is_array($baseConfig['order']['goods_pack']) || $baseConfig['order']['goods_pack'] instanceof \think\Collection || $baseConfig['order']['goods_pack'] instanceof \think\Paginator): if( count($baseConfig['order']['goods_pack'])==0 ) : echo "" ;else: foreach($baseConfig['order']['goods_pack'] as $key2=>$vo2): ?>
																<option value="<?php echo $key2; ?>" <?php if($key2==$vv['estimated_pack_unit']): ?> selected="selected"<?php endif; ?>><?php echo $vo2; ?></option>
																<?php endforeach; endif; else: echo "" ;endif; ?>
															</select></td>
		<td><input name="estimated_weight[]" value="<?php echo $vv['estimated_weight']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdzl" type="text">
										     		</td> 
													<td><input name="estimated_volume[]" value="<?php echo $vv['estimated_volume']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdtj" type="text">
										     		</td>
													
													<td>	<input  name="realy_count[]" value="<?php echo $vv['realy_count']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input jfjs" type="text">
										     			</td><td>	<input  name="realy_pack_count[]" value="<?php echo $vv['realy_pack_count']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input jfsl" type="text">
										     			</td>
														<td>
								
											<select name="realy_pack_unit[]" id="">
																									<?php if(is_array($baseConfig['order']['goods_pack']) || $baseConfig['order']['goods_pack'] instanceof \think\Collection || $baseConfig['order']['goods_pack'] instanceof \think\Paginator): if( count($baseConfig['order']['goods_pack'])==0 ) : echo "" ;else: foreach($baseConfig['order']['goods_pack'] as $key2=>$vo2): ?>
																									<option value="<?php echo $key2; ?>" <?php if($key2==$vv['realy_pack_unit']): ?> selected="selected"<?php endif; ?>><?php echo $vo2; ?></option>
																									<?php endforeach; endif; else: echo "" ;endif; ?>
																								</select>				
														</td>
											<td>
												<input  name="realy_weight[]" value="<?php echo $vv['realy_weight']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input zfzl" type="text">
												
											</td>	
												<td>
											<input  name="realy_volume[]" value="<?php echo $vv['realy_volume']; ?>"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input jftj" type="text">
													
												</td>
												
												<td>
													<?php if($key3==0): ?>
													<a href="javascript:void();" class="addGoods">+</a>
													<?php else: ?>
													<a href="javascript:void();" class="delGoods">-</a>
													<?php endif; ?>
													</td>
		 </tr>
		
		<?php endforeach; endif; else: echo "" ;endif; ?>
		
      </tbody>
    </table>
          
						
						    																																																					
			

						
				<input type='hidden' id='orders_id' name='orders_id'  value="<?php echo $result['orders_id']; ?>" />
				<input type='hidden' id='project_id' name='project_id'  value="<?php echo \think\Request::instance()->get('project_id'); ?>" />
				<div class="layui-form-item">
					<div class="layui-input-block all-button-center">
					<?php if(!\think\Request::instance()->get('view')): ?>
						<button class="layui-btn nav-submit" lay-submit="" lay-filter="formDemo" id="dining_add_button">提交</button>
					<?php endif; ?>
					</div>
				</div>
			</form>
		</div>
		<hr/>
		
		
	</div>
						
	
</div>
<?php if(($function_name == 'showbookinglist') or  ($function_name == 'showclientpaymentlist') or ($function_name == 'showaccountpaymentlist') or ($function_name == 'showcostlist')): ?>
	<!--<script src='/static/javascript/product/all.js'></script>-->
	<script src='/static/javascript/data.js'></script>
	<!--<script src='/static/javascript/product/company_order.js'></script>-->
	<script type="text/javascript" src="/static/layui-v2.6.8/layui.js"></script>
<?php else: ?>
	<script src="/static/layui-v2.6.8/layui.js"></script>
<?php endif; ?>

<input type='hidden' id='foot_InStationLetterStime' value=""/>
<!--<script type="text/javascript" src="/static/javascript/public/help.js"></script>-->
<script src='/static/javascript/public/formSelects-v4.js'></script>

<script type="text/javascript" src="/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>
	function openlayer(url,title,width="500px",height="600px"){
	    layer.open({
	        type:2,
	        title:title,
	        content:url,
	        area:[width,height]
	    })
	}

    !function(){
       layui.use(['jquery','layer','laydate','laypage'], function(){
	    var table = layui.table;
	    var $ = layui.jquery;
	    var laydate = layui.laydate;
	    var soulTable=layui.soulTable;
		var laypage = layui.laypage;
        var InStationLetterStime = $('#foot_InStationLetterStime').val();
        var layer = layui.layer
        var function_name = "<?php echo $function_name; ?>";



        $('#left-nav').find('.layui-nav-item').on('click',function(){
             if($(this).hasClass('layui-nav-itemed')){
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
                 $(this).addClass('layui-nav-itemed');
            }else{
                 $('#left-nav').find('.layui-nav-item').removeClass('layui-nav-itemed');
             }

        });




        $('.tips-system-message').on('click',function(){
			
            var html = $('#tips-system-message-js').html();
            layer.tips(html, '.tips-system-message', {
                tips: [3, '#fff'],
                padding:'0',
                tipsMore: false,
                area: ['450px', 'auto'],
                shade: [0.01, '#fff'],
                shadeClose:true,
                time:0
			
            });

        });

      })
    }();

    function multilingualSettingVisitorMessBackOpenClose(){
        layer.close(open);
    }

    /**
     * 多语言设置
     * id 控件元素ID
     * original_table_name 原始表名
     * original_table_field_name 原表字段名
     * original_table_id 原表名所对应的主键ID
     * */
    function MultilingualSetting(id,original_table_name,original_table_field_name,original_table_id){
//        $.post('/language/multilingualSetting',{'original_table_name':original_table_name,'original_table_field_name':original_table_field_name,'original_table_id':original_table_id});

        open = layer.open({
            title:'',
            type: 2,
            area: ['65%','600px'],
            content: ['/language/multilingualSetting?original_table_name='+original_table_name+'&original_table_field_name='+original_table_field_name+'&original_table_id='+original_table_id] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }


    //阅读系统消息
    function Aurl(obj){
        var idd = $(obj).attr("data-id");
        var url = $(obj).attr('data-href');

        $.post('/reminderManagement/readInStationLetterAjax',{'in_station_letter_id':idd},function(){
            location.href = url;
        });

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }
    userLanguage();
    function userLanguage() {
        var user_language_id=$("#user_language_id").val();
        if(user_language_id>2){
            $(".layui-form-label,.top-right-table td").css({"overflow":"hidden","white-space":"nowrap","text-overflow":"ellipsis"});
            $(".layui-form-label").css("width","145px").siblings(".layui-input-block").css("margin-left","175px");
            $("body .layui-side-scroll").css("width","260px");
            tips($(".layui-form-label"));
            tips($(".layui-table thead th"));
            tips($(".top-right-table td"));
        }
    }

    tips($(".layui-side-scroll dd a,.layui-side-scroll li em"),'left');
    function tips(obj,cont) {
        obj.hover(function () {
            if($(this).html()!=''){
                if(cont=='left'){
                    $(this).attr("title",$(this).html());
                }else{
                    var html=$(this).html().replace("<i>*</i>","");
                    /*layer.tips($(this).html(), this, {time: 0});*/
                    $(this).attr("title",html);
                }

            }
        }
        /*,function () {
                layer.closeAll();
            }*/
        )
    }

    /*table显示暂无数据*/
    table()
    function table() {
        $(".layui-table").each(function (index,item) {
            if($(item).find("tbody tr").length===0){
                var width=$(item).parent(".table-nont").width()-2;
                $(item).find("tfoot").hide();
                $(item).parents(".table-nont").css("padding-bottom","50px").append("<div class='table-none' style='width: "+width+"px'><?php echo $language_tag['index_public_noData']; ?></div>");
            }
        })
    }
    function tableNone(){
        $(".table-none").remove();
        $(".plan-table-nont").css("padding-bottom","0px");
        $(".table-nont").css("padding-bottom","0px");
    }
    /*layer.config({
        skin:'my-skin'
    })*/
    /*label加星号*/
    $(".input-required i").remove();
    $(".input-required").prepend("<i>*</i>");
    $(".table-input-none tr").hover(function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#f2f2f2");
    },function () {
        $(this).find(".layui-input,.layui-select,.layui-textarea").css("background","#fff");
    });


    height();
    $(window).resize(function () {
        height();
    });
    function height() {
        var bodyTopH=$(".body-top").height();
        var bodyH=$(".layui-body").height();
        var tableH=$(".user-manage table").height();
        var height=bodyH-bodyTopH-15-60;//右侧总高度-表格上面内容高度-最外层padding值-底部距离
        var company=bodyH-bodyTopH-15-165;
        var newBg=bodyH-bodyTopH-15-60-55;//右侧总高度-表格上面内容高度-最外层padding值-底部距离-表格上面的按钮//灰背景的
        if(tableH>height||tableH>company||tableH>newBg){
            $(".pageHeight").css("height",height);
    //        $(".company-pageHeight").css("height",company);
            $(".newBg-pageHeight").css("height",newBg);
        }
    }

    /*日期选择*/
    $(".layui-input-date").each(function(){
      //  laydate.render({
       //     elem: this,
       // });
    });
	//获取整个页面高度
	var allHeight = $(window).height();
    var headerHeight = 0//$('.layui-header').height();
	var itemHeight = $('.layui-form-item').height();
	var searchHeight = $('.all-search-bg').height();
	var tableHeight = allHeight-headerHeight-itemHeight-searchHeight-5;	
</script>
<script src='/static/javascript/system/customer.js'></script>
<script src='/static/javascript/system/province_city_area.js'></script>

</body>
</html>
<script>
<?php if($goodsResult): ?>
goods_id=<?php echo $goodsResult[0]['goods_id']; ?>;
goods_id_old=<?php echo $goodsResult[0]['goods_id']; ?>;
<?php endif; ?>
layui.use(['laydate','element','form'], function(){
    var laydate = layui.laydate;
    var element = layui.element;
    var formSelects = layui.formSelects;
    var upload=layui.upload;

	layui.use('element', function(){
		var element = layui.element;
	});
	var isShow = true;  //定义⼀个标志位
	$('.kit-side-fold').click(function(){
		//选择出所有的span，并判断是不是hidden
		$('.layui-nav-item span').each(function(){
			if($(this).is(':hidden')){
				$(this).show();
			}else{
				$(this).hide();
			}
		});
		//判断isshow的状态
		if(isShow){
			$('.layui-side.layui-bg-black').width(50); //设置宽度
			$('.kit-side-fold i').css('margin-right', '70%');  //修改图标的位置
			//将footer和body的宽度修改
			$('.layui-body').css('left', 60+'px');
			$('.layui-footer').css('left', 60+'px');
			//将⼆级导航栏隐藏
			$('dd span').each(function(){
				$(this).hide();
			});
			//修改标志位
			isShow =false;
		}else{
			$('.layui-side.layui-bg-black').width(200);
			$('.kit-side-fold i').css('margin-right', '10%');
			$('.layui-body').css('left', 200+'px');
			$('.layui-footer').css('left', 200+'px');
			$('dd span').each(function(){
				$(this).show();
			});
			isShow =true;
		}
	});

	//默认时间
    laydate.render({
        elem: '#pickup_time',
        trigger: 'click',
        lang: 'zn',
		
    });
    laydate.render({
        elem: '#send_time',
        trigger: 'click',//解决一闪而过的问题
        lang: 'zn',
    });
	$(document).on('keyup','.xdjs',function(){
	var that=this;
		var  v = $(this).val();

		goods_id=$(this).parents("tr").find('.goods_name').attr("goods-id");
		console.log(goods_id)
		$.ajax({
		url:"/source/getGoodsAjax",
		data:{goods_id:goods_id},
		success:function(e){
		
		if(e.calc_way==1)
		{
		$(that).parents("tr").find('td').eq(4).find('input').val((v*e.goods_weight).toFixed(2))
		$(that).parents("tr").find('td').eq(5).find('input').val((v*e.goods_volume).toFixed(2))
		$(that).parents("tr").find('td').eq(9).find('input').val((v*e.goods_weight).toFixed(2))
		$(that).parents("tr").find('td').eq(10).find('input').val((v*e.goods_volume).toFixed(2))
		}
		}})
		
		$(that).parents("tr").find('td').eq(6).find('input').val(v)
	})
	$(document).on('keyup','.xdsl',function(){
	goods_id=$(this).parents("tr").find('.goods_name').attr("goods-id");
		var  v = $(this).val()
		var that=this;
		$.ajax({
		url:"/source/getGoodsAjax",
		data:{goods_id:goods_id},
		success:function(e){
		
		if(e.calc_way==0)
		{
		
		
		$(that).parents("tr").find('td').eq(4).find('input').val((v*e.goods_weight).toFixed(2))
		$(that).parents("tr").find('td').eq(5).find('input').val((v*e.goods_volume).toFixed(2))
		$(that).parents("tr").find('td').eq(9).find('input').val((v*e.goods_weight).toFixed(2))
		$(that).parents("tr").find('td').eq(10).find('input').val((v*e.goods_volume).toFixed(2))
		}
		}})
		
		$(that).parents("tr").find('td').eq(7).find('input').val(v)
	})
	$(document).on('keyup','.xdzl',function(){
		var  v = $(this).val()
		$(this).parents("tr").find('td').eq(9).find('input').val(v)
	})
	$(document).on('keyup','.xdtj',function(){
		var  v = $(this).val()
		$(this).parents("tr").find('td').eq(10).find('input').val(v)
	})
    $(document).on("click",".addGoods",function(){
    	/* $('#goodsList').append($('#hiddenAddGoods').html())
    	form.render('select'); */

		$(".goods-contents-body").append(`<tr>
          <td style="overflow: inherit;display:flex;">
          <div style="width:100px" class="smart-option">
												<select name="goods_id[]"  lay-filter="goods_name" lay-search>
														<option value="">快速选择</option>
														<?php if(is_array($goodsResult) || $goodsResult instanceof \think\Collection || $goodsResult instanceof \think\Paginator): if( count($goodsResult)==0 ) : echo "" ;else: foreach($goodsResult as $key=>$vo): ?>
														<option value="<?php echo $vo['goods_id']; ?>" > <?php echo $vo['goods_name']; ?></option>

														<?php endforeach; endif; else: echo "" ;endif; ?>
												</select>
          </div>
		  <input style="width:100px" placeholder="货物名称" value="" class="layui-input goods_name" name="goods_name[]" lay-verify="required" lay-filter="goods_name_input">
          </td>
          <td>
          	<input  name="estimated_count[]"    value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdjs" type="text">
          </td><td>
          	<input  name="estimated_pack_count[]"    value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdsl" type="text">
          </td>
       <td><select name="estimated_pack_unit[]" lay-filter="estimated_pack_unit" id="">
														<?php if(is_array($baseConfig['order']['goods_pack']) || $baseConfig['order']['goods_pack'] instanceof \think\Collection || $baseConfig['order']['goods_pack'] instanceof \think\Paginator): if( count($baseConfig['order']['goods_pack'])==0 ) : echo "" ;else: foreach($baseConfig['order']['goods_pack'] as $key2=>$vo2): ?>
														<option value="<?php echo $key2; ?>"><?php echo $vo2; ?></option>
														<?php endforeach; endif; else: echo "" ;endif; ?>
													</select></td>
	   <td><input name="estimated_weight[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdzl" type="text">
								     		</td>
											<td><input name="estimated_volume[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input xdtj" type="text">
								     		</td>
											
											<td>	<input  name="realy_count[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input jfsl" type="text">
								     			</td><td>	<input  name="realy_pack_count[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input jfsl" type="text">
								     			</td>
												<td>
						
									<select name="realy_pack_unit[]" id="">
																							<?php if(is_array($baseConfig['order']['goods_pack']) || $baseConfig['order']['goods_pack'] instanceof \think\Collection || $baseConfig['order']['goods_pack'] instanceof \think\Paginator): if( count($baseConfig['order']['goods_pack'])==0 ) : echo "" ;else: foreach($baseConfig['order']['goods_pack'] as $key2=>$vo2): ?>
																							<option value="<?php echo $key2; ?>"><?php echo $vo2; ?></option>
																							<?php endforeach; endif; else: echo "" ;endif; ?>
																						</select>				
												</td>
									<td>
										<input  name="realy_weight[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input zfzl" type="text">
										
									</td>	
										<td>
									<input  name="realy_volume[]" value="0"  lay-verify="required" placeholder="" autocomplete="off" class="layui-input jftj" type="text">
											
										</td>
										
										<td><a href="javascript:void();" class="delGoods">-</a></td>
        </tr>`);
		form.render('select');
		
    })
    $(document).on("click",".delGoods",function(){
    	/* $(this).parent().remove() */
		
		$(this).parents('tr').remove()
		/* if($(".goods-contents-body tr").length>1)
    	$(".goods-contents-body tr:last-child").remove(); */
    })
    

});



if($('#send_goods_id').val()>0)
  $.ajax({
        type: "POST",
        url: '/source/showSendGoodsAjax',
        data: {
        	  'send_goods_id':$('#send_goods_id').val()
        	
        	},
        success: function(data){

            if(data.code!=200){
                layer.msg(data.msg);
                return false;
            }else if(data.code==200) {
            	k = data.data[0];
			console.log(k)
            	$('#send_goods_id').val()      
        
            	$('#send_province_id').find("option[value="+k.province_id+"]").prop("selected",true);
				
            	$('#send_city_id').find("option[value="+k.city_id+"]").prop("selected",true);
            	$('#send_area_id').find("option[value="+k.area_id+"]").prop("selected",true);
            	$('#send_address').val(k.send_goods_address)
            	$('#send_name').val(k.send_goods_name)
            	$('#send_cellphone').val(k.send_goods_cellphone)
            	$('#send_goods_company').val(k.send_goods_company)
            	//再获取发站数据
            	
            		data = {city_id:k.city_id}
				   $.ajax({
				       type: "post",
				       url: "/source/getCityInfo",
				       data: data,
				       dataType: "json",
				       success: function(data){
				
				       		$('#send_location_id').html("");
				           	$('#send_location_id').append("<option value=''>请选择</option>")
				           		$.each(data,function(index,key){
				           			if(k.city_id == key.city_id){
					           			$('#send_location_id').append("<option  value='"+key.city_id+"' selected>"+key.city_name+"</option>")

				           			}else{
					           			$('#send_location_id').append("<option value='"+key.city_id+"'>"+key.city_name+"</option>")
				           				
				           			}
				           			
				           		})        			
				       		
				
				       		form.render('select');
				    
				      },
				      error:function(XMLHttpRequest, textStatus, errorThrown){
				   	   layer.msg('验证失败')
				      }
				   });	   	
            	form.render();  
            	
            	
            }
        },
        error:function(XMLHttpRequest, textStatus, errorThrown){
            layer.msg('验证失败')
        }
    });   
	

	
	form.on('select(goods_name)',function(e){

	goods_id=e.value
	console.log(e)
	})


	
	
	<?php if(\think\Request::instance()->get('order_id') == undefined): if($projectData['default_send_goods_id']): ?>
	var  value = <?php echo $projectData['default_send_goods_id']; ?>
   	//通过项目获取项目的所有信息
    $.ajax({
        type: "POST",
        url: '/source/showSendGoodsAjax',
        data: {
        	  'send_goods_id':value
        	
        	},
        success: function(data){

            if(data.code!=200){
                layer.msg(data.msg);
                return false;
            }else if(data.code==200) {
            	k = data.data[0];
			console.log(k)
            	$('#send_goods_id').val()      
        
            	$('#send_province_id').find("option[value="+k.province_id+"]").prop("selected",true);
				if(k.city_id)
            	$('#send_city_id').html("<option value=\""+k.city_id+"\">"+k.city_name+"</option>")
				if(k.area_id)
				$('#send_area_id').html("<option value=\""+k.area_id+"\">"+k.area_name+"</option>")
            	$('#send_address').val(k.send_goods_address)
            	$('#send_name').val(k.send_goods_name)
            	$('#send_cellphone').val(k.send_goods_cellphone)
            		$('#send_goods_company').val(k.send_goods_company)
            	//再获取发站数据
            	
            		data = {city_id:k.city_id}
				   $.ajax({
				       type: "post",
				       url: "/source/getCityInfo",
				       data: data,
				       dataType: "json",
				       success: function(data){
				
				       		$('#send_location_id').html("");
				           	$('#send_location_id').append("<option value=''>请选择</option>")
				           		$.each(data,function(index,key){
				           			if(k.city_id == key.city_id){
					           			$('#send_location_id').append("<option  value='"+key.city_id+"' selected>"+key.city_name+"</option>")

				           			}else{
					           			$('#send_location_id').append("<option value='"+key.city_id+"'>"+key.city_name+"</option>")
				           				
				           			}
				           			
				           		})        			
				       		
				
				       		form.render('select');
				    
				      },
				      error:function(XMLHttpRequest, textStatus, errorThrown){
				   	   layer.msg('验证失败')
				      }
				   });	   	
            	form.render();  
            	
            	
            }
        },
        error:function(XMLHttpRequest, textStatus, errorThrown){
            layer.msg('验证失败')
        }
    }); 

<?php endif; endif; ?>



form.on('select(estimated_pack_unit)',function(e){
var value=e.value;
var that=e.elem;
var option="";
var goods_packs=baseConfig.order.goods_pack;
var select1=$(that).parents("tr").find('td').eq(8).find('select')
for(item in goods_packs){
if(item==value)
option+="<option selected=\"selected\" value=\""+item+"\">"+goods_packs[item]+"</option>"
else
option+="<option  value=\""+item+"\">"+goods_packs[item]+"</option>"

}

select1.html(option);
form.render()
})

form.on('select(goods_name)',function(e){



var goods_id=e.value;
var that=e.elem;
if(goods_id)
{
$(that).parents("td").find("input").val($(that).find("option:selected").text())
$(that).parents("td").find("input").attr('goods-id',e.value);
}
else
$(that).parents("td").find("input").val("")
$.ajax({
url:"/source/getGoodsAjax",
data:{goods_id:goods_id},
success:function(e1){
var select=$(that).parents("tr").find('td').eq(3).find('select')
var select1=$(that).parents("tr").find('td').eq(8).find('select')
var goods_packs=baseConfig.order.goods_pack;
var option="";

for(item in goods_packs){
if(item==e1.goods_packaging_type)
option+="<option selected=\"selected\" value=\""+item+"\">"+goods_packs[item]+"</option>"
else
option+="<option  value=\""+item+"\">"+goods_packs[item]+"</option>"

}



select.html(option)
select1.html(option)

form.render()
//$(that).parents("tr").find('td').eq(3).find('select').innerHTML="";
}

})



})

form.on('select(shi-qu)', function(data){
layer.load();
var elem=data.elem;
	console.log('shi-qu1')
	var parent=$(this).parents(".layui-form-item")
	var select=$(this).parents(".layui-input-inline").find("select")
	
	switch(select.attr("name")){
		case"zhongzhuan_accept_city_id[]":$("[name='tihuo_send_city_id[]']").find("[value="+data.value+"]").attr("selected",true);break;
		case"tihuo_accept_city_id[]":$("[name='ganxian_send_city_id']").find("[value="+data.value+"]").attr("selected",true);break;
		case"ganxian_accept_city_id":$("[name='peisong_send_city_id']").find("[value="+data.value+"]").attr("selected",true);break;
	}

	
	
    
    var area=parent;

    //先 清空
    area.find(".area_id").html("<option value=''>请选择</option>");

    var  value = data.value


    if(value==''){

        return false;
    }
    data = {city_id:value}
    $.ajax({
        type: "post",
        url: "/publicshow/getCity",
        data: data,
		async:false,
        dataType: "json",
        success: function(data){
            //console.log(data);
            $('#area_id').html("");
            $('#area_id').append("<option value=''>请选择</option>")
            $.each(data,function(index,key){

                area.find(".area_id").append("<option value='"+key.city_id+"'>"+key.city_name+"</option>")

            })


            form.render('select');

        },
        error:function(XMLHttpRequest, textStatus, errorThrown){
            layer.msg('验证失败')
        }
    });
	
	
	  $.ajax({
        type: "post",
        url: "/source/getCityInfo",
        data: data,
        dataType: "json",
        success: function(data){
        		if($(elem).attr("id")=="send_city_id"){
            		$('#send_location_id').html("");
                	$('#send_location_id').append("<option value=''>请选择</option>")
                		$.each(data,function(index,key){
                			
                			$('#send_location_id').append("<option value='"+key.city_id+"'>"+key.city_name+"</option>")
                			
                		})               			
        			
        		}else{
        			
            		$('#accept_location_id').html("");
                	$('#accept_location_id').append("<option value=''>请选择</option>")
                		$.each(data,function(index,key){
                			
                			$('#accept_location_id').append("<option value='"+key.city_id+"'>"+key.city_name+"</option>")
                			
                		})               			
        		}
 			
        		

        		form.render('select');
     
       },
       error:function(XMLHttpRequest, textStatus, errorThrown){
    	   layer.msg('验证失败')
       },
					complete: function(e) {
						layer.closeAll()

					}
    });	
	
	
});
</script>



